<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>

<?php 
class engm002_cls
{
	public $wel_part_no="";
	public $wel_part_des="";
	public $wel_part_des1="";
	public $wel_part_des2="";
	public $wel_part_des3="";
	public $wel_part_des4="";
	public $wel_part_des5="";
	public $wel_cat_code="";
	public $wel_mbc_code="";
	public $wel_unit="";
	public $wel_eng_unit="";
	public $wel_eng_unit_rate="";
	public $wel_pur_unit="";
	public $wel_pur_unit_rate="";
	public $wel_pur_unit1="";
	public $wel_pur_unit_rate1="";
	public $wel_pur_unit2="";
	public $wel_pur_unit_rate2="";
	public $wel_cus_code="";
	public $wel_assort_pn="";
	public $wel_assort_qty="";
	public $wel_scp_fact="";
	
	public $wel_fam_code="";
	public $wel_proj_no="";
	public $wel_std_code="";
	public $wel_drwg_no="";
	public $wel_mould_no="";
	
	public $wel_avg_cost="";
	public $wel_std_cost="";
	public $wel_lat_cost="";
	public $wel_vqt_cost="";
	public $wel_max_pur_cost="";
	public $wel_min_pur_cost="";
	public $wel_pkg_ord="";
	public $wel_min_ord="";
	public $wel_pur_gday="";
	public $wel_lead_tm="";
	public $wel_prpocon="";
	public $wel_part_fob="";
	public $wel_checkyn="";
	public $wel_buyer_code="";
	
	public $wel_manu_tm="";
	public $wel_abc_code="";
	public $wel_min_stk="";
	public $wel_max_stk="";
	public $wel_open_pr="";
	public $wel_pmclead_tm="";
	public $wel_serv_day="";
	public $wel_ctr_code="";
	public $wel_issue_qty="";
	public $wel_std_lot_qty="";
	
	public $wel_net_wt="";
	public $wel_grs_wt="";
	public $wel_uow="";
	public $wel_length="";
	public $wel_width="";
	public $wel_height="";
	public $wel_uod="";
	public $wel_cbm="";
	public $wel_pcs_per="";
	
	public $wel_part_image;
	public $wel_part_image_path="";
	
	private $wel_prog_code="engm002";
	//读取物料档案
	public function read()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_read")){throw new Exception("unauthorized_access");}
			$sql="SELECT * FROM #__wel_partflm WHERE wel_part_no='".$this->wel_part_no."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	//查询sql时出错了
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_part_no_not_found");}	//没有符合条件的记录
			foreach ($row as $key=>$value){$return_val[$key]=$value;}
			throw new Exception("");
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	//新增单位档案
	public function addnew()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_addnew")){throw new Exception("unauthorized_access");}
			
			if($this->wel_part_no==""){throw new Exception("wel_part_no_miss");}
			if($this->wel_part_des==""){throw new Exception("wel_part_des_miss");}
			if($this->wel_cat_code==""){throw new Exception("wel_cat_code_miss");}
			if($this->wel_unit==""){throw new Exception("wel_unit_miss");}
			
			if($this->wel_eng_unit!=""){
				if(!is_numeric($this->wel_eng_unit_rate)){$this->wel_eng_unit_rate=0;}
				if($this->wel_eng_unit_rate==0){throw new Exception("wel_eng_unit_rate_miss");}
				if($this->wel_eng_unit_rate<0){throw new Exception("wel_eng_unit_rate_over_rang");}
			}
			
			if($this->wel_pur_unit!=""){
				if(!is_numeric($this->wel_pur_unit_rate)){$this->wel_pur_unit_rate=0;}
				if($this->wel_pur_unit_rate==0){throw new Exception("wel_pur_unit_rate_miss");}
				if($this->wel_pur_unit_rate<0){throw new Exception("wel_pur_unit_rate_over_rang");}
			}
			
			if($this->wel_pur_unit1!=""){
				if(!is_numeric($this->wel_pur_unit_rate1)){$this->wel_pur_unit_rate1=0;}
				if($this->wel_pur_unit_rate1==0){throw new Exception("wel_pur_unit_rate1_miss");}
				if($this->wel_pur_unit_rate1<0){throw new Exception("wel_pur_unit_rate1_over_rang");}
			}
			
			if($this->wel_pur_unit2!=""){
				if(!is_numeric($this->wel_pur_unit_rate2)){$this->wel_pur_unit_rate2=0;}
				if($this->wel_pur_unit_rate2==0){throw new Exception("wel_pur_unit_rate2_miss");}
				if($this->wel_pur_unit_rate2<0){throw new Exception("wel_pur_unit_rate2_over_rang");}
			}
			
			if(($this->wel_pur_unit1!="") && ($this->wel_pur_unit1==$this->wel_pur_unit)){
				throw new Exception("wel_pur_unit1_unit2_unit3_same");
			}
			
			if(($this->wel_pur_unit2!="") && ($this->wel_pur_unit2==$this->wel_pur_unit)){
				throw new Exception("wel_pur_unit1_unit2_unit3_same");
			}
			
			if(($this->wel_pur_unit2!="") && ($this->wel_pur_unit2==$this->wel_pur_unit1)){
				throw new Exception("wel_pur_unit1_unit2_unit3_same");
			}
			
			//类别档案是否存在
			$sql="SELECT * FROM #__wel_catcdem WHERE wel_cat_code='".$this->wel_cat_code."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_cat_code_not_found");}
			
			//仓存单位档案是否存在
			$sql="SELECT * FROM #__wel_unitflm WHERE wel_unit_code='".$this->wel_unit."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_unit_not_found");}
			
			//工程单位档案是否存在
			if($this->wel_eng_unit!=""){
				$sql="SELECT * FROM #__wel_unitflm WHERE wel_unit_code='".$this->wel_eng_unit."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_eng_unit_not_found");}
			}
			
			//采购单位1档案是否存在
			if($this->wel_pur_unit!=""){
				$sql="SELECT * FROM #__wel_unitflm WHERE wel_unit_code='".$this->wel_pur_unit."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_pur_unit_not_found");}
			}
			
			//采购单位2档案是否存在
			if($this->wel_pur_unit1!=""){
				$sql="SELECT * FROM #__wel_unitflm WHERE wel_unit_code='".$this->wel_pur_unit1."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_pur_unit1_not_found");}
			}
			
			//采购单位3档案是否存在
			if($this->wel_pur_unit2!=""){
				$sql="SELECT * FROM #__wel_unitflm WHERE wel_unit_code='".$this->wel_pur_unit2."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_pur_unit2_not_found");}
			}
			
			//客户档案是否存在
			if($this->wel_cus_code!=""){
				$sql="SELECT * FROM #__wel_cusmasm WHERE wel_cus_code='".$this->wel_cus_code."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_cus_code_not_found");}
			}
			
			//系列档案是否存在
			if($this->wel_fam_code!=""){
				$sql="SELECT * FROM #__wel_famcdem WHERE wel_fam_code='".$this->wel_fam_code."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_fam_code_not_found");}
			}
			
			//项目档案是否存在
			if($this->wel_proj_no!=""){
				$sql="SELECT * FROM #__wel_projflm WHERE wel_proj_no='".$this->wel_proj_no."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_proj_no_not_found");}
			}
			
			//采购员档案是否存在
			if($this->wel_buyer_code!=""){
				$sql="SELECT * FROM #__wel_purhanm WHERE wel_buyer_code='".$this->wel_buyer_code."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_buyer_code_not_found");}
			}
			
			//重量单位档案是否存在
			if($this->wel_uow!=""){
				$sql="SELECT * FROM #__wel_unitflm WHERE wel_unit_code='".$this->wel_uow."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_uow_not_found");}
			}
			
			//尺寸单位档案是否存在
			if($this->wel_uod!=""){
				$sql="SELECT * FROM #__wel_unitflm WHERE wel_unit_code='".$this->wel_uod."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_uod_not_found");}
			}
			
			//生产中心档案是否存在
			if($this->wel_ctr_code!=""){
				$sql="SELECT * FROM #__wel_centrem WHERE wel_ctr_code='".$this->wel_ctr_code."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_ctr_code_not_found");}
			}
			
			//物料档案是否存在
			$sql="SELECT * FROM #__wel_partflm WHERE wel_part_no='".$this->wel_part_no."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if($row=mysql_fetch_array($result)){throw new Exception("wel_part_no_exist");}
			
			try
			{
				mysql_query("begin");
				//插入记录到wel_partflm物料档案中
				//基本档案
				$sql="INSERT INTO #__wel_partflm(wel_part_no,wel_part_des,wel_part_des1,wel_part_des2,".
							"wel_part_des3,wel_part_des4,wel_part_des5,wel_cat_code,wel_mbc_code,".
							"wel_unit,wel_eng_unit,wel_eng_unit_rate,wel_pur_unit,wel_pur_unit_rate,wel_pur_unit1,".
							"wel_pur_unit_rate1,wel_pur_unit2,wel_pur_unit_rate2,".
							"wel_cus_code,wel_assort_pn,wel_assort_qty,".
							"wel_scp_fact,";
				//工程档案
				$sql.="wel_fam_code,wel_proj_no,wel_std_code,wel_drwg_no,wel_mould_no,";
				//采购档案
				$sql.="wel_avg_cost,wel_std_cost,wel_pkg_ord,wel_pur_gday,wel_buyer_code,wel_min_ord,".
							"wel_lead_tm,wel_prpocon,wel_part_fob,wel_checkyn,";
				//生产物控
				$sql.="wel_manu_tm,wel_abc_code,wel_min_stk,wel_max_stk,wel_open_pr,wel_pmclead_tm,".
							"wel_serv_day,wel_ctr_code,wel_issue_qty,wel_std_lot_qty,";
				//物料规格
				$sql.="wel_net_wt,wel_grs_wt,wel_uow,wel_length,wel_width,wel_height,wel_uod,wel_cbm,".
						"wel_pcs_per,";
				$sql.="wel_crt_user,wel_crt_date) ".
						"VALUES('".$this->wel_part_no."','".$this->wel_part_des."',".
							"'".$this->wel_part_des1."','".$this->wel_part_des2."',".
							"'".$this->wel_part_des3."','".$this->wel_part_des4."',".
							"'".$this->wel_part_des5."','".$this->wel_cat_code."',".
							"'".$this->wel_mbc_code."','".$this->wel_unit."',".
							"'".$this->wel_eng_unit."',".doubleval($this->wel_eng_unit_rate).",".
							"'".$this->wel_pur_unit."',".
							"".doubleval($this->wel_pur_unit_rate).",'".$this->wel_pur_unit1."',".
							"".doubleval($this->wel_pur_unit_rate1).",'".$this->wel_pur_unit2."',".
							"".doubleval($this->wel_pur_unit_rate2).",".
							"'".$this->wel_cus_code."',".
							"".$this->wel_assort_pn.",".intval($this->wel_assort_qty).",".
							"".doubleval($this->wel_scp_fact).",";
				//工程档案
				$sql.="'".$this->wel_fam_code."','".$this->wel_proj_no."',".
							"'".$this->wel_std_code."','".$this->wel_drwg_no."',".
							"'".$this->wel_mould_no."',";
				//采购档案
				$sql.="".doubleval($this->wel_avg_cost).",".doubleval($this->wel_std_cost).",".
							"".doubleval($this->wel_pkg_ord).",".intval($this->wel_pur_gday).",".
							"'".$this->wel_buyer_code."',".doubleval($this->wel_min_ord).",".
							"".intval($this->wel_lead_tm).",".$this->wel_prpocon.",".
							"".$this->wel_part_fob.",".intval($this->wel_checkyn).",";
				//生产物控
				$sql.="".intval($this->wel_manu_tm).",'".$this->wel_abc_code."',".
							"".doubleval($this->wel_min_stk).",".doubleval($this->wel_max_stk).",".
							"".doubleval($this->wel_open_pr).",".intval($this->wel_pmclead_tm).",".
							"".intval($this->wel_serv_day).",'".$this->wel_ctr_code."',".
							"".doubleval($this->wel_issue_qty).",".doubleval($this->wel_std_lot_qty).",";
				//物料规格
				$sql.="".doubleval($this->wel_net_wt).",".doubleval($this->wel_grs_wt).",".
						"'".$this->wel_uow."',".doubleval($this->wel_length).",".
						"".doubleval($this->wel_width).",".doubleval($this->wel_height).",".
						"'".$this->wel_uod."',".doubleval($this->wel_cbm).",".
						"".intval($this->wel_pcs_per).",";
				$sql.="'".$_SESSION["wel_user_id"]."',now())";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
					
				mysql_query("commit");
			}
			catch(Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			} 
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		if($msg_code==""){$msg_code="addnew_succee";}
		$return_val["msg_code"]=$msg_code;
		$return_val["wel_part_no"]=$this->wel_part_no;
		return $return_val;
	}
	//编辑单位档案
	public function edit()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_edit")){throw new Exception("unauthorized_access");}
			
			if($this->wel_part_no==""){throw new Exception("wel_part_no_miss");}
			if($this->wel_part_des==""){throw new Exception("wel_part_des_miss");}
			if($this->wel_cat_code==""){throw new Exception("wel_cat_code_miss");}
			if($this->wel_unit==""){throw new Exception("wel_unit_miss");}
			
			if($this->wel_eng_unit!=""){
				if(!is_numeric($this->wel_eng_unit_rate)){$this->wel_eng_unit_rate=0;}
				if($this->wel_eng_unit_rate==0){throw new Exception("wel_eng_unit_rate_miss");}
				if($this->wel_eng_unit_rate<0){throw new Exception("wel_eng_unit_rate_over_rang");}
			}
			
			if($this->wel_pur_unit!=""){
				if(!is_numeric($this->wel_pur_unit_rate)){$this->wel_pur_unit_rate=0;}
				if($this->wel_pur_unit_rate==0){throw new Exception("wel_pur_unit_rate_miss");}
				if($this->wel_pur_unit_rate<0){throw new Exception("wel_pur_unit_rate_over_rang");}
			}
			
			if($this->wel_pur_unit1!=""){
				if(!is_numeric($this->wel_pur_unit_rate1)){$this->wel_pur_unit_rate1=0;}
				if($this->wel_pur_unit_rate1==0){throw new Exception("wel_pur_unit_rate1_miss");}
				if($this->wel_pur_unit_rate1<0){throw new Exception("wel_pur_unit_rate1_over_rang");}
			}
			
			if($this->wel_pur_unit2!=""){
				if(!is_numeric($this->wel_pur_unit_rate2)){$this->wel_pur_unit_rate2=0;}
				if($this->wel_pur_unit_rate2==0){throw new Exception("wel_pur_unit_rate2_miss");}
				if($this->wel_pur_unit_rate2<0){throw new Exception("wel_pur_unit_rate2_over_rang");}
			}
			
			if(($this->wel_pur_unit1!="") && ($this->wel_pur_unit1==$this->wel_pur_unit)){
				throw new Exception("wel_pur_unit1_unit2_unit3_same");
			}
			if(($this->wel_pur_unit2!="") && ($this->wel_pur_unit2==$this->wel_pur_unit)){
				throw new Exception("wel_pur_unit1_unit2_unit3_same");
			}
			if(($this->wel_pur_unit2!="") && ($this->wel_pur_unit2==$this->wel_pur_unit1)){
				throw new Exception("wel_pur_unit1_unit2_unit3_same");
			}
			
			//类别档案是否存在
			$sql="SELECT * FROM #__wel_catcdem WHERE wel_cat_code='".$this->wel_cat_code."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_cat_code_not_found");}
			
			//仓存单位档案是否存在
			$sql="SELECT * FROM #__wel_unitflm WHERE wel_unit_code='".$this->wel_unit."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_unit_not_found");}
			
			//工程单位档案是否存在
			if($this->wel_eng_unit!=""){
				$sql="SELECT * FROM #__wel_unitflm WHERE wel_unit_code='".$this->wel_eng_unit."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_eng_unit_not_found");}
			}
			
			//采购单位1档案是否存在
			if($this->wel_pur_unit!=""){
				$sql="SELECT * FROM #__wel_unitflm WHERE wel_unit_code='".$this->wel_pur_unit."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_pur_unit_not_found");}
			}
			
			//采购单位2档案是否存在
			if($this->wel_pur_unit1!=""){
				$sql="SELECT * FROM #__wel_unitflm WHERE wel_unit_code='".$this->wel_pur_unit1."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_pur_unit1_not_found");}
			}
			//采购单位3档案是否存在
			if($this->wel_pur_unit2!=""){
				$sql="SELECT * FROM #__wel_unitflm WHERE wel_unit_code='".$this->wel_pur_unit2."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_pur_unit2_not_found");}
			}
			
			//客户档案是否存在
			if($this->wel_cus_code!=""){
				$sql="SELECT * FROM #__wel_cusmasm WHERE wel_cus_code='".$this->wel_cus_code."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_cus_code_not_found");}
			}
			
			//系列档案是否存在
			if($this->wel_fam_code!=""){
				$sql="SELECT * FROM #__wel_famcdem WHERE wel_fam_code='".$this->wel_fam_code."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_fam_code_not_found");}
			}
			
			//项目档案是否存在
			if($this->wel_proj_no!=""){
				$sql="SELECT * FROM #__wel_projflm WHERE wel_proj_no='".$this->wel_proj_no."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_proj_no_not_found");}
			}
			
			//采购员档案是否存在
			if($this->wel_buyer_code!=""){
				$sql="SELECT * FROM #__wel_purhanm WHERE wel_buyer_code='".$this->wel_buyer_code."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_buyer_code_not_found");}
			}
			
			//重量单位档案是否存在
			if($this->wel_uow!=""){
				$sql="SELECT * FROM #__wel_unitflm WHERE wel_unit_code='".$this->wel_uow."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_uow_not_found");}
			}
			//尺寸单位档案是否存在
			if($this->wel_uod!=""){
				$sql="SELECT * FROM #__wel_unitflm WHERE wel_unit_code='".$this->wel_uod."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_uod_not_found");}
			}
			
			//生产中心档案是否存在
			if($this->wel_ctr_code!=""){
				$sql="SELECT * FROM #__wel_centrem WHERE wel_ctr_code='".$this->wel_ctr_code."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_ctr_code_not_found");}
			}
			
			//物料档案是否存在
			$sql="SELECT * FROM #__wel_partflm WHERE wel_part_no='".$this->wel_part_no."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_part_no_not_found");}
			
			try
			{
				mysql_query("begin");
				//更新wel_partflm物料档案
				//基本资料
				$sql="UPDATE #__wel_partflm SET ".
							"wel_part_des='".$this->wel_part_des."',".
							"wel_part_des1='".$this->wel_part_des1."',".
							"wel_part_des2='".$this->wel_part_des2."',".
							"wel_part_des3='".$this->wel_part_des3."',".
							"wel_part_des4='".$this->wel_part_des4."',".
							"wel_part_des5='".$this->wel_part_des5."',".
							"wel_cat_code='".$this->wel_cat_code."',".
							"wel_mbc_code='".$this->wel_mbc_code."',".
							"wel_unit='".$this->wel_unit."',".
							"wel_eng_unit='".$this->wel_eng_unit."',".
							"wel_eng_unit_rate=".doubleval($this->wel_eng_unit_rate).",".
							"wel_pur_unit='".$this->wel_pur_unit."',".
							"wel_pur_unit_rate=".doubleval($this->wel_pur_unit_rate).",".
							"wel_pur_unit1='".$this->wel_pur_unit1."',".
							"wel_pur_unit_rate1=".doubleval($this->wel_pur_unit_rate1).",".
							"wel_pur_unit2='".$this->wel_pur_unit2."',".
							"wel_pur_unit_rate2=".doubleval($this->wel_pur_unit_rate2).",".
							"wel_cus_code='".$this->wel_cus_code."',";
				if($row["wel_bom_yn"]==0)
				{
					$sql.="wel_assort_pn=".$this->wel_assort_pn.",".
							"wel_assort_qty=".intval($this->wel_assort_qty).",";
				}
				$sql.="wel_scp_fact=".doubleval($this->wel_scp_fact).",";
				//工程档案
				$sql.="wel_fam_code='".$this->wel_fam_code."',".
						"wel_proj_no='".$this->wel_proj_no."',".
						"wel_std_code='".$this->wel_std_code."',".
						"wel_drwg_no='".$this->wel_drwg_no."',".
						"wel_mould_no='".$this->wel_mould_no."',";
				//采购档案
				$sql.="wel_avg_cost=".doubleval($this->wel_avg_cost).",".
						"wel_std_cost=".doubleval($this->wel_std_cost).",".
						"wel_pkg_ord=".doubleval($this->wel_pkg_ord).",".
						"wel_pur_gday=".intval($this->wel_pur_gday).",".
						"wel_buyer_code='".$this->wel_buyer_code."',".
						"wel_min_ord=".doubleval($this->wel_min_ord).",".
						"wel_lead_tm=".intval($this->wel_lead_tm).",".
						"wel_prpocon=".$this->wel_prpocon.",".
						"wel_part_fob=".$this->wel_part_fob.",".
						"wel_checkyn=".intval($this->wel_checkyn).",";
				//生产物控
				$sql.="wel_manu_tm=".intval($this->wel_manu_tm).",".
						"wel_abc_code='".$this->wel_abc_code."',".
						"wel_min_stk=".doubleval($this->wel_min_stk).",".
						"wel_max_stk=".doubleval($this->wel_max_stk).",".
						"wel_open_pr=".doubleval($this->wel_open_pr).",".
						"wel_pmclead_tm=".intval($this->wel_pmclead_tm).",".
						"wel_serv_day=".intval($this->wel_serv_day).",".
						"wel_ctr_code='".$this->wel_ctr_code."',".
						"wel_issue_qty=".doubleval($this->wel_issue_qty).",".
						"wel_std_lot_qty=".doubleval($this->wel_std_lot_qty).",";
				//物料规格
				$sql.="wel_net_wt=".doubleval($this->wel_net_wt).",".
						"wel_grs_wt=".doubleval($this->wel_grs_wt).",".
						"wel_uow='".$this->wel_uow."',".
						"wel_length=".doubleval($this->wel_length).",".
						"wel_width=".doubleval($this->wel_width).",".
						"wel_height=".doubleval($this->wel_height).",".
						"wel_uod='".$this->wel_uod."',".
						"wel_cbm=".doubleval($this->wel_cbm).",".
						"wel_pcs_per=".intval($this->wel_pcs_per).",";
				$sql.="wel_upd_user='".$_SESSION["wel_user_id"]."',".
						"wel_upd_date=now() ".
					"WHERE wel_part_no='".$this->wel_part_no."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}	
				mysql_query("commit");
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		if($msg_code==""){$msg_code="edit_succee";}		
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	//删除物料档案
	public function delete()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_delete")){throw new Exception("unauthorized_access");}
			
			if($this->wel_part_no==""){throw new Exception("wel_part_no_miss");}
			
			//物料档案是否存在
			$sql="SELECT * FROM #__wel_partflm WHERE wel_part_no='".$this->wel_part_no."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_part_no_not_found");}
			
			//物料是否存在于PO中
			$sql="SELECT * FROM #__wel_pordetm WHERE wel_part_no='".$this->wel_part_no."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if($row=mysql_fetch_array($result)){throw new Exception("wel_part_no_in_wel_pordetm");}
			
			//物料是否存在于仓存中
			$sql="SELECT * FROM #__wel_whlotfm WHERE wel_part_no='".$this->wel_part_no."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if($row=mysql_fetch_array($result)){throw new Exception("wel_part_no_in_wel_whlotfm");}
			
			//物料是否存在于SO中
			$sql="SELECT * FROM #__wel_sordetm WHERE wel_part_no='".$this->wel_part_no."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if($row=mysql_fetch_array($result)){throw new Exception("wel_part_no_in_wel_sordetm");}
			
			//物料是否存在于BOM中
			$sql="SELECT * FROM #__wel_engbomm WHERE ".
				"wel_assm_no='".$this->wel_part_no."' or wel_part_no='".$this->wel_part_no."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if($row=mysql_fetch_array($result)){throw new Exception("wel_part_no_in_wel_engbomm");}
			
			try
			{
				mysql_query("begin");
				//删除wel_partflm物料档案
				$sql="DELETE FROM #__wel_partflm WHERE wel_part_no='".$this->wel_part_no."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}

				mysql_query("commit");
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}		
		if($msg_code==""){$msg_code="delete_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	//上传图片
	public function upload()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_edit")){throw new Exception("unauthorized_access");}
			
			if($this->wel_part_no==""){throw new Exception("wel_part_no_miss");}
			
			//物料档案是否存在
			$sql="SELECT * FROM #__wel_partflm WHERE wel_part_no='".$this->wel_part_no."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_part_no_not_found");}
			
			try
			{
				mysql_query("begin");
				
				$arr__images=$this->wel_part_image;
				//设置允许上传文件格式
				$arr__filetypes=array(
					"image/jpg",
					"image/jpeg",
					"image/png",
					"image/pjpeg",
					"image/gif",
					"image/gif",
					"image/bmp",
					"image/x-png");
				
				$str__filetype=$arr__images["type"];
				$arr__pathinfo=pathinfo($arr__images["name"]);
				$str__file_extension=$arr__pathinfo["extension"];
				$str__destfile_name=trim(strtoupper($this->wel_part_no)).".".$str__file_extension;
				$str__sourcefile=$arr__images["tmp_name"];
				$str__destfile=$this->wel_part_image_path.$str__destfile_name;
				
				//上传文件是否存在
				if(!is_uploaded_file($str__sourcefile)){throw new Exception("upload_file_not_found");}
				
				//上传文件类型是否存在是允许上传的文件类型
				if(!in_array($str__filetype,$arr__filetypes)){throw new Exception("upload_file_filetype_not_allow");}
				
				//上传目标目录是否存在
				if(!file_exists($this->wel_part_image_path)){
					$origmask = @umask(0);
					mkdir($this->wel_part_image_path,0755,true);
					@umask($origmask);
				}
				
				if(!move_uploaded_file($str__sourcefile,$str__destfile)){throw new Exception("upload_file_move_error");}
				@unlink($str__sourcefile);
			
				$sql="UPDATE #__wel_partflm SET ".
					"wel_part_image='".$str__destfile_name."' ".
					"WHERE wel_part_no='".$this->wel_part_no."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
			
				mysql_query("commit");
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}		
		if($msg_code=="")
		{
			$msg_code="upload_succee";
			$return_val["msg_code"]=$msg_code;
			$return_val["wel_part_image_address"]=$str__destfile_name;
		}
		return $return_val;
	}
}
?>