<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT

?>
<?php 
class engm003_cls
{
	public $wel_fg_no="";
	public $wel_assm_no="";
	public $wel_fg_no_alt="";
	public $wel_assm_no_alt="";
	public $wel_part_no="";
	public $wel_part_no_alt="";
	public $wel_alt_part_alt="";
	public $wel_eng_unit="";
	public $wel_scp_fact="";
	public $wel_qp_eng="";
	public $wel_bom_rmk="";
	public $wel_bom_loc="";
	public $wel_featu_yn=0;
	public $wel_ex_qty_alt=0;
	public $wel_priority_alt=0;
	
	public $wel_assm_no_to="";
	public $wel_assm_no_from="";
	public $wel_alt_copy=0;
	
	private $wel_prog_code="engm003";
	//读取bom档案
	public function read()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_read")){throw new Exception("unauthorized_access");}
			
			$sql="SELECT * FROM #__wel_partflm WHERE #__wel_partflm.wel_part_no='".$this->wel_fg_no."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	//查询sql时出错了
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_part_no_not_found");}	//没有符合条件的记录
			foreach ($row as $key=>$value){$return_val[$key]=$value;}
			throw new Exception("");
		}
		catch (Exception $e){$msg_code=$e->getMessage();}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	//detail读取数据
	public function detail_read()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_read")){throw new Exception("unauthorized_access");}
			
			if($this->wel_assm_no==""){throw new Exception("wel_assm_no_miss");}
			if($this->wel_part_no==""){throw new Exception("wel_part_no_miss");}
			
			$sql="SELECT engbomm.*,
				assmflm.wel_part_des as wel_assm_des,
				partflm.wel_part_des as wel_part_des, 
				partflm.wel_unit as wel_unit 
				FROM #__wel_engbomm as engbomm LEFT JOIN #__wel_partflm as assmflm 
				ON engbomm.wel_assm_no=assmflm.wel_part_no LEFT JOIN #__wel_partflm as partflm 
				ON engbomm.wel_part_no=partflm.wel_part_no WHERE 
				engbomm.wel_assm_no='".$this->wel_assm_no."' AND 
				engbomm.wel_part_no='".$this->wel_part_no."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	//查询sql时出错了
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_bom_detail_not_found");}	//没有符合条件的记录
			foreach ($row as $key=>$value){$return_val[$key]=$value;}
			throw new Exception("");
		}
		catch (Exception $e){$msg_code=$e->getMessage();}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	//读取bom细节代用物料
	public function detail_alt_part_read()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_read")){throw new Exception("unauthorized_access");}
			
			if($this->wel_fg_no_alt==""){throw new Exception("wel_fg_no_alt_miss");}
			if($this->wel_assm_no_alt==""){throw new Exception("wel_assm_no_alt_miss");}
			if($this->wel_part_no_alt==""){throw new Exception("wel_part_no_alt_miss");}
			if($this->wel_alt_part_alt==""){throw new Exception("wel_alt_part_alt_miss");}
			
			//物料清单代用物料记录是否存在
			$sql="SELECT * FROM #__wel_altparm ".
				"WHERE wel_prod_no='".$this->wel_fg_no_alt."' ".
					"AND wel_assm_no='".$this->wel_assm_no_alt."' ".
					"AND wel_part_no= '".$this->wel_part_no_alt."' ".
					"AND wel_alt_part='".$this->wel_alt_part_alt."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	//查询sql时出错了
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_alt_part_alt_not_found");}	//没有符合条件的记录
			foreach ($row as $key=>$value){$return_val[$key]=$value;}
			throw new Exception("");
		}
		catch (Exception $e){$msg_code=$e->getMessage();}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	private function approve_loop($wel_assm_no,$approve_flag)
	{
		$msg_code="";
		try 
		{
			$conn=werp_db_connect();
			
			$sql="SELECT * FROM #__wel_partflm WHERE wel_part_no='".$wel_assm_no."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_part_no_not_found");}
			
			//第一次调用，$wel_assm_no没有子物料，也要批核，以便在ECN中使用
			if($row["wel_bom_yn"]!=$approve_flag){	//批核标志不同，则需要批成相同
				$sql="UPDATE #__wel_partflm SET 
						wel_bom_yn=".$approve_flag.",
						wel_bom_date=now(),
						wel_bom_by='".$_SESSION["wel_user_id"]."' 
						WHERE wel_part_no='".$wel_assm_no."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			}

			//然后批核$wel_assm_no下有子物料的$wel_part_no
			$sql="SELECT wel_part_no FROM #__wel_engbomm as engbomm WHERE wel_assm_no='".$wel_assm_no."' AND ".
				"exists(SELECT wel_part_no FROM #__wel_engbomm WHERE wel_assm_no=engbomm.wel_part_no LIMIT 1)";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			while ($row=mysql_fetch_array($result))
			{
				$wel_part_no=is_null($row["wel_part_no"]) ? "" : $row["wel_part_no"];
				$msg_code=$this->approve_loop($wel_part_no,$approve_flag);
				if($msg_code!=""){throw new Exception($msg_code);}
			}
		}
		catch (Exception $e){$msg_code=$e->getMessage();}
		return $msg_code;
	}
	
	//批核 
	public function approve()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_approve")){throw new Exception("unauthorized_access");}
			
			if($this->wel_fg_no==""){throw new Exception("wel_part_no_miss");}
			
			try
			{
				mysql_query('begin');
				$msg_code=$this->approve_loop($this->wel_fg_no,1);
				if($msg_code!=""){throw new Exception($msg_code);}
				mysql_query('commit');
			}
			catch (Exception $e1)
			{
				mysql_query('rollback');
				throw new Exception($e1->getMessage());
			}
		}
		catch (Exception $e){$msg_code=$e->getMessage();}
		if($msg_code==""){$msg_code="approve_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	//取消批核
	public function not_approve()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_approve")){throw new Exception("unauthorized_access");}
			
			if($this->wel_fg_no==""){throw new Exception("wel_part_no_miss");}
			
			try
			{
				mysql_query('begin');
				$msg_code=$this->approve_loop($this->wel_fg_no,0);
				if($msg_code!=""){throw new Exception($msg_code);}
				mysql_query('commit');
			}
			catch (Exception $e1)
			{
				mysql_query('rollback');
				throw new Exception($e1->getMessage());
			}
		}
		catch (Exception $e){$msg_code=$e->getMessage();}
		if($msg_code==""){$msg_code="not_approve_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}

	//detail 添加数据
	public function detail_addnew()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_addnew")){throw new Exception("unauthorized_access");}
			
			if($this->wel_assm_no==""){throw new Exception("wel_assm_no_miss");}
			if($this->wel_part_no==""){throw new Exception("wel_part_no_miss");}
			if($this->wel_eng_unit==""){throw new Exception("wel_eng_unit_miss");}
			if(!is_numeric($this->wel_scp_fact)){$this->wel_scp_fact=0;}
			if(!is_numeric($this->wel_qp_eng)){$this->wel_qp_eng=0;}
			if($this->wel_qp_eng==0){throw new Exception("wel_qp_eng_miss");}
			if($this->wel_qp_eng<0){throw new Exception("wel_qp_eng_over_rang");}
			if($this->wel_scp_fact<0){throw new Exception("wel_scp_fact_over_rang");}
			
			if(strtoupper($this->wel_assm_no)==strtoupper($this->wel_part_no)){
				throw new Exception("wel_assm_no_same_wel_part_no");
			}
			
			//工程单位档案是否存在
			$sql="SELECT * FROM #__wel_unitflm WHERE wel_unit_code='".$this->wel_eng_unit."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_eng_unit_not_found");}
			
			//品号(父件)是否存在
			$sql="SELECT * FROM #__wel_partflm WHERE wel_part_no='".$this->wel_assm_no."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_assm_no_not_found");}
			if($row["wel_bom_yn"]==1){throw new Exception("wel_bom_had_approved");}
			
			//品号(子件)是否存在
			$sql="SELECT * FROM #__wel_partflm WHERE wel_part_no='".$this->wel_part_no."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_part_no_not_found");}
			$wel_unit=trim(is_null($row["wel_unit"]) ? "" : $row["wel_unit"]);
			$wel_eng_unit=trim(is_null($row["wel_eng_unit"]) ? "" : $row["wel_eng_unit"]);
			$wel_eng_unit_rate=floatval(is_null($row["wel_eng_unit_rate"]) ? 0 : $row["wel_eng_unit_rate"]);
			
			if($wel_eng_unit==""){
				$wel_eng_unit=$wel_unit;
				$wel_eng_unit_rate=1;
			}
			if($wel_eng_unit_rate<=0){$wel_eng_unit_rate=1;}
			if(strtolower($wel_eng_unit)==strtolower($this->wel_eng_unit)){
			}else{
				if(strtolower($wel_unit)==strtolower($this->wel_eng_unit)){
					$wel_eng_unit_rate=1;
				}else{
					throw new Exception("wel_eng_unit_error");
				}
			}
			$wel_qty_per=number_format(floatval($this->wel_qp_eng)*floatval($wel_eng_unit_rate),6);
			
			
			//在bom中已存在
			$sql="SELECT * FROM #__wel_engbomm ".
				"WHERE wel_assm_no='".$this->wel_assm_no."' AND wel_part_no='".$this->wel_part_no."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(($row=mysql_fetch_array($result))){throw new Exception("wel_bom_detail_exist");}
			
			//该品号(子件)是否循环
			if($this->bom_loop($this->wel_assm_no,$this->wel_part_no)){
				throw new Exception("wel_bom_loop_error");
			}
			
			try
			{
				mysql_query('begin');
				
				$sql="INSERT INTO #__wel_engbomm(
					wel_assm_no,
					wel_part_no,
					wel_scp_fact,
					wel_eng_unit,
					wel_qp_eng,
					wel_unit_exg,
					wel_qty_per,
					wel_bom_rmk,
					wel_bom_loc,
					wel_featu_yn,
					wel_crt_user,
					wel_crt_date) 
					VALUES(
					'".$this->wel_assm_no."',
					'".$this->wel_part_no."',
					".ROUND(floatval($this->wel_scp_fact),6).",
					'".$this->wel_eng_unit."',
					'".ROUND(floatval($this->wel_qp_eng),6)."',
					".ROUND(floatval($wel_eng_unit_rate),6).",
					".ROUND(floatval($wel_qty_per),6).",
					'".$this->wel_bom_rmk."',
					'".$this->wel_bom_loc."',
					".intval($this->wel_featu_yn,10).",
					'".$_SESSION["wel_user_id"]."',
					now() )";
				$sql=revert_to_the_available_sql($sql);
				if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				
				mysql_query('commit');
			}
			catch (Exception $e1){
				mysql_query('rollback');
				throw new Exception($e1->getMessage());
			}
		}
		catch (Exception $e){$msg_code=$e->getMessage();}
		if($msg_code==""){$msg_code="addnew_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	//detail编辑数据
	public function detail_edit()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_edit")){throw new Exception("unauthorized_access");}
			
			if($this->wel_assm_no==""){throw new Exception("wel_assm_no_miss");}
			if($this->wel_part_no==""){throw new Exception("wel_part_no_miss");}
			if($this->wel_eng_unit==""){throw new Exception("wel_eng_unit_miss");}
			if(!is_numeric($this->wel_scp_fact)){$this->wel_scp_fact=0;}
			if(!is_numeric($this->wel_qp_eng)){$this->wel_qp_eng=0;}
			if($this->wel_qp_eng==0){throw new Exception("wel_qp_eng_miss");}
			if($this->wel_qp_eng<0){throw new Exception("wel_qp_eng_over_rang");}
			if($this->wel_scp_fact<0){throw new Exception("wel_scp_fact_over_rang");}
			
			if(strtoupper($this->wel_assm_no)==strtoupper($this->wel_part_no)){
				throw new Exception("wel_assm_no_same_wel_part_no");
			}
			
			//工程单位档案是否存在
			$sql="SELECT * FROM #__wel_unitflm WHERE wel_unit_code='".$this->wel_eng_unit."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_eng_unit_not_found");}
			
			//品号(父件)是否存在
			$sql="SELECT * FROM #__wel_partflm WHERE wel_part_no='".$this->wel_assm_no."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_assm_no_not_found");}
			if($row["wel_bom_yn"]==1){throw new Exception("wel_bom_had_approved");}
			
			//品号(子件)是否存在
			$sql="SELECT * FROM #__wel_partflm WHERE wel_part_no='".$this->wel_part_no."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_part_no_not_found");}
			$wel_unit=trim(is_null($row["wel_unit"]) ? "" : $row["wel_unit"]);
			$wel_eng_unit=trim(is_null($row["wel_eng_unit"]) ? "" : $row["wel_eng_unit"]);
			$wel_eng_unit_rate=floatval(is_null($row["wel_eng_unit_rate"]) ? 0 : $row["wel_eng_unit_rate"]);
			
			if($wel_eng_unit==""){
				$wel_eng_unit=$wel_unit;
				$wel_eng_unit_rate=1;
			}
			if($wel_eng_unit_rate<=0){$wel_eng_unit_rate=1;}
			if(strtolower($wel_eng_unit)==strtolower($this->wel_eng_unit)){
			}else{
				if(strtolower($wel_unit)==strtolower($this->wel_eng_unit)){
					$wel_eng_unit_rate=1;
				}else{
					throw new Exception("wel_eng_unit_error");
				}
			}
			$wel_qty_per=number_format(floatval($this->wel_qp_eng)*floatval($wel_eng_unit_rate),6);
			
			
			//在bom中已存在
			$sql="SELECT * FROM #__wel_engbomm ".
				"WHERE wel_assm_no='".$this->wel_assm_no."' AND wel_part_no='".$this->wel_part_no."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_bom_detail_not_found");}
			
			//该品号(子件)是否循环
			if($this->bom_loop($this->wel_assm_no,$this->wel_part_no)){
				throw new Exception("wel_bom_loop_error");
			}
			
			try
			{
				mysql_query('begin');

				$sql="UPDATE #__wel_engbomm SET 
					wel_scp_fact=".ROUND(floatval($this->wel_scp_fact),6).",
					wel_eng_unit='".$this->wel_eng_unit."',
					wel_qp_eng=".ROUND(floatval($this->wel_qp_eng),6).",
					wel_unit_exg=".ROUND(floatval($wel_eng_unit_rate),6).",
					wel_qty_per=".ROUND(floatval($wel_qty_per),6).",
					wel_bom_rmk='".$this->wel_bom_rmk."',
					wel_bom_loc='".$this->wel_bom_loc."',
					wel_featu_yn=".intval($this->wel_featu_yn,10).",
					wel_upd_user='".$_SESSION["wel_user_id"]."',
					wel_upd_date=now() 
					WHERE wel_assm_no='".$this->wel_assm_no."' AND wel_part_no='".$this->wel_part_no."'";
				$sql=revert_to_the_available_sql($sql);
				if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				
				mysql_query('commit');
			}
			catch (Exception $e1){
				mysql_query('rollback');
				throw new Exception($e1->getMessage());
			}
		}
		catch (Exception $e){$msg_code=$e->getMessage();}		
		if($msg_code==""){$msg_code="edit_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	//bom细节删除
	public function detail_del()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_delete")){throw new Exception("unauthorized_access");}
			
			if($this->wel_assm_no==""){throw new Exception("wel_assm_no_miss");}
			if($this->wel_part_no==""){throw new Exception("wel_part_no_miss");}
			
			//在bom中已存在
			$sql="SELECT * FROM #__wel_engbomm ".
				"WHERE wel_assm_no='".$this->wel_assm_no."' AND wel_part_no='".$this->wel_part_no."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_bom_detail_not_found");}

			try
			{
				mysql_query('begin');

				$sql="DELETE FROM #__wel_engbomm 
					WHERE wel_assm_no='".$this->wel_assm_no."' AND wel_part_no='".$this->wel_part_no."'";
				$sql=revert_to_the_available_sql($sql);
				if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}

				mysql_query('commit');
			}
			catch (Exception $e1){
				mysql_query('rollback');
				throw new Exception($e1->getMessage());
			}
		}
		catch (Exception $e){$msg_code=$e->getMessage();}		
		if($msg_code==""){$msg_code="delete_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	//bom细节全部删除
	public function detail_del_all()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_delete")){throw new Exception("unauthorized_access");}
			
			if($this->wel_fg_no==""){throw new Exception("wel_assm_no_miss");}
			
			$sql="SELECT wel_assm_no FROM #__wel_engbomm WHERE wel_assm_no='".$this->wel_fg_no."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_bom_detail_not_found");}
			
			try
			{
				mysql_query('begin');

				$sql="DELETE FROM #__wel_engbomm WHERE wel_assm_no='".$this->wel_fg_no."'";
				$sql=revert_to_the_available_sql($sql);
				if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					
				mysql_query('commit');
			}
			catch (Exception $e1){
				mysql_query('rollback');
				throw new Exception($e1->getMessage());
			}
		}
		catch (Exception $e){$msg_code=$e->getMessage();}		
		if($msg_code==""){$msg_code="delete_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	//新增代用物料
	public function detail_alt_part_addnew()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_addnew")){throw new Exception("unauthorized_access");}
			
			if($this->wel_fg_no_alt==""){throw new Exception("wel_fg_no_alt_miss");}
			if($this->wel_assm_no_alt==""){throw new Exception("wel_assm_no_alt_miss");}
			if($this->wel_part_no_alt==""){throw new Exception("wel_part_no_alt_miss");}
			if($this->wel_alt_part_alt==""){throw new Exception("wel_alt_part_alt_miss");}
			if(strtoupper($this->wel_alt_part_alt)==strtoupper($this->wel_fg_no_alt)){
				throw new Exception("wel_alt_part_same_wel_fg_no");}
			if(strtoupper($this->wel_alt_part_alt)==strtoupper($this->wel_assm_no_alt)){
				throw new Exception("wel_alt_part_same_wel_assm_no");}
			if(strtoupper($this->wel_alt_part_alt)==strtoupper($this->wel_part_no_alt)){
				throw new Exception("wel_alt_part_same_wel_part_no");}
			if(!is_numeric($this->wel_ex_qty_alt)){throw new Exception("wel_ex_qty_error");}
			if($this->wel_ex_qty_alt<=0){throw new Exception("wel_ex_qty_over_rang");}
			
			//产品品号是否存在
			$sql="SELECT * FROM #__wel_partflm WHERE wel_part_no='".$this->wel_fg_no_alt."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_fg_no_alt_not_found");}
			
			//半品品号是否存在
			$sql="SELECT * FROM #__wel_partflm WHERE wel_part_no='".$this->wel_assm_no_alt."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_assm_no_alt_not_found");}
			
			//物料品号是否存在
			$sql="SELECT * FROM #__wel_partflm WHERE wel_part_no='".$this->wel_part_no_alt."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_part_no_alt_not_found");}
			
			//代用品号是否存在
			$sql="SELECT * FROM #__wel_partflm WHERE wel_part_no='".$this->wel_alt_part_alt."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_alt_part_alt_not_found");}
			
			//物料清单记录是否存在
			$sql="SELECT * FROM #__wel_engbomm ".
				"WHERE wel_assm_no='".$this->wel_assm_no_alt."' AND wel_part_no= '".$this->wel_part_no_alt."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_bom_detail_not_found");}
			
			//物料清单代用物料记录是否存在
			$sql="SELECT * FROM #__wel_altparm WHERE 
				wel_prod_no='".$this->wel_fg_no_alt."' AND 
				wel_assm_no='".$this->wel_assm_no_alt."' AND 
				wel_part_no= '".$this->wel_part_no_alt."' AND 
				wel_alt_part='".$this->wel_alt_part_alt."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(($row=mysql_fetch_array($result))){throw new Exception("wel_alt_part_alt_exist");}
			
			try
			{
				mysql_query('begin');
				
				$sql="INSERT INTO #__wel_altparm(
					wel_prod_no,
					wel_assm_no,
					wel_part_no,
					wel_alt_part,
					wel_ex_qty,
					wel_priority,
					wel_crt_user,
					wel_crt_date) 
					VALUES(
					'".$this->wel_fg_no_alt."',
					'".$this->wel_assm_no_alt."',
					'".$this->wel_part_no_alt."',
					'".$this->wel_alt_part_alt."',
					".ROUND($this->wel_ex_qty_alt,6).",
					".ROUND($this->wel_priority_alt,0).",
					'".$_SESSION["wel_user_id"]."',
					now() )";
				$sql=revert_to_the_available_sql($sql);
				if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}

				mysql_query('commit');
			}
			catch (Exception $e1){
				mysql_query('rollback');
				throw new Exception($e1->getMessage());
			}
		}
		catch (Exception $e){$msg_code=$e->getMessage();}		
		if($msg_code==""){$msg_code="addnew_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	//编辑代用物料
	public function detail_alt_part_edit()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_edit")){throw new Exception("unauthorized_access");}
			
			if($this->wel_fg_no_alt==""){throw new Exception("wel_fg_no_alt_miss");}
			if($this->wel_assm_no_alt==""){throw new Exception("wel_assm_no_alt_miss");}
			if($this->wel_part_no_alt==""){throw new Exception("wel_part_no_alt_miss");}
			if($this->wel_alt_part_alt==""){throw new Exception("wel_alt_part_alt_miss");}
			if(strtoupper($this->wel_alt_part_alt)==strtoupper($this->wel_fg_no_alt)){
				throw new Exception("wel_alt_part_same_wel_fg_no");}
			if(strtoupper($this->wel_alt_part_alt)==strtoupper($this->wel_assm_no_alt)){
				throw new Exception("wel_alt_part_same_wel_assm_no");}
			if(strtoupper($this->wel_alt_part_alt)==strtoupper($this->wel_part_no_alt)){
				throw new Exception("wel_alt_part_same_wel_part_no");}
			if(!is_numeric($this->wel_ex_qty_alt)){throw new Exception("wel_ex_qty_error");}
			if($this->wel_ex_qty_alt<=0){throw new Exception("wel_ex_qty_over_rang");}
			
			//产品品号是否存在
			$sql="SELECT * FROM #__wel_partflm WHERE wel_part_no='".$this->wel_fg_no_alt."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_fg_no_alt_not_found");}
			
			//半品品号是否存在
			$sql="SELECT * FROM #__wel_partflm WHERE wel_part_no='".$this->wel_assm_no_alt."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_assm_no_alt_not_found");}
			
			//物料品号是否存在
			$sql="SELECT * FROM #__wel_partflm WHERE wel_part_no='".$this->wel_part_no_alt."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_part_no_alt_not_found");}
			
			//代用品号是否存在
			$sql="SELECT * FROM #__wel_partflm WHERE wel_part_no='".$this->wel_alt_part_alt."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_alt_part_alt_not_found");}
			
			//物料清单记录是否存在
			$sql="SELECT * FROM #__wel_engbomm ".
				"WHERE wel_assm_no='".$this->wel_assm_no_alt."' AND wel_part_no= '".$this->wel_part_no_alt."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_bom_detail_not_found");}
			
			//物料清单代用物料记录是否存在
			$sql="SELECT * FROM #__wel_altparm WHERE 
				wel_prod_no='".$this->wel_fg_no_alt."' AND 
				wel_assm_no='".$this->wel_assm_no_alt."' AND 
				wel_part_no= '".$this->wel_part_no_alt."' AND 
				wel_alt_part='".$this->wel_alt_part_alt."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_alt_part_alt_not_found");}
			
			try
			{
				mysql_query('begin');

				$sql="UPDATE #__wel_altparm SET 
					wel_ex_qty=".ROUND($this->wel_ex_qty_alt,6).",
					wel_priority=".ROUND($this->wel_priority_alt,0).",
					wel_upd_user='".$_SESSION["wel_user_id"]."', 
					wel_upd_date=now() 
					WHERE wel_prod_no='".$this->wel_fg_no_alt."' 
					AND wel_assm_no='".$this->wel_assm_no_alt."' 
					AND wel_part_no= '".$this->wel_part_no_alt."' 
					AND wel_alt_part='".$this->wel_alt_part_alt."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}

				mysql_query('commit');
			}
			catch (Exception $e1){
				mysql_query('rollback');
				throw new Exception($e1->getMessage());
			}
		}
		catch (Exception $e){$msg_code=$e->getMessage();}		
		if($msg_code==""){$msg_code="edit_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	//删除代用物料
	public function detail_alt_part_del()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_delete")){throw new Exception("unauthorized_access");}
			
			if($this->wel_fg_no_alt==""){throw new Exception("wel_fg_no_alt_miss");}
			if($this->wel_assm_no_alt==""){throw new Exception("wel_assm_no_alt_miss");}
			if($this->wel_part_no_alt==""){throw new Exception("wel_part_no_alt_miss");}
			if($this->wel_alt_part_alt==""){throw new Exception("wel_alt_part_alt_miss");}
			
			//物料清单代用物料记录是否存在
			$sql="SELECT * FROM #__wel_altparm WHERE 
				wel_prod_no='".$this->wel_fg_no_alt."' AND 
				wel_assm_no='".$this->wel_assm_no_alt."' AND 
				wel_part_no= '".$this->wel_part_no_alt."' AND 
				wel_alt_part='".$this->wel_alt_part_alt."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_alt_part_alt_not_found");}
			
			try
			{
				mysql_query('begin');
				
				$sql="DELETE FROM #__wel_altparm 
					WHERE wel_prod_no='".$this->wel_fg_no_alt."' 
					AND wel_assm_no='".$this->wel_assm_no_alt."' 
					AND wel_part_no= '".$this->wel_part_no_alt."' 
					AND wel_alt_part='".$this->wel_alt_part_alt."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}

				mysql_query('commit');
			}
			catch (Exception $e1){
				mysql_query('rollback');
				throw new Exception($e1->getMessage());
			}
		}
		catch (Exception $e){$msg_code=$e->getMessage();}		
		if($msg_code==""){$msg_code="delete_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	//Copy BOM
	public function detail_bom_copy()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_addnew")){throw new Exception("unauthorized_access");}
			
			if($this->wel_assm_no_to==""){throw new Exception("wel_assm_no_to_miss");}
			if($this->wel_assm_no_from==""){throw new Exception("wel_assm_no_from_miss");}
			if(strtoupper($this->wel_assm_no_to)==strtoupper($this->wel_assm_no_from)){
				throw new Exception("wel_assm_no_to_same_wel_assm_no_from");
			}
			
			//from
			$sql="SELECT * FROM #__wel_partflm WHERE wel_part_no='".$this->wel_assm_no_from."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_assm_no_from_not_found");}
			
			//to
			$sql="SELECT * FROM #__wel_partflm WHERE wel_part_no='".$this->wel_assm_no_to."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_assm_no_to_not_found");}
			
			//from BOM是否存在
			$sql="SELECT * FROM #__wel_engbomm WHERE wel_assm_no='".$this->wel_assm_no_from."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_assm_no_from_bom_not_found");}
			
			//to BOM是否存在
			$sql="SELECT * FROM #__wel_engbomm WHERE wel_assm_no='".$this->wel_assm_no_to."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(($row=mysql_fetch_array($result))){throw new Exception("wel_assm_no_to_bom_exist");}
			
			//to BOM是否循环
			$sql="SELECT * FROM #__wel_engbomm WHERE wel_assm_no='".$this->wel_assm_no_from."'";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			while($row=mysql_fetch_array($result)){
				$wel_part_no=is_null($row["wel_part_no"]) ? "" : $row["wel_part_no"];
				if(strtoupper($this->wel_assm_no_to)==strtoupper($wel_part_no)){
					throw new Exception("wel_bom_loop_error");
				}
				if($this->bom_loop($this->wel_assm_no_to,$wel_part_no)){
					throw new Exception("wel_bom_loop_error");
				}
			}

			try
			{
				mysql_query('begin');
				
				$sql="INSERT INTO #__wel_engbomm(
					wel_assm_no,
					wel_part_no,
					wel_scp_fact,
					wel_eng_unit,
					wel_qp_eng,
					wel_unit_exg,
					wel_qty_per,
					wel_bom_loc,
					wel_bom_rmk,
					wel_featu_yn,
					wel_crt_user,
					wel_crt_date) 
					SELECT 
					'".$this->wel_assm_no_to."',
					wel_part_no,
					wel_scp_fact,
					wel_eng_unit,
					wel_qp_eng,
					wel_unit_exg,
					wel_qty_per,
					wel_bom_loc,
					wel_bom_rmk,
					wel_featu_yn,
					'".$_SESSION["wel_user_id"]."',
					now() 
					FROM #__wel_engbomm WHERE wel_assm_no='".$this->wel_assm_no_from."'";
				$sql=revert_to_the_available_sql($sql);
				if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}

				//复制代用物料
				if($this->wel_alt_copy==1){
					//删除所有wel_fg_no和wel_assm_no_to下的代用物料
					$sql="SELECT * FROM #__wel_altparm WHERE wel_prod_no='".$this->wel_assm_no_from."'";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
					while($row=mysql_fetch_array($result)){
						$sql="SELECT * FROM #__wel_altparm WHERE 
							wel_prod_no='".$this->wel_fg_no."' AND 
							wel_assm_no='".$row["wel_assm_no"]."' AND 
							wel_part_no='".$row["wel_part_no"]."' AND 
							wel_alt_part='".$row["wel_alt_part"]."' ";
						$sql=revert_to_the_available_sql($sql);
						if(!($altparm_res=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
						if(($row_res=mysql_fetch_array($altparm_res))){continue;}	//代用物料已存在
						$sql="INSERT INTO #__wel_altparm(
							wel_prod_no,
							wel_assm_no,
							wel_part_no,
							wel_alt_part,
							wel_ex_qty,
							wel_priority,
							wel_crt_user,
							wel_crt_date) 
							VALUES( 
							'".$this->wel_fg_no."',
							'".$row["wel_assm_no"]."',
							'".$row["wel_part_no"]."',
							'".$row["wel_alt_part"]."',
							'".$row["wel_ex_qty"]."',
							'".$row["wel_priority"]."',
							'".$_SESSION["wel_user_id"]."',
							now() )";
						$sql=revert_to_the_available_sql($sql);
						if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					}
				}
				
				mysql_query('commit');
			}
			catch (Exception $e1){
				mysql_query('rollback');
				throw new Exception($e1->getMessage());
			}
		}
		catch (Exception $e){$msg_code=$e->getMessage();}
		if($msg_code==""){$msg_code="copy_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	//检查子件是否会产生循环
	private function bom_loop($wel_assm_no,$wel_part_no)
	{
		try 
		{
			$conn=werp_db_connect();
			
			$sql="SELECT wel_assm_no FROM #__wel_engbomm WHERE wel_part_no='".$wel_assm_no."'";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			while($row=mysql_fetch_array($result)){
				$wel_assm_no=is_null($row["wel_assm_no"]) ? "" : $row["wel_assm_no"];
				if(strtoupper($wel_assm_no)==strtoupper($wel_part_no)){return true;}
				if($this->bom_loop($wel_assm_no,$wel_part_no)){return true;}
			}
		}
		catch (Exception $e){return true;}
		return false;
	}
	
	private function check_wel_partflm($wel_part_no){
		$check_found=false;
		if($wel_part_no=="") return true;//如果系空的,唔理它
		try{
			$conn=werp_db_connect();
			
			$sql="select wel_part_no from #__wel_partflm where wel_part_no='$wel_part_no'  limit 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}	//查询sql时出错了
			if(!($row=mysql_fetch_array($result))){throw new Exception("");}	//没有符合条件的记录
			$check_found=true;
		}catch(Exception $e){}
		return $check_found;
	}
}
?>