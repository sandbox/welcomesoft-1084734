<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT

?>
<?php 
class engm012_cls
{
	public $wel_pattern="";
	public $wel_pat_des="";
	public $wel_en_nextno=0;
	
	private $wel_prog_code="engm012";
	//读取工程更改通知模式
	public function read()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_read")){throw new Exception("unauthorized_access");}
			
			$sql="SELECT * FROM #__wel_gentenw WHERE wel_pattern='".$this->wel_pattern."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	//查询sql时出错了
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_pattern_not_found");}	//没有符合条件的记录
			foreach ($row as $key=>$value){$return_val[$key]=$value;}
			throw new Exception("");
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	//新增工程更改通知模式
	public function addnew()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			if($this->wel_pattern==""){throw new Exception("wel_pattern_miss");}
			if((strlen($this->wel_pattern)<2) || (strlen($this->wel_pattern)>4)){throw new Exception("wel_pattern_length_error");}
			if($this->wel_pat_des==""){throw new Exception("wel_pat_des_miss");}
			if($this->wel_en_nextno==""){$this->wel_en_nextno=0;}
			
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_addnew")){throw new Exception("unauthorized_access");}
			
			//工程更改通知模式是否存在
			$sql="SELECT * FROM #__wel_gentenw WHERE wel_pattern='".$this->wel_pattern."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if($row=mysql_fetch_array($result)){throw new Exception("wel_pattern_exist");}
			
			try
			{
				mysql_query('begin');
				//插入记录到wel_gentenw中
				$sql="INSERT INTO #__wel_gentenw(wel_pattern,wel_pat_des,wel_en_nextno)".
							" VALUES('".$this->wel_pattern."','".$this->wel_pat_des."',".$this->wel_en_nextno.")";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				mysql_query('commit');
				throw new Exception("addnew_succee"); 
			}
			catch (Exception $e1)
			{
				mysql_query('rollback');
				$msg_code=$e1->getMessage();
			}
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		$return_val["msg_code"]=$msg_code;
		$return_val["wel_pattern"]=$this->wel_pattern;
		return $return_val;
	}
	//编辑工程更改通知模式
	public function edit()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			if($this->wel_pattern==""){throw new Exception("wel_pattern_miss");}
			if($this->wel_pat_des==""){throw new Exception("wel_pat_des_miss");}
			if($this->wel_en_nextno==""){$this->wel_en_nextno=0;}
			
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_edit")){throw new Exception("unauthorized_access");}
			
			//工程更改通知模式是否存在
			$sql="SELECT * FROM #__wel_gentenw WHERE wel_pattern='".$this->wel_pattern."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_pattern_not_found");}
			if($this->wel_en_nextno<$row["wel_en_nextno"]){throw new Exception("wel_en_nextno_too_small");}
			
			try
			{
				mysql_query('begin');
				//更新记录
				$sql="UPDATE #__wel_gentenw SET wel_pat_des='".$this->wel_pat_des."',".
					"wel_en_nextno=".$this->wel_en_nextno." WHERE wel_pattern='".$this->wel_pattern."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				mysql_query('commit');
				throw new Exception("edit_succee");
			}
			catch (Exception $e1)
			{
				mysql_query('rollback');
				$msg_code=$e1->getMessage();
			}
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}		
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	//删除工程更改通知模式
	public function delete()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			if($this->wel_pattern==""){throw new Exception("wel_pattern_miss");}
			
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_delete")){throw new Exception("unauthorized_access");}
			
			//工程更改通知模式是否存在
			$sql="SELECT * FROM #__wel_gentenw WHERE wel_pattern='".$this->wel_pattern."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_pattern_not_found");}
			
			try
			{
				mysql_query('begin');
				//删除记录
				$sql="DELETE FROM #__wel_gentenw WHERE wel_pattern='".$this->wel_pattern."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				mysql_query('commit');
				throw new Exception("delete_succee"); 
			}
			catch (Exception $e1)
			{
				mysql_query('rollback');
				$msg_code=$e1->getMessage();
			}
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}		
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
}
?>