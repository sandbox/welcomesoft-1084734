<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>

<?php 
class engm016_cls
{
	public $wel_ecn_no="";
	public $wel_bom_rev="";
	
	private $wel_prog_code="engm016";
	
	//读取单位档案
	public function read()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_read")){throw new Exception("unauthorized_access");}
			
			$sql="SELECT * FROM #__wel_ecnhdrm WHERE wel_ecn_no='".$this->wel_ecn_no."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	//查询sql时出错了
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_ecn_no_not_found");}	//没有符合条件的记录
			foreach ($row as $key=>$value){$return_val[$key]=$value;}
			throw new Exception("");
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	//过账
	public function post()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_addnew")){throw new Exception("unauthorized_access");}
			
			if($this->wel_ecn_no==""){throw new Exception("wel_ecn_no_miss");}
			
			//工程更改档案是否存在
			$sql="SELECT * FROM #__wel_ecnhdrm WHERE wel_ecn_no='".$this->wel_ecn_no."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_ecn_no_not_found");}
			if($row["wel_appr_yn"]!=1){throw new Exception("wel_ecn_no_not_approved");}
			if($row["wel_post_yn"]==1){throw new Exception("wel_ecn_no_had_posted");}
			
			$a_few_wel_ecn_act_error=false;
			//根据工程更改通知明细的操作状态执行相应的操作
			$sql="SELECT * FROM #__wel_engbomh WHERE wel_ecn_no='".$this->wel_ecn_no."'";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			while($row=mysql_fetch_array($result))
			{
				$wel_assm_no=is_null($row["wel_assm_no"]) ? "" : $row["wel_assm_no"];
				$wel_part_no=is_null($row["wel_part_no"]) ? "" : $row["wel_part_no"];
				$wel_scp_fact=floatval(is_null($row["wel_scp_fact"]) ? 0 : $row["wel_scp_fact"]);
				$wel_eng_unit=is_null($row["wel_eng_unit"]) ? "" : $row["wel_eng_unit"];
				$wel_qp_eng=floatval(is_null($row["wel_qp_eng"]) ? 1 : $row["wel_qp_eng"]);
				$wel_unit_exg=floatval(is_null($row["wel_unit_exg"]) ? 1 : $row["wel_unit_exg"]);
				$wel_qty_per=floatval(is_null($row["wel_qty_per"]) ? 1 : $row["wel_qty_per"]);
				$wel_ecn_loc=is_null($row["wel_ecn_loc"]) ? "" : $row["wel_ecn_loc"];
				$wel_ecn_rmk=is_null($row["wel_ecn_rmk"]) ? "" : $row["wel_ecn_rmk"];
				$wel_ecn_act=is_null($row["wel_ecn_act"]) ? "" : $row["wel_ecn_act"];
					
				try
				{
					mysql_query('begin');
					
					$sql="SELECT * FROM #__wel_engbomm WHERE wel_assm_no='".$wel_assm_no."' ".
						"AND wel_part_no='".$wel_part_no."' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result_engbomm=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
					if(!$row_engbomm=mysql_fetch_array($result_engbomm)){
						if (strtolower($wel_ecn_act)!="add"){
							throw new Exception("wel_ecn_act_error");
						}
						if ($this->bom_loop($wel_assm_no,$wel_part_no)){
							throw new Exception("wel_bom_loop_error");
						}
						$sql="INSERT INTO #__wel_engbomm(".
							"wel_assm_no,wel_part_no,".
							"wel_scp_fact,wel_eng_unit,wel_qp_eng,".
							"wel_unit_exg,wel_qty_per,".
							"wel_bom_rmk,wel_bom_loc,".
							"wel_crt_user,wel_crt_date) ".
							"VALUES(".
							"'".$wel_assm_no."','".$wel_part_no."',".
							"".$wel_scp_fact.",'".$wel_eng_unit."','".$wel_qp_eng."',".
							"".$wel_unit_exg.",".$wel_qty_per.",".
							"'".$wel_ecn_rmk."','".$wel_ecn_loc."',".
							"'".$_SESSION["wel_user_id"]."',now() )";
						$sql=revert_to_the_available_sql($sql);
						if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}

						//批核新增加的BOM
						$sql="UPDATE #__wel_partflm SET ".
							"wel_bom_yn=1,".
							"wel_bom_date=now(),".
							"wel_bom_by='".$_SESSION["wel_user_id"]."' ".
							"WHERE wel_part_no='".$wel_assm_no."' LIMIT 1";
						$sql=revert_to_the_available_sql($sql);
						if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					}else{
						if (strtolower($wel_ecn_act)=="update"){
							$sql="UPDATE #__wel_engbomm SET ".
								"wel_scp_fact=".$wel_scp_fact.",".
								"wel_eng_unit='".$wel_eng_unit."',".
								"wel_qp_eng=".$wel_qp_eng.",".
								"wel_unit_exg=".$wel_unit_exg.",".
								"wel_qty_per=".$wel_qty_per.",".
								"wel_bom_rmk='".$wel_ecn_rmk."',".
								"wel_bom_loc='".$wel_ecn_loc."', ".
								"wel_upd_user='".$_SESSION["wel_user_id"]."', ".
								"wel_upd_date=now() ".
								"WHERE ".
								"wel_assm_no='".$wel_assm_no."' AND ".
								"wel_part_no='".$wel_part_no."' LIMIT 1";
							$sql=revert_to_the_available_sql($sql);
							if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
						}elseif (strtolower($wel_ecn_act)=="delete"){
							$sql="DELETE FROM #__wel_engbomm WHERE ".
								"wel_assm_no='".$wel_assm_no."' AND ".
								"wel_part_no='".$wel_part_no."' LIMIT 1";
							$sql=revert_to_the_available_sql($sql);
							if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
						}else{
							throw new Exception("wel_ecn_act_error");
						}
					}
					
					if ($this->wel_bom_rev==1){		//更新wel_partflm中的bom版本号
						$sql="UPDATE #__wel_partflm SET ".
							"wel_bom_rev=IFNULL(wel_bom_rev,0)+1 ".
							"WHERE wel_part_no='".$wel_assm_no."' LIMIT 1";
						$sql=revert_to_the_available_sql($sql);
						if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					}
					
					//更新工程更改通知细节的过账状态
					$sql="UPDATE #__wel_engbomh SET ".
						"wel_post_yn=1 ".
						"WHERE wel_ecn_no='".$this->wel_ecn_no."' AND ".
						"wel_assm_no='".$wel_assm_no."' AND wel_part_no='".$wel_part_no."' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					
					mysql_query('commit');
				}
				catch(Exception $e1)
				{
					mysql_query('rollback');
					//throw new Exception($e1->getMessage());
					$a_few_wel_ecn_act_error=true;
				}
			}
			//更新工程更改通知过账状态
			$sql="UPDATE #__wel_ecnhdrm SET ".
				"wel_post_yn=1,".
				"wel_post_by='".$_SESSION["wel_user_id"]."',".
				"wel_post_date=now() ".
				"WHERE wel_ecn_no='".$this->wel_ecn_no."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
			
			if ($a_few_wel_ecn_act_error){throw new Exception("a_few_wel_ecn_act_error");}
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		if($msg_code==""){$msg_code="post_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	//检查子件是否会产生循环
	private function bom_loop($wel_assm_no,$wel_part_no)
	{
		try 
		{
			$conn=werp_db_connect();
			
			$sql="SELECT wel_assm_no FROM #__wel_engbomm WHERE wel_part_no='".$wel_assm_no."'";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			while($row=mysql_fetch_array($result)){
				$wel_assm_no=is_null($row["wel_assm_no"]) ? "" : $row["wel_assm_no"];
				if(strtoupper($wel_assm_no)==strtoupper($wel_part_no)){return true;}
				if($this->bom_loop($wel_assm_no,$wel_part_no)){return true;}
			}
		}
		catch (Exception $e){return true;}
		return false;
	}
}
?>