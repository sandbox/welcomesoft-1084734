<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT

?>
<?php
class mktm001_cls
{
	public $wel_seller_code="";
	public $wel_seller_name="";
	public $wel_cm_rate=0;
	public $wel_area_code="";
	public $wel_e_mail="";

	private $wel_prog_code="mktm001";

	public function read()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_read")){throw new Exception("unauthorized_access");}	
		
			$sql="SELECT #__wel_mkthanm.* FROM #__wel_mkthanm WHERE wel_seller_code='$this->wel_seller_code' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_seller_code_not_found");}	
			$int__count=0;
			while ($int__count<mysql_num_fields($result)) 
			{
				$field_name=mysql_fetch_field($result,$int__count)->name;
				$return_val[$field_name]=$row[$field_name];
				$int__count++;
			}
			mysql_free_result($result);
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	public function addnew()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_addnew")){throw new Exception("unauthorized_access");}	
				
			if ($this->wel_seller_code==""){throw new Exception("wel_seller_code_miss");}
			if ($this->wel_seller_name==""){throw new Exception("wel_seller_name_miss");}					
			if (!is_numeric($this->wel_cm_rate)){$this->wel_cm_rate=0;}
			$this->wel_cm_rate=doubleval($this->wel_cm_rate);
			
			$sql="SELECT * FROM #__wel_mkthanm WHERE wel_seller_code='$this->wel_seller_code' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}	
			if($row=mysql_fetch_array($result)){throw new Exception("wel_seller_code_exist");}
			
			if($this->wel_area_code!="")
			{
				$sql="SELECT * FROM #__wel_areaflm WHERE wel_area_code='$this->wel_area_code' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_area_code_not_found");}	
			}

			try
			{
				mysql_query("begin");
				
					$sql="INSERT INTO #__wel_mkthanm(wel_seller_code,wel_seller_name,wel_cm_rate,wel_area_code,wel_e_mail) ".
						"VALUES('$this->wel_seller_code','$this->wel_seller_name',".
							"'$this->wel_cm_rate','$this->wel_area_code','$this->wel_e_mail')";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					
				mysql_query("commit");
					
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}
			
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		if($msg_code==""){$msg_code="addnew_succee";}
		$return_val["msg_code"]=$msg_code;
		$return_val["wel_seller_code"]=$this->wel_seller_code;
		return $return_val;
	}
	
	public function edit()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{			
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_edit")){throw new Exception("unauthorized_access");}
		
			if ($this->wel_seller_code==""){throw new Exception("wel_seller_code_miss");}
			if ($this->wel_seller_name==""){throw new Exception("wel_seller_name_miss");}					
			if (!is_numeric($this->wel_cm_rate)){$this->wel_cm_rate=0;}
			$this->wel_cm_rate=doubleval($this->wel_cm_rate);
			
			$sql="SELECT * FROM #__wel_mkthanm WHERE wel_seller_code='$this->wel_seller_code' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_seller_code_not_found");}	
			
			if($this->wel_area_code!="")
			{
				$sql="SELECT * FROM #__wel_areaflm WHERE wel_area_code='$this->wel_area_code' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_area_code_not_found");}	
			}

			try
			{
				mysql_query("begin");
				
					$sql="UPDATE  #__wel_mkthanm SET ".
							"wel_seller_name='$this->wel_seller_name',".
							"wel_cm_rate='$this->wel_cm_rate',".
							"wel_area_code='$this->wel_area_code',".
							"wel_e_mail='$this->wel_e_mail' ".
						"WHERE wel_seller_code='$this->wel_seller_code' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					
				mysql_query("commit");
					
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}
				
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}		
		if($msg_code==""){$msg_code="edit_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	public function delete()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_delete")){throw new Exception("unauthorized_access");}
			
			if ($this->wel_seller_code==""){throw new Exception("wel_seller_code_miss");}
			
			$sql="SELECT * FROM #__wel_mkthanm WHERE wel_seller_code='$this->wel_seller_code' limit 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_seller_code_not_found");}
			
			try
			{
				mysql_query("begin");
				
					$sql="DELETE FROM #__wel_mkthanm WHERE wel_seller_code='$this->wel_seller_code' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
						
				mysql_query("commit");
					
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}		
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}	
		if($msg_code==""){$msg_code="delete_succee";}	
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
}
?>
