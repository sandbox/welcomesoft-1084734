<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT

?>
<?php
class mktm002_cls
{
	public $wel_cus_code="";
	public $wel_cus_des="";
	public $wel_cus_des1="";
	public $wel_abbre_des="";
	public $wel_cus_add1="";
	public $wel_cus_add2="";
	public $wel_cus_add3="";
	public $wel_cus_add4="";
	public $wel_post_no="";
	public $wel_cus_tele="";
	public $wel_cus_fax="";
	public $wel_cus_email="";
	public $wel_web_site="";
	public $wel_seller_code="";
	public $wel_area_code="";
	
	public $wel_cur_code="";
	public $wel_ex_rate=1;
	public $wel_cus_term="";
	public $wel_ams_yn="";
	public $wel_cr_day=0;
//	public $wel_os_inv=0;
	public $wel_cr_hold="";
	public $wel_cr_lmt=0;
	public $wel_cus_pbase="";
	public $wel_cus_svia="";
	
	public $wel_principal="";
	public $wel_sale_no="";
	public $wel_tax_no="";
	public $wel_tax_type="";
	public $wel_cus_bank="";
	public $wel_cus_bank_acc="";
	
	public $wel_cus_remark="";

	public $wel_cont_line=0;
	public $wel_dely_line=0;
	
	private $wel_prog_code="mktm002";

	public function read()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_read")){throw new Exception("unauthorized_access");}
			
			$sql="SELECT * FROM #__wel_cusmasm WHERE wel_cus_code='$this->wel_cus_code' LIMIT 1";

			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_cus_code_not_found");}
			$int__count=0;
			while ($int__count < mysql_num_fields($result)) 
			{
				$field_name=mysql_fetch_field($result,$int__count)->name;
				$return_val[$field_name]=$row[$field_name];
				$int__count++;
			}
			mysql_free_result($result);
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	public function addnew()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_addnew")){throw new Exception("unauthorized_access");}
				
			if ($this->wel_cus_code==""){throw new Exception("wel_cus_code_miss");}
			if ($this->wel_cus_des==""){throw new Exception("wel_cus_des_miss");}			
			
			if (!is_numeric($this->wel_ex_rate)){$this->wel_ex_rate=1;}
			$this->wel_ex_rate=doubleval($this->wel_ex_rate);
			if (!is_numeric($this->wel_cr_lmt)){$this->wel_cr_lmt=0;}
			$this->wel_cr_lmt=doubleval($this->wel_cr_lmt);
			if (!is_numeric($this->wel_cr_day)){$this->wel_cr_day=0;}
			$this->wel_cr_day=doubleval($this->wel_cr_day);
//			if (!is_numeric($this->wel_os_inv)){$this->wel_os_inv=0;}
//			$this->wel_os_inv=doubleval($this->wel_os_inv);

//			if(! check_wel_mkthanm($conn,$this->wel_seller_code)){throw new Exception("wel_seller_code_not_found");}
			if ($this->wel_seller_code!="")
			{
				$sql="SELECT wel_seller_code FROM #__wel_mkthanm WHERE wel_seller_code='$this->wel_seller_code' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_seller_code_not_found");}
			}
//			if(! check_wel_areaflm($conn,$this->wel_area_code)){throw new Exception("wel_area_code_not_found");}
			if ($this->wel_area_code!="")
			{
				$sql="SELECT wel_area_code FROM #__wel_areaflm WHERE wel_area_code='$this->wel_area_code' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_area_code_not_found");}
			}
//			if(! check_wel_currenm($conn,$this->wel_cur_code)){throw new Exception("wel_cur_code_not_found");}
			if ($this->wel_cur_code!="")
			{
				$sql="SELECT wel_cur_code FROM #__wel_currenm WHERE wel_cur_code='$this->wel_cur_code' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_cur_code_not_found");}
			}
//			if(! check_wel_paytflm($conn,$this->wel_cus_term)){throw new Exception("wel_cus_term_not_found");}
			if ($this->wel_cus_term!="")
			{
				$sql="SELECT wel_pay_type FROM #__wel_paytflm WHERE wel_pay_type='$this->wel_cus_term' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_cus_term_not_found");}
			}
//			if(! check_wel_pbasefm($conn,$this->wel_cus_pbase)){throw new Exception("wel_cus_pbase_not_found");}			
			if ($this->wel_cus_pbase!="")
			{
				$sql="SELECT wel_pbase_code FROM #__wel_pbasefm WHERE wel_pbase_code='$this->wel_cus_pbase' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_cus_pbase_not_found");}
			}
//			if(! check_wel_shpviam($conn,$this->wel_cus_svia)){throw new Exception("wel_cus_svia_not_found");}
			if ($this->wel_cus_svia!="")
			{
				$sql="SELECT wel_svia_code FROM #__wel_shpviam WHERE wel_svia_code='$this->wel_cus_svia' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_cus_pbase_not_found");}
			}
				
			$sql="SELECT * FROM #__wel_cusmasm WHERE wel_cus_code='$this->wel_cus_code' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
			if($row=mysql_fetch_array($result)){throw new Exception("wel_cus_code_exist");}
			
			try
			{
				mysql_query("begin");
					
					$sql="INSERT INTO #__wel_cusmasm SET ".
						"wel_cus_code='$this->wel_cus_code',".
						"wel_cus_des='$this->wel_cus_des',".
						"wel_cus_des1='$this->wel_cus_des1',".
						"wel_abbre_des='$this->wel_abbre_des',".
						"wel_cus_add1='$this->wel_cus_add1',".
						"wel_cus_add2='$this->wel_cus_add2',".
						"wel_cus_add3='$this->wel_cus_add3',".
						"wel_cus_add4='$this->wel_cus_add4',".
						"wel_post_no='$this->wel_post_no',".
						"wel_cus_tele='$this->wel_cus_tele',".
						"wel_cus_fax='$this->wel_cus_fax',".
						"wel_cus_email='$this->wel_cus_email',".
						"wel_web_site='$this->wel_web_site',".
						"wel_seller_code='$this->wel_seller_code',".
						"wel_area_code='$this->wel_area_code',".
						
//						"wel_type_code='$this->wel_type_code',".
//						"wel_sale_type='$this->wel_sale_type',".
						"wel_cur_code='$this->wel_cur_code',".
						"wel_ex_rate='$this->wel_ex_rate',".
						"wel_cus_term='$this->wel_cus_term',".
						"wel_ams_yn='$this->wel_ams_yn',".
						"wel_cr_day='$this->wel_cr_day',".
//						"wel_os_inv='$this->wel_os_inv',".
						"wel_cr_hold='$this->wel_cr_hold',".
						"wel_cr_lmt='$this->wel_cr_lmt',".
						"wel_cus_pbase='$this->wel_cus_pbase',".
						"wel_cus_svia='$this->wel_cus_svia',".

						"wel_principal='$this->wel_principal',".
						"wel_sale_no='$this->wel_sale_no',".
						"wel_tax_no='$this->wel_tax_no',".
						"wel_tax_type='$this->wel_tax_type',".
						"wel_cus_bank='$this->wel_cus_bank',".
						"wel_cus_bank_acc='$this->wel_cus_bank_acc',".
						
						"wel_cus_remark='$this->wel_cus_remark',".
						"wel_crt_user='".$_SESSION["wel_user_id"]."',".
						"wel_crt_date=now()";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					
				mysql_query("commit");
					
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		if($msg_code==""){$msg_code="addnew_succee";}
		$return_val["msg_code"]=$msg_code;
		$return_val["wel_cus_code"]=$this->wel_cus_code;
		return $return_val;
	}
	
	public function edit()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_edit")){throw new Exception("unauthorized_access");}
			
			if ($this->wel_cus_code==""){throw new Exception("wel_cus_code_miss");}
			if ($this->wel_cus_des==""){throw new Exception("wel_cus_des_miss");}			
			
			if (!is_numeric($this->wel_ex_rate)){$this->wel_ex_rate=1;}
			$this->wel_ex_rate=doubleval($this->wel_ex_rate);
			if (!is_numeric($this->wel_cr_lmt)){$this->wel_cr_lmt=0;}
			$this->wel_cr_lmt=doubleval($this->wel_cr_lmt);
			if (!is_numeric($this->wel_cr_day)){$this->wel_cr_day=0;}
			$this->wel_cr_day=doubleval($this->wel_cr_day);
//			if (!is_numeric($this->wel_os_inv)){$this->wel_os_inv=0;}
//			$this->wel_os_inv=doubleval($this->wel_os_inv);

//			if(! check_wel_mkthanm($conn,$this->wel_seller_code)){throw new Exception("wel_seller_code_not_found");}
			if ($this->wel_seller_code!="")
			{
				$sql="SELECT wel_seller_code FROM #__wel_mkthanm WHERE wel_seller_code='$this->wel_seller_code' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_seller_code_not_found");}
			}
//			if(! check_wel_areaflm($conn,$this->wel_area_code)){throw new Exception("wel_area_code_not_found");}
			if ($this->wel_area_code!="")
			{
				$sql="SELECT wel_area_code FROM #__wel_areaflm WHERE wel_area_code='$this->wel_area_code' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_area_code_not_found");}
			}
//			if(! check_wel_currenm($conn,$this->wel_cur_code)){throw new Exception("wel_cur_code_not_found");}
			if ($this->wel_cur_code!="")
			{
				$sql="SELECT wel_cur_code FROM #__wel_currenm WHERE wel_cur_code='$this->wel_cur_code' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_cur_code_not_found");}
			}
//			if(! check_wel_paytflm($conn,$this->wel_cus_term)){throw new Exception("wel_cus_term_not_found");}
			if ($this->wel_cus_term!="")
			{
				$sql="SELECT wel_pay_type FROM #__wel_paytflm WHERE wel_pay_type='$this->wel_cus_term' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_cus_term_not_found");}
			}
//			if(! check_wel_pbasefm($conn,$this->wel_cus_pbase)){throw new Exception("wel_cus_pbase_not_found");}			
			if ($this->wel_cus_pbase!="")
			{
				$sql="SELECT wel_pbase_code FROM #__wel_pbasefm WHERE wel_pbase_code='$this->wel_cus_pbase' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_cus_pbase_not_found");}
			}
//			if(! check_wel_shpviam($conn,$this->wel_cus_svia)){throw new Exception("wel_cus_svia_not_found");}
			if ($this->wel_cus_svia!="")
			{
				$sql="SELECT wel_svia_code FROM #__wel_shpviam WHERE wel_svia_code='$this->wel_cus_svia' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_cus_pbase_not_found");}
			}
				
			$sql="SELECT * FROM #__wel_cusmasm WHERE wel_cus_code='$this->wel_cus_code' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_cus_code_not_found");}
			
			try
			{
				mysql_query("begin");
				
					$sql="UPDATE #__wel_cusmasm SET ".
						"wel_cus_des='$this->wel_cus_des',".
						"wel_cus_des1='$this->wel_cus_des1',".
						"wel_abbre_des='$this->wel_abbre_des',".
						"wel_cus_add1='$this->wel_cus_add1',".
						"wel_cus_add2='$this->wel_cus_add2',".
						"wel_cus_add3='$this->wel_cus_add3',".
						"wel_cus_add4='$this->wel_cus_add4',".
						"wel_post_no='$this->wel_post_no',".
						"wel_cus_tele='$this->wel_cus_tele',".
						"wel_cus_fax='$this->wel_cus_fax',".
						"wel_cus_email='$this->wel_cus_email',".
						"wel_web_site='$this->wel_web_site',".
						"wel_seller_code='$this->wel_seller_code',".
						"wel_area_code='$this->wel_area_code',".
						
//						"wel_type_code='$this->wel_type_code',".
//						"wel_sale_type='$this->wel_sale_type',".
						"wel_cur_code='$this->wel_cur_code',".
						"wel_ex_rate='$this->wel_ex_rate',".
						"wel_cus_term='$this->wel_cus_term',".
						"wel_ams_yn='$this->wel_ams_yn',".
						"wel_cr_day='$this->wel_cr_day',".
//						"wel_os_inv='$this->wel_os_inv',".
						"wel_cr_hold='$this->wel_cr_hold',".
						"wel_cr_lmt='$this->wel_cr_lmt',".
						"wel_cus_pbase='$this->wel_cus_pbase',".
						"wel_cus_svia='$this->wel_cus_svia',".
						
						"wel_principal='$this->wel_principal',".
						"wel_sale_no='$this->wel_sale_no',".
						"wel_tax_no='$this->wel_tax_no',".
						"wel_tax_type='$this->wel_tax_type',".
						"wel_cus_bank='$this->wel_cus_bank',".
						"wel_cus_bank_acc='$this->wel_cus_bank_acc',".
						
						"wel_cus_remark='$this->wel_cus_remark',".
						"wel_upd_user='".$_SESSION["wel_user_id"]."',".
						"wel_upd_date=now() ".
					"WHERE wel_cus_code='$this->wel_cus_code'";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					
				mysql_query("commit");
					
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}
			
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}		
		if($msg_code==""){$msg_code="edit_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	public function delete()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_delete")){throw new Exception("unauthorized_access");}
			
			if ($this->wel_cus_code==""){throw new Exception("wel_cus_code_miss");}
			
			$sql="SELECT * FROM #__wel_cusmasm WHERE wel_cus_code='$this->wel_cus_code' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_cus_code_not_found");}
			
			try
			{
				mysql_query("begin");
				
					$sql="DELETE FROM #__wel_cuscont WHERE wel_cus_code='$this->wel_cus_code'";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					
					$sql="DELETE FROM #__wel_delyflm WHERE wel_cus_code='$this->wel_cus_code'";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					
					$sql="DELETE FROM #__wel_cusmasm WHERE wel_cus_code='$this->wel_cus_code' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}

				mysql_query("commit");
					
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}			
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}		
		if($msg_code==""){$msg_code="delete_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}	
	
	/////////////////detail start///////////////////////
	public function delete_wel_cuscont()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_delete")){throw new Exception("unauthorized_access");}
			
			if ($this->wel_cus_code==""){throw new Exception("wel_cus_code_miss");}
			if ($this->wel_cont_line==""){throw new Exception("wel_cont_line_miss");}
			
			$sql="SELECT * FROM #__wel_cuscont WHERE wel_cus_code='$this->wel_cus_code' AND wel_cont_line='$this->wel_cont_line' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_cus_line_not_found");}
			
			try
			{
				mysql_query("begin");
				
					$sql="DELETE FROM #__wel_cuscont WHERE wel_cus_code='$this->wel_cus_code' AND wel_cont_line='$this->wel_cont_line' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					
				mysql_query("commit");
					
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		if($msg_code==""){$msg_code="delete_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	public function delete_wel_cuscont_all()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_delete")){throw new Exception("unauthorized_access");}
			
			if ($this->wel_cus_code==""){throw new Exception("wel_cus_code_miss");}
			
			$sql="SELECT * FROM #__wel_cuscont WHERE wel_cus_code='$this->wel_cus_code' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_cus_line_not_found");}
			
			try
			{
				mysql_query("begin");
				
					$sql="DELETE FROM #__wel_cuscont WHERE wel_cus_code='$this->wel_cus_code'";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					
				mysql_query("commit");
					
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}
				
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		if($msg_code==""){$msg_code="delete_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	public function delete_wel_delyflm()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_delete")){throw new Exception("unauthorized_access");}
			
			if ($this->wel_cus_code==""){throw new Exception("wel_cus_code_miss");}
			if ($this->wel_dely_line==""){throw new Exception("wel_dely_line_miss");}
			
			$sql="SELECT * FROM #__wel_delyflm WHERE wel_cus_code='$this->wel_cus_code' AND wel_dely_line='$this->wel_dely_line' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_dely_line_not_found");}
			
			try
			{
				mysql_query("begin");
				
					$sql="DELETE FROM #__wel_delyflm WHERE wel_cus_code='$this->wel_cus_code' AND wel_dely_line='$this->wel_dely_line' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					
				mysql_query("commit");
					
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		if($msg_code==""){$msg_code="delete_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	public function delete_wel_delyflm_all()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_delete")){throw new Exception("unauthorized_access");}
			
			$sql="SELECT * FROM #__wel_delyflm WHERE wel_cus_code='$this->wel_cus_code' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_dely_line_not_found");}
			
			try
			{
				mysql_query("begin");
				
					$sql="DELETE FROM #__wel_delyflm WHERE wel_cus_code='$this->wel_cus_code'";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					
				mysql_query("commit");
					
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		if($msg_code==""){$msg_code="delete_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	

	///////////////detail end///////////////////////
}
?>
