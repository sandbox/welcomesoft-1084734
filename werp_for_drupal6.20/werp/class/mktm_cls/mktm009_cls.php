<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>

<?php
class mktm009_cls
{
	public $wel_pattern="";
	public $wel_so_no="";
	public $wel_cus_code="";
	public $wel_cus_ord="";
	public $wel_req_date="";
	public $wel_comp_code="";
	public $wel_cur_code="";
	public $wel_ex_rate=0;
	public $wel_cus_term="";
	public $wel_cus_pbase="";
	public $wel_cus_svia="";
	public $wel_cont_man="";
	public $wel_seller_code="";

	public $wel_dis_type="";
	public $wel_discount=0;
	public $wel_tax_type="";

	public $wel_dely_code="";

	public $wel_remark="";
	public $wel_alrm_date="";
	public $wel_alrm_remark="";

	public $wel_crt_user="";
	public $wel_crt_date="";
	public $wel_upd_user="";
	public $wel_upd_date="";

	public $wel_so_line=0;

	private $wel_prog_code="mktm009";

	public function read()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_read")){throw new Exception("unauthorized_access");}
			
			$sql="SELECT r.*, ".
						"c.wel_cus_des, ".
						"c.wel_cus_des1, ".
						"c.wel_abbre_des, ".
						"c.wel_cus_add1, ".
						"c.wel_cus_add2, ".
						"c.wel_cus_add3, ".
						"c.wel_cus_add4, ".
						"c.wel_cus_email, ".
						"c.wel_cus_tele, ".
						"c.wel_cus_fax, ".
						"b.wel_pay_des as wel_cus_term_des, ".
						"d.wel_pbase_name as wel_cus_pbase_des, ".
						"e.wel_svia_des as wel_cus_svia_des, ".
						"f.wel_dely_des as wel_dely_des, ".
						"f.wel_dely_add1 as wel_dely_add1, ".
						"f.wel_dely_add2 as wel_dely_add2, ".
						"f.wel_dely_add3 as wel_dely_add3, ".
						"f.wel_dely_add4 as wel_dely_add4, ".
						"f.wel_dely_cont as wel_dely_cont, ".
						"f.wel_dely_tel as wel_dely_tel, ".
						"f.wel_dely_fax as wel_dely_fax, ".
						"f.wel_dely_email as wel_dely_email, ".
						"f.wel_dely_mob as wel_dely_mob ".
				"FROM #__wel_sorhdrm r ".
					"LEFT JOIN #__wel_cusmasm c ON r.wel_cus_code=c.wel_cus_code ".
					"LEFT JOIN #__wel_paytflm b ON r.wel_cus_term=b.wel_pay_type ".
					"LEFT JOIN #__wel_pbasefm d ON r.wel_cus_pbase=d.wel_pbase_code ".
					"LEFT JOIN #__wel_shpviam e ON r.wel_cus_svia=e.wel_svia_code ".
					"LEFT JOIN #__wel_delyflm f ON (r.wel_cus_code=f.wel_cus_code and r.wel_dely_code=f.wel_dely_line)".
				"WHERE r.wel_so_no='$this->wel_so_no' LIMIT 1"; 
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_so_no_not_found");}
			$int__count=0;
			while ($int__count < mysql_num_fields($result)) 
			{
				$field_name=mysql_fetch_field($result,$int__count)->name;
				$return_val[$field_name]=$row[$field_name];
				$int__count++;
			}
			mysql_free_result($result);
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	public function read_wel_total_amt()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_read")){throw new Exception("unauthorized_access");}
			
			$sql="SELECT wel_item_amt,(wel_dis_amt+wel_dis_amt2) as wel_dis_amt,wel_tax_amt,wel_order_amt ".
				"FROM #__wel_sorhdrm ".
				"WHERE wel_so_no='".$this->wel_so_no."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_so_no_not_found");}

			$return_val["wel_item_amt"]=doubleval(is_null($row["wel_item_amt"]) ? 0 : $row["wel_item_amt"]);
			$return_val["wel_dis_amt"]=doubleval(is_null($row["wel_dis_amt"]) ? 0 : $row["wel_dis_amt"]);
			$return_val["wel_tax_amt"]=doubleval(is_null($row["wel_tax_amt"]) ? 0 : $row["wel_tax_amt"]);
			$return_val["wel_order_amt"]=doubleval(is_null($row["wel_order_amt"]) ? 0 : $row["wel_order_amt"]);
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	public function addnew()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_addnew")){throw new Exception("unauthorized_access");}

			if ($this->wel_so_no=="" && $this->wel_pattern==""){throw new Exception("wel_so_no_miss");}
			if ($this->wel_comp_code==""){throw new Exception("wel_comp_code_miss");}
			if ($this->wel_cus_code==""){throw new Exception("wel_cus_code_miss");}
			if ($this->wel_cur_code==""){throw new Exception("wel_cur_code_miss");}
			if ($this->wel_req_date==""){throw new Exception("wel_req_date_miss");}
//			if ($this->wel_cus_term==""){throw new Exception("wel_cus_term_miss");}
//			if ($this->wel_seller_code==""){throw new Exception("wel_seller_code_miss");}
//			if ($this->wel_dely_code==""){throw new Exception("wel_dely_code_miss");}
//			if ($this->wel_tax_type==""){throw new Exception("wel_tax_type_miss");}
			
			if (!is_numeric($this->wel_ex_rate)){$this->wel_ex_rate=1;}
			$this->wel_ex_rate=doubleval($this->wel_ex_rate);
			
			if(!is_numeric($this->wel_discount)){$this->wel_discount=0;}
			$this->wel_discount=doubleval($this->wel_discount);
			if($this->wel_discount<0 || $this->wel_discount>100){throw new Exception("wel_discount_error");}

			$this->wel_req_date=(($this->wel_req_date=="") ? "null" : "'".$this->wel_req_date."'");
			$this->wel_alrm_date=(($this->wel_alrm_date=="") ? "null" : "'".$this->wel_alrm_date."'");

			if($this->wel_pattern!="")
			{
				$sql="SELECT wel_pattern from #__wel_gentsow WHERE wel_pattern='".$this->wel_pattern."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_pattern_not_found");}
			}
			if($this->wel_cus_code!="")
			{
				$sql="SELECT wel_cus_code from #__wel_cusmasm WHERE wel_cus_code='".$this->wel_cus_code."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_cus_code_not_found");}
			}
			if($this->wel_comp_code!="")
			{
				$sql="SELECT wel_comp_code from #__wel_compflm WHERE wel_comp_code='".$this->wel_comp_code."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_comp_code_not_found");}
			}
			if($this->wel_cur_code!="")
			{
				$sql="SELECT wel_cur_code,wel_ex_rate from #__wel_currenm WHERE wel_cur_code='".$this->wel_cur_code."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_cur_code_not_found");}
				$this->wel_ex_rate=doubleval($row["wel_ex_rate"]);
			}
			if($this->wel_cus_term!="")
			{
				$sql="SELECT wel_pay_type from #__wel_paytflm WHERE wel_pay_type='".$this->wel_cus_term."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_cus_term_not_found");}
			}
			if($this->wel_cus_pbase!="")
			{
				$sql="SELECT wel_pbase_code from #__wel_pbasefm where wel_pbase_code='".$this->wel_cus_pbase."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_cus_pbase_not_found");}
			}
			if($this->wel_cus_svia!="")
			{
				$sql="SELECT wel_svia_code FROM #__wel_shpviam WHERE wel_svia_code='".$this->wel_cus_svia."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_cus_svia_not_found");}
			}
			if($this->wel_seller_code!="")
			{
				$sql="SELECT wel_seller_code from #__wel_mkthanm where wel_seller_code='".$this->wel_seller_code."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_seller_code_not_found");}
			}
			if($this->wel_dely_code!="")
			{
				$sql="SELECT wel_dely_line from #__wel_delyflm WHERE wel_cus_code='".$this->wel_cus_code."' and wel_dely_line='".$this->wel_dely_code."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_dely_code_not_found");}
			}
//			if(! $this->check_wel_cusmasm($this->wel_cus_code)){throw new Exception("wel_cus_code_not_found");}
//			if(! $this->check_wel_compflm($this->wel_comp_code)){throw new Exception("wel_comp_code_not_found");}
//			if(! $this->check_wel_currenm($this->wel_cur_code)){throw new Exception("wel_cur_code_not_found");}
//			if(! $this->check_wel_paytflm($this->wel_cus_term)){throw new Exception("wel_cus_term_not_found");}
//			if(! $this->check_wel_pbasefm($this->wel_cus_pbase)){throw new Exception("wel_cus_pbase_not_found");}
//			if(! $this->check_wel_shpviam($this->wel_cus_svia)){throw new Exception("wel_cus_svia_not_found");}
//			if(! $this->check_wel_mkthanm($this->wel_seller_code)){throw new Exception("wel_seller_code_not_found");}
//			if(! $this->check_wel_delyflm($this->wel_dely_code)){throw new Exception("wel_dely_code_not_found");}
//			if(! $this->check_wel_cuscont($this->wel_cus_code,$this->wel_cont_man)){throw new Exception("wel_cont_man_not_found");}
			
			try
			{
				mysql_query("begin");
					
				if ($this->wel_pattern!="")
				{
					$sql="SELECT * FROM #__wel_gentsow WHERE wel_pattern='$this->wel_pattern' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
					if(!($row=mysql_fetch_array($result))){throw new Exception("wel_pattern_not_found");}
					
					$wel_pattern=stripslashes(trim($row["wel_pattern"]));
					$wel_so_nextno=trim($row["wel_so_nextno"]);
					$wel_so_nextno=sprintf("%'08s",$wel_so_nextno);

					if(strlen($wel_so_nextno)>8){throw new Exception("wel_pattern_overflow");}
					$this->wel_so_no=$wel_pattern.$wel_so_nextno;

					$sql="UPDATE #__wel_gentsow set wel_so_nextno=IFNULL(wel_so_nextno,0)+1 WHERE wel_pattern='$this->wel_pattern'";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				}

				$sql="SELECT wel_so_no from #__wel_sorhdrm WHERE wel_so_no='".$this->wel_so_no."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if($row=mysql_fetch_array($result)){throw new Exception("wel_so_no_exist");}
					
				$sql="INSERT INTO #__wel_sorhdrm SET ".
						"wel_so_no='$this->wel_so_no',".
						"wel_pattern='$this->wel_pattern',".
						"wel_cus_ord='$this->wel_cus_ord',".
						"wel_req_date=$this->wel_req_date,".
						"wel_cus_code='$this->wel_cus_code',".
						"wel_cont_man='$this->wel_cont_man',".
						"wel_seller_code='$this->wel_seller_code',".
						"wel_comp_code='$this->wel_comp_code',".
						"wel_cur_code='$this->wel_cur_code',".
						"wel_ex_rate='$this->wel_ex_rate',".
						"wel_cus_term='$this->wel_cus_term',".
						"wel_cus_pbase='$this->wel_cus_pbase',".
						"wel_cus_svia='$this->wel_cus_svia',".
						"wel_dis_type='$this->wel_dis_type',".
						"wel_discount='$this->wel_discount',".
						"wel_tax_type='$this->wel_tax_type',".
						"wel_remark='$this->wel_remark',".
						"wel_alrm_date=$this->wel_alrm_date,".
						"wel_alrm_remark='$this->wel_alrm_remark',".
						"wel_crt_user='{$_SESSION['wel_user_id']}',".
						"wel_crt_date=now(),".
						"wel_appr_yn='0'";	 
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
			
				mysql_query("commit");
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		if($msg_code==""){$msg_code="addnew_succee";}
		$return_val["msg_code"]=$msg_code;
		$return_val["wel_so_no"]=$this->wel_so_no;
		return $return_val;
	}
	
	public function edit()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_edit")){throw new Exception("unauthorized_access");}
			
			if ($this->wel_comp_code==""){throw new Exception("wel_comp_code_miss");}
			if ($this->wel_cus_code==""){throw new Exception("wel_cus_code_miss");}
			if ($this->wel_cur_code==""){throw new Exception("wel_cur_code_miss");}
			if ($this->wel_req_date==""){throw new Exception("wel_req_date_miss");}
//			if ($this->wel_cus_term==""){throw new Exception("wel_cus_term_miss");}
//			if ($this->wel_seller_code==""){throw new Exception("wel_seller_code_miss");}
//			if ($this->wel_dely_code==""){throw new Exception("wel_dely_code_miss");}
//			if ($this->wel_tax_type==""){throw new Exception("wel_tax_type_miss");}
			
			if (!is_numeric($this->wel_ex_rate)){$this->wel_ex_rate=1;}
			$this->wel_ex_rate=doubleval($this->wel_ex_rate);
			
			if(!is_numeric($this->wel_discount)){$this->wel_discount=0;}
			$this->wel_discount=doubleval($this->wel_discount);
			if($this->wel_discount<0 || $this->wel_discount>100){throw new Exception("wel_discount_error");}
		
			$this->wel_req_date=(($this->wel_req_date=="") ? "null" : "'".$this->wel_req_date."'");
			$this->wel_alrm_date=(($this->wel_alrm_date=="") ? "null" : "'".$this->wel_alrm_date."'");

			if($this->wel_cus_code!="")
			{
				$sql="SELECT wel_cus_code from #__wel_cusmasm WHERE wel_cus_code='".$this->wel_cus_code."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_cus_code_not_found");}
			}
			if($this->wel_comp_code!="")
			{
				$sql="SELECT wel_comp_code from #__wel_compflm WHERE wel_comp_code='".$this->wel_comp_code."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_comp_code_not_found");}
			}
			if($this->wel_cur_code!="")
			{
				$sql="SELECT wel_cur_code,wel_ex_rate from #__wel_currenm WHERE wel_cur_code='".$this->wel_cur_code."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_cur_code_not_found");}
				$this->wel_ex_rate=doubleval($row["wel_ex_rate"]);
			}
			if($this->wel_cus_term!="")
			{
				$sql="SELECT wel_pay_type from #__wel_paytflm WHERE wel_pay_type='".$this->wel_cus_term."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_cus_term_not_found");}
			}
			if($this->wel_cus_pbase!="")
			{
				$sql="SELECT wel_pbase_code from #__wel_pbasefm where wel_pbase_code='".$this->wel_cus_pbase."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_cus_pbase_not_found");}
			}
			if($this->wel_cus_svia!="")
			{
				$sql="SELECT wel_svia_code FROM #__wel_shpviam WHERE wel_svia_code='".$this->wel_cus_svia."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_cus_svia_not_found");}
			}
			if($this->wel_seller_code!="")
			{
				$sql="SELECT wel_seller_code from #__wel_mkthanm where wel_seller_code='".$this->wel_seller_code."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_seller_code_not_found");}
			}
			if($this->wel_dely_code!="")
			{
				$sql="SELECT wel_dely_line from #__wel_delyflm WHERE wel_cus_code='".$this->wel_cus_code."' and wel_dely_line='".$this->wel_dely_code."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_dely_code_not_found");}
			}
//			if(! $this->check_wel_cusmasm($this->wel_cus_code)){throw new Exception("wel_cus_code_not_found");}
//			if(! $this->check_wel_compflm($this->wel_comp_code)){throw new Exception("wel_comp_code_not_found");}
//			if(! $this->check_wel_currenm($this->wel_cur_code)){throw new Exception("wel_cur_code_not_found");}
//			if(! $this->check_wel_paytflm($this->wel_cus_term)){throw new Exception("wel_cus_term_not_found");}
//			if(! $this->check_wel_pbasefm($this->wel_cus_pbase)){throw new Exception("wel_cus_pbase_not_found");}
//			if(! $this->check_wel_mkthanm($this->wel_seller_code)){throw new Exception("wel_seller_code_not_found");}
//			if(! $this->check_wel_delyflm($this->wel_dely_code)){throw new Exception("wel_dely_code_not_found");}
//			if(! $this->check_wel_shpviam($this->wel_cus_svia)){throw new Exception("wel_cus_svia_not_found");}
//			if(! $this->check_wel_cuscont($this->wel_cus_code,$this->wel_cont_man)){throw new Exception("wel_cont_man_not_found");}
			
			//找出订单信息
			$sql="SELECT IFNULL(wel_appr_yn,0) AS wel_appr_yn,wel_cur_code,wel_cus_term,wel_cus_pbase FROM ".
				"#__wel_sorhdrm WHERE wel_so_no='$this->wel_so_no' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_so_no_not_found");}
			$wel_appr_yn=intval(is_null($row["wel_appr_yn"]) ? 0 : $row["wel_appr_yn"]);
			$wel_cur_code=is_null($row["wel_cur_code"]) ? "" : $row["wel_cur_code"];
			$wel_cus_term=is_null($row["wel_cus_term"]) ? "" : $row["wel_cus_term"];
			$wel_cus_pbase=is_null($row["wel_cus_pbase"]) ? "" : $row["wel_cus_pbase"];
			if($wel_appr_yn==1){throw new Exception("wel_so_no_appr");}
			
			//查找发票表明细是否已写入
			if(strtolower($wel_cur_code)!=strtolower($this->wel_cur_code))
			{
				$sql="SELECT wel_so_no FROM #__wel_invdetm WHERE wel_so_no='$this->wel_so_no' limit 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if($row=mysql_fetch_array($result)){throw new Exception("wel_cur_code_change");}
			}

			try
			{
				mysql_query("begin");

				$sql="UPDATE  #__wel_sorhdrm SET ".
						"wel_cus_ord='$this->wel_cus_ord',".
						"wel_req_date=$this->wel_req_date,".
						"wel_cur_code='$this->wel_cur_code',".
						"wel_ex_rate='$this->wel_ex_rate',".
						"wel_cus_term='$this->wel_cus_term',".
						"wel_cus_pbase='$this->wel_cus_pbase',".
						"wel_cus_svia='$this->wel_cus_svia',".
						"wel_cont_man='$this->wel_cont_man',".
						"wel_seller_code='$this->wel_seller_code',".
						"wel_dis_type='$this->wel_dis_type',".
						"wel_discount='$this->wel_discount',".
						"wel_tax_type='$this->wel_tax_type',".
						"wel_dely_code='$this->wel_dely_code',".			
						"wel_remark='$this->wel_remark',".
						"wel_alrm_date=$this->wel_alrm_date,".
						"wel_alrm_remark='$this->wel_alrm_remark',".
						"wel_upd_user='{$_SESSION['wel_user_id']}',".
						"wel_upd_date=now() ".
						"WHERE wel_so_no='$this->wel_so_no' LIMIT 1";				
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}

					// Update sordetm
					if ($this->wel_dis_type==0 or $this->wel_dis_type==2 or $this->wel_dis_type==5)
					{
						$sql="UPDATE #__wel_sordetm SET ".
							"wel_dis_amt2 = 0 ".
							"WHERE wel_so_no='$this->wel_so_no'";
						$sql=revert_to_the_available_sql($sql);
						if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					}
					if ($this->wel_dis_type==1 or $this->wel_dis_type==3)
					{
						$sql="UPDATE #__wel_sordetm SET wel_dis_amt2 = CASE ".
							"WHEN wel_discount > 0 THEN 0 ".
							"ELSE ROUND((wel_item_amt-wel_dis_amt)*'$this->wel_discount'/100,2) END ".
							"WHERE wel_so_no='$this->wel_so_no'";
						$sql=revert_to_the_available_sql($sql);
						if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					}
					if ($this->wel_dis_type==4 or $this->wel_dis_type==6 )
					{
						$sql="UPDATE #__wel_sordetm SET ".
							"wel_dis_amt2 = ROUND((wel_item_amt-wel_dis_amt)*'$this->wel_discount'/100,2) ".
							"WHERE wel_so_no='$this->wel_so_no'";
						$sql=revert_to_the_available_sql($sql);
						if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					}

					// 價外稅
					if ( $this->wel_tax_type==2 )
					{
						$sql="UPDATE #__wel_sordetm SET ".
							"wel_tax_amt = ROUND((wel_item_amt-wel_dis_amt-wel_dis_amt2)*wel_tax_rate/100,2), ".
							"wel_line_amt = ROUND((wel_item_amt-wel_dis_amt-wel_dis_amt2)*(1+wel_tax_rate/100),2) ".
							"WHERE wel_so_no='$this->wel_so_no'";
						$sql=revert_to_the_available_sql($sql);
						if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					}
					// 價內稅
					if ( $this->wel_tax_type==1 )
					{
						$sql="UPDATE #__wel_sordetm SET ".
							"wel_tax_amt = ROUND((wel_item_amt-wel_dis_amt-wel_dis_amt2)*wel_tax_rate/(100+wel_tax_rate),2), ".
							"wel_line_amt = ROUND((wel_item_amt-wel_dis_amt-wel_dis_amt2),2) ".
							"WHERE wel_so_no='$this->wel_so_no'";
						$sql=revert_to_the_available_sql($sql);
						if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					}
					if ( $this->wel_tax_type==0 )
					{
						$sql="UPDATE #__wel_sordetm SET ".
							"wel_tax_amt = 0, ".
							"wel_line_amt = ROUND((wel_item_amt-wel_dis_amt-wel_dis_amt2),2) ".
							"WHERE wel_so_no='$this->wel_so_no'";
						$sql=revert_to_the_available_sql($sql);
						if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					}

					// P/O MISC.
					if ($this->wel_dis_type==0 or $this->wel_dis_type==1 or $this->wel_dis_type==4)
					{
						$sql="UPDATE #__wel_sormism SET ".
							"wel_dis_amt2 = 0 ".
							"WHERE wel_so_no='$this->wel_so_no'";
						$sql=revert_to_the_available_sql($sql);
						if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					}
					if ($this->wel_dis_type==2 or $this->wel_dis_type==3)
					{
						$sql="UPDATE #__wel_sormism SET wel_dis_amt2 = CASE ".
							"WHEN wel_discount > 0 THEN 0 ".
							"ELSE ROUND((wel_item_amt-wel_dis_amt)*'$this->wel_discount'/100,2) END ".
							"WHERE wel_so_no='$this->wel_so_no'";
						$sql=revert_to_the_available_sql($sql);
						if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					}
					if ($this->wel_dis_type==5 or $this->wel_dis_type==6 )
					{
						$sql="UPDATE #__wel_sormism SET ".
							"wel_dis_amt2 = ROUND((wel_item_amt-wel_dis_amt)*'$this->wel_discount'/100,2) ".
							"WHERE wel_so_no='$this->wel_so_no'";
						$sql=revert_to_the_available_sql($sql);
						if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					}
					// 價外稅
					if ( $this->wel_tax_type==2 )
					{
						$sql="UPDATE #__wel_sormism SET ".
							"wel_tax_amt = ROUND((wel_item_amt-wel_dis_amt-wel_dis_amt2)*wel_tax_rate/100,2), ".
							"wel_line_amt = ROUND((wel_item_amt-wel_dis_amt-wel_dis_amt2)*(1+wel_tax_rate/100),2) ".
							"WHERE wel_so_no='$this->wel_so_no'";
						$sql=revert_to_the_available_sql($sql);
						if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					}
					// 價內稅
					if ( $this->wel_tax_type==1 )
					{
						$sql="UPDATE #__wel_sormism SET ".
							"wel_tax_amt = ROUND((wel_item_amt-wel_dis_amt-wel_dis_amt2)*wel_tax_rate/(100+wel_tax_rate),2), ".
							"wel_line_amt = ROUND((wel_item_amt-wel_dis_amt-wel_dis_amt2),2) ".
							"WHERE wel_so_no='$this->wel_so_no'";
						$sql=revert_to_the_available_sql($sql);
						if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					}
					if ( $this->wel_tax_type==0 )
					{
						$sql="UPDATE #__wel_sormism SET ".
							"wel_tax_amt = 0, ".
							"wel_line_amt = ROUND((wel_item_amt-wel_dis_amt-wel_dis_amt2),2) ".
							"WHERE wel_so_no='$this->wel_so_no'";
						$sql=revert_to_the_available_sql($sql);
						if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					}

					// Update SORHDRM discount & Tax
					$tmp_item_amt=0;
					$tmp_dis_amt=0;
					$tmp_dis_amt2=0;
					$tmp_tax_amt=0;
					$tmp_line_amt=0;
					$sql="SELECT ".
						"SUM(wel_item_amt) as s1,".
						"SUM(wel_dis_amt) as s2,".
						"sum(wel_dis_amt2) as s3,".
						"SUM(wel_tax_amt) as s4,".
						"SUM(wel_line_amt) as s5 ".
						"FROM #__wel_sordetm ".
						"WHERE wel_so_no='$this->wel_so_no' ";
					$sql=revert_to_the_available_sql($sql);
					if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
					if( $row=mysql_fetch_array($result) )
					{
						$tmp_item_amt=doubleval(is_null($row["s1"]) ? 0 : $row["s1"]);
						$tmp_dis_amt =doubleval(is_null($row["s2"]) ? 0 : $row["s2"]);
						$tmp_dis_amt2=doubleval(is_null($row["s3"]) ? 0 : $row["s3"]);
						$tmp_tax_amt =doubleval(is_null($row["s4"]) ? 0 : $row["s4"]);
						$tmp_line_amt=doubleval(is_null($row["s5"]) ? 0 : $row["s5"]);
					}

					$sql="SELECT ".
						"SUM(wel_item_amt) as s1,".
						"SUM(wel_dis_amt) as s2,".
						"sum(wel_dis_amt2) as s3,".
						"SUM(wel_tax_amt) as s4,".
						"SUM(wel_line_amt) as s5 ".
						"FROM #__wel_sormism ".
						"WHERE wel_so_no='$this->wel_so_no' ";
					$sql=revert_to_the_available_sql($sql);
					if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
					if( $row=mysql_fetch_array($result) )
					{
						$tmp_item_amt=$tmp_item_amt + doubleval(is_null($row["s1"]) ? 0 : $row["s1"]);
						$tmp_dis_amt =$tmp_dis_amt  + doubleval(is_null($row["s2"]) ? 0 : $row["s2"]);
						$tmp_dis_amt2=$tmp_dis_amt2 + doubleval(is_null($row["s3"]) ? 0 : $row["s3"]);
						$tmp_tax_amt =$tmp_tax_amt  + doubleval(is_null($row["s4"]) ? 0 : $row["s4"]);
						$tmp_line_amt=$tmp_line_amt + doubleval(is_null($row["s5"]) ? 0 : $row["s5"]);
					}

					$sql="UPDATE #__wel_sorhdrm SET ".
						"wel_item_amt='$tmp_item_amt',".
						"wel_dis_amt='$tmp_dis_amt',".
						"wel_dis_amt2='$tmp_dis_amt2',".
						"wel_tax_amt='$tmp_tax_amt',".
						"wel_order_amt='$tmp_line_amt', ".
						"wel_upd_user='{$_SESSION['wel_user_id']}',".
						"wel_upd_date=now() ".
						"WHERE wel_so_no='$this->wel_so_no'";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}

				mysql_query("commit");
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		if($msg_code==""){$msg_code="edit_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	public function delete()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_delete")){throw new Exception("unauthorized_access");}
			
			$sql="SELECT * FROM #__wel_sorhdrm WHERE wel_so_no='$this->wel_so_no' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}	
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_so_no_not_found");}

			//查找制造单明细信息
			$sql="SELECT * FROM #__wel_mordetm WHERE wel_so_no='$this->wel_so_no' limit 1";
			$sql=revert_to_the_available_sql($sql);
			if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}	
			if( $row=mysql_fetch_array($result)){throw new Exception("wel_so_no_in_mo");}

			try
			{
				mysql_query("begin");

				//查找订单明细信息
					$sql="SELECT * FROM #__wel_sordetm WHERE wel_so_no='$this->wel_so_no'";
					$sql=revert_to_the_available_sql($sql);
					if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					while($row=mysql_fetch_assoc($result))
					{
						$dec_req_qty=doubleval(is_null($row["wel_req_qty"]) ? 0 : $row["wel_req_qty"]);
						$dec_os_qty=doubleval(is_null($row["wel_os_qty"]) ? 0 : $row["wel_os_qty"]);
						$dec_dn_qty=doubleval(is_null($row["wel_dn_qty"]) ? 0 : $row["wel_dn_qty"]);
						$dec_do_qty=doubleval(is_null($row["wel_do_qty"]) ? 0 : $row["wel_do_qty"]);
						
						if( ($dec_req_qty-$dec_os_qty)>0 ){throw new Exception("wel_so_qty_less_inv_qty");}
						if( $dec_dn_qty>0 ){throw new Exception("wel_so_qty_less_dn_qty");}
						if( $dec_do_qty>0 ){throw new Exception("wel_so_qty_less_do_qty");}
					}

					$sql="DELETE FROM #__wel_sordetm WHERE wel_so_no='$this->wel_so_no'";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}

					$sql="DELETE FROM #__wel_sormism WHERE wel_so_no='$this->wel_so_no'";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				
					$sql="DELETE FROM #__wel_sorhdrm WHERE wel_so_no='$this->wel_so_no' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				
				mysql_query("commit");
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		if($msg_code==""){$msg_code="delete_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	public function approve()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_approve")){throw new Exception("unauthorized_access");}
			
			$sql="SELECT * FROM #__wel_sorhdrm WHERE wel_so_no='$this->wel_so_no' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}	
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_so_no_not_found");}
			
			try
			{
				mysql_query("begin");
				
					$sql="UPDATE #__wel_sorhdrm SET ".
						 "wel_appr_yn=1,".
						 "wel_appr_by='{$_SESSION['wel_user_id']}',".
						 "wel_appr_date=now() ".
						 "WHERE wel_so_no='$this->wel_so_no' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				
				mysql_query("commit");
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		if($msg_code==""){$msg_code="approve_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	public function not_approve()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_approve")){throw new Exception("unauthorized_access");}
			
			$sql="SELECT * FROM #__wel_sorhdrm WHERE wel_so_no='$this->wel_so_no' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}	
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_so_no_not_found");}
			
			try
			{
				mysql_query("begin");
				
					$sql="UPDATE #__wel_sorhdrm SET ".
							 "wel_appr_yn=0,".
							 "wel_appr_by='{$_SESSION['wel_user_id']}',".
							 "wel_appr_date=now() ".
						 "WHERE wel_so_no='$this->wel_so_no'";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				
				mysql_query("commit");
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}	
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		if($msg_code==""){$msg_code="not_approve_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	public function delete_detail_tab0()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_delete")){throw new Exception("unauthorized_access");}

			if ($this->wel_so_no==""){throw new Exception("wel_so_no_miss");}
			if (!is_numeric($this->wel_so_line)){$this->wel_so_line=0;}
			$this->wel_so_line=intval($this->wel_so_line);
			if($this->wel_so_line==0){throw new Exception("wel_so_line_miss");}
			
			//查找明细是否存在 
			$sql="SELECT wel_req_qty,wel_os_qty,wel_do_qty,wel_dn_qty FROM #__wel_sordetm ".
				"WHERE wel_so_no='$this->wel_so_no' AND wel_so_line='$this->wel_so_line' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_so_line_not_found");}
			$dec_req_qty=doubleval(is_null($row["wel_req_qty"]) ? 0 : $row["wel_req_qty"]);
			$dec_os_qty=doubleval(is_null($row["wel_os_qty"]) ? 0 : $row["wel_os_qty"]);
			$dec_do_qty=doubleval(is_null($row["wel_do_qty"]) ? 0 : $row["wel_do_qty"]);
			$dec_dn_qty=doubleval(is_null($row["wel_dn_qty"]) ? 0 : $row["wel_dn_qty"]);
				
			if( ($dec_req_qty-$dec_os_qty)>0 ){throw new Exception("wel_so_qty_less_inv_qty");}
			if( $dec_dn_qty>0 ){throw new Exception("wel_so_qty_less_dn_qty");}
			if( $dec_do_qty>0 ){throw new Exception("wel_so_qty_less_do_qty");}
						
			//查询MO制造单
			$sql="SELECT wel_so_no FROM #__wel_mordetm ".
				"WHERE wel_so_no='$this->wel_so_no' AND wel_so_line='$this->wel_so_line' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
			if( $row=mysql_fetch_array($result) ){throw new Exception("wel_so_no_in_mo");}
			
			try
			{
				mysql_query("begin");
				
					$sql="DELETE FROM #__wel_sordetm ".
						"WHERE wel_so_no='$this->wel_so_no' AND wel_so_line='$this->wel_so_line' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				
					// Update SORHDRM discount & Tax
					$tmp_item_amt=0;
					$tmp_dis_amt=0;
					$tmp_dis_amt2=0;
					$tmp_tax_amt=0;
					$tmp_line_amt=0;
					$sql="SELECT ".
						"SUM(wel_item_amt) as s1,".
						"SUM(wel_dis_amt) as s2,".
						"sum(wel_dis_amt2) as s3,".
						"SUM(wel_tax_amt) as s4,".
						"SUM(wel_line_amt) as s5 ".
						"FROM #__wel_sordetm ".
						"WHERE wel_so_no='$this->wel_so_no' ";
					$sql=revert_to_the_available_sql($sql);
					if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
					if( $row=mysql_fetch_array($result) )
					{
						$tmp_item_amt=doubleval(is_null($row["s1"]) ? 0 : $row["s1"]);
						$tmp_dis_amt =doubleval(is_null($row["s2"]) ? 0 : $row["s2"]);
						$tmp_dis_amt2=doubleval(is_null($row["s3"]) ? 0 : $row["s3"]);
						$tmp_tax_amt =doubleval(is_null($row["s4"]) ? 0 : $row["s4"]);
						$tmp_line_amt=doubleval(is_null($row["s5"]) ? 0 : $row["s5"]);
					}

					$sql="SELECT ".
						"SUM(wel_item_amt) as s1,".
						"SUM(wel_dis_amt) as s2,".
						"sum(wel_dis_amt2) as s3,".
						"SUM(wel_tax_amt) as s4,".
						"SUM(wel_line_amt) as s5 ".
						"FROM #__wel_sormism ".
						"WHERE wel_so_no='$this->wel_so_no' ";
					$sql=revert_to_the_available_sql($sql);
					if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
					if( $row=mysql_fetch_array($result) )
					{
						$tmp_item_amt=$tmp_item_amt + doubleval(is_null($row["s1"]) ? 0 : $row["s1"]);
						$tmp_dis_amt =$tmp_dis_amt  + doubleval(is_null($row["s2"]) ? 0 : $row["s2"]);
						$tmp_dis_amt2=$tmp_dis_amt2 + doubleval(is_null($row["s3"]) ? 0 : $row["s3"]);
						$tmp_tax_amt =$tmp_tax_amt  + doubleval(is_null($row["s4"]) ? 0 : $row["s4"]);
						$tmp_line_amt=$tmp_line_amt + doubleval(is_null($row["s5"]) ? 0 : $row["s5"]);
					}

					$sql="UPDATE #__wel_sorhdrm SET ".
						"wel_item_amt='$tmp_item_amt',".
						"wel_dis_amt='$tmp_dis_amt',".
						"wel_dis_amt2='$tmp_dis_amt2',".
						"wel_tax_amt='$tmp_tax_amt',".
						"wel_order_amt='$tmp_line_amt', ".
						"wel_upd_user='{$_SESSION['wel_user_id']}',".
						"wel_upd_date=now() ".
						"WHERE wel_so_no='$this->wel_so_no'";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					
				mysql_query("commit");
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage()); 
			}
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		if($msg_code==""){$msg_code="delete_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	public function delete_detail_tab0_all()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_delete")){throw new Exception("unauthorized_access");}
		
			if ($this->wel_so_no==""){throw new Exception("wel_so_no_miss");}
			
			$sql="SELECT * FROM #__wel_sordetm WHERE wel_so_no='$this->wel_so_no'";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
			while($row=mysql_fetch_array($result))
			{
				$dec_req_qty=doubleval(is_null($row["wel_req_qty"]) ? 0 : $row["wel_req_qty"]);
				$dec_os_qty=doubleval(is_null($row["wel_os_qty"]) ? 0 : $row["wel_os_qty"]);
				$dec_dn_qty=doubleval(is_null($row["wel_dn_qty"]) ? 0 : $row["wel_dn_qty"]);
				$dec_do_qty=doubleval(is_null($row["wel_do_qty"]) ? 0 : $row["wel_do_qty"]);

				if( ($dec_req_qty-$dec_os_qty)>0 ){throw new Exception("wel_so_qty_less_inv_qty");}
				if( $dec_dn_qty>0 ){throw new Exception("wel_so_qty_less_dn_qty");}
				if( $dec_do_qty>0 ){throw new Exception("wel_so_qty_less_do_qty");}
			}

			//查询MO制造单
			$sql="SELECT wel_so_no FROM #__wel_mordetm WHERE wel_so_no='$this->wel_so_no' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
			if( $row=mysql_fetch_array($result) ){throw new Exception("wel_so_no_in_mo");}
			
			try
			{
				mysql_query("begin");
				
					$sql="DELETE FROM #__wel_sordetm WHERE wel_so_no='$this->wel_so_no'";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				
					// Update SORHDRM discount & Tax
					$tmp_item_amt=0;
					$tmp_dis_amt=0;
					$tmp_dis_amt2=0;
					$tmp_tax_amt=0;
					$tmp_line_amt=0;
					$sql="SELECT ".
						"SUM(wel_item_amt) as s1,".
						"SUM(wel_dis_amt) as s2,".
						"sum(wel_dis_amt2) as s3,".
						"SUM(wel_tax_amt) as s4,".
						"SUM(wel_line_amt) as s5 ".
						"FROM #__wel_sormism ".
						"WHERE wel_so_no='$this->wel_so_no' ";
					$sql=revert_to_the_available_sql($sql);
					if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
					if( $row=mysql_fetch_array($result) )
					{
						$tmp_item_amt=$tmp_item_amt + doubleval(is_null($row["s1"]) ? 0 : $row["s1"]);
						$tmp_dis_amt =$tmp_dis_amt  + doubleval(is_null($row["s2"]) ? 0 : $row["s2"]);
						$tmp_dis_amt2=$tmp_dis_amt2 + doubleval(is_null($row["s3"]) ? 0 : $row["s3"]);
						$tmp_tax_amt =$tmp_tax_amt  + doubleval(is_null($row["s4"]) ? 0 : $row["s4"]);
						$tmp_line_amt=$tmp_line_amt + doubleval(is_null($row["s5"]) ? 0 : $row["s5"]);
					}

					$sql="UPDATE #__wel_sorhdrm SET ".
						"wel_item_amt='$tmp_item_amt',".
						"wel_dis_amt='$tmp_dis_amt',".
						"wel_dis_amt2='$tmp_dis_amt2',".
						"wel_tax_amt='$tmp_tax_amt',".
						"wel_order_amt='$tmp_line_amt', ".
						"wel_upd_user='{$_SESSION['wel_user_id']}',".
						"wel_upd_date=now() ".
						"WHERE wel_so_no='$this->wel_so_no'";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					
				mysql_query("commit");
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		if($msg_code==""){$msg_code="delete_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	public function delete_detail_tab1()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_delete")){throw new Exception("unauthorized_access");}

			if ($this->wel_so_no==""){throw new Exception("wel_so_no_miss");}
			if (!is_numeric($this->wel_line_no)){$this->wel_line_no=0;}
			$this->wel_line_no=intval($this->wel_line_no);
			if($this->wel_line_no==0){throw new Exception("wel_so_item_no_miss");}
			
			$sql="SELECT wel_so_no FROM #__wel_sormism WHERE wel_so_no='$this->wel_so_no' and wel_line_no='$this->wel_line_no' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_so_item_not_found");}
			
			try
			{
				mysql_query("begin");

					$sql="DELETE FROM #__wel_sormism WHERE wel_so_no='$this->wel_so_no' AND wel_line_no='$this->wel_line_no'";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					
					// Update SORHDRM discount & Tax
					$tmp_item_amt=0;
					$tmp_dis_amt=0;
					$tmp_dis_amt2=0;
					$tmp_tax_amt=0;
					$tmp_line_amt=0;
					$sql="SELECT ".
						"SUM(wel_item_amt) as s1,".
						"SUM(wel_dis_amt) as s2,".
						"sum(wel_dis_amt2) as s3,".
						"SUM(wel_tax_amt) as s4,".
						"SUM(wel_line_amt) as s5 ".
						"FROM #__wel_sordetm ".
						"WHERE wel_so_no='$this->wel_so_no' ";
					$sql=revert_to_the_available_sql($sql);
					if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
					if( $row=mysql_fetch_array($result) )
					{
						$tmp_item_amt=doubleval(is_null($row["s1"]) ? 0 : $row["s1"]);
						$tmp_dis_amt =doubleval(is_null($row["s2"]) ? 0 : $row["s2"]);
						$tmp_dis_amt2=doubleval(is_null($row["s3"]) ? 0 : $row["s3"]);
						$tmp_tax_amt =doubleval(is_null($row["s4"]) ? 0 : $row["s4"]);
						$tmp_line_amt=doubleval(is_null($row["s5"]) ? 0 : $row["s5"]);
					}

					$sql="SELECT ".
						"SUM(wel_item_amt) as s1,".
						"SUM(wel_dis_amt) as s2,".
						"sum(wel_dis_amt2) as s3,".
						"SUM(wel_tax_amt) as s4,".
						"SUM(wel_line_amt) as s5 ".
						"FROM #__wel_sormism ".
						"WHERE wel_so_no='$this->wel_so_no' ";
					$sql=revert_to_the_available_sql($sql);
					if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
					if( $row=mysql_fetch_array($result) )
					{
						$tmp_item_amt=$tmp_item_amt + doubleval(is_null($row["s1"]) ? 0 : $row["s1"]);
						$tmp_dis_amt =$tmp_dis_amt  + doubleval(is_null($row["s2"]) ? 0 : $row["s2"]);
						$tmp_dis_amt2=$tmp_dis_amt2 + doubleval(is_null($row["s3"]) ? 0 : $row["s3"]);
						$tmp_tax_amt =$tmp_tax_amt  + doubleval(is_null($row["s4"]) ? 0 : $row["s4"]);
						$tmp_line_amt=$tmp_line_amt + doubleval(is_null($row["s5"]) ? 0 : $row["s5"]);
					}

					$sql="UPDATE #__wel_sorhdrm SET ".
						"wel_item_amt='$tmp_item_amt',".
						"wel_dis_amt='$tmp_dis_amt',".
						"wel_dis_amt2='$tmp_dis_amt2',".
						"wel_tax_amt='$tmp_tax_amt',".
						"wel_order_amt='$tmp_line_amt', ".
						"wel_upd_user='{$_SESSION['wel_user_id']}',".
						"wel_upd_date=now() ".
						"WHERE wel_so_no='$this->wel_so_no'";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					
				mysql_query("commit");
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		if($msg_code==""){$msg_code="delete_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	public function delete_detail_tab1_all()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_delete")){throw new Exception("unauthorized_access");}

			if ($this->wel_so_no==""){throw new Exception("wel_so_no_miss");}
			
			$sql="SELECT wel_so_no FROM #__wel_sormism WHERE wel_so_no='$this->wel_so_no' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_so_item_not_found");}
			
			try
			{
				mysql_query("begin");
				
					$sql="DELETE FROM #__wel_sormism WHERE wel_so_no='$this->wel_so_no'";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
			
					// Update SORHDRM discount & Tax
					$tmp_item_amt=0;
					$tmp_dis_amt=0;
					$tmp_dis_amt2=0;
					$tmp_tax_amt=0;
					$tmp_line_amt=0;
					$sql="SELECT ".
						"SUM(wel_item_amt) as s1,".
						"SUM(wel_dis_amt) as s2,".
						"sum(wel_dis_amt2) as s3,".
						"SUM(wel_tax_amt) as s4,".
						"SUM(wel_line_amt) as s5 ".
						"FROM #__wel_sordetm ".
						"WHERE wel_so_no='$this->wel_so_no' ";
					$sql=revert_to_the_available_sql($sql);
					if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
					if( $row=mysql_fetch_array($result) )
					{
						$tmp_item_amt=doubleval(is_null($row["s1"]) ? 0 : $row["s1"]);
						$tmp_dis_amt =doubleval(is_null($row["s2"]) ? 0 : $row["s2"]);
						$tmp_dis_amt2=doubleval(is_null($row["s3"]) ? 0 : $row["s3"]);
						$tmp_tax_amt =doubleval(is_null($row["s4"]) ? 0 : $row["s4"]);
						$tmp_line_amt=doubleval(is_null($row["s5"]) ? 0 : $row["s5"]);
					}

					$sql="UPDATE #__wel_sorhdrm SET ".
						"wel_item_amt='$tmp_item_amt',".
						"wel_dis_amt='$tmp_dis_amt',".
						"wel_dis_amt2='$tmp_dis_amt2',".
						"wel_tax_amt='$tmp_tax_amt',".
						"wel_order_amt='$tmp_line_amt', ".
						"wel_upd_user='{$_SESSION['wel_user_id']}',".
						"wel_upd_date=now() ".
						"WHERE wel_so_no='$this->wel_so_no'";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					
				mysql_query("commit");
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		if($msg_code==""){$msg_code="delete_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}

}
?>
