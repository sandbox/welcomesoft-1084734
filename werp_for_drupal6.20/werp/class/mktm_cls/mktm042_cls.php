<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT 
?>
<?php
class mktm042_cls
{
	public $wel_pay_type="";
	public $wel_cr_day=0;
	public $wel_ams_yn="";
	public $wel_pay_des="";	
	
	private $wel_prog_code="mktm042";

	public function read()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_read")){throw new Exception("unauthorized_access");}
		
			$sql="SELECT * FROM #__wel_paytflm WHERE wel_pay_type='$this->wel_pay_type' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("txt_wel_pay_type_not_found");}
			$int__count=0;
			while ($int__count<mysql_num_fields($result)) 
			{
				$field_name=mysql_fetch_field($result,$int__count)->name;
				$return_val[$field_name]=$row[$field_name];
				$int__count++;
			}
			mysql_free_result($result);
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	public function addnew()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{			
			$conn=werp_db_connect();	
				
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_addnew")){throw new Exception("unauthorized_access");}
			
			if ($this->wel_pay_type==""){throw new Exception("txt_wel_pay_type_miss");}		
			if ($this->wel_pay_des==""){throw new Exception("txt_wel_pay_des_miss");}
			if (!is_numeric($this->wel_cr_day)){$this->wel_cr_day=0;}
			$this->wel_cr_day=doubleval($this->wel_cr_day);
			if (strlen($this->wel_pay_type)<2 || strlen($this->wel_pay_type)>11){throw new Exception("txt_wel_pay_type_length");}
			
			$sql="SELECT * FROM #__wel_paytflm WHERE wel_pay_type='$this->wel_pay_type' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
			if($row=mysql_fetch_array($result)){throw new Exception("txt_wel_pay_type_exist");}

			try
			{		
				mysql_query("begin");
				
					$sql="INSERT INTO #__wel_paytflm(wel_pay_type,wel_cr_day,wel_ams_yn,wel_pay_des) ".
					"VALUES('$this->wel_pay_type','$this->wel_cr_day','$this->wel_ams_yn','$this->wel_pay_des')";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					
				mysql_query("commit");
					
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}
			
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		if($msg_code==""){$msg_code="addnew_succee";}
		$return_val["msg_code"]=$msg_code;
		$return_val["wel_pay_type"]=$this->wel_pay_type;
		return $return_val;
	}
	
	public function edit()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{			
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_edit")){throw new Exception("unauthorized_access");}
		
			if ($this->wel_pay_type==""){throw new Exception("txt_wel_pay_type_miss");}		
			if ($this->wel_pay_des==""){throw new Exception("txt_wel_pay_des_miss");}
			if (!is_numeric($this->wel_cr_day)){$this->wel_cr_day=0;}
			if (strlen($this->wel_pay_type)<2 || strlen($this->wel_pay_type)>11){throw new Exception("txt_wel_pay_type_length");}
			
			$sql="SELECT * FROM #__wel_paytflm WHERE wel_pay_type='$this->wel_pay_type' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("txt_wel_pay_type_not_found");}			
			
			try
			{
				mysql_query("begin");
				
					$sql="UPDATE #__wel_paytflm SET ".
						"wel_cr_day='$this->wel_cr_day',".
						"wel_ams_yn='$this->wel_ams_yn',".
						"wel_pay_des='$this->wel_pay_des' ".
					"WHERE wel_pay_type='$this->wel_pay_type' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					
				mysql_query("commit");
					
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}
				
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}		
		if($msg_code==""){$msg_code="edit_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	public function delete()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_delete")){throw new Exception("unauthorized_access");}
		
			if ($this->wel_pay_type==""){throw new Exception("txt_wel_pay_type_miss");}		
			
			$sql="SELECT * FROM #__wel_paytflm WHERE wel_pay_type='$this->wel_pay_type' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}	
			if(!($row=mysql_fetch_array($result))){throw new Exception("txt_wel_pay_type_not_found");}	
			
			try
			{
				mysql_query("begin");
				
					$sql="DELETE FROM #__wel_paytflm WHERE wel_pay_type='$this->wel_pay_type'";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
						
				mysql_query("commit");
					
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}
						
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}		
		if($msg_code==""){$msg_code="delete_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
}
?>
