<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>

<?php
class pmcm001_cls
{
	public $wel_ctr_code="";
	public $wel_ctr_des="";
	public $wel_ctr_loc1="";
	public $wel_ctr_loc2="";
	public $wel_ctr_loc3="";
	public $wel_ctr_loc4="";
	public $wel_ctr_cont="";
	
	private $wel_prog_code="pmcm001";

	public function read()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_read")){throw new Exception("unauthorized_access");}
			
			$sql="SELECT * FROM #__wel_centrem WHERE wel_ctr_code = '$this->wel_ctr_code' limit 1 ";
			$sql=revert_to_the_available_sql($sql);
			if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_ctr_code_not_found");}
			foreach ($row as $key=>$value){$return_val[$key]=$value;}
			throw new Exception("");
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	public function addnew()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();	
				
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_addnew")){throw new Exception("unauthorized_access");}
				
			if($this->wel_ctr_code==""){throw new Exception("wel_ctr_code_miss");}
			if($this->wel_ctr_des==""){throw new Exception("wel_ctr_des_miss");}

			$sql="SELECT * FROM #__wel_centrem WHERE wel_ctr_code='$this->wel_ctr_code' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
			if($row=mysql_fetch_array($result))	throw new Exception("wel_ctr_code_exist");

			try
			{
				mysql_query("begin");
				$sql="INSERT INTO #__wel_centrem SET ".
					"wel_ctr_code='$this->wel_ctr_code',".
					"wel_ctr_des='$this->wel_ctr_des',".
					"wel_ctr_loc1='$this->wel_ctr_loc1',".
					"wel_ctr_loc2='$this->wel_ctr_loc2',".
					"wel_ctr_loc3='$this->wel_ctr_loc3',".
					"wel_ctr_loc4='$this->wel_ctr_loc4',".
					"wel_ctr_cont='$this->wel_ctr_cont'";
				$sql=revert_to_the_available_sql($sql);
				if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				mysql_query("commit");
			}
			catch (Exception $e1)
			{
				mysql_query('rollback');
				$msg_code=$e1->getMessage();
			}
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		if($msg_code==""){$msg_code="addnew_succee";}
		$return_val["msg_code"]=$msg_code;
		$return_val["wel_ctr_code"]=$this->wel_ctr_code;
		return $return_val;
	}
	
	public function edit()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_edit")){throw new Exception("unauthorized_access");}
			
			if($this->wel_ctr_code==""){throw new Exception("wel_ctr_code_miss");}
			if($this->wel_ctr_des==""){throw new Exception("wel_ctr_des_miss");}

			$sql="SELECT * FROM #__wel_centrem WHERE wel_ctr_code='$this->wel_ctr_code' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)) throw new Exception("wel_ctr_code_not_found");

			try
			{
				mysql_query("begin");
				$sql="UPDATE #__wel_centrem SET ".
					"wel_ctr_des='$this->wel_ctr_des',".
					"wel_ctr_loc1='$this->wel_ctr_loc1',".
					"wel_ctr_loc2='$this->wel_ctr_loc2',".
					"wel_ctr_loc3='$this->wel_ctr_loc3',".
					"wel_ctr_loc4='$this->wel_ctr_loc4',".
					"wel_ctr_cont='$this->wel_ctr_cont' ".
					" WHERE wel_ctr_code='$this->wel_ctr_code'";
				$sql=revert_to_the_available_sql($sql);
				if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				mysql_query("commit");				
			}
			catch (Exception $e1)
			{
				mysql_query('rollback');
				$msg_code=$e1->getMessage();
			}
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}	
		if($msg_code==""){$msg_code="edit_succee";}	
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	public function delete()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
		
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_edit")){throw new Exception("unauthorized_access");}
			
			if($this->wel_ctr_code==""){throw new Exception("wel_ctr_code_miss");}
			
			$sql="SELECT * FROM #__wel_centrem WHERE wel_ctr_code='$this->wel_ctr_code' limit 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_ctr_code_not_found");}
			
			try
			{
				mysql_query('begin');
				$sql="DELETE FROM #__wel_centrem WHERE wel_ctr_code='$this->wel_ctr_code'";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				mysql_query('commit');
			}
			catch (Exception $e1)
			{
				mysql_query('rollback');
				$msg_code=$e1->getMessage();
			}
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}		
		if($msg_code==""){$msg_code="delete_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
}
?>
