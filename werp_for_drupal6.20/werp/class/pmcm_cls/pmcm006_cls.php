<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>

<?php
class pmcm006_cls
{
	public $wel_mo_no="";
	public $wel_pattern="";
	public $wel_ctr_code="";
	public $wel_mo_line=0;
	public $wel_alrm_date="";
	public $wel_mo_remark="";
	public $wel_alrm_remark="";
	
	private $wel_prog_code="pmcm006";

	public function read()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_read")){throw new Exception("unauthorized_access");}
			
			$sql="SELECT * FROM #__wel_morhdrm  WHERE wel_mo_no='".$this->wel_mo_no."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_mo_no_not_found");}	
			
			$int__count=0;
			while ($int__count < mysql_num_fields($result)) 
			{
				$field_name=mysql_fetch_field($result,$int__count)->name;
				$return_val[$field_name]=$row[$field_name];
				$int__count++;
			}
			mysql_free_result($result);
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	//=================================================================================
	//新增制造单
	public function addnew()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_addnew")){throw new Exception("unauthorized_access");}
			
			if(($this->wel_mo_no=="") && ($this->wel_pattern=="")) {throw new Exception("wel_mo_no_miss");}
			if($this->wel_ctr_code==""){throw new Exception("wel_ctr_code_miss");}
			
			//生产中心档案是否存在
			if($this->wel_ctr_code!="") {
				$sql="SELECT * FROM #__wel_centrem WHERE wel_ctr_code='".$this->wel_ctr_code."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_ctr_code_not_found");}
			}

			$this->wel_alrm_date=(($this->wel_alrm_date=="") ? "null" : "'".$this->wel_alrm_date."'");
			
			try
			{
				mysql_query('begin');
				
				if ($this->wel_pattern!="")
				{
					$sql="SELECT * FROM #__wel_gentmow WHERE wel_pattern='$this->wel_pattern' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
					if(!($row=mysql_fetch_array($result))){throw new Exception("wel_pattern_not_found");}
					$wel_mo_nextno=intval(is_null($row["wel_mo_nextno"]) ? 0 : $row["wel_mo_nextno"]);
					$wel_mo_nextno=sprintf("%'08s",$wel_mo_nextno);
					if(strlen($wel_mo_nextno)>8){throw new Exception("wel_pattern_overflow");}
					$this->wel_mo_no=$this->wel_pattern.$wel_mo_nextno;
					
					//更新模式码表
					$sql="UPDATE #__wel_gentmow SET wel_mo_nextno=IFNULL(wel_mo_nextno,0)+1 WHERE ".
						"wel_pattern='$this->wel_pattern' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
				}
				
				//制造单是否存在
				$sql="SELECT * FROM #__wel_morhdrm WHERE wel_mo_no='$this->wel_mo_no' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
				if(($row=mysql_fetch_array($result))){throw new Exception("wel_mo_no_exist");}
				
				//新增制造单明细
				$sql="INSERT INTO #__wel_morhdrm(wel_mo_no,wel_pattern,".
							"wel_ctr_code,wel_alrm_date,".
							"wel_mo_remark,wel_alrm_remark,".
							"wel_crt_user,wel_crt_date,".
							"wel_last_line) ".
						"VALUES('".$this->wel_mo_no."','".$this->wel_pattern."','".
							$this->wel_ctr_code."',".$this->wel_alrm_date.",'".
							$this->wel_mo_remark."','".$this->wel_alrm_remark."','".
							$_SESSION["wel_user_id"]."','".date("Y-m-d H:i:s")."',".
							"0)";
				$sql=revert_to_the_available_sql($sql);
				if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}

				mysql_query('commit');
			}
			catch(Exception $e1)
			{
				mysql_query('rollback');
				throw new Exception($e1->getMessage());
			}
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		if($msg_code==""){$msg_code="addnew_succee";}
		$return_val["msg_code"]=$msg_code;
		$return_val["wel_mo_no"]=$this->wel_mo_no;
		return $return_val;
	}
	
	//=================================================================================
	//更新制造单
	public function edit()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_edit")){throw new Exception("unauthorized_access");}
			
			if($this->wel_mo_no==""){throw new Exception("wel_mo_no_miss");}
			if($this->wel_ctr_code==""){throw new Exception("wel_ctr_code_miss");}
			
			//生产中心是否存在
			if($this->wel_ctr_code!="") {
				$sql="SELECT * FROM #__wel_centrem WHERE wel_ctr_code='".$this->wel_ctr_code."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_ctr_code_not_found");}
			}
			
			//制造单是否存在
			$sql="select * from #__wel_morhdrm where wel_mo_no='$this->wel_mo_no' limit 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}	
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_mo_no_not_found");}
			
			$this->wel_alrm_date=(($this->wel_alrm_date=="") ? "null" : "'".$this->wel_alrm_date."'");
			
			try
			{
				mysql_query('begin');
				
				//更新制造单
				$sql="UPDATE #__wel_morhdrm SET ".
						"wel_alrm_date=".$this->wel_alrm_date.",".
						"wel_mo_remark='".$this->wel_mo_remark."',".
						"wel_alrm_remark='".$this->wel_alrm_remark."',".
						"wel_upd_user='".$_SESSION["wel_user_id"]."',".
						"wel_upd_date='".date("Y-m-d H:i:s")."' ".
					"WHERE wel_mo_no='".$this->wel_mo_no."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}	
					
				mysql_query('commit');
			}
			catch(Exception $e1)
			{
				mysql_query('rollback');
				throw new Exception($e1->getMessage());
			}
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		if($msg_code==""){$msg_code="edit_succee";}		
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	//=================================================================================
	//删除制造单
	public function delete()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_delete")){throw new Exception("unauthorized_access");}
			
			if($this->wel_mo_no==""){throw new Exception("wel_mo_no_miss");}
			
			//制造单是否存在
			$sql="SELECT * FROM #__wel_morhdrm WHERE wel_mo_no='$this->wel_mo_no' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}	
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_mo_no_not_found");}	
			
			//此制造单是否存在工作单
			$sql="SELECT * FROM #__wel_worhdrm WHERE wel_mo_no='$this->wel_mo_no' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}	
			if($row=mysql_fetch_array($result)){throw new Exception("wel_wo_no_exist");}
			
			try
			{
				mysql_query('begin');
				
				//删除制造单明细
				$sql="DELETE FROM #__wel_mordetm WHERE wel_mo_no='".$this->wel_mo_no."'";
				$sql=revert_to_the_available_sql($sql);
				if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}	
				
				//删除制造单
				$sql="DELETE FROM #__wel_morhdrm WHERE wel_mo_no='".$this->wel_mo_no."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}	
					
				mysql_query('commit');
			}
			catch(Exception $e1)
			{
				mysql_query('rollback');
				throw new Exception($e1->getMessage());
			}
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}		
		if($msg_code==""){$msg_code="delete_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	
	//=================================================================================
	//制造单明细删除
	public function detail_tab0_del()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_delete")){throw new Exception("unauthorized_access");}
			
			if($this->wel_mo_no==""){throw new Exception("wel_mo_no_miss");}
			
			//制造单明细是否存在
			$sql="SELECT * FROM #__wel_mordetm WHERE wel_mo_no='".$this->wel_mo_no."' AND wel_mo_line=".$this->wel_mo_line." LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_mo_line_not_found");}	
			
			//此制造单是否存在工作单
			$sql="SELECT * FROM #__wel_worhdrm WHERE wel_mo_no='".$this->wel_mo_no."' AND wel_mo_line=".$this->wel_mo_line." LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
			if(($row=mysql_fetch_array($result))){throw new Exception("wel_wo_no_exist");}
			
			try
			{
				mysql_query('begin');
				
				$sql="DELETE FROM #__wel_mordetm WHERE wel_mo_no='$this->wel_mo_no' AND wel_mo_line='$this->wel_mo_line' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
				
				mysql_query('commit');
			}
			catch(Exception $e1)
			{
				mysql_query('rollback');
				throw new Exception($e1->getMessage());
			}
			
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		if($msg_code==""){$msg_code="delete_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	//=================================================================================
	//制造单明细全部删除
	public function detail_tab0_del_all()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_delete")){throw new Exception("unauthorized_access");}
			
			if($this->wel_mo_no==""){throw new Exception("wel_mo_no_miss");}
			
			//制造单明细是否存在
			$sql="SELECT * FROM #__wel_mordetm WHERE wel_mo_no='".$this->wel_mo_no."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}	
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_mo_line_not_found");}	
			
			//此制造单是否存在工作单
			$sql="SELECT * FROM #__wel_worhdrm WHERE wel_mo_no='".$this->wel_mo_no."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
			if(($row=mysql_fetch_array($result))){throw new Exception("wel_wo_no_exist");}
			
			try
			{
				mysql_query('begin');
				
				$sql="DELETE FROM #__wel_mordetm WHERE wel_mo_no='".$this->wel_mo_no."'";
				$sql=revert_to_the_available_sql($sql);
				if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
					
				mysql_query('commit');
			}
			catch(Exception $e1)
			{
				mysql_query('rollback');
				throw new Exception($e1->getMessage());
			}
			
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		if($msg_code==""){$msg_code="delete_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
}
?>
