<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT

//require_once(WERP_SITE_PATH_INC."mktm_stdlib_cls.php");
?>
<?php
class pmcm006a_cls
{
	public $wel_mo_no="";
	public $wel_mo_line=0;
	public $wel_so_no="";
	public $wel_so_line=0;
	public $wel_part_no="";
	public $wel_part_des="";
	public $wel_req_qty=0;
	public $wel_req_date="";
	
	private $wel_prog_code="pmcm006";

	public function read()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
		
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_read")){throw new Exception("unauthorized_access");}
			
			$sql="SELECT mordetm.*,partflm.wel_part_des FROM #__wel_mordetm as mordetm ".
				"LEFT JOIN #__wel_partflm as partflm ON mordetm.wel_part_no=partflm.wel_part_no WHERE ".
				"mordetm.wel_mo_no='".$this->wel_mo_no."' AND mordetm.wel_mo_line ='".$this->wel_mo_line."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	//查询sql时出错了
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_mo_no_not_found");}	//没有符合条件的记录
			$int__count=0;
			while ($int__count < mysql_num_fields($result)) 
			{
				$field_name=mysql_fetch_field($result,$int__count)->name;
				$return_val[$field_name]=$row[$field_name];
				$int__count++;
			}
			mysql_free_result($result);
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	//=================================================================================
	public function addnew()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_addnew")){throw new Exception("unauthorized_access");}
			
			if($this->wel_mo_no=="") {throw new Exception("wel_mo_no_miss");}
			if($this->wel_part_no=="") {throw new Exception("wel_part_no_miss");}
			if(!is_numeric($this->wel_req_qty)){$this->wel_req_qty=0;}
			$this->wel_req_qty=doubleval($this->wel_req_qty);
			
			if($this->wel_req_qty<0){
				throw new Exception("wel_req_qty_error");
			}elseif($this->wel_req_qty==0){
				throw new Exception("wel_req_qty_miss");
			}
			
			if($this->wel_req_date=="") {throw new Exception("wel_req_date_miss");}
			$this->wel_req_date=(($this->wel_req_date=="") ? "null" : "'".$this->wel_req_date."'");
			
			//制造单是否存在
			$sql="SELECT * FROM #__wel_morhdrm WHERE wel_mo_no='".$this->wel_mo_no."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_mo_no_not_found");}
			$wel_last_line=intval(is_null($row["wel_last_line"]) ? 0 : $row["wel_last_line"])+1;
			
			//产品是否存在
			$sql="SELECT * FROM #__wel_partflm WHERE wel_part_no='".$this->wel_part_no."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_part_no_not_found");}
			
			/*
			$wel_bom_yn=intval(is_null($row["wel_bom_yn"]) ? 0 : $row["wel_bom_yn"]);
			if($wel_bom_yn==0){throw new Exception("wel_bom_not_approved");}		//产品BOM未批核
			*/
			
			if($this->wel_so_no!="")
			{
				if(!is_numeric($this->wel_so_line)){$this->wel_so_line=0;}
				//销售订单是否存在
				$sql="SELECT * FROM #__wel_sorhdrm WHERE wel_so_no='".$this->wel_so_no."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_so_no_not_found");}
				
				/*
				$wel_appr_yn=intval(is_null($row["wel_appr_yn"]) ? 0 : $row["wel_appr_yn"]);
				if($wel_appr_yn==0){throw new Exception("wel_so_no_not_approved");}
				*/
				
				//销售订单明细是否存在，无需与物料号相关联
				$sql="SELECT * FROM #__wel_sordetm WHERE ".
					"wel_so_no='".$this->wel_so_no."' AND wel_so_line=".$this->wel_so_line." LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_so_line_not_found");}
			}else{
				$this->wel_so_line="0";
			}
			
			//制造单明细是否存在
			$this->wel_mo_line=$wel_last_line;
			$sql="SELECT * FROM #__wel_mordetm WHERE ".
				"wel_mo_no='".$this->wel_mo_no."' AND wel_mo_line=".$this->wel_mo_line." LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if($row=mysql_fetch_array($result)){throw new Exception("wel_mo_line_exist");}
			
			try
			{
				mysql_query('begin');
				
				//添加制造单明细
				$sql="INSERT INTO #__wel_mordetm(wel_mo_no,wel_mo_line,wel_so_no,wel_so_line,wel_part_no,".
							"wel_req_qty,wel_req_date,wel_wo_qty,wel_crt_user,wel_crt_date) ".
						"VALUES('".$this->wel_mo_no."',".$wel_last_line.",'".$this->wel_so_no."',".
							$this->wel_so_line.",'".$this->wel_part_no."',".$this->wel_req_qty.",".
							$this->wel_req_date.",0,'".$_SESSION['wel_user_id']."',now())";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
					
				//更新制造单
				$sql="UPDATE #__wel_morhdrm ".
					"SET wel_last_line=".$wel_last_line." WHERE wel_mo_no='".$this->wel_mo_no."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
					
				mysql_query('commit');
			}
			catch(Exception $e1)
			{
				mysql_query('rollback');
				throw new Exception($e1->getMessage());
			}		
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		if($msg_code==""){$msg_code="addnew_succee";}
		$return_val["msg_code"]=$msg_code;
		$return_val["wel_mo_no"]=$this->wel_mo_no;
		$return_val["wel_mo_line"]=$this->wel_mo_line;
		return $return_val;
	}
	
	//=================================================================================
	public function edit()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_edit")){throw new Exception("unauthorized_access");}
			
			if($this->wel_mo_no=="") {throw new Exception("wel_mo_no_miss");}
			if($this->wel_part_no=="") {throw new Exception("wel_part_no_miss");}
			if(!is_numeric($this->wel_req_qty)){$this->wel_req_qty=0;}
			$this->wel_req_qty=doubleval($this->wel_req_qty);
			
			if($this->wel_req_qty<0){
				throw new Exception("wel_req_qty_error");
			}elseif($this->wel_req_qty==0){
				throw new Exception("wel_req_qty_miss");
			}
			
			if($this->wel_req_date=="") {throw new Exception("wel_req_date_miss");}
			$this->wel_req_date=(($this->wel_req_date=="") ? "null" : "'".$this->wel_req_date."'");

			//制造单是否存在
			$sql="SELECT * FROM #__wel_morhdrm WHERE wel_mo_no='".$this->wel_mo_no."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_mo_no_not_found");}
			
			//产品是否存在
			$sql="SELECT * FROM #__wel_partflm WHERE wel_part_no='".$this->wel_part_no."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_part_no_not_found");}
			
			/*
			$wel_bom_yn=intval(is_null($row["wel_bom_yn"]) ? 0 : $row["wel_bom_yn"]);
			if($wel_bom_yn==0){throw new Exception("wel_bom_not_approved");}		//产品BOM未批核
			*/
			
			if($this->wel_so_no!="")
			{
				if(!is_numeric($this->wel_so_line)){$this->wel_so_line=0;}
				//销售订单是否存在
				$sql="SELECT * FROM #__wel_sorhdrm WHERE wel_so_no='".$this->wel_so_no."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_so_no_not_found");}
				
				/*
				$wel_appr_yn=intval(is_null($row["wel_appr_yn"]) ? 0 : $row["wel_appr_yn"]);
				if($wel_appr_yn==0){throw new Exception("wel_so_no_not_approved");}
				*/
				
				//销售订单明细是否存在，无需与物料号相关联
				$sql="SELECT * FROM #__wel_sordetm WHERE ".
					"wel_so_no='".$this->wel_so_no."' AND wel_so_line=".$this->wel_so_line." LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_so_line_not_found");}
			}else{
				$this->wel_so_line="0";
			}

			//制造单明细是否存在
			if(!is_numeric($this->wel_mo_line)){$this->wel_mo_line=0;}
			$sql="SELECT * FROM #__wel_mordetm WHERE ".
				"wel_mo_no='".$this->wel_mo_no."' AND wel_mo_line=".$this->wel_mo_line." LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_mo_line_not_found");}
			
			$wel_req_qty=doubleval(is_null($row["wel_req_qty"]) ? 0 : $row["wel_req_qty"]);
			$wel_wo_qty=doubleval(is_null($row["wel_wo_qty"]) ? 0 : $row["wel_wo_qty"]);
			//需求数是否小于工作单数
			if(($this->wel_req_qty-$wel_wo_qty)<0){throw new Exception("wel_req_qty_less_wel_wo_qty");}
			
			try
			{
				mysql_query('begin');
				
				//更新制造单明细
				$sql="UPDATE #__wel_mordetm SET ".
							"wel_so_no='".$this->wel_so_no."',".
							"wel_so_line='".$this->wel_so_line."',".
							"wel_req_qty=".$this->wel_req_qty.",".
							"wel_req_date=".$this->wel_req_date.",".
							"wel_upd_user='".$_SESSION['wel_user_id']."',".
							"wel_upd_date=now() ".
						"WHERE wel_mo_no='".$this->wel_mo_no."' AND wel_mo_line=".$this->wel_mo_line." LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				
				mysql_query('commit');
			}
			catch(Exception $e1)
			{
				mysql_query('rollback');
				throw new Exception($e1->getMessage());
			}
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		if($msg_code==""){$msg_code="edit_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
}
?>