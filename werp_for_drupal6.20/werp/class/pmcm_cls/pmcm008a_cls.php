<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT

//require_once(WERP_SITE_PATH_INC."mktm_stdlib_cls.php");
?>
<?php
class pmcm008a_cls
{
	public $wel_mr_no="";
	public $wel_mr_line=0;
	public $wel_part_no="";
	public $wel_part_des="";
	public $wel_req_qty=0;
	public $wel_remark="";
	
	private $wel_prog_code="pmcm008";

	public function read()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
		
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_read")){throw new Exception("unauthorized_access");}
			
			$sql="SELECT mrdetfm.*,partflm.wel_part_des,partflm.wel_unit FROM #__wel_mrdetfm as mrdetfm ".
				"LEFT JOIN #__wel_partflm as partflm ON mrdetfm.wel_part_no=partflm.wel_part_no WHERE ".
				"mrdetfm.wel_mr_no='".$this->wel_mr_no."' AND mrdetfm.wel_mr_line ='".$this->wel_mr_line."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	//查询sql时出错了
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_mr_no_not_found");}	//没有符合条件的记录
			$int__count=0;
			while ($int__count < mysql_num_fields($result)) 
			{
				$field_name=mysql_fetch_field($result,$int__count)->name;
				$return_val[$field_name]=$row[$field_name];
				$int__count++;
			}
			mysql_free_result($result);
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	//=================================================================================
	public function addnew()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_addnew")){throw new Exception("unauthorized_access");}
			
			if($this->wel_mr_no=="") {throw new Exception("wel_mr_no_miss");}
			if($this->wel_part_no=="") {throw new Exception("wel_part_no_miss");}
			if(!is_numeric($this->wel_req_qty)){$this->wel_req_qty=0;}
			$this->wel_req_qty=doubleval($this->wel_req_qty);
			
			if($this->wel_req_qty<0){
				throw new Exception("wel_req_qty_error");
			}elseif($this->wel_req_qty==0){
				throw new Exception("wel_req_qty_miss");
			}
			
			//要求单是否存在
			$sql="SELECT * FROM #__wel_mrhdrfm WHERE wel_mr_no='".$this->wel_mr_no."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_mr_no_not_found");}
			$wel_last_line=intval(is_null($row["wel_last_line"]) ? 0 : $row["wel_last_line"])+1;
			
			//产品是否存在
			$sql="SELECT * FROM #__wel_partflm WHERE wel_part_no='".$this->wel_part_no."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_part_no_not_found");}
			
			/*
			$wel_bom_yn=intval(is_null($row["wel_bom_yn"]) ? 0 : $row["wel_bom_yn"]);
			if($wel_bom_yn==0){throw new Exception("wel_bom_not_approved");}		//产品BOM未批核
			*/
			
			//要求单明细是否存在
			$this->wel_mr_line=$wel_last_line;
			$sql="SELECT * FROM #__wel_mrdetfm WHERE ".
				"wel_mr_no='".$this->wel_mr_no."' AND wel_mr_line=".$this->wel_mr_line." LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if($row=mysql_fetch_array($result)){throw new Exception("wel_mr_line_exist");}
			
			try
			{
				mysql_query('begin');
				
				//添加要求单明细
				$sql="INSERT INTO #__wel_mrdetfm SET 
					wel_mr_no='".$this->wel_mr_no."',
					wel_mr_line=".$wel_last_line.",
					wel_part_no='".$this->wel_part_no."',
					wel_req_qty=".$this->wel_req_qty.",
					wel_os_qty=".$this->wel_req_qty.",
					wel_remark='".$this->wel_remark."',
					wel_crt_user='".$_SESSION['wel_user_id']."',
					wel_crt_date=now() ";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
					
				//更新要求单
				$sql="UPDATE #__wel_mrhdrfm ".
					"SET wel_last_line=".$wel_last_line." WHERE wel_mr_no='".$this->wel_mr_no."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
					
				mysql_query('commit');
			}
			catch(Exception $e1)
			{
				mysql_query('rollback');
				throw new Exception($e1->getMessage());
			}		
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		if($msg_code==""){$msg_code="addnew_succee";}
		$return_val["msg_code"]=$msg_code;
		$return_val["wel_mr_no"]=$this->wel_mr_no;
		$return_val["wel_mr_line"]=$this->wel_mr_line;
		return $return_val;
	}
	
	//=================================================================================
	public function edit()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_edit")){throw new Exception("unauthorized_access");}
			
			if($this->wel_mr_no=="") {throw new Exception("wel_mr_no_miss");}
			if($this->wel_part_no=="") {throw new Exception("wel_part_no_miss");}
			if(!is_numeric($this->wel_req_qty)){$this->wel_req_qty=0;}
			$this->wel_req_qty=doubleval($this->wel_req_qty);
			
			if($this->wel_req_qty<0){
				throw new Exception("wel_req_qty_error");
			}elseif($this->wel_req_qty==0){
				throw new Exception("wel_req_qty_miss");
			}
			
			//要求单是否存在
			$sql="SELECT * FROM #__wel_mrhdrfm WHERE wel_mr_no='".$this->wel_mr_no."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_mr_no_not_found");}
			
			//产品是否存在
			$sql="SELECT * FROM #__wel_partflm WHERE wel_part_no='".$this->wel_part_no."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_part_no_not_found");}
			
			/*
			$wel_bom_yn=intval(is_null($row["wel_bom_yn"]) ? 0 : $row["wel_bom_yn"]);
			if($wel_bom_yn==0){throw new Exception("wel_bom_not_approved");}		//产品BOM未批核
			*/
			
			//要求单明细是否存在
			if(!is_numeric($this->wel_mr_line)){$this->wel_mr_line=0;}
			$sql="SELECT * FROM #__wel_mrdetfm WHERE ".
				"wel_mr_no='".$this->wel_mr_no."' AND wel_mr_line=".$this->wel_mr_line." LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_mr_line_not_found");}
			
			$wel_req_qty=doubleval(is_null($row["wel_req_qty"]) ? 0 : $row["wel_req_qty"]);
			$wel_os_qty=doubleval(is_null($row["wel_os_qty"]) ? 0 : $row["wel_os_qty"]);
			//需求数是否小于未发料数
			$wel_os_qty=$this->wel_req_qty-$wel_req_qty+$wel_os_qty;
			if($wel_os_qty<0){throw new Exception("wel_os_qty_error");}
			
			try
			{
				mysql_query('begin');
				
				//更新要求单明细
				$sql="UPDATE #__wel_mrdetfm SET ".
							"wel_req_qty=".$this->wel_req_qty.",".
							"wel_os_qty=".$wel_os_qty.",".
							"wel_remark='".$this->wel_remark."',".
							"wel_upd_user='".$_SESSION['wel_user_id']."',".
							"wel_upd_date=now() ".
						"WHERE wel_mr_no='".$this->wel_mr_no."' AND wel_mr_line=".$this->wel_mr_line." LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				
				mysql_query('commit');
			}
			catch(Exception $e1)
			{
				mysql_query('rollback');
				throw new Exception($e1->getMessage());
			}
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		if($msg_code==""){$msg_code="edit_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
}
?>