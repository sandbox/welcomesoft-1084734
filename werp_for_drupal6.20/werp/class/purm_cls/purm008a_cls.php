<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>

<?php
class purm008a_cls
{
	public $wel_quot_no;
	public $wel_quot_line;
	public $wel_ven_code;	
	public $wel_quot_qty=0;
	public $wel_part_no;
	public $wel_part_des;
	public $wel_u_price=0;
	public $wel_prmy_quot=0;
	public $wel_unit_code;
	public $wel_lead_tm=0;
	public $wel_pur_unit;
	public $wel_pur_unit_rate=0;
	public $wel_pkg_ord=0;
	public $wel_min_ord=0;
	public $wel_ven_part;
	public $wel_ven_part_des;
	
	public $wel_dis_per;
	
	private $wel_prog_code="purm008";

	public function read()
	{
		$msg_code="";
		$return_val=array();
		 
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_read")){throw new Exception("unauthorized_access");}
			
			$sql="SELECT q.wel_quot_no,".
						"q.wel_ven_code,".
						"IFNULL(q.wel_quot_line,0) AS wel_quot_line,".
						"q.wel_part_no,".
						"p.wel_part_des,".
						"q.wel_unit_code,".
						"q.wel_pur_unit,".
						"IFNULL(q.wel_pur_unit_rate,0) AS wel_pur_unit_rate,".
						"IFNULL(q.wel_quot_qty,0) AS wel_quot_qty,".
						"IFNULL(q.wel_u_price,0) AS wel_u_price,".
						"IFNULL(q.wel_min_ord,0) AS wel_min_ord,".
						"IFNULL(q.wel_pkg_ord,0) AS wel_pkg_ord,".
						"IFNULL(q.wel_lead_tm,0) AS wel_lead_tm,".
						"IFNULL(q.wel_prmy_quot,0) AS wel_prmy_quot,".
						"v.wel_ven_part,".
						"v.wel_ven_part_des".
				" FROM #__wel_venqdtm q".
				" LEFT JOIN #__wel_partflm p ON p.wel_part_no=q.wel_part_no".
				" LEFT JOIN #__wel_venparm v ON (q.wel_ven_code=v.wel_ven_code)".
						" AND (q.wel_part_no=v.wel_part_no)".
				" WHERE q.wel_quot_no='$this->wel_quot_no' AND q.wel_quot_line='$this->wel_quot_line' LIMIT 1";  
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	//查询sql时出错了
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_part_no_not_found");}	//没有符合条件的记录
			$int__count=0;
			while ($int__count < mysql_num_fields($result)) 
			{
				$field_name=mysql_fetch_field($result,$int__count)->name;
				$return_val[$field_name]=$row[$field_name];
				$int__count++;
			}
			mysql_free_result($result);
			
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	public function addnew()
	{
		$msg_code="";
		$return_val=array();
 
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_addnew")){throw new Exception("unauthorized_access");}
			
			$str_wel_ven_code="";
			$dat_wel_quot_date="";
			$str_wel_cur_code="";
			//报价单是否存在
			if($this->wel_quot_no==""){throw new Exception("wel_quot_no_miss");}
			$sql="SELECT * FROM #__wel_venqhdm WHERE wel_quot_no='$this->wel_quot_no' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_quot_no_not_found");}
			$str_wel_ven_code=is_null($row["wel_ven_code"]) ? "" : $row["wel_ven_code"];
			$dat_wel_quot_date=is_null($row["wel_quot_date"]) ? "" : $row["wel_quot_date"];
			$str_wel_cur_code=is_null($row["wel_cur_code"]) ? "" : $row["wel_cur_code"];
			
			//判断品号是否存在
			if ($this->wel_part_no==""){throw new Exception("wel_part_no_miss");}
			$sql="SELECT wel_part_no FROM #__wel_partflm WHERE wel_part_no='$this->wel_part_no' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_part_no_not_found");}

			if (!is_numeric($this->wel_quot_qty)){$this->wel_quot_qty=0;}
			$this->wel_quot_qty=doubleval($this->wel_quot_qty);
			if (!is_numeric($this->wel_u_price)){$this->wel_u_price=0;}
			$this->wel_u_price=doubleval($this->wel_u_price);
			if (!is_numeric($this->wel_min_ord)){$this->wel_min_ord=0;}
			$this->wel_min_ord=doubleval($this->wel_min_ord);
			if (!is_numeric($this->wel_pkg_ord)){$this->wel_pkg_ord=0;}
			$this->wel_pkg_ord=doubleval($this->wel_pkg_ord);
			if (!is_numeric($this->wel_lead_tm)){$this->wel_lead_tm=0;}
			$this->wel_lead_tm=doubleval($this->wel_lead_tm);
			if (!is_numeric($this->wel_pur_unit_rate)){$this->wel_pur_unit_rate=0;}
			$this->wel_pur_unit_rate=doubleval($this->wel_pur_unit_rate);
			
			//$this->wel_pur_unit=strtoupper($this->wel_pur_unit);
			if($this->wel_pur_unit==''){
				$sql="SELECT UPPER(wel_unit) as tmp_unit FROM #__wel_partflm WHERE wel_part_no='$this->wel_part_no' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_part_no_not_found");}

				$this->wel_pur_unit=$row["tmp_unit"];
				$this->wel_pur_unit_rate=1.000000;
			}else{
				$this->wel_pur_unit=strtoupper($this->wel_pur_unit);

				$sql="SELECT CASE WHEN wel_unit='$this->wel_pur_unit' THEN ROUND(1.000000,6)".
					" WHEN wel_pur_unit='$this->wel_pur_unit' THEN ROUND(IFNULL(wel_pur_unit_rate,1),6)".
					" WHEN wel_pur_unit1='$this->wel_pur_unit' THEN ROUND(IFNULL(wel_pur_unit_rate1,1),6)".
					" WHEN wel_pur_unit2='$this->wel_pur_unit' THEN ROUND(IFNULL(wel_pur_unit_rate2,1),6)". 
					" END AS pur_rate FROM #__wel_partflm WHERE wel_part_no='$this->wel_part_no'";
				$sql=revert_to_the_available_sql($sql);
				if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_pur_unit_error");}

				if (is_null($row["pur_rate"])){throw new Exception("wel_pur_unit_error");}
				$this->wel_pur_unit_rate=$row["pur_rate"];
			}
			
			//判断相同的单号＋品号+报价数量的细节是否已经添加
			$sql="SELECT * FROM #__wel_venqdtm WHERE wel_part_no='$this->wel_part_no' ".
					"AND wel_quot_no='$this->wel_quot_no' AND wel_quot_qty=$this->wel_quot_qty LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
			if($row=mysql_fetch_array($result)){throw new Exception("wel_part_no_exist");}
			
			try
			{
				mysql_query("begin");
				
					//获取行号
					$sql="SELECT MAX(IFNULL(wel_quot_line,0)) FROM #__wel_venqdtm WHERE wel_quot_no='$this->wel_quot_no' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
					if(!($row=mysql_fetch_array($result)))
					{
						$this->wel_quot_line=1;
					}
					else
					{
						$this->wel_quot_line=intval(is_null($row[0]) ? 0 : $row[0])+1;
					}
				
					$sql="INSERT INTO #__wel_venqdtm SET ".
							 "wel_quot_no='$this->wel_quot_no',".
							 "wel_quot_line='$this->wel_quot_line',".
							 "wel_ven_code='$str_wel_ven_code',".
							 "wel_part_no='$this->wel_part_no',".
							 "wel_unit_code='$this->wel_unit_code',".
							 "wel_pur_unit='$this->wel_pur_unit',".
							 "wel_pur_unit_rate='$this->wel_pur_unit_rate',".
							 "wel_quot_qty='$this->wel_quot_qty',".
							 "wel_u_price='$this->wel_u_price',".
							 "wel_min_ord='$this->wel_min_ord',".
							 "wel_pkg_ord='$this->wel_pkg_ord',".
							 "wel_lead_tm='$this->wel_lead_tm',".
							 "wel_prmy_quot='$this->wel_prmy_quot',".
							 "wel_ven_part='$this->wel_ven_part',".
							 "wel_appr_yn='$int_wel_appr_yn',".
							 "wel_cur_code='$str_wel_cur_code',".
							 "wel_quot_date='$dat_wel_quot_date'";
					$sql=revert_to_the_available_sql($sql);
					if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}

					$sql="UPDATE #__wel_venqhdm SET ".
							 "wel_upd_user='{$_SESSION['wel_user_id']}',".
							 "wel_upd_date=now() ".
						 "WHERE wel_quot_no='$this->wel_quot_no' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}

					//更新供应商物料
					$sql="SELECT wel_ven_code FROM #__wel_venparm ".
							"WHERE wel_ven_code='$str_wel_ven_code' AND wel_part_no='$this->wel_part_no'";
					$sql=revert_to_the_available_sql($sql);
					if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
					if(!($row=mysql_fetch_array($result)))
					{
						$sql="INSERT INTO #__wel_venparm SET ".
									"wel_ven_code='$str_wel_ven_code',".
									"wel_part_no='$this->wel_part_no',".
									"wel_pur_unit='$this->wel_pur_unit',".
									"wel_pur_unit_rate='$this->wel_pur_unit_rate',".
									"wel_min_ord='$this->wel_min_ord',".
									"wel_pkg_ord='$this->wel_pkg_ord',".
									"wel_lead_tm='$this->wel_lead_tm',".
									"wel_ven_part='$this->wel_ven_part',".
									"wel_ven_part_des='$this->wel_ven_part_des'";
							$sql=revert_to_the_available_sql($sql);
							if(!(($result1=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}  
					}else {
						$sql="UPDATE #__wel_venparm SET ".
									"wel_pur_unit='$this->wel_pur_unit',".
									"wel_pur_unit_rate='$this->wel_pur_unit_rate',".
									"wel_min_ord='$this->wel_min_ord',".
									"wel_pkg_ord='$this->wel_pkg_ord',".
									"wel_lead_tm='$this->wel_lead_tm',".
									"wel_ven_part='$this->wel_ven_part',".
									"wel_ven_part_des='$this->wel_ven_part_des' ".
								"WHERE wel_ven_code='$str_wel_ven_code' AND wel_part_no='$this->wel_part_no'";
						$sql=revert_to_the_available_sql($sql);
						if(!(($result1=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}  
					}
					mysql_free_result($result);
				
				mysql_query("commit");
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}
			 
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		if($msg_code==""){$msg_code="addnew_succee";}
		$return_val["msg_code"]=$msg_code;
		$return_val["wel_quot_no"]=$this->wel_quot_no;
		$return_val["wel_part_no"]=$this->wel_part_no;
		return $return_val;
	}
	
	public function edit()
	{
		$msg_code="";
		$return_val=array();
 
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_edit")){throw new Exception("unauthorized_access");}
			
			$str_wel_ven_code="";
			$dat_wel_quot_date="";
			$str_wel_cur_code="";
			//报价单是否存在
			if($this->wel_quot_no==""){throw new Exception("wel_quot_no_miss");}
			$sql="SELECT * FROM #__wel_venqhdm WHERE wel_quot_no='$this->wel_quot_no' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_quot_no_not_found");}
			$str_wel_ven_code=is_null($row["wel_ven_code"]) ? "" : $row["wel_ven_code"];
			$dat_wel_quot_date=is_null($row["wel_quot_date"]) ? "" : $row["wel_quot_date"];
			$str_wel_cur_code=is_null($row["wel_cur_code"]) ? "" : $row["wel_cur_code"];
			
			//报价单明细是否存在
			$sql="select * from #__wel_venqdtm where wel_quot_no='$this->wel_quot_no' and wel_quot_line='$this->wel_quot_line' limit 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_quot_no_detail_not_found");}
			
			if (!is_numeric($this->wel_quot_qty)){$this->wel_quot_qty=0;}
			$this->wel_quot_qty=doubleval($this->wel_quot_qty);
			if (!is_numeric($this->wel_u_price)){$this->wel_u_price=0;}
			$this->wel_u_price=doubleval($this->wel_u_price);
			if (!is_numeric($this->wel_min_ord)){$this->wel_min_ord=0;}
			$this->wel_min_ord=doubleval($this->wel_min_ord);
			if (!is_numeric($this->wel_pkg_ord)){$this->wel_pkg_ord=0;}
			$this->wel_pkg_ord=doubleval($this->wel_pkg_ord);
			if (!is_numeric($this->wel_lead_tm)){$this->wel_lead_tm=0;}
			$this->wel_lead_tm=doubleval($this->wel_lead_tm);
			if (!is_numeric($this->wel_pur_unit_rate)){$this->wel_pur_unit_rate=0;}
			$this->wel_pur_unit_rate=doubleval($this->wel_pur_unit_rate);
			
			if($this->wel_pur_unit==''){
				$sql="SELECT UPPER(wel_unit) as tmp_unit FROM #__wel_partflm WHERE wel_part_no='$this->wel_part_no' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_part_no_not_found");}

				$this->wel_pur_unit=$row["tmp_unit"];
				$this->wel_pur_unit_rate=1.000000;
			}else{
				$this->wel_pur_unit=strtoupper($this->wel_pur_unit);

				$sql="SELECT CASE WHEN wel_unit='$this->wel_pur_unit' THEN ROUND(1.000000,6)".
					" WHEN wel_pur_unit='$this->wel_pur_unit' THEN ROUND(IFNULL(wel_pur_unit_rate,1),6)".
					" WHEN wel_pur_unit1='$this->wel_pur_unit' THEN ROUND(IFNULL(wel_pur_unit_rate1,1),6)".
					" WHEN wel_pur_unit2='$this->wel_pur_unit' THEN ROUND(IFNULL(wel_pur_unit_rate2,1),6)". 
					" END AS pur_rate FROM #__wel_partflm WHERE wel_part_no='$this->wel_part_no'";
				$sql=revert_to_the_available_sql($sql);
				if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_pur_unit_error");}

				if (is_null($row["pur_rate"])){throw new Exception("wel_pur_unit_error");}
				$this->wel_pur_unit_rate=$row["pur_rate"];
			}

			try
			{
				mysql_query("begin");
				
					$sql="UPDATE #__wel_venqdtm SET ".
							 "wel_unit_code='$this->wel_unit_code',".
							 "wel_pur_unit='$this->wel_pur_unit',".
							 "wel_pur_unit_rate='$this->wel_pur_unit_rate',".
							 "wel_quot_qty='$this->wel_quot_qty',".
							 "wel_u_price='$this->wel_u_price',".
							 "wel_min_ord='$this->wel_min_ord',".
							 "wel_pkg_ord='$this->wel_pkg_ord',".
							 "wel_lead_tm='$this->wel_lead_tm',".
							 "wel_prmy_quot='$this->wel_prmy_quot',".
							 "wel_ven_part='$this->wel_ven_part',".
							 "wel_ven_code='$str_wel_ven_code',".
							 "wel_cur_code='$str_wel_cur_code',".
							 "wel_quot_date='$dat_wel_quot_date' ".
						"WHERE wel_quot_no='$this->wel_quot_no' AND wel_quot_line='$this->wel_quot_line'";
					$sql=revert_to_the_available_sql($sql);
					if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}

					$sql="UPDATE #__wel_venqhdm SET ".
							 "wel_upd_user='{$_SESSION['wel_user_id']}',".
							 "wel_upd_date=now() ".
						 "WHERE wel_quot_no='$this->wel_quot_no' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}

					//更新供应商物料
					$sql="SELECT wel_ven_code ".
							"FROM #__wel_venparm ".
							"WHERE wel_ven_code='$str_wel_ven_code' AND wel_part_no='$this->wel_part_no'";
					$sql=revert_to_the_available_sql($sql);
					if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
					if(!($row=mysql_fetch_array($result)))
					{
						$sql="INSERT INTO #__wel_venparm SET ".
									"wel_ven_code='$str_wel_ven_code',".
									"wel_part_no='$this->wel_part_no',".
									"wel_pur_unit='$this->wel_pur_unit',".
									"wel_pur_unit_rate='$this->wel_pur_unit_rate',".
									"wel_min_ord='$this->wel_min_ord',".
									"wel_pkg_ord='$this->wel_pkg_ord',".
									"wel_lead_tm='$this->wel_lead_tm',".
									"wel_ven_part='$this->wel_ven_part',".
									"wel_ven_part_des='$this->wel_ven_part_des'";
						$sql=revert_to_the_available_sql($sql);
						if(!(($result1=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}     
					}else {
						$sql="UPDATE #__wel_venparm SET ".
									"wel_pur_unit='$this->wel_pur_unit',".
									"wel_pur_unit_rate='$this->wel_pur_unit_rate',".
									"wel_min_ord='$this->wel_min_ord',".
									"wel_pkg_ord='$this->wel_pkg_ord',".
									"wel_lead_tm='$this->wel_lead_tm',".
									"wel_ven_part='$this->wel_ven_part',".
									"wel_ven_part_des='$this->wel_ven_part_des' ".
								"WHERE wel_ven_code='$str_wel_ven_code' AND wel_part_no='$this->wel_part_no'";
						$sql=revert_to_the_available_sql($sql);
						if(!(($result1=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}     
					}
					mysql_free_result($result);
					
				mysql_query("commit");
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}
			
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}		
		if($msg_code==""){$msg_code="edit_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
}
?>
