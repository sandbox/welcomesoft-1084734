<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>

<?php 
	class strm007_cls
	{
		public $wel_ctrl_flow="";
		public $wel_tran_date="";
		public $wel_wo_no="";
		public $wel_lot_no="";
		public $wel_tran_qty=0;
		public $wel_ref_no="";
		public $wel_tran_rmk="";
		
		private $wel_prog_code="strm007";
		
		//工作单完成入仓
		public function addnew()
		{
			$msg_code="";
			$return_val=array();
			
			try
			{
				$conn=werp_db_connect();
				
				if (!check_permission($conn,$this->wel_prog_code,"wel_access_addnew")){throw new Exception("unauthorized_access");}

				if($this->wel_ctrl_flow==""){throw new Exception("wel_ctrl_flow_miss");}
				if($this->wel_tran_date==""){throw new Exception("wel_tran_date_miss");}
				if($this->wel_wo_no==""){throw new Exception("wel_wo_no_miss");}

				if(!is_numeric($this->wel_tran_qty)) {throw new Exception("wel_tran_qty_miss");}
				$this->wel_tran_qty=doubleval($this->wel_tran_qty);
				if($this->wel_tran_qty==0){throw new Exception("wel_tran_qty_miss");}

				if($this->wel_ctrl_flow!="")
				{
					$sql="SELECT * FROM #__wel_whctrlm WHERE wel_ctrl_flow='".$this->wel_ctrl_flow."' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
					if(!($row=mysql_fetch_array($result))){throw new Exception("wel_whctrlm_not_found");}	
					$str__wh_fm=is_null($row["wel_wh_fm"]) ? "" : $row["wel_wh_fm"];
					$str__wh_to=is_null($row["wel_wh_to"]) ? "" : $row["wel_wh_to"];
					$int__dis_able=intval(is_null($row["wel_dis_able"]) ? 0 : $row["wel_dis_able"]);
					$int__fgs_flow=intval(is_null($row["wel_fgs_flow"]) ? 0 : $row["wel_fgs_flow"]);
					$int__avg_cflow=intval(is_null($row["wel_avg_cflow"]) ? 0 : $row["wel_avg_cflow"]);

					if($int__fgs_flow!=1){throw new Exception("wel_whctrlm_not_fgs_flow");}
					if($int__dis_able==1){throw new Exception("wel_whctrlm_disable");}

					$sql="SELECT * FROM #__wel_whlocfm WHERE wel_wh_code='".$str__wh_fm."' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
					if(!($row=mysql_fetch_array($result))){throw new Exception("wel_wh_fm_not_found");}	
				
					$sql="SELECT * FROM #__wel_whlocfm WHERE wel_wh_code='".$str__wh_to."' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
					if(!($row=mysql_fetch_array($result))){throw new Exception("wel_wh_to_not_found");}	
				}

				if($this->wel_tran_date!="")
				{
					$this->wel_tran_date=(($this->wel_tran_date=="") ? "null" : $this->wel_tran_date);
					$sql="SELECT * FROM #__wel_closedm LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());} 
					if(!($row=mysql_fetch_array($result))){throw new Exception("system_init_error");}
					$sys_phy_date=is_null($row["wel_period1"]) ? $row["wel_phy_date"] : $row["wel_period1"];

					if( date("Y-m-d",strtotime($sys_phy_date)) >= date("Y-m-d",strtotime($this->wel_tran_date)) )
					{
						throw new Exception("wel_tran_date_error");
					}
				}

				//工作单否存在
				$sql="SELECT * FROM #__wel_worhdrm WHERE wel_wo_no='".$this->wel_wo_no."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_wo_no_not_found");}	
				$wo_part_no=is_null($row["wel_part_no"]) ? "" :  $row["wel_part_no"];
				$wo_ctr_code=is_null($row["wel_ctr_code"]) ? "" :  $row["wel_ctr_code"];
				$wo_mo_no=is_null($row["wel_mo_no"]) ? "" :  $row["wel_mo_no"];
				$wo_mo_line=intval(is_null($row["wel_mo_line"]) ? 0 : $row["wel_mo_line"]);
				$wo_req_qty=doubleval(is_null($row["wel_req_qty"]) ? 0 : $row["wel_req_qty"]);
				$wo_fg_qty=doubleval(is_null($row["wel_fg_qty"]) ? 0 : $row["wel_fg_qty"]);

				//制造单明细是否存在
				$mo_so_no="";
				$mo_so_line=0;
				$mo_fg_qty=0;
				if($this->wo_mo_no!="")
				{
					$sql="SELECT * FROM #__wel_mordetm WHERE wel_mo_no='".$wo_mo_no."' AND wel_mo_line='".$wo_mo_line."' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					if(!($row=mysql_fetch_array($result))){throw new Exception("wel_mo_no_not_found");}	
					$mo_so_no=is_null($row["wel_so_no"]) ? "" : $row["wel_so_no"];
					$mo_so_line=intval(is_null($row["wel_so_line"]) ? 0 : $row["wel_so_line"]);
					$mo_fg_qty=doubleval(is_null($row["wel_fg_qty"]) ? 0 : $row["wel_fg_qty"]);
				}

				//物品是否存在
				$part_cat_code="";
				$part_std_cost=0;
				$part_avg_cost=0;
				$part_avg_bomc=0;
				if($this->wel_part_no!="")
				{
					$sql="SELECT * FROM #__wel_partflm WHERE wel_part_no='".$wo_part_no."' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
					if(!($row=mysql_fetch_array($result))){throw new Exception("wel_part_no_not_found");}	
					$part_cat_code=is_null($row["wel_cat_code"]) ? "" : $row["wel_cat_code"];
					$part_std_cost=doubleval(is_null($row["wel_std_cost"]) ? 0 : $row["wel_std_cost"]);
					$part_avg_cost=doubleval(is_null($row["wel_avg_cost"])? 0 : $row["wel_avg_cost"]);
					$part_avg_bomc=doubleval(is_null($row["wel_avg_bomc"]) ? 0 : $row["wel_avg_bomc"]);
				}

				if($this->wel_tran_qty<0)
				{
					if($wo_fg_qty+$this->wel_tran_qty < 0){throw new Exception("wel_tran_qty_excess_wo");}

					$stk_stk_qty = 0;
					$sql="SELECT * FROM #__wel_whlotfm ".
						" WHERE wel_wh_code='".$str__wh_to."' AND wel_part_no='".$wo_part_no."' ".
						" AND wel_lot_no='".$this->wel_lot_no."' AND wel_stk_loc='' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
					if($row=mysql_fetch_array($result))
					{
						$stk_stk_qty=doubleval(is_null($row["wel_stk_qty"]) ? 0 : $row["wel_stk_qty"]);
					}

					if($stk_stk_qty+$this->wel_tran_qty<0){throw new Exception("wel_tran_qty_excess_stock");}
				}else{
					if($wo_req_qty-$wo_fg_qty-$this->wel_tran_qty < 0){throw new Exception("wel_tran_qty_excess_wo");}
				}

				if($int__avg_cflow==1)
				{
					$stk_sum_qty = 0;
					$sql="SELECT SUM(wel_stk_qty) AS wel_sum_qty".
						" FROM #__wel_whlotfm a".
						" LEFT JOIN #__wel_whlocfm w ON a.wel_wh_code=w.wel_wh_code".
						" WHERE a.wel_part_no='".$wo_part_no."' AND w.wel_wh_cost>0";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
					if($row=mysql_fetch_array($result))
					{
						$stk_sum_qty=doubleval(is_null($row["wel_sum_qty"]) ? 0 : $row["wel_sum_qty"]);
					}

					if($stk_sum_qty+$this->wel_tran_qty>0)
					{
						$part_avg_cost = ROUND( ($stk_sum_qty*$part_avg_cost + $part_avg_bomc*$this->wel_tran_qty)/($stk_sum_qty+$this->wel_tran_qty) ,4);
						if($part_avg_cost<=0){$part_avg_cost=0;}
					}
				}

				try
				{
					mysql_query("begin");
					
						//获得最新交易记录号
						$sql="SELECT wel_trn_recno FROM #__wel_tranflm ORDER BY wel_trn_recno DESC LIMIT 1";
						$sql=revert_to_the_available_sql($sql);
						if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
						if(!($row=mysql_fetch_array($result)))
						{
							$dec__next_trn_recno=1;
						}
						else
						{
							$dec__next_trn_recno=doubleval(is_null($row["wel_trn_recno"]) ? 1 : $row["wel_trn_recno"]+1);
						}

						//添加记录到tranflm表
						$sql="INSERT INTO #__wel_tranflm SET ".           //Transaction File Add 17 Field
								"wel_trn_recno=".$dec__next_trn_recno.",".
								"wel_tran_date=".$this->wel_tran_date.",".
								"wel_wh_fm='".$this->wel_wh_fm."',".
								"wel_wh_to='".$this->wel_wh_to."',".
								"wel_tran_code='".$this->wel_ctrl_flow."',".
								"wel_flow_code='FGS',".
								"wel_wo_no='".$this->wel_wo_no."',".
								"wel_ctr_code='".$wo_ctr_code."',".
								"wel_part_no='".$wo_part_no."',".
								"wel_lot_no_to='".$this->wel_lot_no."',".
								"wel_tran_qty=".$this->wel_tran_qty.",".
								"wel_ref_no='".$this->wel_ref_no."',".
								"wel_tran_rmk='".$this->wel_tran_rmk."',".
								"wel_cat_code='".$part_cat_code."',".
								"wel_so_no='".$mo_so_no."',".
								"wel_so_line=".$mo_so_line.",".
								"wel_std_cost=".$part_std_cost.",".
								"wel_avg_cost=".$part_avg_cost.",".
								"wel_avg_bomc=".$part_avg_bomc.",".
								"wel_crt_user='".$_SESSION['wel_user_id']."',".
								"wel_crt_date=now()";
						$sql=revert_to_the_available_sql($sql);
						if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}

						//更新物品仓存数
						$sql="SELECT * FROM #__wel_whlotfm ".
							" WHERE wel_wh_code='".$str__wh_to."' AND wel_part_no='".$wo_part_no."' ".
							" AND wel_lot_no='".$this->wel_lot_no."' AND wel_stk_loc='' LIMIT 1";
						$sql=revert_to_the_available_sql($sql);
						if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
						if($row=mysql_fetch_array($result))
						{
							$sql="UPDATE #__wel_whlotfm SET ".
								" wel_stk_qty=IFNULL(wel_stk_qty,0)+$this->wel_tran_qty ".
								" WHERE wel_wh_code='".$str__wh_to."' AND wel_part_no='".$wo_part_no."' ".
								" AND wel_lot_no='".$this->wel_lot_no."' AND wel_stk_loc='' LIMIT 1";
							$sql=revert_to_the_available_sql($sql);
							if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
						}else{
							$sql="INSERT INTO #__wel_whlotfm SET ".
									"wel_wh_code='".$str__wh_to."',".
									"wel_part_no='".$wo_part_no."',".
									"wel_lot_no='".$this->wel_lot_no."',".
									"wel_stk_loc='',".
									"wel_stk_qty=".($this->wel_tran_qty).",".
									"wel_crt_user='{$_SESSION['wel_user_id']}', ".
									"wel_crt_date=now()";
							$sql=revert_to_the_available_sql($sql);
							if(!($result1=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
						}

						$sql="UPDATE #__wel_worhdrm SET ".
							" wel_fg_qty=IFNULL(wel_fg_qty,0)+$this->wel_tran_qty".
							" WHERE wel_wo_no='".$this->wel_wo_no."' LIMIT 1";
						$sql=revert_to_the_available_sql($sql);
						if(!($result1=mysql_query($sql,$conn))){throw new Exception(mysql_error());}

						if($this->wo_mo_no!="")
						{
							$sql="UPDATE #__wel_mordetm SET ".
								" wel_fg_qty=IFNULL(wel_fg_qty,0)+$this->wel_tran_qty".
								" WHERE wel_mo_no='".$wo_mo_no."' AND wel_mo_line='".$wo_mo_line."' LIMIT 1";
							$sql=revert_to_the_available_sql($sql);
							if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
						}

						if($int__avg_cflow==1)
						{
							$sql="UPDATE #__wel_partflm SET ".
								" wel_avg_cost=".$part_avg_cost." ".
								" WHERE wel_part_no='".$wo_part_no."' LIMIT 1";
							$sql=revert_to_the_available_sql($sql);
							if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
						}

						mysql_free_result($result);

					mysql_query("commit");
					
				}
				catch(Exception $e1)
				{
					mysql_query("rollback");
					throw new Exception($e1->getMessage());
				}
				
			}
			catch (Exception $e)
			{
				$msg_code=$e->getMessage();
			}
			if($msg_code==""){$msg_code="addnew_succee";}
			$return_val["msg_code"]=$msg_code;
			$return_val["wel_wo_no"]=$this->wel_wo_no;
			return $return_val;
		}
		
	}
?>