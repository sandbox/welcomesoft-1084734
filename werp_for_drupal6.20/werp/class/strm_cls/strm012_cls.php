<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>

<?php 
	class strm012_cls
	{
		public $wel_tran_date;
		
		private $wel_prog_code="strm012";
		
		//确认
		public function confirm()
		{
			$msg_code="";
			$return_val=array();
			
			try
			{
				$conn=werp_db_connect();
				
				if (!check_permission($conn,$this->wel_prog_code,"wel_access_addnew")){throw new Exception("unauthorized_access");}
				
				if($this->wel_tran_date==''){throw new Exception('wel_tran_date_miss');}

				$this->wel_tran_date=(($this->wel_tran_date=="") ? "null" : $this->wel_tran_date);
				$sql="SELECT * FROM #__wel_closedm LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());} 
				if(!($row=mysql_fetch_array($result))){throw new Exception("system_init_error");}
				$sys_phy_date=is_null($row["wel_period1"]) ? $row["wel_phy_date"] : $row["wel_period1"];
				$sys_phy_qty =is_null($row["wel_period1"]) ? 1 : 0;

				if( date("Y-m-d",strtotime($sys_phy_date)) >= date("Y-m-d",strtotime($this->wel_tran_date)) )
				{
					throw new Exception("wel_tran_date_error");
				}
				
				try
				{
					mysql_query('begin');

						$sql =	"UPDATE #__wel_closedm SET ".
    							"wel_period12 = wel_period11,".
    							"wel_period11 = wel_period10,".
    							"wel_period10 = wel_period9,".
    							"wel_period9 = wel_period8,".
    							"wel_period8 = wel_period7,".
    							"wel_period7 = wel_period6,".
    							"wel_period6 = wel_period5,".
    							"wel_period5 = wel_period4,".
    							"wel_period4 = wel_period3,".
		    					"wel_period3 = wel_period2,".
		    					"wel_period2 = wel_period1,".
		    					"wel_period1 = '{$this->wel_tran_date}' ";
						$sql=revert_to_the_available_sql($sql);
		   				if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}

						$sql =	"UPDATE #__wel_partwhm SET ".
    							"wel_period12 = wel_period11,".
		    					"wel_period11 = wel_period10,".
		    					"wel_period10 = wel_period9,".
		    					"wel_period9 = wel_period8,".
		    					"wel_period8 = wel_period7,".
		    					"wel_period7 = wel_period6,".
		    					"wel_period6 = wel_period5,".
		    					"wel_period5 = wel_period4,".
		    					"wel_period4 = wel_period3,".
		    					"wel_period3 = wel_period2,".
		    					"wel_period2 = wel_period1,".
		    					"wel_period1 = 0";
						$sql=revert_to_the_available_sql($sql);
		   				if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}

						$sql =	"SELECT wel_part_no, wel_wh_code, sum(IFNULL(wel_stk_qty,0)) AS wel_stk_qty ".
		    					"FROM #__wel_whlotfm ".
		    					"GROUP BY wel_part_no,wel_wh_code ORDER BY wel_part_no,wel_wh_code";
						$sql=revert_to_the_available_sql($sql);
    					if(!$wh_result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
    				
    					While($wh=mysql_fetch_array($wh_result))
    					{
                			$str_part_no = is_null($wh['wel_part_no'])? '' : trim($wh['wel_part_no']);
                			$str_wh_code = is_null($wh['wel_wh_code'])? '' : trim($wh['wel_wh_code']);

							$dec_close = 0;
	                		// Calc Trans. total-in
							$sql =	"SELECT sum(wel_tran_qty) as sum_tran_qty, sum(wel_spar_qty) as sum_spar_qty ".
									"FROM #__wel_tranflm ".
									"WHERE wel_part_no='{$str_part_no}' AND wel_wh_to='{$str_wh_code}' ".
									"AND wel_tran_date>'{$sys_phy_date}' AND wel_tran_date<='{$this->wel_tran_date}'";
							$sql=revert_to_the_available_sql($sql);
    						if(!$tf_result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
    	            		If($tf=mysql_fetch_array($tf_result))
    	            		{
                				$dec_tran_qty = doubleval(is_null($tf['sum_tran_qty'])? 0 : $tf['sum_tran_qty']);
                        		$dec_spar_qty = doubleval(is_null($tf['sum_spar_qty'])? 0 : $tf['sum_spar_qty']);

                            	$dec_close = $dec_close + $dec_tran_qty + $dec_spar_qty;
    	            		}
	                		mysql_free_result($tf_result);
    	            		unset($tf);

	                		// Calc Trans. total-out
							$sql =	"SELECT sum(wel_tran_qty) as sum_tran_qty, sum(wel_spar_qty) as sum_spar_qty ".
									"FROM #__wel_tranflm ".
									"WHERE wel_part_no='{$str_part_no}' AND wel_wh_fm='{$str_wh_code}' ".
									"AND wel_tran_date>'{$sys_phy_date}' AND wel_tran_date<='{$this->wel_tran_date}'";
							$sql=revert_to_the_available_sql($sql);
    						if(!$tf_result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
    	            		If($tf=mysql_fetch_array($tf_result))
    	            		{
                				$dec_tran_qty = doubleval(is_null($tf['sum_tran_qty'])? 0 : $tf['sum_tran_qty']);
                        		$dec_spar_qty = doubleval(is_null($tf['sum_spar_qty'])? 0 : $tf['sum_spar_qty']);

                            	$dec_close = $dec_close - $dec_tran_qty - $dec_spar_qty;
    	            		}
	                		mysql_free_result($tf_result);
    	            		unset($tf);

                			$sql =	"SELECT * FROM #__wel_partwhm ".
                					"WHERE wel_part_no ='{$str_part_no}' AND wel_wh_code ='{$str_wh_code}' limit 1";
							$sql=revert_to_the_available_sql($sql);
                			if(!$ph_result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
    	            		If($ph=mysql_fetch_array($ph_result))
                			{
								IF ($sys_phy_qty=1)
								{
                        			$dec_stk_qty = is_null($ph['wel_phy_qty'])? 0 : $ph['wel_phy_qty'];
								}Else{
									$dec_stk_qty = is_null($ph['wel_period2'])? 0 : $ph['wel_period2'];
								}

                            	$dec_close = $dec_close + $dec_stk_qty;

								IF ($dec_close > 0)
								{
                					$sql =	"UPDATE #__wel_partwhm SET ".
		                        			"wel_period1 = '{$dec_close}' ".
		                        			"WHERE wel_part_no ='{$str_part_no}' AND wel_wh_code ='{$str_wh_code}'";
									$sql=revert_to_the_available_sql($sql);
		   							if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
		   						}
                			}Else{
								IF ($dec_close > 0)
								{
		                        	$sql =	"INSERT INTO #__wel_partwhm SET ".
			                        		"wel_part_no = '".strtoupper($str_part_no)."',".
			                        		"wel_wh_code = '".strtoupper($str_wh_code)."',".
		    	                    		"wel_period1 = '{$dec_close}',".
		        	                		"wel_phy_qty = 0,".
		            	            		"wel_creat_d = '".date('Y-m-d')."'";
									$sql=revert_to_the_available_sql($sql);
		   							if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
		   						}
                			}
    					}
    				
   						// Complete Calculate Closing Quantity......"

						$sql =	"SELECT wel_part_no, sum(IFNULL(wel_period1,0)) AS sum_stk_qty ".
		    					"FROM #__wel_partwhm ".
		    					"GROUP BY wel_part_no ORDER BY wel_part_no";
						$sql=revert_to_the_available_sql($sql);
    					if(!$cst_result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
    					While($cst=mysql_fetch_array($cst_result))
    					{
	        				$str_part_no = is_null($cst['wel_part_no'])? '' : $cst['wel_part_no'];
    	    				$dec_sum_stk = is_null($cst['sum_stk_qty'])? 0 : $cst['sum_stk_qty'];

							IF ($dec_sum_stk > 0)
							{
		        				$dec_avg_cost = 0;
       							$dec_std_cost = 0;
		        				$dec_lat_cost = 0;
       							$dec_vqt_cost = 0;

								$sql =	"SELECT * FROM #__wel_partflm ".
		    							"WHERE wel_part_no='{$str_part_no}' limit 1";
								$sql=revert_to_the_available_sql($sql);
    							if(!$part_result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
	    	            		If($part=mysql_fetch_array($part_result))
	    	            		{
			        				$dec_avg_cost = is_null($part['wel_avg_cost'])? 0 : $part['wel_avg_cost'];
        							$dec_std_cost = is_null($part['wel_std_cost'])? 0 : $part['wel_std_cost'];
			        				$dec_lat_cost = is_null($part['wel_lat_cost'])? 0 : $part['wel_lat_cost'];
        							$dec_vqt_cost = is_null($part['wel_vqt_cost'])? 0 : $part['wel_vqt_cost'];
        						}

	                			$dec_fif_cost = 0;
    	            			$dec_remain = $dec_sum_stk;
        	        			$dec_total_fifo_amount = 0;

            	    			$int_trn_no = 0;
	                			$str_count = "AAA";
    	           				While($str_count == "AAA")
        	       				{
									if( $int_trn_no == 0)
									{
	                					$sql="SELECT * FROM #__wel_tranflm".
	                    	   				" WHERE wel_part_no='{$str_part_no}' AND wel_tran_date<='{$wel_phy_date}' ".
	                       					" AND wel_flow_code='REC' ".
	                       					" ORDER BY wel_trn_recno DESC,wel_crt_date DESC limit 1";
									}else{
	                					$sql="SELECT * FROM #__wel_tranflm".
	                       					" WHERE wel_part_no='{$str_part_no}' AND wel_tran_date<='{$wel_phy_date}' ".
	                       					" AND wel_trn_recno < {$int_trn_no} AND wel_flow_code='REC' ".
	                       					" ORDER BY wel_trn_recno DESC,wel_crt_date DESC limit 1";
									}
									$sql=revert_to_the_available_sql($sql);
                					if(!$tf_result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				   					If(!$tranflm=mysql_fetch_array($tf_result))
				   					{
										// Exit loop
		               					$str_count = "BBB";
				   					}else{
                    					$trn_tran_qty = doubleval(is_null($tranflm['wel_tran_qty'])?  0 : $tranflm['wel_tran_qty']);
                    					$trn_tran_date = is_null($tranflm['wel_tran_date'])? null : $tranflm['wel_tran_date'];
                    					$trn_po_no = $tranflm['wel_po_no'];
                    					$trn_po_line = $tranflm['wel_po_line'];

										$po_cur_code = "";
										$po_ex_rate = 1;
										if($trn_po_no!="")
										{
		                					$sql="SELECT * FROM #__wel_porhdrm ".
			                       				" WHERE wel_po_no='{$trn_po_no}' limit 1";
											$sql=revert_to_the_available_sql($sql);
        	        						if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
							   				If($row=mysql_fetch_array($result))
							   				{
		            	        				$po_cur_code = $row['wel_cur_code'];
		                	    				$po_ex_rate = $row['wel_ex_rate'];
						   					}
										}

										$po_u_price = 0;
										$po_pur_unit_rate = 1;
										if($trn_po_no!="" && trn_po_line > 0)
										{
		        	        				$sql="SELECT * FROM #__wel_pordetm ".
		            	           				" WHERE wel_po_no='{$trn_po_no}' AND wel_po_line='{$trn_po_line}' limit 1";
											$sql=revert_to_the_available_sql($sql);
                							if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
						   					If($row=mysql_fetch_array($result))
						   					{
		                    					$po_u_price = $row['wel_u_price'];
												$po_pur_unit_rate=doubleval(is_null($row["wel_pur_unit_rate"]) ? 1 : $row["wel_pur_unit_rate"]);
							   				}
										}

										//货币兑换率
										$int_year = date('Y',strtotime($trn_tran_date));
										$int_month = date('m',strtotime($trn_tran_date));
										if(strlen($int_month) == 1)
										{
											$int_month = '0'.$int_month;
										}
										$int_yyyymm = strval($int_year.$int_month);

										$sql = "SELECT * FROM #__wel_curtabh WHERE wel_cur_code='".$po_cur_code."' AND wel_yyyy_mm='".$int_yyyymm."' LIMIT 1";
										$sql=revert_to_the_available_sql($sql);
										if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
										if($row=mysql_fetch_array($result))
										{
											$po_ex_rate = doubleval(is_null($row['wel_ex_rate']) ? 1 : $row['wel_ex_rate']);
										}	

										// Total Amount Calculation
    	                				If($dec_tran_qty >= $dec_remain)
        	            				{
            	                			//$dec_total_fifo_amount = $dec_total_fifo_amount + $dec_remain * $dec_u_price * $dec_ex_rate * $dec_unit_exchange_rate;
                	            			$dec_total_fifo_amount = $dec_total_fifo_amount + $dec_remain  * $po_ex_rate * ($po_u_price / $po_pur_unit_rate);
                    	        			$dec_remain = 0;
											// Exit loop
			               					$str_count = "BBB";
    	                				}	
        	            				Else
            	        				{
                	            			$dec_remain = $dec_remain - $dec_tran_qty;
	                            			//$dec_total_fifo_amount = $dec_total_fifo_amount + $dec_tran_qty * $dec_u_price * $dec_ex_rate * $dec_unit_exchange_rate;
	                            			$dec_total_fifo_amount = $dec_total_fifo_amount + $dec_tran_qty * $po_ex_rate * ($po_u_price / $po_pur_unit_rate);
    	                				}
				   					}
                					mysql_free_result($tf_result);
                					unset($tranflm);
               					}

	                			$dec_fif_cost = $dec_total_fifo_amount / $dec_sum_stk ;
    	           				$sql="SELECT * FROM #__wel_partwcm ".
        	       					" WHERE wel_part_no='{$str_part_no}' AND wel_cut_date ='{$this->wel_tran_date}' limit 1";
								$sql=revert_to_the_available_sql($sql);
	                			if(!$pw_result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
    	            			If(!$partwcm=mysql_fetch_array($pw_result))
        	        			{
	                    			$sql="INSERT INTO #__wel_partwcm set ".
			                    		" wel_part_no = '".strtoupper($str_part_no)."',".
			                    		" wel_cut_date = '{$this->wel_tran_date}',".
		    	                		" wel_fif_cost = '{$dec_fif_cost}',".
		        	            		" wel_avg_cost = '{$dec_avg_cost}',".
			                    		" wel_std_cost = '{$dec_std_cost}',".
			                    		" wel_lat_cost = '{$dec_lat_cost}',".
			                    		" wel_vqt_cost = '{$dec_vqt_cost}' ";
									$sql=revert_to_the_available_sql($sql);
									if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
	                   			}Else{
	                    			$sql="UPDATE #__wel_partwcm set ".
		                				" wel_fif_cost = '{$dec_fif_cost}',".
		                    			" wel_avg_cost = '{$dec_avg_cost}',".
		                    			" wel_std_cost = '{$dec_std_cost}',".
	    	                			" wel_lat_cost = '{$dec_lat_cost}',".
	        	            			" wel_vqt_cost = '{$dec_vqt_cost}' ".
                	    				" WHERE wel_part_no='{$str_part_no}' AND wel_cut_date ='{$this->wel_tran_date}'";
									$sql=revert_to_the_available_sql($sql);
									if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
	            				}
								mysql_free_result($pw_result);
        	       				unset($partwcm);
	           				}
						}

				   	mysql_query('commit');
				}
				catch (Exception $e1)
				{
					mysql_query("rollback");
					throw new Exception($e1->getMessage());
				}

			}
			catch (Exception $e)
			{
				$msg_code=$e->getMessage();
			}
			if($msg_code==""){$msg_code="confirm_succee";}
			$return_val["msg_code"]=$msg_code;
			return $return_val;
		}
	}
?>