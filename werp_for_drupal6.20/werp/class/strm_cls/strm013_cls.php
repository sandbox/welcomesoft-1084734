<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>

<?php 
	class strm013_cls
	{
		public $wel_tran_date;
		public $wel_wh_code;
		public $wel_part_no;
		public $wel_lot_no;
		public $wel_tran_qty;
		public $wel_ref_no;
		public $wel_tran_rmk;
		
		private $wel_prog_code="strm013";
		
		public function confirm()
		{
			$msg_code="";
			$return_val=array();
			
			try
			{
				$conn=werp_db_connect();
				
				if (!check_permission($conn,$this->wel_prog_code,"wel_access_edit")){throw new Exception("unauthorized_access");}
				
				if($this->wel_wh_code==""){throw new Exception("wel_wh_code_miss");}
				if($this->wel_tran_date==""){throw new Exception("wel_tran_date_miss");}
				if($this->wel_part_no==""){throw new Exception("wel_part_no_miss");}

				if(!is_numeric($this->wel_tran_qty)) {throw new Exception("wel_tran_qty_miss");}
				$this->wel_tran_qty=doubleval($this->wel_tran_qty);
				if($this->wel_tran_qty==0){throw new Exception("wel_tran_qty_miss");}

				//检测货仓
				if($this->wel_wh_code!="")
				{
					$sql="SELECT * FROM #__wel_whlocfm WHERE wel_wh_code='".$this->wel_wh_code."' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
					if(!($row=mysql_fetch_array($result))){throw new Exception("wel_wh_code_not_found");}	
					$wh_wh_dum=intval(is_null($row["wel_wh_dum"]) ? 0 : $row["wel_wh_dum"]);

					if($wh_wh_dum==1){throw new Exception("wel_wh_code_not_found");}
				}

				if($this->wel_tran_date!="")
				{
					$this->wel_tran_date=(($this->wel_tran_date=="") ? "null" : $this->wel_tran_date);
					$sql="SELECT * FROM #__wel_closedm LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());} 
					if(!($row=mysql_fetch_array($result))){throw new Exception("system_init_error");}
					$sys_phy_date=is_null($row["wel_period1"]) ? $row["wel_phy_date"] : $row["wel_period1"];

					if( date("Y-m-d",strtotime($sys_phy_date)) >= date("Y-m-d",strtotime($this->wel_tran_date)) )
					{
						throw new Exception("wel_tran_date_error");
					}
				}

				if($this->wel_part_no!="")
				{
					$sql="SELECT * FROM #__wel_partflm WHERE wel_part_no='".$this->wel_part_no."' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
					if(!($row=mysql_fetch_array($result))){throw new Exception("wel_part_no_not_found");}	
					$part_cat_code=is_null($row["wel_cat_code"]) ? "" : $row["wel_cat_code"];
					$part_std_cost=doubleval(is_null($row["wel_std_cost"]) ? 0 : $row["wel_std_cost"]);
					$part_avg_cost=doubleval(is_null($row["wel_avg_cost"])? 0 : $row["wel_avg_cost"]);
				}

				$dec_stk_qty=0;
				$sql="SELECT * FROM #__wel_whlotfm ".
					" WHERE wel_part_no='".$this->wel_part_no."' AND wel_wh_code='".$this->wel_wh_code."' AND wel_lot_no='".$this->wel_lot_no."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				if( $row=mysql_fetch_array($result) )
				{
					$dec_stk_qty=doubleval(is_null($row["wel_stk_qty"]) ? 0 : $row["wel_stk_qty"]);
				}else{
					$dec_stk_qty=0;
				}

				if($dec_stk_qty+$this->wel_tran_qty<0){throw new Exception("wel_tran_qty_error");}		
				
				try
				{
					mysql_query("begin");
				
						$sql="SELECT wel_trn_recno FROM #__wel_tranflm ORDER BY wel_trn_recno DESC LIMIT 1";
						$sql=revert_to_the_available_sql($sql);
						if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
						if(!($row=mysql_fetch_array($result)))
						{
							$dec__next_trn_recno=1;
						}
						else
						{
							$dec__next_trn_recno=doubleval(is_null($row["wel_trn_recno"]) ? 1 : $row["wel_trn_recno"]+1);
						}

						$sql="INSERT INTO #__wel_tranflm SET ".           //Transaction File Add 17 Field
								"wel_trn_recno=".$dec__next_trn_recno.",".
								"wel_wh_to='".$this->wel_wh_code."',".
								"wel_tran_date='".$this->wel_tran_date."',".
								"wel_tran_code='ADJT',".
								"wel_flow_code='ADJ',".
								"wel_part_no='".$this->wel_part_no."',".
								"wel_cat_code='".$part_cat_code."',".
								"wel_lot_no_to='".$this->wel_lot_no."',".
								"wel_tran_qty=".$this->wel_tran_qty.",".
								"wel_ref_no='".$this->wel_ref_no."',".
								"wel_tran_rmk='".$this->wel_tran_rmk."',".
								"wel_std_cost=".$part_std_cost.",".
								"wel_avg_cost=".$part_avg_cost.",".
								"wel_crt_user='{$_SESSION['wel_user_id']}', ".
								"wel_crt_date=now()";
						$sql=revert_to_the_available_sql($sql);
						if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}

						$sql="SELECT * FROM #__wel_whlotfm ".
							" WHERE wel_wh_code='".$this->wel_wh_code."' AND wel_part_no='".$this->wel_part_no."' ".
							" AND wel_lot_no='".$this->wel_lot_no."' AND wel_stk_loc='' LIMIT 1";
						$sql=revert_to_the_available_sql($sql);
						if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
						if($row=mysql_fetch_array($result))
						{
							$sql="UPDATE #__wel_whlotfm SET ".
								" wel_stk_qty=IFNULL(wel_stk_qty,0)+$this->wel_tran_qty ".
								" WHERE wel_wh_code='".$this->wel_wh_code."' AND wel_part_no='".$this->wel_part_no."' ".
								" AND wel_lot_no='".$this->wel_lot_no."' AND wel_stk_loc='' LIMIT 1";
							$sql=revert_to_the_available_sql($sql);
							if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
						}else{
							$sql="INSERT INTO #__wel_whlotfm SET ".
									"wel_wh_code='".$this->wel_wh_code."',".
									"wel_part_no='".$this->wel_part_no."',".
									"wel_lot_no='".$this->wel_lot_no."',".
									"wel_stk_loc='',".
									"wel_stk_qty=".$this->wel_tran_qty.",".
									"wel_crt_user='{$_SESSION['wel_user_id']}', ".
									"wel_crt_date=now()";
							$sql=revert_to_the_available_sql($sql);
							if(!($result1=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
						}

						mysql_free_result($result);

					mysql_query("commit");
					
				}
				catch (Exception $e1)
				{
					mysql_query("rollback");
					throw new Exception($e1->getMessage());
				}	
					
			}
			catch (Exception $e)
			{
				$msg_code=$e->getMessage();
			}
			if($msg_code==""){$msg_code="confirm_succee";}
			$return_val["msg_code"]=$msg_code;
			return $return_val;	
		}
	}
?>