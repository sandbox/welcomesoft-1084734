<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>

<?php 
	class strm017_cls
	{
		public $wel_ctrl_flow="";
		public $wel_wh_fm="";
		public $wel_wh_to="";
		public $wel_ctrl_rmk1="";
		public $wel_ctrl_rmk2="";
		public $wel_ctrl_rmk3="";
		public $wel_rec_flow="";
		public $wel_vrt_flow="";
		public $wel_int_flow="";
		public $wel_iss_flow="";
		public $wel_rtn_flow="";
		public $wel_fgs_flow="";
		public $wel_shp_flow="";
		public $wel_crt_flow="";
		public $wel_avg_cflow="";
		public $wel_dis_able="";
		
		private $wel_prog_code="strm017";
		//读取货仓控制
		public function read()
		{
			$msg_code="";
			$return_val=array();
			
			try
			{
				$conn=werp_db_connect();
				
				if (!check_permission($conn,$this->wel_prog_code,"wel_access_read")){throw new Exception("unauthorized_access");}
				
				if($this->wel_ctrl_flow==""){throw new Exception("wel_ctrl_flow_miss");}

				$sql="SELECT h.*,".
					" a.wel_wh_des AS wel_wh_fm_des,".
					" b.wel_wh_des AS wel_wh_to_des".
					" FROM #__wel_whctrlm h".
					" LEFT JOIN #__wel_whlocfm a ON h.wel_wh_fm=a.wel_wh_code".
					" LEFT JOIN #__wel_whlocfm b ON h.wel_wh_to=b.wel_wh_code".
					" WHERE h.wel_ctrl_flow='".$this->wel_ctrl_flow."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_ctrl_flow_not_found");}	
				$int__count=0;
				while ($int__count < mysql_num_fields($result)) 
				{
					$field_name=mysql_fetch_field($result,$int__count)->name;
					$return_val[$field_name]=$row[$field_name];
					$int__count++;
				}
				mysql_free_result($result);
				throw new Exception("");
			}
			catch (Exception $e)
			{
				$msg_code=$e->getMessage();
			}
			$return_val["msg_code"]=$msg_code;
			return $return_val;
		}
		//新增货仓控制
		public function addnew()
		{
			$msg_code="";
			$return_val=array();
			
			try
			{
				$conn=werp_db_connect();
				
				if (!check_permission($conn,$this->wel_prog_code,"wel_access_addnew")){throw new Exception("unauthorized_access");}
				
				if($this->wel_ctrl_flow==""){throw new Exception("wel_ctrl_flow_miss");}
				if($this->wel_wh_fm==""){throw new Exception("wel_wh_fm_miss");}
				if($this->wel_wh_to==""){throw new Exception("wel_wh_to_miss");}
				
				if($this->wel_wh_fm!="")
				{
					$sql="SELECT * FROM #__wel_whlocfm WHERE wel_wh_code='".$this->wel_wh_fm."' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
					if(!$row=mysql_fetch_array($result)){throw new Exception("wel_wh_fm_not_found");}
				}
				
				if($this->wel_wh_to!="")
				{
					$sql="SELECT * FROM #__wel_whlocfm WHERE wel_wh_code='".$this->wel_wh_to."' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
					if(!$row=mysql_fetch_array($result)){throw new Exception("wel_wh_to_not_found");}
				}
				
				$tmp_check = 0;
				$tmp_check = $tmp_check + intval($this->wel_rec_flow);
				$tmp_check = $tmp_check + intval($this->wel_vrt_flow);
				$tmp_check = $tmp_check + intval($this->wel_int_flow);
				$tmp_check = $tmp_check + intval($this->wel_iss_flow);
				$tmp_check = $tmp_check + intval($this->wel_rtn_flow);
				$tmp_check = $tmp_check + intval($this->wel_fgs_flow);
				$tmp_check = $tmp_check + intval($this->wel_shp_flow);
				$tmp_check = $tmp_check + intval($this->wel_crt_flow);
				if($tmp_check==0){throw new Exception("wel_flow_type_miss");}
				if($tmp_check>1){throw new Exception("wel_flow_type_error");}

				//货仓控制是否存在
				$sql="SELECT * FROM #__wel_whctrlm WHERE wel_ctrl_flow='".$this->wel_ctrl_flow."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if($row=mysql_fetch_array($result)){throw new Exception("wel_ctrl_flow_exist");}

				try
				{
					mysql_query('begin');
						
						$sql="INSERT INTO #__wel_whctrlm SET ".
							" wel_ctrl_flow='".$this->wel_ctrl_flow."',".
							" wel_wh_fm='".$this->wel_wh_fm."',".
							" wel_wh_to='".$this->wel_wh_to."',".
							" wel_ctrl_rmk1='".$this->wel_ctrl_rmk1."',".
							" wel_ctrl_rmk2='".$this->wel_ctrl_rmk2."',".
							" wel_ctrl_rmk3='".$this->wel_ctrl_rmk3."',".
							" wel_rec_flow=".intval($this->wel_rec_flow).",".
							" wel_vrt_flow=".intval($this->wel_vrt_flow).",".
							" wel_int_flow=".intval($this->wel_int_flow).",".
							" wel_iss_flow=".intval($this->wel_iss_flow).",".
							" wel_rtn_flow=".intval($this->wel_rtn_flow).",".
							" wel_fgs_flow=".intval($this->wel_fgs_flow).",".
							" wel_shp_flow=".intval($this->wel_shp_flow).",".
							" wel_crt_flow=".intval($this->wel_crt_flow).",".
							" wel_avg_cflow=".intval($this->wel_avg_cflow).",".
							" wel_dis_able=".intval($this->wel_dis_able)."";
						$sql=revert_to_the_available_sql($sql);
						if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
						mysql_free_result($result);
						
					mysql_query('commit');
					
				}
				catch (Exception $e1)
				{
					mysql_query('rollback');
					throw new Exception($e1->getMessage());
				}
				
			}
			catch (Exception $e)
			{
				$msg_code=$e->getMessage();
			}
			if($msg_code==""){$msg_code="addnew_succee";}
			$return_val["msg_code"]=$msg_code;
			$return_val["wel_ctrl_flow"]=$this->wel_ctrl_flow;
			return $return_val;
		}
		//编辑货仓控制
		public function edit()
		{
			$msg_code="";
			$return_val=array();
			
			try
			{
				$conn=werp_db_connect();
				
				if (!check_permission($conn,$this->wel_prog_code,"wel_access_edit")){throw new Exception("unauthorized_access");}
				
				$tmp_check = 0;
				$tmp_check = $tmp_check + intval($this->wel_rec_flow);
				$tmp_check = $tmp_check + intval($this->wel_vrt_flow);
				$tmp_check = $tmp_check + intval($this->wel_int_flow);
				$tmp_check = $tmp_check + intval($this->wel_iss_flow);
				$tmp_check = $tmp_check + intval($this->wel_rtn_flow);
				$tmp_check = $tmp_check + intval($this->wel_fgs_flow);
				$tmp_check = $tmp_check + intval($this->wel_shp_flow);
				$tmp_check = $tmp_check + intval($this->wel_crt_flow);
				if($tmp_check==0){throw new Exception("wel_flow_type_miss");}
				if($tmp_check>1){throw new Exception("wel_flow_type_error");}

				//货仓控制是否存在
				$sql="SELECT * FROM #__wel_whctrlm WHERE wel_ctrl_flow='".$this->wel_ctrl_flow."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_ctrl_flow_not_found");}
				
				try
				{
					mysql_query('begin');
					
						//更新货仓控制表记录
						$sql="UPDATE #__wel_whctrlm SET ".
									"wel_ctrl_rmk1='".$this->wel_ctrl_rmk1."',".
									"wel_ctrl_rmk2='".$this->wel_ctrl_rmk2."',".
									"wel_ctrl_rmk3='".$this->wel_ctrl_rmk3."',".
									"wel_rec_flow=".intval($this->wel_rec_flow).",".
									"wel_vrt_flow=".intval($this->wel_vrt_flow).",".
									"wel_int_flow=".intval($this->wel_int_flow).",".
									"wel_iss_flow=".intval($this->wel_iss_flow).",".
									"wel_rtn_flow=".intval($this->wel_rtn_flow).",".
									"wel_fgs_flow=".intval($this->wel_fgs_flow).",".
									"wel_shp_flow=".intval($this->wel_shp_flow).",".
									"wel_crt_flow=".intval($this->wel_crt_flow).",".
									"wel_int_iqcf=".intval($this->wel_int_iqcf).",".
									"wel_avg_cflow=".intval($this->wel_avg_cflow).",".
									"wel_dis_able=".intval($this->wel_dis_able)." ".
								"WHERE wel_ctrl_flow='".$this->wel_ctrl_flow."' LIMIT 1";
						$sql=revert_to_the_available_sql($sql);
						if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
						mysql_free_result($result);
						
					mysql_query('commit');
				}
				catch (Exception $e1)
				{
					mysql_query('rollback');
					throw new Exception($e1->getMessage());
				}
				
			}
			catch (Exception $e)
			{
				$msg_code=$e->getMessage();
			}		
			if($msg_code==""){$msg_code="edit_succee";}
			$return_val["msg_code"]=$msg_code;
			return $return_val;
		}
		//删除货仓控制
		public function delete()
		{
			$msg_code="";
			$return_val=array();
			
			try
			{
				$conn=werp_db_connect();
				
				if (!check_permission($conn,$this->wel_prog_code,"wel_access_delete")){throw new Exception("unauthorized_access");}
				
				if($this->wel_ctrl_flow==""){throw new Exception("wel_ctrl_flow_miss");}
				
				//货仓控制是否存在
				$sql="SELECT * FROM #__wel_whctrlm ".
						"WHERE wel_ctrl_flow='".$this->wel_ctrl_flow."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_ctrl_flow_not_found");}
				
				try
				{
					mysql_query('begin');
					
						//删除货仓控制
						$sql="DELETE FROM #__wel_whctrlm ".
								"WHERE wel_ctrl_flow='".$this->wel_ctrl_flow."' LIMIT 1";
						$sql=revert_to_the_available_sql($sql);
						if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
						mysql_free_result($result);
						
					mysql_query('commit');
					 
				}
				catch (Exception $e1)
				{
					mysql_query('rollback');
					throw new Exception($e1->getMessage());
				}
				
			}
			catch (Exception $e)
			{
				$msg_code=$e->getMessage();
			}		
			if($msg_code==""){$msg_code="delete_succee";}
			$return_val["msg_code"]=$msg_code;
			return $return_val;
		}
	}
?>