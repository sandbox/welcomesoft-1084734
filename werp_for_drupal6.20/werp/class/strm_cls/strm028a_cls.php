<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>

<?php 
	class strm028a_cls
	{
		public $wel_to_no="";
		public $wel_to_line=0;
		public $wel_tran_date="";
		public $wel_ctrl_flow="";
		public $wel_wh_fm="";
		public $wel_wh_to="";
		public $wel_part_no="";
		public $wel_lot_no_fm="";
		public $wel_lot_no_to="";
		public $wel_tran_qty=0;
		public $wel_ref_no="";
		public $wel_to_rmk="";
		
		private $wel_prog_code="strm028";
		
		//读取内部转仓明细
		public function read()
		{
			$msg_code="";
			$return_val=array();
			
			try
			{
				$conn=werp_db_connect();
				
				if (!check_permission($conn,$this->wel_prog_code,"wel_access_read")){throw new Exception("unauthorized_access");}
				
				//内部转仓单是否存在
				$sql="SELECT d.*, h.wel_ctrl_flow,h.wel_wh_fm,h.wel_wh_to,h.wel_req_date as wel_hdr_date".
					" FROM #__wel_tordetm d".
						" LEFT JOIN #__wel_torhdrm h ON h.wel_to_no=d.wel_to_no".
					" WHERE d.wel_to_no='".$this->wel_to_no."' AND d.wel_to_line=".$this->wel_to_line." LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_to_detail_not_found");}
				$int__count=0;
				while ($int__count < mysql_num_fields($result)) 
				{
					$field_name=mysql_fetch_field($result,$int__count)->name;
					$return_val[$field_name]=$row[$field_name];
					$int__count++;
				}
				
				$tmp_part_no=is_null($row["wel_part_no"]) ? "" : $row["wel_part_no"];
				$tmp_wh_fm=is_null($row["wel_wh_fm"]) ? "" : $row["wel_wh_fm"];
				$tmp_lot_no_fm=is_null($row["wel_lot_no_fm"]) ? "" : $row["wel_lot_no_fm"];

				//物料档案
				$sql="SELECT * FROM #__wel_partflm WHERE wel_part_no='".$tmp_part_no."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
				if($row=mysql_fetch_array($result))
				{
					$return_val["wel_part_des"]=$row[wel_part_des];
					$return_val["wel_part_des1"]=$row[wel_part_des1];
					$return_val["wel_part_des2"]=$row[wel_part_des2];
					$return_val["wel_part_des3"]=$row[wel_part_des3];
					$return_val["wel_part_des4"]=$row[wel_part_des4];
					$return_val["wel_unit"]=$row[wel_unit];
				}else{
					$return_val["wel_part_des"]="";
					$return_val["wel_part_des1"]="";
					$return_val["wel_part_des2"]="";
					$return_val["wel_part_des3"]="";
					$return_val["wel_part_des4"]="";
					$return_val["wel_unit"]="";
				}

				//物品仓存数
				$sql="SELECT wel_stk_qty FROM #__wel_whlotfm WHERE wel_wh_code='".$tmp_wh_fm."' AND wel_part_no='".$tmp_part_no."' AND wel_lot_no='".$tmp_lot_no_fm."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
				if($row=mysql_fetch_array($result))
				{
					$return_val["wel_stk_qty"]=$row["wel_stk_qty"];
				}else{
					$return_val["wel_stk_qty"]=0;
				}

				mysql_free_result($result);
			}
			catch (Exception $e)
			{
				$msg_code=$e->getMessage();
			}
			$return_val["msg_code"]=$msg_code;
			return $return_val;
		}
		
		//内部转仓明细过账
		public function posted()
		{
			$msg_code="";
			$return_val=array();
			
			try
			{
				$conn=werp_db_connect();
				
				if (!check_permission($conn,$this->wel_prog_code,"wel_access_addnew")){throw new Exception("unauthorized_access");}
				
				if($this->wel_ctrl_flow==""){throw new Exception("wel_ctrl_flow_miss");}
				if($this->wel_to_no==""){throw new Exception("wel_to_no_miss");}
				if($this->wel_req_date==""){throw new Exception("wel_req_date_miss");}

				if(!is_numeric($this->wel_to_line)) {throw new Exception("wel_to_line_miss");}
				$this->wel_to_line=doubleval($this->wel_to_line);
				if($this->wel_to_line==0){throw new Exception("wel_to_line_miss");}

				if(!is_numeric($this->wel_tran_qty)) {throw new Exception("wel_tran_qty_miss");}
				$this->wel_tran_qty=doubleval($this->wel_tran_qty);
				if($this->wel_tran_qty==0){throw new Exception("wel_tran_qty_miss");}

				if($this->wel_ctrl_flow!="")
				{
					$sql="SELECT * FROM #__wel_whctrlm WHERE wel_ctrl_flow='".$this->wel_ctrl_flow."' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
					if(!($row=mysql_fetch_array($result))){throw new Exception("wel_whctrlm_not_found");}	
					$str__wh_fm=is_null($row["wel_wh_fm"]) ? "" : $row["wel_wh_fm"];
					$str__wh_to=is_null($row["wel_wh_to"]) ? "" : $row["wel_wh_to"];
					$int__dis_able=intval(is_null($row["wel_dis_able"]) ? 0 : $row["wel_dis_able"]);
					$int__int_flow=intval(is_null($row["wel_int_flow"]) ? 0 : $row["wel_int_flow"]);

					if($int__int_flow!=1){throw new Exception("wel_whctrlm_not_int_flow");}
					if($int__dis_able==1){throw new Exception("wel_whctrlm_disable");}

					$sql="SELECT * FROM #__wel_whlocfm WHERE wel_wh_code='".$str__wh_fm."' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
					if(!($row=mysql_fetch_array($result))){throw new Exception("wel_wh_fm_not_found");}	
				
					$sql="SELECT * FROM #__wel_whlocfm WHERE wel_wh_code='".$str__wh_to."' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
					if(!($row=mysql_fetch_array($result))){throw new Exception("wel_wh_to_not_found");}	
				}

				if($this->wel_req_date!="")
				{
					$this->wel_req_date=(($this->wel_req_date=="") ? "null" : $this->wel_req_date);
					$sql="SELECT * FROM #__wel_closedm LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());} 
					if(!($row=mysql_fetch_array($result))){throw new Exception("system_init_error");}
					$sys_phy_date=is_null($row["wel_period1"]) ? $row["wel_phy_date"] : $row["wel_period1"];

					if( date("Y-m-d",strtotime($sys_phy_date)) >= date("Y-m-d",strtotime($this->wel_req_date)) )
					{
						throw new Exception("wel_req_date_error");
					}
				}

				$sql="SELECT * FROM #__wel_tordetm ".
						"WHERE wel_to_no='".$this->wel_to_no."' AND wel_to_line=".$this->wel_to_line." LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_to_detail_not_found");}
				$to_part_no=is_null($row["wel_part_no"]) ? "" : $row["wel_part_no"];
				$to_req_qty=ROUND(doubleval(is_null($row["wel_req_qty"]) ? 0 : $row["wel_req_qty"]),4);
				$to_post_qty=ROUND(doubleval(is_null($row["wel_post_qty"]) ? 0 : $row["wel_post_qty"]),4);

				//品号是否存在
				$sql="SELECT * FROM #__wel_partflm WHERE wel_part_no='".$to_part_no."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_part_no_not_found");}
				$part_cat_code=is_null($row["wel_cat_code"]) ? "" : $row["wel_cat_code"];
				$part_std_cost=doubleval(is_null($row["wel_std_cost"]) ? 0 : $row["wel_std_cost"]);
				$part_avg_cost=doubleval(is_null($row["wel_avg_cost"]) ? 0 : $row["wel_avg_cost"]);
				
				if($this->wel_tran_qty<0)
				{
					if($to_post_qty+$this->wel_tran_qty < 0){throw new Exception("wel_tran_qty_excess_post");}

					$stk_stk_qty = 0;
					$sql="SELECT * FROM #__wel_whlotfm ".
						" WHERE wel_wh_code='".$str__wh_to."' AND wel_part_no='".$to_part_no."' ".
						" AND wel_lot_no='".$this->wel_lot_no_to."' AND wel_stk_loc='' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
					if($row=mysql_fetch_array($result))
					{
						$stk_stk_qty=doubleval(is_null($row["wel_stk_qty"]) ? 0 : $row["wel_stk_qty"]);
					}

					if($stk_stk_qty+$this->wel_tran_qty<0){throw new Exception("wel_tran_qty_excess_stock");}

				}else{
					if($to_req_qty-$to_post_qty-$this->wel_tran_qty < 0){throw new Exception("wel_tran_qty_excess_ito");}

					//物品仓存数
					$stk_stk_qty = 0;
					$sql="SELECT * FROM #__wel_whlotfm ".
						" WHERE wel_wh_code='".$str__wh_fm."' AND wel_part_no='".$to_part_no."' ".
						" AND wel_lot_no='".$this->wel_lot_no_fm."' AND wel_stk_loc='' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
					if($row=mysql_fetch_array($result))
					{
						$stk_stk_qty=doubleval(is_null($row["wel_stk_qty"]) ? 0 : $row["wel_stk_qty"]);
					}

					if($stk_stk_qty-$this->wel_tran_qty<0){throw new Exception("wel_tran_qty_excess_stock");}
				}

				try
				{
					mysql_query('begin');
						
						//获得最新交易记录号
						$sql="SELECT wel_trn_recno FROM #__wel_tranflm ORDER BY wel_trn_recno DESC LIMIT 1";
						$sql=revert_to_the_available_sql($sql);
						if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
						if(!($row=mysql_fetch_array($result)))
						{
							$dec__next_trn_recno=1;
						}
						else
						{
							$dec__next_trn_recno=doubleval(is_null($row["wel_trn_recno"]) ? 1 : $row["wel_trn_recno"]+1);
						}

						//添加记录到tranflm表
						$sql="INSERT INTO #__wel_tranflm SET ".
								"wel_trn_recno=".$dec__next_trn_recno.",".
								"wel_tran_date='".$this->wel_req_date."',".
								"wel_wh_fm='".$str__wh_fm."',".
								"wel_wh_to='".$str__wh_to."',".
								"wel_tran_code='".$this->wel_ctrl_flow."',".
								"wel_flow_code='INT',".
								"wel_to_no='".$this->wel_to_no."',".
								"wel_part_no='".$to_part_no."',".
								"wel_cat_code='".$part_cat_code."',".
								"wel_lot_no_fm='".$this->wel_lot_no_fm."',".
								"wel_lot_no_to='".$this->wel_lot_no_to."',".
								"wel_tran_qty=".$this->wel_tran_qty.",".
								"wel_ref_no='".$this->wel_ref_no."',".
								"wel_tran_rmk='".$this->wel_to_rmk."',".
								"wel_crt_user='{$_SESSION['wel_user_id']}', ".
								"wel_crt_date=now()";
						$sql=revert_to_the_available_sql($sql);
						if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}

						//更新物品仓存数
						$sql="SELECT * FROM #__wel_whlotfm ".
							" WHERE wel_wh_code='".$str__wh_fm."' AND wel_part_no='".$to_part_no."' ".
							" AND wel_lot_no='".$this->wel_lot_no_fm."' AND wel_stk_loc='' LIMIT 1";
						$sql=revert_to_the_available_sql($sql);
						if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
						if($row=mysql_fetch_array($result))
						{
							$sql="UPDATE #__wel_whlotfm SET ".
								" wel_stk_qty=IFNULL(wel_stk_qty,0)-$this->wel_tran_qty ".
								" WHERE wel_wh_code='".$str__wh_fm."' AND wel_part_no='".$to_part_no."' ".
								" AND wel_lot_no='".$this->wel_lot_no_fm."' AND wel_stk_loc='' LIMIT 1";
							$sql=revert_to_the_available_sql($sql);
							if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
						}else{
							$sql="INSERT INTO #__wel_whlotfm SET ".
									"wel_wh_code='".$str__wh_fm."',".
									"wel_part_no='".$to_part_no."',".
									"wel_lot_no='".$this->wel_lot_no_fm."',".
									"wel_stk_loc='',".
									"wel_stk_qty=".-1*$this->wel_tran_qty.",".
									"wel_crt_user='{$_SESSION['wel_user_id']}', ".
									"wel_crt_date=now()";
							$sql=revert_to_the_available_sql($sql);
							if(!($result1=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
						}

						$sql="SELECT * FROM #__wel_whlotfm ".
							" WHERE wel_wh_code='".$str__wh_to."' AND wel_part_no='".$to_part_no."' ".
							" AND wel_lot_no='".$this->wel_lot_no_to."' AND wel_stk_loc='' LIMIT 1";
						$sql=revert_to_the_available_sql($sql);
						if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
						if($row=mysql_fetch_array($result))
						{
							$sql="UPDATE #__wel_whlotfm SET ".
								" wel_stk_qty=IFNULL(wel_stk_qty,0)+$this->wel_tran_qty ".
								" WHERE wel_wh_code='".$str__wh_to."' AND wel_part_no='".$to_part_no."' ".
								" AND wel_lot_no='".$this->wel_lot_no_to."' AND wel_stk_loc='' LIMIT 1";
							$sql=revert_to_the_available_sql($sql);
							if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
						}else{
							$sql="INSERT INTO #__wel_whlotfm SET ".
									"wel_wh_code='".$str__wh_to."',".
									"wel_part_no='".$to_part_no."',".
									"wel_lot_no='".$this->wel_lot_no_to."',".
									"wel_stk_loc='',".
									"wel_stk_qty=".$this->wel_tran_qty.",".
									"wel_crt_user='{$_SESSION['wel_user_id']}', ".
									"wel_crt_date=now()";
							$sql=revert_to_the_available_sql($sql);
							if(!($result1=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
						}

						//更新内部转仓单明细
						$sql="UPDATE #__wel_tordetm SET ".
							" wel_post_qty=IFNULL(wel_post_qty,0)+$this->wel_tran_qty".
							" WHERE wel_to_no='".$this->wel_to_no."' ".
							" AND wel_to_line=".$this->wel_to_line." LIMIT 1";
						$sql=revert_to_the_available_sql($sql);
						if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}

						mysql_free_result($result);

					mysql_query('commit');
					
				}
				catch(Exception $e1)
				{
					mysql_query('rollback');
					throw new Exception($e1->getMessage());
				}
				
			}
			catch(Exception $e)
			{
				$msg_code=$e->getMessage();
			}
			if($msg_code==""){$msg_code="addnew_succee";}	
			$return_val["msg_code"]=$msg_code;
			return $return_val;
			
		}
		
	}
?>