<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>

<?php 
	class strm050_cls
	{
		public $wel_pt_no="";
		public $wel_pattern="";
		public $wel_req_date="";
		public $wel_ven_code="";
		public $wel_pt_remark="";
		
		private $wel_prog_code="strm050";
		
		//读取收货单
		public function read()
		{
			$msg_code="";
			$return_val=array();
			
			try
			{
				$conn=werp_db_connect();
				
				if (!check_permission($conn,$this->wel_prog_code,"wel_access_read")){throw new Exception("unauthorized_access");}
				
				$sql="SELECT h.*, ".
						"v.wel_ven_des, ".
						"v.wel_ven_des1, ".
						"v.wel_abbre_des, ".
						"v.wel_ven_add1, ".
						"v.wel_ven_add2, ".
						"v.wel_ven_add3, ".
						"v.wel_ven_add4, ".
						"v.wel_ven_email, ".
						"v.wel_ven_cont, ".
						"v.wel_ven_tele, ".
						"v.wel_ven_tele1, ".
						"v.wel_ven_fax, ".
						"v.wel_ven_fax1 ".
						"FROM #__wel_ptrhdrm h ".
							"LEFT JOIN #__wel_venmasm v ON h.wel_ven_code=v.wel_ven_code ".
						"WHERE h.wel_pt_no='".$this->wel_pt_no."' AND h.wel_order_type='PB' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_pt_no_not_found");}	
				$int__count=0;
				while ($int__count < mysql_num_fields($result)) 
				{
					$field_name=mysql_fetch_field($result,$int__count)->name;
					$return_val[$field_name]=$row[$field_name];
					$int__count++;
				}
				mysql_free_result($result);
				
			}
			catch (Exception $e)
			{
				$msg_code=$e->getMessage();
			}
			$return_val["msg_code"]=$msg_code;
			return $return_val;
		}
		

		//新增收货单
		public function addnew()
		{
			$msg_code="";
			$return_val=array();
			
			try
			{
				$conn=werp_db_connect();
				
				if (!check_permission($conn,$this->wel_prog_code,"wel_access_addnew")){throw new Exception("unauthorized_access");}
				
				if(($this->wel_pt_no=="") && ($this->wel_pattern=="")) {throw new Exception("wel_pt_no_miss");}
				if($this->wel_ven_code==""){throw new Exception("wel_ven_code_miss");}
				if($this->wel_req_date==""){throw new Exception("wel_req_date_miss");}
				
				//供应商是否存在
				if($this->wel_ven_code!="")		
				{
					$sql="SELECT * FROM #__wel_venmasm WHERE wel_ven_code='".$this->wel_ven_code."' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
					if(!$row=mysql_fetch_array($result)){throw new Exception("wel_ven_code_not_found");}
				}

				$this->wel_req_date=(($this->wel_req_date=="") ? "null" : $this->wel_req_date);
				$sql="SELECT * FROM #__wel_closedm LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());} 
				if(!($row=mysql_fetch_array($result))){throw new Exception("system_init_error");}
				$sys_phy_date=is_null($row["wel_period1"]) ? $row["wel_phy_date"] : $row["wel_period1"];

				if( date("Y-m-d",strtotime($sys_phy_date)) >= date("Y-m-d",strtotime($this->wel_req_date)) )
				{
					throw new Exception("wel_req_date_error");
				}
				
				if($this->wel_pattern!="")
				{
					$sql="SELECT wel_pattern FROM #__wel_rtvnote WHERE wel_pattern='".$this->wel_pattern."' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
					if(!($row=mysql_fetch_array($result))){throw new Exception("wel_pattern_not_found");}
				}

				try
				{
					mysql_query("begin");
					
					if ($this->wel_pattern!="")
					{
						$sql="SELECT * FROM #__wel_rtvnote WHERE wel_pattern='$this->wel_pattern' LIMIT 1";
						$sql=revert_to_the_available_sql($sql);
						if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
						if(!($row=mysql_fetch_array($result))){throw new Exception("wel_pattern_not_found");}
						$wel_pattern=stripslashes(trim($row["wel_pattern"]));
						$wel_po_nextno=trim($row["wel_po_nextno"])+1;
						$wel_po_nextno=sprintf("%'08s",$wel_po_nextno);

						if(strlen($wel_po_nextno)>8){throw new Exception(wel_pattern_overflow);}
						$this->wel_pt_no=$wel_pattern.$wel_po_nextno;

						$sql="UPDATE #__wel_rtvnote set wel_po_nextno=IFNULL(wel_po_nextno,0)+1 WHERE wel_pattern='$this->wel_pattern'";
						$sql=revert_to_the_available_sql($sql);
						if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					}

					$sql="SELECT wel_pt_no from #__wel_ptrhdrm WHERE wel_pt_no='".$this->wel_pt_no."' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
					if($row=mysql_fetch_array($result)){throw new Exception("wel_pt_no_exist");}

					$sql="INSERT INTO #__wel_ptrhdrm SET ".
							"wel_order_type='PB',".
							"wel_pt_no='$this->wel_pt_no',".
							"wel_pattern='$this->wel_pattern',".
							"wel_req_date='$this->wel_req_date',".
							"wel_ven_code='$this->wel_ven_code',".
							"wel_pt_remark='$this->wel_pt_remark',".
							"wel_crt_user='{$_SESSION['wel_user_id']}',".
							"wel_crt_date=now()";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
					mysql_free_result($result);
						
					mysql_query("commit");
				}
				catch (Exception $e1)
				{
					mysql_query("rollback");
					$msg_code=$e1->getMessage();
				}
				
			}
			catch (Exception $e)
			{
				$msg_code=$e->getMessage();
			}
			if($msg_code==""){$msg_code="addnew_succee";}
			$return_val["msg_code"]=$msg_code;
			$return_val["wel_pt_no"]=$this->wel_pt_no;
			return $return_val;
		}
		
		//编辑收货单
		public function edit()
		{
			$msg_code="";
			$return_val=array();
			
			try
			{
				$conn=werp_db_connect();
				
				if (!check_permission($conn,$this->wel_prog_code,"wel_access_edit")){throw new Exception("unauthorized_access");}
				
				if($this->wel_pt_no=="") {throw new Exception("wel_pt_no_miss");}
				if($this->wel_req_date==""){throw new Exception("wel_req_date_miss");}
				//if($this->wel_ven_code==""){throw new Exception("wel_ven_code_miss");}
				//供应商是否存在
				//if($this->wel_ven_code!="")		
				//{
				//	$sql="SELECT * FROM #__wel_venmasm WHERE wel_ven_code='".$this->wel_ven_code."' LIMIT 1";
				//	$sql=revert_to_the_available_sql($sql);
				//	if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				//	if(!$row=mysql_fetch_array($result)){throw new Exception("wel_ven_code_not_found");}
				//}
				
				$this->wel_req_date=(($this->wel_req_date=="") ? "null" : $this->wel_req_date);
				$sql="SELECT * FROM #__wel_closedm LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());} 
				if(!($row=mysql_fetch_array($result))){throw new Exception("system_init_error");}
				$sys_phy_date=is_null($row["wel_period1"]) ? $row["wel_phy_date"] : $row["wel_period1"];

				if( date("Y-m-d",strtotime($sys_phy_date)) >= date("Y-m-d",strtotime($this->wel_req_date)) )
				{
					throw new Exception("wel_req_date_error");
				}

				//收货单是否存在
				$sql="SELECT wel_pt_no FROM #__wel_ptrhdrm WHERE wel_pt_no='".$this->wel_pt_no."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_pt_no_not_found");}

				$sql="SELECT * FROM #__wel_ptrhdrm WHERE wel_pt_no='".$this->wel_pt_no."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_pt_no_not_found");}
				
				try
				{
					mysql_query("begin");
					
						//更新记录
						$sql="UPDATE #__wel_ptrhdrm SET ".
									"wel_req_date='".$this->wel_req_date."',".
									"wel_pt_remark='".$this->wel_pt_remark."',".
									"wel_upd_user='{$_SESSION['wel_user_id']}',".
									"wel_upd_date=now() ".
								"WHERE wel_pt_no='".$this->wel_pt_no."' LIMIT 1";
						$sql=revert_to_the_available_sql($sql);
						if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
						mysql_free_result($result);
						
					mysql_query("commit");
					
				}
				catch (Exception $e1)
				{
					mysql_query("rollback");
					$msg_code=$e1->getMessage();
				}
				
			}
			catch (Exception $e)
			{
				$msg_code=$e->getMessage();
			}		
			if($msg_code==""){$msg_code="edit_succee";}
			$return_val["msg_code"]=$msg_code;
			return $return_val;
		}
		
		//删除收货单
		public function delete()
		{
			$msg_code="";
			$return_val=array();
			
			try
			{
				$conn=werp_db_connect();
				
				if (!check_permission($conn,$this->wel_prog_code,"wel_access_delete")){throw new Exception("unauthorized_access");}
				
				if($this->wel_pt_no==""){throw new Exception("wel_pt_no_miss");}
				
				//收货单是否存在
				$sql="SELECT * FROM #__wel_ptrhdrm WHERE wel_pt_no='".$this->wel_pt_no."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_pt_no_not_found");}
				
				$sql="SELECT * FROM #__wel_ptrdetm WHERE wel_pt_no='".$this->wel_pt_no."'";
				$sql=revert_to_the_available_sql($sql);
				if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
				while($row=mysql_fetch_array($result))
				{
					$pt_rn_qty=doubleval(is_null($row["wel_rn_qty"]) ? 0 : $row["wel_rn_qty"]);
					if($pt_rn_qty>0){throw new Exception("wel_pt_detail_to_store");}
				}

				try
				{
					mysql_query("begin");
					
						$sql="SELECT * FROM #__wel_ptrdetm WHERE wel_pt_no='".$this->wel_pt_no."'";
						$sql=revert_to_the_available_sql($sql);
						if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
						while($row=mysql_fetch_array($result))
						{
							$pt_pt_line=doubleval(is_null($row["wel_pt_line"]) ? 0 : $row["wel_pt_line"]);
							$pt_rn_qty=doubleval(is_null($row["wel_rn_qty"]) ? 0 : $row["wel_rn_qty"]);
							$pt_rcv_qty=doubleval(is_null($row["wel_rcv_qty"]) ? 0 : $row["wel_rcv_qty"]);
							$pt_iqc_qty=doubleval(is_null($row["wel_iqc_qty"]) ? 0 : $row["wel_iqc_qty"]);
							$pt_rtv_qty=doubleval(is_null($row["wel_rtv_qty"]) ? 0 : $row["wel_rtv_qty"]);
							$pt_closed=intval(is_null($row["wel_closed"]) ? 0 : $row["wel_closed"]);
							$pt_po_no=is_null($row["wel_po_no"]) ? "" : $row["wel_po_no"];
							$pt_po_line=intval(is_null($row["wel_po_line"]) ? 0 : $row["wel_po_line"]);
							$pt_part_no=is_null($row["wel_part_no"]) ? "" : $row["wel_part_no"];

							$sql="UPDATE #__wel_pordetm SET".
								" wel_rcv_qty=IFNULL(wel_rcv_qty,0)-".$pt_rcv_qty.",".
								" wel_iqc_qty=IFNULL(wel_iqc_qty,0)-".$pt_iqc_qty.",".
								" wel_rtv_qty=IFNULL(wel_rtv_qty,0)-".$pt_rtv_qty." ".
								" WHERE wel_po_no='".$pt_po_no."' AND wel_po_line=".$pt_po_line." LIMIT 1";
							$sql=revert_to_the_available_sql($sql);
							if(!($del_result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}

							$sql="DELETE FROM #__wel_ptrdetm WHERE wel_pt_no='".$this->wel_pt_no."' AND wel_pt_line=".$pt_pt_line." LIMIT 1";
							$sql=revert_to_the_available_sql($sql);
							if(!($del_result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
						}

						//删除收货单
						$sql="delete FROM #__wel_ptrhdrm WHERE wel_pt_no='".$this->wel_pt_no."' LIMIT 1";
						$sql=revert_to_the_available_sql($sql);
						if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
						mysql_free_result($result);
						
					mysql_query("commit");
					
				}
				catch (Exception $e1)
				{
					mysql_query("rollback");
					$msg_code=$e1->getMessage();
				}
				
			}
			catch (Exception $e)
			{
				$msg_code=$e->getMessage();
			}		
			if($msg_code==""){$msg_code="delete_succee";}
			$return_val["msg_code"]=$msg_code;
			return $return_val;
		}
		
		//删除收货单明细
		public function delete_detail_tab0()
		{
			$msg_code="";
			$return_val=array();
			try
			{
				$conn=werp_db_connect();
				if (!check_permission($conn,$this->wel_prog_code,"wel_access_delete")){throw new Exception("unauthorized_access");}
				
				//收货单明细是否存在
				$sql="SELECT * FROM #__wel_ptrdetm WHERE wel_pt_no='".$this->wel_pt_no."' AND wel_pt_line=".intval($this->wel_pt_line)." LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_pt_detail_not_found");}
				$pt_rn_qty=doubleval(is_null($row["wel_rn_qty"]) ? 0 : $row["wel_rn_qty"]);
				$pt_rcv_qty=doubleval(is_null($row["wel_rcv_qty"]) ? 0 : $row["wel_rcv_qty"]);
				$pt_iqc_qty=doubleval(is_null($row["wel_iqc_qty"]) ? 0 : $row["wel_iqc_qty"]);
				$pt_rtv_qty=doubleval(is_null($row["wel_rtv_qty"]) ? 0 : $row["wel_rtv_qty"]);
				$pt_closed=intval(is_null($row["wel_closed"]) ? 0 : $row["wel_closed"]);
				$pt_po_no=is_null($row["wel_po_no"]) ? "" : $row["wel_po_no"];
				$pt_po_line=intval(is_null($row["wel_po_line"]) ? 0 : $row["wel_po_line"]);
				$pt_part_no=is_null($row["wel_part_no"]) ? "" : $row["wel_part_no"];

				if($pt_rn_qty>0){throw new Exception("wel_pt_detail_to_store");}

				try
				{
					mysql_query("begin");
						
						$sql="UPDATE #__wel_pordetm SET".
							" wel_rcv_qty=IFNULL(wel_rcv_qty,0)-".$pt_rcv_qty.",".
							" wel_iqc_qty=IFNULL(wel_iqc_qty,0)-".$pt_iqc_qty.",".
							" wel_rtv_qty=IFNULL(wel_rtv_qty,0)-".$pt_rtv_qty." ".
							" WHERE wel_po_no='".$pt_po_no."' AND wel_po_line=".$pt_po_line." LIMIT 1";
						$sql=revert_to_the_available_sql($sql);
						if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}

						$sql="DELETE FROM #__wel_ptrdetm WHERE wel_pt_no='".$this->wel_pt_no."' AND wel_pt_line=".intval($this->wel_pt_line)." LIMIT 1";
						$sql=revert_to_the_available_sql($sql);
						if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
						
					mysql_query("commit");
					
				}
				catch (Exception $e1)
				{
					mysql_query("rollback");
					$msg_code=$e1->getMessage();
				}
				
			}
			catch (Exception $e)
			{
				$msg_code=$e->getMessage();
			}		
			if($msg_code==""){$msg_code="delete_succee";}
			$return_val["msg_code"]=$msg_code;
			return $return_val;
		}
		
		//全部删除收货单明细
		public function delete_detail_tab0_all()
		{
			$msg_code="";
			$return_val=array();
			
			try
			{
				$conn=werp_db_connect();
				
				if (!check_permission($conn,$this->wel_prog_code,"wel_access_delete")){throw new Exception("unauthorized_access");}
				
				//收货单是否存在
				if($this->wel_pt_no=="") {throw new Exception("wel_pt_no_miss");}
				$sql="SELECT * FROM #__wel_ptrdetm WHERE wel_pt_no='".$this->wel_pt_no."'";
				$sql=revert_to_the_available_sql($sql);
				if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
				while($row=mysql_fetch_array($result))
				{
					$pt_rn_qty=doubleval(is_null($row["wel_rn_qty"]) ? 0 : $row["wel_rn_qty"]);
					if($pt_rn_qty>0){throw new Exception("wel_pt_detail_to_store");}
				}

				try
				{
					mysql_query("begin");
						
						//收货单明细是否存在
						$sql="SELECT * FROM #__wel_ptrdetm WHERE wel_pt_no='".$this->wel_pt_no."'";
						$sql=revert_to_the_available_sql($sql);
						if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
						while($row=mysql_fetch_array($result))
						{
							$pt_pt_line=doubleval(is_null($row["wel_pt_line"]) ? 0 : $row["wel_pt_line"]);
							$pt_rn_qty=doubleval(is_null($row["wel_rn_qty"]) ? 0 : $row["wel_rn_qty"]);
							$pt_rcv_qty=doubleval(is_null($row["wel_rcv_qty"]) ? 0 : $row["wel_rcv_qty"]);
							$pt_iqc_qty=doubleval(is_null($row["wel_iqc_qty"]) ? 0 : $row["wel_iqc_qty"]);
							$pt_rtv_qty=doubleval(is_null($row["wel_rtv_qty"]) ? 0 : $row["wel_rtv_qty"]);
							$pt_closed=intval(is_null($row["wel_closed"]) ? 0 : $row["wel_closed"]);
							$pt_po_no=is_null($row["wel_po_no"]) ? "" : $row["wel_po_no"];
							$pt_po_line=intval(is_null($row["wel_po_line"]) ? 0 : $row["wel_po_line"]);
							$pt_part_no=is_null($row["wel_part_no"]) ? "" : $row["wel_part_no"];

							$sql="UPDATE #__wel_pordetm SET".
								" wel_rcv_qty=IFNULL(wel_rcv_qty,0)-".$pt_rcv_qty.",".
								" wel_iqc_qty=IFNULL(wel_iqc_qty,0)-".$pt_iqc_qty.",".
								" wel_rtv_qty=IFNULL(wel_rtv_qty,0)-".$pt_rtv_qty." ".
								" WHERE wel_po_no='".$pt_po_no."' AND wel_po_line=".$pt_po_line." LIMIT 1";
							$sql=revert_to_the_available_sql($sql);
							if(!($del_result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}

							$sql="DELETE FROM #__wel_ptrdetm WHERE wel_pt_no='".$this->wel_pt_no."' AND wel_pt_line=".$pt_pt_line." LIMIT 1";
							$sql=revert_to_the_available_sql($sql);
							if(!($del_result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
						}
							
						mysql_free_result($result);

					mysql_query("commit");
					
				}
				catch (Exception $e1)
				{
					mysql_query("rollback");
					$msg_code=$e1->getMessage();
				}
				
			}
			catch (Exception $e)
			{
				$msg_code=$e->getMessage();
			}		
			if($msg_code==""){$msg_code="delete_succee";}
			$return_val["msg_code"]=$msg_code;
			return $return_val;
		}
	}
?>