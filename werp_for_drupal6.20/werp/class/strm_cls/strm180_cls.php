<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>

<?php 
	class strm180_cls
	{
		public $wel_is_auto;
		public $wel_wh_code;
		public $wel_tag;
		public $wel_tag_fm;
		public $wel_tag_to;
		
		private $wel_prog_code="strm180";
		//产生
		public function generate()
		{
			$msg_code="";
			$return_val=array();
			try
			{
				$conn=werp_db_connect();
				if (!check_permission($conn,$this->wel_prog_code,"wel_access_addnew")){throw new Exception("unauthorized_access");}
				
				if($this->wel_wh_code==''){throw new Exception('wel_wh_code_miss');}
				$sql="SELECT  * FROM #__wel_whlocfm WHERE wel_wh_code='{$this->wel_wh_code}' limit 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!$whlocfm=mysql_fetch_array($result)){throw new Exception("wel_wh_code_not_found");}
				
				$int_tag = intval(is_null($whlocfm['wel_tag_no'])? 0 : $whlocfm['wel_tag_no']);
				$org_tag = $int_tag;

				if($this->wel_is_auto==1)
				{
					if(empty($this->wel_tag_fm)){throw new Exception('tag_num_miss');}
					if(empty($this->wel_tag_to)){throw new Exception('tag_num_miss');}
					If(intval($this->wel_tag_fm) == 0 || intval($this->wel_tag_to) == 0){throw new Exception('tag_num_miss');}

   					If(intval($this->wel_tag_fm) > intval($this->wel_tag_to))
   					{
        				throw new Exception('tag_num_error');
   					}

					$i = 1;
					$int_tag = $this->wel_tag_fm - 1 ;
					$this->wel_tag = $this->wel_tag_to - $this->wel_tag_fm + 1;
				}
				else 
				{
					if(empty($this->wel_tag)){throw new Exception('wel_tag_miss');}
					if(intval($this->wel_tag)==0){throw new Exception('wel_tag_error');}
				}

				try
				{
					mysql_query('begin');

						$i = 1;
        				While($i <= $this->wel_tag && (($i + $int_tag) <= 999999))
        				{
            				$int_cc=$int_tag + $i;
        					$str_wel_tag_no=substr(strval($int_cc+1000000),1,6);	
        					$sql="INSERT INTO #__wel_countft set ".
	            					"wel_wh_code = '".strtoupper($this->wel_wh_code)."',".
	            					"wel_tag_no = '$str_wel_tag_no'";
							$sql=revert_to_the_available_sql($sql);
							if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}

            				$i = $i + 1;
        				}

						if($int_cc > $org_tag)
        				{
        					$sql="UPDATE #__wel_whlocfm SET ".
        						" wel_tag_no='{$int_cc}' ".
        						" WHERE wel_wh_code='{$this->wel_wh_code}'";
							$sql=revert_to_the_available_sql($sql);
							if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
						}

					mysql_query("commit");
				}
				catch (Exception $e1)
				{
					mysql_query("rollback");
					throw new Exception($e1->getMessage());
				}	

			}
			catch (Exception $e)
			{
				$msg_code=$e->getMessage();
			}
			if($msg_code==""){$msg_code="generate_succee";}
			$return_val["msg_code"]=$msg_code;
			$return_val["wel_is_auto"]=$this->wel_is_auto;
			return $return_val;
		}
	}
?>