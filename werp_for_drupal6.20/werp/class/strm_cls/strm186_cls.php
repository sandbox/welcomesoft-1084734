<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>

<?php 
	class strm186_cls
	{
		public $wel_outs="";
		
		
		private $wel_prog_code="strm186";
		
		//确认
		public function confirm()
		{
			$msg_code="";
			$return_val=array();
			
			try
			{
				$conn=werp_db_connect();
				
				if (!check_permission($conn,$this->wel_prog_code,"wel_access_addnew")){throw new Exception("unauthorized_access");}
				
				mysql_query('begin');
					$sql = "DELETE FROM #__wel_countft";
					$sql=revert_to_the_available_sql($sql);
        			if(!mysql_query($sql,$conn)){
						$msg_code=mysql_error();
						mysql_query('rollback');
						throw new Exception($msg_code);
					}

        			$sql = "UPDATE #__wel_whlocfm SET wel_tag_no = 0";
					$sql=revert_to_the_available_sql($sql);
       				if(!mysql_query($sql,$conn)){
						$msg_code=mysql_error();
						mysql_query('rollback');
						throw new Exception($msg_code);
					}
        		mysql_query('commit');
			}
			catch (Exception $e)
			{
				$msg_code=$e->getMessage();
			}
			if($msg_code==""){$msg_code="confirm_succee";}
			$return_val["msg_code"]=$msg_code;
			return $return_val;
		}
	}
?>