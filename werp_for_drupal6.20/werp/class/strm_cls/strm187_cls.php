<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>

<?php 
	class strm187_cls
	{
		public $wel_phy_date;
		public $wel_tran_code;
		
		private $wel_prog_code="strm187";
		
		//确认
		public function confirm()
		{
			$msg_code="";
			$return_val=array();
			
			try
			{
				$conn=werp_db_connect();
				if (!check_permission($conn,$this->wel_prog_code,"wel_access_addnew")){throw new Exception("unauthorized_access");}
				
				if($this->wel_phy_date==''){throw new Exception('wel_phy_date_miss');}
				if($this->wel_tran_code==''){throw new Exception('wel_tran_code_miss');}
				
				$sql = "SELECT * FROM #__wel_closedm limit 1";
				$sql=revert_to_the_available_sql($sql);
   				if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				if(!$closedm=mysql_fetch_array($result))
				{
					$sql="INSERT INTO #__wel_closedm SET".
						" wel_tran_code='{$this->wel_tran_code}',".
						" wel_lock_yn=0";
					$sql=revert_to_the_available_sql($sql);
	   				if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				}Else{
   					$wel_phy_date=date("Y-m-d",strtotime($this->wel_phy_date));
   					$dat_today=date('Y-m-d');
        			If($closedm['wel_lock_yn'] == 1)
        			{
            			throw new Exception('wel_phy_in_process');
        			}

        			If($wel_phy_date >= $dat_today)
        			{
            			throw new Exception('wel_phy_date_error');
        			}
        			If( date("Y-m-d",strtotime($closedm['wel_phy_date'])) >= $wel_phy_date )
        			{
            			throw new Exception('wel_phy_date_error');
        			}
        			IF (is_null($closedm["wel_period1"])){
        			}ELSE{
        				If( date("Y-m-d",strtotime($closedm['wel_period1'])) >= $wel_phy_date )
        				{
            				throw new Exception('wel_phy_date_error');
        				}
        			}
   				}
   				
				try
				{
					mysql_query('begin');
				
   						$sql="DELETE FROM #__wel_whlotft";
						$sql=revert_to_the_available_sql($sql);
		   				if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}

						$sql="DELETE FROM #__wel_countfm";	
						$sql=revert_to_the_available_sql($sql);
		   				if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
						
						$sql = "UPDATE #__wel_closedm SET ".
								"wel_tran_code='{$this->wel_tran_code}',".
								"wel_phy_d11 = wel_phy_d10 ,".
								"wel_phy_d10 = wel_phy_d9,".
								"wel_phy_d9 = wel_phy_d8,".
								"wel_phy_d8 = wel_phy_d7,".
								"wel_phy_d7 = wel_phy_d6,".
								"wel_phy_d6 = wel_phy_d5,".
								"wel_phy_d5 = wel_phy_d4,".
								"wel_phy_d4 = wel_phy_d3,".
								"wel_phy_d3 = wel_phy_d2,".
								"wel_phy_d2 = wel_phy_d1,".
								"wel_phy_d1 = wel_phy_date,".
								"wel_phy_date='{$this->wel_phy_date}',".
								"wel_lock_yn = 1,".
	                			"wel_period1 = Null,wel_period2 = Null, wel_period3 = Null, wel_period4 = Null,".
	                			"wel_period5 = Null, wel_period6 = Null, wel_period7 = Null, wel_period8 = Null,".
	                			"wel_period9 = Null, wel_period10 = Null, wel_period11 = Null, wel_period12 = Null  ";
						$sql=revert_to_the_available_sql($sql);
		   				if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
   					
   				
						$sql="SELECT * FROM #__wel_whlotfm";
						$sql=revert_to_the_available_sql($sql);
						if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}	//查询sql时出错了
						while($whlotfm=mysql_fetch_array($result))
						{
							$str_wh_code = is_null($whlotfm['wel_wh_code'])? "" : $whlotfm['wel_wh_code'];
        					$str_part_no = is_null($whlotfm['wel_part_no'])? "" : $whlotfm['wel_part_no'];
							$str_lot_no = is_null($whlotfm['wel_lot_no'])? "" : $whlotfm['wel_lot_no'];
        					$dec_stk_qty = is_null($whlotfm['wel_stk_qty'])? 0 : $whlotfm['wel_stk_qty'];
        					$dat_crt_date = is_null($whlotfm['wel_crt_date'])? null : $whlotfm['wel_crt_date'];  

        					$dat_date = date('Y-m-d', strtotime('+1 day'));
        				
        					$sql="SELECT wel_wh_fm,wel_wh_to,wel_lot_no_fm,wel_lot_no_to,wel_tran_qty,wel_spar_qty ".
        						" FROM #__wel_tranflm WHERE wel_part_no='{$str_part_no}' AND wel_tran_date > '{$wel_phy_date}' AND wel_tran_date <= '{$dat_date}'";
							$sql=revert_to_the_available_sql($sql);
    	   					if(!$the_result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
       						While($tranflm=mysql_fetch_array($the_result))
       						{
            					$str_lot_no_fm = is_null($tranflm['wel_lot_no_fm'])? "" : $tranflm['wel_lot_no_fm'];
            					$str_lot_no_to = is_null($tranflm['wel_lot_no_to'])? "" : $tranflm['wel_lot_no_to'];
            					$str_wh_fm = is_null($tranflm['wel_wh_fm'])? "" : $tranflm['wel_wh_fm'];
            					$str_wh_to = is_null($tranflm['wel_wh_to'])? "" :  $tranflm['wel_wh_to'];
            				
            					$dec_tran_qty = is_null($tranflm['wel_tran_qty'])? 0 : $tranflm['wel_tran_qty'];
                				$dec_spar_qty = is_null($tranflm['wel_spar_qty'])? 0 : $tranflm['wel_spar_qty'];
                				
            					If($str_wh_fm == $str_wh_code and $str_lot_no_fm == $str_lot_no)
            					{
                					$dec_stk_qty = $dec_stk_qty + $dec_tran_qty + $dec_spar_qty;
            					}
            					If($str_wh_to == $str_wh_code and $str_lot_no_to == $str_lot_no)
            					{
                					$dec_stk_qty = $dec_stk_qty - $dec_tran_qty - $dec_spar_qty;
            					}
       						}
       						mysql_free_result($the_result);
       						unset($tranflm);
       					
       						If($dec_stk_qty > 0)
        					{
        						$sql="INSERT INTO #__wel_whlotft SET ".
            						" wel_wh_code = '".strtoupper($str_wh_code)."',".
            						" wel_part_no = '".strtoupper($str_part_no)."',".
            						" wel_lot_no = '".strtoupper($str_lot_no)."',".
            						" wel_stk_loc ='',".
            						" wel_stk_qty = '".round($dec_stk_qty, 4)."',".
            						" wel_crt_date =now() ";	// '{$dat_crt_date}'
								$sql=revert_to_the_available_sql($sql);
        						if(!mysql_query($sql,$conn)){throw new Exception($msg_code);}
        					}
						}
        			mysql_query('commit');

				}
				catch (Exception $e1)
				{
					mysql_query("rollback");
					throw new Exception($e1->getMessage());
				}	

			}
			catch (Exception $e)
			{
				$msg_code=$e->getMessage();
			}
			if($msg_code==""){$msg_code="confirm_succee";}
			$return_val["msg_code"]=$msg_code;
			return $return_val;
		}
	}
?>