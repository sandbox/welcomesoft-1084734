<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>

<?php 
	class strm188_cls
	{
		public $wel_post_yn;
		
		private $wel_prog_code="strm188";
		
		//确认
		public function confirm()
		{
			$msg_code="";
			$return_val=array();
			
			try
			{
				$conn=werp_db_connect();
				
				if (!check_permission($conn,$this->wel_prog_code,"wel_access_addnew")){throw new Exception("unauthorized_access");}
				
				$str_workingtitle = 'Loading......';
				set_time_limit(0);
				
				$str_workingtitle = 'Checking......';
				$sql="SELECT * FROM #__wel_closedm limit 1";
				$sql=revert_to_the_available_sql($sql);
				if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("system_init_error");}

				$dat_phy_date = is_null($row['wel_phy_date'])? null : $row['wel_phy_date'];
       			$str_tran_code = is_null($row['wel_tran_code'])? '' : $row['wel_tran_code'];
       			$int_lock = is_null($row['wel_lock_yn'])? 0 : $row['wel_lock_yn'];
       			$dec_ex_rate = 0;
				$wel_phy_date=date("Y-m-d",strtotime($dat_phy_date));

				If($int_lock <> 1)
				{
					throw new Exception('cut_off_error');
				}
				If(trim($str_tran_code) == '')
				{
					throw new Exception('cut_off_error');
				}

				try
				{
					mysql_query('begin');
				
					$str_workingtitle = 'Get The Latest Transation Number.....';
					$sql="SELECT wel_trn_recno FROM #__wel_tranflm ORDER BY wel_trn_recno DESC limit 1";
					$sql=revert_to_the_available_sql($sql);
					if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
	   				If(!$tranflm=mysql_fetch_array($result))
	   				{
	       				$int_next_recorder = 1;
	   				}Else{
	       				$int_next_recorder = is_null($tranflm['wel_trn_recno'])? 1 : $tranflm['wel_trn_recno'];
	   				}
				
					$str_workingtitle = 'Zero Stock Balance On WareHouse';
        			$sql="UPDATE #__wel_whlotfm SET wel_stk_qty = 0";
					$sql=revert_to_the_available_sql($sql);
					if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}

        			$str_workingtitle = 'Post Closing Stock To Transaction File';
			        $sql="SELECT a.wel_wh_code, a.wel_part_no, a.wel_lot_no,".
			        	"a.wel_stk_qty,b.wel_cat_code,b.wel_avg_cost,b.wel_std_cost ".
			        	"FROM #__wel_whlotft a LEFT JOIN #__wel_partflm b ON a.wel_part_no = b.wel_part_no ".
			        	"WHERE IFNULL(a.wel_stk_qty,0)>0";
					$sql=revert_to_the_available_sql($sql);
			        if(!$wt_result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
			        while($whlotft=mysql_fetch_array($wt_result))
			        {
			        	$int_next_recorder = $int_next_recorder + 1;     //Next Transation Number
            			$str_wh_to = is_null($whlotft['wel_wh_code'])? '' : $whlotft['wel_wh_code'];
            			$str_part_no = is_null($whlotft['wel_part_no'])? '' : $whlotft['wel_part_no'];
            			$str_lot_no = is_null($whlotft['wel_lot_no'])? '' : $whlotft['wel_lot_no'];
            			$dec_tran_qty = doubleval(is_null($whlotft['wel_stk_qty'])? 0 : $whlotft['wel_stk_qty']);
            			$str_cat_code = is_null($whlotft['wel_cat_code'])? '' : $whlotft['wel_cat_code'];
            			$dec_avg_cost = is_null($whlotft['wel_avg_cost'])? 0 : $whlotft['wel_avg_cost'];
            			$dec_std_cost = is_null($whlotft['wel_std_cost'])? 0 : $whlotft['wel_std_cost'];

            			$str_workingtitle = "Post {$str_part_no} Stock Quantity To Transaction File";
			        	
            			//新增记录
            			$dec_tran_qty = $dec_tran_qty * -1;
            			$sql="INSERT INTO #__wel_tranflm set ".
		            				"wel_crt_date =now(),".
		            				"wel_crt_user = '{$_SESSION['wel_user_id']}',".
		            				"wel_trn_recno = '{$int_next_recorder}',".
		            				"wel_wh_to = '".strtoupper($str_wh_to)."',".
		            				"wel_lot_no_to = '".strtoupper($str_lot_no)."',".
		            				"wel_stk_loc_to = '',".
		            				"wel_tran_code = '".strtoupper($str_tran_code)."',".
		            				"wel_part_no = '".strtoupper($str_part_no)."',".
		            				"wel_cat_code = '".strtoupper($str_cat_code)."',".
		            				"wel_tran_qty = '{$dec_tran_qty}',".
		            				"wel_tran_date = '{$wel_phy_date}',".
		            				"wel_avg_cost = '{$dec_avg_cost}',".
		            				"wel_std_cost = '{$dec_std_cost}',".
		            				"wel_closed = 1";
						$sql=revert_to_the_available_sql($sql);
						if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
			        }
					mysql_free_result($wt_result);
					unset($whlotft);
			        
			        //Open COUNTFM Control File Post Closing Count Quantity To Transaction File AND UPDATE wel_whlotfm
			        $sql="SELECT a.wel_wh_code,a.wel_part_no,a.wel_tag_no,a.wel_lot_no,".
				        " a.wel_tag_qty,b.wel_cat_code,b.wel_avg_cost,b.wel_std_cost ".
			        	" FROM #__wel_countfm a LEFT JOIN #__wel_partflm b ON a.wel_part_no = b.wel_part_no ".
			        	" WHERE IFNULL(a.wel_tag_qty,0)>0";
					$sql=revert_to_the_available_sql($sql);
			        if(!$cf_result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
			        while ($countfm=mysql_fetch_array($cf_result))
			        {
			        	$int_next_recorder = $int_next_recorder + 1;
            			$str_wh_code = is_null($countfm['wel_wh_code'])? '' : $countfm['wel_wh_code'];
            			$str_part_no = is_null($countfm['wel_part_no'])? '' : $countfm['wel_part_no'];
            			$str_lot_no = is_null($countfm['wel_lot_no'])? '' : $countfm['wel_lot_no'];
            			$str_ref_no = is_null($countfm['wel_tag_no'])? '' : $countfm['wel_tag_no'];
            			$dec_tran_qty = doubleval(is_null($countfm['wel_tag_qty'])? 0 : $countfm['wel_tag_qty']);
            			$str_cat_code = is_null($countfm['wel_cat_code'])? '' : $countfm['wel_cat_code'];
            			$dec_avg_cost = is_null($countfm['wel_avg_cost'])? 0 : $countfm['wel_avg_cost'];
            			$dec_std_cost = is_null($countfm['wel_std_cost'])? 0 : $countfm['wel_std_cost'];

            			$str_workingtitle = "Post {$str_part_no} Count Quantity To Transaction File";
            			//新增记录
            			$sql="INSERT INTO #__wel_tranflm set ".
		            				"wel_crt_date =now(),".
		            				"wel_crt_user = '{$_SESSION['wel_user_id']}',".
		            				"wel_trn_recno = '{$int_next_recorder}',".
		            				"wel_wh_to = '".strtoupper($str_wh_code)."',".
		            				"wel_lot_no_to = '".strtoupper($str_lot_no)."',".
		            				"wel_stk_loc_to = '',".
		            				"wel_tran_code = '".strtoupper($str_tran_code)."',".
		            				"wel_part_no = '".strtoupper($str_part_no)."',".
		            				"wel_cat_code = '".strtoupper($str_cat_code)."',".
		            				"wel_tran_qty = '{$dec_tran_qty}',".
		            				"wel_tran_date = '{$wel_phy_date}',".
		            				"wel_avg_cost = '{$dec_avg_cost}',".
		            				"wel_std_cost = '{$dec_std_cost}',".
		            				"wel_closed = 1,".
		            				"wel_ref_no = '".strtoupper($str_ref_no)."'";
						$sql=revert_to_the_available_sql($sql);
						if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
						
						//Open WHLOTFM Control File And UPDATE Current Inventory
						$sql="SELECT *  FROM #__wel_whlotfm WHERE wel_wh_code= '{$str_wh_code}' ".
								" AND wel_part_no= '{$str_part_no}' AND wel_lot_no= '{$str_lot_no}' ".
								" AND wel_stk_loc = '' limit 1";
						$sql=revert_to_the_available_sql($sql);
						if(!$wh_result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
						if(!$whlotfm=mysql_fetch_array($wh_result))
						{
							$sql="INSERT INTO #__wel_whlotfm set ". 
										"wel_wh_code = '".strtoupper($str_wh_code)."',".
		                				"wel_part_no = '".strtoupper($str_part_no)."',".
		                				"wel_lot_no = '".strtoupper($str_lot_no)."',".
		                				"wel_stk_loc = '',".
		                				"wel_stk_qty = '".round($dec_tran_qty, 4)."',".
		                				"wel_crt_date =now(),".
		            					"wel_crt_user = '{$_SESSION['wel_user_id']}',".
		                				"wel_u_price = 0 "; 
							$sql=revert_to_the_available_sql($sql);
							if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
						}
						else 
						{
							$dec_stk_qty = doubleval(is_null($whlotfm['wel_stk_qty'])? 0 : $whlotfm['wel_stk_qty']);
                			$sql="UPDATE #__wel_whlotfm SET ".
                				" wel_stk_qty = '".round($dec_tran_qty + $dec_stk_qty, 4)."',".
								" wel_upd_date =now(),".
		            			" wel_upd_user = '{$_SESSION['wel_user_id']}' ".
                				" WHERE wel_wh_code= '{$str_wh_code}' AND wel_part_no= '{$str_part_no}' AND wel_lot_no= '{$str_lot_no}' AND wel_stk_loc = ''";	  
							$sql=revert_to_the_available_sql($sql);
							if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
						}

						mysql_free_result($wh_result);
						unset($whlotfm);
			        }
					mysql_free_result($cf_result);
					unset($countfm);
			        
			        $str_workingtitle = "Replace All Quantity With 0 In Part WareHouse File ";
			        
			        //Open PARTWHM Control File and Replace All Quantity With 0
			        $sql = "UPDATE #__wel_partwhm SET ".
					        	"wel_phy_qty11 = wel_phy_qty10,".
					        	"wel_phy_qty10 = wel_phy_qty9,".
					        	"wel_phy_qty9 = wel_phy_qty8,".
					        	"wel_phy_qty8 = wel_phy_qty7,".
					        	"wel_phy_qty7 = wel_phy_qty6,".
					        	"wel_phy_qty6 = wel_phy_qty5,".
					        	"wel_phy_qty5 = wel_phy_qty4,".
					        	"wel_phy_qty4 = wel_phy_qty3,".
					        	"wel_phy_qty3 = wel_phy_qty2,".
					        	"wel_phy_qty2 = wel_phy_qty1,".
					        	"wel_phy_qty1 = wel_phy_qty,".
					        	"wel_phy_qty=0,".
					        	"wel_period1=0,wel_period2=0,wel_period3=0,wel_period4=0,".
					        	"wel_period5=0,wel_period6=0,wel_period7=0,wel_period8=0,".
					        	"wel_period9=0,wel_period10=0,wel_period11=0,wel_period12 = 0";	
					$sql=revert_to_the_available_sql($sql);
					if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					
					$str_workingtitle = "Ready To UPDATE Part WareHouser File...";
        			//Open WHLOTFM Control File AND UPDATE PARTWHM File	
        			$sql = "SELECT a.wel_wh_code,a.wel_part_no,Sum(a.wel_stk_qty) AS wel_stk_qty ".
	        				"FROM #__wel_whlotfm a ".
	        				"GROUP BY a.wel_wh_code,a.wel_part_no ".
	        				"Having (((Sum(IFNULL(a.wel_stk_qty,0))) > 0)) ".
	        				"ORDER BY a.wel_wh_code,a.wel_part_no";
					$sql=revert_to_the_available_sql($sql);
        			if(!$wh_result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
			        while ($whlotfm=mysql_fetch_array($wh_result))
			        {
			        	$str_wh_code = is_null($whlotfm['wel_wh_code'])? '' : $whlotfm['wel_wh_code'];
            			$str_part_no = is_null($whlotfm['wel_part_no'])? '' : $whlotfm['wel_part_no'];
            			$dec_stk_qty = doubleval(is_null($whlotfm['wel_stk_qty'])? 0 : $whlotfm['wel_stk_qty']);

            			$sql = "SELECT * FROM #__wel_partwhm WHERE wel_part_no= '{$str_part_no}' AND wel_wh_code= '{$str_wh_code}' limit 1";
						$sql=revert_to_the_available_sql($sql);
            			if(!$ph_result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
            			if(!$partwhm=mysql_fetch_array($ph_result))
            			{
            				$sql="INSERT INTO #__wel_partwhm set ".
		            					"wel_part_no = '".strtoupper($str_part_no)."',".
		               					"wel_wh_code = '".strtoupper($str_wh_code)."',".
		               					"wel_creat_d = '".date('Y-m-d')."',".
		               					"wel_phy_qty = '{$dec_stk_qty}',".
		               					"wel_u_price = 0 ";		
							$sql=revert_to_the_available_sql($sql);
		        			if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
            			}
            			else 
            			{
            				$sql="UPDATE #__wel_partwhm set ".
	            					"wel_phy_qty = '{$dec_stk_qty}' ".
            					 "WHERE wel_part_no= '{$str_part_no}' AND wel_wh_code= '{$str_wh_code}'";
							$sql=revert_to_the_available_sql($sql);
		        			if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
            			}

						mysql_free_result($ph_result);
						unset($partwhm);	
			        }
			        mysql_free_result($wh_result);
			        unset($whlotfm);
			        
					// Day-back transaction after physical take date
					$sql="SELECT * FROM #__wel_tranflm ".
						" WHERE wel_tran_date > '{$wel_phy_date}'";
					$sql=revert_to_the_available_sql($sql);
					if(!$tf_result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					while($tranflm=mysql_fetch_array($tf_result))
					{
						$dec_tran_qty = doubleval(is_null($tranflm['wel_tran_qty'])? 0 : $tranflm['wel_tran_qty']);
						$dec_spar_qty = doubleval(is_null($tranflm['wel_spar_qty'])? 0 : $tranflm['wel_spar_qty']);
						$str_lot_no_fm = is_null($tranflm['wel_lot_no_fm'])? '' :  $tranflm['wel_lot_no_fm'];
						$str_lot_no_to = is_null($tranflm['wel_lot_no_to'])? '' :  $tranflm['wel_lot_no_to'];
						$str_wh_fm = is_null($tranflm['wel_wh_fm'])? '' :  $tranflm['wel_wh_fm'];
						$str_wh_to = is_null($tranflm['wel_wh_to'])? '' :  $tranflm['wel_wh_to'];
						$str_part_no = is_null($tranflm['wel_part_no'])? '' :  $tranflm['wel_part_no'];
						$str_flow_code = is_null($tranflm['wel_flow_code'])? '' :  $tranflm['wel_flow_code'];

						if($str_flow_code=='INT' || $str_flow_code=='CIN' || $str_flow_code=='VRT' || $str_flow_code=='SHP')
						{
							$sql="SELECT * FROM #__wel_whlotfm ".
								" WHERE wel_wh_code='".$str_wh_fm."' AND wel_part_no='".$str_part_no."' ".
								" AND wel_lot_no='".$str_lot_no_fm."' AND wel_stk_loc='' LIMIT 1";
							$sql=revert_to_the_available_sql($sql);
							if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
							if($row=mysql_fetch_array($result))
							{
								$sql="UPDATE #__wel_whlotfm SET ".
									" wel_stk_qty=IFNULL(wel_stk_qty,0)-$dec_tran_qty-$dec_spar_qty ".
									" WHERE wel_wh_code='".$str_wh_fm."' AND wel_part_no='".$str_part_no."' ".
									" AND wel_lot_no='".$str_lot_no_fm."' AND wel_stk_loc='' LIMIT 1";
								$sql=revert_to_the_available_sql($sql);
								if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
							}else{
								$sql="INSERT INTO #__wel_whlotfm SET ".
									" wel_wh_code='".$str_wh_fm."',".
									" wel_part_no='".$str_part_no."',".
									" wel_lot_no='".$str_lot_no_fm."',".
									" wel_stk_loc='',".
									" wel_stk_qty=0-$dec_tran_qty-$dec_spar_qty,".
									" wel_crt_user='{$_SESSION['wel_user_id']}', ".
									" wel_crt_date=now()";
							}
							$sql=revert_to_the_available_sql($sql);
							if(!($result1=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
						}

						if($str_flow_code=='INT' || $str_flow_code=='REC' || $str_flow_code=='FGS' || $str_flow_code=='COT' || $str_flow_code=='CRT' || $str_flow_code=='ADJ')
						{
							$sql="SELECT * FROM #__wel_whlotfm ".
								" WHERE wel_wh_code='".$str_wh_to."' AND wel_part_no='".$str_part_no."' ".
								" AND wel_lot_no='".$str_lot_no_to."' AND wel_stk_loc='' LIMIT 1";
							$sql=revert_to_the_available_sql($sql);
							if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
							if($row=mysql_fetch_array($result))
							{
								$sql="UPDATE #__wel_whlotfm SET ".
									" wel_stk_qty=IFNULL(wel_stk_qty,0)+$dec_tran_qty+$dec_spar_qty ".
									" WHERE wel_wh_code='".$str_wh_to."' AND wel_part_no='".$str_part_no."' ".
									" AND wel_lot_no='".$str_lot_no_to."' AND wel_stk_loc='' LIMIT 1";
								$sql=revert_to_the_available_sql($sql);
								if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
							}else{
								$sql="INSERT INTO #__wel_whlotfm SET ".
									" wel_wh_code='".$str_wh_to."',".
									" wel_part_no='".$str_part_no."',".
									" wel_lot_no='".$str_lot_no_to."',".
									" wel_stk_loc='',".
									" wel_stk_qty=$dec_tran_qty+$dec_spar_qty,".
									" wel_crt_user='{$_SESSION['wel_user_id']}', ".
									" wel_crt_date=now()";
								$sql=revert_to_the_available_sql($sql);
								if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
							}
						}
					}
			        mysql_free_result($tf_result);
			        unset($tranflm);

					$str_workingtitle = "Being CalCulate FIFO Cost....";
					$sql="SELECT a.wel_part_no, Sum(a.wel_tag_qty) AS wel_stk_qty, ".
						" b.wel_avg_cost,b.wel_std_cost,b.wel_lat_cost,b.wel_vqt_cost ".
						" FROM #__wel_countfm a INNER JOIN #__wel_partflm b ON a.wel_part_no = b.wel_part_no ".
						" GROUP BY a.wel_part_no,b.wel_avg_cost,b.wel_std_cost,b.wel_lat_cost,b.wel_vqt_cost";
					$sql=revert_to_the_available_sql($sql);
					if(!$cp_result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					While($cp=mysql_fetch_array($cp_result))
					{
            			$str_part_no = is_null($cp['wel_part_no'])? '' :  $cp['wel_part_no'];
            			$dec_avg_cost = is_null($cp['wel_avg_cost'])?  0 : $cp['wel_avg_cost'];
            			$dec_std_cost = is_null($cp['wel_std_cost'])?  0 : $cp['wel_std_cost'];
            			$dec_lat_cost = is_null($cp['wel_lat_cost'])?  0 : $cp['wel_lat_cost'];
            			$dec_vqt_cost = is_null($cp['wel_vqt_cost'])?  0 : $cp['wel_vqt_cost'];
            			$dec_sum_stk = doubleval(is_null($cp['wel_stk_qty'])?  0 : $cp['wel_stk_qty']);

            			$str_workingtitle = "CalCulate FIFO Cost {$str_part_no}...";

						If($dec_sum_stk > 0)
						{
                			$dec_fif_cost = 0;
                			$dec_remain = $dec_sum_stk;
                			$dec_total_fifo_amount = 0;

                			$int_trn_no = 0;
                			$str_count = "AAA";
               				While($str_count == "AAA")
               				{
								if( $int_trn_no == 0)
								{
	                				$sql="SELECT * FROM #__wel_tranflm".
	                       				" WHERE wel_part_no='{$str_part_no}' AND wel_tran_date<='{$wel_phy_date}' ".
	                       				" AND wel_flow_code='REC' ".
	                       				" ORDER BY wel_trn_recno DESC,wel_crt_date DESC limit 1";
								}else{
	                				$sql="SELECT * FROM #__wel_tranflm".
	                       				" WHERE wel_part_no='{$str_part_no}' AND wel_tran_date<='{$wel_phy_date}' ".
	                       				" AND wel_trn_recno < {$int_trn_no} AND wel_flow_code='REC' ".
	                       				" ORDER BY wel_trn_recno DESC,wel_crt_date DESC limit 1";
								}
								$sql=revert_to_the_available_sql($sql);
                				if(!$tf_result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				   				If(!$tranflm=mysql_fetch_array($tf_result))
				   				{
									// Exit loop
		               				$str_count = "BBB";
				   				}else{
                    				$trn_tran_qty = doubleval(is_null($tranflm['wel_tran_qty'])?  0 : $tranflm['wel_tran_qty']);
                    				$trn_tran_date = is_null($tranflm['wel_tran_date'])? null : $tranflm['wel_tran_date'];
                    				$trn_po_no = $tranflm['wel_po_no'];
                    				$trn_po_line = $tranflm['wel_po_line'];

									$po_cur_code = "";
									$po_ex_rate = 1;
									if($trn_po_no!="")
									{
		                				$sql="SELECT * FROM #__wel_porhdrm ".
		                       				" WHERE wel_po_no='{$trn_po_no}' limit 1";
										$sql=revert_to_the_available_sql($sql);
                						if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
						   				If($row=mysql_fetch_array($result))
						   				{
		                    				$po_cur_code = $row['wel_cur_code'];
		                    				$po_ex_rate = $row['wel_ex_rate'];
						   				}
									}

									$po_u_price = 0;
									$po_pur_unit_rate = 1;
									if($trn_po_no!="" && trn_po_line > 0)
									{
		                				$sql="SELECT * FROM #__wel_pordetm ".
		                       				" WHERE wel_po_no='{$trn_po_no}' AND wel_po_line='{$trn_po_line}' limit 1";
										$sql=revert_to_the_available_sql($sql);
                						if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
						   				If($row=mysql_fetch_array($result))
						   				{
		                    				$po_u_price = $row['wel_u_price'];
											$po_pur_unit_rate=doubleval(is_null($row["wel_pur_unit_rate"]) ? 1 : $row["wel_pur_unit_rate"]);
						   				}
									}

									//货币兑换率
									$int_year = date('Y',strtotime($trn_tran_date));
									$int_month = date('m',strtotime($trn_tran_date));
									if(strlen($int_month) == 1)
									{
										$int_month = '0'.$int_month;
									}
									$int_yyyymm = strval($int_year.$int_month);

									$sql = "SELECT * FROM #__wel_curtabh WHERE wel_cur_code='".$po_cur_code."' AND wel_yyyy_mm='".$int_yyyymm."' LIMIT 1";
									$sql=revert_to_the_available_sql($sql);
									if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
									if($row=mysql_fetch_array($result))
									{
										$po_ex_rate = doubleval(is_null($row['wel_ex_rate']) ? 1 : $row['wel_ex_rate']);
									}	

									// Total Amount Calculation
                    				If($dec_tran_qty >= $dec_remain)
                    				{
                            			//$dec_total_fifo_amount = $dec_total_fifo_amount + $dec_remain * $dec_u_price * $dec_ex_rate * $dec_unit_exchange_rate;
                            			$dec_total_fifo_amount = $dec_total_fifo_amount + $dec_remain  * $po_ex_rate * ($po_u_price / $po_pur_unit_rate);
                            			$dec_remain = 0;
										// Exit loop
		               					$str_count = "BBB";
                    				}	
                    				Else
                    				{
                            			$dec_remain = $dec_remain - $dec_tran_qty;
                            			//$dec_total_fifo_amount = $dec_total_fifo_amount + $dec_tran_qty * $dec_u_price * $dec_ex_rate * $dec_unit_exchange_rate;
                            			$dec_total_fifo_amount = $dec_total_fifo_amount + $dec_tran_qty * $po_ex_rate * ($po_u_price / $po_pur_unit_rate);
                    				}
				   				}
                				mysql_free_result($tf_result);
                				unset($tranflm);
               				}

               				$str_workingtitle = substr($str_workingtitle,0,50) .'.';

                			$dec_fif_cost = $dec_total_fifo_amount / $dec_sum_stk ;
               				$sql="SELECT * FROM #__wel_partwcm ".
               					" WHERE wel_part_no='{$str_part_no}' AND wel_cut_date ='{$wel_phy_date}' limit 1";
							$sql=revert_to_the_available_sql($sql);
                			if(!$pw_result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
                			If(!$partwcm=mysql_fetch_array($pw_result))
                			{
                    			$sql="INSERT INTO #__wel_partwcm set ".
		                    		" wel_part_no = '".strtoupper($str_part_no)."',".
		                    		" wel_cut_date = '{$wel_phy_date}',".
		                    		" wel_fif_cost = '{$dec_fif_cost}',".
		                    		" wel_avg_cost = '{$dec_avg_cost}',".
		                    		" wel_std_cost = '{$dec_std_cost}',".
		                    		" wel_lat_cost = '{$dec_lat_cost}',".
		                    		" wel_vqt_cost = '{$dec_vqt_cost}' ";
								$sql=revert_to_the_available_sql($sql);
								if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
                   			}Else{
                    			$sql="UPDATE #__wel_partwcm set ".
	                				" wel_fif_cost = '{$dec_fif_cost}',".
	                    			" wel_avg_cost = '{$dec_avg_cost}',".
	                    			" wel_std_cost = '{$dec_std_cost}',".
	                    			" wel_lat_cost = '{$dec_lat_cost}',".
	                    			" wel_vqt_cost = '{$dec_vqt_cost}' ".
                    				" WHERE wel_part_no='{$str_part_no}' AND wel_cut_date ='{$wel_phy_date}'";
								$sql=revert_to_the_available_sql($sql);
								if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
            				}
							mysql_free_result($pw_result);
               				unset($partwcm);
           				}
					}

    				//CLEAR CLOSEDM LOCK
    				$sql="UPDATE #__wel_closedm set wel_lock_yn=0";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}

				   	mysql_query('commit');

				}
				catch (Exception $e1)
				{
					mysql_query("rollback");
					throw new Exception($e1->getMessage());
				}
			
			}
			catch (Exception $e)
			{
				$msg_code=$e->getMessage();	//.$e->__toString();
			}
			if($msg_code==""){$msg_code="confirm_succee";}
			$return_val["msg_code"]=$msg_code;
			return $return_val;
		}
	}
?>