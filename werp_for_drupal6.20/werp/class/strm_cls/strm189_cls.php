<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>

<?php
class strm189_cls{
	public $wel_wh_code;
	public $wel_wh_des;
	
	public $wel_tag_no;
	public $wel_part_no;
	
	private $wel_prog_code="strm189";

	public function read(){
		$msg_code="";
		$return_val=array();

		try{
			$conn=werp_db_connect();
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_read")){throw new Exception("unauthorized_access");}
			
			$sql="SELECT * FROM #__wel_closedm limit 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("system_init_error");}
			If($row['wel_lock_yn'] <> 1)
        	{
				throw new Exception('wel_phy_not_process');
        	}

			$sql="SELECT * from #__wel_whlocfm ".
				" where wel_wh_code='$this->wel_wh_code' limit 1"; 
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	//查询sql时出错了
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_wh_code_not_found");}	//没有符合条件的记录

			$int__count=0;
			while ($int__count < mysql_num_fields($result)) {
				$field_name=mysql_fetch_field($result,$int__count)->name;
				$return_val[$field_name]=$row[$field_name];
				//$msg_code=$msg_code."|".$field_name;
   				$int__count++;
			}
			mysql_free_result($result);
			throw new Exception("");
		}
		catch (Exception $e){
			$msg_code=$e->getMessage();
		}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	public function delete_detail_tab0(){
		$msg_code="";
		$return_val=array();
		 
		try
		{
			$conn=werp_db_connect();
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_delete")){throw new Exception("unauthorized_access");}
			
			if($this->wel_wh_code==''){throw new Exception('wel_wh_code_miss');}
			if($this->wel_tag_no==''){throw new Exception('wel_tag_no_miss');}
		
			$sql="SELECT * FROM #__wel_countfm WHERE wel_wh_code='".$this->wel_wh_code."' AND wel_tag_no='".$this->wel_tag_no."' limit 1";
			$sql=revert_to_the_available_sql($sql);
			if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_tag_no_not_found");}
			
			try
			{
				mysql_query('begin');

					$sql="DELETE FROM #__wel_countfm WHERE wel_wh_code='".$this->wel_wh_code."' AND wel_tag_no='".$this->wel_tag_no."'";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}

				mysql_query("commit");
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				$msg_code=$e1->getMessage();
			}
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}		
		if($msg_code==""){$msg_code="delete_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}

	public function delete_detail_tab0_all(){
		$msg_code="";
		$return_val=array();
	 
		try
		{
			$conn=werp_db_connect();
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_delete")){throw new Exception("unauthorized_access");}
			
			try
			{
				mysql_query('begin');

					$sql="DELETE FROM #__wel_countfm WHERE wel_wh_code='".$this->wel_wh_code."'";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}

				mysql_query("commit");
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				$msg_code=$e1->getMessage();
			}
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}		
		if($msg_code==""){$msg_code="delete_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}	
}
?>
