<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>

<?php
class strm228a_cls{
	public $wel_dn_no="";
	public $wel_dn_line=0;
	public $wel_do_no="";
	public $wel_do_line=0;
	public $wel_so_no="";
	public $wel_so_line=0;
	public $wel_cus_code="";
	public $wel_part_no="";
	public $wel_tran_qty=0;

	private $wel_prog_code="strm228";

	public function read(){
		$msg_code="";
		$return_val=array();
		try
		{
			$conn=werp_db_connect();
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_read")){throw new Exception("unauthorized_access");}
			
			$sql="SELECT d.*,".
				" p.wel_part_des as wel_part_des,p.wel_part_des1 as wel_part_des1,p.wel_part_des2 as wel_part_des2".
				" FROM #__wel_dndetfm d ".
					"LEFT JOIN #__wel_partflm p ON d.wel_part_no=p.wel_part_no ".
				 "WHERE d.wel_dn_no='$this->wel_dn_no' AND d.wel_dn_line='$this->wel_dn_line' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	//查询sql时出错了
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_dn_no_not_found");}
			$int__count=0;
			while ($int__count < mysql_num_fields($result)) 
			{
				$field_name=mysql_fetch_field($result,$int__count)->name;
				$return_val[$field_name]=$row[$field_name];
				$int__count++;
			}
			mysql_free_result($result);
		}
		catch (Exception $e){
			$msg_code=$e->getMessage();
		}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	public function addnew(){
		$msg_code="";
		$return_val=array();
		try
		{
			$conn=werp_db_connect();
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_addnew")){throw new Exception("unauthorized_access");}
			
			if ($this->wel_part_no==""){throw new Exception("wel_part_no_miss");}
			if ($this->wel_tran_qty==""){throw new Exception("wel_tran_qty_miss");}

			if (!is_numeric($this->wel_tran_qty)){$this->wel_tran_qty=0;}
			$this->wel_tran_qty=doubleval($this->wel_tran_qty);
			if($this->wel_tran_qty==0){throw new Exception("wel_tran_qty_miss");}
			if($this->wel_tran_qty<0){throw new Exception("wel_tran_qty_error");}

			if($this->wel_part_no!="")
			{
				$sql="SELECT wel_part_no from #__wel_partflm WHERE wel_part_no='".$this->wel_part_no."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_part_no_not_found");}
			}

			// Get Info from DNHDRFM
			$sql="SELECT * FROM #__wel_dnhdrfm WHERE wel_dn_no='$this->wel_dn_no' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_dn_no_not_found");}
			$dn_cus_code=is_null($row["wel_cus_code"]) ? "" : $row["wel_cus_code"];
			$dn_line=intval(is_null($row["wel_last_line"]) ? 0 : $row["wel_last_line"])+1;

			if (!is_numeric($this->wel_so_line)){$this->wel_so_line=0;}
			$this->wel_so_line=intval($this->wel_so_line);
			if($this->wel_so_no=="")
			{
				$this->wel_so_line=0;
			}
			else {
				if($this->wel_so_line==0){throw new Exception("wel_so_line_miss");}

				// Get Info from S/O
				$sql="SELECT h.wel_cus_code,d.*".
					" FROM #__wel_sordetm d LEFT JOIN #__wel_sorhdrm h ON d.wel_so_no=h.wel_so_no".
					" WHERE d.wel_so_no='$this->wel_so_no' AND d.wel_so_line='$this->wel_so_line' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_so_line_not_found");}
				$so_cus_code=is_null($row["wel_cus_code"]) ? "" : $row["wel_cus_code"];
				$so_part_no=is_null($row["wel_part_no"]) ? "" : $row["wel_part_no"];
				$so_req_qty=doubleval(is_null($row["wel_req_qty"]) ? 0 : $row["wel_req_qty"]);
				$so_dn_qty=doubleval(is_null($row["wel_dn_qty"]) ? 0 : $row["wel_dn_qty"]);

				if(strtolower($so_cus_code)!=strtolower($dn_cus_code)){throw new Exception("wel_cus_code_not_match_so");}

				if($this->wel_tran_qty>$so_req_qty-$so_dn_qty){throw new Exception("wel_tran_qty_excess_so");}
			}
			
			// Check DN Line exist
			$sql="SELECT wel_dn_no,wel_dn_line FROM #__wel_dndetfm ".
				"WHERE wel_dn_no='$this->wel_dn_no' AND ".
				"(wel_dn_line='$dn_line') LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
			if($row=mysql_fetch_array($result))
			{
				$sql="SELECT max(wel_dn_line) as max_line FROM #__wel_dndetfm ".
					"WHERE wel_dn_no='$this->wel_dn_no' ";
				$sql=revert_to_the_available_sql($sql);
				if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_dn_no_not_found");}
				$dn_line=intval(is_null($row["max_line"]) ? 0 : $row["max_line"])+1;
			}

			try
			{
				mysql_query('begin');
			
					$sql="INSERT INTO #__wel_dndetfm SET ".
							"wel_dn_no='$this->wel_dn_no',".
							"wel_dn_line='$dn_line',".
							"wel_cus_code='$dn_cus_code',".
							"wel_so_no='$this->wel_so_no',".
							"wel_so_line='$this->wel_so_line',".
							"wel_part_no='$this->wel_part_no',".
							"wel_req_qty='$this->wel_tran_qty',".
							"wel_tran_qty=0,".
							"wel_crt_user='{$_SESSION['wel_user_id']}', ".
							"wel_crt_date=now()";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}

				if($this->wel_so_no!="")
				{
					$sql="UPDATE #__wel_sordetm SET ".
							"wel_dn_qty=IFNULL(wel_dn_qty,0)+$this->wel_tran_qty ".
							"WHERE wel_so_no='$this->wel_so_no' AND wel_so_line='$this->wel_so_line' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				}
				
					$sql="UPDATE #__wel_dnhdrfm SET ".
						"wel_last_line='$dn_line',".
						"wel_upd_user='{$_SESSION['wel_user_id']}',".
						"wel_upd_date=now() ".
						"WHERE wel_dn_no='$this->wel_dn_no'";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				
				mysql_query('commit');

			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}
				
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		if($msg_code==""){$msg_code="addnew_succee";}
		$return_val["msg_code"]=$msg_code;
		$return_val["wel_dn_no"]=$this->wel_dn_no;
		$return_val["wel_dn_line"]=$this->wel_dn_line;
		return $return_val;
	}

	
	public function edit(){
		$msg_code="";
		$return_val=array();
		try
		{
			$conn=werp_db_connect();
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_edit")){throw new Exception("unauthorized_access");}
			
			if (!is_numeric($this->wel_dn_line)){$this->wel_dn_line=0;}
			$this->wel_dn_line=intval($this->wel_dn_line);
			if ($this->wel_dn_line==0){throw new Exception("wel_dn_line_miss");}

			if (!is_numeric($this->wel_tran_qty)){$this->wel_tran_qty=0;}
			$this->wel_tran_qty=doubleval($this->wel_tran_qty);
			if($this->wel_tran_qty==0){throw new Exception("wel_tran_qty_miss");}
			if($this->wel_tran_qty<0){throw new Exception("wel_tran_qty_error");}

			$sql="SELECT * FROM #__wel_dndetfm ".
				"WHERE wel_dn_no='$this->wel_dn_no' AND wel_dn_line=$this->wel_dn_line LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_dn_line_miss");}
			$dn_so_no=is_null($row["wel_so_no"]) ? "" : $row["wel_so_no"];
			$dn_so_line=intval(is_null($row["wel_so_line"]) ? 0 : $row["wel_so_line"]);
			$dn_req_qty=doubleval(is_null($row["wel_req_qty"]) ? 0 : $row["wel_req_qty"]);
			$dn_tran_qty=doubleval(is_null($row["wel_tran_qty"]) ? 0 : $row["wel_tran_qty"]);

			if($this->wel_tran_qty<$dn_tran_qty){throw new Exception("wel_tran_qty_excess_dely");}

			if($dn_so_no!="")
			{
				if($dn_so_line==0){throw new Exception("wel_so_line_miss");}

				// Get Info from S/O
				$sql="SELECT * FROM #__wel_sordetm".
					" WHERE wel_so_no='$dn_so_no' AND wel_so_line='$dn_so_line' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_so_line_not_found");}
				$so_part_no=is_null($row["wel_part_no"]) ? "" : $row["wel_part_no"];
				$so_req_qty=doubleval(is_null($row["wel_req_qty"]) ? 0 : $row["wel_req_qty"]);
				$so_dn_qty=doubleval(is_null($row["wel_dn_qty"]) ? 0 : $row["wel_dn_qty"]);

				if($this->wel_req_qty>($so_req_qty-$so_dn_qty+$dn_req_qty)){throw new Exception("wel_req_qty_excess_so");}
			}

			//if($dn_do_no!="")
			//{
			// add check for plan-DN
			//}

			try
			{
				mysql_query('begin');
					$sql="UPDATE #__wel_dndetfm SET ".
							"wel_req_qty='$this->wel_tran_qty',".
							"wel_upd_user='{$_SESSION['wel_user_']}',".
							"wel_upd_date=now() ".
						"WHERE wel_dn_no='$this->wel_dn_no' and wel_dn_line='$this->wel_dn_line'";	
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}

				if($dn_so_no!="")
				{
					$sql="UPDATE #__wel_sordetm SET ".
							"wel_dn_qty=IFNULL(wel_dn_qty,0)-$dn_req_qty+$this->wel_tran_qty ".
							"WHERE wel_so_no='$dn_so_no' AND wel_so_line='$dn_so_line' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				}

					$sql="UPDATE #__wel_dnhdrfm SET ".
						"wel_upd_user='{$_SESSION['wel_user_id']}',".
						"wel_upd_date=now() ".
						"WHERE wel_dn_no='$this->wel_dn_no'";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}

				mysql_query('commit');
					
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}
					
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}		
		if($msg_code==""){$msg_code="edit_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
}
?>