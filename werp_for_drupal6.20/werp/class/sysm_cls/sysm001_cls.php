<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT

?>
<?php
class sysm001_cls{
	public $wel_user_id="";
	public $wel_password="";
	public $confirm_wel_password="";
	public $wel_user_name="";
	public $wel_department="";
	public $wel_language_id="";
	public $wel_e_mail="";
	public $wel_group_code="";
	public $wel_prog_code="";
	public $wel_disable=0;
	public $wel_expire_date="";
	
	private $private_wel_prog_code="sysm001";

	public function read(){
		$msg_code="";
		$return_val=array();

		try{
			$conn=werp_db_connect();
			if (!check_permission($conn,$this->private_wel_prog_code,"wel_access_read")){throw new Exception("unauthorized_access");}

			$sql="SELECT * FROM #__wel_userflm WHERE wel_user_id='$this->wel_user_id' limit 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	//查询sql时出错了
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_user_id_not_found");}	//没有符合条件的记录
			$int__count=0;
			while ($int__count < mysql_num_fields($result)) {
				$field_name=mysql_fetch_field($result,$int__count)->name;
				$return_val[$field_name]=$row[$field_name];
				//$msg_code=$msg_code."|".$field_name;
				$int__count++;
			}
			mysql_free_result($result);
			throw new Exception("");
		}
		catch (Exception $e){
			$msg_code=$e->getMessage();
		}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	public function addnew(){
		$msg_code="";
		$return_val=array();

		try{
			$conn=werp_db_connect();
			if (!check_permission($conn,$this->private_wel_prog_code,"wel_access_addnew")){throw new Exception("unauthorized_access");}

			//$this->wel_expire_date=(($this->wel_expire_date=="")?"(DATE_ADD(now(),INTERVAL 30 DAY))":"'".$this->wel_expire_date."'");
			$this->wel_expire_date=(($this->wel_expire_date=="")?"null":"'".$this->wel_expire_date."'");
			
			if ($this->wel_user_id==""){throw new Exception("wel_user_id_miss");}
			if ($this->check_wel_userflm($conn,$this->wel_user_id)){throw new Exception("wel_user_id_exist");}
			if ($this->wel_user_name==""){throw new Exception("wel_user_name_miss");}

			if ($this->wel_password==""){throw new Exception("wel_password_miss");}
			if ($this->confirm_wel_password==""){throw new Exception("confirm_wel_password_miss");}
			if (strcmp($this->wel_password,$this->confirm_wel_password)!=0){throw new Exception("wel_password_not_same");}
			
			/*
			if ($this->wel_language_id==""){throw new Exception("wel_language_id_miss");}
			if (!$this->check_wel_language($conn,$this->wel_language_id)){throw new Exception("wel_language_id_not_found");}
			*/
			
			try{
				mysql_query('begin');
				$sql="INSERT INTO #__wel_userflm SET ".
					"wel_user_id='$this->wel_user_id',".
					"wel_user_name='$this->wel_user_name',".
					"wel_department='$this->wel_department',".
					"wel_language_id='$this->wel_language_id',".
					"wel_e_mail='$this->wel_e_mail',".
					"wel_password='$this->wel_password',".
					"wel_disable=$this->wel_disable,".
					"wel_expire_date=$this->wel_expire_date ";
				$sql=revert_to_the_available_sql($sql);
				if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				mysql_query('commit');
				$msg_code="addnew_succee";
			}catch (Exception $e){
				$msg_code=$e->getMessage();
				mysql_query('rollback');
			}
		}catch (Exception $e){
			$msg_code=$e->getMessage();
		}
		$return_val["msg_code"]=$msg_code;
		$return_val["wel_user_id"]=$this->wel_user_id;
		return $return_val;
	}
	
	public function edit(){
		$msg_code="";
		$return_val=array();

		try{
			$conn=werp_db_connect();
			if (!check_permission($conn,$this->private_wel_prog_code,"wel_access_edit")){throw new Exception("unauthorized_access");}

			//$this->wel_expire_date=(($this->wel_expire_date=="")?"(DATE_ADD(now(),INTERVAL 30 DAY))":"'".$this->wel_expire_date."'");
			$this->wel_expire_date=(($this->wel_expire_date=="")?"null":"'".$this->wel_expire_date."'");
			
			if ($this->wel_user_id==""){throw new Exception("wel_user_id_miss");}
			if (!$this->check_wel_userflm($conn,$this->wel_user_id)){throw new Exception("wel_user_id_not_found");}
			if ($this->wel_user_name==""){throw new Exception("wel_user_name_miss");}

			if (strcmp($this->wel_password,$this->confirm_wel_password)!=0){throw new Exception("wel_password_not_same");}

			/*
			if ($this->wel_language_id==""){throw new Exception("wel_language_id_miss");}
			if (!$this->check_wel_language($conn,$this->wel_language_id)){throw new Exception("wel_language_id_not_found");}
			*/
			
			try{
				mysql_query('begin');
				$sql="UPDATE #__wel_userflm SET ".
					"wel_user_name='$this->wel_user_name',".
					"wel_department='$this->wel_department',".
					"wel_language_id='$this->wel_language_id',".
					"wel_e_mail='$this->wel_e_mail',".
					"wel_disable=$this->wel_disable,".
					"wel_expire_date=$this->wel_expire_date ".
					(($this->wel_password=="")?" ":",wel_password='$this->wel_password' ").
					"WHERE wel_user_id='$this->wel_user_id'";
				$sql=revert_to_the_available_sql($sql);
				if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				mysql_query('commit');
				$msg_code="edit_succee";
			}catch (Exception $e){
				$msg_code=$e->getMessage();
				mysql_query('rollback');
			}
		}catch (Exception $e){
			$msg_code=$e->getMessage();
		}		
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	public function delete(){
		$msg_code="";
		$return_val=array();

		try{
			$conn=werp_db_connect();
			if (!check_permission($conn,$this->private_wel_prog_code,"wel_access_delete")){throw new Exception("unauthorized_access");}

			if ($this->wel_user_id==""){throw new Exception("wel_user_id_miss");}
			if (!$this->check_wel_userflm($conn,$this->wel_user_id)){throw new Exception("wel_user_id_not_found");}
			
			try{
				mysql_query('begin');
				$sql="DELETE FROM #__wel_userflm WHERE wel_user_id='$this->wel_user_id'";
				$sql=revert_to_the_available_sql($sql);
				if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				$sql="DELETE FROM #__wel_usergroup WHERE wel_user_id='$this->wel_user_id'";
				$sql=revert_to_the_available_sql($sql);
				if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				$sql="DELETE FROM #__wel_userprog WHERE wel_user_id='$this->wel_user_id'";
				$sql=revert_to_the_available_sql($sql);
				if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				mysql_query('commit');
				$msg_code="delete_succee";
			}catch (Exception $e){
				$msg_code=$e->getMessage();
				mysql_query('rollback');
			}
		}catch (Exception $e){
			$msg_code=$e->getMessage();
		}		
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	public function delete_group(){
		$msg_code="";
		$return_val=array();

		try{
			$conn=werp_db_connect();
			if (!check_permission($conn,$this->private_wel_prog_code,"wel_access_delete")){throw new Exception("unauthorized_access");}

			$sql="SELECT * from #__wel_usergroup WHERE ".
				"wel_user_id='$this->wel_user_id' and wel_group_code='$this->wel_group_code' limit 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}	//查询sql时出错了
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_group_code_not_found");}	//没有符合条件的记录
			mysql_free_result($result);
			
			try{
				mysql_query('begin');
				$sql="DELETE FROM #__wel_usergroup WHERE wel_user_id='$this->wel_user_id' and wel_group_code='$this->wel_group_code'";
				$sql=revert_to_the_available_sql($sql);
				if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				mysql_query('commit');
				$msg_code="delete_succee";
			}catch (Exception $e){
				$msg_code=$e->getMessage();
				mysql_query('rollback');
			}
		}catch (Exception $e){
			$msg_code=$e->getMessage();
		}		
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}

	public function delete_group_all(){
		$msg_code="";
		$return_val=array();

		try{
			$conn=werp_db_connect();
			if (!check_permission($conn,$this->private_wel_prog_code,"wel_access_delete")){throw new Exception("unauthorized_access");}

			$sql="SELECT * FROM #__wel_usergroup WHERE wel_user_id='$this->wel_user_id' limit 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}	//查询sql时出错了
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_group_code_not_found");}	//没有符合条件的记录
			mysql_free_result($result);
			
			try{
				mysql_query('begin');
				$sql="DELETE FROM #__wel_usergroup WHERE wel_user_id='$this->wel_user_id'";
				$sql=revert_to_the_available_sql($sql);
				if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				mysql_query('commit');
				$msg_code="delete_succee";
			}catch (Exception $e){
				$msg_code=$e->getMessage();
				mysql_query('rollback');
			}
		}catch (Exception $e){
			$msg_code=$e->getMessage();
		}		
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	public function delete_program(){
		$msg_code="";
		$return_val=array();

		try{
			$conn=werp_db_connect();
			if (!check_permission($conn,$this->private_wel_prog_code,"wel_access_delete")){throw new Exception("unauthorized_access");}

			$sql="SELECT * FROM #__wel_userprog WHERE ".
				"wel_user_id='$this->wel_user_id' and wel_prog_code='$this->wel_prog_code' limit 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}	//查询sql时出错了
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_prog_code_not_found");}	//没有符合条件的记录
			mysql_free_result($result);

			try{
				mysql_query('begin');
				$sql="DELETE FROM #__wel_userprog WHERE wel_user_id='$this->wel_user_id' and wel_prog_code='$this->wel_prog_code'";
				$sql=revert_to_the_available_sql($sql);
				if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				mysql_query('commit');
				$msg_code="delete_succee";
			}catch (Exception $e){
				$msg_code=$e->getMessage();
				mysql_query('rollback');
			}
		}catch (Exception $e){
			$msg_code=$e->getMessage();
		}		
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	public function delete_program_all(){
		$msg_code="";
		$return_val=array();

		try{
			$conn=werp_db_connect();
			if (!check_permission($conn,$this->private_wel_prog_code,"wel_access_delete")){throw new Exception("unauthorized_access");}

			$sql="SELECT * FROM #__wel_userprog WHERE wel_user_id='$this->wel_user_id' limit 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}	//查询sql时出错了
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_prog_code_not_found");}	//没有符合条件的记录
			mysql_free_result($result);
			
			try{
				mysql_query('begin');
				$sql="DELETE FROM #__wel_userprog WHERE wel_user_id='$this->wel_user_id'";
				$sql=revert_to_the_available_sql($sql);
				if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				mysql_query('commit');
				$msg_code="delete_succee";
			}catch (Exception $e){
				$msg_code=$e->getMessage();
				mysql_query('rollback');
			}
		}catch (Exception $e){
			$msg_code=$e->getMessage();
		}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	private function check_wel_language($conn,$wel_language_id){
		$check_found=false;
		if($wel_language_id=="" ) return true;//如果系空的,唔理它
		try{
			$sql="SELECT * FROM #__wel_language WHERE wel_language_id='$wel_language_id' limit 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());} //查询sql时出错了
			if(!($row=mysql_fetch_array($result))){throw new Exception("");}	//没有符合条件的记录
			mysql_free_result($result);
			$check_found=true;
		}catch(Exception $e){
		}
		return $check_found;
	}

	private function check_wel_userflm($conn,$wel_user_id){
		$check_found=false;
		if($wel_user_id=="" ) return true;//如果系空的,唔理它
		try{
			$sql="SELECT * FROM #__wel_userflm WHERE wel_user_id='$wel_user_id' limit 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());} //查询sql时出错了
			if(!($row=mysql_fetch_array($result))){throw new Exception("");}	//没有符合条件的记录
			mysql_free_result($result);
			$check_found=true;
		}catch(Exception $e){
		}
		return $check_found;
	}
}
?>