<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT

?>
<?php
class sysm001b_cls{
	public $wel_user_id="";
	public $wel_prog_code="";
	public $from_wel_user_id="";
	public $overwrite_old_setting=0;
	
	public $wel_access_read=0;
	public $wel_access_addnew=0;
	public $wel_access_edit=0;
	public $wel_access_delete=0;
	public $wel_access_approve=0;
	public $wel_access_print=0;
	
	private $private_wel_prog_code="sysm001";

	public function read(){
		$msg_code="";
		$return_val=array();

		try{
			$conn=werp_db_connect();
			if (!check_permission($conn,$this->private_wel_prog_code,"wel_access_read")){throw new Exception("unauthorized_access");}

			$sql="SELECT userprog.*,proghdrm.wel_prog_des ".
				"FROM #__wel_userprog AS userprog LEFT JOIN #__wel_proghdrm AS proghdrm ".
				"ON userprog.wel_prog_code=proghdrm.wel_prog_code WHERE ".
				"userprog.wel_user_id='$this->wel_user_id' and userprog.wel_prog_code='$this->wel_prog_code' limit 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	//查询sql时出错了
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_prog_code_not_found");}	//没有符合条件的记录
			$int__count=0;
			while ($int__count < mysql_num_fields($result)) {
				$field_name=mysql_fetch_field($result,$int__count)->name;
				$return_val[$field_name]=$row[$field_name];
				//$msg_code=$msg_code."|".$field_name;
				$int__count++;
			}
			mysql_free_result($result);
			throw new Exception("");
		}
		catch (Exception $e){
			$msg_code=$e->getMessage();
		}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	public function addnew(){
		$msg_code="";
		$return_val=array();

		try{
			$conn=werp_db_connect();
			if (!check_permission($conn,$this->private_wel_prog_code,"wel_access_addnew")){throw new Exception("unauthorized_access");}

			if ($this->wel_user_id==""){throw new Exception("wel_user_id_miss");}
			if (!$this->check_wel_userflm($conn,$this->wel_user_id)){throw new Exception("wel_user_id_not_found");}

			if ($this->from_wel_user_id!=""){
				if (!$this->check_wel_userflm($conn,$this->from_wel_user_id)){throw new Exception("from_wel_user_id_not_found");}
			}elseif ($this->wel_prog_code!=""){
				if (!$this->check_wel_proghdrm($conn,$this->wel_prog_code)){throw new Exception("wel_prog_code_not_found");}
			}else{
				throw new Exception("wel_prog_code_miss");
			}
			
			if ($this->from_wel_user_id!=""){
				$sql="SELECT wel_user_id FROM #__wel_userprog WHERE wel_user_id='$this->from_wel_user_id' limit 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	//查询sql时出错了
				//没有符合条件的记录
				if(!($row=mysql_fetch_array($result))){throw new Exception("from_wel_user_id_detail_not_found");}
				mysql_free_result($result);
				
				try{
					mysql_query('begin');
					if ($this->overwrite_old_setting=="0"){
						$sql="INSERT INTO #__wel_userprog(".
							"wel_user_id,wel_prog_code,wel_access_read,wel_access_addnew,".
							"wel_access_edit,wel_access_delete,wel_access_approve,wel_access_print) ".
							"SELECT '$this->wel_user_id',wel_prog_code,wel_access_read,wel_access_addnew,".
							"wel_access_edit,wel_access_delete,wel_access_approve,wel_access_print ".
							"FROM #__wel_userprog WHERE wel_user_id='$this->from_wel_user_id' and not wel_prog_code in ".
							"(SELECT wel_prog_code FROM #__wel_userprog WHERE wel_user_id='$this->wel_user_id')";
						$sql=revert_to_the_available_sql($sql);
					}else{
						$sql="DELETE FROM #__wel_userprog WHERE wel_user_id='$this->wel_user_id' and wel_prog_code in ".
							"(SELECT wel_prog_code FROM ".
							"(SELECT wel_prog_code FROM #__wel_userprog WHERE wel_user_id='$this->from_wel_user_id') ".
							"AS temp_userprog)";
						$sql=revert_to_the_available_sql($sql);
						if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}

						$sql="INSERT INTO #__wel_userprog(".
							"wel_user_id,wel_prog_code,wel_access_read,wel_access_addnew,".
							"wel_access_edit,wel_access_delete,wel_access_approve,wel_access_print) ".
							"SELECT '$this->wel_user_id',wel_prog_code,wel_access_read,wel_access_addnew,".
							"wel_access_edit,wel_access_delete,wel_access_approve,wel_access_print ".
							"FROM #__wel_userprog WHERE wel_user_id='$this->from_wel_user_id'";
						$sql=revert_to_the_available_sql($sql);
					}
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					mysql_query('commit');
					$msg_code="addnew_succee";
				}catch (Exception $e){
					$msg_code=$e->getMessage();
					mysql_query('rollback');
				}
			}else{
				$sql="SELECT wel_user_id FROM #__wel_userprog WHERE wel_user_id='$this->wel_user_id' and ".
					"wel_prog_code='$this->wel_prog_code' limit 1";
				$sql=revert_to_the_available_sql($sql);
				if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}	//查询sql时出错了
				if(($row=mysql_fetch_array($result))){throw new Exception("wel_prog_code_exist");}	//单据已存在
				mysql_free_result($result);
				
				try{
					mysql_query('begin');
					$sql="INSERT INTO #__wel_userprog SET ".
						"wel_user_id='$this->wel_user_id',".
						"wel_prog_code='$this->wel_prog_code',".
						"wel_access_read=$this->wel_access_read,".
						"wel_access_addnew=$this->wel_access_addnew,".
						"wel_access_edit=$this->wel_access_edit,".
						"wel_access_delete=$this->wel_access_delete,".
						"wel_access_approve=$this->wel_access_approve,".
						"wel_access_print=$this->wel_access_print ";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					mysql_query('commit');
					$msg_code="addnew_succee";
				}catch (Exception $e){
					$msg_code=$e->getMessage();
					mysql_query('rollback');
				}
			}
		}
		catch (Exception $e){
			$msg_code=$e->getMessage();
		}
		$return_val["msg_code"]=$msg_code;
		$return_val["wel_user_id"]=$this->wel_user_id;
		return $return_val;
	}
	
	public function edit(){
		$msg_code="";
		$return_val=array();

		try{
			$conn=werp_db_connect();
			if (!check_permission($conn,$this->private_wel_prog_code,"wel_access_edit")){throw new Exception("unauthorized_access");}

			if ($this->wel_user_id==""){throw new Exception("wel_user_id_miss");}
			if (!$this->check_wel_userflm($conn,$this->wel_user_id)){throw new Exception("wel_user_id_not_found");}

			if ($this->wel_prog_code==""){throw new Exception("wel_prog_code_miss");}
			if (!$this->check_wel_proghdrm($conn,$this->wel_prog_code)){throw new Exception("wel_prog_code_not_found");}
			
			$sql="SELECT wel_user_id FROM #__wel_userprog WHERE wel_user_id='$this->wel_user_id' and ".
				"wel_prog_code='$this->wel_prog_code' limit 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}	//查询sql时出错了
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_prog_code_not_found");}	//没有符合条件的记录
			mysql_free_result($result);
			
			try{
				mysql_query('begin');
				$sql="UPDATE #__wel_userprog SET ".
					"wel_access_read=$this->wel_access_read,".
					"wel_access_addnew=$this->wel_access_addnew,".
					"wel_access_edit=$this->wel_access_edit,".
					"wel_access_delete=$this->wel_access_delete,".
					"wel_access_approve=$this->wel_access_approve,".
					"wel_access_print=$this->wel_access_print ".
					"WHERE wel_user_id='$this->wel_user_id' and wel_prog_code='$this->wel_prog_code'";
				$sql=revert_to_the_available_sql($sql);
				if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				mysql_query('commit');
				$msg_code="edit_succee"; 
			}catch (Exception $e){
				$msg_code=$e->getMessage();
				mysql_query('rollback');
			}
		}
		catch (Exception $e){
			$msg_code=$e->getMessage();
		}		
		$return_val["msg_code"]=$msg_code;
		$return_val["wel_user_id"]=$this->wel_user_id;
		return $return_val;
	}
	
	private function check_wel_userflm($conn,$wel_user_id){
		$check_found=false;
		if($wel_user_id=="" ) return true;//如果系空的,唔理它
		try{
			$sql="SELECT * from #__wel_userflm WHERE wel_user_id='$wel_user_id' limit 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());} //查询sql时出错了
			if(!($row=mysql_fetch_array($result))){throw new Exception("");}	//没有符合条件的记录
			mysql_free_result($result);
			$check_found=true;
		}catch(Exception $e){
		}
		return $check_found;
	}
	
	private function check_wel_proghdrm($conn,$wel_prog_code){
		$check_found=false;
		if($wel_prog_code=="" ) return true;//如果系空的,唔理它
		try{
			$sql="SELECT * from #__wel_proghdrm WHERE wel_prog_code='$wel_prog_code' limit 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());} //查询sql时出错了
			if(!($row=mysql_fetch_array($result))){throw new Exception("");}	//没有符合条件的记录
			mysql_free_result($result);
			$check_found=true;
		}catch(Exception $e){
		}
		return $check_found;
	}
}
?>
