<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>

<?php
class sysm002_cls{
	public $wel_user_id="";
	public $wel_password="";
	public $new_wel_password="";
	public $confirm_wel_password="";
	
	private $wel_prog_code="sysm002";

	public function read(){
		$msg_code="";
		$return_val=array();

		try{
			$conn=werp_db_connect();
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_read")){throw new Exception("unauthorized_access");}

			$sql="SELECT * FROM #__wel_userflm WHERE wel_user_id='$this->wel_user_id' limit 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	//查询sql时出错了
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_user_id_not_found");}	//没有符合条件的记录
			$int__count=0;
			while ($int__count < mysql_num_fields($result)) {
				$field_name=mysql_fetch_field($result,$int__count)->name;
				$return_val[$field_name]=$row[$field_name];
				$int__count++;
			}
			mysql_free_result($result);
			throw new Exception("");
		}
		catch (Exception $e){
			$msg_code=$e->getMessage();
		}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	public function edit(){
		$msg_code="";
		$return_val=array();

		try{
			$conn=werp_db_connect();
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_edit")){throw new Exception("unauthorized_access");}

			if ($this->wel_user_id==""){throw new Exception("wel_user_id_miss");}
			if ($this->new_wel_password==""){throw new Exception("new_wel_password_miss");}
			if ($this->confirm_wel_password==""){throw new Exception("confirm_wel_password_miss");}
			if (strcmp($this->new_wel_password,$this->confirm_wel_password)!=0){
				throw new Exception("new_wel_password_not_same");
			}
			if (strlen($this->new_wel_password)<6){throw new Exception("new_wel_password_too_short");}
			
			$sql="SELECT * from #__wel_userflm WHERE wel_user_id='$this->wel_user_id' limit 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}	//查询sql时出错了
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_user_id_not_found");}	//没有符合条件的记录
			$wel_password=trim($row["wel_password"]."");
			if (strcmp($wel_password,$this->wel_password)!=0){throw new Exception("wel_password_not_right");}
			mysql_free_result($result);
			
			try{
				mysql_query('begin');
				$sql="UPDATE #__wel_userflm SET ".
					"wel_password='$this->new_wel_password' ".
					"WHERE wel_user_id='$this->wel_user_id'";
				$sql=revert_to_the_available_sql($sql);
				if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				mysql_query('commit');
				$msg_code="edit_succee";
			}catch (Exception $e1){
				$msg_code=$e1->getMessage();
				mysql_query('rollback');
			}
		}catch (Exception $e){
			$msg_code=$e->getMessage();
		}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
}
?>