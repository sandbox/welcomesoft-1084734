<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT

?>
<?php 
class sysm009_cls
{
	public $wel_root_code="";
	public $wel_parent_code="";
	public $wel_prog_code="";
	public $wel_working_dir="";
	public $wel_ordering=0;
	
	private $private_wel_prog_code="sysm009";
	
	public function ordering_read()
	{
		$msg_code="";
		$return_val=array();

		$value_text=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->private_wel_prog_code,"wel_access_read")){throw new Exception("unauthorized_access");}
			
			if($this->wel_parent_code==""){throw new Exception("wel_parent_code_miss");}
			
			$sql="SELECT 
				proglofm.wel_ordering,
				proglofm.wel_prog_code,
				proghdrm.wel_prog_des 
				FROM #__wel_proglofm AS proglofm LEFT JOIN #__wel_proghdrm AS proghdrm 
				ON proglofm.wel_prog_code=  proghdrm.wel_prog_code WHERE 
				proglofm.wel_parent_code='".$this->wel_parent_code."' ORDER BY wel_ordering ASC ";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	//查询sql时出错了
			while($row=mysql_fetch_array($result)){
				$value_text[$row["wel_ordering"]]=$row["wel_prog_code"]."(".$row["wel_prog_des"].")";
			}
			throw new Exception("");
		}
		catch (Exception $e){$msg_code=$e->getMessage();}
		$return_val["value_text"]=$value_text;
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	//detail读取数据
	public function detail_read()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->private_wel_prog_code,"wel_access_read")){throw new Exception("unauthorized_access");}
			
			if($this->wel_parent_code==""){throw new Exception("wel_parent_code_miss");}
			if($this->wel_prog_code==""){throw new Exception("wel_prog_code_miss");}
			
			$sql="SELECT proglofm.*,
				p___prog.wel_prog_des AS wel_parent_des,
				proghdrm.wel_prog_des AS wel_prog_des 
				FROM #__wel_proglofm AS proglofm LEFT JOIN #__wel_proghdrm AS p___prog 
				ON proglofm.wel_parent_code=p___prog.wel_prog_code LEFT JOIN #__wel_proghdrm AS proghdrm 
				ON proglofm.wel_prog_code=  proghdrm.wel_prog_code WHERE 
				proglofm.wel_parent_code='".$this->wel_parent_code."' AND 
				proglofm.wel_prog_code='".$this->wel_prog_code."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	//查询sql时出错了
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_prog_code_not_found");}	//没有符合条件的记录
			foreach ($row as $key=>$value){$return_val[$key]=$value;}
			throw new Exception("");
		}
		catch (Exception $e){$msg_code=$e->getMessage();}
		
		$ordering=$this->ordering_read();
		$return_val["value_text"]=$ordering["value_text"];
		
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	//detail 添加数据
	public function detail_addnew()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->private_wel_prog_code,"wel_access_addnew")){throw new Exception("unauthorized_access");}
			
			if($this->wel_parent_code==""){throw new Exception("wel_parent_code_miss");}
			if($this->wel_prog_code==""){throw new Exception("wel_prog_code_miss");}
			
			$sql="SELECT * FROM #__wel_proghdrm WHERE wel_prog_code='".$this->wel_prog_code."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_prog_code_not_found");}

			//在lof中已存在
			$sql="SELECT * FROM #__wel_proglofm ".
				"WHERE wel_parent_code='".$this->wel_parent_code."' AND wel_prog_code='".$this->wel_prog_code."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(($row=mysql_fetch_array($result))){throw new Exception("wel_prog_code_exist");}
			
			//lof是否循环
			if($this->lof_loop($this->wel_parent_code,$this->wel_prog_code)){
				throw new Exception("wel_lof_loop_error");
			}
			
			try
			{
				mysql_query('begin');
				
				if($this->wel_ordering==""){
					$sql="SELECT * FROM #__wel_proglofm ".
						"WHERE wel_parent_code='".$this->wel_parent_code."' ORDER BY wel_ordering DESC LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
					if(!($row=mysql_fetch_array($result))){}
					$this->wel_ordering=doubleval(is_null($row["wel_ordering"]) ? 0 : $row["wel_ordering"]);
				}
				$this->wel_ordering=$this->wel_ordering+1;
				
				$sql="UPDATE #__wel_proglofm SET 
					wel_ordering=wel_ordering + 1 
					WHERE wel_parent_code='".$this->wel_parent_code."' AND ".
					"wel_ordering>=".$this->wel_ordering;
				$sql=revert_to_the_available_sql($sql);
				if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}

				$sql="INSERT INTO #__wel_proglofm SET
					wel_parent_code='".$this->wel_parent_code."',
					wel_prog_code='".$this->wel_prog_code."',
					wel_working_dir='".$this->wel_working_dir."',
					wel_ordering='".$this->wel_ordering."' ";
				$sql=revert_to_the_available_sql($sql);
				if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				
				mysql_query('commit');
			}
			catch (Exception $e1){
				mysql_query('rollback');
				throw new Exception($e1->getMessage());
			}
		}
		catch (Exception $e){$msg_code=$e->getMessage();}
		if($msg_code==""){$msg_code="addnew_succee";}

		$ordering=$this->ordering_read();
		$return_val["value_text"]=$ordering["value_text"];

		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	//detail编辑数据
	public function detail_edit()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->private_wel_prog_code,"wel_access_edit")){throw new Exception("unauthorized_access");}
			
			if($this->wel_parent_code==""){throw new Exception("wel_parent_code_miss");}
			if($this->wel_prog_code==""){throw new Exception("wel_prog_code_miss");}
			
			$sql="SELECT * FROM #__wel_proghdrm WHERE wel_prog_code='".$this->wel_prog_code."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_prog_code_not_found");}

			//在lof中已存在
			$sql="SELECT * FROM #__wel_proglofm ".
				"WHERE wel_parent_code='".$this->wel_parent_code."' AND wel_prog_code='".$this->wel_prog_code."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_prog_code_not_found");}
			$wel_ordering=$row["wel_ordering"];
			
			//lof是否循环
			if($this->lof_loop($this->wel_parent_code,$this->wel_prog_code)){
				throw new Exception("wel_lof_loop_error");
			}
			
			try
			{
				mysql_query('begin');

				if($this->wel_ordering==""){
					$sql="SELECT * FROM #__wel_proglofm ".
						"WHERE wel_parent_code='".$this->wel_parent_code."' ORDER BY wel_ordering DESC LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
					if(!($row=mysql_fetch_array($result))){throw new Exception("wel_prog_code_not_found");}
					$this->wel_ordering=doubleval(is_null($row["wel_ordering"]) ? 0 : $row["wel_ordering"]);
				}
				if($this->wel_ordering>$wel_ordering){
					$sql="UPDATE #__wel_proglofm SET 
						wel_ordering=wel_ordering - 1 
						WHERE wel_parent_code='".$this->wel_parent_code."' AND ".
						$wel_ordering."<wel_ordering AND wel_ordering<=".$this->wel_ordering;
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				}
				if($this->wel_ordering<$wel_ordering){
					$sql="UPDATE #__wel_proglofm SET 
						wel_ordering=wel_ordering + 1 
						WHERE wel_parent_code='".$this->wel_parent_code."' AND ".
						$wel_ordering.">wel_ordering AND wel_ordering>=".$this->wel_ordering;
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				}

				$sql="UPDATE #__wel_proglofm SET 
					wel_working_dir='".$this->wel_working_dir."',
					wel_ordering='".$this->wel_ordering."' 
					WHERE wel_parent_code='".$this->wel_parent_code."' AND wel_prog_code='".$this->wel_prog_code."'";
				$sql=revert_to_the_available_sql($sql);
				if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				
				mysql_query('commit');
			}
			catch (Exception $e1){
				mysql_query('rollback');
				throw new Exception($e1->getMessage());
			}
		}
		catch (Exception $e){$msg_code=$e->getMessage();}		
		if($msg_code==""){$msg_code="edit_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	//lof细节删除
	public function detail_del()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->private_wel_prog_code,"wel_access_delete")){throw new Exception("unauthorized_access");}
			
			if($this->wel_parent_code==""){throw new Exception("wel_parent_code_miss");}
			if($this->wel_prog_code==""){throw new Exception("wel_prog_code_miss");}
			
			//在lof中已存在
			$sql="SELECT * FROM #__wel_proglofm ".
				"WHERE wel_parent_code='".$this->wel_parent_code."' AND wel_prog_code='".$this->wel_prog_code."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_prog_code_not_found");}

			try
			{
				mysql_query('begin');

				$sql="DELETE FROM #__wel_proglofm 
					WHERE wel_parent_code='".$this->wel_parent_code."' AND wel_prog_code='".$this->wel_prog_code."'";
				$sql=revert_to_the_available_sql($sql);
				if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}

				mysql_query('commit');
			}
			catch (Exception $e1){
				mysql_query('rollback');
				throw new Exception($e1->getMessage());
			}
		}
		catch (Exception $e){$msg_code=$e->getMessage();}		
		if($msg_code==""){$msg_code="delete_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	//检查lof是否会循环
	private function lof_loop($wel_parent_code,$wel_prog_code)
	{
		try 
		{
			$conn=werp_db_connect();
			
			$sql="SELECT wel_parent_code FROM #__wel_proglofm WHERE wel_prog_code='".$wel_parent_code."'";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			while($row=mysql_fetch_array($result)){
				$wel_parent_code=is_null($row["wel_parent_code"]) ? "" : $row["wel_parent_code"];
				if(strtoupper($wel_parent_code)==strtoupper($wel_prog_code)){return true;}
				if($this->lof_loop($wel_parent_code,$wel_prog_code)){return true;}
			}
		}
		catch (Exception $e){return true;}
		return false;
	}
}
?>