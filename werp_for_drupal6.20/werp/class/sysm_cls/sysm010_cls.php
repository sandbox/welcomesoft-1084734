<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT

?>
<?php
class sysm010_cls{
	public $wel_group_code="";
	public $wel_group_des="";
	public $wel_user_id="";
	public $wel_prog_code="";
	
	private $private_wel_prog_code="sysm010";

	public function read(){
		$msg_code="";
		$return_val=array();

		try{
			$conn=werp_db_connect();
			if (!check_permission($conn,$this->private_wel_prog_code,"wel_access_read")){throw new Exception("unauthorized_access");}

			$sql="SELECT * FROM #__wel_groupflm WHERE wel_group_code='$this->wel_group_code' limit 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	//查询sql时出错了
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_group_code_not_found");}	//没有符合条件的记录
			$int__count=0;
			while ($int__count < mysql_num_fields($result)) {
				$field_name=mysql_fetch_field($result,$int__count)->name;
				$return_val[$field_name]=$row[$field_name];
				//$msg_code=$msg_code."|".$field_name;
				$int__count++;
			}
			mysql_free_result($result);
			throw new Exception("");
		}
		catch (Exception $e){
			$msg_code=$e->getMessage();
		}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	public function addnew(){
		$msg_code="";
		$return_val=array();

		try{
			$conn=werp_db_connect();
			if (!check_permission($conn,$this->private_wel_prog_code,"wel_access_addnew")){throw new Exception("unauthorized_access");}

			if ($this->wel_group_code==""){throw new Exception("wel_group_code_miss");}
			if ($this->check_wel_groupflm($conn,$this->wel_group_code)){throw new Exception("wel_group_code_exist");}
			if ($this->wel_group_des==""){throw new Exception("wel_group_des_miss");}

			try{
				mysql_query('begin');
				$sql="INSERT INTO #__wel_groupflm SET ".
					"wel_group_code='$this->wel_group_code',".
					"wel_group_des='$this->wel_group_des' ";
				$sql=revert_to_the_available_sql($sql);
				if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				mysql_query('commit');
				$msg_code="addnew_succee";
			}catch (Exception $e){
				$msg_code=$e->getMessage();
				mysql_query('rollback');
			}
		}catch (Exception $e){
			$msg_code=$e->getMessage();
		}
		$return_val["msg_code"]=$msg_code;
		$return_val["wel_group_code"]=$this->wel_group_code;
		return $return_val;
	}
	
	public function edit(){
		$msg_code="";
		$return_val=array();

		try{
			$conn=werp_db_connect();
			if (!check_permission($conn,$this->private_wel_prog_code,"wel_access_edit")){throw new Exception("unauthorized_access");}

			if ($this->wel_group_code==""){throw new Exception("wel_group_code_miss");}
			if (!$this->check_wel_groupflm($conn,$this->wel_group_code)){throw new Exception("wel_group_code_not_found");}
			if ($this->wel_group_des==""){throw new Exception("wel_group_des_miss");}
			
			try{		
				mysql_query('begin');
				$sql="UPDATE #__wel_groupflm SET ".
					"wel_group_des='$this->wel_group_des' ".
					"WHERE wel_group_code='$this->wel_group_code' ";
				$sql=revert_to_the_available_sql($sql);
				if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				mysql_query('commit');
				$msg_code="edit_succee";
			}catch (Exception $e){
				$msg_code=$e->getMessage();
				mysql_query('rollback');
			}
		}catch (Exception $e){
			$msg_code=$e->getMessage();
		}		
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	public function delete(){
		$msg_code="";
		$return_val=array();

		try{
			$conn=werp_db_connect();
			if (!check_permission($conn,$this->private_wel_prog_code,"wel_access_delete")){throw new Exception("unauthorized_access");}

			if ($this->wel_group_code==""){throw new Exception("wel_group_code_miss");}
			if (!$this->check_wel_groupflm($conn,$this->wel_group_code)){throw new Exception("wel_group_code_not_found");}
			
			try{
				mysql_query('begin');
				$sql="DELETE FROM #__wel_groupflm WHERE wel_group_code='$this->wel_group_code'";
				$sql=revert_to_the_available_sql($sql);
				if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				$sql="DELETE FROM #__wel_usergroup WHERE wel_group_code='$this->wel_group_code'";
				$sql=revert_to_the_available_sql($sql);
				if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				$sql="DELETE FROM #__wel_groupprog WHERE wel_group_code='$this->wel_group_code'";
				$sql=revert_to_the_available_sql($sql);
				if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				mysql_query('commit');
				$msg_code="delete_succee";
			}catch (Exception $e){
				$msg_code=$e->getMessage();
				mysql_query('rollback');
			}
		}catch (Exception $e){
			$msg_code=$e->getMessage();
		}		
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	public function delete_user(){
		$msg_code="";
		$return_val=array();

		try{
			$conn=werp_db_connect();
			if (!check_permission($conn,$this->private_wel_prog_code,"wel_access_delete")){throw new Exception("unauthorized_access");}

			$sql="SELECT * FROM #__wel_usergroup WHERE ".
				"wel_user_id='$this->wel_user_id' and wel_group_code='$this->wel_group_code' limit 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}	//查询sql时出错了
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_user_id_not_found");}	//没有符合条件的记录
			mysql_free_result($result);
			
			try{
				mysql_query('begin');
				$sql="DELETE FROM #__wel_usergroup WHERE wel_user_id='$this->wel_user_id' and wel_group_code='$this->wel_group_code'";
				$sql=revert_to_the_available_sql($sql);
				if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				mysql_query('commit');
				$msg_code="delete_succee";
			}catch (Exception $e){
				$msg_code=$e->getMessage();
				mysql_query('rollback');
			}
		}catch (Exception $e){
			$msg_code=$e->getMessage();
		}		
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}

	public function delete_user_all(){
		$msg_code="";
		$return_val=array();

		try{
			$conn=werp_db_connect();
			if (!check_permission($conn,$this->private_wel_prog_code,"wel_access_delete")){throw new Exception("unauthorized_access");}

			$sql="SELECT * FROM #__wel_usergroup WHERE wel_group_code='$this->wel_group_code' limit 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}	//查询sql时出错了
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_user_id_not_found");}	//没有符合条件的记录
			mysql_free_result($result);
			
			try{
				mysql_query('begin');
				$sql="DELETE FROM #__wel_usergroup WHERE wel_group_code='$this->wel_group_code'";
				$sql=revert_to_the_available_sql($sql);
				if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				mysql_query('commit');
				$msg_code="delete_succee";
			}catch (Exception $e){
				$msg_code=$e->getMessage();
				mysql_query('rollback');
			}
		}catch (Exception $e){
			$msg_code=$e->getMessage();
		}		
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	public function delete_program(){
		$msg_code="";
		$return_val=array();

		try{
			$conn=werp_db_connect();
			if (!check_permission($conn,$this->private_wel_prog_code,"wel_access_delete")){throw new Exception("unauthorized_access");}

			$sql="SELECT * FROM #__wel_groupprog WHERE ".
				"wel_group_code='$this->wel_group_code' and wel_prog_code='$this->wel_prog_code' limit 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}	//查询sql时出错了
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_prog_code_not_found");}	//没有符合条件的记录
			mysql_free_result($result);
			
			try{
				mysql_query('begin');
				$sql="DELETE FROM #__wel_groupprog WHERE wel_group_code='$this->wel_group_code' and wel_prog_code='$this->wel_prog_code'";
				$sql=revert_to_the_available_sql($sql);
				if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				mysql_query('commit');
				$msg_code="delete_succee";
			}catch (Exception $e){
				$msg_code=$e->getMessage();
				mysql_query('rollback');
			}
		}catch (Exception $e){
			$msg_code=$e->getMessage();
		}		
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	public function delete_program_all(){
		$msg_code="";
		$return_val=array();

		try{
			$conn=werp_db_connect();
			if (!check_permission($conn,$this->private_wel_prog_code,"wel_access_delete")){throw new Exception("unauthorized_access");}

			$sql="SELECT * FROM #__wel_groupprog WHERE wel_group_code='$this->wel_group_code' limit 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}	//查询sql时出错了
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_prog_code_not_found");}	//没有符合条件的记录
			mysql_free_result($result);
			
			try{
				mysql_query('begin');
				$sql="DELETE FROM #__wel_groupprog WHERE wel_group_code='$this->wel_group_code'";
				$sql=revert_to_the_available_sql($sql);
				if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				mysql_query('commit');
				$msg_code="delete_succee";
			}catch (Exception $e){
				$msg_code=$e->getMessage();
				mysql_query('rollback');
			}
		}catch (Exception $e){
			$msg_code=$e->getMessage();
		}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	private function check_wel_groupflm($conn,$wel_group_code){
		$check_found=false;
		if($wel_group_code=="" ) return true;//如果系空的,唔理它
		try{
			$sql="SELECT * from #__wel_groupflm WHERE wel_group_code='$wel_group_code' limit 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());} //查询sql时出错了
			if(!($row=mysql_fetch_array($result))){throw new Exception("");}	//没有符合条件的记录
			mysql_free_result($result);
			$check_found=true;
		}catch(Exception $e){
		}
		return $check_found;
	}
}
?>