<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
global $base_url;	//defined('_JEXEC') or die('Restricted access');

define( 'WERP_EXEC', 1 );

define('WERP_VERSION', '0.1');
define('WERP_COPYRIGHT', 
	'<div id="footer">Copyright &copy; 2010 '.
	'<a target="_blank" href="http://welcomesoft.org">Welcome Soft Limited. </a>'.
	'All rights reserved. </div>');

//date_default_timezone_set("Asia/Hong_Kong");
date_default_timezone_set("PRC");

if (!defined("DS")){define( 'DS', DIRECTORY_SEPARATOR );}
//only use for show internal develop information
define("WEL_DEVELOP_INFO",0);

define('WERP_BASE_URI', $base_url."/");	//http://localhost/werp/
define('WERP_FOLL_URI', 'sites/all/modules/werp/');
define('WERP_SITE_URI', WERP_BASE_URI.WERP_FOLL_URI);

define('WERP_EXPA_PARA', '?q=werp');

define('WERP_BASE_PATH', dirname($_SERVER["SCRIPT_FILENAME"]).DS);	//C:\xampp\htdocs\werp\
define('WERP_FOLL_PATH', 'sites'.DS.'all'.DS.'modules'.DS.'werp'.DS);
define('WERP_SITE_PATH', WERP_BASE_PATH.WERP_FOLL_PATH);

define("WERP_SITE_PATH_STDLIB",WERP_SITE_PATH."stdlib".DS);

?>