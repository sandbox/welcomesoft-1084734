<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>
<?php
html_heading();
eval(rebuild_layout(__FILE__));
//Draw Body Layout
?>

<script language="javascript">
//var hidden_object_id_list;	//隐藏对象的列表
//var object_id_list;	//所有对象的列表
//权限基本只有 read addnew edit delete approve print 六种
//var access_read;		//读取权限
//var access_addnew;	//新增权限
//var access_edit;		//编辑权限
//var access_delete;	//删除权限
//var access_approve;	//批核权限
//var access_print;		//打印权限
//以上js变量无需设定，已经由类库自动产生，直接使用即可

var external_opt_action="";
var action_page="";
var wel_part_no="";
var dim_object_id_list="";
var security_button="";

$(document).ready(function(){
	//opt_action操作状态
	//外部要求的操作
	external_opt_action="<?php echo werp_get_request_var("opt_action"); ?>";
	action_page="<?php echo werp_pathinfo_filename(__FILE__); ?>_opt.php";
	wel_part_no="<?php echo werp_get_request_var("txt_wel_part_no"); ?>";
	//一直处于暗淡的对象列表(无法编辑的对象)
	dim_object_id_list="ntxt_wel_assort_qty|ntxt_wel_lat_cost|ntxt_wel_vqt_cost|ntxt_wel_max_pur_cost|"+
		"ntxt_wel_min_pur_cost";
	//要用权限控制的按钮列表
	security_button="btn_head_addnew|btn_head_edit|btn_head_del|btn_head_next|btn_head_save|"+
		"btn_head_cancel";
});

function return_handler_info(return_message)
{
	//window.alert(return_message);	//显示所有信息供测试时查看
	var ret_msg_arr=return_message.split("|");
	if (ret_msg_arr.length<5){window.alert(return_message);return false;}
	for (var i=0;i<ret_msg_arr.length;i++){ret_msg_arr[i]=un_coding_str(ret_msg_arr[i]);}
	var opt_action=ret_msg_arr[1];
	var msg_code=ret_msg_arr[2];
	var msg_detail=ret_msg_arr[3];
	var msg_script=ret_msg_arr[4];
	switch(opt_action)
	{
		case "addnew":	
		case "edit":
		case "btn_head_del_click":
		case "btn_image_upload_click":
		case "bbtn_wel_part_no_load_click":
			if (msg_detail!==""){window.alert(msg_detail);}
			eval(msg_script);//执行返回后产生的脚本
			break;
		default:
			if (msg_detail!==""){window.alert(msg_detail);}eval(msg_script);
			break;
	}
}

function bbtn_wel_part_no_load_click()
{
	if (document.getElementById("txt_wel_part_no").value.trim()==""){return;}
	var url=get_url_parameter(action_page,"bbtn_wel_part_no_load_click",object_id_list);
	var handler=new net.content_loader(url,return_handler_info);
}
$(document).ready(function(){bind_event("bbtn_wel_part_no_load","click",bbtn_wel_part_no_load_click);});

function btn_head_addnew_click()
{
	wel_part_no="";

	clear_screen_layout(object_id_list);
	format_number_el("ntxt_wel_eng_unit_rate");
	format_number_el("ntxt_wel_pur_unit_rate");
	format_number_el("ntxt_wel_pur_unit_rate1");
	format_number_el("ntxt_wel_pur_unit_rate2");
	format_number_el("ntxt_wel_scp_fact");
	format_number_el("ntxt_wel_assort_qty");

	format_number_el("ntxt_wel_avg_cost");
	format_number_el("ntxt_wel_std_cost");
	format_number_el("ntxt_wel_lat_cost");
	format_number_el("ntxt_wel_vqt_cost");
	format_number_el("ntxt_wel_max_pur_cost");
	format_number_el("ntxt_wel_min_pur_cost");
	format_number_el("ntxt_wel_pkg_ord");
	format_number_el("ntxt_wel_pur_gday");
	format_number_el("ntxt_wel_min_ord");
	format_number_el("ntxt_wel_lead_tm");

	format_number_el("ntxt_wel_manu_tm");
	format_number_el("ntxt_wel_min_stk");
	format_number_el("ntxt_wel_max_stk");
	format_number_el("ntxt_wel_open_pr");
	format_number_el("ntxt_wel_pmclead_tm");
	format_number_el("ntxt_wel_serv_day");
	format_number_el("ntxt_wel_issue_qty");
	format_number_el("ntxt_wel_std_lot_qty");
	
	format_number_el("ntxt_wel_net_wt");
	format_number_el("ntxt_wel_grs_wt");
	format_number_el("ntxt_wel_length");
	format_number_el("ntxt_wel_width");
	format_number_el("ntxt_wel_height");
	format_number_el("ntxt_wel_cbm");
	format_number_el("ntxt_wel_pcs_per");
	document.getElementById("chk_wel_checkyn").checked=true;
	$('#head_tab_list>ul').tabs('select',0);
	enable_object(object_id_list,true,"");
	enable_object(dim_object_id_list,false,"");
	enable_object(security_button,false,"");
	enable_object("btn_head_save|btn_head_cancel",true,
		access_addnew+"|"+access_read);
	enable_object("ftxt_wel_part_image|btn_image_upload",false,"");
	view_image("#");
}
$(document).ready(function(){bind_event("btn_head_addnew","click",btn_head_addnew_click);});

function btn_head_edit_click()
{
	enable_object(object_id_list,true,"");
	enable_object(dim_object_id_list,false,"");
	enable_object(security_button,false,"");
	enable_object("txt_wel_unit|bbtn_wel_unit",false,"");
	enable_object("txt_wel_part_no|bbtn_wel_part_no_load|bbtn_wel_part_no",false,"");
	enable_object("btn_head_save|btn_head_cancel",true,
		access_edit+"|"+access_read);
}
$(document).ready(function(){bind_event("btn_head_edit","click",btn_head_edit_click);});

function btn_head_del_click()
{
	var confirm_message=extract_message("delete_confirm");
	confirm_message=confirm_message.replace("s1",$("#txt_wel_part_no").attr("value"));
	if (!window.confirm(confirm_message)){return;}
	var url=get_url_parameter(action_page,"btn_head_del_click",object_id_list);
	var handler=new net.content_loader(url,return_handler_info);
}
$(document).ready(function(){bind_event("btn_head_del","click",btn_head_del_click);});

function btn_head_next_click()
{
	clear_screen_layout(object_id_list);
	enable_object(object_id_list,false,"");
	enable_object(dim_object_id_list,false,"");
	enable_object(security_button,false,"");
	enable_object("txt_wel_part_no|bbtn_wel_part_no_load|bbtn_wel_part_no",true,"");
	enable_object("btn_head_addnew",true,access_addnew);
	$("#head_tab_list > ul").tabs({selected: 0});
}
$(document).ready(function(){bind_event("btn_head_next","click",btn_head_next_click);});

function btn_head_save_click()
{
	if(wel_part_no==""){opt_action="addnew";}else{opt_action="edit";}
	var url=get_url_parameter(action_page,opt_action,object_id_list);
	var handler=new net.content_loader(url,return_handler_info);
}
$(document).ready(function(){bind_event("btn_head_save","click",btn_head_save_click);});

function btn_head_cancel_click()
{
	if (wel_part_no!="")
	{
		bbtn_wel_part_no_load_click();
	}
	else
	{
		btn_head_next_click();
		/*
		enable_object(object_id_list,true,"");
		enable_object(dim_object_id_list,false,"");
		enable_object(security_button,false,"");
		enable_object("btn_head_addnew|btn_head_save|btn_head_cancel",true,
			access_addnew+"|"+access_addnew+"|"+access_addnew);
		enable_object("btn_image_upload",false,"");
		*/
	}
}
$(document).ready(function(){bind_event("btn_head_cancel","click",btn_head_cancel_click);});

function btn_image_upload_click()
{
	if (document.getElementById("txt_wel_part_no").value.trim()==""){return;}
	var url=get_url_parameter(action_page,"btn_image_upload_click",object_id_list);
	document.getElementById("frm_upload").action=url;
	document.getElementById("frm_upload").submit();
}
$(document).ready(function(){bind_event("btn_image_upload","click",btn_image_upload_click);});

function view_image(str__image)
{
	$('#img_wel_view').attr('src',str__image);
}

$(document).ready(function()
{
	if (external_opt_action=="")
	{
		btn_head_next_click();
	}
});
</script>
<?php
	html_footer();
?>
