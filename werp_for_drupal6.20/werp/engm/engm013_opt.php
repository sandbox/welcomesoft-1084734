<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>
<?php
eval(receipt_url_parameter($_GET,$_POST));	//将参数的值放入相应以参数为名的变量中
//echo receipt_url_parameter($_GET,$_POST);
eval(read_cache_from_wel_wrcache($main_page."msg.inc"));
require_once(WERP_SITE_PATH_CLASS.$main_page."_cls.php");

//$opt_action,$msg_code,$msg_detail,$msg_script 为标准定义，用coding_str编码后以
//"|".$opt_action."|".$msg_code."|".$msg_detail."|".$msg_script  格式作为信息返回给回调函数
$opt_action=strtolower($opt_action);	//调用者的动作
$msg_code="";	//调用后产生的信息代码
$msg_detail="";	//信息代码对应的细节内容
$msg_script="";	//要执行的脚本

switch ($opt_action)
{
	//读取工程更改通知档案
	//=====================================================================
	case "bbtn_wel_ecn_no_load_click":
		$cls_engm013=new engm013_cls();
		$cls_engm013->wel_ecn_no=$txt_wel_ecn_no;
		
		$return_val=$cls_engm013->read();
		$msg_code=$return_val["msg_code"];
		$msg_detail=extract_message($msg_code);
		if ($msg_code=="")
		{
			$msg_script="clear_screen_layout(object_id_list);\n".
				"wel_engbomh_sql_grid('".$return_val["wel_ecn_no"]."');\n".
				"document.getElementById('txt_wel_ecn_no').value='".format_slashes($return_val["wel_ecn_no"])."';\n".
				"document.getElementById('txt_wel_pattern').value='".format_slashes($return_val["wel_pattern"])."';\n".
				"document.getElementById('dtxt_wel_ecn_date').value='".format_slashes($return_val["wel_ecn_date"])."';\n".
				"document.getElementById('ntxt_wel_rev_no').value='".format_slashes($return_val["wel_rev_no"])."';\n".
				"document.getElementById('txt_wel_proj_no').value='".format_slashes($return_val["wel_proj_no"])."';\n".
				"document.getElementById('txt_wel_proj_des').value='".format_slashes($return_val["wel_proj_des"])."';\n".
				"document.getElementById('rmk_wel_ecn_rmk').value='".format_slashes($return_val["wel_ecn_rmk"])."';\n".
				
				"document.getElementById('txt_wel_crt_user').value='".format_slashes($return_val["wel_crt_user"])."';\n".
				"document.getElementById('dtxt_wel_crt_date').value='".format_slashes($return_val["wel_crt_date"])."';\n".
				"document.getElementById('txt_wel_upd_user').value='".format_slashes($return_val["wel_upd_user"])."';\n".
				"document.getElementById('dtxt_wel_upd_date').value='".format_slashes($return_val["wel_upd_date"])."';\n".
				"document.getElementById('chk_wel_appr_yn').checked=to_boolean('".$return_val["wel_appr_yn"]."',false);\n".
				"document.getElementById('txt_wel_appr_by').value='".format_slashes($return_val["wel_appr_by"])."';\n".
				"document.getElementById('dtxt_wel_appr_date').value='".format_slashes($return_val["wel_appr_date"])."';\n".
				"document.getElementById('chk_wel_post_yn').checked=to_boolean('".$return_val["wel_post_yn"]."',false);\n".
				"document.getElementById('txt_wel_post_by').value='".format_slashes($return_val["wel_post_by"])."';\n".
				"document.getElementById('dtxt_wel_post_date').value='".format_slashes($return_val["wel_post_date"])."';\n".
				"format_date_el('dtxt_wel_ecn_date');\n".
				"format_date_el('dtxt_wel_crt_date');\n".
				"format_date_el('dtxt_wel_upd_date');\n".
				"format_date_el('dtxt_wel_appr_date');\n".
				"format_date_el('dtxt_wel_post_date');\n".
				"wel_ecn_no=document.getElementById('txt_wel_ecn_no').value;\n";
			
			$msg_script .=
				"format_number_el('ntxt_wel_rev_no');\n".
				"enable_object(object_id_list,false,'');\n".
				"enable_object('btn_head_edit|btn_head_del|btn_head_next|".
				"btn_head_approve|btn_head_not_approve|btn_head_print|".
				"btn_detail_addnew|btn_detail_edit|btn_detail_del|btn_detail_del_all',true,".
				"access_edit+'|'+access_delete+'|'+access_read+'|'+".
				"access_approve+'|'+access_approve+'|'+access_print+'|' +".
				"access_addnew+'|'+access_edit+'|'+access_delete+'|'+access_delete);\n";

			if($return_val["wel_post_yn"]==1)
			{
				$msg_script .="enable_object('btn_head_edit|btn_head_del|btn_head_approve|btn_head_not_approve|".
					"btn_detail_addnew|btn_detail_edit|btn_detail_del|btn_detail_del_all',false,'');";
			}
			else
			{
				if($return_val["wel_appr_yn"]==1)
				{
					$msg_script .="enable_object('btn_head_edit|btn_head_del|btn_head_approve|".
						"btn_detail_addnew|btn_detail_edit|btn_detail_del|btn_detail_del_all',false,'');";
				}
				else
				{
					$msg_script.="enable_object('btn_head_not_approve',false,'');";
				}
			}
		}
		break;
		
	//=====================================================================
	//新增工程更改通知档案
	case "addnew":
		$cls_engm013=new engm013_cls();
		$cls_engm013->wel_ecn_no=$txt_wel_ecn_no;
		$cls_engm013->wel_pattern=$txt_wel_pattern;
		$cls_engm013->wel_ecn_date=$dtxt_wel_ecn_date;
		$cls_engm013->wel_rev_no=$ntxt_wel_rev_no;
		$cls_engm013->wel_proj_no=$txt_wel_proj_no;
		$cls_engm013->wel_ecn_rmk=$rmk_wel_ecn_rmk;
		
		$return_val=$cls_engm013->addnew();
		$msg_code=$return_val["msg_code"];
		$msg_detail=extract_message($msg_code);
		if ($msg_code=="addnew_succee")
		{
			$msg_script=
				"document.getElementById('txt_wel_ecn_no').value='".format_slashes($return_val["wel_ecn_no"])."';\n".
				"bbtn_wel_ecn_no_load_click();\n";
		}
		break;
	
	//=====================================================================
	//编辑工程更改通知档案
	case "edit":
		$cls_engm013=new engm013_cls();
		$cls_engm013->wel_ecn_no=$txt_wel_ecn_no;
		$cls_engm013->wel_pattern=$txt_wel_pattern;
		$cls_engm013->wel_ecn_date=$dtxt_wel_ecn_date;
		$cls_engm013->wel_rev_no=$ntxt_wel_rev_no;
		$cls_engm013->wel_proj_no=$txt_wel_proj_no;
		$cls_engm013->wel_ecn_rmk=$rmk_wel_ecn_rmk;
		
		$return_val=$cls_engm013->edit();
		$msg_code=$return_val["msg_code"];
		$msg_detail=extract_message($msg_code);
		if ($msg_code=="edit_succee")
		{
			$msg_script="bbtn_wel_ecn_no_load_click();\n";
		}
		break;
	
	//=====================================================================
	//删除工程更改通知档案
	case "btn_head_del_click":
		$cls_engm013=new engm013_cls();
		$cls_engm013->wel_ecn_no=$txt_wel_ecn_no;
		
		$return_val=$cls_engm013->delete();
		$msg_code=$return_val["msg_code"];
		$msg_detail=extract_message($msg_code);
		if ($msg_code=="delete_succee")
		{
			$msg_script="btn_head_next_click();\n";
		}
		break;
	
	//=====================================================================
	//工程更改通知档案批核
	case "btn_head_approve_click":
		$cls_engm013=new engm013_cls();
		$cls_engm013->wel_ecn_no=$txt_wel_ecn_no;
		
		$return_val=$cls_engm013->approve();
		$msg_code=$return_val["msg_code"];
		$msg_detail=extract_message($msg_code);
		if ($msg_code=="approve_succee")
		{
			$msg_script="bbtn_wel_ecn_no_load_click();\n";
		}
		break;
	
	//=====================================================================
	//工程更改通知档案取消批核
	case "btn_head_not_approve_click":
		$cls_engm013=new engm013_cls();
		$cls_engm013->wel_ecn_no=$txt_wel_ecn_no;
		
		$return_val=$cls_engm013->not_approve();
		$msg_code=$return_val["msg_code"];
		$msg_detail=extract_message($msg_code);
		if ($msg_code=="not_approve_succee")
		{
			$msg_script="bbtn_wel_ecn_no_load_click();\n";
		}
		break;
	
	//=====================================================================
	//工程更改通知档案细节删除
	case "btn_detail_del_click":
		$cls_engm013=new engm013_cls();
		$cls_engm013->wel_ecn_no=$txt_wel_ecn_no;
		$cls_engm013->wel_assm_no=$txt_wel_assm_no;
		$cls_engm013->wel_part_no=$txt_wel_part_no;
		
		$return_val=$cls_engm013->detail_delete();
		$msg_code=$return_val["msg_code"];
		$msg_detail=extract_message($msg_code);
		if ($msg_code=="delete_succee")
		{
			$msg_script="wel_engbomh_sql_selected_del();\n";
		}
		break;
	
	//=====================================================================
	//工程更改通知档案细节全部删除
	case "btn_detail_del_all_click":
		$cls_engm013=new engm013_cls();
		$cls_engm013->wel_ecn_no=$txt_wel_ecn_no;
		
		$return_val=$cls_engm013->detail_delete_all();
		$msg_code=$return_val["msg_code"];
		$msg_detail=extract_message($msg_code);
		if ($msg_code=="delete_succee")
		{
			$msg_script="wel_engbomh_sql_grid(document.getElementById('txt_wel_ecn_no').value);\n";
		}
		break;
}

echo "|".coding_str($opt_action)."|".coding_str($msg_code)."|".coding_str($msg_detail)."|".coding_str($msg_script);
?>
