<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>
<?php
eval(receipt_url_parameter($_GET,$_POST));	//将参数的值放入相应以参数为名的变量中
//echo receipt_url_parameter($_GET,$_POST);
eval(read_cache_from_wel_wrcache($main_page."msg.inc"));
require_once(WERP_SITE_PATH_CLASS.$main_page."_cls.php");

//$opt_action,$msg_code,$msg_detail,$msg_script 为标准定义，用coding_str编码后以
//"|".$opt_action."|".$msg_code."|".$msg_detail."|".$msg_script  格式作为信息返回给回调函数
$opt_action=strtolower($opt_action);	//调用者的动作
$msg_code="";	//调用后产生的信息代码
$msg_detail="";	//信息代码对应的细节内容
$msg_script="";	//要执行的脚本

switch ($opt_action)
{
	//读取工程项目档案
	//=====================================================================
	case "bbtn_wel_proj_no_load_click":
		$cls_engm022=new engm022_cls();
		$cls_engm022->wel_proj_no=$txt_wel_proj_no;
		
		$return_val=$cls_engm022->read();
		$msg_code=$return_val["msg_code"];
		$msg_detail=extract_message($msg_code);
		if ($msg_code=="")
		{
			$msg_script="clear_screen_layout(object_id_list);\n".
			"document.getElementById('txt_wel_proj_no').value='".format_slashes($return_val["wel_proj_no"])."';\n".
			"document.getElementById('txt_wel_proj_des').value='".format_slashes($return_val["wel_proj_des"])."';\n".
			"wel_proj_no=document.getElementById('txt_wel_proj_no').value;\n";

			$msg_script .="enable_object(object_id_list,false,'');\n".
				"enable_object('btn_head_edit|btn_head_del|btn_head_next',true,".
				"access_edit+'|'+access_delete+'|'+access_read);\n";
		}
		break;
		//=====================================================================
		//新增工程项目档案
		case "addnew":
			$cls_engm022=new engm022_cls();
			$cls_engm022->wel_proj_no=$txt_wel_proj_no;
			$cls_engm022->wel_proj_des=$txt_wel_proj_des;
			
			$return_val=$cls_engm022->addnew();
			$msg_code=$return_val["msg_code"];
			$msg_detail=extract_message($msg_code);
			if ($msg_code=="addnew_succee")
			{
				$msg_script=
					"document.getElementById('txt_wel_proj_no').value='".format_slashes($return_val["wel_proj_no"])."';\n".
					"bbtn_wel_proj_no_load_click();\n";
			}
			break;
		//=====================================================================
		//编辑工程项目档案
		case "edit":
			$cls_engm022=new engm022_cls();
			$cls_engm022->wel_proj_no=$txt_wel_proj_no;
			$cls_engm022->wel_proj_des=$txt_wel_proj_des;
			
			$return_val=$cls_engm022->edit();
			$msg_code=$return_val["msg_code"];
			$msg_detail=extract_message($msg_code);
			if ($msg_code=="edit_succee")
			{
				$msg_script="bbtn_wel_proj_no_load_click();\n";
			}
			break;
		//=====================================================================
		//删除代用物料
		case "btn_head_del_click":
			$cls_engm022=new engm022_cls();
			$cls_engm022->wel_proj_no=$txt_wel_proj_no;
			
			$return_val=$cls_engm022->delete();
			$msg_code=$return_val["msg_code"];
			$msg_detail=extract_message($msg_code);
			if ($msg_code=="delete_succee")
			{
				$msg_script="btn_head_next_click();\n";
			}
			break;
}

echo "|".coding_str($opt_action)."|".coding_str($msg_code)."|".coding_str($msg_detail)."|".coding_str($msg_script);
?>
