<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
defined('WERP_EXEC') or die('Unauthorized access'); 
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT

$redirect_url=WERP_BASE_URI.WERP_EXPA_PARA.
	"&guest_token=guestframe".
	"&lang=".WERP_EXTE_LANG;
?>

<!--HTML_DO_NOT_EDIT_OR_DELETE_THIS_LINE_BEGIN-->
<iframe name="ifra_guest" id="ifra_guest" src ="<?php echo $redirect_url; ?>" 
	width="0px" height="0px" frameborder="0" frameSpacing="0" marginHeight="0" marginWidth="0">
	<p>Your browser does not support iframes.</p>
</iframe>
<!--HTML_DO_NOT_EDIT_OR_DELETE_THIS_LINE_END-->

<div style="color:blue;"><b>
<?php
//echo "Please login. The item selected has access restrictions that requires user authentication.";
?>
</b></div>