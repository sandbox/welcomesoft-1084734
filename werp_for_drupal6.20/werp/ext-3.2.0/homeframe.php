<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
defined('WERP_EXEC') or die('Unauthorized access'); 
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT

frame_html_heading();
$redirect_url=WERP_BASE_URI.WERP_EXPA_PARA.
	"&action_page=homemain".
	"&lang=".WERP_EXTE_LANG;
?>
<iframe name="ifra_homeframe" id="ifra_homeframe" src ="<?php echo $redirect_url; ?>" 
	width="100%" height="680" frameborder="0" frameSpacing="0" marginHeight="0" marginWidth="0">
	<p>Your browser does not support iframes.</p>
</iframe>
<div name="div_footer_info" name="div_footer_info" style="text-align: center;height:32px;">
<?php echo WERP_COPYRIGHT; ?>
</div>

<script language="javascript">
$(document).ready(function(){
	$(window).resize(function(){
		//$("#ifra_homeframe").css({height:($(window).height()-32+"px")});
		$("#ifra_homeframe").attr("height",($(window).height()-32));
	});
	$(window).resize();
});
</script>

<?php
frame_html_footer();
?>
