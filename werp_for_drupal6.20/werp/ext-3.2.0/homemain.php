<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php 
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
defined('WERP_EXEC') or die('Unauthorized access'); 
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT

$refresh_css_and_js="?version=".md5(uniqid(rand()));		//强逼 Firefox 等浏览器 重新加载 css 和 js ,调试时尤其重要
?>
<html lang="en" xmlns:ext="http://www.extjs.com/docs">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>ExtJS 3.2.0 API Documentation</title>
	<?php echo _init_werp_uri_for_javascript(); ?>
	<link rel="stylesheet" type="text/css" href="<?php echo WERP_FOLL_URI; ?>ext-3.2.0/resources/css/ext-all.css<?php echo $refresh_css_and_js; ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo WERP_FOLL_URI; ?>ext-3.2.0/resources/css/docs.css<?php echo $refresh_css_and_js; ?>" />
	<link rel="stylesheet" type="text/css" href="<?php echo WERP_FOLL_URI; ?>ext-3.2.0/resources/css/style.css<?php echo $refresh_css_and_js; ?>" />    
	<style type="text/css"></style>
	<!-- GC -->

	<script type="text/javascript" src="<?php echo WERP_FOLL_URI; ?>ext-3.2.0/ext-base.js<?php echo $refresh_css_and_js; ?>"></script>
	<script type="text/javascript" src="<?php echo WERP_FOLL_URI; ?>ext-3.2.0/ext-all.js<?php echo $refresh_css_and_js; ?>"></script>
	<script type="text/javascript" src="<?php echo WERP_FOLL_URI; ?>ext-3.2.0/TabCloseMenu.js<?php echo $refresh_css_and_js; ?>"></script>
	<script type="text/javascript" src="<?php echo WERP_FOLL_URI; ?>ext-3.2.0/docs.js<?php echo $refresh_css_and_js; ?>"></script>
	<script type="text/javascript">
	<?php
	function extract_wel_prog_des($wel_prog_params,$default=""){
		$wel_prog_des="";
		$wel_language_arr=explode("\n",$wel_prog_params);
		foreach ($wel_language_arr as $key=>$value){
			$value_arr=explode("=",$value);
			if (trim(strtolower($value_arr[0]))==trim(strtolower($_SESSION["wel_language_id"]))){
				$wel_prog_des=trim($value_arr[1]);
				break;
			}
		}
		if ($wel_prog_des==""){$wel_prog_des=$default;}
		$wel_prog_des=str_ireplace('"','\\"',$wel_prog_des);
		return $wel_prog_des;
	}
	
	function generate_wel_prog_code_node($wel_prog_code,$item_id){
		$output = "";
		$output_arr=array();
		
		try 
		{
			$conn=werp_db_connect();
			
			$sql = "SELECT *,".
				"IFNULL((".
				"SELECT COUNT(wel_parent_code) FROM #__wel_proglofm as sub_wel_proglofm ".
				"WHERE sub_wel_proglofm.wel_parent_code=#__wel_proglofm.wel_prog_code".
				"),0) AS sub_count ".
				"FROM #__wel_proglofm ".
				"WHERE wel_parent_code='".$wel_prog_code."' ORDER BY wel_ordering ASC";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			while ($row=mysql_fetch_array($result))
			{
				$wel_prog_code=is_null($row['wel_prog_code']) ? "" : $row['wel_prog_code'];
				$wel_working_dir=is_null($row['wel_working_dir']) ? "" : $row['wel_working_dir'];
				$sub_count=doubleval(is_null($row["sub_count"]) ? 0 : $row["sub_count"]);

				$wel_prog_des="";
				$sql="SELECT * FROM #__wel_proghdrm WHERE wel_prog_code='".$wel_prog_code."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result_proghdrm=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row_proghdrm=mysql_fetch_array($result_proghdrm))){/*throw new Exception("");*/}
				$wel_prog_des=$row_proghdrm["wel_prog_des"];
				$wel_prog_params=$row_proghdrm["wel_prog_params"];
				$wel_prog_des=extract_wel_prog_des($wel_prog_params,$wel_prog_des);
				$wel_prog_des=($wel_prog_des=="")?$wel_prog_code:$wel_prog_des;
				$sub_item_id=$item_id.$wel_prog_code;
				if($sub_count>0){
					$output_arr[] = '{"id":"pkg-WERP.'.$sub_item_id.'","text":"'.$wel_prog_des.'","iconCls":"icon-cmp","cls":"component","singleClickExpand":true, children:['.
						generate_wel_prog_code_node($wel_prog_code,$sub_item_id).
						']}';
				}else{
					$output_arr[] = '{"href":"'.WERP_BASE_URI.WERP_EXPA_PARA.
						'&main_page='.$wel_prog_code.'&main_page_dir='.$wel_working_dir.'&action_page='.$wel_prog_code.'&action_page_dir='.$wel_working_dir.
						'&lang='.WERP_EXTE_LANG.'",'.
						'"text":"'.$wel_prog_des.'",'.
						'"id":"WERP.'.$item_id.'.'.$wel_prog_code.'",'.
						'"isClass":true,
						"iconCls":"icon-form",
						"cls":"cls",
						"leaf":true}';
				}
			}
			$output = implode(",",$output_arr);
		}
		catch (Exception $e){
			//return $e->getMessage();
			$output = "";
		}
		return $output;
	}
	
	function extract_docs_classdata() {
		$wel_root_code="root";

		try
		{
			$conn=werp_db_connect();
			
			$sql="SELECT * FROM #__wel_proghdrm WHERE wel_prog_code='".$wel_root_code."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){/*throw new Exception("");*/}
			$wel_prog_des=$row["wel_prog_des"];
			$wel_prog_params=$row["wel_prog_params"];
			$wel_prog_des=extract_wel_prog_des($wel_prog_params,$wel_prog_des);
			$wel_prog_des=($wel_prog_des=="")?$wel_root_code:$wel_prog_des;
			$output = 'Docs.classData = {"id":"pkg-WERP","text":"'.$wel_prog_des.'","iconCls":"icon-pkg","cls":"package","singleClickExpand":true, "expanded":true, children:[';
				
			$output .= generate_wel_prog_code_node($wel_root_code,$wel_root_code);

			$output .= ']};
				Docs.icons = {
				};';
				
			echo $output;
		}
		catch (Exception $e){
			//return $e->getMessage();
		}
	}

	extract_docs_classdata();
	
	function show_welcome_panel(){
		try
		{
			$conn=werp_db_connect();
			
			$sql="SELECT * FROM #__wel_proghdrm WHERE wel_prog_code='sysm000' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){/*throw new Exception("");*/}
			$wel_prog_des=$row["wel_prog_des"];
			$wel_prog_params=$row["wel_prog_params"];
			$wel_prog_des=extract_wel_prog_des($wel_prog_params,$wel_prog_des);
			
			$node_attributes_id="welcome-panel";
			$node_attributes_text=(($wel_prog_des=="")?"Welcome":$wel_prog_des);
			$node_attributes_href=WERP_BASE_URI.WERP_EXPA_PARA.
				"&main_page=sysm000&main_page_dir=sysm&action_page=sysm000&action_page_dir=sysm".
				"&lang=".WERP_EXTE_LANG;

			$output = 'WERP_mainPanel.load_werp_Class_no_closable("'.$node_attributes_id.'","'.$node_attributes_text.'","'.$node_attributes_href.'");';
			$output = '
				function show_welcome_panel(){'.$output.'}
			';
			
			echo $output;
		}
		catch (Exception $e){
			//return $e->getMessage();
		}
	}
	
	show_welcome_panel();
	
	?>
	</script>
</head>

<body scroll="no" id="docs">
	<div id="loading-mask" style=""></div>
	<div id="loading">
		<div class="loading-indicator">
			<img src="<?php echo WERP_FOLL_URI; ?>ext-3.2.0/resources/images/extanim32.gif" width="32" height="32" style="margin-right:8px;" 
			align="absmiddle"/>Loading...</div>
	</div>

	<div id="header">
		<a href="http://welcomesoft.org" target="_blank" style="float:right;margin-right:10px;">
			<img src="<?php echo WERP_FOLL_URI; ?>ext-3.2.0/resources/images/welcomesoft_sm_logo.gif" style="width:89px;height:25px;margin-top:1px;"/></a>
		<div class="api-title">Welcome ERP Console</div>
	</div>

	<div id="classes"></div>

	<div id="main"></div>
	<select id="search-options" style="display:none">
		<option>Starts with</option>
		<option>Ends with</option>
		<option>Any Match</option>
	</select>
</body>
</html>