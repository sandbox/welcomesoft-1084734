<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT

frame_html_heading();
$_SESSION["wel_user_id"]="";		//清除登入的用户 ID
$form_action=WERP_BASE_URI."?q=logout".
	"&lang=".WERP_EXTE_LANG;
?>
<div style="display:none;">
<form method="POST" action="<?php echo $form_action; ?>" target="_top">
	<table cellSpacing="0" cellPadding="0" height="100%" width="100%" align="center" border="0">
	<tr>
		<td>
		
			<input type="submit" name="btn_werplogout" id="btn_werplogout" value="" />
		</td>
	</tr>
	</table>
</form>
</div>
<script language="javascript">
$(document).ready(function(){
	document.getElementById("btn_werplogout").click();
	//top.location.replace("<?php echo $form_action; ?>");
});
</script>

<?php
frame_html_footer();
?>