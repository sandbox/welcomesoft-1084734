<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or LGPL Version 2.1 (LGPLv2.1-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');

//====================================================
function extract_wel_language_id(){
	$wel_language_id=WERP_EXTE_LANG;
	
	if ($wel_language_id==""){
		global $language;
		$wel_language_id=trim("".$language->language."");
	}

	switch(strtolower($wel_language_id)){
		/*
		case "simplified":
		case "traditional":
		case "english":
			break;
		*/
		case strtolower("zh-hans"):
		case strtolower("zh"):
		case strtolower("zh-CN"):
		case strtolower("zh_CN"):
			$wel_language_id="simplified";
			break;
		case strtolower("tw"):
		case strtolower("zh-TW"):
		case strtolower("zh_TW"):
			$wel_language_id="traditional";
			break;
		case strtolower("en"):
		case strtolower("en-GB"):
		case strtolower("en_GB"):
			$wel_language_id="english";
			break;
		default:
			break;
	}
	return $wel_language_id;
}

function extract_wel_db_config(){
	global $db_url,$db_prefix;

	$url = parse_url($db_url);
	// Decode url-encoded information in the db connection string
	$url['user'] = urldecode($url['user']);
	// Test if database url has a password.
	$url['pass'] = isset($url['pass']) ? urldecode($url['pass']) : '';
	$url['host'] = urldecode($url['host']);
	$url['path'] = urldecode($url['path']);
	
	$wel_db_config=array();
	$wel_db_config["wel_db_host"] = $url['host'];
	$wel_db_config["wel_db_user"] = $url['user'];
	$wel_db_config["wel_db_password"] = $url['pass'];
	$wel_db_config["wel_db_name"] = substr($url['path'], 1);
	$wel_db_config["wel_table_prefix"] = $db_prefix;
	$wel_db_config["wel_db_type"] = "mysql";
	return $wel_db_config;		
}

function current_user_is_guest(){
	global $user;
	return (trim("".$user->name."")=="")?true:false;
}

function extract_wel_user_id(){
	global $user;
	return $user->name;
}
//====================================================

?>