<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or LGPL Version 2.1 (LGPLv2.1-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');


@ini_set( "max_execution_time", "120" );


function opt_wel_proghdrm($wel_prog_code, $wel_prog_des, $wel_prog_params){
	$wel_prog_des=addslashes($wel_prog_des);
	$wel_prog_params=addslashes($wel_prog_params);
	if (record_not_exists("SELECT * FROM `#__wel_proghdrm` WHERE wel_prog_code = '$wel_prog_code' LIMIT 1")){
		execute_sql("
		INSERT INTO `#__wel_proghdrm` (`wel_prog_code`, `wel_prog_des`, `wel_prog_params`) VALUES 
		('$wel_prog_code', '$wel_prog_des', '$wel_prog_params');
		");
	}
}

function opt_wel_proglofm($wel_parent_code, $wel_prog_code, $wel_working_dir, $wel_ordering){
	if (record_not_exists("SELECT * FROM `#__wel_proglofm` 
		WHERE wel_parent_code = '$wel_parent_code' AND wel_prog_code='$wel_prog_code' LIMIT 1")){
		execute_sql("
		INSERT INTO `#__wel_proglofm` (`wel_parent_code`, `wel_prog_code`, `wel_working_dir`, `wel_ordering`) 
		VALUES
		('$wel_parent_code', '$wel_prog_code', '$wel_working_dir', $wel_ordering);
		");
	}else{
		execute_sql("
		UPDATE #__wel_proglofm SET wel_ordering=$wel_ordering 
		WHERE wel_parent_code='$wel_parent_code' AND wel_prog_code='$wel_prog_code';
		");
	}
}

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_compflm` (
  `wel_comp_code` varchar(20) NOT NULL,
  `wel_comp_des` varchar(100) DEFAULT NULL,
  `wel_comp_cdes` varchar(100) DEFAULT NULL,
  `wel_comp_add1` varchar(100) DEFAULT NULL,
  `wel_comp_add2` varchar(100) DEFAULT NULL,
  `wel_comp_add3` varchar(100) DEFAULT NULL,
  `wel_comp_add4` varchar(100) DEFAULT NULL,
  `wel_comp_cont` varchar(50) DEFAULT NULL,
  `wel_comp_tele` varchar(50) DEFAULT NULL,
  `wel_comp_fax` varchar(50) DEFAULT NULL,
  `wel_comp_telx` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`wel_comp_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_currenm` (
  `wel_cur_code` varchar(20) NOT NULL,
  `wel_cur_des` varchar(50) DEFAULT NULL,
  `wel_ex_rate` decimal(20,6) DEFAULT '0.000000',
  PRIMARY KEY (`wel_cur_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_curtabh` (
  `wel_cur_code` varchar(10) NOT NULL,
  `wel_yyyy_mm` varchar(10) NOT NULL,
  `wel_ex_rate` decimal(20,6) NOT NULL,
  PRIMARY KEY (`wel_cur_code`,`wel_yyyy_mm`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_groupflm` (
  `wel_group_code` varchar(50) NOT NULL DEFAULT '',
  `wel_group_des` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`wel_group_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_groupprog` (
  `wel_group_code` varchar(50) NOT NULL DEFAULT '',
  `wel_prog_code` varchar(50) NOT NULL DEFAULT '',
  `wel_access_read` int(11) DEFAULT '0',
  `wel_access_addnew` int(11) DEFAULT '0',
  `wel_access_edit` int(11) DEFAULT '0',
  `wel_access_delete` int(11) DEFAULT '0',
  `wel_access_approve` int(11) DEFAULT '0',
  `wel_access_print` int(11) DEFAULT '0',
  PRIMARY KEY (`wel_group_code`,`wel_prog_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_language` (
  `wel_language_id` varchar(50) NOT NULL DEFAULT '',
  `wel_language_des` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`wel_language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_pbasefm` (
  `wel_pbase_code` varchar(20) NOT NULL DEFAULT '',
  `wel_pbase_name` varchar(100) NOT NULL,
  PRIMARY KEY (`wel_pbase_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_rpttemp01` (
  `wel_string_00` varchar(200) DEFAULT NULL,
  `wel_string_01` varchar(200) DEFAULT NULL,
  `wel_string_02` varchar(200) DEFAULT NULL,
  `wel_string_03` varchar(200) DEFAULT NULL,
  `wel_string_04` varchar(200) DEFAULT NULL,
  `wel_string_05` varchar(200) DEFAULT NULL,
  `wel_string_06` varchar(200) DEFAULT NULL,
  `wel_string_07` varchar(200) DEFAULT NULL,
  `wel_string_08` varchar(200) DEFAULT NULL,
  `wel_string_09` varchar(200) DEFAULT NULL,
  `wel_string_10` varchar(200) DEFAULT NULL,
  `wel_string_11` varchar(200) DEFAULT NULL,
  `wel_string_12` varchar(200) DEFAULT NULL,
  `wel_string_13` varchar(200) DEFAULT NULL,
  `wel_string_14` varchar(200) DEFAULT NULL,
  `wel_string_15` varchar(200) DEFAULT NULL,
  `wel_string_16` varchar(200) DEFAULT NULL,
  `wel_string_17` varchar(200) DEFAULT NULL,
  `wel_string_18` varchar(200) DEFAULT NULL,
  `wel_string_19` varchar(200) DEFAULT NULL,
  `wel_string_20` varchar(200) DEFAULT NULL,
  `wel_decimal_00` decimal(20,8) DEFAULT NULL,
  `wel_decimal_01` decimal(20,8) DEFAULT NULL,
  `wel_decimal_02` decimal(20,8) DEFAULT NULL,
  `wel_decimal_03` decimal(20,8) DEFAULT NULL,
  `wel_decimal_04` decimal(20,8) DEFAULT NULL,
  `wel_decimal_05` decimal(20,8) DEFAULT NULL,
  `wel_decimal_06` decimal(20,8) DEFAULT NULL,
  `wel_decimal_07` decimal(20,8) DEFAULT NULL,
  `wel_decimal_08` decimal(20,8) DEFAULT NULL,
  `wel_decimal_09` decimal(20,8) DEFAULT NULL,
  `wel_decimal_10` decimal(20,8) DEFAULT NULL,
  `wel_decimal_11` decimal(20,8) DEFAULT NULL,
  `wel_decimal_12` decimal(20,8) DEFAULT NULL,
  `wel_decimal_13` decimal(20,8) DEFAULT NULL,
  `wel_decimal_14` decimal(20,8) DEFAULT NULL,
  `wel_decimal_15` decimal(20,8) DEFAULT NULL,
  `wel_decimal_16` decimal(20,8) DEFAULT NULL,
  `wel_decimal_17` decimal(20,8) DEFAULT NULL,
  `wel_decimal_18` decimal(20,8) DEFAULT NULL,
  `wel_decimal_19` decimal(20,8) DEFAULT NULL,
  `wel_decimal_20` decimal(20,8) DEFAULT NULL,
  `wel_text_00` text,
  `wel_text_01` text,
  `wel_text_02` text,
  `wel_text_03` text,
  `wel_text_04` text,
  `wel_text_05` text,
  `wel_uniqid` varchar(60) NOT NULL DEFAULT '',
  `wel_report_id` varchar(60) DEFAULT NULL,
  `wel_rpt_user` varchar(30) DEFAULT NULL,
  `wel_rpt_date` datetime DEFAULT NULL,
  PRIMARY KEY (`wel_uniqid`),
  KEY `wel_report_id` (`wel_report_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_shpviam` (
  `wel_svia_code` varchar(20) NOT NULL DEFAULT '',
  `wel_svia_des` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`wel_svia_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_userflm` (
  `wel_user_id` varchar(30) NOT NULL DEFAULT '',
  `wel_user_name` varchar(50) DEFAULT NULL,
  `wel_e_mail` varchar(100) DEFAULT NULL,
  `wel_department` varchar(20) DEFAULT NULL,
  `wel_password` varchar(50) DEFAULT NULL,
  `wel_language_id` varchar(50) DEFAULT NULL,
  `wel_disable` int(11) DEFAULT NULL,
  `wel_expire_date` date DEFAULT NULL,
  PRIMARY KEY (`wel_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_usergroup` (
  `wel_user_id` varchar(30) NOT NULL DEFAULT '',
  `wel_group_code` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`wel_user_id`,`wel_group_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_userprog` (
  `wel_user_id` varchar(30) NOT NULL DEFAULT '',
  `wel_prog_code` varchar(50) NOT NULL DEFAULT '',
  `wel_access_read` int(11) DEFAULT '0',
  `wel_access_addnew` int(11) DEFAULT '0',
  `wel_access_edit` int(11) DEFAULT '0',
  `wel_access_delete` int(11) DEFAULT '0',
  `wel_access_approve` int(11) DEFAULT '0',
  `wel_access_print` int(11) DEFAULT '0',
  PRIMARY KEY (`wel_user_id`,`wel_prog_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_altparm` (
  `wel_proj_no` varchar(20) NOT NULL,
  `wel_assm_no` varchar(36) NOT NULL DEFAULT '',
  `wel_part_no` varchar(36) NOT NULL DEFAULT '',
  `wel_alt_part` varchar(36) NOT NULL DEFAULT '',
  `wel_priority` varchar(50) DEFAULT NULL,
  `wel_ex_qty` decimal(20,4) DEFAULT '1.0000',
  `wel_user_id` varchar(40) DEFAULT NULL,
  `wel_prod_no` varchar(20) NOT NULL,
  `wel_crt_user` varchar(30) DEFAULT NULL,
  `wel_crt_date` date DEFAULT NULL,
  `wel_upd_user` varchar(30) DEFAULT NULL,
  `wel_upd_date` date DEFAULT NULL,
  PRIMARY KEY (`wel_assm_no`,`wel_part_no`,`wel_alt_part`,`wel_prod_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_catcdem` (
  `wel_cat_code` varchar(20) NOT NULL,
  `wel_cat_des` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`wel_cat_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_ecnhdrm` (
  `wel_ecn_no` varchar(20) NOT NULL DEFAULT '',
  `wel_pattern` varchar(10) DEFAULT NULL,
  `wel_rev_no` int(11) DEFAULT NULL,
  `wel_ecn_date` date DEFAULT NULL,
  `wel_proj_no` varchar(20) DEFAULT NULL,
  `wel_ecn_rmk` text,
  `wel_appr_yn` tinyint(1) DEFAULT NULL,
  `wel_appr_by` varchar(30) DEFAULT NULL,
  `wel_appr_date` date DEFAULT NULL,
  `wel_post_yn` tinyint(1) DEFAULT NULL,
  `wel_post_by` varchar(30) DEFAULT NULL,
  `wel_post_date` date DEFAULT NULL,
  `wel_crt_user` varchar(30) DEFAULT NULL,
  `wel_crt_date` date DEFAULT NULL,
  `wel_upd_user` varchar(30) DEFAULT NULL,
  `wel_upd_date` date DEFAULT NULL,
  PRIMARY KEY (`wel_ecn_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_engbomh` (
  `wel_ecn_no` varchar(20) NOT NULL DEFAULT '',
  `wel_assm_no` varchar(36) NOT NULL DEFAULT '',
  `wel_part_no` varchar(36) NOT NULL DEFAULT '',
  `wel_ecn_act` varchar(20) DEFAULT NULL,
  `wel_scp_fact` decimal(20,6) DEFAULT NULL,
  `wel_eng_unit` varchar(20) DEFAULT NULL,
  `wel_qp_eng` decimal(20,6) DEFAULT NULL,
  `wel_qty_per` decimal(20,6) DEFAULT NULL,
  `wel_unit_exg` decimal(20,6) DEFAULT NULL,
  `wel_ecn_date` date DEFAULT NULL,
  `wel_process_no` varchar(30) DEFAULT NULL,
  `wel_pre_eng_unit` varchar(20) DEFAULT NULL,
  `wel_pre_qp_eng` decimal(20,6) DEFAULT NULL,
  `wel_mod_sign` varchar(20) DEFAULT NULL,
  `wel_ecn_loc` text,
  `wel_ecn_rmk` text,
  `wel_post_yn` tinyint(1) DEFAULT NULL,
  `wel_crt_user` varchar(30) DEFAULT NULL,
  `wel_crt_date` date DEFAULT NULL,
  `wel_upd_user` varchar(30) DEFAULT NULL,
  `wel_upd_date` date DEFAULT NULL,
  PRIMARY KEY (`wel_assm_no`,`wel_part_no`,`wel_ecn_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_engbomm` (
  `wel_assm_no` varchar(36) NOT NULL,
  `wel_part_no` varchar(36) NOT NULL,
  `wel_scp_fact` decimal(20,6) DEFAULT '0.000000',
  `wel_eng_unit` varchar(20) DEFAULT NULL,
  `wel_qp_eng` decimal(20,6) DEFAULT '0.000000',
  `wel_unit_exg` decimal(20,6) DEFAULT '0.000000',
  `wel_qty_per` decimal(20,6) DEFAULT '0.000000',
  `wel_bom_loc` text,
  `wel_bom_rmk` text,
  `wel_process_no` varchar(20) DEFAULT NULL,
  `wel_featu_yn` tinyint(1) DEFAULT '0',
  `wel_dis_wt` decimal(20,6) DEFAULT '0.000000',
  `wel_crt_user` varchar(30) DEFAULT NULL,
  `wel_crt_date` date DEFAULT NULL,
  `wel_upd_user` varchar(30) DEFAULT NULL,
  `wel_upd_date` date DEFAULT NULL,
  PRIMARY KEY (`wel_assm_no`,`wel_part_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_famcdem` (
  `wel_fam_code` varchar(20) NOT NULL,
  `wel_fam_des` varchar(50) NOT NULL,
  PRIMARY KEY (`wel_fam_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_gentenw` (
  `wel_pattern` varchar(10) NOT NULL DEFAULT '',
  `wel_pat_des` varchar(50) DEFAULT NULL,
  `wel_en_nextno` int(11) DEFAULT NULL,
  PRIMARY KEY (`wel_pattern`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_partflm` (
  `wel_part_no` varchar(36) NOT NULL,
  `wel_part_des` varchar(100) DEFAULT NULL,
  `wel_part_des1` varchar(100) DEFAULT NULL,
  `wel_part_des2` varchar(100) DEFAULT NULL,
  `wel_part_des3` varchar(100) DEFAULT NULL,
  `wel_part_des4` varchar(100) DEFAULT NULL,
  `wel_part_des5` varchar(50) DEFAULT NULL,
  `wel_std_code` varchar(100) DEFAULT NULL,
  `wel_cat_code` varchar(20) DEFAULT NULL,
  `wel_unit` varchar(20) DEFAULT NULL,
  `wel_eng_unit` varchar(20) DEFAULT NULL,
  `wel_eng_unit_rate` decimal(20,6) DEFAULT '0.000000',
  `wel_scp_fact` decimal(20,6) DEFAULT '0.000000',
  `wel_mould_no` varchar(36) DEFAULT NULL,
  `wel_mbc_code` varchar(10) DEFAULT NULL,
  `wel_abc_code` varchar(10) DEFAULT NULL,
  `wel_proj_no` varchar(36) DEFAULT NULL,
  `wel_assort_qty` int(11) DEFAULT '0',
  `wel_assort_pn` int(1) DEFAULT '0',
  `wel_appy_yn` int(1) DEFAULT '0',
  `wel_appy_date` datetime DEFAULT NULL,
  `wel_appy_by` varchar(40) DEFAULT NULL,
  `wel_appy_lvl` int(11) DEFAULT '0',
  `wel_bom_yn` tinyint(1) DEFAULT '0',
  `wel_bom_date` date DEFAULT NULL,
  `wel_crt_user` varchar(30) DEFAULT NULL,
  `wel_crt_date` date DEFAULT NULL,
  `wel_upd_user` varchar(30) DEFAULT NULL,
  `wel_upd_date` date DEFAULT NULL,
  `wel_bom_lvl` int(11) DEFAULT '0',
  `wel_bom_by` varchar(40) DEFAULT NULL,
  `wel_bom_rev` int(11) DEFAULT NULL,
  `wel_lead_tm` int(10) DEFAULT NULL,
  `wel_min_ord` decimal(20,0) DEFAULT NULL,
  `wel_pkg_ord` decimal(20,0) DEFAULT NULL,
  `wel_ven_code` varchar(20) DEFAULT NULL,
  `wel_cur_code` varchar(10) DEFAULT NULL,
  `wel_u_price` decimal(20,6) DEFAULT '0.000000',
  `wel_quot_no` varchar(30) DEFAULT NULL,
  `wel_avg_cost` decimal(20,8) DEFAULT '0.00000000',
  `wel_avg_bomc` decimal(20,2) DEFAULT '0.00',
  `wel_std_cost` decimal(20,8) DEFAULT '0.00000000',
  `wel_lat_cost` decimal(20,6) DEFAULT NULL,
  `wel_vqt_cost` decimal(20,6) DEFAULT NULL,
  `wel_manu_tm` int(10) DEFAULT NULL,
  `wel_min_stk` int(11) DEFAULT NULL,
  `wel_max_stk` int(11) DEFAULT NULL,
  `wel_pur_gday` int(11) DEFAULT NULL,
  `wel_open_pr` int(11) DEFAULT NULL,
  `wel_buyer_code` varchar(40) DEFAULT NULL,
  `wel_tol_code` varchar(20) DEFAULT NULL,
  `wel_cus_code` varchar(20) DEFAULT NULL,
  `wel_prpocon` tinyint(1) DEFAULT NULL,
  `wel_sys_date` datetime DEFAULT NULL,
  `wel_sys_time` varchar(20) DEFAULT NULL,
  `wel_user_id` varchar(40) DEFAULT NULL,
  `wel_part_fob` tinyint(1) DEFAULT NULL,
  `wel_amd_date` date DEFAULT NULL,
  `wel_bom_userid` varchar(50) DEFAULT NULL,
  `wel_bom_amddate` datetime DEFAULT NULL,
  `wel_bom_crtuser` varchar(51) DEFAULT NULL,
  `wel_bom_crtdate` datetime DEFAULT NULL,
  `wel_pmclead_tm` int(11) DEFAULT NULL,
  `wel_fam_code` varchar(50) DEFAULT NULL,
  `wel_drwg_no` varchar(30) DEFAULT NULL,
  `wel_daily_out` decimal(20,4) DEFAULT NULL,
  `wel_std_lot_qty` decimal(20,6) DEFAULT NULL,
  `wel_ctr_code` varchar(50) DEFAULT NULL,
  `wel_checkyn` tinyint(1) DEFAULT NULL,
  `wel_issue_qty` decimal(20,6) DEFAULT NULL,
  `wel_serv_day` int(11) DEFAULT NULL,
  `wel_appr_yn` int(1) DEFAULT '0',
  `wel_appr_by` varchar(50) DEFAULT NULL,
  `wel_appr_date` datetime DEFAULT NULL,
  `wel_appr_time` varchar(50) DEFAULT NULL,
  `wel_pur_unit` varchar(20) DEFAULT NULL,
  `wel_pur_unit_rate` decimal(20,6) DEFAULT NULL,
  `wel_pur_unit1` varchar(20) DEFAULT NULL,
  `wel_pur_unit_rate1` decimal(20,6) DEFAULT NULL,
  `wel_pur_unit2` varchar(20) DEFAULT NULL,
  `wel_pur_unit_rate2` decimal(20,6) DEFAULT NULL,
  `wel_net_wt` decimal(20,4) DEFAULT NULL,
  `wel_grs_wt` decimal(20,4) DEFAULT NULL,
  `wel_uow` varchar(20) DEFAULT NULL,
  `wel_length` decimal(20,4) DEFAULT NULL,
  `wel_width` decimal(20,4) DEFAULT NULL,
  `wel_height` decimal(20,4) DEFAULT NULL,
  `wel_uod` varchar(20) DEFAULT NULL,
  `wel_cbm` decimal(20,4) DEFAULT NULL,
  `wel_pcs_per` decimal(20,4) DEFAULT NULL,
  `wel_part_image` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`wel_part_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_projflm` (
  `wel_proj_no` varchar(50) NOT NULL,
  `wel_proj_des` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`wel_proj_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_unitflm` (
  `wel_unit_code` varchar(50) NOT NULL DEFAULT '',
  `wel_unit_des` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`wel_unit_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_delytom` (
  `wel_dely_code` varchar(20) NOT NULL,
  `wel_dely_des` varchar(100) DEFAULT NULL,
  `wel_dely_add1` varchar(100) DEFAULT NULL,
  `wel_dely_add2` varchar(100) DEFAULT NULL,
  `wel_dely_add3` varchar(100) DEFAULT NULL,
  `wel_dely_add4` varchar(100) DEFAULT NULL,
  `wel_dely_email` varchar(100) DEFAULT NULL,
  `wel_dely_cont` varchar(50) DEFAULT NULL,
  `wel_dely_tele` varchar(50) DEFAULT NULL,
  `wel_dely_fax` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`wel_dely_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_gentpow` (
  `wel_pattern` varchar(10) NOT NULL,
  `wel_pat_des` varchar(50) DEFAULT NULL,
  `wel_po_nextno` int(11) DEFAULT NULL,
  `wel_ppo_no_yn` tinyint(1) NOT NULL,
  PRIMARY KEY (`wel_pattern`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_planpod` (
  `wel_plan_no` varchar(20) NOT NULL,
  `wel_plan_line` int(11) NOT NULL,
  `wel_ven_code` varchar(20) DEFAULT NULL,
  `wel_buyer_code` varchar(20) DEFAULT NULL,
  `wel_dely_code` varchar(20) DEFAULT NULL,
  `wel_ctr_code` varchar(20) DEFAULT NULL,
  `wel_cur_code` varchar(20) DEFAULT NULL,
  `wel_part_no` varchar(36) DEFAULT NULL,
  `wel_qty_per` decimal(20,6) DEFAULT NULL,
  `wel_ord_no` varchar(20) DEFAULT NULL,
  `wel_ord_line` int(11) DEFAULT NULL,
  `wel_req_date` date DEFAULT NULL,
  `wel_req_qty` decimal(20,4) DEFAULT NULL,
  `wel_pur_unit` varchar(20) DEFAULT NULL,
  `wel_unit_exg` decimal(20,6) DEFAULT NULL,
  `wel_org_date` date DEFAULT NULL,
  `wel_org_qty` decimal(20,4) DEFAULT NULL,
  `wel_u_price` decimal(20,6) DEFAULT NULL,
  `wel_out_price` decimal(20,6) DEFAULT NULL,
  `wel_price_scr` varchar(20) DEFAULT NULL,
  `wel_quot_no` varchar(20) DEFAULT NULL,
  `wel_lead_tm` int(11) DEFAULT NULL,
  `wel_manu_tm` int(11) DEFAULT NULL,
  `wel_proc_tm` int(11) DEFAULT NULL,
  `wel_pkg_ord` decimal(20,4) DEFAULT NULL,
  `wel_min_ord` decimal(20,4) DEFAULT NULL,
  `wel_pr_no` varchar(20) DEFAULT NULL,
  `wel_so_no` varchar(20) DEFAULT NULL,
  `wel_assm_no` varchar(36) DEFAULT NULL,
  `wel_cat_code` varchar(20) DEFAULT NULL,
  `wel_po_rmk` text,
  `wel_po_no` varchar(20) DEFAULT NULL,
  `wel_po_line` int(11) NOT NULL,
  `wel_po_type` varchar(10) DEFAULT NULL,
  `wel_cfm_yn` tinyint(1) DEFAULT NULL,
  `wel_cfm_by` char(30) DEFAULT NULL,
  `wel_cfm_date` datetime DEFAULT NULL,
  `wel_appr_yn` tinyint(1) NOT NULL,
  `wel_appr_by` varchar(30) DEFAULT NULL,
  `wel_appr_date` datetime DEFAULT NULL,
  `wel_crt_user` char(30) DEFAULT NULL,
  `wel_crt_date` datetime DEFAULT NULL,
  `wel_upd_user` char(30) DEFAULT NULL,
  `wel_upd_date` datetime DEFAULT NULL,
  PRIMARY KEY (`wel_plan_no`,`wel_plan_line`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_planpoh` (
  `wel_plan_no` varchar(50) NOT NULL,
  `wel_comp_code` varchar(50) DEFAULT NULL,
  `wel_appr_yn` tinyint(1) NOT NULL,
  `wel_appr_by` varchar(50) DEFAULT NULL,
  `wel_appr_date` datetime DEFAULT NULL,
  `wel_crt_user` varchar(50) DEFAULT NULL,
  `wel_crt_date` datetime DEFAULT NULL,
  `wel_upd_user` varchar(50) DEFAULT NULL,
  `wel_upd_date` datetime DEFAULT NULL,
  PRIMARY KEY (`wel_plan_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_pordetm` (
  `wel_ven_code` varchar(20) DEFAULT NULL,
  `wel_po_no` varchar(20) NOT NULL DEFAULT '',
  `wel_po_line` int(11) NOT NULL,
  `wel_rev_no` int(11) DEFAULT NULL,
  `wel_part_no` varchar(36) DEFAULT NULL,
  `wel_cat_code` varchar(20) DEFAULT NULL,
  `wel_req_qty` decimal(20,6) DEFAULT NULL,
  `wel_os_qty` decimal(20,6) DEFAULT NULL,
  `wel_quot_no` varchar(20) DEFAULT NULL,
  `wel_u_price` decimal(20,6) DEFAULT NULL,
  `wel_tmp_price` decimal(20,6) DEFAULT '0.000000',
  `wel_pur_unit` varchar(20) DEFAULT NULL,
  `wel_pur_qty` decimal(20,6) DEFAULT NULL,
  `wel_pur_unit_rate` decimal(20,6) DEFAULT NULL,
  `wel_req_date` date DEFAULT NULL,
  `wel_com_date` date DEFAULT NULL,
  `wel_buyer_code` varchar(30) DEFAULT NULL,
  `wel_pr_no` varchar(20) DEFAULT NULL,
  `wel_po_rmk` text,
  `wel_net_qty` decimal(20,2) DEFAULT NULL,
  `wel_rcv_qty` decimal(20,2) DEFAULT NULL,
  `wel_iqc_qty` decimal(20,2) DEFAULT NULL,
  `wel_tol_per` decimal(20,6) DEFAULT NULL,
  `wel_checkyn` tinyint(1) NOT NULL,
  `wel_closed` tinyint(1) NOT NULL,
  `wel_inv_qty` decimal(20,2) DEFAULT NULL,
  `wel_rtv_qty` decimal(20,2) DEFAULT NULL,
  `wel_crt_user` varchar(30) DEFAULT NULL,
  `wel_crt_date` datetime DEFAULT NULL,
  `wel_upd_user` varchar(30) DEFAULT NULL,
  `wel_upd_date` datetime DEFAULT NULL,
  `wel_dis_rate` decimal(20,6) DEFAULT NULL,
  `wel_tax_rate` decimal(20,6) DEFAULT NULL,
  `wel_item_amt` decimal(20,6) DEFAULT NULL,
  `wel_dis_amt` decimal(20,6) DEFAULT NULL,
  `wel_dis_amt2` decimal(20,6) DEFAULT NULL,
  `wel_tax_amt` decimal(20,6) DEFAULT NULL,
  `wel_line_amt` decimal(20,6) DEFAULT NULL,
  PRIMARY KEY (`wel_po_no`,`wel_po_line`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_porhdrm` (
  `wel_po_no` varchar(20) NOT NULL DEFAULT '',
  `wel_rev_no` int(11) DEFAULT NULL,
  `wel_pattern` varchar(10) DEFAULT NULL,
  `wel_comp_code` varchar(20) DEFAULT NULL,
  `wel_buyer_code` varchar(30) DEFAULT NULL,
  `wel_ven_code` varchar(20) DEFAULT NULL,
  `wel_cur_code` varchar(20) DEFAULT NULL,
  `wel_ex_rate` decimal(20,6) DEFAULT NULL,
  `wel_pr_no` varchar(30) DEFAULT NULL,
  `wel_req_date` date DEFAULT NULL,
  `wel_ven_term` varchar(20) DEFAULT NULL,
  `wel_ven_svia` varchar(20) DEFAULT NULL,
  `wel_ven_pbase` varchar(20) DEFAULT NULL,
  `wel_dely_code` varchar(20) DEFAULT NULL,
  `wel_ctr_code` varchar(20) DEFAULT NULL,
  `wel_closed` tinyint(1) DEFAULT NULL,
  `wel_tax_type` int(11) DEFAULT NULL,
  `wel_tax_rate` decimal(20,6) DEFAULT NULL,
  `wel_last_line` int(11) DEFAULT NULL,
  `wel_po_remark` longtext,
  `wel_cancel_yn` tinyint(1) DEFAULT NULL,
  `wel_cancel_by` varchar(30) DEFAULT NULL,
  `wel_cancel_date` datetime DEFAULT NULL,
  `wel_appr_yn` tinyint(1) DEFAULT NULL,
  `wel_appr_by` varchar(30) DEFAULT NULL,
  `wel_appr_date` datetime DEFAULT NULL,
  `wel_crt_user` varchar(30) DEFAULT NULL,
  `wel_crt_date` datetime DEFAULT NULL,
  `wel_upd_user` varchar(30) DEFAULT NULL,
  `wel_upd_date` datetime DEFAULT NULL,
  `wel_dis_type` tinyint(11) DEFAULT NULL,
  `wel_dis_rate` decimal(20,6) DEFAULT NULL,
  `wel_item_amt` decimal(20,6) DEFAULT NULL,
  `wel_dis_amt` decimal(20,6) DEFAULT NULL,
  `wel_dis_amt2` decimal(20,6) DEFAULT NULL,
  `wel_tax_amt` decimal(20,6) DEFAULT NULL,
  `wel_order_amt` decimal(20,6) DEFAULT NULL,
  PRIMARY KEY (`wel_po_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_pormism` (
  `wel_po_no` varchar(20) NOT NULL,
  `wel_line_no` int(11) DEFAULT NULL,
  `wel_item` varchar(100) DEFAULT NULL,
  `wel_item_amt` decimal(20,6) DEFAULT NULL,
  `wel_dis_rate` decimal(20,6) DEFAULT NULL,
  `wel_tax_rate` decimal(20,6) DEFAULT NULL,
  `wel_dis_amt` decimal(20,6) DEFAULT NULL,
  `wel_dis_amt2` decimal(20,6) DEFAULT NULL,
  `wel_tax_amt` decimal(20,6) DEFAULT NULL,
  `wel_line_amt` decimal(20,6) DEFAULT NULL,
  PRIMARY KEY (`wel_po_no`,`wel_line_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_portyem` (
  `wel_type_code` varchar(20) NOT NULL,
  `wel_type_des` varchar(50) DEFAULT NULL,
  `wel_cr_day` int(11) DEFAULT NULL,
  `wel_ams_yn` tinyint(1) NOT NULL,
  PRIMARY KEY (`wel_type_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_purhanm` (
  `wel_buyer_code` varchar(30) DEFAULT NULL,
  `wel_buyer_name` varchar(50) DEFAULT NULL,
  `wel_emailadd` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`wel_buyer_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_venmasm` (
  `wel_ven_code` varchar(20) NOT NULL,
  `wel_ven_des` varchar(100) DEFAULT NULL,
  `wel_ven_des1` varchar(100) DEFAULT NULL,
  `wel_ven_add1` varchar(100) DEFAULT NULL,
  `wel_ven_add2` varchar(100) DEFAULT NULL,
  `wel_ven_add3` varchar(100) DEFAULT NULL,
  `wel_ven_add4` varchar(100) DEFAULT NULL,
  `wel_ven_cont` varchar(30) DEFAULT NULL,
  `wel_ven_tele` varchar(100) DEFAULT NULL,
  `wel_ven_fax` varchar(100) DEFAULT NULL,
  `wel_ven_term` varchar(20) DEFAULT NULL,
  `wel_ven_pbase` varchar(50) DEFAULT NULL,
  `wel_ven_svia` varchar(100) DEFAULT NULL,
  `wel_ven_rmk` text,
  `wel_buyer_code` varchar(100) DEFAULT NULL,
  `wel_cr_day` int(11) DEFAULT NULL,
  `wel_ams_yn` tinyint(4) DEFAULT NULL,
  `wel_dely_code` varchar(20) DEFAULT NULL,
  `wel_ven_email` varchar(100) DEFAULT NULL,
  `wel_cur_code` varchar(20) DEFAULT NULL,
  `wel_discount` decimal(20,2) DEFAULT NULL,
  `wel_abbre_des` varchar(50) DEFAULT NULL,
  `wel_active` tinyint(4) DEFAULT NULL,
  `wel_tax_type` int(11) DEFAULT NULL,
  `wel_tax_no` varchar(50) DEFAULT '',
  `wel_bank_account` varchar(50) DEFAULT '',
  `wel_ven_tele1` varchar(50) DEFAULT '',
  `wel_ven_fax1` varchar(50) DEFAULT '',
  `wel_tax_rate` decimal(20,6) DEFAULT NULL,
  PRIMARY KEY (`wel_ven_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_venparm` (
  `wel_ven_code` varchar(20) NOT NULL,
  `wel_part_no` varchar(36) NOT NULL,
  `wel_ven_part` varchar(50) DEFAULT NULL,
  `wel_ven_part_des` varchar(100) NOT NULL,
  `wel_lead_tm` int(11) DEFAULT NULL,
  `wel_min_ord` decimal(20,6) DEFAULT NULL,
  `wel_pkg_ord` decimal(20,6) DEFAULT NULL,
  `wel_pur_unit` varchar(20) NOT NULL,
  `wel_pur_unit_rate` decimal(20,6) DEFAULT NULL,
  PRIMARY KEY (`wel_ven_code`,`wel_part_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_venqdtm` (
  `wel_quot_no` varchar(20) NOT NULL,
  `wel_quot_line` int(4) NOT NULL,
  `wel_ven_code` varchar(20) DEFAULT NULL,
  `wel_part_no` varchar(36) NOT NULL,
  `wel_cur_code` varchar(20) DEFAULT NULL,
  `wel_u_price` decimal(20,6) DEFAULT NULL,
  `wel_unit_code` varchar(20) DEFAULT NULL,
  `wel_quot_qty` decimal(20,2) NOT NULL,
  `wel_quot_date` date DEFAULT NULL,
  `wel_prmy_quot` tinyint(1) NOT NULL,
  `wel_appr_yn` tinyint(1) NOT NULL,
  `wel_qt_memo` longtext,
  `wel_appr_by` varchar(30) DEFAULT NULL,
  `wel_appr_date` datetime DEFAULT NULL,
  `wel_lead_tm` int(11) DEFAULT NULL,
  `wel_dis_per` decimal(20,6) DEFAULT NULL,
  `wel_pkg_ord` decimal(20,2) DEFAULT NULL,
  `wel_min_ord` decimal(20,2) DEFAULT NULL,
  `wel_ven_part` varchar(50) DEFAULT NULL,
  `wel_pur_unit` varchar(20) DEFAULT NULL,
  `wel_pur_unit_rate` decimal(20,6) DEFAULT '0.000000',
  `wel_crt_user` varchar(30) DEFAULT NULL,
  `wel_crt_date` datetime DEFAULT NULL,
  `wel_upd_user` varchar(30) DEFAULT NULL,
  `wel_upd_date` datetime DEFAULT NULL,
  PRIMARY KEY (`wel_quot_no`,`wel_quot_line`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_venqhdm` (
  `wel_quot_no` varchar(20) NOT NULL,
  `wel_quot_date` date DEFAULT NULL,
  `wel_ven_code` varchar(20) DEFAULT NULL,
  `wel_buyer_code` varchar(30) DEFAULT NULL,
  `wel_cur_code` varchar(20) DEFAULT NULL,
  `wel_ex_rate` decimal(20,6) DEFAULT NULL,
  `wel_ven_term` varchar(20) DEFAULT NULL,
  `wel_ven_pbase` varchar(20) DEFAULT NULL,
  `wel_ven_svia` varchar(20) DEFAULT NULL,
  `wel_qt_remark` text,
  `wel_appr_yn` tinyint(1) NOT NULL,
  `wel_appr_by` varchar(30) DEFAULT NULL,
  `wel_appr_date` datetime DEFAULT NULL,
  `wel_crt_user` varchar(30) DEFAULT NULL,
  `wel_crt_date` datetime DEFAULT NULL,
  `wel_upd_user` varchar(30) DEFAULT NULL,
  `wel_upd_date` datetime DEFAULT NULL,
  PRIMARY KEY (`wel_quot_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_centrem` (
  `wel_ctr_code` varchar(200) NOT NULL,
  `wel_ctr_des` varchar(200) DEFAULT NULL,
  `wel_ctr_loc1` varchar(200) DEFAULT NULL,
  `wel_ctr_loc2` varchar(200) DEFAULT NULL,
  `wel_ctr_loc3` varchar(200) DEFAULT NULL,
  `wel_ctr_loc4` varchar(200) DEFAULT NULL,
  `wel_ctr_cont` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`wel_ctr_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_gentmow` (
  `wel_pattern` varchar(10) NOT NULL DEFAULT '',
  `wel_pat_des` varchar(50) NOT NULL,
  `wel_mo_nextno` int(11) NOT NULL,
  PRIMARY KEY (`wel_pattern`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_gentmrw` (
  `wel_pattern` varchar(20) NOT NULL,
  `wel_pat_des` varchar(50) NOT NULL,
  `wel_mr_nextno` int(11) NOT NULL,
  PRIMARY KEY (`wel_pattern`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_gentwow` (
  `wel_pattern` varchar(10) NOT NULL DEFAULT '',
  `wel_pat_des` varchar(50) NOT NULL,
  `wel_wo_nextno` int(11) NOT NULL,
  PRIMARY KEY (`wel_pattern`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_mordetm` (
  `wel_ctr_code` varchar(10) DEFAULT NULL,
  `wel_mo_no` varchar(255) NOT NULL DEFAULT '',
  `wel_mo_line` int(11) NOT NULL DEFAULT '0',
  `wel_part_no` varchar(36) DEFAULT NULL,
  `wel_req_qty` decimal(20,6) DEFAULT NULL,
  `wel_req_date` date DEFAULT NULL,
  `wel_start_date` date DEFAULT NULL,
  `wel_so_no` varchar(20) DEFAULT NULL,
  `wel_so_line` int(11) DEFAULT NULL,
  `wel_wo_qty` decimal(20,6) DEFAULT NULL,
  `wel_fg_qty` decimal(20,6) DEFAULT NULL,
  `wel_qc_qty` decimal(20,6) DEFAULT NULL,
  `wel_exposure` tinyint(1) DEFAULT NULL,
  `wel_net_qty` decimal(20,6) DEFAULT NULL,
  `wel_crt_user` varchar(30) DEFAULT NULL,
  `wel_crt_date` datetime DEFAULT NULL,
  `wel_upd_user` varchar(30) DEFAULT NULL,
  `wel_upd_date` datetime DEFAULT NULL,
  PRIMARY KEY (`wel_mo_no`,`wel_mo_line`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_morhdrm` (
  `wel_pattern` varchar(10) DEFAULT NULL,
  `wel_mo_no` varchar(20) NOT NULL DEFAULT '',
  `wel_ctr_code` varchar(20) DEFAULT NULL,
  `wel_stat_date` date DEFAULT NULL,
  `wel_req_date` date DEFAULT NULL,
  `wel_alrm_date` date DEFAULT NULL,
  `wel_mo_remark` text,
  `wel_alrm_remark` text,
  `wel_last_line` int(11) DEFAULT NULL,
  `wel_close_yn` tinyint(1) DEFAULT NULL,
  `wel_crt_user` varchar(30) DEFAULT NULL,
  `wel_crt_date` datetime DEFAULT NULL,
  `wel_upd_user` varchar(30) DEFAULT NULL,
  `wel_upd_date` datetime DEFAULT NULL,
  PRIMARY KEY (`wel_mo_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_mrdetfm` (
  `wel_mr_no` varchar(20) NOT NULL DEFAULT '',
  `wel_mr_line` int(11) NOT NULL DEFAULT '0',
  `wel_ctr_code` varchar(20) DEFAULT NULL,
  `wel_part_no` varchar(36) DEFAULT NULL,
  `wel_cat_code` varchar(20) DEFAULT NULL,
  `wel_req_qty` decimal(20,6) DEFAULT NULL,
  `wel_os_qty` decimal(20,6) DEFAULT NULL,
  `wel_req_date` date DEFAULT NULL,
  `wel_remark` text,
  `wel_wo_no` varchar(20) DEFAULT NULL,
  `wel_model_no` varchar(36) DEFAULT NULL,
  `wel_reason_code` varchar(20) DEFAULT NULL,
  `wel_closed` tinyint(1) DEFAULT NULL,
  `wel_crt_user` varchar(30) DEFAULT NULL,
  `wel_crt_date` datetime DEFAULT NULL,
  `wel_upd_user` varchar(30) DEFAULT NULL,
  `wel_upd_date` datetime DEFAULT NULL,
  PRIMARY KEY (`wel_mr_no`,`wel_mr_line`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_mrhdrfm` (
  `wel_pattern` varchar(10) DEFAULT NULL,
  `wel_mr_no` varchar(20) NOT NULL DEFAULT '',
  `wel_ctr_code` varchar(20) DEFAULT NULL,
  `wel_req_date` date DEFAULT NULL,
  `wel_last_line` int(11) DEFAULT NULL,
  `wel_closed` tinyint(1) DEFAULT NULL,
  `wel_remark` text,
  `wel_crt_user` varchar(30) DEFAULT NULL,
  `wel_crt_date` datetime DEFAULT NULL,
  `wel_upd_user` varchar(30) DEFAULT NULL,
  `wel_upd_date` datetime DEFAULT NULL,
  PRIMARY KEY (`wel_mr_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_wordetm` (
  `wel_wo_no` varchar(20) NOT NULL DEFAULT '',
  `wel_wo_line` int(11) NOT NULL DEFAULT '0',
  `wel_assm_no` varchar(36) DEFAULT NULL,
  `wel_part_no` varchar(36) DEFAULT NULL,
  `wel_org_part` varchar(36) DEFAULT NULL,
  `wel_prod_code` varchar(20) DEFAULT NULL,
  `wel_cat_code` varchar(20) DEFAULT NULL,
  `wel_req_date` date DEFAULT NULL,
  `wel_qty_per` decimal(20,6) DEFAULT NULL,
  `wel_qp_ratio` decimal(20,6) DEFAULT NULL,
  `wel_req_qty` decimal(20,6) DEFAULT NULL,
  `wel_iss_qty` decimal(20,6) DEFAULT NULL,
  `wel_spar_qty` decimal(20,6) DEFAULT NULL,
  `wel_scp_qty` decimal(20,6) DEFAULT NULL,
  `wel_rtn_qty` decimal(20,6) DEFAULT NULL,
  `wel_avg_cost` decimal(20,6) DEFAULT NULL,
  `wel_std_cost` decimal(20,6) DEFAULT NULL,
  `wel_prn_qty` decimal(20,6) DEFAULT NULL,
  `wel_org_qty` decimal(20,6) DEFAULT NULL,
  `wel_alt_flag` tinyint(1) DEFAULT NULL,
  `wel_fm_bom` tinyint(1) DEFAULT NULL,
  `wel_remark` text,
  `wel_crt_user` varchar(30) DEFAULT NULL,
  `wel_crt_date` datetime DEFAULT NULL,
  `wel_upd_user` varchar(30) DEFAULT NULL,
  `wel_upd_date` datetime DEFAULT NULL,
  PRIMARY KEY (`wel_wo_no`,`wel_wo_line`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_worhdrm` (
  `wel_pattern` varchar(10) DEFAULT NULL,
  `wel_wo_no` varchar(20) NOT NULL DEFAULT '',
  `wel_ctr_code` varchar(20) DEFAULT NULL,
  `wel_mo_no` varchar(20) DEFAULT NULL,
  `wel_mo_line` int(11) DEFAULT NULL,
  `wel_so_no` varchar(20) DEFAULT NULL,
  `wel_so_line` int(11) DEFAULT NULL,
  `wel_part_no` varchar(36) DEFAULT NULL,
  `wel_req_qty` decimal(20,2) DEFAULT NULL,
  `wel_fg_qty` decimal(20,2) DEFAULT NULL,
  `wel_req_date` date DEFAULT NULL,
  `wel_stat_date` date DEFAULT NULL,
  `wel_bom_date` date DEFAULT NULL,
  `wel_bom_by` varchar(30) DEFAULT NULL,
  `wel_bom_rev` decimal(20,2) DEFAULT NULL,
  `wel_std_lot_qty` decimal(20,2) DEFAULT NULL,
  `wel_scp_check` tinyint(1) DEFAULT NULL,
  `wel_net_qty` decimal(20,6) DEFAULT NULL,
  `wel_last_line` int(11) DEFAULT NULL,
  `wel_wo_remark` text,
  `wel_alrm_remark` text,
  `wel_cancel_yn` tinyint(4) DEFAULT NULL,
  `wel_cancel_by` varchar(30) DEFAULT NULL,
  `wel_cancel_date` datetime DEFAULT NULL,
  `wel_close_yn` tinyint(1) DEFAULT NULL,
  `wel_close_by` varchar(30) DEFAULT NULL,
  `wel_close_date` datetime DEFAULT NULL,
  `wel_crt_user` varchar(30) DEFAULT NULL,
  `wel_crt_date` datetime DEFAULT NULL,
  `wel_upd_user` varchar(30) DEFAULT NULL,
  `wel_upd_date` datetime DEFAULT NULL,
  PRIMARY KEY (`wel_wo_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_areaflm` (
  `wel_area_code` varchar(20) NOT NULL,
  `wel_area_des` varchar(50) NOT NULL,
  PRIMARY KEY (`wel_area_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_cuscont` (
  `wel_cus_code` varchar(20) NOT NULL DEFAULT '',
  `wel_cont_line` int(11) NOT NULL DEFAULT '0',
  `wel_cont_man` varchar(30) DEFAULT NULL,
  `wel_cont_tel` varchar(50) DEFAULT NULL,
  `wel_cont_fax` varchar(50) DEFAULT NULL,
  `wel_cont_tel2` varchar(50) DEFAULT NULL,
  `wel_cont_mob` varchar(50) DEFAULT NULL,
  `wel_email` varchar(100) DEFAULT NULL,
  `wel_appointment` varchar(50) DEFAULT NULL,
  `wel_dept` varchar(50) DEFAULT NULL,
  `wel_hobby` varchar(100) DEFAULT NULL,
  `wel_birthday` date DEFAULT NULL,
  `wel_default` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`wel_cus_code`,`wel_cont_line`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_cusmasm` (
  `wel_cus_code` varchar(20) NOT NULL DEFAULT '',
  `wel_cus_des` varchar(100) DEFAULT NULL,
  `wel_cus_des1` varchar(100) DEFAULT NULL,
  `wel_abbre_des` varchar(50) DEFAULT NULL,
  `wel_cus_add1` varchar(100) DEFAULT NULL,
  `wel_cus_add2` varchar(100) DEFAULT NULL,
  `wel_cus_add3` varchar(100) DEFAULT NULL,
  `wel_cus_add4` varchar(100) DEFAULT NULL,
  `wel_post_no` varchar(20) DEFAULT NULL,
  `wel_cus_cont` varchar(50) DEFAULT NULL,
  `wel_cus_tele` varchar(100) DEFAULT NULL,
  `wel_cus_fax` varchar(100) DEFAULT NULL,
  `wel_web_site` varchar(100) DEFAULT NULL,
  `wel_cus_email` varchar(100) DEFAULT NULL,
  `wel_cus_term` varchar(20) DEFAULT NULL,
  `wel_cus_pbase` varchar(20) DEFAULT NULL,
  `wel_cus_svia` varchar(20) DEFAULT NULL,
  `wel_seller_code` varchar(30) DEFAULT NULL,
  `wel_cus_remark` text,
  `wel_cur_code` varchar(20) DEFAULT NULL,
  `wel_ex_rate` double DEFAULT NULL,
  `wel_area_code` varchar(20) DEFAULT NULL,
  `wel_type_code` varchar(10) DEFAULT NULL,
  `wel_sale_type` varchar(20) DEFAULT NULL,
  `wel_principal` varchar(50) DEFAULT NULL,
  `wel_tax_no` varchar(50) DEFAULT NULL,
  `wel_tax_type` int(11) DEFAULT NULL,
  `wel_cus_bank` varchar(100) DEFAULT NULL,
  `wel_cus_bank_acc` varchar(50) DEFAULT NULL,
  `wel_sale_no` varchar(50) DEFAULT NULL,
  `wel_ams_yn` tinyint(4) DEFAULT NULL,
  `wel_cr_lmt` int(11) DEFAULT NULL,
  `wel_cr_day` int(11) DEFAULT NULL,
  `wel_cr_hold` tinyint(4) DEFAULT NULL,
  `wel_os_inv` double DEFAULT NULL,
  `wel_crt_user` varchar(30) DEFAULT NULL,
  `wel_crt_date` datetime DEFAULT NULL,
  `wel_upd_user` varchar(30) DEFAULT NULL,
  `wel_upd_date` datetime DEFAULT NULL,
  PRIMARY KEY (`wel_cus_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_cusmodm` (
  `wel_cus_code` varchar(20) NOT NULL DEFAULT '',
  `wel_cus_line` int(11) NOT NULL DEFAULT '0',
  `wel_part_no` varchar(36) NOT NULL DEFAULT '',
  `wel_cus_part` varchar(50) DEFAULT NULL,
  `wel_cus_part_des1` varchar(100) DEFAULT NULL,
  `wel_cus_part_des2` varchar(100) DEFAULT NULL,
  `wel_cus_part_des3` varchar(100) DEFAULT NULL,
  `wel_cus_part_des4` varchar(100) DEFAULT NULL,
  `wel_std_cur` varchar(20) DEFAULT NULL,
  `wel_std_price` decimal(20,4) DEFAULT NULL,
  `wel_roll_qty` decimal(20,2) DEFAULT NULL,
  `wel_crt_user` varchar(30) DEFAULT NULL,
  `wel_crt_date` datetime DEFAULT NULL,
  `wel_upd_user` varchar(30) DEFAULT NULL,
  `wel_upd_date` datetime DEFAULT NULL,
  PRIMARY KEY (`wel_cus_code`,`wel_part_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_delyflm` (
  `wel_cus_code` varchar(20) NOT NULL DEFAULT '',
  `wel_dely_line` int(11) DEFAULT NULL,
  `wel_dely_code` varchar(20) NOT NULL DEFAULT '',
  `wel_dely_des` varchar(100) DEFAULT NULL,
  `wel_dely_add1` varchar(100) DEFAULT NULL,
  `wel_dely_add2` varchar(100) DEFAULT NULL,
  `wel_dely_add3` varchar(100) DEFAULT NULL,
  `wel_dely_add4` varchar(100) DEFAULT NULL,
  `wel_dely_cont` varchar(50) DEFAULT NULL,
  `wel_dely_tel` varchar(50) DEFAULT NULL,
  `wel_dely_fax` varchar(50) DEFAULT NULL,
  `wel_dely_email` varchar(100) DEFAULT NULL,
  `wel_dely_mob` varchar(50) DEFAULT NULL,
  `wel_default` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`wel_cus_code`,`wel_dely_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_gentivw` (
  `wel_pattern` varchar(20) NOT NULL DEFAULT '',
  `wel_pat_des` varchar(20) DEFAULT NULL,
  `wel_iv_nextno` int(11) DEFAULT NULL,
  `wel_checkdo` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`wel_pattern`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_gentsow` (
  `wel_pattern` varchar(10) NOT NULL DEFAULT '',
  `wel_pat_des` varchar(50) DEFAULT NULL,
  `wel_so_nextno` int(11) DEFAULT '0',
  PRIMARY KEY (`wel_pattern`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_invdetm` (
  `wel_inv_no` varchar(50) NOT NULL DEFAULT '',
  `wel_inv_line` int(11) NOT NULL DEFAULT '0',
  `wel_dn_no` varchar(20) DEFAULT NULL,
  `wel_dn_line` int(11) DEFAULT NULL,
  `wel_do_no` varchar(50) DEFAULT NULL,
  `wel_do_line` int(10) DEFAULT NULL,
  `wel_req_qty` decimal(20,6) DEFAULT NULL,
  `wel_cus_ord` varchar(255) DEFAULT NULL,
  `wel_cus_code` varchar(50) DEFAULT NULL,
  `wel_part_no` varchar(36) DEFAULT NULL,
  `wel_part_des` varchar(255) DEFAULT NULL,
  `wel_part_des1` varchar(255) DEFAULT NULL,
  `wel_part_des2` varchar(255) DEFAULT NULL,
  `wel_part_des3` varchar(255) DEFAULT NULL,
  `wel_part_des4` varchar(255) DEFAULT NULL,
  `wel_so_no` varchar(20) DEFAULT NULL,
  `wel_so_line` int(11) DEFAULT NULL,
  `wel_u_price` decimal(20,6) DEFAULT NULL,
  `wel_inv_remark` text,
  `wel_tax_rate` decimal(20,6) DEFAULT NULL,
  `wel_discount` decimal(20,6) DEFAULT NULL,
  `wel_is_inv` tinyint(4) DEFAULT '1',
  `wel_crt_user` varchar(30) DEFAULT NULL,
  `wel_crt_date` datetime DEFAULT NULL,
  `wel_upd_user` varchar(30) DEFAULT NULL,
  `wel_upd_date` datetime DEFAULT NULL,
  `wel_item_amt` decimal(20,6) DEFAULT NULL,
  `wel_dis_amt` decimal(20,6) DEFAULT NULL,
  `wel_dis_amt2` decimal(20,6) DEFAULT NULL,
  `wel_tax_amt` decimal(20,6) DEFAULT NULL,
  `wel_line_amt` decimal(20,6) DEFAULT NULL,
  PRIMARY KEY (`wel_inv_no`,`wel_inv_line`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_invhdrm` (
  `wel_inv_no` varchar(20) NOT NULL,
  `wel_pattern` varchar(10) DEFAULT NULL,
  `wel_comp_code` varchar(10) DEFAULT NULL,
  `wel_so_no` varchar(20) DEFAULT NULL,
  `wel_cus_code` varchar(20) DEFAULT NULL,
  `wel_dn_no` varchar(100) DEFAULT NULL,
  `wel_cur_code` varchar(10) DEFAULT NULL,
  `wel_ex_rate` decimal(20,6) DEFAULT NULL,
  `wel_inv_date` date DEFAULT NULL,
  `wel_tran_date` date DEFAULT NULL,
  `wel_dely_code` varchar(255) DEFAULT NULL,
  `wel_inv_remark` text,
  `wel_posted` tinyint(4) DEFAULT '0',
  `wel_lock_yn` tinyint(1) DEFAULT NULL,
  `wel_discount` decimal(20,6) DEFAULT NULL,
  `wel_tax_type` tinyint(3) unsigned DEFAULT NULL,
  `wel_tax_amt` decimal(20,6) DEFAULT NULL,
  `wel_seller_code` varchar(10) DEFAULT NULL,
  `wel_is_inv` tinyint(4) DEFAULT '1',
  `wel_cus_pbase` varchar(30) DEFAULT NULL,
  `wel_quot_qty` decimal(20,6) DEFAULT NULL,
  `wel_cus_term` varchar(50) DEFAULT NULL,
  `wel_shp_remark` text,
  `wel_crt_user` varchar(50) DEFAULT NULL,
  `wel_crt_date` date DEFAULT NULL,
  `wel_appr_yn` tinyint(4) DEFAULT NULL,
  `wel_appr_by` varchar(50) DEFAULT NULL,
  `wel_appr_date` date DEFAULT NULL,
  `wel_upd_user` varchar(30) DEFAULT NULL,
  `wel_upd_date` date DEFAULT NULL,
  `wel_vessel` varchar(100) DEFAULT NULL,
  `wel_etd_date` date DEFAULT NULL,
  `wel_lc_no` varchar(255) DEFAULT NULL,
  `wel_last_line` int(11) DEFAULT NULL,
  `wel_bill_to` varchar(50) DEFAULT NULL,
  `wel_ar_date` date DEFAULT NULL,
  `wel_pol_hog` varchar(50) DEFAULT NULL,
  `wel_portchog` varchar(50) DEFAULT NULL,
  `wel_eta_date` date DEFAULT NULL,
  `wel_cont_man` varchar(30) DEFAULT NULL,
  `wel_dis_type` tinyint(11) DEFAULT NULL,
  `wel_item_amt` decimal(20,6) DEFAULT NULL,
  `wel_dis_amt` decimal(20,6) DEFAULT NULL,
  `wel_dis_amt2` decimal(20,6) DEFAULT NULL,
  `wel_order_amt` decimal(20,6) DEFAULT NULL,
  PRIMARY KEY (`wel_inv_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_invmism` (
  `wel_inv_no` varchar(255) NOT NULL DEFAULT '',
  `wel_line_no` int(11) NOT NULL DEFAULT '0',
  `wel_item` text,
  `wel_itemamt` decimal(20,6) DEFAULT NULL,
  `wel_tax_rate` decimal(20,6) DEFAULT NULL,
  `wel_is_inv` int(11) DEFAULT '1',
  `wel_crt_user` varchar(30) DEFAULT NULL,
  `wel_crt_date` datetime DEFAULT NULL,
  `wel_upd_user` varchar(30) DEFAULT NULL,
  `wel_upd_date` datetime DEFAULT NULL,
  `wel_discount` decimal(20,6) DEFAULT NULL,
  `wel_dis_amt` decimal(20,6) DEFAULT NULL,
  `wel_dis_amt2` decimal(20,6) DEFAULT NULL,
  `wel_tax_amt` decimal(20,6) DEFAULT NULL,
  `wel_line_amt` decimal(20,6) DEFAULT NULL,
  PRIMARY KEY (`wel_inv_no`,`wel_line_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_mkthanm` (
  `wel_seller_code` varchar(30) NOT NULL,
  `wel_seller_name` varchar(50) DEFAULT NULL,
  `wel_cm_rate` decimal(20,4) DEFAULT NULL,
  `wel_area_code` varchar(20) DEFAULT NULL,
  `wel_e_mail` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`wel_seller_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_paytflm` (
  `wel_pay_type` varchar(20) DEFAULT NULL,
  `wel_pay_des` varchar(50) DEFAULT NULL,
  `wel_cr_day` int(11) DEFAULT NULL,
  `wel_ams_yn` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`wel_pay_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_sordetm` (
  `wel_cus_code` varchar(20) DEFAULT NULL,
  `wel_so_no` varchar(20) NOT NULL,
  `wel_so_line` int(11) NOT NULL DEFAULT '0',
  `wel_part_no` varchar(36) DEFAULT NULL,
  `wel_req_date` date DEFAULT NULL,
  `wel_req_qty` decimal(20,6) DEFAULT '0.000000',
  `wel_u_price` decimal(20,6) DEFAULT '0.000000',
  `wel_os_qty` decimal(20,6) DEFAULT '0.000000',
  `wel_do_qty` decimal(20,6) DEFAULT NULL,
  `wel_dn_qty` decimal(20,6) DEFAULT NULL,
  `wel_area_code` varchar(20) DEFAULT NULL,
  `wel_cus_part` varchar(50) DEFAULT NULL,
  `wel_cus_date` date DEFAULT NULL,
  `wel_pmc_date` date DEFAULT NULL,
  `wel_quot_no` varchar(20) DEFAULT NULL,
  `wel_tax_rate` decimal(20,6) DEFAULT NULL,
  `wel_discount` decimal(20,6) DEFAULT NULL,
  `wel_so_rmk` text,
  `wel_item_amt` decimal(20,6) DEFAULT NULL,
  `wel_dis_amt` decimal(20,6) DEFAULT NULL,
  `wel_dis_amt2` decimal(20,6) DEFAULT NULL,
  `wel_tax_amt` decimal(20,6) DEFAULT NULL,
  `wel_line_amt` decimal(20,6) DEFAULT NULL,
  `wel_crt_user` varchar(30) DEFAULT NULL,
  `wel_crt_date` datetime DEFAULT NULL,
  `wel_upd_user` varchar(30) DEFAULT NULL,
  `wel_upd_date` datetime DEFAULT NULL,
  PRIMARY KEY (`wel_so_no`,`wel_so_line`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_sorhdrm` (
  `wel_cus_code` varchar(20) DEFAULT NULL,
  `wel_comp_code` varchar(20) DEFAULT NULL,
  `wel_pattern` varchar(10) DEFAULT NULL,
  `wel_so_no` varchar(20) NOT NULL DEFAULT '',
  `wel_rev_no` int(11) DEFAULT NULL,
  `wel_cus_ord` varchar(50) DEFAULT NULL,
  `wel_req_date` date DEFAULT NULL,
  `wel_seller_code` varchar(30) DEFAULT NULL,
  `wel_cur_code` varchar(20) DEFAULT NULL,
  `wel_ex_rate` decimal(20,6) DEFAULT '0.000000',
  `wel_cus_term` varchar(20) DEFAULT NULL,
  `wel_cus_pbase` varchar(20) DEFAULT NULL,
  `wel_cus_svia` varchar(20) DEFAULT NULL,
  `wel_dely_code` varchar(20) DEFAULT NULL,
  `wel_cont_man` varchar(30) DEFAULT NULL,
  `wel_discount` decimal(20,6) DEFAULT NULL,
  `wel_tax_type` int(11) DEFAULT NULL,
  `wel_tax_amt` decimal(20,6) DEFAULT NULL,
  `wel_dis_amt` decimal(20,6) DEFAULT NULL,
  `wel_remark` text,
  `wel_last_line` int(11) DEFAULT NULL,
  `wel_alrm_date` date DEFAULT NULL,
  `wel_alrm_remark` text,
  `wel_finished` tinyint(1) DEFAULT NULL,
  `wel_appr_yn` tinyint(4) DEFAULT NULL,
  `wel_appr_by` varchar(30) DEFAULT NULL,
  `wel_appr_date` date DEFAULT NULL,
  `wel_crt_user` varchar(30) DEFAULT NULL,
  `wel_crt_date` datetime DEFAULT NULL,
  `wel_upd_user` varchar(30) DEFAULT NULL,
  `wel_upd_date` datetime DEFAULT NULL,
  `wel_dis_type` tinyint(11) DEFAULT NULL,
  `wel_item_amt` decimal(20,6) DEFAULT NULL,
  `wel_dis_amt2` decimal(20,6) DEFAULT NULL,
  `wel_order_amt` decimal(20,6) DEFAULT NULL,
  PRIMARY KEY (`wel_so_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_sormism` (
  `wel_so_no` varchar(20) NOT NULL DEFAULT '',
  `wel_line_no` int(11) NOT NULL DEFAULT '0',
  `wel_item` varchar(100) DEFAULT NULL,
  `wel_item_amt` decimal(20,6) DEFAULT '0.000000',
  `wel_tax_rate` decimal(20,6) DEFAULT NULL,
  `wel_discount` decimal(20,6) DEFAULT NULL,
  `wel_dis_amt` decimal(20,6) DEFAULT NULL,
  `wel_dis_amt2` decimal(20,6) DEFAULT NULL,
  `wel_tax_amt` decimal(20,6) DEFAULT NULL,
  `wel_line_amt` decimal(20,6) DEFAULT NULL,
  `wel_crt_user` varchar(30) DEFAULT NULL,
  `wel_crt_date` datetime DEFAULT NULL,
  `wel_upd_user` varchar(30) DEFAULT NULL,
  `wel_upd_date` datetime DEFAULT NULL,
  PRIMARY KEY (`wel_so_no`,`wel_line_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_closedm` (
  `wel_phy_date` date DEFAULT NULL,
  `wel_tran_code` varchar(10) DEFAULT NULL,
  `wel_period1` date DEFAULT NULL,
  `wel_period2` date DEFAULT NULL,
  `wel_period3` date DEFAULT NULL,
  `wel_period4` date DEFAULT NULL,
  `wel_period5` date DEFAULT NULL,
  `wel_period6` date DEFAULT NULL,
  `wel_period7` date DEFAULT NULL,
  `wel_period8` date DEFAULT NULL,
  `wel_period9` date DEFAULT NULL,
  `wel_period10` date DEFAULT NULL,
  `wel_period11` date DEFAULT NULL,
  `wel_period12` date DEFAULT NULL,
  `wel_lock_yn` tinyint(1) NOT NULL,
  `wel_phy_d1` date DEFAULT NULL,
  `wel_phy_d2` date DEFAULT NULL,
  `wel_phy_d3` date DEFAULT NULL,
  `wel_phy_d4` date DEFAULT NULL,
  `wel_phy_d5` date DEFAULT NULL,
  `wel_phy_d6` date DEFAULT NULL,
  `wel_phy_d7` date DEFAULT NULL,
  `wel_phy_d8` date DEFAULT NULL,
  `wel_phy_d9` date DEFAULT NULL,
  `wel_phy_d10` date DEFAULT NULL,
  `wel_phy_d11` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_countfm` (
  `wel_wh_code` varchar(30) NOT NULL DEFAULT '',
  `wel_tag_no` varchar(50) NOT NULL DEFAULT '',
  `wel_part_no` varchar(36) NOT NULL,
  `wel_lot_no` varchar(50) DEFAULT NULL,
  `wel_stk_loc` varchar(50) DEFAULT NULL,
  `wel_eff_date` date DEFAULT NULL,
  `wel_tag_qty` decimal(20,4) NOT NULL,
  PRIMARY KEY (`wel_wh_code`,`wel_part_no`,`wel_tag_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_countft` (
  `wel_wh_code` varchar(20) NOT NULL,
  `wel_tag_no` varchar(20) NOT NULL DEFAULT '',
  `wel_part_no` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`wel_wh_code`,`wel_tag_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_dndetfm` (
  `wel_dn_no` varchar(20) NOT NULL DEFAULT '',
  `wel_dn_line` int(11) NOT NULL DEFAULT '0',
  `wel_so_no` varchar(20) DEFAULT NULL,
  `wel_so_line` int(10) DEFAULT NULL,
  `wel_part_no` varchar(50) DEFAULT NULL,
  `wel_tran_qty` decimal(20,6) DEFAULT NULL,
  `wel_cus_code` varchar(20) DEFAULT NULL,
  `wel_u_price` decimal(20,6) DEFAULT NULL,
  `wel_req_qty` decimal(20,6) DEFAULT NULL,
  `wel_crt_user` varchar(50) DEFAULT NULL,
  `wel_crt_date` datetime DEFAULT NULL,
  `wel_upd_user` varchar(50) DEFAULT NULL,
  `wel_upd_date` datetime DEFAULT NULL,
  `wel_do_no` varchar(20) DEFAULT NULL,
  `wel_do_line` tinyint(11) DEFAULT NULL,
  `wel_inv_qty` decimal(20,6) DEFAULT NULL,
  PRIMARY KEY (`wel_dn_no`,`wel_dn_line`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_dnhdrfm` (
  `wel_dn_no` varchar(20) NOT NULL DEFAULT '',
  `wel_pattern` varchar(20) DEFAULT NULL,
  `wel_dn_date` date DEFAULT NULL,
  `wel_cus_code` varchar(20) DEFAULT NULL,
  `wel_comp_code` varchar(20) DEFAULT NULL,
  `wel_last_line` int(11) DEFAULT '0',
  `wel_remark` text,
  `wel_crt_user` varchar(30) DEFAULT NULL,
  `wel_crt_date` datetime DEFAULT NULL,
  `wel_upd_user` varchar(30) DEFAULT NULL,
  `wel_upd_date` datetime DEFAULT NULL,
  `wel_dely_code` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`wel_dn_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_gentdnw` (
  `wel_pattern` varchar(10) NOT NULL DEFAULT '',
  `wel_pat_des` varchar(50) DEFAULT NULL,
  `wel_dn_nextno` int(11) DEFAULT NULL,
  PRIMARY KEY (`wel_pattern`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_genttow` (
  `wel_pattern` varchar(9) NOT NULL DEFAULT '',
  `wel_pat_des` varchar(50) DEFAULT NULL,
  `wel_to_nextno` int(11) DEFAULT NULL,
  PRIMARY KEY (`wel_pattern`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_partwcm` (
  `wel_part_no` varchar(36) NOT NULL,
  `wel_cut_date` date NOT NULL DEFAULT '0000-00-00',
  `wel_wh_code` varchar(20) DEFAULT NULL,
  `wel_fif_cost` decimal(20,6) DEFAULT NULL,
  `wel_avg_cost` decimal(20,6) DEFAULT NULL,
  `wel_std_cost` decimal(20,6) DEFAULT NULL,
  `wel_lat_cost` decimal(20,6) DEFAULT NULL,
  `wel_vqt_cost` decimal(20,6) DEFAULT NULL,
  PRIMARY KEY (`wel_part_no`,`wel_cut_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_partwhm` (
  `wel_part_no` varchar(36) NOT NULL,
  `wel_wh_code` varchar(20) NOT NULL,
  `wel_u_price` decimal(20,6) DEFAULT NULL,
  `wel_phy_qty` decimal(20,6) DEFAULT NULL,
  `wel_period1` decimal(20,6) DEFAULT NULL,
  `wel_period2` decimal(20,6) DEFAULT NULL,
  `wel_period3` decimal(20,6) DEFAULT NULL,
  `wel_period4` decimal(20,6) DEFAULT NULL,
  `wel_period5` decimal(20,6) DEFAULT NULL,
  `wel_period6` decimal(20,6) DEFAULT NULL,
  `wel_period7` decimal(20,6) DEFAULT NULL,
  `wel_period8` decimal(20,6) DEFAULT NULL,
  `wel_period9` decimal(20,6) DEFAULT NULL,
  `wel_period10` decimal(20,6) DEFAULT NULL,
  `wel_period11` decimal(20,6) DEFAULT NULL,
  `wel_period12` decimal(20,6) DEFAULT NULL,
  `wel_creat_d` date DEFAULT NULL,
  `wel_phy_qty1` decimal(20,4) DEFAULT NULL,
  `wel_phy_qty2` decimal(20,4) DEFAULT NULL,
  `wel_phy_qty3` decimal(20,4) DEFAULT NULL,
  `wel_phy_qty4` decimal(20,4) DEFAULT NULL,
  `wel_phy_qty5` decimal(20,4) DEFAULT NULL,
  `wel_phy_qty6` decimal(20,4) DEFAULT NULL,
  `wel_phy_qty7` decimal(20,4) DEFAULT NULL,
  `wel_phy_qty8` decimal(20,4) DEFAULT NULL,
  `wel_phy_qty9` decimal(20,4) DEFAULT NULL,
  `wel_phy_qty10` decimal(20,4) DEFAULT NULL,
  `wel_phy_qty11` decimal(20,4) DEFAULT NULL,
  PRIMARY KEY (`wel_part_no`,`wel_wh_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_ptrdetm` (
  `wel_pt_no` varchar(20) NOT NULL DEFAULT '',
  `wel_pt_line` decimal(20,0) NOT NULL,
  `wel_part_no` varchar(36) DEFAULT NULL,
  `wel_po_no` varchar(20) DEFAULT NULL,
  `wel_po_line` int(11) DEFAULT NULL,
  `wel_req_date` date DEFAULT NULL,
  `wel_u_price` decimal(20,4) DEFAULT NULL,
  `wel_rcv_qty` decimal(20,2) DEFAULT NULL,
  `wel_rtv_qty` decimal(20,2) DEFAULT NULL,
  `wel_rn_qty` decimal(20,2) DEFAULT NULL,
  `wel_os_qty` decimal(20,2) DEFAULT NULL,
  `wel_pt_rmk` text,
  `wel_inv_qty` decimal(20,2) DEFAULT NULL,
  `wel_spar_qty` decimal(20,2) DEFAULT NULL,
  `wel_spar_os_qty` decimal(20,2) DEFAULT NULL,
  `wel_iqc_qty` decimal(20,2) DEFAULT NULL,
  `wel_unpass_qty` decimal(20,2) DEFAULT NULL,
  `wel_closed` tinyint(1) NOT NULL,
  `wel_unit` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`wel_pt_no`,`wel_pt_line`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_ptrhdrm` (
  `wel_pt_no` varchar(20) NOT NULL DEFAULT '',
  `wel_pattern` varchar(10) DEFAULT NULL,
  `wel_comp_code` varchar(20) DEFAULT NULL,
  `wel_ven_code` varchar(20) DEFAULT NULL,
  `wel_req_date` date DEFAULT NULL,
  `wel_ven_dn` varchar(30) DEFAULT NULL,
  `wel_last_line` int(11) DEFAULT NULL,
  `wel_order_type` varchar(10) DEFAULT NULL,
  `wel_pt_remark` text,
  `wel_appr_yn` tinyint(1) NOT NULL,
  `wel_appr_by` varchar(30) DEFAULT NULL,
  `wel_appr_date` datetime DEFAULT NULL,
  `wel_crt_user` varchar(30) DEFAULT NULL,
  `wel_crt_date` datetime DEFAULT NULL,
  `wel_upd_user` varchar(30) DEFAULT NULL,
  `wel_upd_date` datetime DEFAULT NULL,
  PRIMARY KEY (`wel_pt_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_rcvnote` (
  `wel_pattern` varchar(10) DEFAULT NULL,
  `wel_pat_des` varchar(50) DEFAULT NULL,
  `wel_po_nextno` int(11) DEFAULT NULL,
  PRIMARY KEY (`wel_pattern`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_rtvnote` (
  `wel_pattern` varchar(4) DEFAULT NULL,
  `wel_pat_des` varchar(50) DEFAULT NULL,
  `wel_po_nextno` int(11) DEFAULT NULL,
  PRIMARY KEY (`wel_pattern`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_tordetm` (
  `wel_to_no` varchar(20) NOT NULL DEFAULT '',
  `wel_to_line` int(11) NOT NULL DEFAULT '0',
  `wel_part_no` varchar(36) DEFAULT NULL,
  `wel_lot_no_fm` varchar(20) DEFAULT NULL,
  `wel_req_qty` decimal(20,2) DEFAULT NULL,
  `wel_post_qty` decimal(20,2) DEFAULT NULL,
  `wel_ref_no` varchar(20) DEFAULT NULL,
  `wel_to_rmk` text,
  `wel_crt_user` varchar(30) DEFAULT NULL,
  `wel_crt_date` datetime DEFAULT NULL,
  `wel_upd_user` varchar(30) DEFAULT NULL,
  `wel_upd_date` datetime DEFAULT NULL,
  `wel_lot_no_to` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`wel_to_no`,`wel_to_line`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_torhdrm` (
  `wel_to_no` varchar(20) NOT NULL DEFAULT '',
  `wel_pattern` varchar(20) DEFAULT NULL,
  `wel_last_line` int(11) DEFAULT NULL,
  `wel_ctrl_flow` varchar(20) DEFAULT NULL,
  `wel_wh_fm` varchar(20) DEFAULT NULL,
  `wel_wh_to` varchar(20) DEFAULT NULL,
  `wel_req_date` date DEFAULT NULL,
  `wel_to_remark` text,
  `wel_crt_user` varchar(30) DEFAULT NULL,
  `wel_crt_date` datetime DEFAULT NULL,
  `wel_upd_user` varchar(30) DEFAULT NULL,
  `wel_upd_date` datetime DEFAULT NULL,
  PRIMARY KEY (`wel_to_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_tranflm` (
  `wel_trn_recno` decimal(20,0) DEFAULT NULL,
  `wel_wh_fm` varchar(50) DEFAULT NULL,
  `wel_wh_to` varchar(50) DEFAULT NULL,
  `wel_lot_no_fm` varchar(50) DEFAULT NULL,
  `wel_lot_no_to` varchar(50) DEFAULT NULL,
  `wel_tran_code` varchar(20) DEFAULT NULL,
  `wel_flow_code` varchar(20) DEFAULT NULL,
  `wel_ctr_code` varchar(20) DEFAULT NULL,
  `wel_ven_code` varchar(20) DEFAULT NULL,
  `wel_cus_code` varchar(20) DEFAULT NULL,
  `wel_stk_loc_fm` varchar(50) DEFAULT NULL,
  `wel_stk_loc_to` varchar(20) DEFAULT NULL,
  `wel_eff_date` date DEFAULT NULL,
  `wel_ref_no` varchar(20) DEFAULT NULL,
  `wel_po_no` varchar(20) DEFAULT NULL,
  `wel_po_line` int(11) DEFAULT NULL,
  `wel_mr_no` varchar(20) DEFAULT NULL,
  `wel_mr_line` int(11) DEFAULT NULL,
  `wel_ro_no` varchar(20) DEFAULT NULL,
  `wel_ro_line` int(11) DEFAULT NULL,
  `wel_dn_no` varchar(20) DEFAULT NULL,
  `wel_rt_no` varchar(20) DEFAULT NULL,
  `wel_rt_line` int(11) DEFAULT NULL,
  `wel_part_no` varchar(36) DEFAULT NULL,
  `wel_cat_code` varchar(20) DEFAULT NULL,
  `wel_tran_qty` decimal(20,4) DEFAULT NULL,
  `wel_spar_qty` decimal(20,4) DEFAULT NULL,
  `wel_tran_date` date DEFAULT NULL,
  `wel_tran_rmk` varchar(255) DEFAULT NULL,
  `wel_to_no` varchar(20) DEFAULT NULL,
  `wel_so_no` varchar(20) DEFAULT NULL,
  `wel_so_line` int(11) DEFAULT NULL,
  `wel_u_price` decimal(20,6) DEFAULT NULL,
  `wel_wo_no` varchar(20) DEFAULT NULL,
  `wel_assm_no` varchar(36) DEFAULT NULL,
  `wel_avg_cost` decimal(20,6) DEFAULT NULL,
  `wel_std_cost` decimal(20,6) DEFAULT NULL,
  `wel_avg_bomc` decimal(20,6) DEFAULT NULL,
  `wel_closed` tinyint(1) NOT NULL,
  `wel_repm` tinyint(4) DEFAULT NULL,
  `wel_rec_unit` varchar(20) DEFAULT NULL,
  `wel_pur_unit_rate` decimal(20,6) DEFAULT NULL,
  `wel_inv_price` decimal(20,6) DEFAULT NULL,
  `wel_inv_qty` decimal(20,2) DEFAULT NULL,
  `wel_lock_yn` tinyint(1) NOT NULL,
  `wel_comp_code` varchar(20) DEFAULT NULL,
  `wel_crt_user` varchar(40) DEFAULT NULL,
  `wel_crt_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_whctrlm` (
  `wel_ctrl_flow` varchar(20) NOT NULL DEFAULT '',
  `wel_wh_fm` varchar(20) DEFAULT NULL,
  `wel_wh_to` varchar(20) DEFAULT NULL,
  `wel_tran_code` varchar(20) DEFAULT NULL,
  `wel_ctrl_rmk1` varchar(100) DEFAULT '',
  `wel_ctrl_rmk2` varchar(100) DEFAULT '',
  `wel_ctrl_rmk3` varchar(100) DEFAULT '',
  `wel_int_flow` tinyint(1) NOT NULL,
  `wel_int_iqcf` tinyint(1) NOT NULL,
  `wel_iss_flow` tinyint(1) NOT NULL,
  `wel_rtn_flow` tinyint(1) NOT NULL,
  `wel_fgs_flow` tinyint(1) NOT NULL,
  `wel_rec_flow` tinyint(1) NOT NULL,
  `wel_shp_flow` tinyint(1) NOT NULL,
  `wel_crt_flow` tinyint(1) NOT NULL,
  `wel_vrt_flow` tinyint(1) NOT NULL,
  `wel_avg_cflow` tinyint(1) NOT NULL,
  `wel_rtv_cflow` tinyint(1) NOT NULL,
  `wel_dis_able` tinyint(1) NOT NULL,
  PRIMARY KEY (`wel_ctrl_flow`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_whlocfm` (
  `wel_wh_code` varchar(20) NOT NULL DEFAULT '',
  `wel_wh_des` varchar(50) DEFAULT NULL,
  `wel_wh_loc1` varchar(50) DEFAULT NULL,
  `wel_wh_loc2` varchar(50) DEFAULT NULL,
  `wel_wh_loc3` varchar(50) DEFAULT NULL,
  `wel_wh_loc4` varchar(50) DEFAULT NULL,
  `wel_wh_cont` varchar(50) DEFAULT NULL,
  `wel_wh_tele` varchar(50) DEFAULT NULL,
  `wel_wh_email` varchar(50) DEFAULT NULL,
  `wel_wh_cost` tinyint(1) NOT NULL,
  `wel_wh_dum` tinyint(1) NOT NULL,
  `wel_net_able` tinyint(1) NOT NULL,
  `wel_tag_no` int(11) DEFAULT NULL,
  PRIMARY KEY (`wel_wh_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_whlotfm` (
  `wel_wh_code` varchar(20) NOT NULL DEFAULT '',
  `wel_part_no` varchar(36) NOT NULL DEFAULT '',
  `wel_lot_no` varchar(20) NOT NULL DEFAULT '',
  `wel_stk_loc` varchar(20) NOT NULL DEFAULT '',
  `wel_eff_date` date DEFAULT NULL,
  `wel_stk_qty` decimal(20,6) DEFAULT NULL,
  `wel_cur_code` varchar(20) DEFAULT NULL,
  `wel_ex_rate` decimal(20,6) DEFAULT NULL,
  `wel_u_price` decimal(20,6) DEFAULT NULL,
  `wel_lock_yn` tinyint(1) DEFAULT NULL,
  `wel_net_stk` decimal(20,6) DEFAULT NULL,
  `wel_crt_user` varchar(30) DEFAULT NULL,
  `wel_crt_date` datetime DEFAULT NULL,
  `wel_upd_user` varchar(30) DEFAULT NULL,
  `wel_upd_date` datetime DEFAULT NULL,
  PRIMARY KEY (`wel_wh_code`,`wel_part_no`,`wel_lot_no`,`wel_stk_loc`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_whlotft` (
  `wel_wh_code` varchar(30) NOT NULL,
  `wel_part_no` varchar(30) NOT NULL,
  `wel_lot_no` varchar(20) DEFAULT NULL,
  `wel_stk_loc` varchar(20) DEFAULT NULL,
  `wel_eff_date` date DEFAULT NULL,
  `wel_stk_qty` decimal(20,4) DEFAULT NULL,
  `wel_crt_date` datetime DEFAULT NULL,
  PRIMARY KEY (`wel_wh_code`,`wel_part_no`,`wel_lot_no`,`wel_stk_loc`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_proghdrm` (
  `wel_prog_code` varchar(50) NOT NULL DEFAULT '',
  `wel_prog_des` varchar(100) DEFAULT NULL,
  `wel_prog_params` text,
  PRIMARY KEY (`wel_prog_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
");

//==========================================================================
//添加程序码
opt_wel_proghdrm("root", "Welcome ERP", "english=Welcome ERP\ntraditional=迎新ERP\nsimplified=迎新ERP");
opt_wel_proghdrm("engm", "Eng. Module", "english=Eng. Module\ntraditional=工程模塊\nsimplified=工程模块");	
opt_wel_proghdrm("engm002", "Part File", "english=Part File\ntraditional=物料檔案\nsimplified=物料档案");	
opt_wel_proghdrm("engm003", "Bill of Material", "english=Bill of Material\ntraditional=材料清單\nsimplified=材料清单");	
opt_wel_proghdrm("engm012", "ECN Pattern", "english=ECN Pattern\ntraditional=模式(工程更改單)\nsimplified=模式(工程更改单)");	
opt_wel_proghdrm("engm013", "ECN Maintenance", "english=ECN Maintenance\ntraditional=工程更改單\nsimplified=工程更改单");	
opt_wel_proghdrm("engm016", "ECN Positing", "english=ECN Positing\ntraditional=工程更改單(過帳)\nsimplified=工程更改单(过帐)");	
opt_wel_proghdrm("engm017", "Unit File", "english=Unit File\ntraditional=單位檔案\nsimplified=单位档案");	
opt_wel_proghdrm("engm022", "Project File", "english=Project File\ntraditional=工程項目檔案\nsimplified=工程项目档案");	
opt_wel_proghdrm("engm903", "Category File", "english=Category File\ntraditional=物料類別檔案\nsimplified=物料类别档案");	
opt_wel_proghdrm("engr002", "Part List", "english=Part List\ntraditional=物料報表\nsimplified=物料报表");	
opt_wel_proghdrm("engr003", "BOM Report", "english=BOM Report\ntraditional=材料清單報表\nsimplified=材料清单报表");	
opt_wel_proghdrm("engr004", "Reverse BOM", "english=Reverse BOM\ntraditional=反查材料清單\nsimplified=反查材料清单");
opt_wel_proghdrm("engr013", "ECN Report", "english=ECN Report\ntraditional=工程更改報表\nsimplified=工程更改报表");	
opt_wel_proghdrm("engr014", "ECN History", "english=ECN History\ntraditional=ECN History\nsimplified=ECN History");
opt_wel_proghdrm("engm_bas", "Basic File", "english=Basic File\ntraditional=基本檔案\nsimplified=基本档案");
opt_wel_proghdrm("engm_rpt", "Report", "english=Report\ntraditional=工程報表\nsimplified=工程报表");
opt_wel_proghdrm("mktm", "Marketing Module", "english=Marketing Module\ntraditional=銷售模塊\nsimplified=销售模块");	
opt_wel_proghdrm("mktm001", "Salesman File", "english=Salesman File\ntraditional=銷售員檔案\nsimplified=销售员档案");	
opt_wel_proghdrm("mktm002", "Customer File", "english=Customer File\ntraditional=客戶檔案\nsimplified=客户档案");	
opt_wel_proghdrm("mktm003", "Currency File", "english=Currency File\ntraditional=貨幣檔案\nsimplified=货币档案");	
opt_wel_proghdrm("mktm005", "Pattern (S/O)", "english=Pattern (S/O)\ntraditional=模式(銷售單)\nsimplified=模式(销售单)");	
opt_wel_proghdrm("mktm006", "Pattern (Invoice)", "english=Pattern (Invoice)\ntraditional=模式(發票)\nsimplified=模式(发票)");	
opt_wel_proghdrm("mktm009", "S/O Maintenance", "english=S/O Maintenance\ntraditional=銷售單維護\nsimplified=销售单维护");	
opt_wel_proghdrm("mktm025", "Area File", "english=Area File\ntraditional=地區檔案\nsimplified=地区档案");	
opt_wel_proghdrm("mktm034", "Family File", "english=Family File\ntraditional=產品系列檔案\nsimplified=产品系列档案");
opt_wel_proghdrm("mktm035", "Price Basis File", "english=Price Basis File\ntraditional=價格基準檔案\nsimplified=价格基准档案");
opt_wel_proghdrm("mktm038", "Invoice Maintenance", "english=Invoice Maintenance\ntraditional=發票維護\nsimplified=发票维护");
opt_wel_proghdrm("mktm042", "Payment Term File", "english=Payment Term File\ntraditional=付款方式檔案\nsimplified=付款方式档案");
opt_wel_proghdrm("mktm128", "Customer Model File", "english=Customer Model File\ntraditional=客戶貨品資料\nsimplified=客户货品资料");
opt_wel_proghdrm("mktm_bas", "Basic File", "english=Basic File\ntraditional=基本檔案\nsimplified=基本档案");
opt_wel_proghdrm("pmcm", "PMC Module", "english=PMC Module\ntraditional=PMC 模塊\nsimplified=PMC 模块");
opt_wel_proghdrm("pmcm001", "Create Center File", "english=Create Center File\ntraditional=生產中心檔案\nsimplified=生產中心档案");
opt_wel_proghdrm("pmcm002", "Pattern (M/O)", "english=Pattern (M/O)\ntraditional=模式(製造單)\nsimplified=模式(制造单)");
opt_wel_proghdrm("pmcm003", "Pattern (W/O)", "english=Pattern (W/O)\ntraditional=模式(工作單)\nsimplified=模式(工作单)");
opt_wel_proghdrm("pmcm004", "Pattern (M/R)", "english=Pattern (M/R)\ntraditional=模式(要求單)\nsimplified=模式(要求单)");
opt_wel_proghdrm("pmcm006", "M/O Maintenance", "english=M/O Maintenance\ntraditional=製造單維護\nsimplified=制造单维护");
opt_wel_proghdrm("pmcm007", "W/O Maintenance", "english=W/O Maintenance\ntraditional=工作單維護\nsimplified=工作单维护");
opt_wel_proghdrm("pmcm008", "M/R Maintenance", "english=M/R Maintenance\ntraditional=要求單維護\nsimplified=要求单维护");
opt_wel_proghdrm("pmcr001", "M/O Report", "english=M/O Report\ntraditional=M/O Report\nsimplified=M/O Report");
opt_wel_proghdrm("pmcr002", "M/O Status Look-up", "english=M/O Status Look-up\ntraditional=M/O Status Look-up\nsimplified=M/O Status Look-up");
opt_wel_proghdrm("pmcr003", "W/O Report", "english=W/O Report\ntraditional=W/O Report\nsimplified=W/O Report");
opt_wel_proghdrm("pmcr004", "W/O Status Look-up", "english=W/O Status Look-up\ntraditional=W/O Status Look-up\nsimplified=W/O Status Look-up");
opt_wel_proghdrm("pmcr005", "W/O Material Follow", "english=W/O Material Follow\ntraditional=W/O Material Follow\nsimplified=W/O Material Follow");
opt_wel_proghdrm("pmcr006", "M/R Report", "english=M/R Report\ntraditional=M/R Report\nsimplified=M/R Report");
opt_wel_proghdrm("pmcr008", "W/O Material in-Out", "english=W/O Material in-Out\ntraditional=W/O Material in-Out\nsimplified=W/O Material in-Out");
opt_wel_proghdrm("pmcr009", "W/O & M/R & Requirement Planning", 
	"english=W/O & M/R & Requirement Planning\ntraditional=W/O & M/R & Requirement Planning\nsimplified=W/O & M/R & Requirement Planning");
opt_wel_proghdrm("pmcr010", "Slow Moving", "english=Slow Moving\ntraditional=Slow Moving\nsimplified=Slow Moving");
opt_wel_proghdrm("pmcm_bas", "Basic File", "english=Basic File\ntraditional=基本檔案\nsimplified=基本档案");
opt_wel_proghdrm("purm", "Purchase Module", "english=Purchase Module\ntraditional=採購模塊\nsimplified=采购模块");
opt_wel_proghdrm("purm001", "Buyer File", "english=Buyer File\ntraditional=採購員檔案\nsimplified=采购员档案");
opt_wel_proghdrm("purm002", "Vendor File", "english=Vendor File\ntraditional=供應商檔案\nsimplified=供应商档案");
opt_wel_proghdrm("purm003", "Vendor-Part File", "english=Vendor-Part File\ntraditional=供應商物料資料\nsimplified=供应商物料资料");
opt_wel_proghdrm("purm004", "Pattern (P/O)", "english=Pattern (P/O)\ntraditional=模式(採購單)\nsimplified=模式(采购单)");
opt_wel_proghdrm("purm005", "P/O Maintenance", "english=P/O Maintenance\ntraditional=採購單維護\nsimplified=采购单维护");
opt_wel_proghdrm("purr003", "O/S P/O Report", "english=O/S P/O Report\ntraditional=採購單餘數報表\nsimplified=采购单余数报表");
opt_wel_proghdrm("purr004", "P/O Status Report", "english=P/O status Report\ntraditional=採購單狀況報表\nsimplified=采购单状况报表");
opt_wel_proghdrm("purr005", "P/O Print-Out", "english=P/O Printing\ntraditional=採購單打印\nsimplified=采购单打印");
opt_wel_proghdrm("purr006", "Vendor Quotation History", "english=Vendor Quotation History\ntraditional=供應商報價歷史\nsimplified=供应商报价历史");
opt_wel_proghdrm("purr007", "Purchase Price History", "english=Purchase Price History\ntraditional=採購價格歷史\nsimplified=采购价格历史");
opt_wel_proghdrm("purr008", "Vendor List", "english=Vendor List\ntraditional=供應商列表\nsimplified=供应商列表");
opt_wel_proghdrm("purr009", "Yearly Purchase Analysis", "english=Yearly Purchase Analysis\ntraditional=年度採購分析\nsimplified=年度采购分析");
opt_wel_proghdrm("purr010", "Payment Projection", "english=Payment Projection\ntraditional=採購單付款預算\nsimplified=采购单付款预算");
opt_wel_proghdrm("purr011", "Payable Projection", "english=Payable Projection\ntraditional=採購應付款預算\nsimplified=采购应付款预算");
opt_wel_proghdrm("purm008", "Quotation Maintenance", "english=Quotation Maintenance\ntraditional=供應商報價維護\nsimplified=供应商报价维护");
opt_wel_proghdrm("purm026", "Payment Term File", "english=Payment Term File\ntraditional=付款方式檔案\nsimplified=付款方式档案");
opt_wel_proghdrm("purm034", "Ship Via File", "english=Ship Via File\ntraditional=運輸方式檔案\nsimplified=运输方式档案");
opt_wel_proghdrm("purm038", "Delivery-To File", "english=Delivery-To File\ntraditional=送貨地點檔案\nsimplified=送货地点档案");
opt_wel_proghdrm("purm_bas", "Basic File", "english=Basic File\ntraditional=基本檔案\nsimplified=基本档案");
opt_wel_proghdrm("purm_rpt", "Report", "english=Report\ntraditional=採購報表\nsimplified=采购报表");
opt_wel_proghdrm("strm", "Store Module", "english=Store Module\ntraditional=貨倉模塊\nsimplified=货仓模块");
opt_wel_proghdrm("strm004", "Pattern (ITO)", "english=Pattern (ITO)\ntraditional=模式(轉倉單)\nsimplified=模式(转仓单)");
opt_wel_proghdrm("strm005", "ITO Maintenance", "english=ITO Maintenance\ntraditional=轉倉單維護\nsimplified=转仓单维护");
opt_wel_proghdrm("strm006", "W/O Mat'l Issue", "english=W/O Mat'l Issue\ntraditional=工作單發料\nsimplified=工作单发料");
opt_wel_proghdrm("strm007", "Finish to Store", "english=Finish to Store\ntraditional=成品入倉\nsimplified=成品入仓");
opt_wel_proghdrm("strm009", "GRN Receipt", "english=GRN Receipt\ntraditional=收貨單進料\nsimplified=收货单进料");
opt_wel_proghdrm("strm011", "W/O Mat'l Return", "english=W/O Mat'l Return\ntraditional=工作單退料\nsimplified=工作单退料");
opt_wel_proghdrm("strm012", "Store Monthly Cut-Off", "english=Store Monthly Cut-Off\ntraditional=倉庫月末結算\nsimplified=仓库月末结算");
opt_wel_proghdrm("strm013", "Inventory Adjustment", "english=Inventory Adjustment\ntraditional=倉存調整\nsimplified=仓存调整");
opt_wel_proghdrm("strm016", "Warehouse File", "english=Warehouse File\ntraditional=倉庫檔案\nsimplified=仓库档案");
opt_wel_proghdrm("strm017", "Warehouse Flow File", "english=Warehouse Flow File\ntraditional=流程碼檔案\nsimplified=流程码档案");
opt_wel_proghdrm("strm018", "Pattern (GRN)", "english=Pattern (GRN)\ntraditional=模式(收貨單)\nsimplified=模式(收货单)");
opt_wel_proghdrm("strm019", "M/R Mat'l Issue", "english=M/R Mat'l Issue\ntraditional=要求單發料\nsimplified=要求单发料");
opt_wel_proghdrm("strm028", "ITo Mat'l Transfer", "english=ITo Mat'l Transfer\ntraditional=轉倉單轉料\nsimplified=转仓单转料");
opt_wel_proghdrm("strm047", "GRN Maintenance", "english=GRN Maintenance\ntraditional=收貨單維護\nsimplified=收货单维护");
opt_wel_proghdrm("strm048", "GRN IQC Process", "english=GRN IQC Process\ntraditional=收貨單檢驗(IQC)\nsimplified=收货单检验(IQC)");
opt_wel_proghdrm("strm050", "Vendor Return Note", "english=Vendor Return Note\ntraditional=供應商退貨單\nsimplified=供应商退货单");
opt_wel_proghdrm("strm051", "Mat'l Return to Vendor", "english=Mat'l Return to Vendor\ntraditional=退貨單退貨\nsimplified=退货单退货");
opt_wel_proghdrm("strm180", "Count Tag Generate", "english=Count Tag Generate\ntraditional=產生盤點票\nsimplified=产生盘点票");
opt_wel_proghdrm("strm186", "Count Tag Initialization", "english=Count Tag Initialization\ntraditional=盤點標籤初始化\nsimplified=盘点标签初始化");
opt_wel_proghdrm("strm187", "Physical Take Start", "english=Physical Take Start\ntraditional=年度盤點開始\nsimplified=年度盘点开始");
opt_wel_proghdrm("strm188", "Physical Take Confirm", "english=Physical Take Confirm\ntraditional=盤點票錄入系統\nsimplified=盘点票录入系统");
opt_wel_proghdrm("strm189", "Count Tag Entry", "english=Count Tag Entry\ntraditional=盤點票輸入\nsimplified=盘点票输入");
opt_wel_proghdrm("strm221", "Pattern (D/N)", "english=Pattern (D/N)\ntraditional=模式(送貨單)\nsimplified=模式(送货单)");
opt_wel_proghdrm("strm222", "Pattern (RTV)", "english=Pattern (RTV)\ntraditional=模式(退貨單)\nsimplified=模式(退货单)");
opt_wel_proghdrm("strm228", "D/N Maintenance", "english=D/N Maintenance\ntraditional=送貨單維護\nsimplified=送货单维护");
opt_wel_proghdrm("strm229", "D/N Delivery", "english=D/N Delivery\ntraditional=送貨單出貨\nsimplified=送货单出货");
opt_wel_proghdrm("strr001", "Inventory Summary", "english=Inventory Summary\ntraditional=Inventory Summary\nsimplified=Inventory Summary");
opt_wel_proghdrm("strr002", "Inventory Lot", "english=Inventory Lot\ntraditional=Inventory Lot\nsimplified=Inventory Lot");
opt_wel_proghdrm("strr003", "Closing Inventory", "english=Closing Inventory\ntraditional=Closing Inventory\nsimplified=Closing Inventory");
opt_wel_proghdrm("strr004", "Below Minimum Stock", "english=Below Minimum Stock\ntraditional=Below Minimum Stock\nsimplified=Below Minimum Stock");
opt_wel_proghdrm("strr005", "Part Movement", "english=Part Movement\ntraditional=Part Movement\nsimplified=Part Movement");
opt_wel_proghdrm("strr006", "Material Flow", "english=Material Flow\ntraditional=Material Flow\nsimplified=Material Flow");
opt_wel_proghdrm("strr007", "Count Tag Printing", "english=Count Tag Printing\ntraditional=Count Tag Printing\nsimplified=Count Tag Printing");
opt_wel_proghdrm("strr008", "Count Tag Audit List", "english=Count Tag Audit List\ntraditional=Count Tag Audit List\nsimplified=Count Tag Audit List");
opt_wel_proghdrm("strr009", "Physical Take", "english=Physical Take\ntraditional=Physical Take\nsimplified=Physical Take");
opt_wel_proghdrm("strm_adm", "Stock Management", "english=Stock Management\ntraditional=倉務管理\nsimplified=仓务管理");
opt_wel_proghdrm("strm_bas", "Basic File", "english=Basic File\ntraditional=基本檔案\nsimplified=基本档案");
opt_wel_proghdrm("strm_dn", "Good Delivery", "english=Good Delivery\ntraditional=送貨管理\nsimplified=送货管理");
opt_wel_proghdrm("strm_fgs", "Finish Good to Store", "english=Finish Good to Store\ntraditional=成品入倉\nsimplified=成品入仓");
opt_wel_proghdrm("strm_grn", "Good Receipt", "english=Good Receipt\ntraditional=收料管理\nsimplified=收料管理");
opt_wel_proghdrm("strm_iss", "Mat'l Issue", "english=Mat'l Issue\ntraditional=發料管理\nsimplified=发料管理");
opt_wel_proghdrm("strm_ito", "Internal Transfer", "english=Internal Transfer\ntraditional=轉倉管理\nsimplified=转仓管理");
opt_wel_proghdrm("strm_phy", "Physical Take", "english=Physical Take\ntraditional=年度盤點\nsimplified=年度盘点");
opt_wel_proghdrm("strm_rtv", "Mat'l Retrun", "english=Mat'l Return\ntraditional=退料管理\nsimplified=退料管理");
opt_wel_proghdrm("sysm", "System Module", "english=System Module\ntraditional=系統模塊\nsimplified=系统模块");
opt_wel_proghdrm("sysm008", "Program File", "english=Program File\ntraditional=程序碼檔案\nsimplified=程序码档案");
if (WERP_PACKAGE_TYPE=="standalone"){
	opt_wel_proghdrm("sysm001", "User File", "english=User File\ntraditional=用戶檔案\nsimplified=用户档案");
	opt_wel_proghdrm("sysm002", "Change Password", "english=Change Password\ntraditional=修改密碼\nsimplified=修改密码");
	opt_wel_proghdrm("sysm003", "Group File", "english=Group File\ntraditional=組檔案\nsimplified=组档案");
}else if (WERP_PACKAGE_TYPE=="drupal"){
	opt_wel_proghdrm("sysm007", "Group File", "english=Group File\ntraditional=組檔案\nsimplified=组档案");
}else if (WERP_PACKAGE_TYPE=="joomla"){
	opt_wel_proghdrm("sysm010", "Group File", "english=Group File\ntraditional=組檔案\nsimplified=组档案");
}
opt_wel_proghdrm("sysm009", "List of Function", "english=List of Function\ntraditional=功能清單\nsimplified=功能清单");
opt_wel_proghdrm("sysm011", "Company File", "english=Company File\ntraditional=公司檔案\nsimplified=公司档案");
opt_wel_proghdrm("sysm012", "System Settings", "english=System Settings\ntraditional=系統設置\nsimplified=系统设置");
opt_wel_proghdrm("werplogout", "Logout", "english=Logout\ntraditional=登出系統\nsimplified=登出系统");
//==========================================================================


execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_proglofm` (
  `wel_parent_code` varchar(50) NOT NULL,
  `wel_prog_code` varchar(50) NOT NULL,
  `wel_working_dir` varchar(50) DEFAULT NULL,
  `wel_ordering` int(11) DEFAULT NULL,
  PRIMARY KEY (`wel_parent_code`,`wel_prog_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

//==========================================================================
//创建菜单树
opt_wel_proglofm("root", "engm", "", 1);
opt_wel_proglofm("root", "mktm", "", 2);
opt_wel_proglofm("root", "purm", "", 3);
opt_wel_proglofm("root", "pmcm", "", 4);
opt_wel_proglofm("root", "strm", "", 5);
opt_wel_proglofm("root", "sysm", "", 6);
opt_wel_proglofm("root", "werplogout", "ext-3.2.0", 7);
opt_wel_proglofm("engm", "engm002", "engm", 1);
opt_wel_proglofm("engm", "engm003", "engm", 2);
opt_wel_proglofm("engm", "engm013", "engm", 3);
opt_wel_proglofm("engm", "engm016", "engm", 4);
opt_wel_proglofm("engm", "engm_bas", "", 5);
opt_wel_proglofm("engm_bas", "engm017", "engm", 1);
opt_wel_proglofm("engm_bas", "engm903", "engm", 2);
opt_wel_proglofm("engm_bas", "engm022", "engm", 3);
opt_wel_proglofm("engm_bas", "mktm034", "mktm", 4);
opt_wel_proglofm("engm_bas", "engm012", "engm", 5);
opt_wel_proglofm("engm", "engm_rpt", "", 6);
opt_wel_proglofm("engm_rpt", "engr002", "engm", 1);
opt_wel_proglofm("engm_rpt", "engr003", "engm", 2);
opt_wel_proglofm("engm_rpt", "engr004", "engm", 3);
opt_wel_proglofm("engm_rpt", "engr013", "engm", 4);
opt_wel_proglofm("engm_rpt", "engr014", "engm", 5);
opt_wel_proglofm("mktm", "mktm002", "mktm", 1);
opt_wel_proglofm("mktm", "mktm009", "mktm", 2);
opt_wel_proglofm("mktm", "mktm038", "mktm", 3);
opt_wel_proglofm("mktm", "mktm_bas", "", 4);
opt_wel_proglofm("mktm_bas", "mktm001", "mktm", 1);
opt_wel_proglofm("mktm_bas", "mktm003", "mktm", 2);
opt_wel_proglofm("mktm_bas", "mktm025", "mktm", 3);
opt_wel_proglofm("mktm_bas", "mktm034", "mktm", 4);
opt_wel_proglofm("mktm_bas", "mktm042", "mktm", 5);
opt_wel_proglofm("mktm_bas", "mktm035", "mktm", 6);
opt_wel_proglofm("mktm_bas", "mktm128", "mktm", 7);
opt_wel_proglofm("mktm_bas", "mktm005", "mktm", 8);
opt_wel_proglofm("mktm_bas", "mktm006", "mktm", 9);
opt_wel_proglofm("pmcm", "pmcm006", "pmcm", 1);
opt_wel_proglofm("pmcm", "pmcm007", "pmcm", 2);
opt_wel_proglofm("pmcm", "pmcm008", "pmcm", 3);
opt_wel_proglofm("pmcm", "pmcr001", "pmcm", 4);
opt_wel_proglofm("pmcm", "pmcr002", "pmcm", 5);
opt_wel_proglofm("pmcm", "pmcr003", "pmcm", 6);
opt_wel_proglofm("pmcm", "pmcr004", "pmcm", 7);
opt_wel_proglofm("pmcm", "pmcr005", "pmcm", 8);
opt_wel_proglofm("pmcm", "pmcr006", "pmcm", 9);
opt_wel_proglofm("pmcm", "pmcr008", "pmcm", 10);
opt_wel_proglofm("pmcm", "pmcr009", "pmcm", 11);
opt_wel_proglofm("pmcm", "pmcr010", "pmcm", 12);
opt_wel_proglofm("pmcm", "pmcm_bas", "", 13);
opt_wel_proglofm("pmcm_bas", "pmcm001", "pmcm", 1);
opt_wel_proglofm("pmcm_bas", "pmcm002", "pmcm", 2);
opt_wel_proglofm("pmcm_bas", "pmcm003", "pmcm", 3);
opt_wel_proglofm("pmcm_bas", "pmcm004", "pmcm", 4);
opt_wel_proglofm("purm", "purm002", "purm", 1);
opt_wel_proglofm("purm", "purm008", "purm", 2);
opt_wel_proglofm("purm", "purm005", "purm", 3);
opt_wel_proglofm("purm", "purr005", "purm", 4);
opt_wel_proglofm("purm", "purm_bas", "", 5);
opt_wel_proglofm("purm_bas", "purm001", "purm", 1);
opt_wel_proglofm("purm_bas", "purm038", "purm", 2);
opt_wel_proglofm("purm_bas", "purm026", "purm", 3);
opt_wel_proglofm("purm_bas", "mktm035", "mktm", 4);
opt_wel_proglofm("purm_bas", "purm034", "purm", 5);
opt_wel_proglofm("purm_bas", "purm003", "purm", 6);
opt_wel_proglofm("purm_bas", "purm004", "purm", 7);
opt_wel_proglofm("purm", "purm_rpt", "", 6);
opt_wel_proglofm("purm_rpt", "purr003", "purm", 1);
opt_wel_proglofm("purm_rpt", "purr004", "purm", 2);
opt_wel_proglofm("purm_rpt", "purr006", "purm", 3);
opt_wel_proglofm("purm_rpt", "purr007", "purm", 4);
opt_wel_proglofm("purm_rpt", "purr008", "purm", 5);
opt_wel_proglofm("purm_rpt", "purr009", "purm", 6);
opt_wel_proglofm("purm_rpt", "purr010", "purm", 7);
opt_wel_proglofm("purm_rpt", "purr011", "purm", 8);
opt_wel_proglofm("strm", "strm_grn", "strm", 1);
opt_wel_proglofm("strm", "strm_ito", "strm", 2);
opt_wel_proglofm("strm", "strm_iss", "strm", 3);
opt_wel_proglofm("strm", "strm_fgs", "strm", 4);
opt_wel_proglofm("strm", "strm_rtv", "strm", 5);
opt_wel_proglofm("strm", "strm_dn", "strm", 6);
opt_wel_proglofm("strm", "strm_adm", "strm", 7);
opt_wel_proglofm("strm", "strm_phy", "strm", 8);
opt_wel_proglofm("strm", "strm_bas", "strm", 9);
opt_wel_proglofm("strm", "strr001", "strm", 10);
opt_wel_proglofm("strm", "strr002", "strm", 11);
opt_wel_proglofm("strm", "strr003", "strm", 12);
opt_wel_proglofm("strm", "strr004", "strm", 13);
opt_wel_proglofm("strm", "strr005", "strm", 14);
opt_wel_proglofm("strm", "strr006", "strm", 15);
opt_wel_proglofm("strm", "strr007", "strm", 16);
opt_wel_proglofm("strm", "strr008", "strm", 17);
opt_wel_proglofm("strm", "strr009", "strm", 18);
opt_wel_proglofm("strm_grn", "strm047", "strm", 1);
opt_wel_proglofm("strm_grn", "strm048", "strm", 2);
opt_wel_proglofm("strm_grn", "strm009", "strm", 3);
opt_wel_proglofm("strm_ito", "strm005", "strm", 1);
opt_wel_proglofm("strm_ito", "strm028", "strm", 2);
opt_wel_proglofm("strm_iss", "strm006", "strm", 1);
opt_wel_proglofm("strm_iss", "strm019", "strm", 2);
opt_wel_proglofm("strm_fgs", "strm007", "strm", 1);
opt_wel_proglofm("strm_rtv", "strm011", "strm", 1);
opt_wel_proglofm("strm_rtv", "strm050", "strm", 2);
opt_wel_proglofm("strm_rtv", "strm051", "strm", 3);
opt_wel_proglofm("strm_dn", "strm228", "strm", 1);
opt_wel_proglofm("strm_dn", "strm229", "strm", 2);
opt_wel_proglofm("strm_adm", "strm013", "strm", 1);
opt_wel_proglofm("strm_adm", "strm012", "strm", 2);
opt_wel_proglofm("strm_phy", "strm186", "strm", 1);
opt_wel_proglofm("strm_phy", "strm180", "strm", 2);
opt_wel_proglofm("strm_phy", "strm187", "strm", 3);
opt_wel_proglofm("strm_phy", "strm189", "strm", 4);
opt_wel_proglofm("strm_phy", "strm188", "strm", 5);
opt_wel_proglofm("strm_bas", "strm016", "strm", 1);
opt_wel_proglofm("strm_bas", "strm017", "strm", 2);
opt_wel_proglofm("strm_bas", "strm018", "strm", 3);
opt_wel_proglofm("strm_bas", "strm004", "strm", 4);
opt_wel_proglofm("strm_bas", "strm221", "strm", 5);
opt_wel_proglofm("strm_bas", "strm222", "strm", 6);
opt_wel_proglofm("sysm", "sysm008", "sysm", 1);
if (WERP_PACKAGE_TYPE=="standalone"){
	opt_wel_proglofm("sysm", "sysm001", "sysm", 2);
	opt_wel_proglofm("sysm", "sysm002", "sysm", 3);
	opt_wel_proglofm("sysm", "sysm003", "sysm", 4);
}else if (WERP_PACKAGE_TYPE=="drupal"){
	opt_wel_proglofm("sysm", "sysm007", "sysm", 5);
}else if (WERP_PACKAGE_TYPE=="joomla"){
	opt_wel_proglofm("sysm", "sysm010", "sysm", 6);
}
opt_wel_proglofm("sysm", "sysm009", "sysm", 7);
opt_wel_proglofm("sysm", "sysm011", "sysm", 8);
opt_wel_proglofm("sysm", "sysm012", "sysm", 9);
//==========================================================================

//modify #__wel_engbomh
if (col_name_not_exists("#__wel_engbomh","wel_pre_scp_fact")){
	execute_sql("
	ALTER TABLE `#__wel_engbomh` 
	ADD `wel_pre_scp_fact` DECIMAL( 20, 6 ) NULL DEFAULT NULL ;
	");
}

if (col_name_not_exists("#__wel_engbomh","wel_pre_unit_exg")){
	execute_sql("
	ALTER TABLE `#__wel_engbomh` 
	ADD `wel_pre_unit_exg` DECIMAL( 20, 6 ) NULL DEFAULT NULL ;
	");
}

if (col_name_not_exists("#__wel_engbomh","wel_pre_qty_per")){
	execute_sql("
	ALTER TABLE `#__wel_engbomh` 
	ADD `wel_pre_qty_per` DECIMAL( 20, 6 ) NULL DEFAULT NULL ;
	");
}

//modify #__wel_worhdrm
if (col_name_not_exists("#__wel_worhdrm","wel_reduce_mo")){
	execute_sql("
	ALTER TABLE `#__wel_worhdrm` 
	ADD `wel_reduce_mo` TINYINT NULL ;
	");
}

if (!col_name_not_exists("#__wel_worhdrm","wel_stat_date")){
	execute_sql("
	ALTER TABLE `#__wel_worhdrm` 
	CHANGE `wel_stat_date` `wel_start_date` DATE NULL DEFAULT NULL ;
	");
}

if (!col_name_not_exists("#__wel_worhdrm","wel_close_yn")){
	execute_sql("
	ALTER TABLE `#__wel_worhdrm` 
	CHANGE `wel_close_yn` `wel_settle_yn` TINYINT( 1 ) NULL DEFAULT NULL ;
	");
}

if (!col_name_not_exists("#__wel_worhdrm","wel_close_by")){
	execute_sql("
	ALTER TABLE `#__wel_worhdrm` 
	CHANGE `wel_close_by` `wel_settle_by` VARCHAR( 30 ) NULL DEFAULT NULL ;
	");
}

if (!col_name_not_exists("#__wel_worhdrm","wel_close_date")){
	execute_sql("
	ALTER TABLE `#__wel_worhdrm` 
	CHANGE `wel_close_date` `wel_settle_date` DATETIME NULL DEFAULT NULL ;
	");
}

//modify #__wel_compflm by weiwei 20101031
if (!col_name_not_exists("#__wel_compflm","wel_comp_cdes")){
	execute_sql("
	ALTER TABLE `#__wel_compflm` 
	CHANGE `wel_comp_cdes` `wel_comp_des1` VARCHAR( 100 ) 
	CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ;
	");
}

if (!col_name_not_exists("#__wel_compflm","wel_comp_telx")){
	execute_sql("
	ALTER TABLE `#__wel_compflm` 
	CHANGE `wel_comp_telx` `wel_comp_email` VARCHAR( 50 ) 
	CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ;
	");
}

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_syssets` (
  `wel_sets_code` varchar(50) NOT NULL,
  `wel_comp_des` varchar(100) DEFAULT NULL,
  `wel_comp_des1` varchar(100) DEFAULT NULL,
  `wel_comp_add1` varchar(100) DEFAULT NULL,
  `wel_comp_add2` varchar(100) DEFAULT NULL,
  `wel_comp_add3` varchar(100) DEFAULT NULL,
  `wel_comp_add4` varchar(100) DEFAULT NULL,
  `wel_comp_cont` varchar(50) DEFAULT NULL,
  `wel_comp_tele` varchar(50) DEFAULT NULL,
  `wel_comp_fax` varchar(50) DEFAULT NULL,
  `wel_comp_email` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`wel_sets_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
");

execute_sql("
CREATE TABLE IF NOT EXISTS `#__wel_wrcache` (
  `wel_session_id` varchar(100) NOT NULL,
  `wel_cache_id` varchar(60) NOT NULL,
  `wel_crt_date` datetime DEFAULT NULL,
  `wel_cache_expire` int(11) DEFAULT NULL,
  `wel_cache_content` mediumblob,
  PRIMARY KEY (`wel_session_id`,`wel_cache_id`),
  KEY `wel_session_id` (`wel_session_id`,`wel_cache_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");

//show execute error message
$execute_message=execute_sql("",true);
if (is_array($execute_message)){
	foreach ($execute_message as $key=>$message){
		echo '<div style="color:blue;">'.$message["error_sql"].'</div>';
		echo '<div style="color:red;"><B>'.$message["error_description"].'</B></div>';
	}
}

?>