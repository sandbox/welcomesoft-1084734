<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>
<?php
html_heading();
eval(rebuild_layout(__FILE__));
//Draw Body Layout
?>

<script language="javascript">
//var hidden_object_id_list;	//隐藏对象的列�?
//var object_id_list;	//�?有对象的列表
//权限基本只有 read addnew edit delete approve print 六种
//var access_read;		//读取权限
//var access_addnew;	//新增权限
//var access_edit;		//编辑权限
//var access_delete;	//删除权限
//var access_approve;	//批核权限
//var access_print;		//打印权限
//以上js变量无需设定，已经由类库自动产生，直接使用即�?

//opt_action操作状�??
//外部要求的操�?
var external_opt_action="";
var action_page="";
var wel_cus_code="";
//�?直处于暗淡的对象列表(无法编辑的对�?)
var dim_object_id_list="";
//要用权限控制的按钮列�?
var security_button="";

$(document).ready(function()
{
	external_opt_action="<?php echo werp_get_request_var(
"opt_action"); ?>";
	action_page="<?php echo werp_pathinfo_filename(__FILE__); ?>_opt.php";
	wel_cus_code="<?php echo werp_get_request_var("txt_wel_cus_code"); ?>";
	//�?直处于暗淡的对象列表(无法编辑的对�?)
	dim_object_id_list="txt_wel_cus_term_des|txt_wel_cus_pbase_des|txt_wel_cus_svia_des|chk_wel_ams_yn|ntxt_wel_cr_day";//readonly对象
	//要用权限控制的按钮列�?
	security_button="btn_head_addnew|btn_head_edit|btn_head_del|btn_head_next|btn_head_save|btn_head_cancel|"+
		"btn_wel_cuscont_addnew|btn_wel_cuscont_edit|btn_wel_cuscont_del|btn_wel_cuscont_del_all|btn_wel_delyflm_addnew|btn_wel_delyflm_edit|btn_wel_delyflm_del|btn_wel_delyflm_del_all";

});

$(document).ready(function()
{
	wel_cuscont_sql_grid(document.getElementById("txt_wel_cus_code").value);
	wel_delyflm_sql_grid(document.getElementById("txt_wel_cus_code").value);

});

function return_handler_info(return_message)
{
	//window.alert(return_message);	//显示�?有信息供测试时查�?
	if (script_timeout(return_message)){return false};
	
	var ret_msg_arr=return_message.split("|");
	if (ret_msg_arr.length<5){window.alert(return_message);return false;}
	for (var i=0;i<ret_msg_arr.length;i++){ret_msg_arr[i]=un_coding_str(ret_msg_arr[i]);}
	var opt_action=ret_msg_arr[1];
	var msg_code=ret_msg_arr[2];
	var msg_detail=ret_msg_arr[3];
	var msg_script=ret_msg_arr[4];
	switch(opt_action)
	{
		case "lbtn_wel_cus_code_load_click":
			if (msg_detail!==""){window.alert(msg_detail);}
			eval(msg_script);//执行返回后产生的脚本
			if (msg_code=="")
			{
				enable_object("txt_wel_cus_term",true,"");
				enable_object("txt_wel_cus_pbase",true,"");
				enable_object("txt_wel_cus_svia",true,"");
				$("#txt_wel_cus_term").blur();
				$("#txt_wel_cus_pbase").blur();
				$("#txt_wel_cus_svia").blur();
				enable_object(object_id_list,false,"");
				enable_object("btn_head_edit|btn_head_del|btn_head_next|btn_wel_cuscont_addnew|btn_wel_cuscont_edit|btn_wel_cuscont_del|btn_wel_cuscont_del_all|btn_wel_delyflm_addnew|btn_wel_delyflm_edit|btn_wel_delyflm_del|btn_wel_delyflm_del_all",true,access_addnew+"|"+access_edit+"|"+access_delete+"|"+access_read+"|"+access_addnew+"|"+access_edit+"|"+access_delete+"|"+access_read+"|"+access_addnew+"|"+access_edit+"|"+access_delete+"|"+access_read);
			}
			format_number_el("ntxt_wel_ex_rate");
			format_number_el("ntxt_wel_cr_lmt");
			format_number_el("ntxt_wel_cr_day");
//			format_number_el("ntxt_wel_os_inv");
			break;
		
		case "addnew":
		case "edit":
		case "btn_head_del_click":
		case "btn_wel_cuscont_del_click":
		case "btn_wel_cuscont_del_all_click":
		case "btn_wel_delyflm_del_click":
		case "btn_wel_delyflm_del_all_click":
			if (msg_detail!==""){window.alert(msg_detail);}
			eval(msg_script);//执行返回后产生的脚本
			break;

		default:
			if (msg_detail!==""){window.alert(msg_detail);}eval(msg_script);
			break;
	}
}

function lbtn_wel_cus_code_load_click()
{
	if (document.getElementById("txt_wel_cus_code").value.trim()==""){return;}
	var url=get_url_parameter(action_page,"lbtn_wel_cus_code_load_click",object_id_list);
	var handler=new net.content_loader(url,return_handler_info);
}
$(document).ready(function(){bind_event("lbtn_wel_cus_code_load","click",lbtn_wel_cus_code_load_click);});

function btn_head_addnew_click()
{
	wel_cus_code="";
	wel_cuscont_sql_grid("");
	wel_delyflm_sql_grid("");

	clear_screen_layout(object_id_list);
	enable_object(object_id_list,true,"");
	enable_object(dim_object_id_list,false,"");
	enable_object(security_button,false,"");
	enable_object("btn_head_save|btn_head_cancel",true,
				access_addnew+"|"+access_read);
	format_number_el("ntxt_wel_ex_rate");
	format_number_el("ntxt_wel_cr_lmt");
	format_number_el("ntxt_wel_cr_day");
//	format_number_el("ntxt_wel_os_inv");
}
$(document).ready(function(){bind_event("btn_head_addnew","click",btn_head_addnew_click);});

function btn_head_edit_click()
{
	enable_object(object_id_list,true,"");
	enable_object(dim_object_id_list,false,"");
	enable_object(security_button,false,"");
	enable_object("txt_wel_cus_code|lbtn_wel_cus_code_load|bbtn_wel_cus_code",false,"");
	enable_object("btn_head_save|btn_head_cancel",true,
				access_edit+"|"+access_read);
}
$(document).ready(function(){bind_event("btn_head_edit","click",btn_head_edit_click);});

function btn_head_del_click()
{
	if (!window.confirm("Delete this Record?")){return;}
	var url=get_url_parameter(action_page,"btn_head_del_click",object_id_list);
	var handler=new net.content_loader(url,return_handler_info);
}
$(document).ready(function(){bind_event("btn_head_del","click",btn_head_del_click);});

function btn_head_next_click()
{
	wel_cus_code="";
	wel_cuscont_sql_grid("");
	wel_delyflm_sql_grid("");

	clear_screen_layout(object_id_list);
	enable_object(object_id_list,false,"");
	enable_object("txt_wel_cus_code|lbtn_wel_cus_code_load|bbtn_wel_cus_code",true,"");
	enable_object(security_button,false,"");
	enable_object("btn_head_addnew",true,access_addnew);
}
$(document).ready(function(){bind_event("btn_head_next","click",btn_head_next_click);});

function btn_head_save_click()
{
	if(wel_cus_code==""){opt_action="addnew";}else{opt_action="edit";}
	var url=get_url_parameter(action_page,opt_action,object_id_list);
	var handler=new net.content_loader(url,return_handler_info);
}
$(document).ready(function(){bind_event("btn_head_save","click",btn_head_save_click);});

function btn_head_cancel_click()
{
	if (wel_cus_code!="")
	{
		lbtn_wel_cus_code_load_click();	//重新加载�?次数据，让当前更改的内容失效
	}
	else
	{
		btn_head_next_click()
//		enable_object(object_id_list,true,"");
//		enable_object(dim_object_id_list,false,"");
//		enable_object(security_button,false,"");
//		enable_object("btn_head_addnew|btn_head_save|btn_head_cancel",true,
//			access_addnew+"|"+access_addnew+"|"+access_read);
	}
}
$(document).ready(function(){bind_event("btn_head_cancel","click",btn_head_cancel_click);});

function btn_wel_cuscont_addnew_click()
{
	var new_object_id_list=object_id_list;
	new_object_id_list=remove_object_id(new_object_id_list,"wel_cus_add1");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_cus_add2");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_cus_add3");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_cus_add4");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_cus_term");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_cus_pbase");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_cur_code");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_ex_rate");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_principal");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_sale_no");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_tax_no");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_tax_type");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_bank_account");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_acc_ac");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_cr_lmt");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_cr_day");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_cr_hold");
//	new_object_id_list=remove_object_id(new_object_id_list,"wel_os_inv");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_ams_yn");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_checkok");
	var url=get_url_parameter("mktm002a.php","btn_wel_cuscont_addnew_click",new_object_id_list);
	document.location.replace(url);
}
$(document).ready(function(){bind_event("btn_wel_cuscont_addnew","click",btn_wel_cuscont_addnew_click);});

function btn_wel_cuscont_edit_click()
{
	var rt=wel_cuscont_sql_selected_col(0);if (!rt[0]){return false;}
	var new_object_id_list=object_id_list;
	new_object_id_list=remove_object_id(new_object_id_list,"wel_cus_add1");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_cus_add2");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_cus_add3");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_cus_add4");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_cus_term");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_cus_pbase");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_cur_code");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_ex_rate");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_principal");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_sale_no");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_tax_no");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_tax_type");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_bank_account");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_acc_ac");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_cr_lmt");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_cr_day");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_cr_hold");
//	new_object_id_list=remove_object_id(new_object_id_list,"wel_os_inv");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_ams_yn");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_checkok");
	var url=get_url_parameter("mktm002a.php","btn_wel_cuscont_edit_click",new_object_id_list);
	url=url+"&ntxt_wel_cont_line="+url_escape(rt[1]);
	document.location.replace(url);
}
$(document).ready(function(){bind_event("btn_wel_cuscont_edit","click",btn_wel_cuscont_edit_click);});

function btn_wel_cuscont_del_click()
{
	var rt=wel_cuscont_sql_selected_col(0);if (!rt[0]){return false;}
	if (!window.confirm("Delete data line ["+rt[1]+"] ?")){return;}
	var url=get_url_parameter(action_page,"btn_wel_cuscont_del_click",object_id_list);
	url=url+"&ntxt_wel_cont_line="+url_escape(rt[1]);
	var handler=new net.content_loader(url,return_handler_info);
}
$(document).ready(function(){bind_event("btn_wel_cuscont_del","click",btn_wel_cuscont_del_click);});

function btn_wel_cuscont_del_all_click()
{
	if (!window.confirm("Delete all data Record?")){return;}
	var url=get_url_parameter(action_page,"btn_wel_cuscont_del_all_click",object_id_list);
	var handler=new net.content_loader(url,return_handler_info);
}
$(document).ready(function(){bind_event("btn_wel_cuscont_del_all","click",btn_wel_cuscont_del_all_click);});

function btn_wel_delyflm_addnew_click()
{
	var new_object_id_list=object_id_list;
	new_object_id_list=remove_object_id(new_object_id_list,"wel_cus_add1");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_cus_add2");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_cus_add3");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_cus_add4");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_cus_term");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_cus_pbase");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_cur_code");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_ex_rate");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_principal");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_sale_no");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_tax_no");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_tax_type");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_bank_account");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_acc_ac");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_cr_lmt");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_cr_day");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_cr_hold");
//	new_object_id_list=remove_object_id(new_object_id_list,"wel_os_inv");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_ams_yn");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_checkok");
	var url=get_url_parameter("mktm002b.php","btn_wel_delyflm_addnew_click",new_object_id_list);
	document.location.replace(url);
}
$(document).ready(function(){bind_event("btn_wel_delyflm_addnew","click",btn_wel_delyflm_addnew_click);});

function btn_wel_delyflm_edit_click()
{
	var rt=wel_delyflm_sql_selected_col(0);if (!rt[0]){return false;}
	var new_object_id_list=object_id_list;
	new_object_id_list=remove_object_id(new_object_id_list,"wel_cus_add1");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_cus_add2");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_cus_add3");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_cus_add4");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_cus_term");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_cus_pbase");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_cur_code");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_ex_rate");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_principal");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_sale_no");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_tax_no");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_tax_type");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_bank_account");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_acc_ac");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_cr_lmt");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_cr_day");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_cr_hold");
//	new_object_id_list=remove_object_id(new_object_id_list,"wel_os_inv");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_ams_yn");
	new_object_id_list=remove_object_id(new_object_id_list,"wel_checkok");
	var url=get_url_parameter("mktm002b.php","btn_wel_delyflm_edit_click",new_object_id_list);
	url=url+"&ntxt_wel_dely_line="+url_escape(rt[1]);
	document.location.replace(url);
}
$(document).ready(function(){bind_event("btn_wel_delyflm_edit","click",btn_wel_delyflm_edit_click);});

function btn_wel_delyflm_del_click()
{
	var rt=wel_delyflm_sql_selected_col(0);if (!rt[0]){return false;}
	if (!window.confirm("Delete data line ["+rt[1]+"] ?")){return;}
	var url=get_url_parameter(action_page,"btn_wel_delyflm_del_click",object_id_list);
	url=url+"&ntxt_wel_dely_line="+url_escape(rt[1]);
	var handler=new net.content_loader(url,return_handler_info);
}
$(document).ready(function(){bind_event("btn_wel_delyflm_del","click",btn_wel_delyflm_del_click);});

function btn_wel_delyflm_del_all_click()
{
	if (!window.confirm("Delete all data Record?")){return;}
	var url=get_url_parameter(action_page,"btn_wel_delyflm_del_all_click",object_id_list);
	var handler=new net.content_loader(url,return_handler_info);
}
$(document).ready(function(){bind_event("btn_wel_delyflm_del_all","click",btn_wel_delyflm_del_all_click);});

$(document).ready(function()
{
	if (external_opt_action=="")
	{
		btn_head_next_click();
//		btn_head_addnew_click();
	}

	if ((external_opt_action=="btn_wel_cuscont_addnew_click") || 
		(external_opt_action=="btn_wel_cuscont_edit_click"))
	{
		document.getElementById("txt_wel_cus_code").value=wel_cus_code;
		lbtn_wel_cus_code_load_click();
		$("#detail_tab_list > ul").tabs({selected: 0});
	}
	
	if ((external_opt_action=="btn_wel_delyflm_addnew_click") || 
		(external_opt_action=="btn_wel_delyflm_edit_click"))
	{
		document.getElementById("txt_wel_cus_code").value=wel_cus_code;
		lbtn_wel_cus_code_load_click();
		$("#detail_tab_list > ul").tabs({selected: 1});
	}
});
</script>
<?php
html_footer();
?>