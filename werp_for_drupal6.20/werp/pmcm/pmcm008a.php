<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>
<?php
html_heading();
eval(rebuild_layout(__FILE__));
//Draw Body Layout
?>

<script language="javascript">
//var hidden_object_id_list;	//隐藏对象的列表
//var object_id_list;	//所有对象的列表
//权限基本只有 read addnew edit delete approve print 六种
//var access_read;		//读取权限
//var access_addnew;	//新增权限
//var access_edit;		//编辑权限
//var access_delete;	//删除权限
//var access_approve;	//批核权限
//var access_print;		//打印权限
//以上js变量无需设定，已经由类库自动产生，直接使用即可

//opt_action操作状态
//外部要求的操作
var external_opt_action="";
var action_page="";
var wel_mr_no="";
var wel_mr_line="";
//一直处于暗淡的对象列表(无法编辑的对象)
var dim_object_id_list="";
//要用权限控制的按钮列表
var security_button="";

$(document).ready(function()
{
	external_opt_action="<?php echo werp_get_request_var("opt_action"); ?>";
	action_page="<?php echo werp_pathinfo_filename(__FILE__); ?>_opt.php";
	wel_mr_no="<?php echo werp_get_request_var("txt_wel_mr_no"); ?>";
	wel_mr_line="<?php echo werp_get_request_var("ntxt_wel_mr_line"); ?>";
	//一直处于暗淡的对象列表(无法编辑的对象)
	dim_object_id_list="txt_wel_mr_no|ntxt_wel_mr_line|txt_wel_part_des|txt_wel_unit";
	//要用权限控制的按钮列表
	var security_button="btn_save";
});

function return_handler_info(return_message)
{
	//window.alert(return_message);	//显示所有信息供测试时查看
	var ret_msg_arr=return_message.split("|");
	if (ret_msg_arr.length<5){window.alert(return_message);return false;}
	for (var i=0;i<ret_msg_arr.length;i++){ret_msg_arr[i]=un_coding_str(ret_msg_arr[i]);}
	var opt_action=ret_msg_arr[1];
	var msg_code=ret_msg_arr[2];
	var msg_detail=ret_msg_arr[3];
	var msg_script=ret_msg_arr[4];
	switch(opt_action)
	{
		case "addnew":
		case "edit":
			if (msg_detail!==""){window.alert(msg_detail);}
			eval(msg_script);//执行返回后产生的脚本
			break;
			
		case "load_wel_mrdetfm":
			if (msg_detail!==""){window.alert(msg_detail);}
			eval(msg_script);//执行返回后产生的脚本
			break;
			
		default:
			if (msg_detail!==""){window.alert(msg_detail);}eval(msg_script);
			break;
	}
}

function btn_save_click()
{
	if (external_opt_action=="btn_detail_tab0_addnew_click")
	{
		opt_action="addnew";
	}
	else if (external_opt_action=="btn_detail_tab0_edit_click")
	{
		opt_action="edit";
	}
	
	var url=get_url_parameter(action_page,opt_action,object_id_list);
	var handler=new net.content_loader(url,return_handler_info);
}
$(document).ready(function(){bind_event("btn_save","click",btn_save_click);});

function btn_return_click()
{
	opt_action=external_opt_action;
	var url=get_url_parameter("pmcm008.php",opt_action,object_id_list);
	document.location.replace(url);
}
$(document).ready(function(){bind_event("btn_return","click",btn_return_click);});

function addnew_init()
{
	clear_screen_layout(object_id_list);
	format_number_el("ntxt_wel_mr_line");
	format_number_el("ntxt_wel_req_qty");
	enable_object(dim_object_id_list,false,"");
	enable_object(security_button,false,"");
	enable_object("btn_save",true,access_addnew);
	document.getElementById("txt_wel_mr_no").value=wel_mr_no;
}
function edit_init()
{
	clear_screen_layout(object_id_list);
	format_number_el("ntxt_wel_mr_line");
	format_number_el("ntxt_wel_req_qty");
	enable_object(dim_object_id_list,false,"");
	enable_object(security_button,false,"");
	enable_object("txt_wel_part_no|bbtn_wel_part_no",false,"");
	enable_object("btn_save",true,access_edit);
	document.getElementById("txt_wel_mr_no").value=wel_mr_no;
	document.getElementById("ntxt_wel_mr_line").value=wel_mr_line;
}
function load_wel_mrdetfm()
{
	var url=get_url_parameter(action_page,"load_wel_mrdetfm",object_id_list);
	var handler=new net.content_loader(url,return_handler_info);
}

$(document).ready(function()
{
	if (external_opt_action=="btn_detail_tab0_addnew_click")
	{
		addnew_init();
	}
	if (external_opt_action=="btn_detail_tab0_edit_click")
	{
		edit_init();
		load_wel_mrdetfm();
	}
});

</script>
<?php
html_footer();
?>