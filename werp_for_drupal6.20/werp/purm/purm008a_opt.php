<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>
<?php
eval(receipt_url_parameter($_GET,$_POST));	//将参数的值放入相应以参数为名的变量中
//echo receipt_url_parameter($_GET,$_POST);
eval(read_cache_from_wel_wrcache($main_page."msg.inc"));
require_once(WERP_SITE_PATH_CLASS.$main_page."_cls.php");

//$opt_action,$msg_code,$msg_detail,$msg_script 为标准定义，用coding_str编码后以
//"|".$opt_action."|".$msg_code."|".$msg_detail."|".$msg_script  格式作为信息返回给回调函数
$opt_action=strtolower($opt_action);	//调用者的动作
$msg_code="";	//调用后产生的信息代码
$msg_detail="";	//信息代码对应的细节内容
$msg_script="";	//要执行的脚本

$cls_purm008a=new purm008a_cls();
$cls_purm008a->wel_quot_no=$txt_wel_quot_no;
$cls_purm008a->wel_quot_line=$ntxt_wel_quot_line;	
$cls_purm008a->wel_ven_code=$txt_wel_ven_code;	
$cls_purm008a->wel_quot_qty=$ntxt_wel_quot_qty;
$cls_purm008a->wel_part_no=$txt_wel_part_no;
$cls_purm008a->wel_part_des=$txt_wel_part_des;
$cls_purm008a->wel_u_price=$ntxt_wel_u_price;
$cls_purm008a->wel_prmy_quot=$chk_wel_prmy_quot;
$cls_purm008a->wel_unit_code=$txt_wel_unit_code;
$cls_purm008a->wel_lead_tm=$ntxt_wel_lead_tm;
$cls_purm008a->wel_pur_unit=$txt_wel_pur_unit;
$cls_purm008a->wel_pur_unit_rate=$ntxt_wel_pur_unit_rate;
$cls_purm008a->wel_pkg_ord=$ntxt_wel_pkg_ord;
$cls_purm008a->wel_min_ord=$ntxt_wel_min_ord;
$cls_purm008a->wel_ven_part=$txt_wel_ven_part;
$cls_purm008a->wel_ven_part_des=$txt_wel_ven_part_des;

switch ($opt_action)
{
	case "load_wel_venqdtm":	
		$return_val=$cls_purm008a->read();
		$msg_code=$return_val["msg_code"];
		$msg_detail=extract_message($msg_code);
		if ($msg_code=="")
		{
			$msg_script="clear_screen_layout(object_id_list);\n".
			"document.getElementById('txt_wel_quot_no').value='".format_slashes($return_val["wel_quot_no"])."';\n".
			"document.getElementById('ntxt_wel_quot_line').value='".format_slashes($return_val["wel_quot_line"])."';\n".
			"document.getElementById('txt_wel_ven_code').value='".format_slashes($return_val["wel_ven_code"])."';\n".
			"document.getElementById('ntxt_wel_quot_qty').value='".format_slashes($return_val["wel_quot_qty"])."';\n".
			"document.getElementById('txt_wel_part_no').value='".format_slashes($return_val["wel_part_no"])."';\n".
			"document.getElementById('txt_wel_part_des').value='".format_slashes($return_val["wel_part_des"])."';\n".
			"document.getElementById('ntxt_wel_u_price').value='".format_slashes($return_val["wel_u_price"])."';\n".
			"document.getElementById('chk_wel_prmy_quot').checked=to_boolean('".$return_val["wel_prmy_quot"]."',false);\n".
			"document.getElementById('txt_wel_unit_code').value='".format_slashes($return_val["wel_unit_code"])."';\n".
			"document.getElementById('ntxt_wel_lead_tm').value='".format_slashes($return_val["wel_lead_tm"])."';\n".
			//"document.getElementById('ntxt_wel_dis_per').value='".format_slashes($row["wel_dis_per"])."';\n".
			"document.getElementById('txt_wel_pur_unit').value='".format_slashes($return_val["wel_pur_unit"])."';\n".
			"document.getElementById('ntxt_wel_pur_unit_rate').value='".format_slashes($return_val["wel_pur_unit_rate"])."';\n".
			"document.getElementById('ntxt_wel_pkg_ord').value='".format_slashes($return_val["wel_pkg_ord"])."';\n".
			"document.getElementById('ntxt_wel_min_ord').value='".format_slashes($return_val["wel_min_ord"])."';\n".
			"document.getElementById('txt_wel_ven_part').value='".format_slashes($return_val["wel_ven_part"])."';\n".
			"document.getElementById('txt_wel_ven_part_des').value='".format_slashes($return_val["wel_ven_part_des"])."';\n";
		}
		break;
		
	case "addnew":
		$return_val=$cls_purm008a->addnew();
		$msg_code=$return_val["msg_code"];
		$msg_detail=extract_message($msg_code);
		if ($msg_code=="addnew_succee")
		{
			$msg_script="wel_quot_no='".format_slashes($txt_wel_quot_no)."';".
						"document.getElementById('txt_wel_quot_no').value='".format_slashes($txt_wel_quot_no)."';\n".
						"document.getElementById('ntxt_wel_quot_qty').value='".format_slashes($ntxt_wel_quot_qty)."';\n".
						"addnew_init();\n";
		}
		break;		
			
	case "edit":
		$return_val=$cls_purm008a->edit();
		$msg_code=$return_val["msg_code"];
		$msg_detail=extract_message($msg_code);
		if ($msg_code=="edit_succee")
		{
			$msg_script="btn_return_click();\n";
		}
		break;
		
	case "txt_wel_part_no_blur":			
		try
		{
			$conn=werp_db_connect();
			
			//获取partflm数据
			$sql="SELECT wel_part_des,wel_unit,wel_pur_unit,wel_pur_unit_rate FROM #__wel_partflm ".
				"WHERE wel_part_no='$txt_wel_part_no' limit 1";
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	//查询sql时出错了
			if($partflm=mysql_fetch_array($result))
			{
				$str_wel_part_des=$partflm['wel_part_des'];
				$str_wel_unit=$partflm['wel_unit'];
				$str_wel_pur_unit=$partflm['wel_pur_unit'];
				$dec_wel_pur_unit_rate=$partflm['wel_pur_unit_rate'];;
			}
			
			// get vendor part MOQ information
			$sql="select wel_lead_tm,wel_pkg_ord,wel_min_ord,wel_ven_part,wel_ven_part_des from #__wel_venparm ".
				"where wel_ven_code='$txt_wel_ven_code' and wel_part_no='$txt_wel_part_no' limit 1";
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	//查询sql时出错了
			if($row=mysql_fetch_array($result))
			{
				$dec_wel_lead_tm=$row['wel_lead_tm'];
				$dec_wel_pkg_ord=$row['wel_pkg_ord'];
				$dec_wel_min_ord=$row['wel_min_ord'];
				$str_wel_ven_part=$row['wel_ven_part'];
				$str_wel_ven_part_des=$row['wel_ven_part_des'];	
			}	
			
			$msg_script="document.getElementById('txt_wel_part_des').value='".format_slashes($str_wel_part_des)."';\n".
						"document.getElementById('txt_wel_unit_code').value='".format_slashes($str_wel_unit)."';\n".
						"document.getElementById('txt_wel_pur_unit').value='".format_slashes($str_wel_pur_unit)."';\n".
						"document.getElementById('ntxt_wel_pur_unit_rate').value='".format_slashes($dec_wel_pur_unit_rate)."';\n".
						"document.getElementById('ntxt_wel_lead_tm').value='".format_slashes($dec_wel_lead_tm)."';\n".
						"document.getElementById('ntxt_wel_pkg_ord').value='".format_slashes($dec_wel_pkg_ord)."';\n".
						"document.getElementById('ntxt_wel_min_ord').value='".format_slashes($dec_wel_min_ord)."';\n".
						"document.getElementById('txt_wel_ven_part').value='".format_slashes($str_wel_ven_part)."';\n".
						"document.getElementById('txt_wel_ven_part_des').value='".format_slashes($str_wel_ven_part_des)."';\n";	
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
			$msg_detail=extract_message($e->getMessage());
		}
		break;
			
	default:
		break;
}
echo "|".coding_str($opt_action)."|".coding_str($msg_code)."|".coding_str($msg_detail)."|".coding_str($msg_script);
?>