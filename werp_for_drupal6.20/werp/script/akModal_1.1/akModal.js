/**
*	akModal-  simplest alternative to thickbox
*	author: Amit Kumar Singh 
* 	project url : http://amiworks.co.in/talk/akmodal-simplest-alternative-to-thickbox/
 * 	inspired from early versions of thickbox
 *	
**/
/**
  * Version 2.0.0
  *  @param String  navurl             url to dispaly in the ifame
  *  @param String  title      title of the pop up box
  *  @param  Numeric  box_width	width of the box in pixels
  *  @param  Numeric  box_height	height of the box in pixels
  *   
 **/

jQuery.extend({
	showAkModal:function(navurl,title,box_width,box_height,ifra_scrolling)
	{
		var offset={};
		var options ={
		margin:1,
		border:1,
		padding:1,
		scroll:0
		};
		
		var win_width =$(window).width();
		var scrollToLeft=$(window).scrollLeft();
		var win_height =$(window).height();
		var scrollToBottom=$(window).scrollTop();
	   

		$('body').append("<div id='ak_modal_div' style='padding-top:0px;border:1px solid black;"+
			"background-color:#FFFFFF;position: absolute;z-index:2999;display:none;width:"+box_width+"px'>"+
			"<div style='height:32px;background:url(" + WERP_FOLL_URI + "images/headerbg.gif) repeat-x 50%;"+
			"border-bottom: 1px solid #4FA4E5; color: #005C89; font-size: 16pt; "+
			"padding-left: 8px;font-family:Arial, helvetica, sans-serif;'>"+
			"<table border='0' cellpadding='0' cellspacing='0'>"+
			"<tr>"+
			"<td><div style='font-weight:bold;font-size:15pt;color:#005C89;width:"+(box_width-32)+"px;'>"+title+"</div></td>"+
			"<td><img src='" + WERP_FOLL_URI + "script/akModal_1.1/close3.gif' id='close' style='cursor:pointer;'></td>"+
			"</tr>"+
			"</table>"+
			"</div>"+
			"<iframe width='100%' height='"+(box_height-32)+
			"' frameborder='0' marginwidth='0' marginheight='0' scrolling='"+ifra_scrolling+"' "+
			"name='frmTest' src='"+navurl+"'>"+
			"</iframe>"+
			"</div>");

		$('#ak_modal_div').css({
			left:(((win_width/2-box_width/2))+scrollToLeft)+'px',
			top:(((win_height/2-box_height/2))+scrollToBottom)+'px'});
			
		/*
		$("#ak_modal_div").draggable();
		$('#ak_modal_div').bind('drag',
			function(event){$(this).css({top:event.offsetY,left:event.offsetX});}
		);
		*/

		$(document).bind("keypress",$.closeAkModal);
		$('#close').click( function() {
			$(document).unbind("keypress",$.closeAkModal);
			$('#ak_modal_div').fadeOut(500);
			$('#ak_modal_div').remove();
			$.dimScreenStop();
		});
		$.dimScreen(500, 0.3, function() {
			$('#ak_modal_div').fadeIn(100);
		});
		
		var offset = {};
		offset=$("#ak_modal_div").offset({ scroll: false });
	
		X_left=offset.left+box_width-25;
		X_top=offset.top;

		/*
		$('#close').css({left:X_left,top:8});
		*/
	},
	
	closeAkModal:function(e){
		var code = e.keyCode ? e.keyCode : e.which;
		//window.alert(code);
		if(code == 27){$("#close").click();}
	},
	
	akModalRemove:function()
	{
	   $('#ak_modal_div').fadeOut(500);
	   $.dimScreenStop();
	},
	
	akModalHideAndRedirect:function(redirect_url)
	{
		$('#ak_modal_div').fadeOut(500);
		$.dimScreenStop();
		window.location=redirect_url;
	}
});	