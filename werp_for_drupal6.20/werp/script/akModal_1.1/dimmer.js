//dimScreen()
//by Brandon Goldman
jQuery.extend({
	//dims the screen
	dimScreen: function(speed, opacity, callback) {
		if(jQuery('#__dimScreen').size() > 0) return;
		
		if(typeof speed == 'function') {
			callback = speed;
			speed = null;
		}

		if(typeof opacity == 'function') {
			callback = opacity;
			opacity = null;
		}

		if(speed < 1) {
			var placeholder = opacity;
			opacity = speed;
			speed = placeholder;
		}
		
		if(opacity >= 1) {
			var placeholder = speed;
			speed = opacity;
			opacity = placeholder;
		}

		speed = (speed > 0) ? speed : 500;
		opacity = (opacity > 0) ? opacity : 0.5;
		
		jQuery('<iframe></iframe>').attr({
				id: '__dimScreen__iframe'
				,frameBorder: 0
				,fade_opacity: opacity
				,speed: speed
			}).css({
			background: '#000'
			,height: $(document).height() + 'px'
			//,height: '100%'
			,left: '0px'
			,opacity: 0
			,position: 'absolute'
			,top: '0px'
			//,width: $(document).width() + 'px'
			,width: '100%'
			,zIndex: 999
		}).appendTo(document.body);
		
		jQuery('<div></div>').attr({
				id: '__dimScreen'
				,fade_opacity: opacity
				,speed: speed
			}).css({
			background: '#000'
			,height: $(document).height() + 'px'
			//,height: '100%'
			,left: '0px'
			,opacity: 0
			,position: 'absolute'
			,top: '0px'
			//,width: $(document).width() + 'px'
			,width: '100%'
			,zIndex: 1999
		}).appendTo(document.body).fadeTo(speed, 0.3, callback);
		
		$("#__dimScreen").bind("click",function (){$("#close").click();});
	},
	
	//stops current dimming of the screen
	dimScreenStop: function(callback) {
		var x = jQuery('#__dimScreen');
		var y = jQuery('#__dimScreen__iframe');
		var opacity = x.attr('fade_opacity');
		var speed = x.attr('speed');
		x.fadeOut(speed, function() {
			y.remove();
			x.remove();
			if(typeof callback == 'function') callback();
		});
	}
});