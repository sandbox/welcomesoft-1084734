
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/

function script_timeout(return_script){
	var b_flag="<!--HTML_DO_NOT_EDIT_OR_DELETE_THIS_LINE_BEGIN-->";
	var e_flag="<!--HTML_DO_NOT_EDIT_OR_DELETE_THIS_LINE_END-->";
	var b_pos=return_script.indexOf(b_flag);
	var e_pos=return_script.indexOf(e_flag);
	if ((b_pos!=-1) && (e_pos!=-1)){	//长时间没动作，timeout了
		return_script=return_script.substr(b_pos + b_flag.length , e_pos - b_pos - b_flag.length);
		//window.alert(return_script);
		$('body').append(return_script);
		return true;
	}
	return false;
}

function show_all_object_ids(){	//这个函数只供查看在页面设计时对象的完整性
	var object_id_list_arr=object_id_list.split('|');
	var object_id_list_arr_str="";
	for(var i=0;i<object_id_list_arr.length;i++){
		object_id_list_arr_str=
		object_id_list_arr_str+object_id_list_arr[i]+'\n';
	}

	var hidden_object_id_list_arr=hidden_object_id_list.split('|');
	var hidden_object_id_list_arr_str="";
	for(var i=0;i<hidden_object_id_list_arr.length;i++){
		hidden_object_id_list_arr_str=
		hidden_object_id_list_arr_str+hidden_object_id_list_arr[i]+'\n';
	}
	
	var only_lay_object_id_list_arr=only_lay_object_id_list.split('|');
	var only_lay_object_id_list_arr_str="";
	for(var i=0;i<only_lay_object_id_list_arr.length;i++){
		only_lay_object_id_list_arr_str=
		only_lay_object_id_list_arr_str+only_lay_object_id_list_arr[i]+'\n';
	}
	
	alert('follow message only for testing\n'+
				'XML OBJECT LIST:'+object_id_list_arr_str+
				'HIDDEN OBJECT LIST:'+hidden_object_id_list_arr_str+
				'ONLY LAY OBJECT LIST:'+only_lay_object_id_list_arr_str);
}


//把remove_object_id_list列表从object_id_list列表中移除，然后还回移除后的结果
function remove_object_id(object_id_list,remove_object_id_list){
	var new_object_id_list="";
	var object_id_list_arr=object_id_list.split("|");
	var remove_object_id_list_arr=remove_object_id_list.split("|");
	for(var key_index=0; key_index<object_id_list_arr.length; key_index++){
		for(var key=0; key<remove_object_id_list_arr.length; key++){
			if (object_id_list_arr[key_index]==remove_object_id_list_arr[key]){
				object_id_list_arr[key_index]="";
			}
		}
	}
	for(var key_index=0; key_index<object_id_list_arr.length; key_index++){
		if (object_id_list_arr[key_index]!=""){
			new_object_id_list=new_object_id_list+"|"+object_id_list_arr[key_index];
		}
	}
	if (new_object_id_list.length>0){new_object_id_list=new_object_id_list.substr(1);}
	return new_object_id_list;
}

//清除当前页面所有可输入对象的内容
function clear_screen_layout(object_id_list){
	object_id_list=object_id_list.toLowerCase();

	var object_id_list_arr=object_id_list.split("|");
	for (var key_index=0; key_index<object_id_list_arr.length; key_index++){
		var object_id=trim(object_id_list_arr[key_index]);

		if (object_id!=null && object_id!="" && 
			object_id!="main_page" && object_id!="main_page_dir" && 
			object_id!="action_page" && object_id!="action_page_dir"){
			try{
			switch((document.getElementById(object_id).tagName).toUpperCase()){
				case "SELECT":	//type=="SELECT-ONE",type=="SELECT-MULTIPLE"
					var sel=document.getElementById(object_id);
					if (sel.length>0){sel[0].selected=true;}
					break;
				case "TEXTAREA":
					document.getElementById(object_id).value="";
					break;
				case "INPUT":
					switch((document.getElementById(object_id).type).toUpperCase()){
						case "RADIO":
							var rad=document.getElementsByName(object_id);
							if (rad.length>0){rad[0].checked=true;}
							break;
						case "CHECKBOX":
							document.getElementById(object_id).checked=false;
							break;
						case "IMAGE":
						case "SUBMIT":
						case "RESET":
						case "BUTTON":
							break;	//按钮不清除value
						case "FILE":
						case "HIDDEN":
						case "PASSWORD":
						case "TEXT":
							document.getElementById(object_id).value="";
							
							//预设客户端当前日期到对象中
							var fd_attr=''+$('#'+object_id).attr('format_date');
							if (fd_attr!='undefined'){
								$('#'+object_id).attr('value',$('#'+object_id).attr('default_value'));
							}
							break;
					}
					break;
				default:
					break;
			}
			}catch(err){
				if (to_boolean(WEL_DEVELOP_INFO,false)){
					window.alert(object_id+" maybe not found ! (clear_screen_layout)");
				}
			}
		}
	}	
}

/*
add_select_item 函数功能:对SELECT元素增加项
使用:只能传入2种格式
格式1:每项之间用|分隔，select_value里的项数跟select_description相等
var select_value="a|b|c|d|e";
var select_description="a name|b name|c name|d name|e name";
格式2:把每个项放入数组里，select_value里的元素个数跟select_description相等
var select_value=new Array("a","b","c","d","e");
var select_description=new Array("a name","b name","c name","d name","e name");
*/
function add_select_item(object_id,select_value,select_description){
	var select_value_arr;
	var select_description_arr;
	if ((typeof(select_value)=='object') && select_value.constructor==Array){
		select_value_arr=select_value;
		select_description_arr=select_description;
	}
	else{
		select_value=""+select_value+"";
		select_description=""+select_description+"";
		select_value_arr=select_value.split("|");
		select_description_arr=select_description.split("|");
	}

	var sel=document.getElementById(object_id);
	for (var key_index=0; key_index<select_value_arr.length; key_index++){
		var value=un_coding_str(""+select_value_arr[key_index]+"");
		var description=un_coding_str(""+select_description_arr[key_index]+"");
		var oOption = document.createElement("OPTION");
		oOption.text=description;
		oOption.value=value;
		sel.options.add(oOption);
	}
}

/*
remove_select_list 函数功能:对SELECT元素移除项
使用:
object_id为SELECT的id
select_value为要移除的项的value,如果select_value为空字符串，则移除所有的项
*/
function remove_select_list(object_id,select_value){
	var sel=document.getElementById(object_id);
	select_value=trim(un_coding_str(""+select_value+""));
	if (select_value==""){
		sel.innerHTML="";
		return true;
	}
	else{
		for (var i=0; i<sel.length; i++){
			if (sel[i].value.toLowerCase()==select_value.toLowerCase()){
				sel.remove(i);
				return true;
				break;
			}
		}
		return false;
	}
}

/*
选择指定value的SELECT元素项,
object_id为SELECT的id,
select_value为要选择的项的value,如果类型为数字,则当作索引项
*/
function set_select_selected(object_id,select_value){
	var sel=document.getElementById(object_id);
	if (typeof(select_value)=="number"){
		for (var i=0; i<sel.length; i++){
			if (i==select_value){sel[i].selected=true;return true;break;}
		}
		return false;
	}

	select_value=trim(un_coding_str(""+select_value+""));
	for (var i=0; i<sel.length; i++){
		if (sel[i].value.toLowerCase()==select_value.toLowerCase()){
			sel[i].selected=true;
			return true;
			break;
		}
	}
	return false;
}

/*
选择指定value的INPUT type=radio元素项,
object_name为INPUT的name,
radio_value为要选择的项的value,如果类型为数字,则当作索引项
*/
function set_radio_checked(object_name,radio_value){
	var rad=document.getElementsByName(object_name);
	if (typeof(radio_value)=="number"){
		for (var i=0; i<rad.length; i++){
			if (i==radio_value){rad[i].checked=true;return true;break;}
		}
		return false;
	}

	radio_value=trim(un_coding_str(""+radio_value+""));
	for (var i=0; i<rad.length; i++){
		if (rad[i].value.toLowerCase()==radio_value.toLowerCase()){
			rad[i].checked=true;
			return true;
			break;
		}
	}
	return false;
}

/*
取得INPUT TYPE=radio元素项的value,
object_name为INPUT的name
*/
function get_radio_checked_value(object_name){
	var rad=document.getElementsByName(object_name);
	for (var i=0; i<rad.length; i++){
		if (rad[i].checked){
			return rad[i].value;
			break;
		}
	}
	return "";
}

/*
取得INPUT TYPE=radio元素项的id,
object_name为INPUT的name
*/
function get_radio_checked_id(object_name){
	var rad=document.getElementsByName(object_name);
	for (var i=0; i<rad.length; i++){
		if (rad[i].checked){
			return rad[i].id;
			break;
		}
	}
	return "";
}

//对字符串里的字符|进行编码
function coding_str(str){
	return str.replace(/\%/g, '%PERCENT').replace(/\|/g, '%VERTICAL');
}
//对字符串里的字符|进行编码还原
function un_coding_str(str){
	return str.replace(/%VERTICAL/g, "|").replace(/%PERCENT/g, "%");
}

//对url进escape
function url_escape(url){
	url=""+url+"";
	return escape(url.replace(/\%/g, '%URL')).replace(/\+/g, "%2B").replace(/\//g, "%2F");
}

function get_object_value(object_id){
	object_id=trim(""+object_id+"");
	var return_value="";
	try{
	switch((document.getElementById(object_id).tagName).toUpperCase()){
		case "SELECT":	//type=="SELECT-ONE",type=="SELECT-MULTIPLE"
		case "TEXTAREA":
			return_value=document.getElementById(object_id).value;
			break;
		case "INPUT":
			switch((document.getElementById(object_id).type).toUpperCase()){
				case "RADIO":
					return_value=
						document.getElementById(object_id).checked?
						document.getElementById(object_id).value:"";
					break;
				case "CHECKBOX":
					return_value=document.getElementById(object_id).checked?"1":"0";
					break;
				case "IMAGE":
				case "SUBMIT":
				case "RESET":
				case "BUTTON":
					break;	//按钮不取url
				case "FILE":
				case "HIDDEN":
				case "PASSWORD":
				case "TEXT":
					return_value=document.getElementById(object_id).value;
					break;
			}
			break;
		default:
			break;
	}
	}catch(err){
		if (to_boolean(WEL_DEVELOP_INFO,false)){
			window.alert(object_id+" maybe not found ! (get_object_value)");
		}
	}
	return return_value;
}

/*对可输入对象进行url进行自动组装,
action_page:页面，
opt_action:指令，
object_id_list:对象集合
*/
function get_url_parameter(action_page,opt_action,object_id_list){
	action_page=trim(action_page);
	document.getElementById("action_page").value=action_page.substr(0,action_page.length-4);

	var url_parameter=WERP_BASE_URI+WERP_EXPA_PARA+"&opt_action="+url_escape(opt_action)+
		"&lang="+url_escape(WERP_EXTE_LANG);

	object_id_list=object_id_list.toLowerCase();

	var object_id_list_arr=object_id_list.split("|");
	for (var key_index=0; key_index<object_id_list_arr.length; key_index++){
		var object_id=trim(object_id_list_arr[key_index]);

		if (object_id!=null && object_id!=""){
			url_parameter=
				url_parameter+"&"+object_id+"="+url_escape(get_object_value(object_id));
		}
	}
	return url_parameter;
}

/*对象进行事件帮定,
el_id:对象id,
event_name:事件名称,
event_function:事件函数
*/
function bind_event(el_id,event_name,event_function){
	$("#"+el_id).bind(event_name,event_function);return true;
	if(document.getElementById(el_id).addEventListener){
		document.getElementById(el_id).addEventListener(event_name,event_function,false);
		return true;
	}
	else if(document.getElementById(el_id).attachEvent){
		document.getElementById(el_id).attachEvent("on"+event_name,event_function);
		return true;
	}
	else{return false;}
}

//移除字符串左边的所有空格
function ltrim(str){
	if (typeof(str)=="undefined"){return "";}
	if (typeof(str)!="string"){return "";}
	var return_str="";
	for(var i=0;i<str.length;i++){if(str.charAt(i)!=' '){return_str=str.substr(i);break;}}
	return return_str;
}
//移除字符串右边的所有空格
function rtrim(str){
	if (typeof(str)=="undefined"){return "";}
	if (typeof(str)!="string"){return "";}
	var return_str="";
	for(var i=str.length-1;i>=0;i--){if(str.charAt(i)!=' '){return_str=str.substring(0,i+1);break;}}
	return return_str;
}
//移除字符串左边和右边的所有空格
function trim(str){return ltrim(rtrim(str));}

//以下3个function给javascript内置对象String加上3个函数
String.prototype.ltrim=function(){
	var return_str="";
	for(var i=0;i<this.length;i++){if(this.charAt(i)!=' '){return_str=this.substr(i);break;}}
	return return_str;
}
String.prototype.rtrim=function(){
	var return_str="";
	for(var i=this.length-1;i>=0;i--){if(this.charAt(i)!=' '){return_str=this.substring(0,i+1);break;}}
	return return_str;
}
String.prototype.trim=function(){return this.ltrim().rtrim();}

//将表达式exp转换为boolan值,如果无法转换则传回return_default
function to_boolean(exp,return_default){
	if (typeof(exp)=="undefined"){return return_default;}
	if (typeof(exp)=="boolean"){return exp;}
	if (typeof(exp)=="number"){if (exp==0){return false;}else{return true;}}
	
	var expression=exp.toString().toLowerCase();
	if (expression=="false"){return false;}
	if (expression=="true"){return true;}
	if (expression=="yes"){return false;}
	if (expression=="no"){return true;}
	
	var temp_number=parseFloat(exp);
	if (!isNaN(temp_number)){if (temp_number==0){return false;}else{return true;}}
	return return_default;
}

/*
类库内部调用函数
对页面输入控件进行开启或关闭输入功能
object_id:对象ID,
status:开启或关闭的状态,
该功能与enable_object基本相同，不同的是，enable_object是设置多个，这个函数只设置一个
请使用enable_object函数，因为这个函数不会设置对象的className
*/
function _set_enabled_status(object_id,status){
	status=to_boolean(status,false);
	try{
	switch((document.getElementById(object_id).tagName).toUpperCase()){
		case "SELECT":	//type=="SELECT-ONE",type=="SELECT-MULTIPLE"
			document.getElementById(object_id).disabled=!status;
			break;
		case "TEXTAREA":
			document.getElementById(object_id).readOnly=!status;
			break;
		case "INPUT":
			switch((document.getElementById(object_id).type).toUpperCase()){
				case "RADIO":
					var rad=document.getElementsByName(object_id);
					for (var i=0;i<rad.length;i++){
						rad[i].disabled=!status;
					}
					break;
				case "CHECKBOX":
				
				case "IMAGE":
				case "SUBMIT":
				case "RESET":
				case "BUTTON":
					document.getElementById(object_id).disabled=!status;
					break;
				case "FILE":
					document.getElementById(object_id).disabled=!status;
					break;
				case "HIDDEN":
				case "PASSWORD":
				case "TEXT":
					document.getElementById(object_id).readOnly=!status;
					break;
			}
			break;
		default:
			break;
	}
	}catch(err){
		if (to_boolean(WEL_DEVELOP_INFO,false)){
			window.alert(object_id+" maybe not found ! (_set_enabled_status)");
		}
	}
}

/*
类库内部调用函数
读取控件开启或关闭的状态
*/
function _get_enabled_status(object_id){
	try{
	switch((document.getElementById(object_id).tagName).toUpperCase()){
		case "SELECT":	//type=="SELECT-ONE",type=="SELECT-MULTIPLE"
			return(!document.getElementById(object_id).disabled);
			break;
		case "TEXTAREA":
			return(!document.getElementById(object_id).readOnly);
			break;
		case "INPUT":
			switch((document.getElementById(object_id).type).toUpperCase()){
				case "RADIO":
					var rad=document.getElementsByName(object_id);
					for (var i=0;i<rad.length;i++){
						return(!rad[i].disabled);
					}
					break;
				case "CHECKBOX":
				
				case "IMAGE":
				case "SUBMIT":
				case "RESET":
				case "BUTTON":
					return(!document.getElementById(object_id).disabled);
					break;
				case "FILE":
				case "HIDDEN":
				case "PASSWORD":
				case "TEXT":
					return(!document.getElementById(object_id).readOnly);
					break;
			}
			break;
		default:
			break;
	}
	}catch(err){
		if (to_boolean(WEL_DEVELOP_INFO,false)){
			window.alert(object_id+" maybe not found ! (_get_enabled_status)");
		}
	}
	return false;
}

/*
类库内部调用函数
设置对象的className
*/
function _set_object_className(object_id,className){
	if ((document.getElementById(object_id).tagName).toUpperCase()=="INPUT" &&
		(document.getElementById(object_id).type).toUpperCase()=="RADIO"){
		var rad=document.getElementsByName(object_id);
		for (var i=0;i<rad.length;i++){
			rad[i].className=className;
		}
	}else{
		document.getElementById(object_id).className=className;
	}	
}

/*
类库内部调用函数
读取对象的className
*/
function _get_object_className(object_id){
	if ((document.getElementById(object_id).tagName).toUpperCase()=="INPUT" &&
		(document.getElementById(object_id).type).toUpperCase()=="RADIO"){
		var rad=document.getElementsByName(object_id);
		for (var i=0;i<rad.length;i++){
			return(rad[i].className);
		}
	}else{
		return(document.getElementById(object_id).className);
	}
}

/*
对页面输入控件进行开启或关闭输入功能
object_id_list:对象ID列表,
status:开启或关闭的状态列表,
security_list:权限列表
*/
function enable_object(object_id_list,status,security_list){
	object_id_list=object_id_list.toLowerCase();
	security_list=security_list.toLowerCase();
	status=to_boolean(status,false);
	var security_list_arr=security_list.split("|");

	var object_id_list_arr=object_id_list.split("|");
	for (var key_index=0; key_index<object_id_list_arr.length; key_index++){
		var object_id=trim(object_id_list_arr[key_index]);
		var security=to_boolean(trim(security_list_arr[key_index]),true);
		var security_status=security && status;

		if (object_id!=null && object_id!=""){
			_set_enabled_status(object_id,security_status);

			var className=_get_object_className(object_id);
			var sEnable="";
			var sDim="";
			if (className!=null){
				if (className.length>4){
					sEnable=className.substr(0,className.length-4);
					sDim=className.substr(className.length-4);
					if (sDim!="_dim"){
						if (!security_status){
							_set_object_className(object_id,className+"_dim");
						}
					}
					else{
						if (security_status){
							_set_object_className(object_id,sEnable);
						}
					}
				}
			}
		}
	}
}

/*
类库内部调用函数
功能：设定对象的标签
*/
function _set_client_object_label(object_id,value){
	try{
		object_id=object_id.toLowerCase();
		switch((document.getElementById(object_id).tagName).toUpperCase()){
			case "OPTION":
				document.getElementById(object_id).text=value;
				break;
			case "DIV":
			case "SPAN":
			case "LABEL":
				document.getElementById(object_id).innerHTML=value;
				break;
			case "INPUT":
				switch((document.getElementById(object_id).type).toUpperCase()){
					case "SUBMIT":
					case "RESET":
					case "BUTTON":
						document.getElementById(object_id).value=value;
						break;
					default:
						break;
				}
				break;
			default:
				break;
		}
	}catch(e){return false;}
}

/*
0  描述一种"未初始化"状态；此时，已经创建一个XMLHttpRequest对象，但是还没有初始化。 
1  描述一种"发送"状态；此时，代码已经调用了XMLHttpRequest open()方法并且XMLHttpRequest已经准备好把一个请求发送到服务器。 
2  描述一种"发送"状态；此时，已经通过send()方法把一个请求发送到服务器端，但是还没有收到一个响应。 
3  描述一种"正在接收"状态；此时，已经接收到HTTP响应头部信息，但是消息体部分还没有完全接收结束。 
4  描述一种"已加载"状态；此时，响应已经被完全接收。 
*/
var net = new Object();
net.READ_STATE_UNINITIALIZED=0;
net.READ_STATE_LOADING=1;
net.READ_STATE_LOADED=2;
net.READ_STATE_INTERACTIVE=3;
net.READ_STATE_COMPLETE=4;
net.content_loader = function(url,onload,onerror){ 
	this.url = url;
	this.req = null;
	this.onload = onload;
	this.onerror = onerror ? onerror : this.defaultError;
	
	this.object_id_list_arr=new Array();
	this.enabled_status_arr=new Array();
	this.keep_original______object_id_list______enabled_status();
	
	this.loadXMLDoc(url);
}
net.content_loader.prototype={
	keep_original______object_id_list______enabled_status:function(){
		this.object_id_list_arr=object_id_list.split("|");
		for (var key_index=0; key_index<this.object_id_list_arr.length; key_index++){
			var object_id=trim(this.object_id_list_arr[key_index]);
			if (object_id!=null && object_id!=""){
				this.enabled_status_arr[key_index]=_get_enabled_status(object_id);
				//_set_enabled_status(object_id,false);
			}
		}
		//enable_object(object_id_list,false,"");
	},
	
	restore_enabled_status:function(){
		for (var key_index=0; key_index<this.object_id_list_arr.length; key_index++){
			var object_id=trim(this.object_id_list_arr[key_index]);
			if (object_id!=null && object_id!=""){
				//_set_enabled_status(object_id,this.enabled_status_arr[key_index]);
				//enable_object(object_id_list,this.enabled_status_arr[key_index],"");
			}
		}
	},
	
	loadXMLDoc:function(){ 
		try{ 
		this.req = new XMLHttpRequest();
		}catch(e1){ 
		try{ 
			this.req = new ActiveXObject("Msxml2.XMLHTTP");
		}catch(e2){ 
			try{ 
				this.req = new ActiveXObject("Microsoft.XMLHTTP");
			}catch(e3){ 
				this.req = null;
			}
		}
		}

		if(this.req){ 
			try{
				var loader = this;
				this.req.onreadystatechange = function(){
					loader.onReadyState.call(loader); 
				}
/*
open(method, url, async, username, password)method 参数是用于请求的 HTTP 方法。值包括 GET、POST 和 HEAD。
url 参数是请求的主体。大多数浏览器实施了一个同源安全策略，并且要求这个 URL 与包含脚本的文本具有相同的主机名和端口。
async 参数指示请求使用应该异步地执行。如果这个参数是 false，请求是同步的，后续对 send() 的调用将阻塞，直到响应完全接收。
如果这个参数是 true 或省略，请求是异步的，且通常需要一个 onreadystatechange 事件句柄。
username 和 password 参数是可选的，为 url 所需的授权提供认证资格。如果指定了，它们会覆盖 url 自己指定的任何资格。
*/
				/*
				window.alert(this.url);
				this.req.open("POST",this.url,true);
				this.req.setRequestHeader("Content-Type", "text/html;charset=UTF-8");
				this.req.send(null);
				*/
				var this_url_arr=this.url.split("?");
				var lowercase_wep=WERP_EXPA_PARA.toLowerCase();
				if (lowercase_wep=="?q=werp"){		//the case only for drupal
					lowercase_wep_pos=this.url.indexOf(lowercase_wep);
					if (lowercase_wep_pos!=-1){
						this_url_arr[0]=this.url.substr(0,lowercase_wep_pos+lowercase_wep.length);
						this_url_arr[1]=this.url.substr(lowercase_wep_pos+lowercase_wep.length+1);
					}
				}
				//window.alert(this_url_arr[0]);
				//window.alert(this_url_arr[1]);
				this.req.open("POST",this_url_arr[0],true);
				this.req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
				this.req.send(this_url_arr[1]);
			}catch(err){
				this.onerror.call(this);
			}
		}
		else{
		throw "create XMLHttpRequest error,u brower doesn't surrpot ajax";
		return;
		}
	},

	onReadyState:function(){ 
		var req = this.req;
		var readyState = req.readyState;
		if(readyState == net.READ_STATE_COMPLETE){ 
		var httpStatus = req.status;
		if(httpStatus == 200 || httpStatus == 0){
			this.restore_enabled_status();
			if (!script_timeout(req.responseText)){this.onload.call(this,req.responseText);}
		}else{ 
			this.onerror.call(this);
		}
		}
	},

	defaultError:function(){ 
		var s = "error operation data!\n"
		  +"\n readyState:"+this.req.readyState
		  +"\n status:"+this.req.status
		  +"\n headers:"+this.req.getAllResponseHeaders();
	window.alert(s); 
	}
}

/*
类库内部调用函数
功能：显示细节数据
*/
function _grid(grid_name,para,checkbox_col){
	var fix_str="";
	if (typeof(para)=="undefined"){para="";}
	if ((typeof(para)=='object') && para.constructor==Array){
		for (var key_index=0; key_index<para.length; key_index++){
			para[key_index]=coding_str(para[key_index]);
			if (fix_str!=""){
				fix_str=fix_str+"|"+para[key_index];
			}
			else{
				fix_str=para[key_index];
			}
		}
	}
	else{
		fix_str=para.trim();
	}
	var url=WERP_BASE_URI+WERP_EXPA_PARA+
		"&main_page="+url_escape(document.getElementById("main_page").value)+
		"&main_page_dir="+url_escape(document.getElementById("main_page_dir").value)+
		"&action_page="+url_escape("detailgrid")+
		"&action_page_dir="+url_escape("stdlib")+
		"&grid_name="+url_escape(grid_name)+
		"&grid_width="+document.getElementById(grid_name).clientWidth+
		"&grid_height="+document.getElementById(grid_name).clientHeight+
		"&fix_str="+url_escape(fix_str)+
		"&lang="+url_escape(WERP_EXTE_LANG)+
		"&checkbox_col="+url_escape(checkbox_col);
	//window.alert(url);
	document.getElementById(grid_name).src=url;
}

/*
类库内部调用函数
功能：近回选择行的一行数据,格式为一维js数组
*/
function _selected_row(grid_name){
	var rt=new Array(false,"data not ready,try again later!");
	var win=document.getElementById(grid_name).contentWindow;
	if (typeof(win._selected_row)=="function"){rt=win._selected_row();}else{}
	if (!rt[0]){window.alert(rt[1]);}
	return rt;
}

/*
类库内部调用函数
功能：删除选择行数据，该动作不会影响数据库内容
*/
function _selected_del(grid_name){
	var rt=new Array(false,"data not ready,try again later!");
	var win=document.getElementById(grid_name).contentWindow;
	if (typeof(win._selected_del)=="function"){rt=win._selected_del();}else{}
	if (!rt[0]){window.alert(rt[1]);}
	return rt;
}

/*
类库内部调用函数
功能：返回选择行的指定列数据(col为指定传入的列)
*/
function _selected_col(grid_name,col){
	var rt=new Array(false,"data not ready,try again later!");
	var win=document.getElementById(grid_name).contentWindow;
	if (typeof(win._selected_col)=="function"){rt=win._selected_col(col);}else{}
	if (!rt[0]){window.alert(rt[1]);}
	return rt;
}

/*
类库内部调用函数
功能：返回所有选择行的数据(有checkbox并且己经checked的数据集合)
*/
function _checkbox_array(grid_name){
	var rt=new Array(false,"data not ready,try again later!");
	var win=document.getElementById(grid_name).contentWindow;
	if (typeof(win._checkbox_array)=="function"){rt=win._checkbox_array();}else{}
	if (!rt[0]){window.alert(rt[1]);}
	return rt;
}

/*
类库内部调用函数
功能：显示data browser
*/
function _browser_click_function(
	browser_name,search_bar,fix_list,search_list,search_col,browser_return_function){
	fix_list=fix_list.trim();
	search_list=search_list.trim();		
	var fix_str="";
	var search_str="";
	if (fix_list!=""){
		var fix_arr=fix_list.split("|");
		for(var key_index=0;key_index<fix_arr.length;key_index++){
			var object_value=get_object_value(fix_arr[key_index]);
			var object_value=coding_str(object_value);
			if (key_index==0){
				fix_str=object_value;
			}
			else{
				fix_str=fix_str+"|"+object_value;
			}
		}
	}
	if (search_list!=""){
		var search_arr=search_list.split("|");
		for(var key_index=0;key_index<search_arr.length;key_index++){
			var object_value=get_object_value(search_arr[key_index]);
			
			//测试数字对象的值是否为零，为零则将要查询的内容设置为空再传递过去
			var fn_attr=''+$('#'+search_arr[key_index]).attr('format_number');
			if (fn_attr!='undefined'){
				if (!isNaN(parseFloat(object_value))){
					if (parseFloat(object_value)==0){object_value="";}
				}
			}
			
			var object_value=coding_str(object_value);
			if (search_str==""){
				search_str=object_value;
			}
			else{
				search_str=search_str+"|"+object_value;
			}
		}
	}
	var browser_url=WERP_BASE_URI+WERP_EXPA_PARA+
		"&main_page="+url_escape(document.getElementById("main_page").value)+
		"&main_page_dir="+url_escape(document.getElementById("main_page_dir").value)+
		"&action_page="+url_escape("browser")+
		"&action_page_dir="+url_escape("stdlib")+
		"&browser_name="+url_escape(browser_name)+
		"&search_bar="+url_escape(search_bar)+
		"&fix_str="+url_escape(fix_str)+
		"&search_str="+url_escape(search_str)+
		"&search_col="+url_escape(search_col)+
		"&lang="+url_escape(WERP_EXTE_LANG)+
		"&browser_return_function="+url_escape(browser_return_function);
	show_browser(browser_url);

//WS	$.showAkModal('http://google.com','Data Browser',502,310);
}

/*
类库内部调用函数
功能：返回data browser选择行的的数据到指定的对象列表
*/
function _browser_return_function(browser_name,return_list,rowdata){
	switch(browser_name) {
	default:
		break;
	}
	
	var bset_first_el_id_focus=false;
	var el_id="";
	var return_arr=return_list.split("|");
	for(var key_index=0;key_index<return_arr.length;key_index++){
		el_id=return_arr[key_index].trim();
		if (el_id!=""){
			if (!bset_first_el_id_focus){$("#"+el_id).focus();bset_first_el_id_focus=true;}
			
			var object_value=rowdata[key_index];
			var fd_attr=""+$("#"+el_id).attr("format_date");
			var fn_attr=""+$("#"+el_id).attr("format_number");
			var d_attr=""+$("#"+el_id).attr("decimal");
			if (fd_attr!="undefined"){
				$("#"+el_id).attr("value",format_date(object_value));
			}else if (fn_attr!="undefined"){
				$("#"+el_id).attr("value",format_number(object_value,d_attr));
			}else{
				if ((document.getElementById(el_id).tagName).toUpperCase()=='SELECT'){
					set_select_selected(el_id,object_value);
				}else if ((document.getElementById(el_id).tagName).toUpperCase()=='INPUT'){
					if ((document.getElementById(el_id).type).toUpperCase()=='RADIO'){
						set_radio_checked(el_id,object_value);
					}else if ((document.getElementById(el_id).type).toUpperCase()=='CHECKBOX'){
						document.getElementById(el_id).checked=to_boolean(object_value,false);
					}else{
						$("#"+el_id).attr('value',object_value);
					}
				}
			}
		}
	}
}

/*
类库内部调用函数
功能：返回blur事件取得的数据到指定的对象列表
*/
function _blur_return_function(return_script){
	//window.alert(return_script);
	if (script_timeout(return_script)){return false};
	return_script=return_script.trim();
	if (return_script!=''){eval(return_script);}
}
/*
类库内部调用函数
功能：对象失去焦点调用数据抽取
*/
function _blur_blur_function(
	blur_name,fix_list,return_list,blur_return_function){
	var fix_str="";
	var fix_arr=fix_list.split("|");
	for(var key_index=0;key_index<fix_arr.length;key_index++){
		var object_value=document.getElementById(fix_arr[key_index]).value;
		var object_value=coding_str(object_value);
		if (fix_str==""){
			fix_str=object_value;
		}
		else{
			fix_str=fix_str+"|"+object_value;
		}
	}

	var blur_url=WERP_BASE_URI+WERP_EXPA_PARA+
		"&main_page="+url_escape(document.getElementById("main_page").value)+
		"&main_page_dir="+url_escape(document.getElementById("main_page_dir").value)+
		"&action_page="+url_escape("blur")+
		"&action_page_dir="+url_escape("stdlib")+
		"&blur_name="+url_escape(blur_name)+
		"&fix_str="+url_escape(fix_str)+
		"&return_list="+url_escape(return_list)+
		"&lang="+url_escape(WERP_EXTE_LANG);
	var handler=new net.content_loader(blur_url,blur_return_function);
}

/*
类库内部调用函数
功能：测试键盘输入的内容是否在指的列表中
*/
function _valKey (keyRE, e) {
	/*
	在Opera下：
	>> 支持keyCode和which，二者的值相同
	>> 不支持charCode，值为 undefined 
	在IE下：
	>> 支持keyCode
	>> 不支持which和charCode,二者值为 undefined
	在Firefox下：
	>> 支持keyCode，除功能键外，其他键值始终为 0
	>> 支持which和charCode，二者的值相同
	*/
	
	if ( !(keyRE instanceof RegExp) ) {return 0;}
	if ( /^13$/.test(String(e.keyCode || e.charCode)) ) {return -1;}
	if (typeof(e.charCode) == 'undefined'){	//IE or Opera
		if (String.fromCharCode(e.keyCode).search(keyRE) != (-1)){return 1;}
		return 0;
	}else{	//Firefox
		//charCode==0,则表示按下的是功能键(keyCode表示功能键)
		if (e.charCode == 0){return 2;}
		if (String.fromCharCode(e.charCode).search(keyRE) != (-1)){return 1;}
		return 0;
	}
}


/*
类库内部调用函数
功能：初始化date picker
*/
function _init_date_picker(picker_name){
	var url=WERP_BASE_URI+WERP_EXPA_PARA+
		"&main_page="+url_escape(document.getElementById("main_page").value)+
		"&main_page_dir="+url_escape(document.getElementById("main_page_dir").value)+
		"&action_page="+url_escape("datepicker")+
		"&action_page_dir="+url_escape("stdlib")+
		"&picker_name="+url_escape(picker_name);
	//window.alert(url);
	document.getElementById(picker_name).src=url;
}

/*
类库内部调用函数
功能：打开date picker
*/
function _date_picker(div_date,ifra_date,original_date,return_list){
	var div_date_offsetLeft=0;
	var div_date_offsetTop=0;
	var div_date_offsetWidth=262;	//$("#"+div_date).attr('offsetWidth');
	var div_date_offsetHeight=186;	//$("#"+div_date).attr('offsetHeight');
	var div_offsetParent=document.getElementById(div_date);
	if (div_offsetParent){div_offsetParent=div_offsetParent.offsetParent;}
	while (div_offsetParent){
		if (div_offsetParent.tagName.toLowerCase()=="body" || 
			div_offsetParent.tagName.toLowerCase()=="html"){break;}
		div_date_offsetLeft=div_date_offsetLeft+div_offsetParent.offsetLeft;
		div_date_offsetTop=div_date_offsetTop+div_offsetParent.offsetTop;
		div_offsetParent=div_offsetParent.offsetParent;
	}

	var original_date_offsetLeft=0;
	var original_date_offsetTop=0;
	var original_date_offsetWidth=$("#"+original_date).attr('offsetWidth');
	var original_date_offsetHeight=$("#"+original_date).attr('offsetHeight');
	var original_offsetParent=document.getElementById(original_date);
	while (original_offsetParent){
		if (original_offsetParent.tagName.toLowerCase()=="body" || 
			original_offsetParent.tagName.toLowerCase()=="html"){break;}
		original_date_offsetLeft=original_date_offsetLeft+original_offsetParent.offsetLeft;
		original_date_offsetTop=original_date_offsetTop+original_offsetParent.offsetTop;
		original_offsetParent=original_offsetParent.offsetParent;
	}

	var document_offsetHeight = document.body.offsetHeight;
	var document_offsetWidth = document.body.offsetWidth;
	//window.alert("document_offsetHeight:"+document_offsetHeight);
	//window.alert("document_offsetWidth:"+document_offsetWidth);

	var div_date_left=0;
	var div_date_top=0;
	
	div_date_left=original_date_offsetLeft-div_date_offsetLeft-
		(div_date_offsetWidth-original_date_offsetWidth)/2;
	if ((document_offsetWidth-div_date_offsetWidth)<div_date_left){
		div_date_left=document_offsetWidth-div_date_offsetWidth-4;
	}if (div_date_left<0){div_date_left=0;}

	div_date_top=original_date_offsetTop-div_date_offsetTop+
		original_date_offsetHeight;
	if ((document_offsetHeight-div_date_offsetHeight)<div_date_top){
		if ((original_date_offsetTop-div_date_offsetHeight-2)>=0){
			div_date_top=original_date_offsetTop-div_date_offsetHeight-2;
		}
	}if (div_date_top<0){div_date_top=0;}

	//window.alert(div_date_left + " " +div_date_top);
	
	$("#"+div_date).css({left:div_date_left,top:div_date_top});
	var original_date_date=document.getElementById(original_date).value;
	var win=document.getElementById(ifra_date).contentWindow;
	win.set_calendar_parameter(div_date,ifra_date,original_date_date,return_list);
}

/*
类库内部调用函数
功能：关闭date picker
*/
function _hidden_date_picker(div_date){
	//$("#"+div_date).css({display:'none'});
	$("#"+div_date).css({zIndex:-1000,height:0,width:0});
	$("input").unbind("click",_un_date_picker);
	$("select").unbind("click",_un_date_picker);
	$("textarea").unbind("click",_un_date_picker);
}
function _un_date_picker(event){_hidden_date_picker(event.data.div_date);}
/*
类库内部调用函数
功能：打开date picker,在_date_picker调用之后被自动调用
*/
function _show_date_picker(div_date){
	//$("#"+div_date).css({display:'inline'});
	$("#"+div_date).css({zIndex:1000,height:186,width:262});
	$("input").bind("click",{div_date:div_date},_un_date_picker);
	$("select").bind("click",{div_date:div_date},_un_date_picker);
	$("textarea").bind("click",{div_date:div_date},_un_date_picker);
}

/*
类库内部调用函数
功能：过滤日期对象的输入字符
*/
function _date_keypress(el_id,e){
	var keyTest=_valKey(/[0-9-]/, e);
	if (keyTest==0){return false;}
	if (keyTest==-1 || keyTest==2){return true;}
	var keyTest=_valKey(/-/, e);
	if (keyTest==1){
		var count=0;
		var a=$("#"+el_id).attr("value").trim().split("");
		for (var i=0;i<a.length;i++){if (a[i]=='-'){count++;if (count>1){return false;}}}
	}
	return true;
}
/*
类库内部调用函数
功能：日期对象失去焦点后格式化日期
*/
function _date_blur(el_id,e){
	var ymd=$("#"+el_id).attr("value").trim();
	if (ymd==""){
		$("#"+el_id).attr("value","");
		return false;
	}
	ymd=format_date(ymd);
	if (ymd==""){
		//Format dates for ISO standard.
		window.alert('Invalid Format - exemple [2008-12-31] !');
		$("#"+el_id).attr("value","");
		return false;
	}
	$("#"+el_id).attr("value",ymd);
}
/*
日期对象格式化日期
el_id:对象id
*/
function format_date_el(el_id){
	var value=$("#"+el_id).attr("value").trim();
	$("#"+el_id).attr("value",format_date(value));
}
/*
格式化日期,以yyyy-mm-dd格式传回
value:被格式化的内容，可以是 Date 对象
*/
function format_date(value){
	if (typeof(value)!='string'){
		try{
			value=value.getFullYear()+'-'+(value.getMonth()+1)+'-'+value.getDate();
		}catch(e){
			try{
				value=value.trim();
			}catch(e){
				value='';
			}
		}
	}
	value=value.trim();if (value==""){return '';}
	var ymd_arr=value.split('-');
	var y=parseInt(ymd_arr[0],10);
	var m=parseInt(ymd_arr[1],10);
	var d=parseInt(ymd_arr[2],10);
	if (isNaN(y) || isNaN(m) || isNaN(d)){return '';}
	var date = new Date(y, m-1, d);
	y=date.getFullYear();m=date.getMonth()+1;d=date.getDate();
	if (m<10){m='0'+m;}
	if (d<10){d='0'+d;}
	return y+"-"+m+"-"+d;
}

/*
类库内部调用函数
功能：过滤数字对象的输入字符
*/
function _number_keypress(el_id,e){
	var keyRE="";
	
	//测试是否充许输入负数
	var allow_negative=''+$('#'+el_id).attr('allow_negative');
	if (allow_negative.toLowerCase()!="true"){keyRE=/[0-9\.]/ ;}else{keyRE=/[0-9-\.]/ ;}

	var keyTest=_valKey(keyRE, e);
	if (keyTest==0){return false;}
	if (keyTest==-1 || keyTest==2){return true;}
	var keyTest=_valKey(/\./, e);
	if (keyTest==1){
		var count=0;
		var a=$("#"+el_id).attr("value").trim().split("");
		for (var i=0;i<a.length;i++){if (a[i]=='.'){count++;if (count>0){return false;}}}
	}

	//测试是否充许输入负数
	var allow_negative=''+$('#'+el_id).attr('allow_negative');
	if (allow_negative.toLowerCase()!="true"){return true;}
	
	var keyTest=_valKey(/-/, e);
	if (keyTest==1){	//输入的是负号
		var count=0;
		var a=$("#"+el_id).attr("value").trim().split("");
		for (var i=0;i<a.length;i++){if (a[i]=='-'){count++;if (count>0){return false;}}}
		
		document.getElementById(el_id).value="-"+document.getElementById(el_id).value;
		return false;		//输入框里没有负号，加一个负号在数的前面
	}
	
	return true;
}
/*
类库内部调用函数
功能：数字对象失去焦点后格式化数字
*/
function _number_blur(el_id,e){
	format_number_el(el_id);
}
/*
数字对象格式化数字
el_id:对象id
*/
function format_number_el(el_id){
	var value=$("#"+el_id).attr("value").trim();
	value=parseFloat(value);if (isNaN(value)){value=0;}
	var decimal=$("#"+el_id).attr("decimal").trim();
	decimal=parseInt(decimal,10);if (isNaN(decimal)){decimal=0;}
	$("#"+el_id).attr("value",format_number(value,decimal));
}
/*
格式化数字,传回格式: 
value=56.123456 decimal=5 则传回 56.12346
value=56.12 		decimal=5 则传回 56.12000
*@param value 表示要格式化的数
*@param decimal 要保留的位数
*/
function format_number(value,decimal){
	negative="";
	value=trim(""+value+"");
	
	var a=value.split("");
	for (var i=0;i<a.length;i++){if (a[i]=='-'){negative="-";a[i]="";}}
	value=a.join("");
	
	value=parseFloat(""+value+"");if (isNaN(value)){value=0;}
	decimal=parseFloat(""+decimal+"");if (isNaN(decimal)){decimal=0;}
	var n_ten=1;for(var i=0;i<decimal;i++){n_ten=n_ten*10;}
	value=""+Math.round(parseFloat(value)*n_ten)+"";
	var zero_len=decimal-value.length+1;for(var i=0;i<zero_len;i++){value='0'+value;}
	var number_value=value.substr(0,value.length-decimal);
	if (decimal>0){number_value=number_value+"."+value.substr(value.length-decimal);}
	return negative+""+number_value;
}


// WS - moved below from login\mainpage.php
function height_width(){
	var hw=new Array();
	var browser_height=0;
	var browser_width=0;
	hw["height"]=$(window).height()*0.88;
	hw["width"]=$(window).width()*0.88;

	var loop_length=(hw["height"]>hw["width"])?hw["height"]:hw["width"];
	for(var loop_step=0;loop_step<loop_length;loop_step++){
		browser_height=Math.round(loop_step*5);
		browser_width=Math.round(loop_step*5/0.618);
		if (browser_height>hw["height"] || browser_width>hw["width"]){
			hw["height"]=browser_height;
			hw["width"]=browser_width;
			break;
		}
	}
	
	if (hw["width"]<710){	//710是broswer里的cancel按钮width+left(px)
		hw["width"]=710;
		if (hw["width"]>$(window).width()*0.98){hw["width"]=$(window).width()*0.98;}
		
		hw["height"]=hw["width"]*0.618;
		if (hw["height"]>$(window).height()*0.98){hw["height"]=$(window).height()*0.98;}
	}
	
	return hw;
}
//==========================================================================
function show_browser(url){
	var hw=height_width();
	browser_height	=hw["height"];
	browser_width	=hw["width"];
	
	url=url+
		"&browser_height="+browser_height+
		"&browser_width="+browser_width;
	$.showAkModal(url,'Data Browser',browser_width,browser_height,'no');return false;
}
function close_browser(){$('#close').click();return false;}
//==========================================================================

//==========================================================================
function show_d230c01e08285d10e0c26984588f7e0_return_handler_info(return_message){
	//window.alert(return_message);	//显示所有信息供测试时查看
	if (script_timeout(return_message)){return false};
	
	var ret_msg_arr=return_message.split("|");
	if (ret_msg_arr.length<5){window.alert(return_message);return false;}
	for (var i=0;i<ret_msg_arr.length;i++){ret_msg_arr[i]=un_coding_str(ret_msg_arr[i]);}
	var opt_action=ret_msg_arr[1];
	var msg_code=ret_msg_arr[2];
	var msg_detail=ret_msg_arr[3];
	var msg_script=ret_msg_arr[4];
	switch(opt_action){
		case "report":
			if (msg_detail!==""){window.alert(msg_detail);}
			if (msg_script!==""){
				eval("var url="+msg_script);
				window.open(url);
			};
			break;
		case "enquiry":
			if (msg_detail!==""){window.alert(msg_detail);}
			if (msg_script!==""){
				eval("var url="+msg_script);

				var hw=height_width();
				enquiry_height	=hw["height"];
				enquiry_width	=hw["width"];
				
				url=url+
					"&enquiry_height="+enquiry_height+
					"&enquiry_width="+enquiry_width;
				$.showAkModal(url,'Data Enquiry',enquiry_width,enquiry_height,'auto');return false;
			};
			break;
		default:
			break;
	}
}
function show_d230c01e08285d10e0c26984588f7e0(opt_action,extra_para,extra_value){
	var action_page="report.php";
	action_page_dir_bak=document.getElementById("action_page_dir").value;
	document.getElementById("action_page_dir").value="stdlib";
	var url=get_url_parameter(action_page,opt_action,object_id_list);
	document.getElementById("action_page_dir").value=action_page_dir_bak;

	var extra_url="";
	if ((typeof(extra_para)=='object') && extra_para.constructor==Array){
		for (var key_index=0; key_index<extra_para.length; key_index++){
			if (trim(extra_para[key_index])!=""){
				extra_url=extra_url+((extra_url!="")?("&"):(""))+
					trim(extra_para[key_index])+"="+url_escape(extra_value[key_index]);
			}
		}
	}else{
		if (typeof(extra_para)!="undefined"){
			if (trim(extra_para)!=""){extra_url=trim(extra_para)+"="+url_escape(extra_value);}
		}
	}

	url=url+"&"+extra_url;
	var handler=new net.content_loader(url,show_d230c01e08285d10e0c26984588f7e0_return_handler_info);
}
//==========================================================================

//==========================================================================
function show_report(extra_para,extra_value){
	show_d230c01e08285d10e0c26984588f7e0("report",extra_para,extra_value);
}
function show_enquiry(extra_para,extra_value){
	show_d230c01e08285d10e0c26984588f7e0("enquiry",extra_para,extra_value);
}
//==========================================================================
