<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or LGPL Version 2.1 (LGPLv2.1-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>
<?php
datepicker_html_heading();
?>
<div id="div_datepicker_border" name="div_datepicker_border" class="div_datepicker_border">
	<table name="tab_head_one" id="tab_head_one" cellpadding="0" cellspacing="0" 
		border="0" class="tab_head_one">
	<tr>
		<td width="12%">
			<input type="button" class="btn_normal" id="btn_year_left" name="btn_year_left" value="<"
					onclick="javascript:btn_year_left_click();" />
		</td>
		<td width="26%">
			<select class="sel_normal" id="sel_year" name="sel_year" onchange="javascript:sel_year_change();">
			</select>
		</td>
		<td width="12%">
			<input type="button" class="btn_normal" id="btn_year_right" name="btn_year_right" value=">"
					onclick="javascript:btn_year_right_click();" />
		</td>
		<td width="12%">
			<input type="button" class="btn_normal" id="btn_month_left" name="btn_month_left" value="<"
					onclick="javascript:btn_month_left_click();" />
		</td>
		<td width="26%">
			<select class="sel_normal" id="sel_month" name="sel_month" onchange="javascript:sel_month_change();">
			</select>
		</td>
		<td width="12%">
			<input type="button" class="btn_normal" id="btn_month_right" name="btn_month_right" value=">"
					onclick="javascript:btn_month_right_click();" />
		</td>
	</tr>
	</table>
	<div id="div_detail_grid" name="div_detail_grid" class="div_detail_grid"></div>
	<table name="tab_foot_one" id="tab_foot_one" cellpadding="0" cellspacing="0" 
		border="0" class="tab_foot_one">
	<tr>
		<td width="52%">
			<div id="lbl_today_is" class="lbl_normal">Today is:</div>
		</td>
		<td width="24%">
			<input type="button" class="btn_normal" id="btn_to_today" name="btn_to_today" value="Today"
					onclick="javascript:btn_to_today_click();" />
		</td>
		<td width="24%">
			<input type="button" class="btn_normal" id="btn_close" name="btn_close" value="Close"
					onclick="javascript:btn_close_click();" />
		</td>
	</tr>
	</table>
</div>
<?php $odd_bgColor="#ffffff";$even_bgColor="#eeeeee"; ?>
<?php $original_date_bgColor="#B5DAFF";$mouseover_bgColor="#B5DA00"; ?>
<script language="javascript">
var odd_bgColor="<?php echo $odd_bgColor; ?>";
var even_bgColor="<?php echo $even_bgColor; ?>";
var original_date_bgColor="<?php echo $original_date_bgColor; ?>";
var mouseover_bgColor="<?php echo $mouseover_bgColor; ?>";
var mouseover_td_original_bgColor=null;
function td_onmouseover(row,col){
	td=document.getElementById("tab_detail_grid").rows[row].cells[col];
	mouseover_td_original_bgColor=td.bgColor;
	td.bgColor=mouseover_bgColor;
}
function td_onmouseout(row,col){
	td=document.getElementById("tab_detail_grid").rows[row].cells[col];
	td.bgColor=mouseover_td_original_bgColor;
}
function td_onclick(cell){
	var el_id="";
	var return_arr=return_list.split("|");
	for(var key_index=0;key_index<return_arr.length;key_index++){
		el_id=return_arr[key_index].trim();
		if (el_id!=""){
			self.parent.document.getElementById(el_id).value=format_date(theDays[cell]);
		}
	}
	btn_close_click();
}

var begin_year=2000;var end_year=2030;
var theYears_value=new Array();
var theYears=new Array();
var theMonths_value=new Array(0,1,2,3,4,5,6,7,8,9,10,11);
var theMonths=new Array("1-January","2-Feburary","3-March","4-April","5-May","6-June",
												"7-July","8-August","9-September","10-October","11-November","12-December");
var theWeeks_title=new Array("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday");
var theWeeks=new Array("Sun","Mon","Tue","Wed","Thu","Fri","Sat");
var theDays= new Array(42);
var default_date="";

var div_date="";
var ifra_date="";
var original_date="";
var return_list="";
var timeoutID=0;

function init_data_array(){
	var y=default_date.getFullYear();	var m=default_date.getMonth(); var d=default_date.getDate();
	//0 = 星期天,1 = 星期一,2 = 星期二,3 = 星期三,4 = 星期四,5 = 星期五,6 = 星期六
	var first_day_week=(new Date(y,m,1)).getDay();
	for(var i=0;i<42;i++){theDays[i]=new Date(y,m,1+i-first_day_week);}
	for(var i=0;i<=end_year-begin_year;i++){theYears_value[i]=i+begin_year;theYears[i]=i+begin_year;}
	
	remove_select_list("sel_year","");
	add_select_item("sel_year",theYears_value,theYears);
	set_select_selected("sel_year",""+y+"");
	remove_select_list("sel_month","");
	add_select_item("sel_month",theMonths_value,theMonths);
	set_select_selected("sel_month",""+m+"");
}

function draw_detail_grid(){
	var tab_detail_grid="";
	tab_detail_grid=
		'<table id="tab_detail_grid" name="tab_detail_grid" '+
		'cellpadding="0" cellspacing="0" border="0" style="width:100%;">'+
		'<tr class="grid_title_tr">';
	for(var j=0;j<theWeeks.length;j++){
		tab_detail_grid=tab_detail_grid+'<td noWrap align="center" class="grid_title_td" '+
			'title="'+theWeeks_title[j]+'">'+theWeeks[j]+'</td>';
	}
	tab_detail_grid=tab_detail_grid+'</tr>';

	for(var i=0;i<6;i++){
		tab_detail_grid=tab_detail_grid+'<tr class="grid_body_tr">';
		for(var j=i*7;j<i*7+7;j++){
			td_data=theDays[j].getDate();
			if (default_date.getMonth()==theDays[j].getMonth()){
				td_data='<font color="#0000ff"><b>'+td_data+'</b></font>';
			}
			o_d_b=false;
			if (original_date.getFullYear()==theDays[j].getFullYear() && 
					original_date.getMonth()==theDays[j].getMonth() && 
					original_date.getDate()==theDays[j].getDate()){o_d_b=true;}
			tab_detail_grid=tab_detail_grid+'<td noWrap align="center" class="grid_body_td" '+
				'bgColor="'+(o_d_b?original_date_bgColor:(((i+1)&1)?odd_bgColor:even_bgColor))+'" '+
				'onmouseover="td_onmouseover('+(i+1)+','+(j-i*7)+');" '+
				'onmouseout="td_onmouseout('+(i+1)+','+(j-i*7)+');" '+
				'onclick="td_onclick('+(j)+');" >'+
				td_data+'</td>';
		}
		tab_detail_grid=tab_detail_grid+'</tr>';
	}
	tab_detail_grid=tab_detail_grid+'</table>';
	document.getElementById('div_detail_grid').innerHTML=tab_detail_grid;
}

function btn_year_left_click(){
	var y=default_date.getFullYear()-1;
	var m=default_date.getMonth();
	var d=1;
	if (y<begin_year){return false;}
	default_date=new Date(y,m,d);init_data_array();draw_detail_grid();
}
function sel_year_change(){
	var y=document.getElementById('sel_year').value;
	var m=default_date.getMonth();
	var d=1;
	default_date=new Date(y,m,d);init_data_array();draw_detail_grid();
}
function btn_year_right_click(){
	var y=default_date.getFullYear()+1;
	var m=default_date.getMonth();
	var d=1;
	if (y>end_year){return false;}
	default_date=new Date(y,m,d);init_data_array();draw_detail_grid();
}


function btn_month_left_click(){
	var y=default_date.getFullYear();
	var m=default_date.getMonth()-1;
	var d=1;
	if ((new Date(y,m,d)).getFullYear()<begin_year){return false;}
	default_date=new Date(y,m,d);init_data_array();draw_detail_grid();
}
function sel_month_change(){
	var y=default_date.getFullYear();
	var m=document.getElementById('sel_month').value;
	var d=1;
	default_date=new Date(y,m,d);init_data_array();draw_detail_grid();
}
function btn_month_right_click(){
	var y=default_date.getFullYear();
	var m=default_date.getMonth()+1;
	var d=1;
	if ((new Date(y,m,d)).getFullYear()>end_year){return false;}
	default_date=new Date(y,m,d);init_data_array();draw_detail_grid();
}

function btn_to_today_click(){
	default_date=new Date();original_date=new Date();init_data_array();draw_detail_grid();
}
function btn_close_click(){self.parent._hidden_date_picker(div_date);}
function _show_date_picker(){self.parent._show_date_picker(div_date);}

function set_calendar_parameter(div_date_el_id,ifra_date_el_id,original_date_date,return_list_el_id){
	div_date=div_date_el_id;
	ifra_date=ifra_date_el_id;
	original_date=original_date_date;
	return_list=return_list_el_id;
	original_date=format_date(original_date);
	if (original_date!=""){
		ymd_arr=original_date.split('-');
		y=parseInt(ymd_arr[0],10);
		m=parseInt(ymd_arr[1],10);
		d=parseInt(ymd_arr[2],10);
		original_date=new Date(y,m-1,d);
		if (original_date.getFullYear()>end_year || original_date.getFullYear()<begin_year){
			original_date=new Date();
		}
	}else{original_date=new Date();}
	default_date=original_date;
	document.getElementById('lbl_today_is').innerHTML="Today is:"+theWeeks_title[(new Date()).getDay()];
	
	init_data_array();draw_detail_grid();
	window.clearTimeout(timeoutID);
	timeoutID=window.setTimeout("_show_date_picker();", 200,"javascript");
}

</script>
<?php
datepicker_html_footer();
?>