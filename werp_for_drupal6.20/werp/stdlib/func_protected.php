<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or LGPL Version 2.1 (LGPLv2.1-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');

function no_cache_for_static_page_file(){
	return "?version=".md5(uniqid(rand()));		//程序设计时，为了能让静态页面文件每次刷新
	return "?version=".md5(WERP_VERSION);	//项目完成了
}

function _init_werp_uri_for_javascript(){
	return '
		<script language="javascript">
			var WERP_BASE_URI="'.WERP_BASE_URI.'";
			var WERP_FOLL_URI="'.WERP_FOLL_URI.'";
			var WERP_SITE_URI="'.WERP_SITE_URI.'";
			var WERP_EXTE_LANG="'.WERP_EXTE_LANG.'";
			var WERP_EXPA_PARA="'.WERP_EXPA_PARA.'";
			var WEL_DEVELOP_INFO="'.WEL_DEVELOP_INFO.'";
		</script>
		';
}

function html_heading(){
	$js_base=WERP_SITE_URI.'script/';
	$new_client_version=no_cache_for_static_page_file();

	$strResult="<html><head>\n".
		"<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n".
		_init_werp_uri_for_javascript().
		"<script type=\"text/javascript\" src=\"".$js_base."jquery-1.3.2.min.js".$new_client_version."\"></script>\n".
		"<link type=\"text/css\" rel=\"stylesheet\" href=\"".$js_base."style.css".$new_client_version."\" />\n".
		"<script type=\"text/javascript\" src=\"".$js_base."akModal_1.1/dimmer.js".$new_client_version."\"></script>\n".
		"<script type=\"text/javascript\" src=\"".$js_base."akModal_1.1/dimensions.pack.js".$new_client_version."\"></script>\n".
		"<script type=\"text/javascript\" src=\"".$js_base."akModal_1.1/akModal.js".$new_client_version."\"></script>\n".
		"<script type=\"text/javascript\" src=\"".$js_base."stdlib.js".$new_client_version."\"></script>\n".

		"<link rel=\"stylesheet\" href=\"".$js_base."tabs/flora.tabs.css".$new_client_version."\" ".
		"type=\"text/css\" media=\"screen\" title=\"Flora (Default)\">\n".
		"<script type=\"text/javascript\" src=\"".$js_base."tabs/ui.core.js".$new_client_version."\"></script>\n".
		"<script type=\"text/javascript\" src=\"".$js_base."tabs/ui.tabs.js".$new_client_version."\"></script>\n".
		"<title></title>\n".
		"</head>\n".
		"<body bottommargin=\"0\" topmargin=\"0\" leftmargin=\"0\" rightmargin=\"0\"  
			scroll=\"no\" style=\"overflow:scroll;overflow-x:hidden;overflow-y:hidden;\">\n";
	echo $strResult;
}

function frame_html_heading(){
	$js_base=WERP_SITE_URI.'script/';
	$new_client_version=no_cache_for_static_page_file();
	
	$strResult="<html><head>\n".
		"<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n".
		_init_werp_uri_for_javascript().
		"<script type=\"text/javascript\" src=\"".$js_base."jquery-1.3.2.min.js".$new_client_version."\"></script>\n".
		"<link type=\"text/css\" rel=\"stylesheet\" href=\"".$js_base."style.css".$new_client_version."\" />\n".
		"<script type=\"text/javascript\" src=\"".$js_base."stdlib.js".$new_client_version."\"></script>\n".
		"<title></title>\n".
		"</head>\n".
		"<body bottommargin=\"0\" topmargin=\"0\" leftmargin=\"0\" rightmargin=\"0\"  
			scroll=\"no\" style=\"overflow:scroll;overflow-x:hidden;overflow-y:hidden;\">\n";
	echo $strResult;
}

function tree_html_heading(){
	$js_base=WERP_SITE_URI.'script/';
	$new_client_version=no_cache_for_static_page_file();
	
	$strResult="<html><head>\n".
		"<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n".
		_init_werp_uri_for_javascript().
		"<script type=\"text/javascript\" src=\"".$js_base."jquery-1.3.2.min.js".$new_client_version."\"></script>\n".
		"<link type=\"text/css\" rel=\"stylesheet\" href=\"".$js_base."style.css".$new_client_version."\" />\n".
		"<script type=\"text/javascript\" src=\"".$js_base."stdlib.js".$new_client_version."\"></script>\n".
		"<link type=\"text/css\" rel=\"stylesheet\" href=\"".
			$js_base."simpleTreeD_D/js/jquery.simple.tree.css".$new_client_version."\" />\n".
		"<script type=\"text/javascript\" src=\"".
			$js_base."simpleTreeD_D/js/jquery.simple.tree.js".$new_client_version."\"></script>\n".
		"<title></title>\n".
		"</head>\n".
		"<body bottommargin=\"0\" topmargin=\"0\" leftmargin=\"0\" rightmargin=\"0\"  
			scroll=\"no\" style=\"overflow:scroll;overflow-x:hidden;overflow-y:hidden;\">\n";
	echo $strResult;
}

function detail_html_heading(){
	$js_base=WERP_SITE_URI.'script/';
	$new_client_version=no_cache_for_static_page_file();
	
	$strResult="<html><head>\n".
		"<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n".
		_init_werp_uri_for_javascript().
		"<script type=\"text/javascript\" src=\"".$js_base."jquery-1.3.2.min.js".$new_client_version."\"></script>\n".
		"<link type=\"text/css\" rel=\"stylesheet\" href=\"".$js_base."detailgrid.css".$new_client_version."\" />\n".
		"<script type=\"text/javascript\" src=\"".$js_base."stdlib.js".$new_client_version."\"></script>\n".
		"<title></title>\n".
		"</head>\n".
		"<body bottommargin=\"0\" topmargin=\"0\" leftmargin=\"0\" rightmargin=\"0\"  
			scroll=\"no\" style=\"overflow:scroll;overflow-x:hidden;overflow-y:hidden;\">\n";
	echo $strResult;
}

function browser_html_heading(){
	$js_base=WERP_SITE_URI.'script/';
	$new_client_version=no_cache_for_static_page_file();
	
	$strResult="<html><head>\n".
		"<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n".
		_init_werp_uri_for_javascript().
		"<script type=\"text/javascript\" src=\"".$js_base."jquery-1.3.2.min.js".$new_client_version."\"></script>\n".
		"<link type=\"text/css\" rel=\"stylesheet\" href=\"".$js_base."browser.css".$new_client_version."\" />\n".
		"<script type=\"text/javascript\" src=\"".$js_base."stdlib.js".$new_client_version."\"></script>\n".
		"<title></title>\n".
		"</head>\n".
		"<body bottommargin=\"0\" topmargin=\"0\" leftmargin=\"0\" rightmargin=\"0\"  
			scroll=\"no\" style=\"overflow:scroll;overflow-x:hidden;overflow-y:hidden;\">\n";
	echo $strResult;
}

function datepicker_html_heading(){
	$js_base=WERP_SITE_URI.'script/';
	$new_client_version=no_cache_for_static_page_file();
	
	$strResult="<html><head>\n".
		"<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n".
		_init_werp_uri_for_javascript().
		"<link type=\"text/css\" rel=\"stylesheet\" href=\"".$js_base."datepicker.css".$new_client_version."\" />\n".
		"<script type=\"text/javascript\" src=\"".$js_base."stdlib.js".$new_client_version."\"></script>\n".
		"<title></title>\n".
		"</head>\n".
		"<body bottommargin=\"0\" topmargin=\"0\" leftmargin=\"0\" rightmargin=\"0\"  
			scroll=\"no\" style=\"overflow:scroll;overflow-x:hidden;overflow-y:hidden;\">\n";
	echo $strResult;
}

function html_footer(){
	$strResult="</body>\n</html>\n";
	echo $strResult;
}

function frame_html_footer(){
	$strResult="</body>\n</html>\n";
	echo $strResult;
}

function tree_html_footer(){
	$strResult="</body>\n</html>\n";
	echo $strResult;
}

function detail_html_footer(){
	$strResult="</body>\n</html>\n";
	echo $strResult;
}

function browser_html_footer(){
	$strResult="</body>\n</html>\n";
	echo $strResult;
}

function datepicker_html_footer(){
	$strResult="</body>\n</html>\n";
	echo $strResult;
}

function coding_str($str){
	return str_replace("|","%VERTICAL",str_replace("%","%PERCENT",$str));
}
function un_coding_str($str){
	return str_replace("%PERCENT","%",str_replace("%VERTICAL","|",$str));
}

function werp_get_request_var($para_name){
	$return_val=trim($_GET[$para_name]);
	if (array_key_exists($para_name,$_POST)){$return_val=trim($_POST[$para_name]);}
	if (!get_magic_quotes_gpc()){$return_val=addslashes($return_val);}
	return $return_val;
}


class compare_different_object{
	private $caller_file;
	private $parser;
	private $xml_array=array();
	private $lay_array=array();
	
	private $xml_object="xml_object";
	private $lay_object="lay_object";
	private $parser_kind="";
	private $hidden_object_html="";

	function __construct($_caller_file){
		$this->caller_file=$_caller_file;
	}
	function __destruct(){
	}
	
	private function startElement($parser_instance, $element_name, $attrs){     //起始标签事件的函数
		$object_id="";
		$object_whole="";
		switch (strtolower($element_name)){
			case "textarea":
			case "select":
			case "input":
				if ($attrs!=null){
					$object_whole=$object_whole."<".strtolower($element_name)." ";
					foreach ($attrs as $key=>$value){
						$object_whole=$object_whole." ".strtolower($key)."=\"$value\"";
						if (strtolower($key)=="id"){
							$object_id=strtolower($value);
						}
					}
					if (strtolower($element_name)=="input"){
						$object_whole=$object_whole." />";
					}
					else{
						$object_whole=$object_whole."></".strtolower($element_name).">";
					}
					if ($object_id!=null and $object_id!=""){
						if ($this->parser_kind==$this->xml_object){
							$this->xml_array[$object_id]=$object_whole;
						}
						elseif ($this->parser_kind==$this->lay_object){
							$this->lay_array[$object_id]=$object_whole;
						}
					}
				}
				break;
			default:
				break;
		}
	}

	private function characterData($parser_instance, $XML_data){                //读取数据时的函数 
	}
	
	private function endElement($parser_instance, $element_name){               //结束标签事件的函数
	}
	
	public function get_different_object(){
		$this->parser = xml_parser_create("UTF-8"); //创建一个parser编辑器
		//pool xml_set_object ( resource $parser , object &$object )
		//该函数使得 parser 指定的解析器可以被用在 object 对象中。
		//所有的回叫函数（callback function）
		//都可以由 xml_set_element_handler() 等函数来设置，它们被假定为 object 对象的方法。
		xml_set_object($this->parser, $this);
		//设立标签触发时的相应函数 这里分别为startElement和endElenment
		xml_set_element_handler($this->parser, "startElement", "endElement");
		//设立数据读取时的相应函数
		xml_set_character_data_handler($this->parser, "characterData");
		
		$this->parser_kind=$this->xml_object;
		//指定所要读取的XML文件,可以是url
		$xml_file=get_xml_file(WERP_SITE_PATH_TEMPLATE,werp_pathinfo_filename($this->caller_file));
		$filehandler = fopen($xml_file, "r");//打开文件
		while ($data = fread($filehandler, 4096)){
			if (!xml_parse($this->parser, $data, feof($filehandler))){
				//报错
				die(sprintf("XML error: %s at line %d",
				xml_error_string(xml_get_error_code($this->parser)),
				xml_get_current_line_number($this->parser)));
			}
		}//每次取出4096个字节进行处理
		fclose($filehandler);
		xml_parser_free($this->parser);//关闭和释放parser解析器	
		
		
		$this->parser = xml_parser_create("UTF-8"); //创建一个parser编辑器
		//pool xml_set_object ( resource $parser , object &$object )
		//该函数使得 parser 指定的解析器可以被用在 object 对象中。
		//所有的回叫函数（callback function）
		//都可以由 xml_set_element_handler() 等函数来设置，它们被假定为 object 对象的方法。
		xml_set_object($this->parser, $this);
		//设立标签触发时的相应函数 这里分别为startElement和endElenment
		xml_set_element_handler($this->parser, "startElement", "endElement");
		//设立数据读取时的相应函数
		xml_set_character_data_handler($this->parser, "characterData");
		
		$this->parser_kind=$this->lay_object;
		//指定所要读取的XML文件,可以是url
		$lay_xml_file=get_lay_xml_file(WERP_SITE_PATH_TEMPLATE,werp_pathinfo_filename($this->caller_file));
		$filehandler = fopen($lay_xml_file, "r");//打开文件
		while ($data = fread($filehandler, 4096)){
			if (!xml_parse($this->parser, $data, feof($filehandler))){
				//报错
				die(sprintf("XML error: %s at line %d",
				xml_error_string(xml_get_error_code($this->parser)),
				xml_get_current_line_number($this->parser)));
			}
		}//每次取出4096个字节进行处理
		fclose($filehandler);
		xml_parser_free($this->parser);//关闭和释放parser解析器	
		
		//添加3个隐藏的对象,用来承放当前页面&活动面页的名字和面页所在的目录(相对于根目录)
		$this->xml_array["main_page"]=
			"<input type=\"text\" id=\"main_page\" name=\"main_page\" value=\"".
			werp_pathinfo_filename($this->caller_file)."\" />";
		$this->xml_array["action_page"]=
			"<input type=\"text\" id=\"action_page\" name=\"action_page\" value=\"".
			werp_pathinfo_filename($this->caller_file)."\" />";
		$this->xml_array["main_page_dir"]=
			"<input type=\"text\" id=\"main_page_dir\" name=\"main_page_dir\" value=\"".
			get_page_directory($this->caller_file)."\" />";
		$this->xml_array["action_page_dir"]=
			"<input type=\"text\" id=\"action_page_dir\" name=\"action_page_dir\" value=\"".
			get_page_directory($this->caller_file)."\" />";
		
		$object_id_list="";
		$hidden_object_id_list="";
		$only_lay_object_id_list="";
		$this->hidden_object_html="";
		$hidden_object_id_arr="\$hidden_object_id_arr=array();\n";

		if (is_array($this->xml_array) and is_array($this->lay_array)){
			$object_id_list=implode("|",array_keys($this->xml_array));
			
			$temp_array=$this->lay_array;
			foreach ($temp_array as $key=>$value){
				if (array_key_exists($key,$this->xml_array)){
					unset($this->xml_array[$key]);		//删除元素
					unset($this->lay_array[$key]);		//删除元素
				}
			}
			$hidden_object_id_list=implode("|",array_keys($this->xml_array));
			$only_lay_object_id_list=implode("|",array_keys($this->lay_array));
			$this->hidden_object_html="<div style=\"display:none\">\n".
				implode("\n",$this->xml_array)."\n</div>\n";
			foreach ($this->xml_array as $key=>$value){
				$hidden_object_id_arr .="\$hidden_object_id_arr[\"$key\"]=true;\n";
			}
		}

		$return_val=array();
		$return_val["hidden_object_html"]=$this->hidden_object_html;
		$return_val["hidden_object_id_list"]=$hidden_object_id_list;
		$return_val["hidden_object_id_arr"]=$hidden_object_id_arr;
		$return_val["object_id_list"]=$object_id_list;
		$return_val["only_lay_object_id_list"]=$only_lay_object_id_list;
		return $return_val;
	}
}
			
class rebuild_layout_body {
	private $caller_file;
	private $parser;
	private $part_tag;
	
	private $label_message="label_message";
	private $label_normal="label_normal";
	private $label_sql="label_sql";
	private $sql_sentence="sql_sentence";
	private $body_detail="body_detail";
	private $draw_detail="draw_detail";
	private $format_date="format_date";
	private $format_number="format_number";
	private $blur_data="blur_data";
	private $page_title="page_title";
	private $page_script="page_script";
	
	private $extract_message_script="";
	private $label_message_script="";
	private $label_normal_script="";
	private $label_sql_script="";
	private $blur_sql_script="";
	private $body_detail_html="";
	private $draw_detail_script="";
	private $page_title_html="";
	
	private $browser_click_script="";
	private $browser_return_script="";
	
	private $blur_blur_script="";
	private $blur_return_script="";
	private $page_title_script="";
	private $page_script_script="";
	
	private $div_date="";
	private $ifra_date="";
	private $XML_data="";
	
	function __construct($_caller_file){
		 $this->label_sql_script=
			"\$label_sql_d230c01e08285d10e0c26984588f7e0=array();\n".
			"\$sql_sentence_d230c01e08285d10e0c26984588f7e0=array();\n";
		 $this->blur_sql_script="\$blur_sql_d230c01e08285d10e0c26984588f7e0=array();\n";
		$this->caller_file=$_caller_file;
	}
	function __destruct(){
	}
	
	private function startElement($parser_instance, $element_name, $attrs){     //起始标签事件的函数
		switch ($this->part_tag){
			case $this->label_message:
			case $this->label_normal:
			case $this->page_title:
				$slabel="";
				if ($attrs!=null){
					foreach ($attrs as $key=>$value){
						$slabel=str_replace('$','\$',str_replace('"','\"',$value));
						if ($this->part_tag==$this->page_title){	//这个标签特别对待
							if ($slabel!=""){
								$wel_prog_code=werp_pathinfo_filename($this->caller_file);
								$slabel=str_replace('$','\$',str_replace('"','\"',$value))."(".$wel_prog_code.")";
							}
						}
						if (strtolower($key)!="id" && strtolower($key)!="menu"){	//排除这些关键字
							$this->label_message_script .="\$label_message".
								"[\"".strtolower($element_name)."\"][\"".strtolower($key)."\"]=\"$slabel\";\n";
						}
					}
					$this->label_message_script .="\n";
					
					$this->extract_message_script .=
						"case (\"".strtolower($element_name)."\").toUpperCase():\n".
						"return	\"<?php echo ".
						"str_replace('\"','\\\"',extract_message('".strtolower($element_name)."')); ?>\";".
						"break;\n";
						
					if ($this->part_tag==$this->label_normal || $this->part_tag==$this->page_title){
						$this->label_normal_script=$this->label_normal_script.
							"_set_client_object_label('".strtolower($element_name)."',".
							"extract_message('".strtolower($element_name)."'));\n";
					}
					
					if ($this->part_tag==$this->page_title){
						$menu=false;
						foreach ($attrs as $key=>$value){
							if (strtolower($key)=="menu"){
								if (strtolower($value)=="true"){$menu=true;break;}
								break;
							}
						}

						$wel_prog_code=werp_pathinfo_filename($this->caller_file);
						if ($menu){
							foreach ($attrs as $key=>$value){
								if (strtolower($key)=="menu"){
								}else{
									$this->add_page_title_to_wel_progdetm($wel_prog_code,$key,$value);
								}
							}
							$this->add_page_title_to_wel_proghdrm($wel_prog_code);
						}

						$key_id=strtolower($element_name);
						$this->page_title_html="\n".
							"<div name=\"".$key_id."\" id=\"".$key_id."\" class=\"page_title\"></div>\n";
					}
				}
				break;
			case $this->label_normal:	//这个CASE不再调用，由case $this->label_message一同处理
				break;
			case $this->label_sql:
				$slabel="";
				$key_id="";
				if ($attrs!=null){
					foreach ($attrs as $key=>$value){
						if (strtolower($key)=="id"){
							$key_id=$value;		//取得标签数组的第一维下标的名称(以$value为key)
							break;
						}
					}
					if ($key_id!=""){			//只有取得第一维的下标才进创建标签数组
						foreach ($attrs as $key=>$value){
							$slabel=str_replace('$','\$',str_replace('"','\"',$value));
							if (strtolower($key)!="id"){
								$this->label_sql_script=$this->label_sql_script.
									"\$label_sql_d230c01e08285d10e0c26984588f7e0".
									"[\"".strtolower($element_name)."\"]".
									"[\"".strtolower($key_id)."\"]".
									"[\"".strtolower($key)."\"]=\"".$slabel."\";\n";
							}
						}
						$this->label_sql_script=$this->label_sql_script."\n";
					}
				}
				break;
			case $this->page_script:
				$this->XML_data="";
				break;
			case $this->sql_sentence:
				$search_bar="";
				$fix_list="";
				$search_list="";
				$search_col="";
				$return_list="";
				$key_id=strtolower($element_name);
				if ($attrs!=null){
					foreach ($attrs as $key=>$value){
						if (strtolower($key)=="search_bar"){
							$search_bar=strtolower($value);
						}else if (strtolower($key)=="fix_list"){
							$fix_list=strtolower($this->remove_r_n_t_space($value));
						}else if (strtolower($key)=="search_list"){
							$search_list=strtolower($this->remove_r_n_t_space($value));
						}else if (strtolower($key)=="search_col"){
							$search_col=strtolower($this->remove_r_n_t_space($value));
						}else if (strtolower($key)=="return_list"){
							$return_list=strtolower($this->remove_r_n_t_space($value));
						}
					}
				}

				$this->XML_data="";
				$this->label_sql_script=$this->label_sql_script.
					"\$sql_sentence_d230c01e08285d10e0c26984588f7e0[\"".$key_id."\"]=\"";

				$browser_return_function="browser_return_".md5(uniqid(rand()))."";
				$key_id_click_event_function_name=$key_id."_click".md5(uniqid(rand()))."";

				$this->browser_click_script=$this->browser_click_script."
					function $key_id_click_event_function_name (){
						_browser_click_function(
							'$key_id','$search_bar','$fix_list','$search_list','$search_col',
							'$browser_return_function');
					}
					\$(document).ready(function(){
						bind_event('$key_id','click',$key_id_click_event_function_name);
					});
					";

				$this->browser_return_script=$this->browser_return_script."
					function $browser_return_function (rowdata){
						_browser_return_function('$key_id','$return_list',rowdata);
					}
					";
				break;
			case $this->body_detail:
				if (strtolower($element_name)==$this->draw_detail){
					if ($attrs!=null){
						$sql_id="";
						$grid_height="";
						$checkbox_col="";
						foreach ($attrs as $key=>$value){
							if (strtolower($key)=="sql_id"){
								$sql_id=strtolower($value);
							}else if (strtolower($key)=="grid_height"){
								$grid_height=strtolower($value);
							}else if (strtolower($key)=="checkbox_col"){
								$checkbox_col=strtolower($value);
							}
						}
						if ($grid_height==""){$grid_height="200";}
						if (!is_numeric($grid_height)){$grid_height="200";}
		  
						if ($checkbox_col==""){$checkbox_col="-1";}
						if (!is_numeric($checkbox_col)){$checkbox_col="-1";}

						if ($sql_id!=""){
							$this->body_detail_html=$this->body_detail_html.
							"<div style='width: 100%;border:solid 0px #123456;'>".
							"<iframe id=\"$sql_id\" name=\"$sql_id\" ".
							"style=\"width:100%;height:".$grid_height."px;\" ".
							"frameSpacing=\"0\" marginHeight=\"0\" marginWidth=\"0\" ".
							"scrolling=\"no\" frameBorder=\"0\"></iframe></div>";
							$this->draw_detail_script=$this->draw_detail_script."
							function ".$sql_id."_grid(para){
								return _grid('$sql_id',para,'$checkbox_col');
							}\n".
							"function ".$sql_id."_selected_row(){\n".
							" return _selected_row('".$sql_id."');\n".
							"}\n".
							"function ".$sql_id."_selected_del(){\n".
							" return _selected_del('".$sql_id."');\n".
							"}\n".
							"function ".$sql_id."_selected_col(col){\n".
							" return _selected_col('".$sql_id."',col);\n".
							"}\n".
							"function ".$sql_id."_checkbox_array(){\n".
							" return _checkbox_array('".$sql_id."');\n".
							"}\n";
							break;
						}
					}
				}
				else{
					$this->body_detail_html=$this->body_detail_html."<".strtolower($element_name);
					if ($attrs!=null){
						$key_id="";
						$key_class="";
						foreach ($attrs as $key=>$value){
							$this->body_detail_html=$this->body_detail_html." ".strtolower($key)."=\"$value\"";
							if (strtolower($key)=="id"){$key_id=$value;}
							if (strtolower($key)=="class"){$key_class=$value;}
						}
						if ($key_id!="" and $key_class=="flora"){
							$this->draw_detail_script=$this->draw_detail_script."\n".
								"\$(document).ready(function(){".
								"\$(\"#".$key_id." > ul\").tabs();".
								"});\n";
						}
					}
					switch (strtolower($element_name)){
						case "input":
						case "br":
							$this->body_detail_html=$this->body_detail_html." />";
							break;
						default:
							$this->body_detail_html=$this->body_detail_html.">";
							break;
					}
				}
				break;
			case $this->format_date:
				$key_id=strtolower($element_name);
				$picker_id="";
				$return_list="";
				$default_value="";
				if ($attrs!=null){
					foreach ($attrs as $key=>$value){
						if (strtolower($key)=="picker_id"){$picker_id=strtolower($value);}
						if (strtolower($key)=="return_list"){
							$return_list=strtolower($this->remove_r_n_t_space($value));
						}
						if (strtolower($key)=="default_value"){$default_value=trim($value);}
					}
				}
				$picker_id=trim($picker_id);if ($picker_id==""){$picker_id=$key_id;}
				$return_list=trim($return_list);if ($return_list==""){$return_list=$key_id;}

				if ($this->div_date==""){	//日期控件未产生过
					$this->div_date="div_date_".md5(uniqid(rand()))."";
					$this->ifra_date="ifra_date_".md5(uniqid(rand()))."";
					/*
					$this->body_detail_html=$this->body_detail_html.
						"<div id=\"$this->div_date\" name=\"$this->div_date\" \n".
						"style='height:0px;width:0px;border:none;".
						"display:inline;position:absolute;left:0px;top:0px;z-index:-1000;'>\n".
						"<iframe id=\"$this->ifra_date\" name=\"$this->ifra_date\" \n".
						"style=\"width:100%;height:100%;\" \n".
						"frameSpacing=\"0\" marginHeight=\"0\" marginWidth=\"0\" ".
						"scrolling=\"no\" frameBorder=\"0\" src=\"_blank\">\n".
						"</iframe></div>\n";
					*/
					//将日期设置到最上层对象，以免秀出来时被上层对象挡住
					$this->draw_detail_script=$this->draw_detail_script."\n".
						"\$(document).ready(function(){\n".
							"\$(\"body\").append(\"<div id='$this->div_date' name='$this->div_date'  ".
							"style='height:0px;width:0px;border:none;display:inline;position:absolute;".
							"left:0px;top:0px;z-index:-1000;'><iframe id='$this->ifra_date' ".
							"name='$this->ifra_date' style='width:100%;height:100%;' ".
							"frameSpacing='0' marginHeight='0' marginWidth='0' ".
							"scrolling='no' frameBorder='0' src='_blank'></iframe></div>\");\n".
						"});\n";
				}
				
				$this->draw_detail_script=$this->draw_detail_script."\n".
					"\$(document).ready(function(){\n".
					"_init_date_picker('$this->ifra_date');\n".
					"\$('#".$picker_id."').bind('click',function(){\n".
					"	if (to_boolean(\$('#".$picker_id."').attr('readOnly'),false)){return false;}\n".
					"	return _date_picker('".$this->div_date."','".$this->ifra_date."','".
					$key_id."','".$return_list."');\n".
					"});\n".
					"\$('#".$key_id."').bind('dblclick',function(){\n".
					"	if (to_boolean(\$('#".$key_id."').attr('readOnly'),false)){return false;}\n".
					"	return _date_picker('".$this->div_date."','".$this->ifra_date."','".
					$key_id."','".$return_list."');\n".
					"});\n".
					"\$('#".$key_id."').attr('maxlength','10');\n".
					"\$('#".$key_id."').attr('size','12');\n".
					"\$('#".$key_id."').attr('format_date','yyyy-mm-dd');\n".
					"\$('#".$key_id."').bind('keypress',function(e){\n".
					"	return _date_keypress('".$key_id."',e);\n".
					"});\n".
					"\$('#".$key_id."').bind('blur',function(e){\n".
					"	if (to_boolean(\$('#".$key_id."').attr('readOnly'),false)){return false;}\n".
					"	return _date_blur('".$key_id."',e);\n".
					"});\n".
					"//预设客户端当前日期到对象中\n".
					"//$('#".$key_id."').attr('value',format_date(new Date()));\n".
					"});\n";
				
				if ($default_value!=""){
					$default_value_function="default_value_function_".md5(uniqid(rand()))."";
					$this->draw_detail_script=$this->draw_detail_script."\n".
						"\$(document).ready(function(){\n".
						"function $default_value_function(){\n".
						"	try{".$default_value."}catch(error){}finally{}\n".
						"}\n".
						"var default_value=format_date($default_value_function());\n".
						"$('#".$key_id."').attr('value',default_value);\n".
						"$('#".$key_id."').attr('default_value',default_value);\n".
						"});\n";
				}
				
				break;
			case $this->format_number:
				$key_id=strtolower($element_name);
				$length="";
				$decimal="";
				$allow_negative="";
				if ($attrs!=null){
					foreach ($attrs as $key=>$value){
						if (strtolower($key)=="length"){$length=$value;}
						if (strtolower($key)=="decimal"){$decimal=$value;}
						if (strtolower($key)=="allow_negative"){$allow_negative=strtolower($value);}
					}
				}
				if (!is_numeric($length)){$length=16;}if (!is_numeric($decimal)){$decimal=2;}
				if ($length<0 || $length>16){$length=16;}if ($decimal<0 || $decimal>8){$decimal=4;}
				
				$this->draw_detail_script=$this->draw_detail_script."\n".
					"\$(document).ready(function(){\n".
					"\$('#".$key_id."').attr('maxlength','".($length+1)."');\n".
					"\$('#".$key_id."').attr('size','".($length+1+2)."');\n".
					"\$('#".$key_id."').attr('format_number','0.".$decimal."');\n".
					"\$('#".$key_id."').attr('decimal','".$decimal."');\n".
					"\$('#".$key_id."').attr('allow_negative','".$allow_negative."');\n".
					"\$('#".$key_id."').bind('keypress',function(e){\n".
					"	return _number_keypress('".$key_id."',e);\n".
					"});\n".
					"\$('#".$key_id."').bind('blur',function(e){\n".
					"	if (to_boolean(\$('#".$key_id."').attr('readOnly'),false)){return false;}\n".
					"	return _number_blur('".$key_id."',e);\n".
					"});\n".
					"});\n";
				break;
			case $this->blur_data:
				$fix_list="";
				$return_list="";
				$key_id=strtolower($element_name);
				if ($attrs!=null){
					foreach ($attrs as $key=>$value){
						if (strtolower($key)=="fix_list"){
							$fix_list=strtolower($this->remove_r_n_t_space($value));
						}else if (strtolower($key)=="return_list"){
							$return_list=strtolower($this->remove_r_n_t_space($value));
						}
					}
				}

				$blur_return_function="blur_return_".md5(uniqid(rand()))."";
				$key_id_blur_event_function_name=$key_id."_blur".md5(uniqid(rand()))."";
				
				$this->XML_data="";
				$this->blur_sql_script=$this->blur_sql_script.
					"\$blur_sql_d230c01e08285d10e0c26984588f7e0".
					"['".$key_id_blur_event_function_name."']=\"";
				
				$this->blur_blur_script=$this->blur_blur_script."\n".
					"function $key_id_blur_event_function_name(){\n".
					"	if (to_boolean(\$('#".$key_id."').attr('readOnly'),false)){return false;}\n".
					"	_blur_blur_function('".$key_id_blur_event_function_name."','".$fix_list."','".
					$return_list."',".$blur_return_function.");\n".
					"return true;}\$(document).ready(function(){".
					"bind_event('".$key_id."','blur',$key_id_blur_event_function_name);".
					"});\n";

				$this->blur_return_script=$this->blur_return_script."\n".
					"function ".$blur_return_function."(return_script){\n".
					"	_blur_return_function(return_script);\n".
					"}\n";
				break;
			case $this->page_title:	//这个CASE不再调用，由case $this->label_message一同处理
				break;
			default:
				break;
		}

		switch (strtolower($element_name)){
			case strtolower($this->label_message):
				$this->part_tag=$this->label_message;
				break;
			case strtolower($this->label_normal):
				$this->part_tag=$this->label_normal;
				break;
			case strtolower($this->label_sql):
				$this->part_tag=$this->label_sql;
				break;
			case strtolower($this->page_script):
				$this->part_tag=$this->page_script;
				break;
			case strtolower($this->sql_sentence):
				$this->part_tag=$this->sql_sentence;
				break;
			case strtolower($this->body_detail):
				$this->part_tag=$this->body_detail;
				break;
			case strtolower($this->format_date):
				$this->part_tag=$this->format_date;
				break;
			case strtolower($this->format_number):
				$this->part_tag=$this->format_number;
				break;
			case strtolower($this->blur_data):
				$this->part_tag=$this->blur_data;
				break;
			case strtolower($this->page_title):
				$this->part_tag=$this->page_title;
				break;
			default:
				break;
		}
	}
	
	private function characterData($parser_instance, $XML_data){                //读取数据时的函数 
		switch ($this->part_tag){
			case $this->label_message:
				break;
			case $this->label_normal:
				break;
			case $this->label_sql:
				break;
			case $this->page_script:
				$this->XML_data=$this->XML_data.$XML_data;
				break;
			case $this->sql_sentence:
				$this->XML_data=$this->XML_data.$XML_data;
				break;
			case $this->body_detail:
				$this->body_detail_html=$this->body_detail_html.$XML_data;
				break;
			case $this->format_date:
				break;
			case $this->format_number:
				break;
			case $this->blur_data:
				$this->XML_data=$this->XML_data.$XML_data;
				break;
			case $this->page_title:
				break;
			default:
				break;
		}
	}
	
	private function endElement($parser_instance, $element_name){               //结束标签事件的函数
		switch ($this->part_tag){
			case $this->label_message:
				switch (strtolower($element_name)){
					case strtolower($this->label_message):
						$this->part_tag='';
						break;
					default:
						break;
				}
				break;
			case $this->label_normal:
				switch (strtolower($element_name)){
					case strtolower($this->label_normal):
						$this->part_tag='';
						break;
					default:
						break;
				}
				break;
			case $this->label_sql:
				switch (strtolower($element_name)){
					case strtolower($this->label_sql):
						$this->part_tag='';
						break;
					default:
						break;
				}
				break;
			case $this->page_script:
				switch (strtolower($element_name)){
					case strtolower($this->page_script):
						$this->part_tag='';
						break;
					default:
						$this->page_script_script=$this->page_script_script.$this->XML_data;
						break;
				}
				break;
			case $this->sql_sentence:
				switch (strtolower($element_name)){
					case strtolower($this->sql_sentence):
						$this->part_tag='';
						break;
					default:
						$this->XML_data=$this->replace_r_n_t_to_space($this->XML_data);
						$this->XML_data=str_replace('$','\$',str_replace('"','\"',($this->XML_data)));
						$this->label_sql_script=$this->label_sql_script.$this->XML_data."\";\n";
						break;
				}
				break;
			case $this->body_detail:
				switch (strtolower($element_name)){
					case strtolower($this->body_detail):
						$this->part_tag='';
						break;
					case strtolower($this->draw_detail);
						break;
					case "input":
					case "br":
						break;
					default:
						$this->body_detail_html=$this->body_detail_html."</".strtolower($element_name).">";
						break;
				}
				break;
			case $this->format_date:
				switch (strtolower($element_name)){
					case strtolower($this->format_date):
						$this->part_tag='';
						break;
					default:
						break;
				}
				break;
			case $this->format_number:
				switch (strtolower($element_name)){
					case strtolower($this->format_number):
						$this->part_tag='';
						break;
					default:
						break;
				}
				break;
			case $this->blur_data:
				switch (strtolower($element_name)){
					case strtolower($this->blur_data):
						$this->part_tag='';
						break;
					default:
						$this->XML_data=$this->replace_r_n_t_to_space($this->XML_data);
						$this->XML_data=str_replace('$','\$',str_replace('"','\"',($this->XML_data)));
						$this->blur_sql_script=$this->blur_sql_script.$this->XML_data."\";\n";
						break;
				}
				break;
			case $this->page_title:
				switch (strtolower($element_name)){
					case strtolower($this->page_title):
						$this->part_tag='';
						break;
					default:
						break;
				}
				break;
			default:
				break;
		}
	}
	
	private function remove_r_n_t_space($XML_data){
		$XML_data=str_replace("\r\n","",$XML_data);
		$XML_data=str_replace("\r", "", $XML_data);
		$XML_data=str_replace("\n", "", $XML_data);
		$XML_data=str_replace("\t", "", $XML_data);
		$XML_data=str_replace(" ", "", $XML_data);
		return $XML_data;
	}

	private function replace_r_n_t_to_space($XML_data){
		$XML_data=str_replace("\r", "\t", $XML_data);
		$XML_data=str_replace("\n", "\t", $XML_data);
		while (true){
			$_t_pos=stripos($XML_data,"\t\t");
			if (!($_t_pos===false)){$XML_data=str_replace("\t\t", "\t", $XML_data);}else{break;}
		}
		return str_replace("\t", " ", $XML_data);
	}
	
	private function read_label_message($wel_prog_code){
		try{
			$private_label_message=array();
			$conn=werp_db_connect();
			$sql="select * from #__wel_progdetm where wel_prog_code='$wel_prog_code'";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
			while($row=mysql_fetch_array($result)){
				$private_label_message["wel_page_dir"]=$row["wel_page_dir"];
				$private_label_message[$row["wel_language_id"]]=$row["wel_prog_des"];
			}
			return $private_label_message;
		}
		catch (Exception $e){
			echo $e->getMessage();
			$private_label_message=array();
			return $private_label_message;
		}
	}
	
	private function add_page_title_to_wel_proghdrm($wel_prog_code){
		return true;
		
		$wel_prog_code=strtolower($wel_prog_code);
		try{
			$conn=werp_db_connect();
			$sql="select wel_prog_des from #__wel_progdetm where wel_prog_code='$wel_prog_code' and ".
				"wel_language_id='english' limit 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){
				$sql="select wel_prog_des from #__wel_progdetm where wel_prog_code='$wel_prog_code' limit 1";
				$sql=revert_to_the_available_sql($sql);
				if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){
					return false;
				}else{
					$wel_prog_des=$row["wel_prog_des"];
				}
			}else{
				$wel_prog_des=$row["wel_prog_des"];
			}
			
			$wel_prog_des=addslashes($wel_prog_des);
			$sql="select * from #__wel_proghdrm where wel_prog_code='$wel_prog_code' limit 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){
				$sql="insert into #__wel_proghdrm(wel_prog_code,wel_prog_des)values('$wel_prog_code','$wel_prog_des')";
				$sql=revert_to_the_available_sql($sql);
				mysql_query($sql,$conn);
			}else{
				$sql="update #__wel_proghdrm set wel_prog_des='$wel_prog_des' where wel_prog_code='$wel_prog_code'";
				$sql=revert_to_the_available_sql($sql);
				mysql_query($sql,$conn);
				
				//把最新的标签更新到后台管理进行同步管理
				/*
				$sql="update #__wel_werp set title='$wel_prog_des' where alias='$wel_prog_code'";
				$sql=revert_to_the_available_sql($sql);
				mysql_query($sql,$conn);
				*/
			}
		}
		catch (Exception $e){
			echo $e->getMessage();
		}
	}
		
	private function add_page_title_to_wel_progdetm($wel_prog_code,$wel_language_id,$wel_prog_des){
		return true;
		
		$wel_prog_code=strtolower($wel_prog_code);
		$wel_language_id=strtolower($wel_language_id);
		$wel_page_dir=get_page_directory($this->caller_file);
		try{
			$conn=werp_db_connect();
			$wel_prog_des=addslashes($wel_prog_des);
			$sql="select * from #__wel_progdetm where ".
				"wel_prog_code='$wel_prog_code' and wel_language_id='$wel_language_id' limit 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){
				$sql="insert into #__wel_progdetm(wel_prog_code,wel_language_id,wel_prog_des,wel_page_dir)".
					"values('$wel_prog_code','$wel_language_id','$wel_prog_des','$wel_page_dir')";
				$sql=revert_to_the_available_sql($sql);
				if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
			}else{
				$sql="update #__wel_progdetm set".
					" wel_prog_des='$wel_prog_des',wel_page_dir='$wel_page_dir'".
					" where wel_prog_code='$wel_prog_code' and wel_language_id='$wel_language_id'";
				$sql=revert_to_the_available_sql($sql);
				if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
			}
		}
		catch (Exception $e){
			echo $e->getMessage();
		}
	}
	

	public function rebuild_layout_body_inc(){
		
		$this->part_tag = "";
		$this->parser = xml_parser_create("UTF-8"); //创建一个parser编辑器
		//pool xml_set_object ( resource $parser , object &$object )
		//该函数使得 parser 指定的解析器可以被用在 object 对象中。
		//所有的回叫函数（callback function）
		//都可以由 xml_set_element_handler() 等函数来设置，它们被假定为 object 对象的方法。
		xml_set_object($this->parser, $this);
		//设立标签触发时的相应函数 这里分别为startElement和endElenment
		xml_set_element_handler($this->parser, "startElement", "endElement");
		//设立数据读取时的相应函数
		xml_set_character_data_handler($this->parser, "characterData");
		
		//指定所要读取的XML文件,可以是url
		$lay_xml_file=get_lay_xml_file(WERP_SITE_PATH_TEMPLATE,werp_pathinfo_filename($this->caller_file));
		$filehandler = fopen($lay_xml_file, "r");//打开文件
		while ($data = fread($filehandler, 4096)){
			if (!xml_parse($this->parser, $data, feof($filehandler))){
				//报错
				die(sprintf("XML error: %s at line %d",
				xml_error_string(xml_get_error_code($this->parser)),
				xml_get_current_line_number($this->parser)));
			}
		}//每次取出4096个字节进行处理
		fclose($filehandler);
		xml_parser_free($this->parser);//关闭和释放parser解析器	

		$return_val=array();
		$compare_different_object=new compare_different_object($this->caller_file);
		$return_val=$compare_different_object->get_different_object();
		$hidden_object_html=$return_val["hidden_object_html"];
		$hidden_object_id_list=$return_val["hidden_object_id_list"];
		$hidden_object_id_arr=$return_val["hidden_object_id_arr"];
		$object_id_list=$return_val["object_id_list"];
		$only_lay_object_id_list=$return_val["only_lay_object_id_list"];

		$this->draw_detail_script="\n".$this->draw_detail_script."\n"."
			\$(document).ready(function(){
				//Here is for check Enter Key replace to Tab Key !!
			});
			var only_lay_object_id_list='';
			var hidden_object_id_list='';
			var object_id_list='';
			var access_arr='';
			var access_read		='';
			var access_addnew	='';
			var access_edit		='';
			var access_delete	='';
			var access_approve	='';
			var access_print		='';
			\$(document).ready(function(){
				only_lay_object_id_list=\"$only_lay_object_id_list\";
				hidden_object_id_list=\"$hidden_object_id_list\";
				object_id_list=\"$object_id_list\";
				access_arr=\"<?php echo _get_access_list(__FILE__); ?>\".split(\"|\");
				access_read		=access_arr[0];
				access_addnew	=access_arr[1];
				access_edit		=access_arr[2];
				access_delete	=access_arr[3];
				access_approve	=access_arr[4];
				access_print	=access_arr[5];
			});

			var object_id_keypress_d230c01e08285d10e0c26984588f7e0='';
			\$(document).ready(function (){
				function parent_visible(\$obj){
					try{
						var \$obj_parent=\$obj.parent();
						if (\$obj_parent==\$obj || \$obj_parent==null){
							return true;
						}else{
							var style_display=(\$obj_parent.css('display')).toLowerCase();
							var style_visibility=(\$obj_parent.css('visibility')).toLowerCase();
							if (style_display=='none' || style_visibility=='hidden'){
								return false;
							}
							return parent_visible(\$obj_parent);
						}
					}catch(e){return true;}
				}
				
				function input_control_keypress (e){
					if (_valKey(/^13\$/,e)!=-1){return true;}		//测试到是输入的不是回车键
					
					var disabled_object_id_list='';
					var object_id_list_arr=object_id_list.split('|');
					for (var key_index=0; key_index<object_id_list_arr.length; key_index++){
						var object_id=trim(object_id_list_arr[key_index]);
						if (document.getElementById(object_id).disabled || 
							document.getElementById(object_id).readOnly){
							disabled_object_id_list=disabled_object_id_list+'|'+object_id;
						}else{
							//parent_visible函数使用客户端cpu率很高，暂时不调用
							/*
							if (!parent_visible(\$(document.getElementById(object_id)))){
								disabled_object_id_list=disabled_object_id_list+'|'+object_id;
							}else{
							}
							*/
						}
					}
					
					if (object_id_keypress_d230c01e08285d10e0c26984588f7e0!=''){
						disabled_object_id_list=remove_object_id(disabled_object_id_list,
							object_id_keypress_d230c01e08285d10e0c26984588f7e0);
					}
					
					var temp_id_list=object_id_list;
					temp_id_list=remove_object_id(temp_id_list,hidden_object_id_list);
					temp_id_list=remove_object_id(temp_id_list,disabled_object_id_list);
					var temp_id_list_arr=temp_id_list.split('|');

					for (var key_index=0; key_index<temp_id_list_arr.length; key_index++){
						var object_id=trim(temp_id_list_arr[key_index]);
						var object_id_next=trim(temp_id_list_arr[0]);
						if (temp_id_list_arr.length!=1 && key_index!=temp_id_list_arr.length-1){
							object_id_next=trim(temp_id_list_arr[key_index+1]);
						}

						if (object_id==object_id_keypress_d230c01e08285d10e0c26984588f7e0){
							\$('#'+object_id).blur();
							\$('#'+object_id_next).focus();
							\$('#'+object_id_next).select();
							//document.getElementById('txt_wel_cus_des').value=object_id_next;
							return false;
							break;
						}
					}
					return true;
				}
				
				\$('input').bind('keypress',function (e){
					object_id_keypress_d230c01e08285d10e0c26984588f7e0=this.id;
					return input_control_keypress(e);
				});
				
				\$('select').bind('keypress',function (e){
					object_id_keypress_d230c01e08285d10e0c26984588f7e0=this.id;
					return input_control_keypress(e);
				});
			});
		";

		$this->extract_message_script="
			function extract_message(msg_code){
				switch(msg_code.toUpperCase()){
				".$this->extract_message_script."
				default:
				return msg_code;
				break;
				}
			}
			";
		
		$this->label_normal_script="\$(document).ready(function(){\n".$this->label_normal_script."});";
		
		$wel_develop_info="";
		if (WEL_DEVELOP_INFO){
			$wel_develop_info_id="wel_develop_info_".md5(uniqid(rand()));
			$wel_textarea_id="wel_textarea_info_".md5(uniqid(rand()));
			$wel_develop_info="
				<div id='".$wel_develop_info_id."' name='".$wel_develop_info_id."' style='display:none;'>
				<textarea id='".$wel_textarea_id."' name='".$wel_textarea_id."' 
				wrap='off' style='width:100%;height:100%;overflow:scroll;font-size:10pt;'>".
				htmlspecialchars($lay_xml_file)."\n".
				htmlspecialchars(file_get_contents($lay_xml_file))."\n".
				"</textarea>
				</div>
				
				<script language='javascript'>
				\$(document).ready(function (){
					\$('#lbl_page_title').bind('click',function (){
						var develop_info_left=0;
						var develop_info_top=32;
						var develop_info_width=\$(window).width();
						var develop_info_height=\$(window).height()-develop_info_top;
						\$('#".$wel_develop_info_id."').css({
							position:'absolute',
							left:develop_info_left,
							top:develop_info_top,
							width:develop_info_width,
							height:develop_info_height
							});
						\$('#".$wel_textarea_id."').css({
							width:develop_info_width,
							height:develop_info_height
							});
						if (\$('#".$wel_develop_info_id."').css('display').toLowerCase()=='none'){
							\$('#".$wel_develop_info_id."').css('display','inline');
						}else{
							\$('#".$wel_develop_info_id."').css('display','none');
						}
					});
				});
				</script>
			";
		}
		
		$this->label_message_script=$hidden_object_id_arr.
			"function label_message_d230c01e08285d10e0c26984588f7e0(){\n".
			"static \$label_message=array();\n".
			"static \$init_msg=false;\n".
			"if (!(\$init_msg)){\n".
			"\$init_msg=true;\n".
			$this->label_message_script.
			"}\n".
			"return \$label_message;\n".
			"}\n";

		$this->body_detail_html=
			"\$body_inc_d230c01e08285d10e0c26984588f7e0='body_inc_begin';\n".
			$this->label_sql_script.
			$this->label_message_script." ?>".
			
			$this->page_title_html.
			$this->body_detail_html.
			$hidden_object_html.
			"<?php \$hidden_object_id_list=\"$hidden_object_id_list\"; ?>\n".
			"<?php \$object_id_list=\"$object_id_list\"; ?>\n".
			"<?php \$only_lay_object_id_list=\"$only_lay_object_id_list\"; ?>\n".
			$wel_develop_info."\n".
			"<script language=\"javascript\">\n".
			$this->extract_message_script."\n".
			$this->label_normal_script."\n".
			$this->draw_detail_script."\n".
			$this->browser_return_script."\n".
			$this->browser_click_script."\n".
			$this->blur_return_script."\n".
			$this->blur_blur_script."\n".
			$this->page_title_script."\n".
			$this->page_script_script."\n".
			"</script>\n".
			"<?php \$body_inc_d230c01e08285d10e0c26984588f7e0='body_inc_end';";

	
		//write_to_temp_file_for_analysis_test($wel_cache_content);

		
		$wel_cache_id=werp_pathinfo_filename($this->caller_file);
		$wel_cache_content_arr=array();
		$wel_cache_content_arr[$wel_cache_id."msg.inc"]=$this->label_message_script;
		$wel_cache_content_arr[$wel_cache_id."sql.inc"]=$this->label_sql_script;
		$wel_cache_content_arr[$wel_cache_id."blur.inc"]=$this->blur_sql_script;
		add_cache_to_wel_wrcache($wel_cache_id,serialize($wel_cache_content_arr));
		//$this->body_detail_html 内容有点大，接下来只使用一次就不写入数据库了，直接传回去
		return $this->body_detail_html;
	}
}

function werp_pathinfo_filename($_caller_file){
	$basename=substr($_caller_file,strlen(dirname($_caller_file))+1);
	$basename_arr=explode(".",$basename);
	return $basename_arr[0];
}

function add_cache_to_wel_wrcache($wel_cache_id,$wel_cache_content){
	$wel_session_id=session_id();
	$wel_session_id=strtolower($wel_session_id);
	$wel_cache_id=strtolower($wel_cache_id);
	$wel_cache_expire=session_cache_expire();
	if (!is_numeric($wel_cache_expire)){$wel_cache_expire=0;}

	//compress cache content
	switch(true){
		case function_exists("gzdeflate") && function_exists("gzinflate"):
			$wel_cache_content=gzdeflate($wel_cache_content,9);
			break;
		case function_exists("gzcompress") && function_exists("gzuncompress"):
			$wel_cache_content=gzcompress($wel_cache_content,9);
			break;
		case function_exists("gzencode") && function_exists("gzdecode"):
			$wel_cache_content=gzencode($wel_cache_content,9);
			break;
		default:
		break;
	}
	
	try{
		$conn=werp_db_connect();
		$wel_cache_content=addslashes($wel_cache_content);
		$sql="select * from #__wel_wrcache where ".
			"wel_session_id='$wel_session_id' and wel_cache_id='$wel_cache_id' limit 1";
		$sql=revert_to_the_available_sql($sql);
		if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
		if(!($row=mysql_fetch_array($result))){
			$sql="insert into #__wel_wrcache(wel_session_id,wel_cache_id,".
				"wel_crt_date,wel_cache_expire,wel_cache_content)".
				"values('$wel_session_id','$wel_cache_id',".
				"now(),$wel_cache_expire,'$wel_cache_content')";
			$sql=revert_to_the_available_sql($sql);
			if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
		}else{
			$sql="update #__wel_wrcache set ".
				"wel_crt_date=now(),".
				"wel_cache_expire=$wel_cache_expire,".
				"wel_cache_content='$wel_cache_content' ".
				"where wel_session_id='$wel_session_id' and wel_cache_id='$wel_cache_id'";
			$sql=revert_to_the_available_sql($sql);
			if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
		}
	}
	catch (Exception $e){
		echo $e->getMessage();
	}
}

function read_cache_from_wel_wrcache($wel_cache_id){
	$wel_unserialize_id="";
	$wel_cache_id=strtolower($wel_cache_id);
	$wel_temp_flag_arr=array("msg.inc","sql.inc","blur.inc");
	
	foreach ($wel_temp_flag_arr as $key=>$value){
		if (substr($wel_cache_id, - strlen($value))==$value){
			$wel_unserialize_id=$wel_cache_id;
			$wel_cache_id=substr($wel_cache_id,0,strlen($wel_cache_id)-strlen($value));
			break;
		}
	}
	
	$wel_session_id=session_id();
	$wel_session_id=strtolower($wel_session_id);
	$wel_cache_id=strtolower($wel_cache_id);
	$wel_cache_content="";
	try{
		$conn=werp_db_connect();
		$sql="select * from #__wel_wrcache where ".
			"wel_session_id='$wel_session_id' and wel_cache_id='$wel_cache_id' limit 1";
		$sql=revert_to_the_available_sql($sql);
		if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
		if($row=mysql_fetch_array($result)){
			$wel_cache_content=$row["wel_cache_content"];
		}
	}
	catch (Exception $e){
		echo $e->getMessage();
	}

	//uncompress cache content
	switch(true){
		case function_exists("gzdeflate") && function_exists("gzinflate"):
			$wel_cache_content=gzinflate($wel_cache_content);
			break;
		case function_exists("gzcompress") && function_exists("gzuncompress"):
			$wel_cache_content=gzuncompress($wel_cache_content);
			break;
		case function_exists("gzencode") && function_exists("gzdecode"):
			$wel_cache_content=gzdecode($wel_cache_content);
			break;
		default:
		break;
	}
	
	if ($wel_unserialize_id!=""){
		$wel_cache_content_arr=unserialize($wel_cache_content);
		$wel_cache_content=$wel_cache_content_arr[$wel_unserialize_id];
	}
	
	return $wel_cache_content;
}

function remove_expire_cache_from_wel_wrcache($expired_wel_session_id = ""){
	$wel_session_id=session_id();
	$wel_session_id=strtolower($wel_session_id);
	
	//移除其他已过期session的cache
	try{
		$conn=werp_db_connect();
		$expired_wel_session_id=trim($expired_wel_session_id);
		if ($expired_wel_session_id!=""){	//移除指定的过期cache
			$sql="DELETE FROM #__wel_wrcache WHERE wel_session_id='".$expired_wel_session_id."'";
			$sql=revert_to_the_available_sql($sql);
			if(!mysql_query($sql,$conn)){/*throw new Exception(mysql_error());*/}
		}
		
		$sql="select * from #__wel_wrcache where wel_session_id=wel_cache_id and ".
			"wel_session_id<>'$wel_session_id' and ".
			"DATE_ADD(wel_crt_date,INTERVAL IFNULL(wel_cache_expire,0) MINUTE)<now()";
		$sql=revert_to_the_available_sql($sql);
		if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
		while($row=mysql_fetch_array($result)){
			$wel_session_id=$row["wel_session_id"];
			$sql="DELETE FROM #__wel_wrcache WHERE wel_session_id='".$wel_session_id."'";
			$sql=revert_to_the_available_sql($sql);
			if(!mysql_query($sql,$conn)){/*throw new Exception(mysql_error());*/}
		}
	}
	catch (Exception $e){
		echo $e->getMessage();
	}
}

function get_xml_file($XML_FILENAME_ABSOLUTE_PATH,$FILENAME){
	$xml_file=$XML_FILENAME_ABSOLUTE_PATH.$FILENAME.".xml";
	return $xml_file;
}
function get_lay_xml_file($XML_FILENAME_ABSOLUTE_PATH,$FILENAME){
	$xml_file=$XML_FILENAME_ABSOLUTE_PATH.$FILENAME.".xml";
	$lay_xml_file=$XML_FILENAME_ABSOLUTE_PATH.$FILENAME."_lay.xml";

	//测试layout模板文件是否存在，不存在则从标准的模板文件复制过来
	//if (!file_exists($lay_xml_file)){copy($xml_file,$lay_xml_file);}	//GAE不允许建立和修改文件
	return $lay_xml_file;
}

function get_page_directory($_caller_file){
	return substr(dirname($_caller_file),strlen(WERP_SITE_PATH));
}

function rebuild_layout($_caller_file){
	$caller_file=$_caller_file;
	clearstatcache();
	$rebuild_layout_body=new rebuild_layout_body($_caller_file);
	return $rebuild_layout_body->rebuild_layout_body_inc();
}

function js_unescape($str){
	$ret = '';
	$len = strlen($str);
	
	for ($i = 0; $i < $len; $i++){
		if ($str[$i] == '%' && $str[$i+1] == 'u'){
			$val = hexdec(substr($str, $i+2, 4));
	
			if ($val < 0x7f) $ret .= chr($val);
			else if($val < 0x800) $ret .= chr(0xc0|($val>>6)).chr(0x80|($val&0x3f));
			else $ret .= chr(0xe0|($val>>12)).chr(0x80|(($val>>6)&0x3f)).chr(0x80|($val&0x3f));
	
			$i += 5;
		} else if ($str[$i] == '%'){
			$ret .= urldecode(substr($str, $i, 3));
			$i += 2;
		}
		else $ret .= $str[$i];
	}
	return $ret;
}

function receipt_url_parameter_get_or_post($_GET_OR_POST){
	$parameter_list="";
	if($_GET_OR_POST){
		foreach($_GET_OR_POST as $key=>$value){
			$var_value="";
			$temp_value="";
			if(is_array($_GET_OR_POST[$key])){
				foreach($_GET_OR_POST[$key] as $key_second=>$value_second){
					$temp_value=$value_second;
					$temp_value=str_ireplace("%URL","%",js_unescape($temp_value));
					$temp_value=addslashes($temp_value);
					if (!get_magic_quotes_gpc()){$temp_value=addslashes($temp_value);}
					$temp_value=str_ireplace("\\'","'",$temp_value);
					$temp_value=str_ireplace("$","\\$",$temp_value);
					$var_value="\$".$key."[\"$key_second\"]=trim(\"$temp_value\");\n";
				}
			}
			else{
				$temp_value=$value;
				$temp_value=str_ireplace("%URL","%",js_unescape($temp_value));
				$temp_value=addslashes($temp_value);
				if (!get_magic_quotes_gpc()){$temp_value=addslashes($temp_value);}
				$temp_value=str_ireplace("\\'","'",$temp_value);
				$temp_value=str_ireplace("$","\\$",$temp_value);
				$var_value="\$".$key."=trim(\"$temp_value\");\n";
			}
			$parameter_list=$parameter_list.$var_value;
		}
	}
	return $parameter_list;
}
	
function receipt_url_parameter($_GET,$_POST){
	return receipt_url_parameter_get_or_post($_GET).receipt_url_parameter_get_or_post($_POST);
}

function format_slashes($str,$delimiter='\''){
	if (is_null($str)){$str="";}
	return str_ireplace("\r","\\r",str_ireplace("\n","\\n",str_replace($delimiter,'\\'.$delimiter,$str)));
}

function validate_character($str){
	$special_character_pos=stripos($str,"\"");
	if (!($special_character_pos===false)){return "Invalid primary key character [\"] !";}
	$special_character_pos=stripos($str,"'");
	if (!($special_character_pos===false)){return "Invalid primary key character ['] !";}
	$special_character_pos=stripos($str,"|");
	if (!($special_character_pos===false)){return "Invalid primary key character [|] !";}
	return "";
}

function extract_message($msg_code){
	$wel_language_id=$_SESSION["wel_language_id"];
	$label_message=label_message_d230c01e08285d10e0c26984588f7e0();
	/*
	if (is_null($msg_code) || empty($msg_code)){return $msg_code;}
	if ($label_message[$msg_code][$wel_language_id]==null){return $msg_code;}
	if ($label_message[$msg_code][$wel_language_id]==""){return $msg_code;}
	return $label_message[$msg_code][$wel_language_id];
	*/
	$msg_message=$msg_code;
	if (($label_message[$msg_code][$wel_language_id]!=null) and
		($label_message[$msg_code][$wel_language_id]!="")){
		$msg_message=$label_message[$msg_code][$wel_language_id];
	}else{
		if (($label_message[$msg_code]["english"]!=null) and
			($label_message[$msg_code]["english"]!="")){
			$msg_message=$label_message[$msg_code]["english"];
		}
	}
	return $msg_message;
}

function werp_db_connect(){
	static $conn=null;
	if($conn){return $conn;}
	
	$wel_db_host=$_SESSION["wel_db_host"];
	$wel_db_user=$_SESSION["wel_db_user"];
	$wel_db_password=$_SESSION["wel_db_password"];
	$wel_db_name=$_SESSION["wel_db_name"];
	
	$conn=mysql_connect($wel_db_host,$wel_db_user,$wel_db_password,true);
	if(!$conn){
		//trigger_error(mysql_error(),E_USER_ERROR);
	}
	if(!(mysql_select_db($wel_db_name,$conn))){
		//trigger_error(mysql_error(),E_USER_ERROR);
	}
	mysql_query("SET NAMES 'utf8'");
	return $conn;
}

function revert_to_the_available_sql($sql){
	$wel_table_prefix=$_SESSION["wel_table_prefix"];
	return str_ireplace("#__",$wel_table_prefix,$sql);
}

function recombine_sql_sentence($sql_sentence,$label_values){
	//if (!is_array($label_values)){return false;}
	
	$wel_language_id=strtolower($_SESSION["wel_language_id"]);
	$sql=trim($sql_sentence);
	$count=0;
	$field_arr=array();
	$label_arr=array();
	$as_arr=array();
	$str_field="";
	$str_label="";
	$had_lab_sql="";
	$no_lab_sql="";
	$as_lab_sql="";
	
	$start_pos = stripos($sql, " ", 0);
	if (!($start_pos===false)){		//测试 DISTINCT
		$sql_DISTINCT=trim(substr($sql,$start_pos));
		$sql_DISTINCT=trim(str_ireplace("\n","",str_ireplace("\t","",$sql_DISTINCT)));
		if (strtoupper(substr($sql_DISTINCT,0,strlen("DISTINCT")))=="DISTINCT"){
			$start_pos = stripos($sql, "DISTINCT", 0)+strlen("DISTINCT");
		}
	}
	
	while (!($start_pos===false)){
		$as_pos = stripos($sql, " as ", $start_pos+1);
		if (!($as_pos===false)){
			$left_pos = stripos($sql, "[", $as_pos+1);
			if (!($left_pos===false)){
				$right_pos = stripos($sql, "]", $left_pos+1);
				if (!($right_pos===false)){
					$field_arr[$count]=trim(substr($sql,$start_pos+1,$as_pos-$start_pos-1));
					$label_arr[$count]=trim(substr($sql,$left_pos+1,$right_pos-$left_pos-1));
					$as_arr[$count]   =trim(substr($sql,$left_pos+1,$right_pos-$left_pos-1));
					$as_lab_sql=$as_lab_sql.substr($sql,0,$as_pos)." as ".$as_arr[$count];
					$no_lab_sql=$no_lab_sql.substr($sql,0,$as_pos);
					$had_lab_sql=$had_lab_sql.substr($sql,0,$left_pos);
					
					$str_label=strtolower($label_arr[$count]);
					if (($label_values[$str_label][$wel_language_id]!="") and 
						($label_values[$str_label][$wel_language_id]!=null)){
						$label_arr[$count]=$label_values[$str_label][$wel_language_id];
					}else{
						if (($label_values[$str_label]["english"]!="") and 
							($label_values[$str_label]["english"]!=null)){
							$label_arr[$count]=$label_values[$str_label]["english"];
						}
					}
					$had_lab_sql=$had_lab_sql."[".$label_arr[$count]."]";
					$count++;
					
					$sql=substr($sql,$right_pos+1);
					if (substr(trim($sql),0,1)<>","){
						$as_lab_sql=$as_lab_sql.$sql;
						$no_lab_sql=$no_lab_sql.$sql;
						$had_lab_sql=$had_lab_sql.$sql;
						break;
					}
					$start_pos = stripos($sql, ",", 0);
				}else break;
			}else break;
		}else break;
	}
	$return_arr=array();
	$return_arr["had_lab_sql"]=$had_lab_sql;
	$return_arr["no_lab_sql"]=$no_lab_sql;
	$return_arr["as_lab_sql"]=$as_lab_sql;
	$return_arr["field_arr"]=$field_arr;
	$return_arr["label_arr"]=$label_arr;
	$return_arr["as_arr"]=$as_arr;
	return $return_arr;
}

/*
function format_date($value){
	try{
		return $value;	//禁止使用这个函数和同类方法格式化日期
		//小心使用这个函数，只能正确格式化[1901-12-14]-[2038-01-19]
		$date_string=date("Y-m-d",strtotime($value));return $date_string;
	}catch (Exception $e){return "";}
}
*/

function format_number($value,$decimal){
	
}

function get_access_array($wel_prog_code,$wel_user_id=null){
	$access_list_arr=array(
		"wel_access_read"=>"false",
		"wel_access_addnew"=>"false",
		"wel_access_edit"=>"false",
		"wel_access_delete"=>"false",
		"wel_access_approve"=>"false",
		"wel_access_print"=>"false");		//预设没有任何权限

	/*
	用 $wel_user_id 和 $wel_prog_code 从表格 wel_groupprog 中取得权限
	*/

	try{
		if ($wel_user_id===null){		//如果没有指定要测试的用户，则使用当前登入的用户
			$wel_user_id=$_SESSION["wel_user_id"];
		}
		$wel_user_id=strtolower($wel_user_id);
		$wel_prog_code=strtolower($wel_prog_code);
		
		//预设 admin 和 sa 为超级用户，不检查权限，预设拥有任何权限
		if ($wel_user_id=="admin" || $wel_user_id=="sa"){
			//foreach ($access_list_arr as $key=>$value){$access_list_arr[$key]="true";}
			//throw new Exception("");	//结束权限查询
		}

		$conn=werp_db_connect();

		$sql="SELECT * FROM #__wel_userprog WHERE wel_prog_code='$wel_prog_code' limit 1";
		$sql=revert_to_the_available_sql($sql);
		if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}	//查询sql时出错了
		if(!($row=mysql_fetch_array($result))){
			$sql="SELECT * FROM #__wel_groupprog WHERE wel_prog_code='$wel_prog_code' limit 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}	//查询sql时出错了
			if(!($row=mysql_fetch_array($result))){
				//没有符合条件的记录，则说明这个wel_prog_code没有设置权限
				foreach ($access_list_arr as $key=>$value){$access_list_arr[$key]="true";}
				throw new Exception("");
			}
		}
		
		$sql_wel_userprog="SELECT ".
			"wel_access_read,wel_access_addnew,wel_access_edit,".
			"wel_access_delete,wel_access_approve,wel_access_print ".
			"FROM #__wel_userprog ".
			"WHERE wel_user_id='$wel_user_id' AND wel_prog_code='$wel_prog_code'";
			
		$sql_wel_groupprog="SELECT ".
			"wel_access_read,wel_access_addnew,wel_access_edit,".
			"wel_access_delete,wel_access_approve,wel_access_print ".
			"FROM #__wel_groupprog AS groupprog LEFT JOIN #__wel_usergroup AS usergroup ".
			"ON groupprog.wel_group_code=usergroup.wel_group_code WHERE ".
			"usergroup.wel_user_id='$wel_user_id' AND groupprog.wel_prog_code='$wel_prog_code'";
			
		$sql="SELECT ".
			"SUM(wel_access_read) AS wel_access_read,".
			"SUM(wel_access_addnew) AS wel_access_addnew,".
			"SUM(wel_access_edit) AS wel_access_edit,".
			"SUM(wel_access_delete) AS wel_access_delete,".
			"SUM(wel_access_approve) AS wel_access_approve,".
			"SUM(wel_access_print) AS wel_access_print ".
			"FROM (".
			"SELECT * FROM ($sql_wel_userprog) AS table001 UNION ALL ".
			"SELECT * FROM ($sql_wel_groupprog) AS table002".
			") AS table003";

		$sql=revert_to_the_available_sql($sql);
		if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}	//查询sql时出错了
		if(!($row=mysql_fetch_array($result))){throw new Exception("");}	//没有符合条件的记录
		
		$wel_access_read=$row["wel_access_read"];
		$wel_access_addnew=$row["wel_access_addnew"];
		$wel_access_edit=$row["wel_access_edit"];
		$wel_access_delete=$row["wel_access_delete"];
		$wel_access_approve=$row["wel_access_approve"];
		$wel_access_print=$row["wel_access_print"];
		
		if (!is_numeric($wel_access_read)){$wel_access_read=0;}
		if (!is_numeric($wel_access_addnew)){$wel_access_addnew=0;}
		if (!is_numeric($wel_access_edit)){$wel_access_edit=0;}
		if (!is_numeric($wel_access_delete)){$wel_access_delete=0;}
		if (!is_numeric($wel_access_approve)){$wel_access_approve=0;}
		if (!is_numeric($wel_access_print)){$wel_access_print=0;}
		
		if ($wel_access_read>0){$access_list_arr["wel_access_read"]="true";}
		if ($wel_access_addnew>0){$access_list_arr["wel_access_addnew"]="true";}
		if ($wel_access_edit>0){$access_list_arr["wel_access_edit"]="true";}
		if ($wel_access_delete>0){$access_list_arr["wel_access_delete"]="true";}
		if ($wel_access_approve>0){$access_list_arr["wel_access_approve"]="true";}
		if ($wel_access_print>0){$access_list_arr["wel_access_print"]="true";}
	}catch (Exception $e){

	}
	return $access_list_arr;
}

function check_permission($conn,$wel_prog_code,$wel_access_action,$wel_user_id=null){
	$permission=false;
	$access_list_arr=get_access_array($wel_prog_code,$wel_user_id);
	$permission=(($access_list_arr[strtolower($wel_access_action)]=="true")?true:false);
	return $permission;
}

function _get_access_list($_caller_file){
	$access_list_arr=array(
		"wel_access_read"=>"false",
		"wel_access_addnew"=>"false",
		"wel_access_edit"=>"false",
		"wel_access_delete"=>"false",
		"wel_access_approve"=>"false",
		"wel_access_print"=>"false");		//预设没有任何权限
	
	$wel_prog_code=werp_pathinfo_filename($_caller_file);

	/*
	取得正确的程序码，
	例:$wel_prog_code=sysm001a
	则取到后正确的程序码 $wel_prog_code=sysm001
	*/
	$numeric_arr=array("0","1","2","3","4","5","6","7","8","9");
	$str_file=strrev($wel_prog_code);
	for($char_count=0;$char_count<strlen($str_file);$char_count++){
		if (in_array(substr($str_file,$char_count,1),$numeric_arr)){
			$wel_prog_code=strrev(substr($str_file,$char_count));
			break;
		}
	}
	
	$access_list_arr=get_access_array($wel_prog_code,null);
	return implode("|",$access_list_arr);
}

function extract_system_settings_d230c01e08285d10e0c26984588f7e0(){
	$msg_code="";
	$return_val=array();

	try
	{
		$conn=werp_db_connect();
		
		$sql="SELECT * FROM #__wel_syssets LIMIT 1";
		$sql=revert_to_the_available_sql($sql);
		if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
		if(!$row=mysql_fetch_array($result)){throw new Exception("");}		//无需错误信息
		foreach ($row as $key=>$value){$return_val[$key]=$value;}
		throw new Exception("");
	}
	catch (Exception $e)
	{
		$msg_code=$e->getMessage();
	}
	$return_val["msg_code"]=$msg_code;
	return $return_val;
}

function werp_number_to_words($number){
	$comma_sp=array(
		0=> "",
		1=> "Thousand ",
		2=> "Million ",
		3=> "Billion ",
		4=> "Trillion ",
		5=> "Quadrillion ",
		6=> "Quintillion ",
		7=> "Sextillion ",
		8=> "Septillion ",
		9=> "Octillion ",
		10=> "Nonillion ",
		11=> "Decillion ",
		12=> "Undecillion ",
		13=> "Duodecillion ",
		14=> "Tredecillion ",
		15=> "Septemdecillion ",
		16=> "Octodecillion ",
		17=> "Novemdecillion ",
		18=> "Vigintillion "
		);
		
	$number_cell=array(
		0 => "",
		1 => "One ",
		2 => "Two ",
		3 => "Three ",
		4 => "Four ",
		5 => "Five ",
		6 => "Six ",
		7 => "Seven ",
		8 => "Eight ",
		9 => "Nine ",
		10 => "Ten ",
		11 => "Eleven ",
		12 => "Twelve ",
		13 => "Thirteen ",
		14 => "Fourteen ",
		15 => "Fifteen ",
		16 => "Sixteen ",
		17 => "Seventeen ",
		18 => "Eighteen ",
		19 => "Nineteen ",
		20 => "Twenty ",
		30 => "Thirty ",
		40 => "Forty ",
		50 => "Fifty ",
		60 => "Sixty ",
		70 => "Seventy ",
		80 => "Eighty ",
		90 => "Ninety ",
		100 => "Hundred "
		);
		
	$num_string=number_format($number,2,".",",");
	$num_string_arr=explode(",",$num_string);
	$num_cents_arr=explode(".",$num_string_arr[count($num_string_arr)-1]);
	$num_string_arr[count($num_string_arr)-1]=$num_cents_arr[0];
	$num_string_cents=$num_cents_arr[1];
	
	$comma_sp_index=0;
	$total_format_str="";
	for ($temp_index=count($num_string_arr)-1;$temp_index>=0;$temp_index--){
		$segment_format="";
		$temp_num_str=$num_string_arr[$temp_index];
		if (doubleval($temp_num_str)>=100){
			$segment_format=$number_cell[doubleval(substr($temp_num_str,0,1))].$number_cell[100];
			$temp_num_str=substr($temp_num_str,1);
		}
		if (doubleval($temp_num_str)<20){
			$segment_format .=$number_cell[doubleval($temp_num_str)];
		}else{
			$segment_format .=$number_cell[doubleval(substr($temp_num_str,0,1))*10];
			$temp_num_str=substr($temp_num_str,1);
			if (doubleval($temp_num_str)>0){
				$segment_format=trim($segment_format)."-".$number_cell[doubleval($temp_num_str)];
			}
		}
		if ($segment_format!=""){$segment_format=$segment_format.$comma_sp[$comma_sp_index];}
		if ($total_format_str!="" && $segment_format!=""){
			$total_format_str=$segment_format."And ".$total_format_str;
		}else if ($segment_format!=""){
			$total_format_str=$segment_format;
		}
		$comma_sp_index++;
	}

		$segment_format="";
		$temp_num_str=$num_string_cents;
		if (doubleval($temp_num_str)<20){
			$segment_format .=$number_cell[doubleval($temp_num_str)];
		}else{
			$segment_format .=$number_cell[doubleval(substr($temp_num_str,0,1))*10];
			$temp_num_str=substr($temp_num_str,1);
			if (doubleval($temp_num_str)>0){
				$segment_format=trim($segment_format)."-".$number_cell[doubleval($temp_num_str)];
			}
		}
		if ($segment_format!=""){$segment_format=$segment_format."Cents ";}
		if ($total_format_str!="" && $segment_format!=""){
			$total_format_str=$total_format_str."And ".$segment_format;
		}else if ($segment_format!=""){
			$total_format_str=$segment_format;
		}
	
	if ($total_format_str!=""){$total_format_str=$total_format_str."Only";}
	return $total_format_str;
}


/*
function DeleteExpiredTempFiles($temp_dir){
	if (rand(1,10)>3){return false;}	//不用每次调用都去删除一次文件列表
	
	$arr_file=array();
	//打开目录
	$fp=opendir($temp_dir);
	//阅读目录
	while($sfile=readdir($fp)){
	//列出所有文件并去掉'.'和'..'
		if($sfile!='.' && $sfile!='..'){
			//赋值给数组
			$arr_file[$sfile]=$temp_dir.$sfile;
		}
	}
	$sfile_prefix="report_temp_";	//要清理的文件名前缀
	$current_time=strtotime(date("Y-m-d"));
	$timeout_seconds=24*3600;	//超时秒数，24*3600为一天的时间，大于一天就删除
	foreach ($arr_file as $key=>$value){
		try{
			if (strtolower(substr($key,0,strlen($sfile_prefix)))==$sfile_prefix){
				if ($current_time-fileatime($value)>$timeout_seconds ||
					$current_time-filectime($value)>$timeout_seconds ||
					$current_time-filemtime($value)>$timeout_seconds){
					//文件的上次访问时间 
					//文件的上次 inode 修改时间 
					//在大多数 Unix 文件系统中，
					//当一个文件的 inode 数据被改变时则该文件被认为是修改了。
					//也就是说，当文件的权限，所有者，所有组或其它 inode 中的元数据被更新时。
					//文件内容上次的修改时间
					@unlink($value);
				}
			}
		}catch(Exception $e){}
	}
}
DeleteExpiredTempFiles(WERP_SITE_PATH_TEMP);
*/


function write_to_temp_file_for_analysis_test($analysis_test_contents){

	if (!file_exists(WERP_SITE_PATH_TEMP)){
		$origmask = @umask(0);
		mkdir(WERP_SITE_PATH_TEMP,0755);	//临时目录，
		@umask($origmask);
	}

	$temp_file_name=WERP_SITE_PATH_TEMP."analysis_test_".md5(uniqid(rand())).".inc";
	$filehandler = fopen($temp_file_name, "w");
	if (fwrite($filehandler, $analysis_test_contents) === FALSE) {
		echo "can not write to file: ".$temp_file_name;
	}
	fclose($filehandler);
	return $temp_file_name;
}
?>