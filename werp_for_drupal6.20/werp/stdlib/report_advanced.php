<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or LGPL Version 2.1 (LGPLv2.1-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>
<?php
eval(receipt_url_parameter($_GET,$_POST));	//将参数的值放入相应以参数为名的变量中
//echo receipt_url_parameter($_GET,$_POST);

//=================================================================
class report_datasrc_info{
	private $lay_xml_contents;
	private $parser;
	private $sub_id;
	private $sub_datasrc_arr=array();
	private $part_tag;
	
	private $datasrc="datasrc";
	private $sql_sentence="";
	
	private $datafld="datafld";
	private $table="table";
	private $where="where";
	private $sort="sort";
	private $group="group";
	private $limit="limit";

	private $datafld_id_arr=array();
	private $datafld_from_arr=array();
	
	private $table_from_arr=array();

	private $where_from_arr=array();
	
	private $sort_direction_arr=array();
	private $sort_from_arr=array();
	
	private $group_arr=array();
	
	private $limit_arr=array();
	
	function __construct($_lay_xml_contents){
		$this->lay_xml_contents=$_lay_xml_contents;
	}
	function __destruct(){
	}
	
	private function startElement($parser_instance, $element_name, $attrs){     //起始标签事件的函数
		switch ($this->part_tag){
			case $this->datasrc:
				switch (strtolower($element_name)){
					case strtolower($this->datafld):
						$id="";
						$from="";
						foreach ($attrs as $key=>$value){
							if (strtolower($key)=="id"){$id=$value;}
							if (strtolower($key)=="from"){$from=$value;}
						}
						if ($id=="" || $from==""){break;}
						if ($this->sub_id==""){
							$this->datafld_id_arr[]=$id;
							$this->datafld_from_arr[]=$from;
						}else{
							$this->sub_datasrc_arr[$this->sub_id]["datafld_id_arr"][]=$id;
							$this->sub_datasrc_arr[$this->sub_id]["datafld_from_arr"][]=$from;
						}
						break;
					case strtolower($this->table):
						$from="";
						foreach ($attrs as $key=>$value){
							if (strtolower($key)=="from"){$from=$value;}
						}
						if ($from==""){break;}
						if ($this->sub_id==""){
							$this->table_from_arr[]=$from;
						}else{
							$this->sub_datasrc_arr[$this->sub_id]["table_from_arr"][]=$from;
						}
						break;
					case strtolower($this->where):
						$from="";
						foreach ($attrs as $key=>$value){
							if (strtolower($key)=="from"){$from=$value;}
						}
						if ($from==""){break;}
						if ($this->sub_id==""){
							$this->where_from_arr[]=$from;
						}else{
							$this->sub_datasrc_arr[$this->sub_id]["where_from_arr"][]=$from;
						}
						break;
					case strtolower($this->sort):
						$direction="";
						$from="";
						foreach ($attrs as $key=>$value){
							if (strtolower($key)=="direction"){$direction=strtolower($value);}
							if (strtolower($key)=="from"){$from=$value;}
						}
						if ($direction!="asc" && $direction!="desc"){$direction="asc";}			//错误的排序标识
						if ($from==""){break;}
						if ($this->sub_id==""){
							$this->sort_direction_arr[]=$direction;
							$this->sort_from_arr[]=$from;
						}else{
							$this->sub_datasrc_arr[$this->sub_id]["sort_direction_arr"][]=$direction;
							$this->sub_datasrc_arr[$this->sub_id]["sort_from_arr"][]=$from;
						}
						break;
					case strtolower($this->group):
						$id="";
						$from="";
						$direction="";
						foreach ($attrs as $key=>$value){
							if (strtolower($key)=="id"){$id=$value;}
							if (strtolower($key)=="from"){$from=$value;}
							if (strtolower($key)=="direction"){$direction=strtolower($value);}
						}
						if ($direction!="asc" && $direction!="desc"){$direction="asc";}			//错误的排序标识
						if ($this->sub_id==""){
							$this->group_arr[$id]["from"]=$from;
							$this->group_arr[$id]["direction"]=$direction;
						}else{
							$this->sub_datasrc_arr[$this->sub_id]["group_arr"][$id]["from"]=$from;
							$this->sub_datasrc_arr[$this->sub_id]["group_arr"][$id]["direction"]=$direction;
						}
						break;
					case strtolower($this->limit):
						$limit="";
						foreach ($attrs as $key=>$value){
							if (strtolower($key)=="limit"){$limit=$value;}
						}
						if ($limit==""){break;}
						$limit=floatval(is_numeric($limit)?$limit:0);
						if ($limit<=0){break;}
						if ($this->sub_id==""){
							$this->limit_arr[]=$limit;
						}else{
							$this->sub_datasrc_arr[$this->sub_id]["limit_arr"][]=$limit;
						}
						break;
					default:
						break;
				}
				break;
			default:
				break;
		}
		
		switch (strtolower($element_name)){
			case strtolower($this->datasrc):
				$this->part_tag=$this->datasrc;
				foreach ($attrs as $key=>$value){
					if (strtolower($key)=="id"){
						$this->sub_id=strtolower($value);
						
						$this->sub_datasrc_arr[$this->sub_id]["datafld_id_arr"]=array();
						$this->sub_datasrc_arr[$this->sub_id]["datafld_from_arr"]=array();
							
						$this->sub_datasrc_arr[$this->sub_id]["table_from_arr"]=array();

						$this->sub_datasrc_arr[$this->sub_id]["where_from_arr"]=array();
							
						$this->sub_datasrc_arr[$this->sub_id]["sort_direction_arr"]=array();
						$this->sub_datasrc_arr[$this->sub_id]["sort_from_arr"]=array();
							
						$this->sub_datasrc_arr[$this->sub_id]["group_arr"]=array();
							
						$this->sub_datasrc_arr[$this->sub_id]["limit_arr"]=array();
					}
				}
				break;
			default:
				break;
		}
	}

	private function characterData($parser_instance, $XML_data){                //读取数据时的函数 
		switch ($this->part_tag){
			case $this->datasrc:
				break;
				
			case $this->datafld:
				break;
			case $this->table:
				break;
			case $this->where:
				break;
			case $this->sort:
				break;
			case $this->group:
				break;
			case $this->limit:
				break;
			default:
				break;
		}
	}
	
	private function endElement($parser_instance, $element_name){               //结束标签事件的函数
		switch ($this->part_tag){
			case $this->datasrc:
				switch (strtolower($element_name)){
					case strtolower($this->datasrc):
						$this->sub_id="";
						$this->part_tag='';
						break;
					default:
						break;
				}
				break;
			case $this->datafld:
				break;
			case $this->table:
				break;
			case $this->where:
				break;
			case $this->sort:
				break;
			case $this->group:
				break;
			case $this->limit:
				break;
			default:
				break;
		}
	}
	
	private function analysis_extract_xml(){
		$this->parser = xml_parser_create("UTF-8"); //创建一个parser编辑器
		//pool xml_set_object ( resource $parser , object &$object )
		//该函数使得 parser 指定的解析器可以被用在 object 对象中。
		//所有的回叫函数（callback function）
		//都可以由 xml_set_element_handler() 等函数来设置，它们被假定为 object 对象的方法。
		xml_set_object($this->parser, $this);
		//设立标签触发时的相应函数 这里分别为startElement和endElenment
		xml_set_element_handler($this->parser, "startElement", "endElement");
		//设立数据读取时的相应函数
		xml_set_character_data_handler($this->parser, "characterData");
		
		if (!xml_parse($this->parser, $this->lay_xml_contents, true)){
			//报错
			die(sprintf("XML error: %s at line %d",
			xml_error_string(xml_get_error_code($this->parser)),
			xml_get_current_line_number($this->parser)));
		}
		xml_parser_free($this->parser);//关闭和释放parser解析器	
	}

	private function recombine_sql_sentence(
		$datafld_id_arr,$datafld_from_arr,$table_from_arr,$where_from_arr,
		$group_arr,$sort_from_arr,$sort_direction_arr,$limit_arr){
		
		$str_columns="";
		foreach ($datafld_from_arr as $key=>$value){
			$str_columns .=($key==0?"":",").$value." as ".$datafld_id_arr[$key];
		}
		
		$str_tables="";
		foreach ($table_from_arr as $key=>$value){
			$str_tables .=$value;
		}
		
		$str_wheres="";
		foreach ($where_from_arr as $key=>$value){
			if ($key==0){$str_wheres=$value;}else{$str_wheres .=" and ".$value;}
		}
		if (trim($str_wheres)==""){$str_wheres="1=1";}
		
		$temp_count=0;
		$str_group_sorts="";
		foreach ($group_arr as $group_id=>$group_info_arr){
			//经过组检查，$this->group_arr 元素下标不一定从0开始
			$str_group_sorts .=($temp_count==0?"":",").
				$datafld_from_arr[array_search($group_arr[$group_id]["from"],$datafld_id_arr)].
				" ".$group_arr[$group_id]["direction"];
			$temp_count++;
		}
		
		$str_sorts="";
		foreach ($sort_from_arr as $key=>$value){
			$str_sorts .=($key==0?"":",").$value." ".$sort_direction_arr[$key];
		}
		
		$str_order_by="";
		$str_group_sorts=trim($str_group_sorts);
		$str_sorts=trim($str_sorts);
		if ($str_group_sorts!="" && $str_sorts!=""){
			$str_order_by=$str_group_sorts.",".$str_sorts;
		}else{
			$str_order_by=$str_group_sorts.$str_sorts;
		}
		
		$str_limit="";
		foreach ($limit_arr as $key=>$value){
			$str_limit =trim($value);
		}
		
		$sql_sentence="select ".$str_columns." from ".$str_tables." where ".$str_wheres;
		if ($str_order_by!=""){$sql_sentence=$sql_sentence." order by ".$str_order_by;}
		if ($str_limit!=""){$sql_sentence=$sql_sentence." limit ".$str_limit;}
		
		return  $sql_sentence;
	}
	
	public function get_report_datasrc_info(){
		$this->analysis_extract_xml();
		
		$temp_arr=$this->group_arr;
		foreach ($temp_arr as $group_id=>$group_info_arr){
			if (!in_array($this->group_arr[$group_id]["from"],$this->datafld_id_arr)){
				unset($this->group_arr[$group_id]);		//该组无效
			}
		}

		//组装主报表的SQL语句
		$this->sql_sentence=$this->recombine_sql_sentence(
			$this->datafld_id_arr,$this->datafld_from_arr,$this->table_from_arr,$this->where_from_arr,
			$this->group_arr,$this->sort_from_arr,$this->sort_direction_arr,$this->limit_arr);
		
		
		foreach ($this->sub_datasrc_arr as $sub_id=>$sub_info_arr){
			$temp_arr=$this->sub_datasrc_arr[$sub_id]["group_arr"];
			foreach ($temp_arr as $group_id=>$group_info_arr){
				if (!in_array($this->sub_datasrc_arr[$sub_id]["group_arr"][$group_id]["from"],
					$this->sub_datasrc_arr[$sub_id]["datafld_id_arr"])){
					unset($this->sub_datasrc_arr[$sub_id]["group_arr"][$group_id]);		//该组无效
				}
			}
		}

		//组装子区域报表的SQL语句
		foreach ($this->sub_datasrc_arr as $sub_id=>$sub_info_arr){
			$this->sub_datasrc_arr[$sub_id]["sql_sentence"]=$this->recombine_sql_sentence(
				$this->sub_datasrc_arr[$sub_id]["datafld_id_arr"],
				$this->sub_datasrc_arr[$sub_id]["datafld_from_arr"],
				$this->sub_datasrc_arr[$sub_id]["table_from_arr"],
				$this->sub_datasrc_arr[$sub_id]["where_from_arr"],
				$this->sub_datasrc_arr[$sub_id]["group_arr"],
				$this->sub_datasrc_arr[$sub_id]["sort_from_arr"],
				$this->sub_datasrc_arr[$sub_id]["sort_direction_arr"],
				$this->sub_datasrc_arr[$sub_id]["limit_arr"]
			);
		}
		
		$return_info=array();
		$return_info["sql_sentence"]=$this->sql_sentence;
		$return_info["datafld_id_arr"]=$this->datafld_id_arr;
		$return_info["group_arr"]=$this->group_arr;
		$return_info["sub_datasrc_arr"]=$this->sub_datasrc_arr;
		return $return_info;
	}
}
//=================================================================

//=================================================================
class report_body_info{
	private $lay_xml_contents;
	private $parser;
	private $part_tag;
	
	private $report_style="report_style";
	private $report="report";
	private $report_header="report_header";
	private $page_header="page_header";
	private $group_header="group_header";
	private $details="details";
	private $group_footer="group_footer";
	private $page_footer="page_footer";
	private $report_footer="report_footer";
	private $sub_section="sub_section";
	
	private $report_style_arr=array();
	private $report_arr=array();
	private $report_header_arr=array();
	private $page_header_arr=array();
	private $group_header_arr=array();
	private $details_arr=array();
	private $group_footer_arr=array();
	private $page_footer_arr=array();
	private $report_footer_arr=array();
	private $sub_section_arr=array();
	
	private $body_html="";
	private $section_attrs=array();
	
	function __construct($_lay_xml_contents){
		$this->lay_xml_contents=$_lay_xml_contents;
	}
	function __destruct(){
	}
	
	private function startElement($parser_instance, $element_name, $attrs){     //起始标签事件的函数
		switch ($this->part_tag){
			case strtolower($this->report_style):
			case strtolower($this->report):
			case strtolower($this->report_header):
			case strtolower($this->page_header):
			case strtolower($this->group_header):
			case strtolower($this->details):
			case strtolower($this->group_footer):
			case strtolower($this->page_footer):
			case strtolower($this->report_footer):
			case strtolower($this->sub_section):
				$this->body_html .="<".strtolower($element_name);
				foreach ($attrs as $key=>$value){
					$this->body_html .=" ".strtolower($key)."=\"$value\"";
				}
				switch (strtolower($element_name)){
					case "input":
					case "br":
						$this->body_html .=" />";
						break;
					default:
						$this->body_html .=">";
						break;
				}
				break;
			default:
				break;
		}
		
		switch (strtolower($element_name)){
			case strtolower($this->report_style):
				$this->part_tag=$this->report_style;
				break;
			case strtolower($this->report):
				$this->part_tag=$this->report;
				break;
			case strtolower($this->report_header):
				$this->part_tag=$this->report_header;
				break;
			case strtolower($this->page_header):
				$this->part_tag=$this->page_header;
				break;
			case strtolower($this->group_header):
				$this->part_tag=$this->group_header;
				break;
			case strtolower($this->details):
				$this->part_tag=$this->details;
				break;
			case strtolower($this->group_footer):
				$this->part_tag=$this->group_footer;
				break;
			case strtolower($this->page_footer):
				$this->part_tag=$this->page_footer;
				break;
			case strtolower($this->report_footer):
				$this->part_tag=$this->report_footer;
				break;
			case strtolower($this->sub_section):
				$this->part_tag=$this->sub_section;
				break;
			default:
				break;
		}
		
		switch (strtolower($element_name)){
			case strtolower($this->report_style):
			case strtolower($this->report):
			case strtolower($this->report_header):
			case strtolower($this->page_header):
			case strtolower($this->group_header):
			case strtolower($this->details):
			case strtolower($this->group_footer):
			case strtolower($this->page_footer):
			case strtolower($this->report_footer):
			case strtolower($this->sub_section):
				$this->body_html="";
				$this->section_attrs=array();
				$this->section_attrs["part_tag"]=strtolower($element_name);
				foreach ($attrs as $key=>$value){
					$this->section_attrs[strtolower($key)]=$value;
				}
				$height=$this->section_attrs["height"];
				$height=floatval(is_numeric($height)?$height:0);if ($height<=0){$height=0;}
				$this->section_attrs["height"]=$height;
				
				/*
				$height=$this->section_attrs["height"];
				$height=floatval(is_numeric($height)?$height:0);
				if ($height<=0){
					switch (strtolower($element_name)){
						case strtolower($this->report_header):
						case strtolower($this->page_header):
						case strtolower($this->group_header):
						case strtolower($this->details):
						case strtolower($this->group_footer):
						case strtolower($this->page_footer):
						case strtolower($this->report_footer):
							$this->section_attrs=array();			//移除没有高度的部份
							break;
						default:
							break;
					}
				}
				*/
				break;
			default:
				break;
		}
	}

	private function characterData($parser_instance, $XML_data){                //读取数据时的函数 
		switch ($this->part_tag){
			case $this->report_style:
			case $this->report:
			case $this->report_header:
			case $this->page_header:
			case $this->group_header:
			case $this->details:
			case $this->group_footer:
			case $this->page_footer:
			case $this->report_footer:
				$this->body_html .=$XML_data;
				break;
			case $this->sub_section:
				//仍需二次解析，所以将被解析后的$XML_data用htmlspecialchars再打包回去
				$this->body_html .=htmlspecialchars($XML_data);
				break;
			default:
				break;
		}
	}
	
	private function endElement($parser_instance, $element_name){               //结束标签事件的函数
		switch ($this->part_tag){
			case $this->report_style:
			case $this->report:
			case $this->report_header:
			case $this->page_header:
			case $this->group_header:
			case $this->details:
			case $this->group_footer:
			case $this->page_footer:
			case $this->report_footer:
			case $this->sub_section:
				switch (strtolower($element_name)){
					case $this->report_style:
						$this->part_tag='';
						$this->section_attrs["body_html"]=$this->body_html;
						$this->report_style_arr[]=$this->section_attrs;
						break;
					case $this->report:
						$this->part_tag='';
						$this->section_attrs["body_html"]=$this->body_html;
						$this->report_arr[]=$this->section_attrs;
						break;
					case $this->report_header:
						$this->part_tag='';
						$this->section_attrs["body_html"]=$this->body_html;
						$this->report_header_arr[]=$this->section_attrs;
						break;
					case $this->page_header:
						$this->part_tag='';
						$this->section_attrs["body_html"]=$this->body_html;
						$this->page_header_arr[]=$this->section_attrs;
						break;
					case $this->group_header:
						$this->part_tag='';
						$this->section_attrs["body_html"]=$this->body_html;
						$this->group_header_arr[]=$this->section_attrs;
						break;
					case $this->details:
						$this->part_tag='';
						$this->section_attrs["body_html"]=$this->body_html;
						$this->details_arr[]=$this->section_attrs;
						break;
					case $this->group_footer:
						$this->part_tag='';
						$this->section_attrs["body_html"]=$this->body_html;
						$this->group_footer_arr[]=$this->section_attrs;
						break;
					case $this->page_footer:
						$this->part_tag='';
						$this->section_attrs["body_html"]=$this->body_html;
						$this->page_footer_arr[]=$this->section_attrs;
						break;
					case $this->report_footer:
						$this->part_tag='';
						$this->section_attrs["body_html"]=$this->body_html;
						$this->report_footer_arr[]=$this->section_attrs;
						break;
					case $this->sub_section:
						$this->part_tag='';
						$this->section_attrs["body_html"]=$this->body_html;
						//没有 id 和没有 datasrc 的 sub_section 无效
						if ($this->section_attrs["id"]=="" || $this->section_attrs["datasrc"]==""){break;}
						$this->sub_section_arr[$this->section_attrs["id"]]=$this->section_attrs;
						break;
					case "input":
					case "br":
						break;
					default:
						$this->body_html .="</".strtolower($element_name).">";
						break;
				}
				break;
			default:
				break;
		}
	}
	
	private function analysis_extract_xml(){
		$this->parser = xml_parser_create("UTF-8"); //创建一个parser编辑器
		//pool xml_set_object ( resource $parser , object &$object )
		//该函数使得 parser 指定的解析器可以被用在 object 对象中。
		//所有的回叫函数（callback function）
		//都可以由 xml_set_element_handler() 等函数来设置，它们被假定为 object 对象的方法。
		xml_set_object($this->parser, $this);
		//设立标签触发时的相应函数 这里分别为startElement和endElenment
		xml_set_element_handler($this->parser, "startElement", "endElement");
		//设立数据读取时的相应函数
		xml_set_character_data_handler($this->parser, "characterData");
		
		if (!xml_parse($this->parser, $this->lay_xml_contents, true)){
			//报错
			die(sprintf("XML error: %s at line %d",
			xml_error_string(xml_get_error_code($this->parser)),
			xml_get_current_line_number($this->parser)));
		}
		xml_parser_free($this->parser);//关闭和释放parser解析器	
	}
	
	public function get_report_body_info(){
		$this->analysis_extract_xml();
		
		$return_info=array();
		$return_info["report_style_arr"]=$this->report_style_arr;
		$return_info["report_arr"]=$this->report_arr;
		$return_info["report_header_arr"]=$this->report_header_arr;
		$return_info["page_header_arr"]=$this->page_header_arr;
		$return_info["group_header_arr"]=$this->group_header_arr;
		$return_info["details_arr"]=$this->details_arr;
		$return_info["group_footer_arr"]=$this->group_footer_arr;
		$return_info["page_footer_arr"]=$this->page_footer_arr;
		$return_info["report_footer_arr"]=$this->report_footer_arr;
		$return_info["sub_section_arr"]=$this->sub_section_arr;
		return $return_info;
	}
}
//=================================================================

//=================================================================
class report_sub_details_info{
	private $sub_details_html;
	private $parser;
	private $part_tag;
	
	private $sub_details="sub_details";
	
	private $sub_details_arr=array();
	
	private $body_html="";
	private $sub_section_attrs=array();
	
	function __construct($_sub_details_html){
		$this->sub_details_html=$_sub_details_html;
	}
	function __destruct(){
	}
	
	private function startElement($parser_instance, $element_name, $attrs){     //起始标签事件的函数
		switch ($this->part_tag){
			case strtolower($this->sub_details):
				$this->body_html .="<".strtolower($element_name);
				foreach ($attrs as $key=>$value){
					$this->body_html .=" ".strtolower($key)."=\"$value\"";
				}
				switch (strtolower($element_name)){
					case "input":
					case "br":
						$this->body_html .=" />";
						break;
					default:
						$this->body_html .=">";
						break;
				}
				break;
			default:
				break;
		}
		
		switch (strtolower($element_name)){
			case strtolower($this->sub_details):
				$this->part_tag=$this->sub_details;
				break;
			default:
				break;
		}
		
		switch (strtolower($element_name)){
			case strtolower($this->sub_details):
				$this->body_html="";
				$this->sub_section_attrs=array();
				$this->sub_section_attrs["part_tag"]=strtolower($element_name);
				foreach ($attrs as $key=>$value){
					$this->sub_section_attrs[strtolower($key)]=$value;
				}
				$height=$this->sub_section_attrs["height"];
				$height=floatval(is_numeric($height)?$height:0);if ($height<=0){$height=0;}
				$this->sub_section_attrs["height"]=$height;
				break;
			default:
				break;
		}
	}

	private function characterData($parser_instance, $XML_data){                //读取数据时的函数 
		switch ($this->part_tag){
			case $this->sub_details:
				$this->body_html .=$XML_data;
				break;
			default:
				break;
		}
	}
	
	private function endElement($parser_instance, $element_name){               //结束标签事件的函数
		switch ($this->part_tag){
			case $this->sub_details:
				switch (strtolower($element_name)){
					case $this->sub_details:
						$this->part_tag='';
						$this->sub_section_attrs["body_html"]=$this->body_html;
						$this->sub_details_arr[]=$this->sub_section_attrs;
						break;
					case "input":
					case "br":
						break;
					default:
						$this->body_html .="</".strtolower($element_name).">";
						break;
				}
				break;
			default:
				break;
		}
	}
	
	private function analysis_extract_xml(){
		$this->parser = xml_parser_create("UTF-8"); //创建一个parser编辑器
		//pool xml_set_object ( resource $parser , object &$object )
		//该函数使得 parser 指定的解析器可以被用在 object 对象中。
		//所有的回叫函数（callback function）
		//都可以由 xml_set_element_handler() 等函数来设置，它们被假定为 object 对象的方法。
		xml_set_object($this->parser, $this);
		//设立标签触发时的相应函数 这里分别为startElement和endElenment
		xml_set_element_handler($this->parser, "startElement", "endElement");
		//设立数据读取时的相应函数
		xml_set_character_data_handler($this->parser, "characterData");

		$data="<?xml version=\"1.0\" encoding=\"utf-8\"?>\n<root>\n".
			$this->sub_details_html."\n</root>\n";
		if (!xml_parse($this->parser, $data, true)){
			//报错
			die(sprintf("XML error: %s at line %d",
			xml_error_string(xml_get_error_code($this->parser)),
			xml_get_current_line_number($this->parser)));
		}
		xml_parser_free($this->parser);//关闭和释放parser解析器	
	}
	
	public function get_report_sub_details_info(){
		$this->analysis_extract_xml();
		
		$return_info=array();
		$return_info["sub_details_arr"]=$this->sub_details_arr;
		return $return_info;
	}
}
//=================================================================

$cls_report_body_info=new report_body_info($lay_xml_contents);
$return_info=$cls_report_body_info->get_report_body_info();
unset($cls_report_body_info);		//销毁变量对象？不知道行不行
$report_style_arr=$return_info["report_style_arr"];
$report_arr=$return_info["report_arr"];
$report_header_arr=$return_info["report_header_arr"];
$page_header_arr=$return_info["page_header_arr"];
$group_header_arr=$return_info["group_header_arr"];
$details_arr=$return_info["details_arr"];
$group_footer_arr=$return_info["group_footer_arr"];
$page_footer_arr=$return_info["page_footer_arr"];
$report_footer_arr=$return_info["report_footer_arr"];
$sub_section_arr=$return_info["sub_section_arr"];
unset($return_info);		//销毁变量对象？不知道行不行
/*
print_r($report_arr);
print_r($report_header_arr);
print_r($page_header_arr);
print_r($group_header_arr);
print_r($details_arr);
print_r($group_footer_arr);
print_r($page_footer_arr);
print_r($report_footer_arr);
*/

$cls_report_datasrc_info=new report_datasrc_info($lay_xml_contents);
$return_info=$cls_report_datasrc_info->get_report_datasrc_info();
unset($cls_report_datasrc_info);		//销毁变量对象？不知道行不行
$sql_sentence=$return_info["sql_sentence"];
$datafld_id_arr=$return_info["datafld_id_arr"];
$group_arr=$return_info["group_arr"];
$sub_datasrc_arr=$return_info["sub_datasrc_arr"];
$sql=revert_to_the_available_sql($sql_sentence);//echo $sql ;
unset($return_info);		//销毁变量对象？不知道行不行
/*
echo $sql ;
print_r($return_info);
exit;
*/

unset($lay_xml_contents);		//销毁变量对象？不知道行不行
?>

<?php
//整理分组
$group_id_list=array();
$group_html_list=array();
$group_current_value=array();
$group_next_value=array();
foreach ($group_arr as $group_id=>$group_info_arr){		//始初化
	$group_current_value[$group_id]="";
	$group_next_value[$group_id]="";
	$group_id_list[]=$group_id;
	$group_html_list[]=array();
}
$group_html_list[count($group_id_list)]=array();		//将细节内容放置到最里分组的里一层


$conn=werp_db_connect();
if(!($result=mysql_query($sql,$conn))){
	trigger_error(mysql_error(),E_USER_ERROR);
}
for ($count=0;$count<mysql_num_fields($result);$count++){
	$fields[]=mysql_fetch_field($result,$count);
	$columns =mysql_fetch_field($result,$count);
	$fields_type[]=$columns->type;
}
while($row=mysql_fetch_array($result,MYSQL_ASSOC)){$instances[]=$row;}

$html_display_buffer='
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
';
foreach ($report_style_arr as $key=>$value){$html_display_buffer .=$value["body_html"];}	//报表的 CSS
$html_display_buffer .='
</head>
<body bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0">
';

function debug_body_html_height($body_html,$height){return $body_html;
	$font_size=9;
	$div_height=$height-1;
	if ($div_height<$font_size){$div_height=$font_size;}
	$body_html='
		<table cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td><div style="border-top: 1pt inset black;border-right: 1pt inset black;'
			.'height:'.$div_height.'pt;font-size:'.$font_size.'pt;">'.$font_size.'&nbsp;'.$height.'</div></td>
			<td>'.$body_html.'</td>
		</tr>
		</table>';
	return $body_html;
}

function add_additional_space($height,$width=1){
	$body_html=
		'<table cellpadding="0" cellspacing="0" border="0">'.
		'<tr><td><div style="height:'.$height.'pt;width:'.$width.'pt;"></div></td></tr></table>';
	return debug_body_html_height($body_html,$height);
}

function test_report_class_expression($expression){
	$expression=trim($expression);
	if ($expression=="" || (
		substr($expression,0,1)!="{" && 
		substr(strrev($expression),0,1)!="}")){return false;}		//表达式有误
	$report_class_expression=substr($expression,1,strlen($expression)-2);

	$report_class_expression_arr=explode("::",$report_class_expression,2);
	$report_class_expression_arr[0]=trim($report_class_expression_arr[0]);
	$report_class_expression_arr[1]=trim($report_class_expression_arr[1]);
	$report_class_name=$report_class_expression_arr[0];
	$report_class_m__p=$report_class_expression_arr[1];
	while (true){
		if (substr($report_class_m__p,0,1)!="\$"){break;}
		$report_class_m__p=substr($report_class_m__p,1);
	}
	
	$char_pos=stripos($report_class_m__p,"(");
	if (!($char_pos===false)){$report_class_m__p=substr($report_class_m__p,0,$char_pos);}
	$char_pos=stripos($report_class_m__p,"[");
	if (!($char_pos===false)){$report_class_m__p=substr($report_class_m__p,0,$char_pos);}
	if (!class_exists($report_class_name,false)){return false;}		//没发现这个类
	if (!method_exists($report_class_name,$report_class_m__p) && 
		!property_exists($report_class_name,$report_class_m__p)){
		return false;	//在 report_class_name 类中没有发现这个方法或属性
	}
	
	return $report_class_expression;
}

function calculate_expression($row,$data_row,$sub_row,$sub_data_row,$expression){
	$report_class_expression=test_report_class_expression($expression);
	if ($report_class_expression===false){return $report_class_expression;}
	/*
	report_special::$row=$row;
	report_special::$data_row=$data_row;
	report_special::$sub_row=$sub_row;
	report_special::$sub_data_row=$sub_data_row;
	*/
	report_special::$datarow=$row;
	report_special::$sub_datarow=$sub_row;

	@eval("\$return_value=$report_class_expression;");
	return $return_value;
}

function fill_data_from_flag(
	$row,$data_row,$sub_row,$sub_data_row,$body_html,$growup_count,$bbbbb_flag_arr,$eeeee_flag_arr){
	if ((!is_array($bbbbb_flag_arr)) || (!is_array($eeeee_flag_arr))){return $body_html;}
	if (count($bbbbb_flag_arr)<=0 || count($eeeee_flag_arr)<=0){return $body_html;}

	if (!is_numeric($growup_count)){$growup_count=0;}
	$bbbbb_flag=array_pop($bbbbb_flag_arr);
	$eeeee_flag=array_pop($eeeee_flag_arr);
	
	$segment_arr=array();
	$expression_arr=array();

	while (true){
		$bbbbb_pos=false;
		$eeeee_pos=false;
		$bbbbb_pos=stripos($body_html,$bbbbb_flag);
		if (!($bbbbb_pos===false)){
			$eeeee_pos=stripos($body_html,$eeeee_flag,$bbbbb_pos+strlen($bbbbb_flag));
		}
		if (($bbbbb_pos===false) || ($eeeee_pos===false)){
			$segment_arr[]=$body_html;break;
		}
		
		$segment_arr[]=substr($body_html,0,$bbbbb_pos);
		$expression_arr[]=substr($body_html,$bbbbb_pos,$eeeee_pos-$bbbbb_pos+1);
		$body_html=substr($body_html,$eeeee_pos+1);
	}
	
	foreach ($expression_arr as $key=>$value){
		if ($growup_count>1){
			$expression_arr[$key]="";
		}else{
			$expression_arr[$key]=calculate_expression($row,$data_row,$sub_row,$sub_data_row,$value);
		}
	}
	
	if (is_array($bbbbb_flag_arr) && is_array($eeeee_flag_arr) &&
		count($bbbbb_flag_arr)>0 && count($eeeee_flag_arr)>0){
		foreach ($segment_arr as $key=>$value){
			$segment_arr[$key]=fill_data_from_flag(
				$row,$data_row,$sub_row,$sub_data_row,$value,$growup_count,$bbbbb_flag_arr,$eeeee_flag_arr);
		}
	}
	
	$body_html="";
	foreach ($segment_arr as $key=>$value){
		$body_html=$body_html.$segment_arr[$key].$expression_arr[$key];
	}
	
	return $body_html;
}

function fill_data_from_expression(
	$row,$data_row,$sub_row,$sub_data_row,$body_html,$growup_count){
	$bbbbb_flag_arr=array();
	$bbbbb_flag_arr[]="{report_class::";
	$bbbbb_flag_arr[]="{report_special::";
	$eeeee_flag_arr=array();
	$eeeee_flag_arr[]="}";	
	$eeeee_flag_arr[]="}";	
	
	return fill_data_from_flag(
		$row,$data_row,$sub_row,$sub_data_row,$body_html,$growup_count,$bbbbb_flag_arr,$eeeee_flag_arr);
}

function extract_page_footer_body_html($row,$data_row,$sub_row,$sub_data_row,$page_footer_arr_fill_data){
	$page_footer_body_html="";
	if (!is_array($page_footer_arr_fill_data)){return $page_footer_body_html;}
	
	foreach ($page_footer_arr_fill_data as $page_footer_key=>$page_footer_value){
		$growup_count=$page_footer_value["growup_count"];
		$pf_body_html=$page_footer_value["body_html"];
		$pf_height=$page_footer_value["height"];

		//进行表达式计算
		$pf_body_html=fill_data_from_expression($row,$data_row,$sub_row,$sub_data_row,$pf_body_html,$growup_count);
		if ($pf_height<=0){
		}else{
			$page_footer_body_html=$page_footer_body_html.$pf_body_html;
		}
	}
	return $page_footer_body_html;
}

function fill_data($template_arr,$instances,$data_row,$sub_section_arr,$sub_datasrc_arr){
	$row=$instances[$data_row];
	$return_arr=array();
	
	$temp_count=0;
	$temp_arr=array();
	foreach ($template_arr as $key=>$value){
		$suppress=$value["suppress"];
		$suppress_section=calculate_expression($row,$data_row,null,null,$suppress);
		if ((bool)($suppress_section)){$value["height"]=0;}	//压制条件成立
		
		if (trim($value["sub_section"])!=""){
			$sub_datasrc=$sub_section_arr[$value["sub_section"]]["datasrc"];
			$sub_details_html=$sub_section_arr[$value["sub_section"]]["body_html"];
			
			$cls_report_sub_details_info=new report_sub_details_info($sub_details_html);
			$return_info=$cls_report_sub_details_info->get_report_sub_details_info();
			$sub_details_arr=$return_info["sub_details_arr"];

			$sql=$sub_datasrc_arr[$sub_datasrc]["sql_sentence"];
			//将当前父记录对应内容替换到子查询的条件语句中
			foreach ($row as $row_key=>$row_value){
				$sql=str_ireplace("{datafld:".$row_key."}",addslashes($row_value),$sql);
			}
			$sql=revert_to_the_available_sql($sql);
			$sub_conn=werp_db_connect();
			if (!($sub_result=mysql_query($sql,$sub_conn))){continue;}	//出错或者没有记录
			$sub_instances=array();
			while($sub_row=mysql_fetch_array($sub_result,MYSQL_ASSOC)){
				$sub_instances[]=$sub_row;
			}
			for ($sub_data_row=0;$sub_data_row<count($sub_instances);$sub_data_row++){
				foreach ($sub_details_arr as $sub_details_key=>$sub_details_value){
					$sub_height=$sub_details_value["height"];
					$sub_suppress=$sub_details_value["suppress"];
					$sub_body_html=$sub_details_value["body_html"];
					
					$sub_row=$sub_instances[$sub_data_row];
					$sub_suppress_section=
						calculate_expression($row,$data_row,$sub_row,$sub_data_row,$sub_suppress);
					if (((bool)($suppress_section)) || ((bool)($sub_suppress_section))){
						$sub_height=0;	//父或子其中一个压制条件成立
					}
					foreach ($value as $k=>$v){
						switch (strtolower($k)){
							case "print_at_bottom_of":
							case "new_page_after":
							case "reset_page_number_after":
								//子区域没到子数据集最后一条记录不继承以上三项父属性
								if ($sub_data_row<count($sub_instances)-1){$v="";}
								$temp_arr[$temp_count][$k]=strtolower($v);
								break;
							default:
								$temp_arr[$temp_count][$k]=$v;
								break;
						}
					}
				
					$temp_arr[$temp_count]["data_row"]=$data_row;
					$temp_arr[$temp_count]["height"]=$sub_height;
					$temp_arr[$temp_count]["datasrc"]=$sub_datasrc;
					$temp_arr[$temp_count]["suppress"]=$sub_suppress;
					$temp_arr[$temp_count]["body_html"]=$sub_body_html;
					$temp_arr[$temp_count]["sub_data_row"]=$sub_data_row;
					$temp_arr[$temp_count]["sub_row"]=$sub_row;
					$temp_arr[$temp_count]["data_row"]=$data_row;
					$temp_arr[$temp_count]["row"]=$row;
					$temp_count++;
				}
			}
		}else{
			foreach ($value as $k=>$v){
				switch (strtolower($k)){
					case "print_at_bottom_of":
					case "new_page_after":
					case "reset_page_number_after":
						$temp_arr[$temp_count][$k]=strtolower($v);
						break;
					default:
						$temp_arr[$temp_count][$k]=$v;
						break;
				}
			}
			
			$temp_arr[$temp_count]["data_row"]=$data_row;
			$temp_arr[$temp_count]["row"]=$row;
			$temp_count++;
		}
	}
	$template_arr=$temp_arr;unset($temp_arr);		//将整理过后的行信息设置回模板数组

	//grow up line data
	foreach ($template_arr as $key=>$value){
		$body_html=$value["body_html"];
		$datafld_arr=array();
		$growup_arr=array();
		$flag_len_arr=array();
		
		foreach ($row as $row_key=>$row_value){
			$datafld="{datafld:".$row_key."}";
			$growup_begin="{growup:";
			$growup_end="}";
			$datafld_pos=0;
			while (true){
				$growup=-1;
				$flag_len=strlen($datafld);
				$datafld_pos=stripos($body_html,$datafld,$datafld_pos);
				if ($datafld_pos===false){break;}else{
					$growup_begin_pos=$datafld_pos+strlen($datafld);
					if (strtolower(substr($body_html,$growup_begin_pos,strlen($growup_begin)))
						==strtolower($growup_begin)){
						$growup_end_pos=stripos($body_html,$growup_end,$growup_begin_pos);
						if ($growup_end_pos===false){}else{
							$growup=substr($body_html,
								$growup_begin_pos+strlen($growup_begin),$growup_end_pos-
								$growup_begin_pos-strlen($growup_begin));
							$growup=floatval(is_numeric($growup)?$growup:0);
							$flag_len=$growup_end_pos-$datafld_pos+strlen($growup_end);
						}
					}
					$datafld_arr[$datafld_pos]=$row_value;
					$growup_arr[$datafld_pos]=$growup;
					$flag_len_arr[$datafld_pos]=$flag_len;
				}
				$datafld_pos=$datafld_pos+$flag_len;		//移动指针，查找下一个相同的字段
			}
		}
		
		$sub_section=$value["sub_section"];
		$sub_datasrc=$value["datasrc"];
		$sub_row=$value["sub_row"];
		if ($sub_section!="" && $sub_datasrc!="" && is_array($sub_row)){	//有子记录集
			foreach ($sub_row as $row_key=>$row_value){
				$datafld="{".$sub_datasrc.":datafld:".$row_key."}";
				$growup_begin="{growup:";
				$growup_end="}";
				$datafld_pos=0;
				while (true){
					$growup=-1;
					$flag_len=strlen($datafld);
					$datafld_pos=stripos($body_html,$datafld,$datafld_pos);
					if ($datafld_pos===false){break;}else{
						$growup_begin_pos=$datafld_pos+strlen($datafld);
						if (strtolower(substr($body_html,$growup_begin_pos,strlen($growup_begin)))
							==strtolower($growup_begin)){
							$growup_end_pos=stripos($body_html,$growup_end,$growup_begin_pos);
							if ($growup_end_pos===false){}else{
								$growup=substr($body_html,
									$growup_begin_pos+strlen($growup_begin),$growup_end_pos-
									$growup_begin_pos-strlen($growup_begin));
								$growup=floatval(is_numeric($growup)?$growup:0);
								$flag_len=$growup_end_pos-$datafld_pos+strlen($growup_end);
							}
						}
						$datafld_arr[$datafld_pos]=$row_value;
						$growup_arr[$datafld_pos]=$growup;
						$flag_len_arr[$datafld_pos]=$flag_len;
					}
					$datafld_pos=$datafld_pos+$flag_len;		//移动指针，查找下一个相同的字段
				}
			}
		}

		ksort($datafld_arr);
		ksort($growup_arr);
		ksort($flag_len_arr);
		
		$growup_count=0;
		while (true){
			$body_html_fill_data="";
			if (count($datafld_arr)<=0){$body_html_fill_data=$body_html;}	//没有字段要填充

			$fill_complete=true;			
			$temp_pos=0;
			$datafld_count=0;
			foreach ($datafld_arr as $datafld_pos=>$datafld_value){
				$disp_value="";
				$next_value="";
				$growup=$growup_arr[$datafld_pos];
				$datafld_value_arr=explode("\n",$datafld_value,2);
				if ($growup>0){
					$disp_value=mb_substr($datafld_value,0,$growup,"utf-8");
					$next_value="";
					if (mb_stripos($disp_value,"\n",0,"utf-8")===false){
						$next_length=mb_strlen($datafld_value,"utf-8")-$growup;
						if ($next_length>0){
							$next_value=mb_substr($datafld_value,$growup,$next_length,"utf-8");
						}
					}else{
						$disp_value=$datafld_value_arr[0];
						$next_value=$datafld_value_arr[1];
					}
				}
				if ($growup==0){
					if (mb_stripos($datafld_value,"\n",0,"utf-8")===false){
						$disp_value=$datafld_value;
						$next_value="";
					}else{
						$disp_value=$datafld_value_arr[0];
						$next_value=$datafld_value_arr[1];
					}
				}
				if ($growup<0){
					$disp_value=$datafld_value;
					$next_value="";
				}
				
				$disp_value=$disp_value."";
				$next_value=$next_value."";
				$datafld_arr[$datafld_pos]=$next_value;		//剩下的内容下一行显示
				
				$body_html_fill_data .=substr($body_html,$temp_pos,$datafld_pos-$temp_pos).
					htmlspecialchars($disp_value);
				$temp_pos=$datafld_pos+$flag_len_arr[$datafld_pos];

				$datafld_count=$datafld_count+1;
				if ($datafld_count==count($datafld_arr)){		//到了最后一个字段了
					$body_html_fill_data .=substr($body_html,$temp_pos);
				}
				$fill_complete=$fill_complete && (($next_value=="")?true:false);
			}
			$temp_arr=array();
			$temp_arr=$value;
			if (strtolower($temp_arr["part_tag"])=="page_header" || 
				strtolower($temp_arr["part_tag"])=="page_footer"){		
				//page_header 和 page_footer 不支持属性 print_at_bottom_of 和 new_page_after
				$temp_arr["print_at_bottom_of"]="";
				$temp_arr["new_page_after"]="";
			}
			if (!$fill_complete){	//未完，禁用以下属性
				$temp_arr["print_at_bottom_of"]="";
				$temp_arr["new_page_after"]="";
				$temp_arr["reset_page_number_after"]="";
			}

			$growup_count++;
			while (true){
				$label_begin="{label:";
				$label_end="}";
				$label_begin_pos=stripos($body_html_fill_data,$label_begin,0);
				if ($label_begin_pos===false){break;}
				$label_end_pos=stripos($body_html_fill_data,$label_end,$label_begin_pos);
				if ($label_end_pos===false){break;}
				if ($growup_count==1){		//growup 第一次不能抛弃标签
					$body_html_fill_data=substr($body_html_fill_data,0,$label_begin_pos).
						substr($body_html_fill_data,
							($label_begin_pos+strlen($label_begin)),$label_end_pos-
							($label_begin_pos+strlen($label_begin))).
						substr($body_html_fill_data,$label_end_pos+strlen($label_end));
				}else{
					$body_html_fill_data=substr($body_html_fill_data,0,$label_begin_pos).
						substr($body_html_fill_data,$label_end_pos+strlen($label_end));
				}
			}
			
			$temp_arr["growup_count"]=$growup_count;
			$temp_arr["body_html"]=debug_body_html_height($body_html_fill_data,$temp_arr["height"]);
			$return_arr[]=$temp_arr;
			if ($fill_complete){break;}
		}
	}
	return $return_arr;
}

function page_info($body_html,$reset_page_number_after){
	$page_info_arr=array();
	$page_info_arr["body_html"]=$body_html;
	$page_info_arr["reset_page_number_after"]=$reset_page_number_after;
	return $page_info_arr;
}

if (is_array($instances)){		//有数据行要显示
	$report_header_arr_fill_data=array();
	$page_header_arr_fill_data=array();
	$group_header_arr_fill_data=array();
	$details_arr_fill_data=array();
	$group_footer_arr_fill_data=array();
	$page_footer_arr_fill_data=array();
	$report_footer_arr_fill_data=array();

	$data_row=0;
	while (true){
		if ($data_row==0){		//第一行数据出现了，填充报表头内容
			$report_header_arr_fill_data=fill_data($report_header_arr,$instances,$data_row,$sub_section_arr,$sub_datasrc_arr);
		}

		if ($instances[$data_row]["wel_so_no"]=="SN0000000016"){
			$xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx=100;	//only for 断点测试
		}
		
		//将细节内容放置到最里分组的里一层
		$details_arr_fill_data=fill_data($details_arr,$instances,$data_row,$sub_section_arr,$sub_datasrc_arr);
		$template_arr=$group_html_list[count($group_id_list)];
		foreach ($details_arr_fill_data as $key=>$value){$template_arr[]=$value;}
		$group_html_list[count($group_id_list)]=$template_arr;
		
		foreach ($group_arr as $group_id=>$group_info_arr){		//取得当前行键值内容
			$group_current_value[$group_id]=$instances[$data_row][$group_arr[$group_id]["from"]];
		}

		$display_group="";
		if ($data_row==count($instances)-1){	//到达了最后一行数据，将显示所有分组
			foreach ($group_arr as $group_id=>$group_info_arr){
				$display_group=$group_id;
				break;
			}
		}else{
			foreach ($group_arr as $group_id=>$group_info_arr){		//取得下一行键值内容
				$group_next_value[$group_id]=$instances[$data_row+1][$group_arr[$group_id]["from"]];
			}
			foreach ($group_arr as $group_id=>$group_info_arr){		//进行分组测试
				$current_value=strtolower($group_current_value[$group_id]);
				$next_value=strtolower($group_next_value[$group_id]);
				if ($current_value!=$next_value){
					$display_group=$group_id;
					break;
				}
			}
		}
		
		if ($display_group=="wel_cus_code"){
			$xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx=100;	//only for 断点测试
		}

		if ($display_group!=""){		//分组出现了，从最里的分组开始加入
			for($count=count($group_id_list)-1;$count>=array_search($display_group,$group_id_list);$count--){
				$template_arr=array();
				foreach ($group_header_arr as $key=>$value){
					if (strtolower($group_id_list[$count])==strtolower($value["group_id"])){$template_arr[]=$value;}
				}
				$group_header_arr_fill_data=fill_data($template_arr,$instances,$data_row,$sub_section_arr,$sub_datasrc_arr);
				
				$template_arr=array();
				foreach ($group_footer_arr as $key=>$value){
					if (strtolower($group_id_list[$count])==strtolower($value["group_id"])){$template_arr[]=$value;}
				}
				$group_footer_arr_fill_data=fill_data($template_arr,$instances,$data_row,$sub_section_arr,$sub_datasrc_arr);

				$template_arr=array();
				foreach ($group_html_list[$count] as $key=>$value){$template_arr[]=$value;}
				foreach ($group_header_arr_fill_data as $key=>$value){$template_arr[]=$value;}
				foreach ($group_html_list[$count+1] as $key=>$value){$template_arr[]=$value;}
				foreach ($group_footer_arr_fill_data as $key=>$value){$template_arr[]=$value;}
				$group_html_list[$count]=$template_arr;
				$group_html_list[$count+1]=array();
			}
		}

		if ($data_row==count($instances)-1){		//到达了最后一行数据
			$report_footer_arr_fill_data=fill_data($report_footer_arr,$instances,$data_row,$sub_section_arr,$sub_datasrc_arr);
			break;
		}
		$data_row=$data_row+1;
	}

	
	$value=$report_arr[0];
	$content_height=floatval(is_numeric($value["content_height"])?$value["content_height"]:0);
	if ($content_height<=0){	//没限定单张纸的内容高度，用流水账显示一篇即可
		$data_row=0;	//第一条记录
		$page_header_arr_fill_data=fill_data($page_header_arr,$instances,$data_row,$sub_section_arr,$sub_datasrc_arr);
		$data_row=count($instances)-1;	//最后一条记录
		$page_footer_arr_fill_data=fill_data($page_footer_arr,$instances,$data_row,$sub_section_arr,$sub_datasrc_arr);

		$template_arr=array();
		foreach ($report_header_arr_fill_data as $key=>$value){$template_arr[]=$value;}
		foreach ($page_header_arr_fill_data as $key=>$value){$template_arr[]=$value;}
		foreach ($group_html_list[0] as $key=>$value){$template_arr[]=$value;}
		foreach ($report_footer_arr_fill_data as $key=>$value){$template_arr[]=$value;}
		foreach ($page_footer_arr_fill_data as $key=>$value){$template_arr[]=$value;}

		$page_number=1;
		$total_page_count=1;
		$page_n_of_m="Page 1 of 1";
		foreach ($template_arr as $key=>$value){
			$html_display_buffer .=
				str_ireplace("{report:page_number}",$page_number,
				str_ireplace("{report:total_page_count}",$total_page_count,
				str_ireplace("{report:page_n_of_m}",$page_n_of_m,$value["body_html"])));
		}
	}else{		//限定单张纸的内容高度，分页显示报表
		$data_row=0;	//第一条记录
		$page_header_arr_fill_data=fill_data($page_header_arr,$instances,$data_row,$sub_section_arr,$sub_datasrc_arr);
		/*
		$data_row=count($instances)-1;	//最后一条记录
		$page_footer_arr_fill_data=fill_data($page_footer_arr,$instances,$data_row,$sub_section_arr,$sub_datasrc_arr);
		*/

		$template_arr=array();
		foreach ($report_header_arr_fill_data as $key=>$value){$template_arr[]=$value;}
		foreach ($page_header_arr_fill_data as $key=>$value){$template_arr[]=$value;}
		foreach ($group_html_list[0] as $key=>$value){$template_arr[]=$value;}
		foreach ($report_footer_arr_fill_data as $key=>$value){$template_arr[]=$value;}
		//foreach ($page_footer_arr_fill_data as $key=>$value){$template_arr[]=$value;}

		$report_page_arr=array();
		$report_html="";
		$temp_height=0;
		$previous_reset_page_number_after="";
		$current_reset_page_number_after="";
		$page_printed_complete=false;
		$only_page_header_information=false;
		foreach ($template_arr as $key=>$value){
			$part_tag=strtolower($value["part_tag"])."";
			switch ($part_tag){
				case "report_header":
				case "page_header":
					$row=$value["row"];
					$data_row=$value["data_row"];
					$sub_row=$value["sub_row"];
					$sub_data_row=$value["sub_data_row"];
					
					$print_at_bottom_of=$value["print_at_bottom_of"];
					$new_page_after=$value["new_page_after"];
					$growup_count=$value["growup_count"];
					$body_html=$value["body_html"];
					$height=$value["height"];

					if ($page_printed_complete){
						$report_html="";
						$temp_height=0;
						$previous_reset_page_number_after="";
						$current_reset_page_number_after="";
						$page_printed_complete=false;
						$only_page_header_information=true;
					}
					
					$previous_reset_page_number_after=$current_reset_page_number_after;
					if (strtolower($value["reset_page_number_after"])=="true"){
						$current_reset_page_number_after="true";
					}

					//进行表达式计算
					$body_html=fill_data_from_expression($row,$data_row,$sub_row,$sub_data_row,$body_html,$growup_count);
					if ($height<=0){
						if (($print_at_bottom_of=="true") && (!$only_page_header_information) && (!$page_printed_complete)){
							$additional_html=add_additional_space($content_height-$temp_height-$height);
							$report_page_arr[]=page_info($report_html.$additional_html.$body_html,$current_reset_page_number_after);
							$page_printed_complete=true;
						}
					}else if ($height>=$content_height){
						if ($temp_height>0){$report_page_arr[]=page_info($report_html,$previous_reset_page_number_after);}
						$report_page_arr[]=page_info($body_html,$current_reset_page_number_after);
						$page_printed_complete=true;
					}else if ($temp_height+$height>$content_height){
						$report_page_arr[]=page_info($report_html,$previous_reset_page_number_after);
						$report_html=$body_html;
						$temp_height=$height;
					}else if ($temp_height+$height==$content_height){
						$report_page_arr[]=page_info($report_html.$body_html,$current_reset_page_number_after);
						$page_printed_complete=true;
					}else if ($print_at_bottom_of=="true"){
						$additional_html=add_additional_space($content_height-$temp_height-$height);
						$report_page_arr[]=page_info($report_html.$additional_html.$body_html,$current_reset_page_number_after);
						$page_printed_complete=true;
					}else if ($new_page_after=="true"){
						$report_page_arr[]=page_info($report_html.$body_html,$current_reset_page_number_after);
						$page_printed_complete=true;
					}else{
						$report_html=$report_html.$body_html;
						$temp_height=$temp_height+$height;
						$only_page_header_information=false;	//有内容，不只是 page header
					}
					break;
				case "group_header":
				case "details":
				case "group_footer":
				case "report_footer":
					$row=$value["row"];
					$data_row=$value["data_row"];
					$sub_row=$value["sub_row"];
					$sub_data_row=$value["sub_data_row"];

					$print_at_bottom_of=$value["print_at_bottom_of"];
					$new_page_after=$value["new_page_after"];
					$growup_count=$value["growup_count"];
					$body_html=$value["body_html"];
					$height=$value["height"];

					if ($page_printed_complete){
						$report_html="";
						$temp_height=0;
						$previous_reset_page_number_after="";
						$current_reset_page_number_after="";
						$page_printed_complete=false;
						$only_page_header_information=true;

						$page_header_arr_fill_data=fill_data($page_header_arr,$instances,$data_row,$sub_section_arr,$sub_datasrc_arr);
						foreach ($page_header_arr_fill_data as $page_header_key=>$page_header_value){
							if (strtolower($page_header_value["reset_page_number_after"])=="true"){
								$current_reset_page_number_after="true";
							}
							$ph_growup_count=$page_header_value["growup_count"];
							$ph_body_html=$page_header_value["body_html"];
							$ph_height=$page_header_value["height"];
							
							//进行表达式计算
							$ph_body_html=fill_data_from_expression($row,$data_row,$sub_row,$sub_data_row,$ph_body_html,$ph_growup_count);
							if ($ph_height<=0){
							}else{
								$report_html=$report_html.$ph_body_html;
								$temp_height=$temp_height+$ph_height;
							}
						}
					}

					$previous_reset_page_number_after=$current_reset_page_number_after;
					if (strtolower($value["reset_page_number_after"])=="true"){
						$current_reset_page_number_after="true";
					}
					
					$page_footer_reset_page_number_after="";
					$page_footer_body_html="";
					$page_footer_height=0;
					$page_footer_arr_fill_data=fill_data($page_footer_arr,$instances,$data_row,$sub_section_arr,$sub_datasrc_arr);
					foreach ($page_footer_arr_fill_data as $page_footer_key=>$page_footer_value){
						if (strtolower($page_footer_value["reset_page_number_after"])=="true"){
							$page_footer_reset_page_number_after="true";
						}
						$pf_height=$page_footer_value["height"];

						if ($pf_height<=0){
						}else{
							$page_footer_height=$page_footer_height+$pf_height;
						}
					}
					
					//进行表达式计算
					$body_html=fill_data_from_expression($row,$data_row,$sub_row,$sub_data_row,$body_html,$growup_count);
					if ($height<=0){
						if (($print_at_bottom_of=="true") && (!$only_page_header_information) && (!$page_printed_complete)){
							$additional_html=add_additional_space($content_height-$temp_height-$height-$page_footer_height);
							if ($current_reset_page_number_after=="true"){$page_footer_reset_page_number_after="true";}
							$page_footer_body_html=extract_page_footer_body_html($row,$data_row,$sub_row,$sub_data_row,$page_footer_arr_fill_data);
							$report_page_arr[]=page_info($report_html.$additional_html.$body_html.$page_footer_body_html,$page_footer_reset_page_number_after);
							$page_printed_complete=true;
						}
					}else if ($height>$content_height){
						$report_page_arr[]=page_info($report_html.$body_html,$current_reset_page_number_after);
						if ($page_footer_height>0){		//把页脚印出来
							$page_footer_body_html=extract_page_footer_body_html($row,$data_row,$sub_row,$sub_data_row,$page_footer_arr_fill_data);
							$report_page_arr[]=page_info($page_footer_body_html,$page_footer_reset_page_number_after);
						}
						$page_printed_complete=true;
					}else if ($height==$content_height){
						if ($temp_height>0){$report_page_arr[]=page_info($report_html,$previous_reset_page_number_after);}
						$report_page_arr[]=page_info($body_html,$current_reset_page_number_after);
						if ($page_footer_height>0){		//把页脚印出来
							$page_footer_body_html=extract_page_footer_body_html($row,$data_row,$sub_row,$sub_data_row,$page_footer_arr_fill_data);
							$report_page_arr[]=page_info($page_footer_body_html,$page_footer_reset_page_number_after);
						}
						$page_printed_complete=true;
					}else if ($temp_height+$height>$content_height){
						$report_page_arr[]=page_info($report_html.$body_html,$current_reset_page_number_after);
						if ($page_footer_height>0){		//把页脚印出来
							$page_footer_body_html=extract_page_footer_body_html($row,$data_row,$sub_row,$sub_data_row,$page_footer_arr_fill_data);
							$report_page_arr[]=page_info($page_footer_body_html,$page_footer_reset_page_number_after);
						}
						$page_printed_complete=true;
					}else if ($temp_height+$height==$content_height){
						$report_page_arr[]=page_info($report_html.$body_html,$current_reset_page_number_after);
						if ($page_footer_height>0){		//把页脚印出来
							$page_footer_body_html=extract_page_footer_body_html($row,$data_row,$sub_row,$sub_data_row,$page_footer_arr_fill_data);
							$report_page_arr[]=page_info($page_footer_body_html,$page_footer_reset_page_number_after);
						}
						$page_printed_complete=true;
					}else if ($page_footer_height>$content_height){
						$report_page_arr[]=page_info($report_html.$body_html,$current_reset_page_number_after);
						if ($page_footer_height>0){		//把页脚印出来
							$page_footer_body_html=extract_page_footer_body_html($row,$data_row,$sub_row,$sub_data_row,$page_footer_arr_fill_data);
							$report_page_arr[]=page_info($page_footer_body_html,$page_footer_reset_page_number_after);
						}
						$page_printed_complete=true;
					}else if ($page_footer_height==$content_height){
						$report_page_arr[]=page_info($report_html.$body_html,$current_reset_page_number_after);
						if ($page_footer_height>0){		//把页脚印出来
							$page_footer_body_html=extract_page_footer_body_html($row,$data_row,$sub_row,$sub_data_row,$page_footer_arr_fill_data);
							$report_page_arr[]=page_info($page_footer_body_html,$page_footer_reset_page_number_after);
						}
						$page_printed_complete=true;
					}else if ($temp_height+$height+$page_footer_height>$content_height){
						$report_page_arr[]=page_info($report_html.$body_html,$current_reset_page_number_after);
						if ($page_footer_height>0){		//把页脚印出来
							$page_footer_body_html=extract_page_footer_body_html($row,$data_row,$sub_row,$sub_data_row,$page_footer_arr_fill_data);
							$report_page_arr[]=page_info($page_footer_body_html,$page_footer_reset_page_number_after);
						}
						$page_printed_complete=true;
					}else if ($temp_height+$height+$page_footer_height==$content_height){
						if ($current_reset_page_number_after=="true"){$page_footer_reset_page_number_after="true";}
						$page_footer_body_html=extract_page_footer_body_html($row,$data_row,$sub_row,$sub_data_row,$page_footer_arr_fill_data);
						$report_page_arr[]=page_info($report_html.$body_html.$page_footer_body_html,$page_footer_reset_page_number_after);
						$page_printed_complete=true;
					}else if ($print_at_bottom_of=="true"){
						$additional_html=add_additional_space($content_height-$temp_height-$height-$page_footer_height);
						if ($current_reset_page_number_after=="true"){$page_footer_reset_page_number_after="true";}
						$page_footer_body_html=extract_page_footer_body_html($row,$data_row,$sub_row,$sub_data_row,$page_footer_arr_fill_data);
						$report_page_arr[]=page_info($report_html.$additional_html.$body_html.$page_footer_body_html,$page_footer_reset_page_number_after);
						$page_printed_complete=true;
					}else if ($new_page_after=="true"){
						if ($current_reset_page_number_after=="true"){$page_footer_reset_page_number_after="true";}
						$page_footer_body_html=extract_page_footer_body_html($row,$data_row,$sub_row,$sub_data_row,$page_footer_arr_fill_data);
						$report_page_arr[]=page_info($report_html.$body_html.$page_footer_body_html,$page_footer_reset_page_number_after);
						$page_printed_complete=true;
					}else{
						//测试跟下一个部分有效高度比较，这里的测试很关键
						$next_height=0;
						$next_page_footer_height=0;

						for ($next_key=$key+1;$next_key<count($template_arr);$next_key++){
							if ($template_arr[$next_key]["height"]<=0){
							}else{
								$next_data_row=$template_arr[$next_key]["data_row"];
								$next_height=$template_arr[$next_key]["height"];
								$next_page_footer_height=0;
								$next_page_footer_arr_fill_data=fill_data($page_footer_arr,$instances,$next_data_row,$sub_section_arr,$sub_datasrc_arr);
								foreach ($next_page_footer_arr_fill_data as $page_footer_key=>$page_footer_value){
									if ($page_footer_value["height"]<=0){
									}else{
										$next_page_footer_height=$next_page_footer_height+$page_footer_value["height"];
									}
								}
								break;
							}
						}
						
						//加上下一部分的高度一张纸页越界了
						if ($temp_height+$height+$next_height+$next_page_footer_height>$content_height){
							$additional_html=add_additional_space($content_height-$temp_height-$height-$page_footer_height);
							if ($current_reset_page_number_after=="true"){$page_footer_reset_page_number_after="true";}
							$page_footer_body_html=extract_page_footer_body_html($row,$data_row,$sub_row,$sub_data_row,$page_footer_arr_fill_data);
							$report_page_arr[]=page_info($report_html.$body_html.$additional_html.$page_footer_body_html,$page_footer_reset_page_number_after);
							$page_printed_complete=true;
						}else{
							$report_html=$report_html.$body_html;
							$temp_height=$temp_height+$height;
							$only_page_header_information=false;	//有内容，不只是 page header
						}
					}
					break;
				case "page_footer":		//没有这个case，不用处理
					break;
				default:
					break;
			}
			
			//到了最后一个部分 && 有内容要打印 && 一页未结
			if (($key==count($template_arr)-1) && (!$only_page_header_information) && (!$page_printed_complete)){
				$row=$value["row"];
				$data_row=$value["data_row"];
				$sub_row=$value["sub_row"];
				$sub_data_row=$value["sub_data_row"];
				
				$page_footer_body_html="";
				$page_footer_arr_fill_data=fill_data($page_footer_arr,$instances,$data_row,$sub_section_arr,$sub_datasrc_arr);
				$page_footer_body_html=extract_page_footer_body_html($row,$data_row,$sub_row,$sub_data_row,$page_footer_arr_fill_data);
				$current_reset_page_number_after="true";
				$report_page_arr[]=page_info($report_html.$page_footer_body_html,$current_reset_page_number_after);
			}
		}
		
		$page_number=0;
		for ($count=0;$count<=count($report_page_arr)-1;$count++){
			$page_number=$page_number+1;
			$report_page_arr[$count]["page_number"]=$page_number;
			if (strtolower($report_page_arr[$count]["reset_page_number_after"])=="true"){$page_number=0;}
		}
		for ($count=count($report_page_arr)-1;$count>=0;$count--){
			$page_number=$report_page_arr[$count]["page_number"];
			if (($count==count($report_page_arr)-1) || 
				(strtolower($report_page_arr[$count]["reset_page_number_after"])=="true")){
				$total_page_count=$report_page_arr[$count]["page_number"];
			}
			$page_n_of_m="Page ".$page_number." of ".$total_page_count;
			$body_html=
				str_ireplace("{report:page_number}",$page_number,
				str_ireplace("{report:total_page_count}",$total_page_count,
				str_ireplace("{report:page_n_of_m}",$page_n_of_m,$report_page_arr[$count]["body_html"])));

			$report_page_arr[$count]["total_page_count"]=$total_page_count;
			$report_page_arr[$count]["page_n_of_m"]=$page_n_of_m;
			$report_page_arr[$count]["body_html"]=$body_html;
		}
		
		foreach ($report_page_arr as $key=>$value){
			if ($key==count($report_page_arr)-1){$html_display_buffer .=$value["body_html"];break;}
			$html_display_buffer .=
				'<table cellpadding="0" cellspacing="0" border="0" style="page-break-after:always;">'.
				'<tr><td>'.$value["body_html"].'</td></tr></table>';
		}
	}
}

$html_display_buffer .='</body></html>';
foreach ($report_para_d230c01e08285d10e0c26984588f7e0 as $key=>$value){
	$html_display_buffer=str_replace("{@".$key."}",stripslashes($value),$html_display_buffer);
}
echo $html_display_buffer;
?>