<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>
<?php
html_heading();
eval(rebuild_layout(__FILE__));
//Draw Body Layout
?>

<script language="javascript">
//var hidden_object_id_list;	//隐藏对象的列表
//var object_id_list;	//所有对象的列表
//权限基本只有 read addnew edit delete approve print 六种
//var access_read;		//读取权限
//var access_addnew;	//新增权限
//var access_edit;		//编辑权限
//var access_delete;	//删除权限
//var access_approve;	//批核权限
//var access_print;		//打印权限
//以上js变量无需设定，已经由类库自动产生，直接使用即可

//opt_action操作状态
//外部要求的操作
var external_opt_action;
var action_page;
var wel_wh_code;
var wel_tran_date;
var wel_part_no;
//一直处于暗淡的对象列表(无法编辑的对象)
var dim_object_id_list;
//要用权限控制的按钮列表
var security_button;

$(document).ready(function()
{
	external_opt_action="<?php echo werp_get_request_var("opt_action"); ?>";
	action_page="<?php echo werp_pathinfo_filename(__FILE__); ?>_opt.php";
	wel_wh_code="<?php echo werp_get_request_var("txt_wel_wh_code"); ?>";
	wel_tran_date="<?php echo werp_get_request_var("dtxt_wel_tran_date"); ?>";
//	wel_part_no="<?php echo werp_get_request_var("txt_wel_part_no"); ?>";
	dim_object_id_list="txt_wel_part_des|txt_wel_part_des1|txt_wel_part_des2|txt_wel_part_des3|txt_wel_part_des4|txt_wel_unit|ntxt_wel_stk_qty";
	security_button="btn_head_save";	
});
	
function return_handler_info(return_message)
{
	//window.alert(return_message);	//显示所有信息供测试时查看
	if (script_timeout(return_message)){return false};
	var ret_msg_arr=return_message.split("|");
	if (ret_msg_arr.length<5){window.alert(return_message);return false;}
	for (var i=0;i<ret_msg_arr.length;i++){ret_msg_arr[i]=un_coding_str(ret_msg_arr[i]);}
	var opt_action=ret_msg_arr[1];
	var msg_code=ret_msg_arr[2];
	var msg_detail=ret_msg_arr[3];
	var msg_script=ret_msg_arr[4];
	switch(opt_action)
	{
		case "confirm":
		default:
			if (msg_detail!==""){window.alert(msg_detail);}eval(msg_script);
			break;
	}
}

function load_state()
{
	enable_object(object_id_list,true,"");
	enable_object(dim_object_id_list,false,"");
	enable_object(security_button,false,"");
	enable_object("btn_head_save",true,access_edit);
	format_number_el("ntxt_wel_tran_qty");
	format_number_el("ntxt_wel_stk_qty");
}

function btn_head_save_click()
{
	opt_action="confirm";
	var url=get_url_parameter(action_page,opt_action,object_id_list);
	var handler=new net.content_loader(url,return_handler_info);
	
}
$(document).ready(function(){bind_event("btn_head_save","click",btn_head_save_click);});

function btn_head_cancel_click()
{
	clear_screen_layout(object_id_list);
	load_state();
}
$(document).ready(function(){bind_event("btn_head_cancel","click",btn_head_cancel_click);});

$(document).ready(function()
{
	if (external_opt_action=="")
	{
		load_state();	
	}
});

</script>
<?php
html_footer();
?>