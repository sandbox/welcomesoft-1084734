<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>
<?php
html_heading();
eval(rebuild_layout(__FILE__));
//Draw Body Layout
?>

	<script language="javascript"><!--
		//var hidden_object_id_list;	//隐藏对象的列表
		//var object_id_list;	//所有对象的列表
		//权限基本只有 read addnew edit delete approve print 六种
		//var access_read;		//读取权限
		//var access_addnew;	//新增权限
		//var access_edit;		//编辑权限
		//var access_delete;	//删除权限
		//var access_approve;	//批核权限
		//var access_print;		//打印权限
		//以上js变量无需设定，已经由类库自动产生，直接使用即可

		var external_opt_action="";
		var action_page="";
		var wel_pt_no="";
		var dim_object_id_list="";
		var security_button="";
	
		$(document).ready(function()
		{
			//opt_action操作状态
			//外部要求的操作
			external_opt_action="<?php echo werp_get_request_var("opt_action"); ?>";
			action_page="<?php echo werp_pathinfo_filename(__FILE__); ?>_opt.php";
			wel_pt_no="<?php echo werp_get_request_var("txt_wel_pt_no"); ?>";
			//一直处于暗淡的对象列表(无法编辑的对象)
			dim_object_id_list= "txt_wel_ven_code|txt_wel_ven_des|dtxt_wel_req_date|txt_wel_ven_dn|rmk_wel_pt_remark|"+
								"txt_wel_abbre_des|txt_wel_ven_des1|"+
								"txt_wel_ven_add1|txt_wel_ven_add2|txt_wel_ven_add3|txt_wel_ven_add4|"+
								"txt_wel_ven_cont|txt_wel_ven_tele|txt_wel_ven_tele1|txt_wel_ven_fax|txt_wel_ven_fax1|";
			//要用权限控制的按钮列表
			security_button="btn_detail_tab0_edit";
		});

		$(document).ready(function()
		{
			wel_ptrdetm_sql_grid(document.getElementById("txt_wel_pt_no").value);
		});
		
		function return_handler_info(return_message)
		{
			//window.alert(return_message);	//显示所有信息供测试时查看
			var ret_msg_arr=return_message.split("|");
			if (ret_msg_arr.length<5){window.alert(return_message);return false;}
			for (var i=0;i<ret_msg_arr.length;i++){ret_msg_arr[i]=un_coding_str(ret_msg_arr[i]);}
			var opt_action=ret_msg_arr[1];
			var msg_code=ret_msg_arr[2];
			var msg_detail=ret_msg_arr[3];
			var msg_script=ret_msg_arr[4];
			switch(opt_action)
			{
				case "bbtn_wel_pt_no_load_click":
					if (msg_detail!==""){window.alert(msg_detail);}
					if (msg_code=="")
					{
						enable_object(object_id_list,false,"");
						enable_object("btn_head_next",true,"");
						enable_object("btn_detail_tab0_edit",true,
									access_edit);
					}
					eval(msg_script);//执行返回后产生的脚本
					break;
					
				case "check_iqc":
					if (msg_detail!==""){window.alert(msg_detail);}
					eval(msg_script);
					break;
					
				default:
					if (msg_detail!==""){window.alert(msg_detail);}eval(msg_script);
					break;
			}
		}

		function bbtn_wel_pt_no_load_click()
		{
			if (document.getElementById("txt_wel_pt_no").value.trim()==""){return;}
			var url=get_url_parameter(action_page,"bbtn_wel_pt_no_load_click",object_id_list);
			var handler=new net.content_loader(url,return_handler_info);
		}
		$(document).ready(function(){bind_event("bbtn_wel_pt_no_load","click",bbtn_wel_pt_no_load_click);});

		function btn_head_next_click()
		{
			clear_screen_layout(object_id_list);
			enable_object(object_id_list,true,"");
			enable_object(dim_object_id_list,false,"");
			enable_object(security_button,false,"");
			wel_ptrdetm_sql_grid("");
		}
		$(document).ready(function(){bind_event("btn_head_next","click",btn_head_next_click);});

		function btn_detail_tab0_edit_click()
		{
			var rt=wel_ptrdetm_sql_selected_col(0);if (!rt[0]){return false;}

			var url=get_url_parameter(action_page,"check_iqc",object_id_list);
			url=url+"&ntxt_wel_pt_line="+url_escape(rt[1]);
			var handler=new net.content_loader(url,return_handler_info);
		}
		$(document).ready(function(){bind_event("btn_detail_tab0_edit","click",btn_detail_tab0_edit_click);});

		function check_if_iqc(str_pt_no,int_pt_line)
		{
			
		}
		
		$(document).ready(function()
		{
			if (external_opt_action=="")
			{
				btn_head_next_click();	
			}
			if (external_opt_action=="btn_detail_tab0_edit_click")
			{
				document.getElementById("txt_wel_pt_no").value=wel_pt_no;
				bbtn_wel_pt_no_load_click();
				$("#detail_tab_list > ul").tabs({selected: 0});
			}
		});
	//
	--></script>
<?php
html_footer();
?>
