<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>

<?php
tree_html_heading();
?>

<div style="display:none;">
	<input type="button" id="btn_wel_test_info" onclick="javascript:test_info();" value="view node info" />
</div>

<script language="javascript">
function test_info(){
	if (tree_node.node_id==""){return false;}
	window.alert(
		"node_id:" + tree_node.node_id +"\n" + 
		"wel_root_code:" + tree_node.wel_root_code +"\n" + 
		"wel_parent_code:" + tree_node.wel_parent_code +"\n" + 
		"wel_prog_code:" + tree_node.wel_prog_code +"\n" + 
		"sub_count:" + tree_node.sub_count +"\n" + 
		"wel_parent_des:" + tree_node.wel_parent_des +"\n" + 
		"wel_prog_des:" + tree_node.wel_prog_des +"\n"
		);
}

function test_operation_node(){
	if (tree_node.node_id==""){return null;}else{return tree_node;}
}
</script>

<script type="text/javascript">
var tree_node = new Object();
$(document).ready(function(){
	tree_node.node_id="";
	tree_node.wel_root_code="";
	tree_node.wel_parent_code="";
	tree_node.wel_prog_code="";
	tree_node.sub_count="";
	tree_node.wel_parent_des="";
	tree_node.wel_prog_des="";
});

var simpleTreeCollection;
$(document).ready(function(){
	simpleTreeCollection = $('.simpleTree').simpleTree({
		drag : false,
		autoclose: false,
		afterClick:function(node){
			
			tree_node.node_id=node.attr("id");
			tree_node.wel_root_code=node.attr("wel_root_code");
			tree_node.wel_parent_code=node.attr("wel_parent_code");
			tree_node.wel_prog_code=node.attr("wel_prog_code");
			tree_node.sub_count=node.attr("sub_count");
			tree_node.wel_parent_des=node.attr("wel_parent_des");
			tree_node.wel_prog_des=node.attr("wel_prog_des");

			//alert("text-"+$('span:first',node).text());
		},
		afterDblClick:function(node){
			//alert("text-"+$('span:first',node).text());
		},
		afterMove:function(destination, source, pos){
			//alert("destination-"+destination.attr('id')+" source-"+source.attr('id')+" pos-"+pos);
		},
		afterAjax:function()
		{
			//alert('Loaded');
		},
		animate:true
		//,docToFolderConvert:true
	});
});
</script>


<?php
$txt_wel_root_code=werp_get_request_var('txt_wel_root_code');
add_wel_root_code_to_wel_proghdrm_if_not_exist($txt_wel_root_code);

$sub_node_html=generate_wel_root_code_node($txt_wel_root_code,$txt_wel_root_code);
if($sub_node_html!=""){echo '<div style="height: 100%;width: 100%;overflow: auto;">'.$sub_node_html.'</div>';}

function add_wel_root_code_to_wel_proghdrm_if_not_exist($wel_root_code){
	try 
	{
		$conn=werp_db_connect();
		
		$sql="SELECT * FROM #__wel_proghdrm WHERE wel_prog_code='".$wel_root_code."' LIMIT 1";
		$sql=revert_to_the_available_sql($sql);
		if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
		if(!($row=mysql_fetch_array($result))){
			$sql="INSERT INTO #__wel_proghdrm SET
				wel_prog_code='root',
				wel_prog_des='Welcome ERP'
				";
			$sql=revert_to_the_available_sql($sql);
			if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
		}
	}
	catch (Exception $e){
		//return $e->getMessage();
		//return "";
	}
}

function get_wel_prog_des($wel_prog_code){
	$wel_prog_des="";

	try 
	{
		$conn=werp_db_connect();

		$sql="SELECT wel_prog_des FROM #__wel_proghdrm WHERE wel_prog_code='".$wel_prog_code."' LIMIT 1";
		$sql=revert_to_the_available_sql($sql);
		if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
		if(!($row=mysql_fetch_array($result))){}
		$wel_prog_des=$row["wel_prog_des"];
	}
	catch (Exception $e){
		//return $e->getMessage();
		//return "";
	}
	return $wel_prog_des;
}

function generate_wel_root_code_node($wel_root_code){
	$return_html="";
	
	try 
	{
		$conn=werp_db_connect();
		
		$sql="SELECT *,".
			"IFNULL((".
			"SELECT COUNT(wel_parent_code) FROM #__wel_proglofm as sub_wel_proglofm ".
			"WHERE sub_wel_proglofm.wel_parent_code=#__wel_proghdrm.wel_prog_code".
			"),0) AS sub_count ".
		"FROM #__wel_proghdrm WHERE wel_prog_code='".$wel_root_code."' LIMIT 1";
		$sql=revert_to_the_available_sql($sql);
		if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
		if(!($row=mysql_fetch_array($result))){throw new Exception("");}
		
		//$wel_root_code=is_null($row['wel_prog_code']) ? "" : $row['wel_prog_code'];	//wel_root_code
		$wel_parent_code=is_null($row['wel_prog_code']) ? "" : $row['wel_prog_code'];	//wel_parent_code
		$wel_prog_code=is_null($row['wel_prog_code']) ? "" : $row['wel_prog_code'];		//wel_prog_code
		$sub_count=doubleval(is_null($row["sub_count"]) ? 0 : $row["sub_count"]);		//sub_count
		$wel_parent_des=is_null($row['wel_prog_des']) ? "" : $row['wel_prog_des'];		//wel_parent_des
		$wel_prog_des=is_null($row['wel_prog_des']) ? "" : $row['wel_prog_des'];		//wel_prog_des
		
		$node_id=md5(uniqid(rand()));
		$return_html .='<ul class="simpleTree"><li class="root" id="'.$node_id.'"><span></span>';
		$node_id=md5(uniqid(rand()));
		$node_attribute='
			id="'.str_ireplace('"','&quot;',$node_id).'" 
			wel_root_code="'.str_ireplace('"','&quot;',$wel_root_code).'" 
			wel_parent_code="'.str_ireplace('"','&quot;',$wel_parent_code).'" 
			wel_prog_code="'.str_ireplace('"','&quot;',$wel_prog_code).'" 
			sub_count="'.str_ireplace('"','&quot;',$sub_count).'" 
			wel_parent_des="'.str_ireplace('"','&quot;',$wel_parent_des).'" 
			wel_prog_des="'.str_ireplace('"','&quot;',$wel_prog_des).'" 
			';
		$return_html .='<ul><li class="open" '.$node_attribute.'><span>'.$wel_prog_code.' ('.$wel_prog_des.') </span>';
		$sub_node_html=generate_wel_parent_code_node($wel_root_code,$wel_parent_code);
		if($sub_node_html!=""){$return_html .='<ul>'.$sub_node_html.'</ul>';}
		$return_html .='</li></ul></li></ul>';
	}
	catch (Exception $e){
		//return $e->getMessage();
		return "";
	}
	return $return_html;
}


function generate_wel_parent_code_node($wel_root_code,$wel_parent_code){
	$return_html="";
	
	try 
	{
		$conn=werp_db_connect();
		
		$sql = "SELECT *,".
			"IFNULL((".
			"SELECT COUNT(wel_parent_code) FROM #__wel_proglofm as sub_wel_proglofm ".
			"WHERE sub_wel_proglofm.wel_parent_code=#__wel_proglofm.wel_prog_code".
			"),0) AS sub_count ".
			"FROM #__wel_proglofm ".
			"WHERE wel_parent_code='".$wel_parent_code."' ORDER BY wel_ordering ASC";
		$sql=revert_to_the_available_sql($sql);
		if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
		while ($row=mysql_fetch_array($result))
		{
			//$wel_root_code=is_null($row['wel_prog_code']) ? "" : $row['wel_prog_code'];	//wel_root_code
			//$wel_parent_code=is_null($row['wel_parent_code']) ? "" : $row['wel_parent_code'];	//wel_parent_code
			$wel_prog_code=is_null($row['wel_prog_code']) ? "" : $row['wel_prog_code'];		//wel_prog_code
			$sub_count=doubleval(is_null($row["sub_count"]) ? 0 : $row["sub_count"]);		//sub_count
			$wel_parent_des=get_wel_prog_des($wel_parent_code);		//wel_parent_des
			$wel_prog_des=get_wel_prog_des($wel_prog_code);		//wel_prog_des
			
			$node_id=md5(uniqid(rand()));
			$node_attribute='
				id="'.str_ireplace('"','&quot;',$node_id).'" 
				wel_root_code="'.str_ireplace('"','&quot;',$wel_root_code).'" 
				wel_parent_code="'.str_ireplace('"','&quot;',$wel_parent_code).'" 
				wel_prog_code="'.str_ireplace('"','&quot;',$wel_prog_code).'" 
				sub_count="'.str_ireplace('"','&quot;',$sub_count).'" 
				wel_parent_des="'.str_ireplace('"','&quot;',$wel_parent_des).'" 
				wel_prog_des="'.str_ireplace('"','&quot;',$wel_prog_des).'" 
				';
			$return_html .='<li class="open" '.$node_attribute.'><span>'.$wel_prog_code.' ('.$wel_prog_des.') </span>';
			if($sub_count>0){
				$sub_node_html=generate_wel_parent_code_node($wel_root_code,$wel_prog_code);
				if($sub_node_html!=""){$return_html .='<ul>'.$sub_node_html.'</ul>';}
			}
			$return_html .='</li>';
		}
	}
	catch (Exception $e){
		//return $e->getMessage();
		return "";
	}
	return $return_html;
}

tree_html_footer();
?>