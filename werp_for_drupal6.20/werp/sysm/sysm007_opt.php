<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>
<?php
eval(receipt_url_parameter($_GET,$_POST));	//将参数的值放入相应以参数为名的变量中
//echo receipt_url_parameter($_GET,$_POST);
eval(read_cache_from_wel_wrcache($main_page."msg.inc"));
require_once(WERP_SITE_PATH_CLASS.$main_page."_cls.php");

//$opt_action,$msg_code,$msg_detail,$msg_script 为标准定义，用coding_str编码后以
//"|".$opt_action."|".$msg_code."|".$msg_detail."|".$msg_script  格式作为信息返回给回调函数
$opt_action=strtolower($opt_action);	//调用者的动作
$msg_code="";	//调用后产生的信息代码
$msg_detail="";	//信息代码对应的细节内容
$msg_script="";	//要执行的脚本

$cls_sysm007=new sysm007_cls();
$cls_sysm007->wel_group_code=$txt_wel_group_code;
$cls_sysm007->wel_group_des=$txt_wel_group_des;
$cls_sysm007->wel_user_id=$txt_wel_user_id;
$cls_sysm007->wel_prog_code=$txt_wel_prog_code;

switch ($opt_action){
	case "lbtn_wel_group_code_load_click":
		$return_val=$cls_sysm007->read();
		$msg_code=$return_val["msg_code"];
		$msg_detail=extract_message($msg_code);
		if ($msg_code==""){
			$msg_script="clear_screen_layout(object_id_list);\n".
			"document.getElementById('txt_wel_group_code').value='".format_slashes($return_val["wel_group_code"])."';\n".
			"document.getElementById('txt_wel_group_des').value='".format_slashes($return_val["wel_group_des"])."';\n".
			"wel_usergroup_sql_grid(document.getElementById(\"txt_wel_group_code\").value);\n".
			"wel_groupprog_sql_grid(document.getElementById(\"txt_wel_group_code\").value);\n".
			"wel_group_code=document.getElementById('txt_wel_group_code').value;\n";
		}
		break;
		
	case "addnew":
		$return_val=$cls_sysm007->addnew();
		$msg_code=$return_val["msg_code"];
		$msg_detail=extract_message($msg_code);
		if ($msg_code=="addnew_succee"){
			$msg_script="wel_group_code='".format_slashes($return_val["wel_group_code"])."';".
				"document.getElementById('txt_wel_group_code').value='".format_slashes($return_val["wel_group_code"])."';\n".
				"lbtn_wel_group_code_load_click();\n";
		}
		break;
		
	case "edit":
		$return_val=$cls_sysm007->edit();
		$msg_code=$return_val["msg_code"];
		$msg_detail=extract_message($msg_code);
		if ($msg_code=="edit_succee"){
			$msg_script="lbtn_wel_group_code_load_click();\n";
		}
		break;

	case "btn_head_del_click":
		$return_val=$cls_sysm007->delete();
		$msg_code=$return_val["msg_code"];
		$msg_detail=extract_message($msg_code);
		if ($msg_code=="delete_succee"){
			$msg_script="btn_head_next_click();\n";
		}
		break;

	case "btn_user_del_click":
		$return_val=$cls_sysm007->delete_user();
		$msg_code=$return_val["msg_code"];
		$msg_detail=extract_message($msg_code);
		if ($msg_code=="delete_succee"){
			$msg_script="wel_usergroup_sql_selected_del();\n";
		}
		break;

	case "btn_user_del_all_click":
		$return_val=$cls_sysm007->delete_user_all();
		$msg_code=$return_val["msg_code"];
		$msg_detail=extract_message($msg_code);
		if ($msg_code=="delete_succee"){
			$msg_script="wel_usergroup_sql_grid(document.getElementById(\"txt_wel_group_code\").value);\n";
		}
		break;

	case "btn_program_del_click":
		$return_val=$cls_sysm007->delete_program();
		$msg_code=$return_val["msg_code"];
		$msg_detail=extract_message($msg_code);
		if ($msg_code=="delete_succee"){
			$msg_script="wel_groupprog_sql_selected_del();\n";
		}
		break;

	case "btn_program_del_all_click":
		$return_val=$cls_sysm007->delete_program_all();
		$msg_code=$return_val["msg_code"];
		$msg_detail=extract_message($msg_code);
		if ($msg_code=="delete_succee"){
			$msg_script="wel_groupprog_sql_grid(document.getElementById(\"txt_wel_group_code\").value);\n";
		}
		break;

	default:
		break;
}
echo "|".coding_str($opt_action)."|".coding_str($msg_code)."|".coding_str($msg_detail)."|".coding_str($msg_script);
?>