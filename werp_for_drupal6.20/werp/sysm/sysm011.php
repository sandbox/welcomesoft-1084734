<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>
<?php
html_heading();
eval(rebuild_layout(__FILE__));
//Draw Body Layout
?>

<script language="javascript">
//var hidden_object_id_list;	//隐藏对象的列表
//var object_id_list;	//所有对象的列表
//权限基本只有 read addnew edit delete approve print 六种
//var access_read;		//读取权限
//var access_addnew;	//新增权限
//var access_edit;		//编辑权限
//var access_delete;	//删除权限
//var access_approve;	//批核权限
//var access_print;		//打印权限
//以上js变量无需设定，已经由类库自动产生，直接使用即可

//opt_action操作状态
//外部要求的操作
var external_opt_action;
var action_page;
var wel_comp_code;
//一直处于暗淡的对象列表(无法编辑的对象)
var dim_object_id_list;
//要用权限控制的按钮列表
var security_button;
$(document).ready(function()
{
	external_opt_action="<?php echo werp_get_request_var("opt_action"); ?>";
	action_page="<?php echo werp_pathinfo_filename(__FILE__); ?>_opt.php";
	wel_comp_code="<?php echo werp_get_request_var("txt_wel_comp_code"); ?>";
	//readonly对象
	dim_object_id_list="";
	security_button="btn_head_addnew|btn_head_edit|btn_head_del|btn_head_next|btn_head_save|btn_head_cancel";
});

function return_handler_info(return_message)
{
	//window.alert(return_message);	//显示所有信息供测试时查看
	if (script_timeout(return_message)){return false};
	var ret_msg_arr=return_message.split("|");
	if (ret_msg_arr.length<5){window.alert(return_message);return false;}
	for (var i=0;i<ret_msg_arr.length;i++){ret_msg_arr[i]=un_coding_str(ret_msg_arr[i]);}
	var opt_action=ret_msg_arr[1];
	var msg_code=ret_msg_arr[2];
	var msg_detail=ret_msg_arr[3];
	var msg_script=ret_msg_arr[4];
	switch(opt_action)
	{
		case "addnew":
		case "edit":
		case "btn_head_del_click":
			if (msg_detail!==""){window.alert(msg_detail);}
			eval(msg_script);//执行返回后产生的脚本
			break;
			
		case "lbtn_wel_comp_code_load_click":
			if (msg_detail!==""){window.alert(msg_detail);}
			eval(msg_script);//执行返回后产生的脚本
			if (msg_code=="")
			{
				enable_object(object_id_list,false,"");
				enable_object("btn_head_edit|btn_head_del|btn_head_next",true,
				access_edit+"|"+access_delete+"|"+access_read);
			}
			break;
			
		default:
			if (msg_detail!==""){window.alert(msg_detail);}eval(msg_script);
			break;
			
	}
}

function lbtn_wel_comp_code_load_click()
{
	if (document.getElementById("txt_wel_comp_code").value.trim()==""){return;}
	var url=get_url_parameter(action_page,"lbtn_wel_comp_code_load_click",object_id_list);
	var handler=new net.content_loader(url,return_handler_info);
}
$(document).ready(function(){bind_event("lbtn_wel_comp_code_load","click",lbtn_wel_comp_code_load_click);});

function btn_head_addnew_click()
{
	wel_comp_code="";
	clear_screen_layout(object_id_list);
	enable_object(object_id_list,true,"");
	enable_object(dim_object_id_list,false,"");
	enable_object(security_button,false,"");
	enable_object("btn_head_save|btn_head_cancel",true,
				access_addnew+"|"+access_read);
}
$(document).ready(function(){bind_event("btn_head_addnew","click",btn_head_addnew_click);});

function btn_head_edit_click()
{
	enable_object(object_id_list,true,"");
	enable_object(dim_object_id_list,false,"");
	enable_object(security_button,false,"");
	enable_object("txt_wel_comp_code|lbtn_wel_comp_code_load|bbtn_wel_comp_code",false,"");
	enable_object("btn_head_save|btn_head_cancel",true,
			access_edit+"|"+access_read);
}
$(document).ready(function(){bind_event("btn_head_edit","click",btn_head_edit_click);});

function btn_head_del_click()
{
	var confirm_message=extract_message("delete_confirm");
	confirm_message=confirm_message.replace("s1",$("#txt_wel_comp_code").attr("value"));
	if (!window.confirm(confirm_message)){return;}
	var url=get_url_parameter(action_page,"btn_head_del_click",object_id_list);
	var handler=new net.content_loader(url,return_handler_info);
}
$(document).ready(function(){bind_event("btn_head_del","click",btn_head_del_click);});

function btn_head_next_click()
{
	clear_screen_layout(object_id_list);
	enable_object(object_id_list,false,"");
	enable_object(dim_object_id_list,false,"");
	enable_object(security_button,false,"");
	enable_object("txt_wel_comp_code|lbtn_wel_comp_code_load|bbtn_wel_comp_code",true,"");
	enable_object("btn_head_addnew",true,access_addnew);
}
$(document).ready(function(){bind_event("btn_head_next","click",btn_head_next_click);});

function btn_head_save_click()
{
	if(wel_comp_code==""){opt_action="addnew";}else{opt_action="edit";}
	var url=get_url_parameter(action_page,opt_action,object_id_list);
	var handler=new net.content_loader(url,return_handler_info);
}
$(document).ready(function(){bind_event("btn_head_save","click",btn_head_save_click);});

function btn_head_cancel_click()
{
	if (wel_comp_code!="")
	{
		lbtn_wel_comp_code_load_click();	//重新加载一次数据，让当前更改的内容失效
	}
	else
	{
		btn_head_next_click()
//		enable_object(object_id_list,true,"");
//		enable_object(dim_object_id_list,false,"");
//		enable_object(security_button,false,"");
//		enable_object("btn_head_addnew|btn_head_save|btn_head_cancel",true,
//			access_addnew+"|"+access_addnew+"|"+access_addnew);
	}
}
$(document).ready(function(){bind_event("btn_head_cancel","click",btn_head_cancel_click);});

$(document).ready(function()
{
	if (external_opt_action=="")
	{
		btn_head_next_click();	
	}
});
</script>
<?php
html_footer();
?>