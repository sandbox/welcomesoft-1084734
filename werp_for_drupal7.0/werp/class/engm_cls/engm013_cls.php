<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>

<?php 
class engm013_cls
{
	public $wel_ecn_no="";
	public $wel_pattern="";
	public $wel_proj_no="";
	public $wel_ecn_date="";
	public $wel_rev_no="";
	public $wel_ecn_rmk="";
	public $wel_assm_no="";
	public $wel_part_no="";
	
	private $wel_prog_code="engm013";
	
	//读取工程更改通知档案
	public function read()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_read")){throw new Exception("unauthorized_access");}
			
			$sql="SELECT #__wel_ecnhdrm.*,#__wel_projflm.wel_proj_des as wel_proj_des 
					FROM #__wel_ecnhdrm LEFT JOIN #__wel_projflm 
					ON #__wel_ecnhdrm.wel_proj_no=#__wel_projflm.wel_proj_no 
					WHERE #__wel_ecnhdrm.wel_ecn_no='".$this->wel_ecn_no."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	//查询sql时出错了
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_ecn_no_not_found");}	//没有符合条件的记录
			foreach ($row as $key=>$value){$return_val[$key]=$value;}
			throw new Exception("");
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	//新增工程更改通知档案
	public function addnew()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_addnew")){throw new Exception("unauthorized_access");}
			
			if(($this->wel_ecn_no=="") && ($this->wel_pattern=="")){throw new Exception("wel_ecn_no_miss");}
			if($this->wel_ecn_date==""){throw new Exception("wel_ecn_date_miss");}
			
			if($this->wel_pattern!=""){			//如单号不为空，把手工输入的单号作为单号
				$sql="SELECT * FROM #__wel_gentenw WHERE wel_pattern='".$this->wel_pattern."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_pattern_not_found");}
			}else{
				$str__wel_ecn_no=$this->wel_ecn_no;
				$this->wel_pattern="";
			}
			
			//项目档案是否存在
			if($this->wel_proj_no!="")		
			{
				$sql="SELECT * FROM #__wel_projflm WHERE wel_proj_no='".$this->wel_proj_no."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_proj_no_not_found");}
			}
			
			//工程更改通知档案是否存在
			$sql="SELECT * FROM #__wel_ecnhdrm WHERE wel_ecn_no='".$this->wel_ecn_no."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if($row=mysql_fetch_array($result)){throw new Exception("wel_ecn_no_exist");}
			
			try
			{
				mysql_query('begin');
				
				if($this->wel_pattern!="")		//单号为空,根据模式码动态产生一单号作为单号
				{
					$sql="SELECT * FROM #__wel_gentenw WHERE wel_pattern='".$this->wel_pattern."' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
					if(!$row=mysql_fetch_array($result)){throw new Exception("wel_pattern_not_found");}
					$str__wel_ecn_no=trim($row["wel_pattern"]).
						sprintf("%0".(12-strlen(trim($row["wel_pattern"])))."s",doubleval($row["wel_en_nextno"]+1));

					//更新模式初始号
					$sql="UPDATE #__wel_gentenw SET 
						wel_en_nextno=wel_en_nextno+1 
						WHERE wel_pattern='".$this->wel_pattern."' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				}
				
				//插入记录到wel_ecnhdrm中
				$sql="INSERT INTO #__wel_ecnhdrm(
					wel_ecn_no,
					wel_pattern,
					wel_ecn_date,
					wel_rev_no,
					wel_proj_no,
					wel_ecn_rmk,
					wel_crt_user,
					wel_crt_date 
					)VALUES(
					'".$str__wel_ecn_no."',
					'".$this->wel_pattern."',
					'".date("Y-m-d",strtotime($this->wel_ecn_date))."',
					".intval($this->wel_rev_no).",
					'".$this->wel_proj_no."',
					'".$this->wel_ecn_rmk."',
					'".$_SESSION["wel_user_id"]."',
					now())";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				mysql_query('commit');
			}
			catch (Exception $e1)
			{
				mysql_query('rollback');
				throw new Exception($e1->getMessage());
			}
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		if($msg_code==""){$msg_code="addnew_succee";}
		$return_val["msg_code"]=$msg_code;
		$return_val["wel_ecn_no"]=$str__wel_ecn_no;
		return $return_val;
	}
	
	//编辑工程更改通知档案
	public function edit()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_edit")){throw new Exception("unauthorized_access");}
			
			if($this->wel_ecn_no==""){throw new Exception("wel_ecn_no_miss");}
			if($this->wel_ecn_date==""){throw new Exception("wel_ecn_date_miss");}
			
			//项目档案是否存在
			if($this->wel_proj_no!="")		
			{
				$sql="SELECT * FROM #__wel_projflm WHERE wel_proj_no='".$this->wel_proj_no."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_proj_no_not_found");}
			}
			
			//工程更改通知档案是否存在
			$sql="SELECT * FROM #__wel_ecnhdrm WHERE wel_ecn_no='".$this->wel_ecn_no."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_ecn_no_not_found");}
			if($row["wel_appr_yn"]==1){throw new Exception("wel_ecn_no_had_approved");}
			if($row["wel_post_yn"]==1){throw new Exception("wel_ecn_no_had_posted");}
			
			try
			{
				mysql_query('begin');
				//更新记录
				$sql="UPDATE #__wel_ecnhdrm SET 
					wel_ecn_date='".date("Y-m-d",strtotime($this->wel_ecn_date))."',
					wel_rev_no=".intval($this->wel_rev_no).",
					wel_proj_no='".$this->wel_proj_no."',
					wel_ecn_rmk='".$this->wel_ecn_rmk."',
					wel_upd_user='".$_SESSION["wel_user_id"]."', 
					wel_upd_date=now() 
					WHERE wel_ecn_no='".$this->wel_ecn_no."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				mysql_query('commit');
			}
			catch (Exception $e1)
			{
				mysql_query('rollback');
				throw new Exception($e1->getMessage());
			}
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}		
		if($msg_code==""){$msg_code="edit_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	//删除工程更改通知档案
	public function delete()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_delete")){throw new Exception("unauthorized_access");}
			
			if($this->wel_ecn_no==""){throw new Exception("wel_ecn_no_miss");}
			
			//工程更改通知档案是否存在
			$sql="SELECT * FROM #__wel_ecnhdrm WHERE wel_ecn_no='".$this->wel_ecn_no."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_ecn_no_not_found");}
			if($row["wel_appr_yn"]==1){throw new Exception("wel_ecn_no_had_approved");}
			if($row["wel_post_yn"]==1){throw new Exception("wel_ecn_no_had_posted");}
			
			try
			{
				mysql_query('begin');
				//删除记录
				$sql="DELETE FROM #__wel_ecnhdrm WHERE wel_ecn_no='".$this->wel_ecn_no."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}

				//删除记录
				$sql="DELETE FROM #__wel_engbomh WHERE wel_ecn_no='".$this->wel_ecn_no."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				
				mysql_query('commit');
			}
			catch (Exception $e1)
			{
				mysql_query('rollback');
				throw new Exception($e1->getMessage());
			}
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}		
		if($msg_code==""){$msg_code="delete_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	//工程更改通知批核
	public function approve()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_approve")){throw new Exception("unauthorized_access");}
			
			if($this->wel_ecn_no==""){throw new Exception("wel_ecn_no_miss");}
			
			//工程更改通知档案是否存在
			$sql="SELECT * FROM #__wel_ecnhdrm WHERE wel_ecn_no='".$this->wel_ecn_no."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_ecn_no_not_found");}
			if($row["wel_appr_yn"]==1){throw new Exception("wel_ecn_no_had_approved");}
			if($row["wel_post_yn"]==1){throw new Exception("wel_ecn_no_had_posted");}
			
			try
			{
				mysql_query('begin');
				
				$sql="UPDATE #__wel_ecnhdrm SET 
					wel_appr_yn=1,
					wel_appr_by='".$_SESSION["wel_user_id"]."',
					wel_appr_date=now() 
					WHERE wel_ecn_no='".$this->wel_ecn_no."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				
				mysql_query('commit');
			}
			catch (Exception $e1)
			{
				mysql_query('rollback');
				throw new Exception($e1->getMessage());
			}
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}		
		if($msg_code==""){$msg_code="approve_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	//工程更改通知取消批核
	public function not_approve()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_approve")){throw new Exception("unauthorized_access");}
			
			if($this->wel_ecn_no==""){throw new Exception("wel_ecn_no_miss");}
			
			//工程更改通知档案是否存在
			$sql="SELECT * FROM #__wel_ecnhdrm WHERE wel_ecn_no='".$this->wel_ecn_no."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_ecn_no_not_found");}
			if($row["wel_appr_yn"]!=1){throw new Exception("wel_ecn_no_not_approved");}
			if($row["wel_post_yn"]==1){throw new Exception("wel_ecn_no_had_posted");}
			
			try
			{
				mysql_query('begin');
				
				$sql="UPDATE #__wel_ecnhdrm SET 
					wel_appr_yn=0,
					wel_appr_by='".$_SESSION["wel_user_id"]."',
					wel_appr_date=now() 
					WHERE wel_ecn_no='".$this->wel_ecn_no."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				
				mysql_query('commit');
				
			}
			catch (Exception $e1)
			{
				mysql_query('rollback');
				throw new Exception($e1->getMessage());
			}
			
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}		
		if($msg_code==""){$msg_code="not_approve_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	//工程更改通知档案细节删除
	public function detail_delete()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_delete")){throw new Exception("unauthorized_access");}
			
			if($this->wel_ecn_no=="") {throw new Exception("wel_ecn_no_miss");}
			if($this->wel_assm_no=="") {throw new Exception("wel_assm_no_miss");}
			if($this->wel_part_no=="") {throw new Exception("wel_part_no_miss");}
			
			//工程更改通知档案是否存在
			$sql="SELECT * FROM #__wel_ecnhdrm WHERE wel_ecn_no='".$this->wel_ecn_no."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_ecn_no_not_found");}
			if($row["wel_appr_yn"]==1){throw new Exception("wel_ecn_no_had_approved");}
			if($row["wel_post_yn"]==1){throw new Exception("wel_ecn_no_had_posted");}
			
			$sql="SELECT * FROM #__wel_engbomh WHERE wel_ecn_no='".$this->wel_ecn_no."' AND 
				wel_assm_no='".$this->wel_assm_no."' AND wel_part_no='".$this->wel_part_no."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	//查询sql时出错了
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_ecn_detail_not_found");}	//没有符合条件的记录
			
			try
			{
				mysql_query('begin');
					
				//删除记录
				$sql="DELETE FROM #__wel_engbomh WHERE wel_ecn_no='".$this->wel_ecn_no."' AND 
					wel_assm_no='".$this->wel_assm_no."' AND wel_part_no='".$this->wel_part_no."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
					
				mysql_query('commit');
				
			}
			catch (Exception $e1)
			{
				mysql_query('rollback');
				throw new Exception($e1->getMessage());
			}
			
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}		
		if($msg_code==""){$msg_code="delete_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	//工程更改通知档案细节全部删除
	public function detail_delete_all()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_delete")){throw new Exception("unauthorized_access");}
			
			if($this->wel_ecn_no=="") {throw new Exception("wel_ecn_no_miss");}
			
			//工程更改通知档案是否存在
			$sql="SELECT * FROM #__wel_ecnhdrm WHERE wel_ecn_no='".$this->wel_ecn_no."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_ecn_no_not_found");}
			if($row["wel_appr_yn"]==1){throw new Exception("wel_ecn_no_had_approved");}
			if($row["wel_post_yn"]==1){throw new Exception("wel_ecn_no_had_posted");}
				
			$sql="SELECT * FROM #__wel_engbomh WHERE wel_ecn_no='".$this->wel_ecn_no."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	//查询sql时出错了
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_ecn_detail_not_found");}	//没有符合条件的记录
			
			try
			{
				mysql_query('begin');
					
				//删除记录
				$sql="DELETE FROM #__wel_engbomh WHERE wel_ecn_no='".$this->wel_ecn_no."'";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				
				mysql_query('commit');
				
			}
			catch (Exception $e1)
			{
				mysql_query('rollback');
				throw new Exception($e1->getMessage());
			}
			
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}		
		if($msg_code==""){$msg_code="delete_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
}
?>