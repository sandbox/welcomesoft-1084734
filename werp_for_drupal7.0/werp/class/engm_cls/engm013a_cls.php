<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>

<?php 
class engm013a_cls
{
	public $wel_ecn_no="";
	public $wel_assm_no="";
	public $wel_part_no="";
	public $wel_ecn_act="";
	public $wel_eng_unit="";
	public $wel_qp_eng="";
	public $wel_scp_fact=0;
	public $wel_ecn_rmk="";
	public $wel_ecn_loc="";
	public $wel_mod_sign="";
	
	private $wel_prog_code="engm013";
	
	//读取单位档案
	public function read()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_read")){throw new Exception("unauthorized_access");}
			
			if($this->wel_ecn_no=="") {throw new Exception("wel_ecn_no_miss");}
			if($this->wel_assm_no=="") {throw new Exception("wel_assm_no_miss");}
			if($this->wel_part_no=="") {throw new Exception("wel_part_no_miss");}
			
			$sql="SELECT engbomh.*,
				assmflm.wel_part_des as wel_assm_des,
				partflm.wel_part_des as wel_part_des, 
				partflm.wel_unit as wel_unit 
				FROM #__wel_engbomh as engbomh 
				LEFT JOIN #__wel_partflm as assmflm ON engbomh.wel_assm_no=assmflm.wel_part_no 
				LEFT JOIN #__wel_partflm as partflm ON engbomh.wel_part_no=partflm.wel_part_no WHERE 
				engbomh.wel_ecn_no='".$this->wel_ecn_no."' AND 
				engbomh.wel_assm_no='".$this->wel_assm_no."' AND engbomh.wel_part_no='".$this->wel_part_no."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	//查询sql时出错了
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_ecn_detail_not_found");}	//没有符合条件的记录
			foreach ($row as $key=>$value){$return_val[$key]=$value;}
			throw new Exception("");
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	//新增工程通知档案细节
	public function addnew()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_addnew")){throw new Exception("unauthorized_access");}
			
			if($this->wel_ecn_no=="") {throw new Exception("wel_ecn_no_miss");}
			if($this->wel_assm_no=="") {throw new Exception("wel_assm_no_miss");}
			if($this->wel_part_no=="") {throw new Exception("wel_part_no_miss");}
			if($this->wel_eng_unit=="") {throw new Exception("wel_eng_unit_miss");}
			
			if(!is_numeric($this->wel_scp_fact)){$this->wel_scp_fact=0;}
			if(!is_numeric($this->wel_qp_eng)){$this->wel_qp_eng=0;}
			
			if(strtolower($this->wel_ecn_act)=="delete"){
			}else{
				if($this->wel_scp_fact<0){throw new Exception("wel_scp_fact_over_rang");}
				if($this->wel_qp_eng<0){throw new Exception("wel_qp_eng_over_rang");}
				if($this->wel_qp_eng==0){throw new Exception("wel_qp_eng_miss");}
				if($this->wel_eng_unit=="") {throw new Exception("wel_eng_unit_miss");}
			}
			
			//工程单位档案是否存在
			$sql="SELECT * FROM #__wel_unitflm WHERE wel_unit_code='".$this->wel_eng_unit."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_eng_unit_not_found");}
			
			//工程更改通知档案是否存在
			$sql="SELECT * FROM #__wel_ecnhdrm WHERE wel_ecn_no='".$this->wel_ecn_no."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_ecn_no_not_found");}
			if($row["wel_appr_yn"]==1){throw new Exception("wel_ecn_no_had_approved");}
			if($row["wel_post_yn"]==1){throw new Exception("wel_ecn_no_had_posted");}
			
			//产品档案是否存在
			$sql="SELECT * FROM #__wel_partflm WHERE wel_part_no='".$this->wel_assm_no."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_assm_no_not_found");}
			if($row["wel_bom_yn"]!=1){throw new Exception("wel_assm_no_not_approved");}
			
			//物料档案是否存在
			$sql="SELECT * FROM #__wel_partflm WHERE wel_part_no='".$this->wel_part_no."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_part_no_not_found");}
			$wel_unit=trim(is_null($row["wel_unit"]) ? "" : $row["wel_unit"]);
			$wel_eng_unit=trim(is_null($row["wel_eng_unit"]) ? "" : $row["wel_eng_unit"]);
			$wel_eng_unit_rate=floatval(is_null($row["wel_eng_unit_rate"]) ? 0 : $row["wel_eng_unit_rate"]);
			
			if($wel_eng_unit==""){
				$wel_eng_unit=$wel_unit;
				$wel_eng_unit_rate=1;
			}
			if($wel_eng_unit_rate<=0){$wel_eng_unit_rate=1;}
			if(strtolower($wel_eng_unit)==strtolower($this->wel_eng_unit)){
			}else{
				if(strtolower($wel_unit)==strtolower($this->wel_eng_unit)){
					$wel_eng_unit_rate=1;
				}else{
					throw new Exception("wel_eng_unit_error");
				}
			}
			$wel_qty_per=number_format(floatval($this->wel_qp_eng)*floatval($wel_eng_unit_rate),6);
			
			//查询物料清单
			$sql="SELECT * FROM #__wel_engbomm WHERE 
				wel_assm_no='".$this->wel_assm_no."' AND wel_part_no='".$this->wel_part_no."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result))	//没有在BOM中找到
			{
				if(strtolower($this->wel_ecn_act)=="update" || 
					strtolower($this->wel_ecn_act)=="delete"){
					throw new Exception("wel_bom_detail_not_found");
				}
				$wel_pre_scp_fact=0;
				$wel_pre_eng_unit="";
				$wel_pre_qp_eng=0;
				$wel_pre_unit_exg=0;
				$wel_pre_qty_per=0;
			}
			else
			{
				if(strtolower($this->wel_ecn_act)=="add"){
					throw new Exception("wel_bom_detail_exist");
				}
				$wel_pre_scp_fact=is_null($row["wel_scp_fact"]) ? 0 : $row["wel_scp_fact"];
				$wel_pre_eng_unit=is_null($row["wel_eng_unit"]) ? "" : $row["wel_eng_unit"];
				$wel_pre_qp_eng=is_null($row["wel_qp_eng"]) ? 0 : $row["wel_qp_eng"];
				$wel_pre_unit_exg=is_null($row["wel_unit_exg"]) ? 0 : $row["wel_unit_exg"];
				$wel_pre_qty_per=is_null($row["wel_qty_per"]) ? 0 : $row["wel_qty_per"];
			}
			
			$sql="SELECT * FROM #__wel_engbomh WHERE wel_ecn_no='".$this->wel_ecn_no."' AND 
				wel_assm_no='".$this->wel_assm_no."' AND wel_part_no='".$this->wel_part_no."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if($row=mysql_fetch_array($result)){throw new Exception("wel_ecn_detail_exist");}

			try
			{
				mysql_query('begin');
				
				$sql="INSERT INTO #__wel_engbomh(
					wel_ecn_no,
					wel_ecn_act,
					wel_assm_no,
					wel_part_no,
					wel_eng_unit,
					wel_qp_eng,
					wel_scp_fact,
					wel_ecn_rmk,
					wel_ecn_loc,
					wel_crt_user,
					wel_crt_date,
					wel_unit_exg,
					wel_qty_per,
					wel_pre_scp_fact,
					wel_pre_eng_unit,
					wel_pre_qp_eng,
					wel_pre_unit_exg,
					wel_pre_qty_per,
					wel_mod_sign 
					)VALUES(
					'".$this->wel_ecn_no."',
					'".$this->wel_ecn_act."',
					'".$this->wel_assm_no."',
					'".$this->wel_part_no."',
					'".$this->wel_eng_unit."',
					".floatval($this->wel_qp_eng).",
					".floatval($this->wel_scp_fact).",
					'".$this->wel_ecn_rmk."',
					'".$this->wel_ecn_loc."',
					'".$_SESSION["wel_user_id"]."',
					now(),
					".floatval($wel_eng_unit_rate).",
					".floatval($wel_qty_per).",
					'".$wel_pre_scp_fact."',
					'".$wel_pre_eng_unit."',
					'".$wel_pre_qp_eng."',
					'".$wel_pre_unit_exg."',
					'".$wel_pre_qty_per."',
					'".$this->wel_mod_sign."')";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				
				mysql_query('commit');
			}
			catch (Exception $e1)
			{
				mysql_query('rollback');
				throw new Exception($e1->getMessage());
			}
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		if($msg_code==""){$msg_code="addnew_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	//编辑工程通知档案细节
	public function edit()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_edit")){throw new Exception("unauthorized_access");}
			
			if($this->wel_ecn_no=="") {throw new Exception("wel_ecn_no_miss");}
			if($this->wel_assm_no=="") {throw new Exception("wel_assm_no_miss");}
			if($this->wel_part_no=="") {throw new Exception("wel_part_no_miss");}
			if($this->wel_eng_unit=="") {throw new Exception("wel_eng_unit_miss");}
			
			if(!is_numeric($this->wel_scp_fact)){$this->wel_scp_fact=0;}
			if(!is_numeric($this->wel_qp_eng)){$this->wel_qp_eng=0;}
			
			if(strtolower($this->wel_ecn_act)=="delete"){
			}else{
				if($this->wel_scp_fact<0){throw new Exception("wel_scp_fact_over_rang");}
				if($this->wel_qp_eng<0){throw new Exception("wel_qp_eng_over_rang");}
				if($this->wel_qp_eng==0){throw new Exception("wel_qp_eng_miss");}
				if($this->wel_eng_unit=="") {throw new Exception("wel_eng_unit_miss");}
			}
			
			//工程单位档案是否存在
			$sql="SELECT * FROM #__wel_unitflm WHERE wel_unit_code='".$this->wel_eng_unit."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_eng_unit_not_found");}
			
			//工程更改通知档案是否存在
			$sql="SELECT * FROM #__wel_ecnhdrm WHERE wel_ecn_no='".$this->wel_ecn_no."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_ecn_no_not_found");}
			if($row["wel_appr_yn"]==1){throw new Exception("wel_ecn_no_had_approved");}
			if($row["wel_post_yn"]==1){throw new Exception("wel_ecn_no_had_posted");}
			
			//产品档案是否存在
			$sql="SELECT * FROM #__wel_partflm WHERE wel_part_no='".$this->wel_assm_no."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_assm_no_not_found");}
			if($row["wel_bom_yn"]!=1){throw new Exception("wel_assm_no_not_approved");}
			
			//物料档案是否存在
			$sql="SELECT * FROM #__wel_partflm WHERE wel_part_no='".$this->wel_part_no."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_part_no_not_found");}
			$wel_unit=trim(is_null($row["wel_unit"]) ? "" : $row["wel_unit"]);
			$wel_eng_unit=trim(is_null($row["wel_eng_unit"]) ? "" : $row["wel_eng_unit"]);
			$wel_eng_unit_rate=floatval(is_null($row["wel_eng_unit_rate"]) ? 0 : $row["wel_eng_unit_rate"]);
			
			if($wel_eng_unit==""){
				$wel_eng_unit=$wel_unit;
				$wel_eng_unit_rate=1;
			}
			if($wel_eng_unit_rate<=0){$wel_eng_unit_rate=1;}
			if(strtolower($wel_eng_unit)==strtolower($this->wel_eng_unit)){
			}else{
				if(strtolower($wel_unit)==strtolower($this->wel_eng_unit)){
					$wel_eng_unit_rate=1;
				}else{
					throw new Exception("wel_eng_unit_error");
				}
			}
			$wel_qty_per=number_format(floatval($this->wel_qp_eng)*floatval($wel_eng_unit_rate),6);
			
			//查询物料清单
			$sql="SELECT * FROM #__wel_engbomm WHERE 
				wel_assm_no='".$this->wel_assm_no."' AND wel_part_no='".$this->wel_part_no."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result))	//没有在BOM中找到
			{
				if(strtolower($this->wel_ecn_act)=="update" || 
					strtolower($this->wel_ecn_act)=="delete"){
					throw new Exception("wel_bom_detail_not_found");
				}
				$wel_pre_scp_fact=0;
				$wel_pre_eng_unit="";
				$wel_pre_qp_eng=0;
				$wel_pre_unit_exg=0;
				$wel_pre_qty_per=0;
			}
			else
			{
				if(strtolower($this->wel_ecn_act)=="add"){
					throw new Exception("wel_bom_detail_exist");
				}
				$wel_pre_scp_fact=is_null($row["wel_scp_fact"]) ? 0 : $row["wel_scp_fact"];
				$wel_pre_eng_unit=is_null($row["wel_eng_unit"]) ? "" : $row["wel_eng_unit"];
				$wel_pre_qp_eng=is_null($row["wel_qp_eng"]) ? 0 : $row["wel_qp_eng"];
				$wel_pre_unit_exg=is_null($row["wel_unit_exg"]) ? 0 : $row["wel_unit_exg"];
				$wel_pre_qty_per=is_null($row["wel_qty_per"]) ? 0 : $row["wel_qty_per"];
			}
			
			$sql="SELECT * FROM #__wel_engbomh WHERE wel_ecn_no='".$this->wel_ecn_no."' AND 
				wel_assm_no='".$this->wel_assm_no."' AND wel_part_no='".$this->wel_part_no."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_ecn_detail_not_found");}
			
			try
			{
				mysql_query('begin');
				
				$sql="UPDATE #__wel_engbomh SET 
						wel_ecn_act='".$this->wel_ecn_act."',
						wel_eng_unit='".$this->wel_eng_unit."',
						wel_qp_eng=".floatval($this->wel_qp_eng).",
						wel_scp_fact=".floatval($this->wel_scp_fact).",
						wel_ecn_rmk='".$this->wel_ecn_rmk."',
						wel_ecn_loc='".$this->wel_ecn_loc."',
						wel_upd_user='".$_SESSION["wel_user_id"]."',
						wel_upd_date=now(),
						wel_unit_exg=".floatval($wel_eng_unit_rate).",
						wel_qty_per=".floatval($wel_qty_per).",
						wel_pre_scp_fact='".$wel_pre_scp_fact."',
						wel_pre_eng_unit='".$wel_pre_eng_unit."',
						wel_pre_qp_eng='".$wel_pre_qp_eng."',
						wel_pre_unit_exg='".$wel_pre_unit_exg."',
						wel_pre_qty_per='".$wel_pre_qty_per."',
						wel_mod_sign='".$this->wel_mod_sign."' 
					WHERE wel_ecn_no='".$this->wel_ecn_no."' AND 
					wel_assm_no='".$this->wel_assm_no."' AND wel_part_no='".$this->wel_part_no."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
					
				mysql_query('commit');
			}
			catch (Exception $e1)
			{
				mysql_query('rollback');
				throw new Exception($e1->getMessage());
			}
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}	
		if($msg_code==""){$msg_code="edit_succee";}	
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
}
?>