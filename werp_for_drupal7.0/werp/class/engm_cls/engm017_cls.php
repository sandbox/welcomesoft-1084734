<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>

<?php 
class engm017_cls
{
	public $wel_unit_code="";
	public $wel_unit_des="";
	
	private $wel_prog_code="engm017";
	//读取单位档案
	public function read()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_read")){throw new Exception("unauthorized_access");}
			
			$sql="select * from #__wel_unitflm where wel_unit_code='".$this->wel_unit_code."' limit 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	//查询sql时出错了
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_unit_code_not_found");}	//没有符合条件的记录
			foreach ($row as $key=>$value){$return_val[$key]=$value;}
			throw new Exception("");
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	//新增单位档案
	public function addnew()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			if($this->wel_unit_code==""){throw new Exception("wel_unit_code_miss");}
			if($this->wel_unit_des==""){throw new Exception("wel_unit_des_miss");}
			
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_addnew")){throw new Exception("unauthorized_access");}
			
			//单位档案是否存在
			$sql="select * from #__wel_unitflm where wel_unit_code='".$this->wel_unit_code."' limit 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if($row=mysql_fetch_array($result)){throw new Exception("wel_unit_code_exist");}
			
			try
			{
				mysql_query('begin');
				//插入记录到wel_unitflm中
				$sql="insert into #__wel_unitflm(wel_unit_code,wel_unit_des)".
							" values('".$this->wel_unit_code."','".$this->wel_unit_des."')";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				mysql_query('commit');
			}
			catch (Exception $e1)
			{
				mysql_query('rollback');
				$msg_code=$e1->getMessage();
			}
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		if($msg_code==""){$msg_code="addnew_succee";}
		$return_val["msg_code"]=$msg_code;
		$return_val["wel_unit_code"]=$this->wel_unit_code;
		return $return_val;
	}
	//编辑单位档案
	public function edit()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			if($this->wel_unit_code==""){throw new Exception("wel_unit_code_miss");}
			if($this->wel_unit_des==""){throw new Exception("wel_unit_des_miss");}
			
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_edit")){throw new Exception("unauthorized_access");}
			
			//单位档案是否存在
			$sql="select * from #__wel_unitflm where wel_unit_code='".$this->wel_unit_code."' limit 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_unit_code_not_found");}
			
			try
			{
				mysql_query('begin');
				//更新记录
				$sql="update #__wel_unitflm set wel_unit_des='".$this->wel_unit_des."' ".
					"where wel_unit_code='".$this->wel_unit_code."' limit 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				mysql_query('commit');
			}
			catch (Exception $e1)
			{
				mysql_query('rollback');
				$msg_code=$e1->getMessage();
			}
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		if($msg_code==""){$msg_code="edit_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	//删除单位档案
	public function delete()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			if($this->wel_unit_code==""){throw new Exception("wel_unit_code_miss");}
			
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_delete")){throw new Exception("unauthorized_access");}
			
			//单位档案是否存在
			$sql="select * from #__wel_unitflm where wel_unit_code='".$this->wel_unit_code."' limit 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_unit_code_not_found");}
			
			try
			{
				mysql_query('begin');
				//删除记录
				$sql="delete from #__wel_unitflm where wel_unit_code='".$this->wel_unit_code."' limit 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				mysql_query('commit');
			}
			catch (Exception $e1)
			{
				mysql_query('rollback');
				$msg_code=$e1->getMessage();
			}
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		if($msg_code==""){$msg_code="delete_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
}
?>