<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT

?>
<?php
class mktm002a_cls
{
	public $wel_cus_code="";
	public $wel_cont_line="";
	public $wel_cont_man="";
	public $wel_cont_tel="";
	public $wel_cont_fax="";
	public $wel_cont_tel2="";
	public $wel_cont_mob="";
	public $wel_email="";
	public $wel_appointment="";
	public $wel_dept="";
	public $wel_hobby="";
	public $wel_birthday="";
	public $wel_default="";

	private $wel_prog_code="mktm002";

	public function read()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_read")){throw new Exception("unauthorized_access");}
		
			$sql="SELECT * FROM #__wel_cuscont WHERE wel_cus_code='$this->wel_cus_code' AND wel_cont_line ='$this->wel_cont_line' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_cus_con_not_found");}
			$int__count=0;
			while ($int__count<mysql_num_fields($result)) 
			{
				$field_name=mysql_fetch_field($result,$int__count)->name;
				$return_val[$field_name]=$row[$field_name];
				$int__count++;
			}
			mysql_free_result($result);
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	public function addnew()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{			
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_addnew")){throw new Exception("unauthorized_access");}
			
			if ($this->wel_cont_man=="") {throw new Exception("wel_cus_con_miss");}	
				
			$this->wel_birthday=(($this->wel_birthday=="") ? "null" : "'".$this->wel_birthday."'");
			
			$sql="SELECT MAX(wel_cont_line) AS wel_cont_line FROM #__wel_cuscont WHERE wel_cus_code='$this->wel_cus_code' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){$this->wel_cont_line=1;}
			$int__cont_line=intval(is_null($row["wel_cont_line"]) ? 0 : $row["wel_cont_line"])+1;
			
			$sql="SELECT * FROM #__wel_cuscont WHERE wel_cus_code='$this->wel_cus_code' AND wel_cont_man='$this->wel_cont_man' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
			if($row=mysql_fetch_array($result)){throw new Exception("wel_cus_con_exist");}
		
			try
			{
				mysql_query("begin");
				
					//对默认值设置 start
					if($this->wel_default==1)
					{
						$sql="UPDATE #__wel_cuscont SET wel_default=0 WHERE wel_cus_code='$this->wel_cus_code'";
						$sql=revert_to_the_available_sql($sql);
						if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					}
					//对默认值设置 end
					$sql="INSERT INTO #__wel_cuscont(wel_cus_code,wel_cont_line,wel_cont_man,wel_cont_tel,wel_cont_fax,".
								"wel_cont_tel2,wel_cont_mob,wel_email,wel_appointment,wel_dept,wel_hobby,wel_birthday,wel_default) ".
							"VALUES ('$this->wel_cus_code','$int__cont_line','$this->wel_cont_man','$this->wel_cont_tel',".
							"'$this->wel_cont_fax','$this->wel_cont_tel2','$this->wel_cont_mob','$this->wel_email',".
							"'$this->wel_appointment','$this->wel_dept','$this->wel_hobby',$this->wel_birthday,'$this->wel_default')";			
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					
				mysql_query("commit");
					
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}
				
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		if($msg_code==""){$msg_code="addnew_succee";}
		$return_val["msg_code"]=$msg_code;
		$return_val["wel_cus_code"]=$this->wel_cus_code;
		$return_val["wel_cont_line"]=$this->wel_cont_line;
		return $return_val;
	}
	
	public function edit()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{			
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_edit")){throw new Exception("unauthorized_access");}
			
			$this->wel_birthday=(($this->wel_birthday=="") ? "null" : "'".$this->wel_birthday."'");
			
			$sql="SELECT * FROM #__wel_cuscont WHERE wel_cus_code='$this->wel_cus_code' AND wel_cont_man='$this->wel_cont_man' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_cus_con_not_found");}
			
			try
			{
				mysql_query("begin");
				
					//对默认值设置 start
					if($this->wel_default== 1)
					{
						$sql="UPDATE #__wel_cuscont SET wel_default=0 WHERE wel_cus_code='$this->wel_cus_code'";
						$sql=revert_to_the_available_sql($sql);
						if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					}
					
					//对默认值设置 end
					$sql="UPDATE  #__wel_cuscont SET ".
							"wel_cont_tel='$this->wel_cont_tel',".
							"wel_cont_fax='$this->wel_cont_fax',".
							"wel_cont_tel2='$this->wel_cont_tel2',".
							"wel_cont_mob='$this->wel_cont_mob',".
							"wel_email='$this->wel_email',".
							"wel_appointment='$this->wel_appointment',".
							"wel_dept='$this->wel_dept',".
							"wel_hobby='$this->wel_hobby',".
							"wel_birthday=$this->wel_birthday, ".
							"wel_default='$this->wel_default' ".
						"WHERE wel_cus_code='$this->wel_cus_code' AND wel_cont_man='$this->wel_cont_man'";			
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					
				mysql_query("commit");
					
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}
			
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}		
		if($msg_code==""){$msg_code="edit_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
}
?>