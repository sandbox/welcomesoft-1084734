<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT

?>
<?php
class mktm002b_cls
{
	public $wel_cus_code="";
	public $wel_dely_line=0;
	public $wel_dely_des="";
	public $wel_dely_add1="";
	public $wel_dely_add2="";
	public $wel_dely_add3="";
	public $wel_dely_add4="";
	public $wel_dely_cont="";
	public $wel_dely_tel="";
	public $wel_dely_fax="";
	public $wel_dely_email="";
	public $wel_dely_mob="";
	public $wel_default="";
	
	private $wel_prog_code="mktm002";

	public function read()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_read")){throw new Exception("unauthorized_access");}
			
			$sql="SELECT * FROM #__wel_delyflm WHERE wel_cus_code='$this->wel_cus_code' AND wel_dely_line ='$this->wel_dely_line' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_dely_line_not_found");}
			$int__count=0;
			while ($int__count<mysql_num_fields($result)) 
			{
				$field_name=mysql_fetch_field($result,$int__count)->name;
				$return_val[$field_name]=$row[$field_name];
				$int__count++;
			}
			mysql_free_result($result);
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	public function addnew()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_addnew")){throw new Exception("unauthorized_access");}
		
			if (!is_numeric($this->wel_dely_line)){$this->wel_dely_line=0;}
			if ($this->wel_dely_des=="") {throw new Exception("wel_dely_code_des_miss");}
			
			if ($this->wel_dely_line==0)
			{
				$sql="SELECT MAX(wel_dely_line) AS wel_dely_line FROM #__wel_delyflm WHERE wel_cus_code='$this->wel_cus_code' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){$this->wel_dely_line=1;}
				$int__dely_line=intval(is_null($row["wel_dely_line"]) ? 0 : $row["wel_dely_line"])+1;
			} else {
				$int__dely_line=$this->wel_dely_line;
			}
			
			$sql="SELECT * FROM #__wel_delyflm WHERE wel_cus_code='$this->wel_cus_code' AND wel_dely_line='$int__dely_line' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}	
			if($row=mysql_fetch_array($result)){throw new Exception("wel_dely_line_exist");}
			
			try
			{
				mysql_query("begin");
				
					//对默认值设置 start
					if($this->wel_default == 1)
					{
						$sql="UPDATE #__wel_delyflm SET wel_default=0 WHERE wel_cus_code='$this->wel_cus_code'";
						$sql=revert_to_the_available_sql($sql);
						if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					}
					
					//对默认值设置 end
					$sql="INSERT INTO #__wel_delyflm(wel_cus_code,wel_dely_line,wel_dely_code,wel_dely_des,".
							"wel_dely_add1,wel_dely_add2,wel_dely_add3,wel_dely_add4,".
							"wel_dely_cont,wel_dely_tel,wel_dely_fax,wel_dely_email,wel_dely_mob,wel_default) ".
						"VALUES('$this->wel_cus_code','$int__dely_line','$int__dely_line','$this->wel_dely_des',".
							"'$this->wel_dely_add1','$this->wel_dely_add2','$this->wel_dely_add3','$this->wel_dely_add4',".
							"'$this->wel_dely_cont','$this->wel_dely_tel','$this->wel_dely_fax','$this->wel_dely_email',".
							"'$this->wel_dely_mob','$this->wel_default')";			
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					
				mysql_query("commit");
					
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}
			
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		if($msg_code==""){$msg_code="addnew_succee";}
		$return_val["msg_code"]=$msg_code;
		$return_val["wel_cus_code"]=$this->wel_cus_code;
		$return_val["wel_dely_line"]=$this->wel_dely_line;
		return $return_val;
	}
	
	public function edit()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_edit")){throw new Exception("unauthorized_access");}
		
			if (!is_numeric($this->wel_dely_line)){$this->wel_dely_line=0;}
			if ($this->wel_dely_des=="") {throw new Exception("wel_dely_code_des_miss");}
			
			$sql="SELECT * FROM #__wel_delyflm WHERE wel_cus_code='$this->wel_cus_code' AND wel_dely_line='$this->wel_dely_line' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_dely_line_not_found");}

			try
			{
				mysql_query("begin");
			
				//对默认值设置 start
				if($this->wel_default == 1)
				{
					$sql="UPDATE #__wel_delyflm SET wel_default=0 WHERE wel_cus_code='$this->wel_cus_code'";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				}
				
				//对默认值设置 end
				$sql="UPDATE #__wel_delyflm SET ".
						"wel_dely_des='$this->wel_dely_des',".
						"wel_dely_add1='$this->wel_dely_add1',".
						"wel_dely_add2='$this->wel_dely_add2',".
						"wel_dely_add3='$this->wel_dely_add3',".
						"wel_dely_add4='$this->wel_dely_add4',".
						"wel_dely_cont='$this->wel_dely_cont',".
						"wel_dely_tel='$this->wel_dely_tel',".
						"wel_dely_fax='$this->wel_dely_fax',".
						"wel_dely_email='$this->wel_dely_email',".
						"wel_dely_mob='$this->wel_dely_mob',".
						"wel_default='$this->wel_default' ".
					"WHERE wel_cus_code='$this->wel_cus_code' AND wel_dely_line='$this->wel_dely_line'";			
				$sql=revert_to_the_available_sql($sql);
				if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					
				mysql_query("commit");
					
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}
			
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}		
		if($msg_code==""){$msg_code="edit_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
}
?>