<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT

?>
<?php
class mktm003_cls{
	public $wel_cur_code="";
	public $wel_cur_des="";
	public $wel_ex_rate=0;
	private $wel_prog_code="mktm003";

	public function read(){
		$msg_code="";
		$return_val=array();
		try{
			$conn=werp_db_connect();
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_read")){throw new Exception("unauthorized_access");}
			
			$sql="SELECT * FROM #__wel_currenm WHERE wel_cur_code='$this->wel_cur_code' limit 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	//查询sql时出错了
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_cur_code_not_found");}	//没有符合条件的记�?
			$int__count=0;
			while ($int__count < mysql_num_fields($result)) {
				$field_name=mysql_fetch_field($result,$int__count)->name;
				$return_val[$field_name]=$row[$field_name];
				//$msg_code=$msg_code."|".$field_name;
			$int__count++;
			}
			mysql_free_result($result);
			throw new Exception("");
		}
		catch (Exception $e){
			$msg_code=$e->getMessage();
		}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	public function addnew(){
		$msg_code="";
		$return_val=array();
		try{			
			$conn=werp_db_connect();		
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_addnew")){throw new Exception("unauthorized_access");}
			
			if ($this->wel_cur_code==""){throw new Exception("wel_cur_code_miss");}		
			if ($this->wel_cur_des==""){throw new Exception("wel_cur_des_miss");}		
			if ($this->wel_ex_rate==""){throw new Exception("wel_ex_rate_miss");}		
			if (!is_numeric($this->wel_ex_rate)){$this->wel_ex_rate=0;}
			if (strlen($this->wel_cur_code)<2 || strlen($this->wel_cur_code)>4){throw new Exception("wel_cur_code_length");}
			
			$sql="SELECT * FROM #__wel_currenm WHERE wel_cur_code='$this->wel_cur_code' limit 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}	//查询sql时出错了
			if($row=mysql_fetch_array($result))	throw new Exception("wel_cur_code_exist");	//单据已存�?

			mysql_query('begin');
			$sql="INSERT INTO #__wel_currenm(wel_cur_code,wel_cur_des,wel_ex_rate) VALUES ('$this->wel_cur_code','$this->wel_cur_des','$this->wel_ex_rate')";
			$sql=revert_to_the_available_sql($sql);
			
			if(!mysql_query($sql,$conn)){
				$msg_code=mysql_error();
				mysql_query('rollback');
				throw new Exception($msg_code);
			}
			mysql_free_result($result);
			mysql_query('commit');
			throw new Exception("addnew_succee"); 
		}
		catch (Exception $e){
			$msg_code=$e->getMessage();
		}
		$return_val["msg_code"]=$msg_code;
		$return_val["wel_cur_code"]=$this->wel_cur_code;
		return $return_val;
	}
	
	public function edit(){
		$msg_code="";
		$return_val=array();
		try{
			$conn=werp_db_connect();
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_edit")){throw new Exception("unauthorized_access");}

			if ($this->wel_cur_code==""){throw new Exception("wel_cur_code_miss");}		
			if ($this->wel_cur_des==""){throw new Exception("wel_cur_des_miss");}		
			if ($this->wel_ex_rate==""){throw new Exception("wel_ex_rate_miss");}		
			if (!is_numeric($this->wel_ex_rate)){$this->wel_ex_rate=0;}
			if (strlen($this->wel_cur_code)<2 || strlen($this->wel_cur_code)>4){throw new Exception("wel_cur_code_length");}
			
			$sql="SELECT * FROM #__wel_currenm WHERE wel_cur_code='$this->wel_cur_code' limit 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}	//查询sql时出错了
			if(!($row=mysql_fetch_array($result))){
				throw new Exception("wel_cur_code_not_found");	//没有符合条件的记�?
			}
			
			
			mysql_query('begin');
			$sql="UPDATE #__wel_currenm set wel_cur_des='$this->wel_cur_des',wel_ex_rate='$this->wel_ex_rate' WHERE wel_cur_code='$this->wel_cur_code'";
			$sql=revert_to_the_available_sql($sql);

			if(mysql_query($sql,$conn)){
				mysql_free_result($result);
				mysql_query('commit');
				throw new Exception("edit_succee"); 
			}
			else{
				$msg_code=mysql_error();
				mysql_query('rollback');
				throw new Exception($msg_code); 
			}
		}
		catch (Exception $e){
			$msg_code=$e->getMessage();
		}		
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	public function delete(){
		$msg_code="";
		$return_val=array();
		try{
			$conn=werp_db_connect();
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_delete")){throw new Exception("unauthorized_access");}
		
			$sql="SELECT * FROM #__wel_currenm WHERE wel_cur_code='$this->wel_cur_code' limit 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}	//查询sql时出错了
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_cur_code_not_found");}	//没有符合条件的记�?
			mysql_query('begin');
			$sql="DELETE FROM #__wel_currenm WHERE wel_cur_code='$this->wel_cur_code'";
			$sql=revert_to_the_available_sql($sql);
			if(mysql_query($sql,$conn)){
				mysql_free_result($result);
				mysql_query('commit');
				throw new Exception("delete_succee"); 
			}
			else{
				$msg_code=mysql_error();
				mysql_query('rollback');
				throw new Exception($msg_code); 
			}			
		}catch (Exception $e){
			$msg_code=$e->getMessage();
		}		
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
}
?>
