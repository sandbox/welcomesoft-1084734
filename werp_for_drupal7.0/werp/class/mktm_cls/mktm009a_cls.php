<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT

//require_once(WERP_SITE_PATH_INC."mktm_stdlib_cls.php");
?>
<?php
class mktm009a_cls
{
	public $wel_so_no="";
	public $wel_so_line=0;
	public $wel_cus_code="";
	public $wel_cur_code="";
	public $wel_part_no="";
	public $wel_u_price=0;
	public $wel_req_qty=0;
	public $wel_req_date="";
	public $wel_area_code="";
	public $wel_discount=0;
	public $wel_tax_rate=0;
	public $wel_cus_date="";
	public $wel_pmc_date="";
	public $wel_so_rmk="";

	private $wel_prog_code="mktm009";

	public function read()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_read")){throw new Exception("unauthorized_access");}
			
			$sql="SELECT r.*,p.wel_part_des,p.wel_part_des1,p.wel_part_des2 ".
				 "FROM #__wel_sordetm r ".
					"LEFT JOIN #__wel_partflm p ON r.wel_part_no=p.wel_part_no ".
				 "WHERE r.wel_so_no='$this->wel_so_no' AND r.wel_so_line='$this->wel_so_line' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	//查询sql时出错了
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_so_no_not_found");}	//没有符合条件的记录
			$int__count=0;
			while ($int__count < mysql_num_fields($result)) 
			{
				$field_name=mysql_fetch_field($result,$int__count)->name;
				$return_val[$field_name]=$row[$field_name];
				$int__count++;
			}
			mysql_free_result($result);
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	public function addnew()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_addnew")){throw new Exception("unauthorized_access");}
			
//			if($this->wel_so_no==""){throw new Exception("wel_so_no_not_found");}
			if($this->wel_part_no==""){throw new Exception("wel_part_no_miss");}
			
			if (!is_numeric($this->wel_req_qty)){$this->wel_req_qty=0;}
			$this->wel_req_qty=doubleval($this->wel_req_qty);
			if($this->wel_req_qty==0){throw new Exception("wel_req_qty_miss");}
			if($this->wel_req_qty<0){throw new Exception("wel_req_qty_error");}
			
			if (!is_numeric($this->wel_u_price)){$this->wel_u_price=0;}	
			$this->wel_u_price=doubleval($this->wel_u_price);
//			Accept zero price item
//			if($this->wel_u_price==0){throw new Exception("wel_u_price_miss");}
			if($this->wel_u_price<0){throw new Exception("wel_u_price_error");}
			
			if($this->wel_req_date==""){throw new Exception("wel_req_date_miss");}
//			if($this->wel_area_code==""){throw new Exception("wel_area_code_miss");}
			
			if (!is_numeric($this->wel_tax_rate)){$this->wel_tax_rate=0;}
			$this->wel_tax_rate=doubleval($this->wel_tax_rate);
			if($this->wel_tax_rate<0 || $this->wel_tax_rate>100){throw new Exception("wel_tax_rate_error");}
			if (!is_numeric($this->wel_discount)){$this->wel_discount=0;}
			$this->wel_discount=doubleval($this->wel_discount);
			if($this->wel_discount<0 || $this->wel_discount>100){throw new Exception("wel_discount_error");}
			
			$this->wel_req_date=(($this->wel_req_date=="") ? "null" : "'".$this->wel_req_date."'");
			$this->wel_cus_date=(($this->wel_cus_date=="") ? "null" : "'".$this->wel_cus_date."'");
			$this->wel_pmc_date=(($this->wel_pmc_date=="") ? "null" : "'".$this->wel_pmc_date."'");

			//检查地区码
			if($this->wel_area_code!="")
			{
				$sql="SELECT wel_area_code from #__wel_areaflm WHERE wel_area_code='".$this->wel_area_code."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_area_code_not_found");}
			}
			
			//检验成品物料表并返回信息
			if($this->wel_part_no!="")
			{
				$sql="SELECT wel_part_no from #__wel_partflm WHERE wel_part_no='".$this->wel_part_no."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_part_no_not_found");}
			}
			
			// Get Info from SORHDRM
			$sql="SELECT * FROM #__wel_sorhdrm WHERE wel_so_no = '$this->wel_so_no' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_so_no_not_found");}
			$str_cus_code=is_null($row["wel_cus_code"]) ? "" : $row["wel_cus_code"];
			$int_line=intval(is_null($row["wel_last_line"]) ? 0 : $row["wel_last_line"])+1;
			$int_dis_type_h=intval(is_null($row["wel_dis_type"]) ? 0 : $row["wel_dis_type"]);
			$int_dis_rate_h=doubleval(is_null($row["wel_discount"]) ? 0 : $row["wel_discount"]);
			$int_tax_type_h=intval(is_null($row["wel_tax_type"]) ? 0 : $row["wel_tax_type"]);

			$dec_wel_item_amt = Round($this->wel_req_qty*$this->wel_u_price,2);
			$dec_wel_dis_amt = 0;
			$dec_wel_dis_amt2 = 0;
			$dec_wel_tax_amt = 0;
			$dec_wel_line_amt = 0;
			if($this->wel_discount>0)
			{
				$dec_wel_dis_amt = Round($dec_wel_item_amt * $this->wel_discount / 100,2);
			}
			if ( ($int_dis_type_h==1 or $int_dis_type_h==3) and $this->wel_discount==0)
			{
				$dec_wel_dis_amt2 = Round(($dec_wel_item_amt-$dec_wel_dis_amt) * $int_dis_rate_h / 100,2);
			}
			if ( $int_dis_type_h==4 or $int_dis_type_h==6 )
			{
				$dec_wel_dis_amt2 = Round(($dec_wel_item_amt-$dec_wel_dis_amt) * $int_dis_rate_h / 100,2);
			}

			$dec_wel_line_amt = $dec_wel_item_amt-$dec_wel_dis_amt-$dec_wel_dis_amt2;
			// 價外稅
			if ( $int_tax_type_h==2 )
			{
				$dec_wel_tax_amt = Round($dec_wel_line_amt * $this->wel_tax_rate / 100,2);
				$dec_wel_line_amt = $dec_wel_line_amt + $dec_wel_tax_amt;
			}
			// 價內稅
			if ( $int_tax_type_h==1 )
			{
				$dec_wel_tax_amt = Round($dec_wel_line_amt * $this->wel_tax_rate /(100 + $this->wel_tax_rate),2);
			}

			// Check S/O Line exist
			$sql="SELECT wel_so_no,wel_so_line FROM #__wel_sordetm ".
				"WHERE wel_so_no='$this->wel_so_no' AND ".
				"(wel_so_line='$int_line') LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
			if($row=mysql_fetch_array($result))
			{
				$sql="SELECT max(wel_so_line) as max_line FROM #__wel_sordetm ".
					"WHERE wel_so_no='$this->wel_so_no' ";
				$sql=revert_to_the_available_sql($sql);
				if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_so_no_not_found");}
				$int_line=intval(is_null($row["max_line"]) ? 0 : $row["max_line"])+1;
			}

			try
			{
				mysql_query("begin");
				
					$sql="INSERT INTO #__wel_sordetm SET ".
						"wel_so_no='$this->wel_so_no', ".
						"wel_so_line='$int_line', ".
						"wel_cus_code='$this->wel_cus_code', ".
						"wel_part_no='$this->wel_part_no', ".
						"wel_u_price='$this->wel_u_price', ".
						"wel_req_qty='$this->wel_req_qty', ".
						"wel_os_qty='$this->wel_req_qty', ".
						"wel_req_date=$this->wel_req_date, ".
						"wel_area_code='$this->wel_area_code', ".
						"wel_cus_date=$this->wel_cus_date, ".
						"wel_pmc_date=$this->wel_pmc_date, ".
						"wel_discount='$this->wel_discount', ".
						"wel_tax_rate='$this->wel_tax_rate', ".
						"wel_item_amt='$dec_wel_item_amt',".
						"wel_dis_amt='$dec_wel_dis_amt',".
						"wel_dis_amt2='$dec_wel_dis_amt2',".
						"wel_tax_amt='$dec_wel_tax_amt',".
						"wel_line_amt='$dec_wel_line_amt',".
						"wel_so_rmk='$this->wel_so_rmk', ".
						"wel_crt_user='{$_SESSION['wel_user_id']}', ".
						"wel_crt_date=now()";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					
					// Update SORHDRM discount & Tax
					$tmp_item_amt=0;
					$tmp_dis_amt=0;
					$tmp_dis_amt2=0;
					$tmp_tax_amt=0;
					$tmp_line_amt=0;
					$sql="SELECT ".
						"SUM(wel_item_amt) as s1,".
						"SUM(wel_dis_amt) as s2,".
						"sum(wel_dis_amt2) as s3,".
						"SUM(wel_tax_amt) as s4,".
						"SUM(wel_line_amt) as s5 ".
						"FROM #__wel_sordetm ".
						"WHERE wel_so_no='$this->wel_so_no' ";
					$sql=revert_to_the_available_sql($sql);
					if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
					if( $row=mysql_fetch_array($result) )
					{
						$tmp_item_amt=doubleval(is_null($row["s1"]) ? 0 : $row["s1"]);
						$tmp_dis_amt =doubleval(is_null($row["s2"]) ? 0 : $row["s2"]);
						$tmp_dis_amt2=doubleval(is_null($row["s3"]) ? 0 : $row["s3"]);
						$tmp_tax_amt =doubleval(is_null($row["s4"]) ? 0 : $row["s4"]);
						$tmp_line_amt=doubleval(is_null($row["s5"]) ? 0 : $row["s5"]);
					}

					$sql="SELECT ".
						"SUM(wel_item_amt) as s1,".
						"SUM(wel_dis_amt) as s2,".
						"sum(wel_dis_amt2) as s3,".
						"SUM(wel_tax_amt) as s4,".
						"SUM(wel_line_amt) as s5 ".
						"FROM #__wel_sormism ".
						"WHERE wel_so_no='$this->wel_so_no' ";
					$sql=revert_to_the_available_sql($sql);
					if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
					if( $row=mysql_fetch_array($result) )
					{
						$tmp_item_amt=$tmp_item_amt + doubleval(is_null($row["s1"]) ? 0 : $row["s1"]);
						$tmp_dis_amt =$tmp_dis_amt  + doubleval(is_null($row["s2"]) ? 0 : $row["s2"]);
						$tmp_dis_amt2=$tmp_dis_amt2 + doubleval(is_null($row["s3"]) ? 0 : $row["s3"]);
						$tmp_tax_amt =$tmp_tax_amt  + doubleval(is_null($row["s4"]) ? 0 : $row["s4"]);
						$tmp_line_amt=$tmp_line_amt + doubleval(is_null($row["s5"]) ? 0 : $row["s5"]);
					}

					$sql="UPDATE #__wel_sorhdrm SET ".
						"wel_item_amt='$tmp_item_amt',".
						"wel_dis_amt='$tmp_dis_amt',".
						"wel_dis_amt2='$tmp_dis_amt2',".
						"wel_tax_amt='$tmp_tax_amt',".
						"wel_order_amt='$tmp_line_amt', ".
						"wel_last_line='$int_line',".
						"wel_upd_user='{$_SESSION['wel_user_id']}',".
						"wel_upd_date=now() ".
						"WHERE wel_so_no='$this->wel_so_no'";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					
				mysql_query("commit");
			}
			catch (Exception $e1)
			{
				rollbacktrans($database);
				throw new Exception($e1->getMessage());
			}
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		if($msg_code==""){$msg_code="addnew_succee";}
		$return_val["msg_code"]=$msg_code;
		$return_val["wel_so_no"]=$this->wel_so_no;
		$return_val["wel_so_line"]=$this->wel_so_line;
		return $return_val;
	}

	
	public function edit()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_edit")){throw new Exception("unauthorized_access");}
			
//			if($this->wel_so_no==""){throw new Exception("wel_so_no_not_found");}
//			if($this->wel_part_no==""){throw new Exception("wel_part_no_miss");}
			
			if (!is_numeric($this->wel_req_qty)){$this->wel_req_qty=0;}
			$this->wel_req_qty=doubleval($this->wel_req_qty);
			if($this->wel_req_qty==0){throw new Exception("wel_req_qty_miss");}
			if($this->wel_req_qty<0){throw new Exception("wel_req_qty_error");}
			
			if (!is_numeric($this->wel_u_price)){$this->wel_u_price=0;}	
			$this->wel_u_price=doubleval($this->wel_u_price);
			if($this->wel_u_price==0){throw new Exception("wel_u_price_miss");}
			if($this->wel_u_price<0){throw new Exception("wel_u_price_error");}
			
			if($this->wel_req_date==""){throw new Exception("wel_req_date_miss");}
//			if($this->wel_area_code==""){throw new Exception("wel_area_code_miss");}
			
			if (!is_numeric($this->wel_tax_rate)){$this->wel_tax_rate=0;}
			$this->wel_tax_rate=doubleval($this->wel_tax_rate);
			if($this->wel_tax_rate<0 || $this->wel_tax_rate>100){throw new Exception("wel_tax_rate_error");}
			if (!is_numeric($this->wel_discount)){$this->wel_discount=0;}
			$this->wel_discount=doubleval($this->wel_discount);
			if($this->wel_discount<0 || $this->wel_discount>100){throw new Exception("wel_discount_error");}
			
			$this->wel_req_date=(($this->wel_req_date=="") ? "null" : "'".$this->wel_req_date."'");
			$this->wel_cus_date=(($this->wel_cus_date=="") ? "null" : "'".$this->wel_cus_date."'");
			$this->wel_pmc_date=(($this->wel_pmc_date=="") ? "null" : "'".$this->wel_pmc_date."'");

			//检查地区码
			if($this->wel_area_code!="")
			{
				$sql="SELECT wel_area_code from #__wel_areaflm WHERE wel_area_code='".$this->wel_area_code."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_area_code_not_found");}
			}
			
			//检查订单信息
			// Get Info from SORHDRM
			$sql="SELECT * FROM #__wel_sorhdrm WHERE wel_so_no = '$this->wel_so_no' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_so_no_not_found");}
			$int_dis_type_h=intval(is_null($row["wel_dis_type"]) ? 0 : $row["wel_dis_type"]);
			$int_dis_rate_h=doubleval(is_null($row["wel_discount"]) ? 0 : $row["wel_discount"]);
			$int_tax_type_h=intval(is_null($row["wel_tax_type"]) ? 0 : $row["wel_tax_type"]);
			
			//检查订单明细信息
			$sql="SELECT IFNULL(wel_req_qty,0) AS wel_req_qty,".
					"IFNULL(wel_u_price,0) AS wel_u_price,".
					"IFNULL(wel_tax_rate,0) AS wel_tax_rate,".
					"IFNULL(wel_req_qty,0) AS wel_req_qty,".
					"IFNULL(wel_os_qty,0) AS wel_os_qty,".
					"IFNULL(wel_do_qty,0) AS wel_do_qty,".
					"IFNULL(wel_dn_qty,0) AS wel_dn_qty,".
					"wel_req_date,wel_cus_date FROM #__wel_sordetm ".
				"WHERE wel_so_no='$this->wel_so_no' AND wel_so_line='$this->wel_so_line' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_so_line_not_found");}
			$old_req_qty=doubleval(is_null($row["wel_req_qty"]) ? 0 : $row["wel_req_qty"]);
			$old_u_price=doubleval(is_null($row["wel_u_price"]) ? 0 : $row["wel_u_price"]);
			$old_tax_rate=doubleval(is_null($row["wel_tax_rate"]) ? 0 : $row["wel_tax_rate"]);
			$old_req_qty=doubleval(is_null($row["wel_req_qty"]) ? 0 : $row["wel_req_qty"]);
			$old_os_qty=doubleval(is_null($row["wel_os_qty"]) ? 0 : $row["wel_os_qty"]);
			$old_do_qty=doubleval(is_null($row["wel_do_qty"]) ? 0 : $row["wel_do_qty"]);
			$old_dn_qty=doubleval(is_null($row["wel_dn_qty"]) ? 0 : $row["wel_dn_qty"]);
			$old_req_date=$row["wel_req_date"];
			$old_cus_date=$row["wel_cus_date"];
			//检查订单明细信息
			
			if(($this->wel_req_qty-($old_req_qty-$old_os_qty))<0){throw new Exception("wel_so_qty_less_inv_qty");}
			if(($this->wel_req_qty-$old_do_qty)<0){throw new Exception("wel_so_qty_less_do_qty");}
			if(($this->wel_req_qty-$old_dn_qty)<0){throw new Exception("wel_so_qty_less_dn_qty");}
			
			//检查发票表的数据
			if($this->wel_u_price!=$old_u_price)
			{
				if( ($old_req_qty-$old_os_qty)>0 )
				{
					throw new Exception("wel_u_price_cannot_change");
				}
			}

			$dec_wel_item_amt = Round($this->wel_req_qty*$this->wel_u_price,2);
			$dec_wel_dis_amt = 0;
			$dec_wel_dis_amt2 = 0;
			$dec_wel_tax_amt = 0;
			$dec_wel_line_amt = 0;
			if($this->wel_discount>0)
			{
				$dec_wel_dis_amt = Round($dec_wel_item_amt * $this->wel_discount / 100,2);
			}
			if ( ($int_dis_type_h==1 or $int_dis_type_h==3) and $this->wel_discount==0)
			{
				$dec_wel_dis_amt2 = Round(($dec_wel_item_amt-$dec_wel_dis_amt) * $int_dis_rate_h / 100,2);
			}
			if ( $int_dis_type_h==4 or $int_dis_type_h==6 )
			{
				$dec_wel_dis_amt2 = Round(($dec_wel_item_amt-$dec_wel_dis_amt) * $int_dis_rate_h / 100,2);
			}

			$dec_wel_line_amt = $dec_wel_item_amt-$dec_wel_dis_amt-$dec_wel_dis_amt2;
			// 價外稅
			if ( $int_tax_type_h==2 )
			{
				$dec_wel_tax_amt = Round($dec_wel_line_amt * $this->wel_tax_rate / 100,2);
				$dec_wel_line_amt = $dec_wel_line_amt + $dec_wel_tax_amt;
			}
			// 價內稅
			if ( $int_tax_type_h==1 )
			{
				$dec_wel_tax_amt = Round($dec_wel_line_amt * $this->wel_tax_rate /(100 + $this->wel_tax_rate),2);
			}

			try
			{
				mysql_query("begin");
				
				$dec_so_os_qty=$this->wel_req_qty-($old_req_qty-$old_os_qty);

				$sql="UPDATE #__wel_sordetm SET ".
						"wel_u_price='$this->wel_u_price',".
						"wel_req_qty='$this->wel_req_qty',".
						"wel_os_qty='$dec_so_os_qty', ".
						"wel_req_date=$this->wel_req_date,".
						"wel_area_code='$this->wel_area_code',".
						"wel_discount='$this->wel_discount',".
						"wel_tax_rate='$this->wel_tax_rate', ".
						"wel_cus_date=$this->wel_cus_date,".
						"wel_pmc_date=$this->wel_pmc_date,".
						"wel_so_rmk='$this->wel_so_rmk',".
						"wel_item_amt='$dec_wel_item_amt',".
						"wel_dis_amt='$dec_wel_dis_amt',".
						"wel_dis_amt2='$dec_wel_dis_amt2',".
						"wel_tax_amt='$dec_wel_tax_amt',".
						"wel_line_amt='$dec_wel_line_amt',".
						"wel_upd_user='{$_SESSION['wel_user_']}',".
						"wel_upd_date=now() ".
					"WHERE wel_so_no='$this->wel_so_no' AND wel_so_line='$this->wel_so_line' LIMIT 1";			
				$sql=revert_to_the_available_sql($sql);
				if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
			
					// Update SORHDRM discount & Tax
					$tmp_item_amt=0;
					$tmp_dis_amt=0;
					$tmp_dis_amt2=0;
					$tmp_tax_amt=0;
					$tmp_line_amt=0;
					$sql="SELECT ".
						"SUM(wel_item_amt) as s1,".
						"SUM(wel_dis_amt) as s2,".
						"sum(wel_dis_amt2) as s3,".
						"SUM(wel_tax_amt) as s4,".
						"SUM(wel_line_amt) as s5 ".
						"FROM #__wel_sordetm ".
						"WHERE wel_so_no='$this->wel_so_no' ";
					$sql=revert_to_the_available_sql($sql);
					if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
					if( $row=mysql_fetch_array($result) )
					{
						$tmp_item_amt=doubleval(is_null($row["s1"]) ? 0 : $row["s1"]);
						$tmp_dis_amt =doubleval(is_null($row["s2"]) ? 0 : $row["s2"]);
						$tmp_dis_amt2=doubleval(is_null($row["s3"]) ? 0 : $row["s3"]);
						$tmp_tax_amt =doubleval(is_null($row["s4"]) ? 0 : $row["s4"]);
						$tmp_line_amt=doubleval(is_null($row["s5"]) ? 0 : $row["s5"]);
					}

					$sql="SELECT ".
						"SUM(wel_item_amt) as s1,".
						"SUM(wel_dis_amt) as s2,".
						"sum(wel_dis_amt2) as s3,".
						"SUM(wel_tax_amt) as s4,".
						"SUM(wel_line_amt) as s5 ".
						"FROM #__wel_sormism ".
						"WHERE wel_so_no='$this->wel_so_no' ";
					$sql=revert_to_the_available_sql($sql);
					if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
					if( $row=mysql_fetch_array($result) )
					{
						$tmp_item_amt=$tmp_item_amt + doubleval(is_null($row["s1"]) ? 0 : $row["s1"]);
						$tmp_dis_amt =$tmp_dis_amt  + doubleval(is_null($row["s2"]) ? 0 : $row["s2"]);
						$tmp_dis_amt2=$tmp_dis_amt2 + doubleval(is_null($row["s3"]) ? 0 : $row["s3"]);
						$tmp_tax_amt =$tmp_tax_amt  + doubleval(is_null($row["s4"]) ? 0 : $row["s4"]);
						$tmp_line_amt=$tmp_line_amt + doubleval(is_null($row["s5"]) ? 0 : $row["s5"]);
					}

					$sql="UPDATE #__wel_sorhdrm SET ".
						"wel_item_amt='$tmp_item_amt',".
						"wel_dis_amt='$tmp_dis_amt',".
						"wel_dis_amt2='$tmp_dis_amt2',".
						"wel_tax_amt='$tmp_tax_amt',".
						"wel_order_amt='$tmp_line_amt', ".
						"wel_upd_user='{$_SESSION['wel_user_id']}',".
						"wel_upd_date=now() ".
						"WHERE wel_so_no='$this->wel_so_no'";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					
				mysql_query("commit");
			}
			catch (Exception $e1)
			{
				rollbacktrans($database);
				throw new Exception($e1->getMessage());
			}
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		if($msg_code==""){$msg_code="edit_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
}
?>