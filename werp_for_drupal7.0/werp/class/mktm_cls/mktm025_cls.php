<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT

?>
<?php
class mktm025_cls
{
	public $wel_area_code="";
	public $wel_area_des="";

	private $wel_prog_code="mktm025";

	public function read()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_read")){throw new Exception("unauthorized_access");}
		
			$sql="SELECT * FROM #__wel_areaflm WHERE wel_area_code='$this->wel_area_code' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_area_code_not_found");}
			$int__count=0;
			while ($int__count < mysql_num_fields($result)) 
			{
				$field_name=mysql_fetch_field($result,$int__count)->name;
				$return_val[$field_name]=$row[$field_name];
				$int__count++;
			}
			mysql_free_result($result);
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	public function addnew()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();	
					
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_read")){throw new Exception("unauthorized_access");}
		
			if ($this->wel_area_code==""){throw new Exception("wel_area_code_miss");}	
			if ($this->wel_area_des==""){throw new Exception("wel_area_des_miss");}	
			
			$sql="SELECT * FROM #__wel_areaflm WHERE wel_area_code='$this->wel_area_code' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
			if($row=mysql_fetch_array($result)){throw new Exception("wel_area_code_exist");}

			try
			{
				mysql_query("begin");
				
					$sql="INSERT INTO #__wel_areaflm(wel_area_code,wel_area_des) ".
						"VALUES('$this->wel_area_code','$this->wel_area_des')";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					
				mysql_query("commit");
					
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}
				
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		if($msg_code==""){$msg_code="addnew_succee";}
		$return_val["msg_code"]=$msg_code;
		$return_val["wel_area_code"]=$this->wel_area_code;
		return $return_val;
	}
	
	public function edit()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{			
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_edit")){throw new Exception("unauthorized_access");}
		
			if ($this->wel_area_code==""){throw new Exception("wel_area_code_miss");}	
			if ($this->wel_area_des==""){throw new Exception("wel_area_des_miss");}
			
			$sql="SELECT * FROM #__wel_areaflm WHERE wel_area_code='$this->wel_area_code' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_area_code_not_found");}
			
			try
			{
				mysql_query("begin");
				
					$sql="UPDATE #__wel_areaflm SET ".
							"wel_area_des='$this->wel_area_des' ".
						"WHERE wel_area_code='$this->wel_area_code' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					
				mysql_query("commit");
					
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}
			
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}		
		if($msg_code==""){$msg_code="edit_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	public function delete()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{			
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_delete")){throw new Exception("unauthorized_access");}
		
			if ($this->wel_area_code==""){throw new Exception("wel_area_code_miss");}	
			
			$sql="SELECT * FROM #__wel_areaflm WHERE wel_area_code='$this->wel_area_code' limit 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_area_code_not_found");}
			
			try
			{
				mysql_query("begin");
				
					$sql="DELETE FROM #__wel_areaflm WHERE wel_area_code='$this->wel_area_code' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					
				mysql_query("commit");
					
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}
			
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}		
		if($msg_code==""){$msg_code="delete_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
}
?>
