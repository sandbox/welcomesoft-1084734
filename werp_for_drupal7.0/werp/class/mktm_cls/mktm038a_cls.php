<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>

<?php
class mktm038a_cls
{
	public $wel_inv_no="";
	public $wel_inv_line="";
	public $wel_so_no="";
	public $wel_so_line=0;
	public $wel_part_no="";
	public $wel_cus_ord="";
	public $wel_cpart_des1="";
	public $wel_cpart_des2="";
	public $wel_cpart_des3="";
	public $wel_cpart_des4="";
	public $wel_req_qty=0;
	public $wel_u_price=0;
	public $wel_discount=0;
	public $wel_tax_rate=0;
	public $wel_inv_remark="";

	private $wel_prog_code="mktm038";

	public function read()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_read")){throw new Exception("unauthorized_access");}
			
			$sql="SELECT r.*,h.wel_cus_code,h.wel_cur_code,h.wel_comp_code,".
				" p.wel_part_des as p_des,p.wel_part_des1 as p_des1,p.wel_part_des2 as p_des2".
				" FROM #__wel_invdetm r ".
					"LEFT JOIN #__wel_invhdrm h ON r.wel_inv_no=h.wel_inv_no ".
					"LEFT JOIN #__wel_partflm p ON r.wel_part_no=p.wel_part_no ".
				 "WHERE r.wel_inv_no='$this->wel_inv_no' AND r.wel_inv_line='$this->wel_inv_line' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_inv_no_not_found");}
			$int__count=0;
			while ($int__count < mysql_num_fields($result)) 
			{
				$field_name=mysql_fetch_field($result,$int__count)->name;
				$return_val[$field_name]=$row[$field_name];
				$int__count++;
			}
			mysql_free_result($result);
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	public function addnew()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_addnew")){throw new Exception("unauthorized_access");}

			if($this->wel_part_no==""){throw new Exception("wel_part_no_miss");}

			if (!is_numeric($this->wel_req_qty)){$this->wel_req_qty=0;}
			$this->wel_req_qty=doubleval($this->wel_req_qty);
			if($this->wel_req_qty==0){throw new Exception("wel_req_qty_miss");}
			if($this->wel_req_qty<0){throw new Exception("wel_req_qty_error");}

			if (!is_numeric($this->wel_u_price)){$this->wel_u_price=0;}	
			$this->wel_u_price=doubleval($this->wel_u_price);
//			Accept zero price item
//			if($this->wel_u_price==0){throw new Exception("wel_u_price_miss");}
			if($this->wel_u_price<0){throw new Exception("wel_u_price_error");}

			if (!is_numeric($this->wel_tax_rate)){$this->wel_tax_rate=0;}
			$this->wel_tax_rate=doubleval($this->wel_tax_rate);
			if($this->wel_tax_rate<0 || $this->wel_tax_rate>100){throw new Exception("wel_tax_rate_error");}
			if (!is_numeric($this->wel_discount)){$this->wel_discount=0;}
			$this->wel_discount=doubleval($this->wel_discount);
			if($this->wel_discount<0 || $this->wel_discount>100){throw new Exception("wel_discount_error");}

			if($this->wel_part_no!="")
			{
				$sql="SELECT wel_part_no from #__wel_partflm WHERE wel_part_no='".$this->wel_part_no."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_part_no_not_found");}
			}

			// Get Info from INVHDRM
			$sql="SELECT * FROM #__wel_invhdrm WHERE IFNULL(wel_is_inv,0)=1 AND wel_inv_no='$this->wel_inv_no' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_inv_no_not_found");}
			if($wel_posted==1){throw new Exception("wel_inv_no_posted");}
			$str_cus_code=is_null($row["wel_cus_code"]) ? "" : $row["wel_cus_code"];
			$str_cur_code=is_null($row["wel_cur_code"]) ? "" : $row["wel_cur_code"];
			$str_comp_code=is_null($row["wel_comp_code"]) ? "" : $row["wel_comp_code"];
			$int_line=intval(is_null($row["wel_last_line"]) ? 0 : $row["wel_last_line"])+1;
			$int_dis_type_h=intval(is_null($row["wel_dis_type"]) ? 0 : $row["wel_dis_type"]);
			$int_dis_rate_h=doubleval(is_null($row["wel_discount"]) ? 0 : $row["wel_discount"]);
			$int_tax_type_h=intval(is_null($row["wel_tax_type"]) ? 0 : $row["wel_tax_type"]);

			if (!is_numeric($this->wel_so_line)){$this->wel_so_line=0;}
			$this->wel_so_line=intval($this->wel_so_line);
			if($this->wel_so_no=="")
			{
				$this->wel_so_line=0;
			}
			else {
				if($this->wel_so_line==0){throw new Exception("wel_so_line_miss");}

				// Get Info from S/O
				$sql="SELECT h.wel_cus_code,h.wel_cur_code,h.wel_comp_code,d.*".
					" FROM #__wel_sordetm d LEFT JOIN #__wel_sorhdrm h ON d.wel_so_no=h.wel_so_no".
					" WHERE d.wel_so_no='$this->wel_so_no' AND d.wel_so_line='$this->wel_so_line' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_so_line_not_found");}
				$so_cus_code=is_null($row["wel_cus_code"]) ? "" : $row["wel_cus_code"];
				$so_cur_code=is_null($row["wel_cur_code"]) ? "" : $row["wel_cur_code"];
				$so_comp_code=is_null($row["wel_comp_code"]) ? "" : $row["wel_comp_code"];
				$so_part_no=is_null($row["wel_part_no"]) ? "" : $row["wel_part_no"];
				$so_os_qty=doubleval(is_null($row["wel_os_qty"]) ? 0 : $row["wel_os_qty"]);

				if(strtolower($so_cus_code)!=strtolower($str_cus_code)){throw new Exception("wel_cus_code_not_match_so");}
				if(strtolower($so_cur_code)!=strtolower($str_cur_code)){throw new Exception("wel_cur_code_not_match_so");}
				if(strtolower($so_comp_code)!=strtolower($str_comp_code)){throw new Exception("wel_comp_code_not_match_so");}
				if(strtolower($so_part_no)!=strtolower($this->wel_part_no)){throw new Exception("wel_part_no_not_match_so");}

				if($this->wel_req_qty>$so_os_qty){throw new Exception("wel_req_qty_excess_so");}
			}
		
			$dec_wel_item_amt = Round($this->wel_req_qty*$this->wel_u_price,2);
			$dec_wel_dis_amt = 0;
			$dec_wel_dis_amt2 = 0;
			$dec_wel_tax_amt = 0;
			$dec_wel_line_amt = 0;
			if($this->wel_discount>0)
			{
				$dec_wel_dis_amt = Round($dec_wel_item_amt * $this->wel_discount / 100,2);
			}
			if ( ($int_dis_type_h==1 or $int_dis_type_h==3) and $this->wel_discount==0)
			{
				$dec_wel_dis_amt2 = Round(($dec_wel_item_amt-$dec_wel_dis_amt) * $int_dis_rate_h / 100,2);
			}
			if ( $int_dis_type_h==4 or $int_dis_type_h==6 )
			{
				$dec_wel_dis_amt2 = Round(($dec_wel_item_amt-$dec_wel_dis_amt) * $int_dis_rate_h / 100,2);
			}

			$dec_wel_line_amt = $dec_wel_item_amt-$dec_wel_dis_amt-$dec_wel_dis_amt2;
			// 價外稅
			if ( $int_tax_type_h==2 )
			{
				$dec_wel_tax_amt = Round($dec_wel_line_amt * $this->wel_tax_rate / 100,2);
				$dec_wel_line_amt = $dec_wel_line_amt + $dec_wel_tax_amt;
			}
			// 價內稅
			if ( $int_tax_type_h==1 )
			{
				$dec_wel_tax_amt = Round($dec_wel_line_amt * $this->wel_tax_rate /(100 + $this->wel_tax_rate),2);
			}

			// Check Invoice Line exist
			$sql="SELECT wel_inv_no,wel_inv_line FROM #__wel_invdetm ".
				"WHERE wel_inv_no='$this->wel_inv_no' AND ".
				"(wel_inv_line='$int_line') LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
			if($row=mysql_fetch_array($result))
			{
				$sql="SELECT max(wel_inv_line) as max_line FROM #__wel_invdetm ".
					"WHERE wel_inv_no='$this->wel_inv_no' ";
				$sql=revert_to_the_available_sql($sql);
				if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_inv_no_not_found");}
				$int_line=intval(is_null($row["max_line"]) ? 0 : $row["max_line"])+1;
			}

			try
			{
				mysql_query("begin");

					$sql="INSERT INTO #__wel_invdetm SET ".
							"wel_inv_no='$this->wel_inv_no',".
							"wel_inv_line='$int_line',".
							"wel_cus_code='$str_cus_code',".
							"wel_so_no='$this->wel_so_no',".
							"wel_so_line='$this->wel_so_line',".
							"wel_part_no='$this->wel_part_no',".
							"wel_cus_ord='$this->wel_cus_ord',".
							"wel_part_des1='$this->wel_cpart_des1',".
							"wel_part_des2='$this->wel_cpart_des2',".
							"wel_part_des3='$this->wel_cpart_des3',".
							"wel_part_des4='$this->wel_cpart_des4',".
							"wel_req_qty='$this->wel_req_qty',".
							"wel_u_price='$this->wel_u_price',".
							"wel_discount='$this->wel_discount',".
							"wel_tax_rate='$this->wel_tax_rate',".
							"wel_item_amt='$dec_wel_item_amt',".
							"wel_dis_amt='$dec_wel_dis_amt',".
							"wel_dis_amt2='$dec_wel_dis_amt2',".
							"wel_tax_amt='$dec_wel_tax_amt',".
							"wel_line_amt='$dec_wel_line_amt',".
							"wel_inv_remark='$this->wel_inv_remark',".
							"wel_is_inv='1',".
							"wel_crt_user='{$_SESSION['wel_user_id']}', ".
							"wel_crt_date=now()";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					
				if($this->wel_so_no!="")
				{
					$sql="UPDATE #__wel_sordetm SET ".
							"wel_os_qty=IFNULL(wel_os_qty,0)-$this->wel_req_qty ".
							"WHERE wel_so_no='$this->wel_so_no' AND wel_so_line='$this->wel_so_line' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				}
					
					// Update INVHDRM discount & Tax
					$tmp_item_amt=0;
					$tmp_dis_amt=0;
					$tmp_dis_amt2=0;
					$tmp_tax_amt=0;
					$tmp_line_amt=0;
					$sql="SELECT ".
						"SUM(wel_item_amt) as s1,".
						"SUM(wel_dis_amt) as s2,".
						"sum(wel_dis_amt2) as s3,".
						"SUM(wel_tax_amt) as s4,".
						"SUM(wel_line_amt) as s5 ".
						"FROM #__wel_invdetm ".
						"WHERE wel_inv_no='$this->wel_inv_no' ";
					$sql=revert_to_the_available_sql($sql);
					if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
					if( $row=mysql_fetch_array($result) )
					{
						$tmp_item_amt=doubleval(is_null($row["s1"]) ? 0 : $row["s1"]);
						$tmp_dis_amt =doubleval(is_null($row["s2"]) ? 0 : $row["s2"]);
						$tmp_dis_amt2=doubleval(is_null($row["s3"]) ? 0 : $row["s3"]);
						$tmp_tax_amt =doubleval(is_null($row["s4"]) ? 0 : $row["s4"]);
						$tmp_line_amt=doubleval(is_null($row["s5"]) ? 0 : $row["s5"]);
					}

					$sql="SELECT ".
						"SUM(wel_itemamt) as s1,".
						"SUM(wel_dis_amt) as s2,".
						"sum(wel_dis_amt2) as s3,".
						"SUM(wel_tax_amt) as s4,".
						"SUM(wel_line_amt) as s5 ".
						"FROM #__wel_invmism ".
						"WHERE wel_inv_no='$this->wel_inv_no' ";
					$sql=revert_to_the_available_sql($sql);
					if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
					if( $row=mysql_fetch_array($result) )
					{
						$tmp_item_amt=$tmp_item_amt + doubleval(is_null($row["s1"]) ? 0 : $row["s1"]);
						$tmp_dis_amt =$tmp_dis_amt  + doubleval(is_null($row["s2"]) ? 0 : $row["s2"]);
						$tmp_dis_amt2=$tmp_dis_amt2 + doubleval(is_null($row["s3"]) ? 0 : $row["s3"]);
						$tmp_tax_amt =$tmp_tax_amt  + doubleval(is_null($row["s4"]) ? 0 : $row["s4"]);
						$tmp_line_amt=$tmp_line_amt + doubleval(is_null($row["s5"]) ? 0 : $row["s5"]);
					}

					$sql="UPDATE #__wel_invhdrm SET ".
						"wel_item_amt='$tmp_item_amt',".
						"wel_dis_amt='$tmp_dis_amt',".
						"wel_dis_amt2='$tmp_dis_amt2',".
						"wel_tax_amt='$tmp_tax_amt',".
						"wel_order_amt='$tmp_line_amt', ".
						"wel_last_line='$int_line',".
						"wel_upd_user='{$_SESSION['wel_user_id']}',".
						"wel_upd_date=now() ".
						"WHERE wel_inv_no='$this->wel_inv_no'";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}

				mysql_query("commit");
					
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}
				
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		if($msg_code==""){$msg_code="addnew_succee";}
		$return_val["msg_code"]=$msg_code;
		$return_val["wel_inv_no"]=$this->wel_inv_no;
		$return_val["wel_inv_line"]=$int__inv_line;
		return $return_val;
	}
	
	public function edit()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_edit")){throw new Exception("unauthorized_access");}
			
			if (!is_numeric($this->wel_inv_line)){$this->wel_inv_line=0;}
			$this->wel_inv_line=intval($this->wel_inv_line);
			if ($this->wel_inv_line==0){throw new Exception("wel_inv_line_miss");}

			if (!is_numeric($this->wel_req_qty)){$this->wel_req_qty=0;}
			$this->wel_req_qty=doubleval($this->wel_req_qty);
			if($this->wel_req_qty==0){throw new Exception("wel_req_qty_miss");}
			if($this->wel_req_qty<0){throw new Exception("wel_req_qty_error");}

			if (!is_numeric($this->wel_u_price)){$this->wel_u_price=0;}	
			$this->wel_u_price=doubleval($this->wel_u_price);
//			Accept zero price item
//			if($this->wel_u_price==0){throw new Exception("wel_u_price_miss");}
			if($this->wel_u_price<0){throw new Exception("wel_u_price_error");}

			if (!is_numeric($this->wel_tax_rate)){$this->wel_tax_rate=0;}
			$this->wel_tax_rate=doubleval($this->wel_tax_rate);
			if($this->wel_tax_rate<0 || $this->wel_tax_rate>100){throw new Exception("wel_tax_rate_error");}
			if (!is_numeric($this->wel_discount)){$this->wel_discount=0;}
			$this->wel_discount=doubleval($this->wel_discount);
			if($this->wel_discount<0 || $this->wel_discount>100){throw new Exception("wel_discount_error");}

			//销售发票检测 start
			$sql="SELECT * FROM #__wel_invhdrm WHERE IFNULL(wel_is_inv,0)=1 AND wel_inv_no='$this->wel_inv_no' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_inv_no_not_found");}
			if($wel_posted==1){throw new Exception("wel_inv_no_posted");}
			$int_dis_type_h=intval(is_null($row["wel_dis_type"]) ? 0 : $row["wel_dis_type"]);
			$int_dis_rate_h=doubleval(is_null($row["wel_discount"]) ? 0 : $row["wel_discount"]);
			$int_tax_type_h=intval(is_null($row["wel_tax_type"]) ? 0 : $row["wel_tax_type"]);
			
			$sql="SELECT * FROM #__wel_invdetm ".
				"WHERE IFNULL(wel_is_inv,0)=1 AND wel_inv_no='$this->wel_inv_no' AND wel_inv_line=$this->wel_inv_line LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_inv_line_not_found");}
			$old_req_qty=doubleval(is_null($row["wel_req_qty"]) ? 0 : $row["wel_req_qty"]);
			$old_so_no=is_null($row["wel_so_no"]) ? "" : $row["wel_so_no"];
			$old_so_line=intval(is_null($row["wel_so_line"]) ? 0 : $row["wel_so_line"]);

			if($old_so_no!="")
			{					
				if($old_so_line==0){throw new Exception("wel_so_line_miss");}

				// Get Info from S/O
				$sql="SELECT * FROM #__wel_sordetm".
					" WHERE wel_so_no='$old_so_no' AND wel_so_line='$old_so_line' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_so_line_not_found");}
				$so_part_no=is_null($row["wel_part_no"]) ? "" : $row["wel_part_no"];
				$so_req_qty=doubleval(is_null($row["wel_req_qty"]) ? 0 : $row["wel_req_qty"]);
				$so_os_qty=doubleval(is_null($row["wel_os_qty"]) ? 0 : $row["wel_os_qty"]);

				if(strtolower($so_part_no)!=strtolower($this->wel_part_no)){throw new Exception("wel_part_no_not_match_so");}

				if($this->wel_req_qty>($so_os_qty+$old_req_qty)){throw new Exception("wel_req_qty_excess_so");}
			}
			
			$dec_wel_item_amt = Round($this->wel_req_qty*$this->wel_u_price,2);
			$dec_wel_dis_amt = 0;
			$dec_wel_dis_amt2 = 0;
			$dec_wel_tax_amt = 0;
			$dec_wel_line_amt = 0;
			if($this->wel_discount>0)
			{
				$dec_wel_dis_amt = Round($dec_wel_item_amt * $this->wel_discount / 100,2);
			}
			if ( ($int_dis_type_h==1 or $int_dis_type_h==3) and $this->wel_discount==0)
			{
				$dec_wel_dis_amt2 = Round(($dec_wel_item_amt-$dec_wel_dis_amt) * $int_dis_rate_h / 100,2);
			}
			if ( $int_dis_type_h==4 or $int_dis_type_h==6 )
			{
				$dec_wel_dis_amt2 = Round(($dec_wel_item_amt-$dec_wel_dis_amt) * $int_dis_rate_h / 100,2);
			}

			$dec_wel_line_amt = $dec_wel_item_amt-$dec_wel_dis_amt-$dec_wel_dis_amt2;
			// 價外稅
			if ( $int_tax_type_h==2 )
			{
				$dec_wel_tax_amt = Round($dec_wel_line_amt * $this->wel_tax_rate / 100,2);
				$dec_wel_line_amt = $dec_wel_line_amt + $dec_wel_tax_amt;
			}
			// 價內稅
			if ( $int_tax_type_h==1 )
			{
				$dec_wel_tax_amt = Round($dec_wel_line_amt * $this->wel_tax_rate /(100 + $this->wel_tax_rate),2);
			}

			try
			{
				mysql_query("begin");
				
					$sql="UPDATE #__wel_invdetm SET ".
							"wel_cus_ord='$this->wel_cus_ord',".
							"wel_part_des1='$this->wel_part_des1',".
							"wel_part_des2='$this->wel_part_des2',".
							"wel_part_des3='$this->wel_part_des3',".
							"wel_part_des4='$this->wel_part_des4',".
							"wel_req_qty='$this->wel_req_qty',".
							"wel_u_price='$this->wel_u_price',".
							"wel_discount='$this->wel_discount',".
							"wel_tax_rate='$this->wel_tax_rate',".
							"wel_item_amt='$dec_wel_item_amt',".
							"wel_dis_amt='$dec_wel_dis_amt',".
							"wel_dis_amt2='$dec_wel_dis_amt2',".
							"wel_tax_amt='$dec_wel_tax_amt',".
							"wel_line_amt='$dec_wel_line_amt',".
							"wel_inv_remark='$this->wel_inv_remark',".
							"wel_upd_user='{$_SESSION['wel_user_']}',".
							"wel_upd_date=now() ".
						"WHERE wel_inv_no='$this->wel_inv_no' AND wel_inv_line='$this->wel_inv_line' LIMIT 1";			
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					
				if($old_so_no!="")
				{
					$sql="UPDATE #__wel_sordetm SET ".
							"wel_os_qty=IFNULL(wel_os_qty,0)+$old_req_qty-$this->wel_req_qty ".
							"WHERE wel_so_no='$this->wel_so_no' AND wel_so_line='$this->wel_so_line' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				}

					// Update INVHDRM discount & Tax
					$tmp_item_amt=0;
					$tmp_dis_amt=0;
					$tmp_dis_amt2=0;
					$tmp_tax_amt=0;
					$tmp_line_amt=0;
					$sql="SELECT ".
						"SUM(wel_item_amt) as s1,".
						"SUM(wel_dis_amt) as s2,".
						"sum(wel_dis_amt2) as s3,".
						"SUM(wel_tax_amt) as s4,".
						"SUM(wel_line_amt) as s5 ".
						"FROM #__wel_invdetm ".
						"WHERE wel_inv_no='$this->wel_inv_no' ";
					$sql=revert_to_the_available_sql($sql);
					if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
					if( $row=mysql_fetch_array($result) )
					{
						$tmp_item_amt=doubleval(is_null($row["s1"]) ? 0 : $row["s1"]);
						$tmp_dis_amt =doubleval(is_null($row["s2"]) ? 0 : $row["s2"]);
						$tmp_dis_amt2=doubleval(is_null($row["s3"]) ? 0 : $row["s3"]);
						$tmp_tax_amt =doubleval(is_null($row["s4"]) ? 0 : $row["s4"]);
						$tmp_line_amt=doubleval(is_null($row["s5"]) ? 0 : $row["s5"]);
					}

					$sql="SELECT ".
						"SUM(wel_itemamt) as s1,".
						"SUM(wel_dis_amt) as s2,".
						"sum(wel_dis_amt2) as s3,".
						"SUM(wel_tax_amt) as s4,".
						"SUM(wel_line_amt) as s5 ".
						"FROM #__wel_invmism ".
						"WHERE wel_inv_no='$this->wel_inv_no' ";
					$sql=revert_to_the_available_sql($sql);
					if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
					if( $row=mysql_fetch_array($result) )
					{
						$tmp_item_amt=$tmp_item_amt + doubleval(is_null($row["s1"]) ? 0 : $row["s1"]);
						$tmp_dis_amt =$tmp_dis_amt  + doubleval(is_null($row["s2"]) ? 0 : $row["s2"]);
						$tmp_dis_amt2=$tmp_dis_amt2 + doubleval(is_null($row["s3"]) ? 0 : $row["s3"]);
						$tmp_tax_amt =$tmp_tax_amt  + doubleval(is_null($row["s4"]) ? 0 : $row["s4"]);
						$tmp_line_amt=$tmp_line_amt + doubleval(is_null($row["s5"]) ? 0 : $row["s5"]);
					}

					$sql="UPDATE #__wel_invhdrm SET ".
						"wel_item_amt='$tmp_item_amt',".
						"wel_dis_amt='$tmp_dis_amt',".
						"wel_dis_amt2='$tmp_dis_amt2',".
						"wel_tax_amt='$tmp_tax_amt',".
						"wel_order_amt='$tmp_line_amt', ".
						"wel_upd_user='{$_SESSION['wel_user_id']}',".
						"wel_upd_date=now() ".
						"WHERE wel_inv_no='$this->wel_inv_no'";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					
				mysql_query("commit");
					
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}
					
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}		
		if($msg_code==""){$msg_code="edit_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
}
?>