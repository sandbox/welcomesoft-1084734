<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>

<?php
class mktm038b_cls
{
	public $wel_inv_no="";
	public $wel_line_no=0;
	public $wel_item="";
	public $wel_itemamt=0;
	public $wel_discount=0;
	public $wel_tax_rate=0;

	private $wel_prog_code="mktm038";

	public function read()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_read")){throw new Exception("unauthorized_access");}
			
			$sql="SELECT * FROM #__wel_invmism WHERE wel_inv_no='$this->wel_inv_no' AND wel_line_no ='$this->wel_line_no' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_inv_no_not_found");}
			$int__count=0;
			while ($int__count < mysql_num_fields($result)) 
			{
				$field_name=mysql_fetch_field($result,$int__count)->name;
				$return_val[$field_name]=$row[$field_name];
				$int__count++;
			}
			mysql_free_result($result);
		}
		catch (Exception $e)
		{				
			$msg_code=$e->getMessage();
		}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	public function addnew()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_addnew")){throw new Exception("unauthorized_access");}
			
//			if ($this->wel_inv_no==""){throw new Exception("wel_inv_no_miss");}
			if ($this->wel_item==""){throw new Exception("wel_item_miss");}
			
			if (!is_numeric($this->wel_line_no)){$this->wel_line_no=0;}
			if (!is_numeric($this->wel_itemamt)){$this->wel_itemamt=0;}
			$this->wel_itemamt=doubleval($this->wel_itemamt);
//			if ($this->wel_itemamt==0){throw new Exception("wel_item_amt_miss");}
			
			if (!is_numeric($this->wel_tax_rate)){$this->wel_tax_rate=0;}
			$this->wel_tax_rate=doubleval($this->wel_tax_rate);
			if($this->wel_tax_rate <0 || $this->wel_tax_rate>100){throw new Exception("wel_tax_rate_error");}
			if (!is_numeric($this->wel_discount)){$this->wel_discount=0;}
			$this->wel_discount=doubleval($this->wel_discount);
			if($this->wel_discount<0 || $this->wel_discount>100){throw new Exception("wel_discount_error");}
			
			// Get Info from INVHDRM
			$sql="SELECT * FROM #__wel_invhdrm WHERE IFNULL(wel_is_inv,0)=1 AND wel_inv_no='$this->wel_inv_no' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_inv_no_not_found");}
			if($wel_posted==1){throw new Exception("wel_inv_no_posted");}
			$int_dis_type_h=intval(is_null($row["wel_dis_type"]) ? 0 : $row["wel_dis_type"]);
			$int_dis_rate_h=doubleval(is_null($row["wel_discount"]) ? 0 : $row["wel_discount"]);
			$int_tax_type_h=intval(is_null($row["wel_tax_type"]) ? 0 : $row["wel_tax_type"]);

			$dec_wel_itemamt = $this->wel_itemamt;
			$dec_wel_dis_amt = 0;
			$dec_wel_dis_amt2 = 0;
			$dec_wel_tax_amt = 0;
			$dec_wel_line_amt = 0;
			if($this->wel_discount>0)
			{
				$dec_wel_dis_amt = Round($dec_wel_itemamt * $this->wel_discount / 100,2);
			}

			if ( ($int_dis_type_h==2 or $int_dis_type_h==3) and $this->wel_discount==0)
			{
				$dec_wel_dis_amt2 = Round(($dec_wel_itemamt-$dec_wel_dis_amt) * $int_dis_rate_h / 100,2);
			}
			if ( $int_dis_type_h==5 or $int_dis_type_h==6 )
			{
				$dec_wel_dis_amt2 = Round(($dec_wel_itemamt-$dec_wel_dis_amt) * $int_dis_rate_h / 100,2);
			}

			$dec_wel_line_amt = $dec_wel_itemamt-$dec_wel_dis_amt-$dec_wel_dis_amt2;
			// 價外稅
			if ( $int_tax_type_h==2 )
			{
				$dec_wel_tax_amt = Round($dec_wel_line_amt * $this->wel_tax_rate / 100,2);
				$dec_wel_line_amt = $dec_wel_line_amt + $dec_wel_tax_amt;
			}
			// 價內稅
			if ( $int_tax_type_h==1 )
			{
				$dec_wel_tax_amt = Round($dec_wel_line_amt * $this->wel_tax_rate /(100 + $this->wel_tax_rate),2);
			}

			$sql="SELECT MAX(wel_line_no) AS wel_line_no FROM #__wel_invmism WHERE wel_inv_no='$this->wel_inv_no' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){$this->wel_line_no=1;}
			$this->wel_line_no=intval(is_null($row["wel_line_no"]) ? 0 : $row["wel_line_no"])+1;
			
			$sql="SELECT * FROM #__wel_invmism WHERE wel_inv_no='".$this->wel_inv_no."' AND wel_line_no=".$this->wel_line_no." LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
			if($row=mysql_fetch_array($result)){throw new Exception("wel_inv_line_no_exist");}

			try
			{
				mysql_query("begin");
				
					$sql="INSERT INTO #__wel_invmism SET ".
							"wel_inv_no='$this->wel_inv_no',".
							"wel_line_no='$this->wel_line_no',".
							"wel_item='$this->wel_item',".
							"wel_itemamt='$this->wel_itemamt',".
							"wel_discount='$this->wel_discount',".
							"wel_tax_rate='$this->wel_tax_rate',".
							"wel_dis_amt='$dec_wel_dis_amt',".
							"wel_dis_amt2='$dec_wel_dis_amt2',".
							"wel_tax_amt='$dec_wel_tax_amt',".
							"wel_line_amt='$dec_wel_line_amt',".
							"wel_crt_user='{$_SESSION['wel_user_id']}', ".
							"wel_crt_date=now()";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					
					// Update INVHDRM discount & Tax
					$tmp_item_amt=0;
					$tmp_dis_amt=0;
					$tmp_dis_amt2=0;
					$tmp_tax_amt=0;
					$tmp_line_amt=0;
					$sql="SELECT ".
						"SUM(wel_item_amt) as s1,".
						"SUM(wel_dis_amt) as s2,".
						"sum(wel_dis_amt2) as s3,".
						"SUM(wel_tax_amt) as s4,".
						"SUM(wel_line_amt) as s5 ".
						"FROM #__wel_invdetm ".
						"WHERE wel_inv_no='$this->wel_inv_no' ";
					$sql=revert_to_the_available_sql($sql);
					if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
					if( $row=mysql_fetch_array($result) )
					{
						$tmp_item_amt=doubleval(is_null($row["s1"]) ? 0 : $row["s1"]);
						$tmp_dis_amt =doubleval(is_null($row["s2"]) ? 0 : $row["s2"]);
						$tmp_dis_amt2=doubleval(is_null($row["s3"]) ? 0 : $row["s3"]);
						$tmp_tax_amt =doubleval(is_null($row["s4"]) ? 0 : $row["s4"]);
						$tmp_line_amt=doubleval(is_null($row["s5"]) ? 0 : $row["s5"]);
					}

					$sql="SELECT ".
						"SUM(wel_itemamt) as s1,".
						"SUM(wel_dis_amt) as s2,".
						"sum(wel_dis_amt2) as s3,".
						"SUM(wel_tax_amt) as s4,".
						"SUM(wel_line_amt) as s5 ".
						"FROM #__wel_invmism ".
						"WHERE wel_inv_no='$this->wel_inv_no' ";
					$sql=revert_to_the_available_sql($sql);
					if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
					if( $row=mysql_fetch_array($result) )
					{
						$tmp_item_amt=$tmp_item_amt + doubleval(is_null($row["s1"]) ? 0 : $row["s1"]);
						$tmp_dis_amt =$tmp_dis_amt  + doubleval(is_null($row["s2"]) ? 0 : $row["s2"]);
						$tmp_dis_amt2=$tmp_dis_amt2 + doubleval(is_null($row["s3"]) ? 0 : $row["s3"]);
						$tmp_tax_amt =$tmp_tax_amt  + doubleval(is_null($row["s4"]) ? 0 : $row["s4"]);
						$tmp_line_amt=$tmp_line_amt + doubleval(is_null($row["s5"]) ? 0 : $row["s5"]);
					}

					$sql="UPDATE #__wel_invhdrm SET ".
						"wel_item_amt='$tmp_item_amt',".
						"wel_dis_amt='$tmp_dis_amt',".
						"wel_dis_amt2='$tmp_dis_amt2',".
						"wel_tax_amt='$tmp_tax_amt',".
						"wel_order_amt='$tmp_line_amt', ".
						"wel_upd_user='{$_SESSION['wel_user_id']}',".
						"wel_upd_date=now() ".
						"WHERE wel_inv_no='$this->wel_inv_no'";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}

				mysql_query("commit");
					
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}
					
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		if($msg_code==""){$msg_code="addnew_succee";}
		$return_val["msg_code"]=$msg_code;
		$return_val["wel_inv_no"]=$this->wel_inv_no;
		$return_val["wel_line_no"]=$this->wel_line_no;
		return $return_val;
	}
	
	public function edit()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_edit")){throw new Exception("unauthorized_access");}
			
//			if ($this->wel_inv_no==""){throw new Exception("wel_inv_no_miss");}
			if ($this->wel_item==""){throw new Exception("wel_item_miss");}	

			if (!is_numeric($this->wel_line_no)){$this->wel_line_no=0;}
			if (!is_numeric($this->wel_itemamt)){$this->wel_itemamt=0;}
			$this->wel_itemamt=doubleval($this->wel_itemamt);
//			if ($this->wel_item_amt==0){throw new Exception("wel_item_amt_miss");}

			if (!is_numeric($this->wel_tax_rate)){$this->wel_tax_rate=0;}
			$this->wel_tax_rate=doubleval($this->wel_tax_rate);
			if($this->wel_tax_rate <0 || $this->wel_tax_rate>100){throw new Exception("wel_tax_rate_error");}
			if (!is_numeric($this->wel_discount)){$this->wel_discount=0;}
			$this->wel_discount=doubleval($this->wel_discount);
			if($this->wel_discount<0 || $this->wel_discount>100){throw new Exception("wel_discount_error");}
			
			// Get Info from INVHDRM
			$sql="SELECT * FROM #__wel_invhdrm WHERE IFNULL(wel_is_inv,0)=1 AND wel_inv_no='$this->wel_inv_no' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_inv_no_not_found");}
			if($wel_posted==1){throw new Exception("wel_inv_no_posted");}
			$int_dis_type_h=intval(is_null($row["wel_dis_type"]) ? 0 : $row["wel_dis_type"]);
			$int_dis_rate_h=doubleval(is_null($row["wel_discount"]) ? 0 : $row["wel_discount"]);
			$int_tax_type_h=intval(is_null($row["wel_tax_type"]) ? 0 : $row["wel_tax_type"]);

			$dec_wel_itemamt = $this->wel_itemamt;
			$dec_wel_dis_amt = 0;
			$dec_wel_dis_amt2 = 0;
			$dec_wel_tax_amt = 0;
			$dec_wel_line_amt = 0;
			if($this->wel_discount>0)
			{
				$dec_wel_dis_amt = Round($dec_wel_itemamt * $this->wel_discount / 100,2);
			}

			if ( ($int_dis_type_h==2 or $int_dis_type_h==3) and $this->wel_discount==0)
			{
				$dec_wel_dis_amt2 = Round(($dec_wel_itemamt-$dec_wel_dis_amt) * $int_dis_rate_h / 100,2);
			}
			if ( $int_dis_type_h==5 or $int_dis_type_h==6 )
			{
				$dec_wel_dis_amt2 = Round(($dec_wel_itemamt-$dec_wel_dis_amt) * $int_dis_rate_h / 100,2);
			}

			$dec_wel_line_amt = $dec_wel_itemamt-$dec_wel_dis_amt-$dec_wel_dis_amt2;
			// 價外稅
			if ( $int_tax_type_h==2 )
			{
				$dec_wel_tax_amt = Round($dec_wel_line_amt * $this->wel_tax_rate / 100,2);
				$dec_wel_line_amt = $dec_wel_line_amt + $dec_wel_tax_amt;
			}
			// 價內稅
			if ( $int_tax_type_h==1 )
			{
				$dec_wel_tax_amt = Round($dec_wel_line_amt * $this->wel_tax_rate /(100 + $this->wel_tax_rate),2);
			}

			$sql="SELECT * FROM #__wel_invmism WHERE wel_inv_no='$this->wel_inv_no' AND wel_line_no='$this->wel_line_no' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_inv_line_not_found");}
		
			try
			{
				mysql_query("begin");
				
					$sql="UPDATE #__wel_invmism SET ".
							"wel_item='$this->wel_item',".
							"wel_itemamt='$this->wel_itemamt',".
							"wel_discount='$this->wel_discount',".
							"wel_tax_rate='$this->wel_tax_rate',".
							"wel_dis_amt='$dec_wel_dis_amt',".
							"wel_dis_amt2='$dec_wel_dis_amt2',".
							"wel_tax_amt='$dec_wel_tax_amt',".
							"wel_line_amt='$dec_wel_line_amt',".
							"wel_upd_user='{$_SESSION['wel_user_']}',".
							"wel_upd_date=now() ".
						"WHERE wel_inv_no='$this->wel_inv_no' AND wel_line_no='$this->wel_line_no' LIMIT 1";			
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
			
					// Update INVHDRM discount & Tax
					$tmp_item_amt=0;
					$tmp_dis_amt=0;
					$tmp_dis_amt2=0;
					$tmp_tax_amt=0;
					$tmp_line_amt=0;
					$sql="SELECT ".
						"SUM(wel_item_amt) as s1,".
						"SUM(wel_dis_amt) as s2,".
						"sum(wel_dis_amt2) as s3,".
						"SUM(wel_tax_amt) as s4,".
						"SUM(wel_line_amt) as s5 ".
						"FROM #__wel_invdetm ".
						"WHERE wel_inv_no='$this->wel_inv_no' ";
					$sql=revert_to_the_available_sql($sql);
					if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
					if( $row=mysql_fetch_array($result) )
					{
						$tmp_item_amt=doubleval(is_null($row["s1"]) ? 0 : $row["s1"]);
						$tmp_dis_amt =doubleval(is_null($row["s2"]) ? 0 : $row["s2"]);
						$tmp_dis_amt2=doubleval(is_null($row["s3"]) ? 0 : $row["s3"]);
						$tmp_tax_amt =doubleval(is_null($row["s4"]) ? 0 : $row["s4"]);
						$tmp_line_amt=doubleval(is_null($row["s5"]) ? 0 : $row["s5"]);
					}

					$sql="SELECT ".
						"SUM(wel_itemamt) as s1,".
						"SUM(wel_dis_amt) as s2,".
						"sum(wel_dis_amt2) as s3,".
						"SUM(wel_tax_amt) as s4,".
						"SUM(wel_line_amt) as s5 ".
						"FROM #__wel_invmism ".
						"WHERE wel_inv_no='$this->wel_inv_no' ";
					$sql=revert_to_the_available_sql($sql);
					if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
					if( $row=mysql_fetch_array($result) )
					{
						$tmp_item_amt=$tmp_item_amt + doubleval(is_null($row["s1"]) ? 0 : $row["s1"]);
						$tmp_dis_amt =$tmp_dis_amt  + doubleval(is_null($row["s2"]) ? 0 : $row["s2"]);
						$tmp_dis_amt2=$tmp_dis_amt2 + doubleval(is_null($row["s3"]) ? 0 : $row["s3"]);
						$tmp_tax_amt =$tmp_tax_amt  + doubleval(is_null($row["s4"]) ? 0 : $row["s4"]);
						$tmp_line_amt=$tmp_line_amt + doubleval(is_null($row["s5"]) ? 0 : $row["s5"]);
					}

					$sql="UPDATE #__wel_invhdrm SET ".
						"wel_item_amt='$tmp_item_amt',".
						"wel_dis_amt='$tmp_dis_amt',".
						"wel_dis_amt2='$tmp_dis_amt2',".
						"wel_tax_amt='$tmp_tax_amt',".
						"wel_order_amt='$tmp_line_amt', ".
						"wel_upd_user='{$_SESSION['wel_user_id']}',".
						"wel_upd_date=now() ".
						"WHERE wel_inv_no='$this->wel_inv_no'";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				
				mysql_query("commit");
					
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}	
				
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}		
		if($msg_code==""){$msg_code="edit_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
}
?>