<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>

<?php
class mktm128a_cls
{
	public $wel_cus_code="";
	public $wel_part_no="";
	public $wel_cus_part="";
	public $wel_cus_part_des1="";
	public $wel_cus_part_des2="";
	public $wel_cus_part_des3="";
	public $wel_cus_part_des4="";
	public $wel_std_cur="";
	public $wel_std_price=0;
	public $wel_roll_qty=0;
	
	private $wel_prog_code="mktm128";

	public function read()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_read")){throw new Exception("unauthorized_access");}
			
			$sql="SELECT #__wel_cusmodm.*,#__wel_partflm.wel_part_des ".
				"FROM #__wel_cusmodm ".
					"LEFT JOIN #__wel_partflm ON #__wel_cusmodm.wel_part_no=#__wel_partflm.wel_part_no ".
				"WHERE #__wel_cusmodm.wel_cus_code='$this->wel_cus_code' AND #__wel_cusmodm.wel_part_no='$this->wel_part_no' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_cus_code_not_found");}
			$int__count=0;
			while ($int__count < mysql_num_fields($result)) 
			{
				$field_name=mysql_fetch_field($result,$int__count)->name;
				$return_val[$field_name]=$row[$field_name];
				$int__count++;
			}
			mysql_free_result($result);
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	public function addnew()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_addnew")){throw new Exception("unauthorized_access");}
		
			if ($this->wel_cus_code==""){throw new Exception("wel_cus_code_miss");}
			if($this->wel_part_no==""){throw new Exception("wel_part_no_miss");}
			if($this->wel_cus_part==""){throw new Exception("wel_cus_part_miss");}			
			
			if (!is_numeric($this->wel_std_price)){$this->wel_std_price=0;}
			$this->wel_std_price=doubleval($this->wel_std_price);
			if (!is_numeric($this->wel_roll_qty)){$this->wel_roll_qty=0;}
			$this->wel_roll_qty=doubleval($this->wel_roll_qty);
			
//			if(!check_wel_cusmasm($conn,$this->wel_cus_code)){throw new Exception("wel_cus_code_not_found");}
			if ($this->wel_cus_code!="")
			{
				$sql="select wel_cus_code from #__wel_cusmasm where wel_cus_code='$this->wel_cus_code' limit 1";
				$sql=revert_to_the_available_sql($sql);
				if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_cus_code_not_found");}
			}
//			if(!check_wel_partflm($conn,$this->wel_part_no)){throw new Exception("wel_part_no_not_found");}
			if ($this->wel_part_no!="")
			{
				$sql="select wel_part_no from #__wel_partflm where wel_part_no='$this->wel_part_no'  limit 1";
				$sql=revert_to_the_available_sql($sql);
				if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_part_no_not_found");}
			}
//			if(!check_wel_currenm($conn,$this->wel_std_cur)){throw new Exception("wel_cur_code_not_found");}
			if ($this->wel_std_cur!="")
			{
				$sql="select wel_cur_code from #__wel_currenm where wel_cur_code='$this->wel_std_cur'  limit 1";
				$sql=revert_to_the_available_sql($sql);
				if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_cur_code_not_found");}
			}
			
			$sql="SELECT wel_cus_code FROM #__wel_cusmodm WHERE wel_cus_code='$this->wel_cus_code' ".
					"AND wel_part_no='$this->wel_part_no' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
			if($row=mysql_fetch_array($result)){throw new Exception("wel_cus_part_exist");}
			
			try
			{
			
				mysql_query("begin");
				
					$sql="INSERT INTO #__wel_cusmodm(wel_cus_code,wel_cus_part,wel_part_no,".
							"wel_std_cur,wel_std_price,".
							"wel_cus_part_des1,wel_cus_part_des2,wel_cus_part_des3,wel_cus_part_des4,".
							"wel_roll_qty,wel_crt_user,wel_crt_date) ".
						"VALUES('$this->wel_cus_code','$this->wel_cus_part','$this->wel_part_no',".
							"'$this->wel_std_cur','$this->wel_std_price',".
							"'$this->wel_cus_part_des1','$this->wel_cus_part_des2','$this->wel_cus_part_des3',".
							"'$this->wel_cus_part_des4','$this->wel_roll_qty',".
							"'{$_SESSION['wel_user_id']}',now())";			
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					
				mysql_query("commit");
					
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		if($msg_code==""){$msg_code="addnew_succee";}
		$return_val["msg_code"]=$msg_code;
		$return_val["wel_cus_code"]=$this->wel_cus_code;
		$return_val["wel_part_no"]=$this->wel_part_no;
		return $return_val;
	}
	
	public function edit()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_edit")){throw new Exception("unauthorized_access");}
			
			if($this->wel_cus_code==""){throw new Exception("wel_cus_code_miss");}
			if($this->wel_part_no=="") throw new Exception("wel_part_no_miss");
			if($this->wel_cus_part=="") throw new Exception("wel_cus_part_miss");			
			
			if (!is_numeric($this->wel_std_price)){$this->wel_std_price=0;}
			$this->wel_std_price=doubleval($this->wel_std_price);
			if (!is_numeric($this->wel_roll_qty)){$this->wel_roll_qty=0;}
			$this->wel_roll_qty=doubleval($this->wel_roll_qty);
			
//			if(!check_wel_cusmasm($conn,$this->wel_cus_code)){throw new Exception("wel_cus_code_not_found");}
			if ($this->wel_cus_code!="")
			{
				$sql="select wel_cus_code from #__wel_cusmasm where wel_cus_code='$this->wel_cus_code' limit 1";
				$sql=revert_to_the_available_sql($sql);
				if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_cus_code_not_found");}
			}
//			if(!check_wel_partflm($conn,$this->wel_part_no)){throw new Exception("wel_part_no_not_found");}
			if ($this->wel_part_no!="")
			{
				$sql="select wel_part_no from #__wel_partflm where wel_part_no='$this->wel_part_no'  limit 1";
				$sql=revert_to_the_available_sql($sql);
				if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_part_no_not_found");}
			}
//			if(!check_wel_currenm($conn,$this->wel_std_cur)){throw new Exception("wel_cur_code_not_found");}
			if ($this->wel_std_cur!="")
			{
				$sql="select wel_cur_code from #__wel_currenm where wel_cur_code='$this->wel_std_cur'  limit 1";
				$sql=revert_to_the_available_sql($sql);
				if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_cur_code_not_found");}
			}
			
			$sql="SELECT wel_cus_code FROM #__wel_cusmodm WHERE wel_cus_code='$this->wel_cus_code' ".
					"AND wel_part_no='$this->wel_part_no' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_cus_part_not_found");}
			
			try
			{
				mysql_query("begin");
				
					$sql="UPDATE #__wel_cusmodm SET ".
							"wel_cus_part='$this->wel_cus_part',".
							"wel_part_no='$this->wel_part_no',".
							"wel_std_cur='$this->wel_std_cur',".
							"wel_std_price='$this->wel_std_price',".
							"wel_cus_part_des1='$this->wel_cus_part_des1',".
							"wel_cus_part_des2='$this->wel_cus_part_des2',".
							"wel_cus_part_des3='$this->wel_cus_part_des3',".
							"wel_cus_part_des4='$this->wel_cus_part_des4',".
							"wel_roll_qty='$this->wel_roll_qty',".
							"wel_upd_user='{$_SESSION['wel_user_id']}',".
							"wel_upd_date=now() ".
						"WHERE wel_cus_code='$this->wel_cus_code' AND wel_part_no='$this->wel_part_no' LIMIT 1";			
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					
				mysql_query("commit");
					
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}
			
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}		
		if($msg_code==""){$msg_code="edit_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
}
?>