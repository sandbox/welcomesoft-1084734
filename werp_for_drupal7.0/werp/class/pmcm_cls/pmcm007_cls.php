<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>

<?php
	class pmcm007_cls
	{
		public $wel_wo_no="";
		public $wel_pattern="";
		public $wel_mo_no="";
		public $wel_mo_line=0;
		public $wel_so_no="";
		public $wel_so_line=0;
		public $wel_part_no="";
		public $wel_req_qty=0;
		public $wel_req_date="";
		public $wel_start_date="";
		public $wel_fg_qty=0;
		public $wel_ctr_code="";
		public $wel_wo_remark="";
		public $wel_alrm_remark="";
		public $wel_reduce_mo="0";
		
		private $wel_prog_code="pmcm007";
	
		//=================================================================================
		//读取工作单档案
		public function read()
		{
			$msg_code="";
			$return_val=array();
			
			try
			{
				$conn=werp_db_connect();
				
				if (!check_permission($conn,$this->wel_prog_code,"wel_access_read")){throw new Exception("unauthorized_access");}
				
				$sql="SELECT worhdrm.*,partflm.wel_part_des ".
					"FROM #__wel_worhdrm AS worhdrm LEFT JOIN #__wel_partflm AS partflm ".
					"ON worhdrm.wel_part_no=partflm.wel_part_no ".
					"WHERE worhdrm.wel_wo_no='".$this->wel_wo_no."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	//查询sql时出错了
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_wo_no_not_found");}	//没有符合条件的记录

				$int__count=0;
				while ($int__count < mysql_num_fields($result)) 
				{
					$field_name=mysql_fetch_field($result,$int__count)->name;
					$return_val[$field_name]=$row[$field_name];
					$int__count++;
				}
				$wo_so_no=is_null($row["wel_so_no"]) ? "" : $row["wel_so_no"];
				$wo_so_line=doubleval(is_null($row["wel_so_line"]) ? 0 : $row["wel_so_line"]);

				if($wo_so_no!="")
				{
					$sql="SELECT a.*, c.wel_cus_des FROM #__wel_sordetm a".
						" LEFT JOIN #__wel_cusmasm c ON a.wel_cus_code=c.wel_cus_code".
						" WHERE a.wel_so_no='".$wo_so_no."' AND a.wel_so_line='".$wo_so_line."'LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
					if($row=mysql_fetch_array($result))
					{
						$return_val[wel_so_cus_code]=is_null($row["wel_cus_code"]) ? "" : $row["wel_cus_code"];
						$return_val[wel_so_cus_name]=is_null($row["wel_cus_des"]) ? "" : $row["wel_cus_des"];
						$return_val[wel_so_qty]=is_null($row["wel_req_qty"]) ? 0 : $row["wel_req_qty"];
						$return_val[wel_so_date]=is_null($row["wel_req_date"]) ? "" : $row["wel_req_date"];
						$return_val[wel_so_rmk]=is_null($row["wel_remark"]) ? "" : $row["wel_remark"];
					}else{
						$return_val[wel_so_cus_code]="";
						$return_val[wel_so_cus_name]="";
						$return_val[wel_so_qty]=0;
						$return_val[wel_so_date]="";
						$return_val[wel_so_rmk]="";
					}
				}else{
						$return_val[wel_so_cus_code]="";
						$return_val[wel_so_cus_name]="";
						$return_val[wel_so_qty]=0;
						$return_val[wel_so_date]="";
						$return_val[wel_so_rmk]="";
				}

				mysql_free_result($result);
			}
			catch (Exception $e)
			{
				$msg_code=$e->getMessage();
			}
			$return_val["msg_code"]=$msg_code;
			return $return_val;
		}
		
		//=================================================================================
		//新增工作单档案
		public function addnew()
		{
			$msg_code="";
			$return_val=array();
			
			try
			{
				$conn=werp_db_connect();
				
				if (!check_permission($conn,$this->wel_prog_code,"wel_access_addnew")){throw new Exception("unauthorized_access");}
				
				if(($this->wel_wo_no=="") && ($this->wel_pattern=="")) {throw new Exception("wel_wo_no_miss");}
				
				if($this->wel_req_date==""){throw new Exception("wel_req_date_miss");}
				$this->wel_req_date=(($this->wel_req_date=="") ? "null" : "'".$this->wel_req_date."'");
				$this->wel_start_date=(($this->wel_start_date=="") ? "null" : "'".$this->wel_start_date."'");
				
				//需求数量是否在有效范围内
				if(!is_numeric($this->wel_req_qty)){$this->wel_req_qty=0;}
				$this->wel_req_qty=doubleval($this->wel_req_qty);
				if($this->wel_req_qty<0){throw new Exception("wel_req_qty_error");}
				if($this->wel_req_qty==0){throw new Exception("wel_req_qty_miss");}
				
				//生产中心档案是否存在
				if($this->wel_ctr_code==""){throw new Exception("wel_ctr_code_miss");}
				$sql="SELECT * FROM #__wel_centrem WHERE wel_ctr_code='".$this->wel_ctr_code."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_ctr_code_not_found");}
				
				//制造单档案是否存在
				if($this->wel_mo_no==""){throw new Exception("wel_mo_no_miss");}
				$sql="SELECT * FROM #__wel_morhdrm WHERE wel_mo_no='".$this->wel_mo_no."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_mo_no_not_found");}
				
				//制造单档案明细是否存在
				if(!is_numeric($this->wel_mo_line)){$this->wo_mo_line=0;}
				$this->wel_mo_line=intval($this->wel_mo_line);
				$sql="SELECT * FROM #__wel_mordetm WHERE ".
					"wel_mo_no='".$this->wel_mo_no."' AND wel_mo_line=".$this->wel_mo_line." LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_mo_line_not_found");}
				
				$mo_wel_req_qty=doubleval(is_null($row["wel_req_qty"]) ? 0 : $row["wel_req_qty"]);
				$mo_wel_wo_qty=doubleval(is_null($row["wel_wo_qty"]) ? 0 : $row["wel_wo_qty"]);
				$this->wel_so_no=is_null($row["wel_so_no"]) ? "" : $row["wel_so_no"];
				$this->wel_so_line=intval(is_null($row["wel_so_line"]) ? 0 : $row["wel_so_line"]);
				$this->wel_part_no=(is_null($row["wel_part_no"]) ? "" : $row["wel_part_no"]);
				
				//物料档案是否存在
				$sql="SELECT * FROM #__wel_partflm WHERE wel_part_no='".$this->wel_part_no."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_part_no_not_found");}
				
				//如制造单需求数小于等于工单需求数,则此制造单已完成
				if($mo_wel_req_qty<=$mo_wel_wo_qty){throw new Exception("wel_mo_line_finished");}
				//如此工单需求数与已有工单需求数之和大于制造单需求数,则此工单需求数已超出范围
				if(($this->wel_req_qty+$mo_wel_wo_qty)>$mo_wel_req_qty){throw new Exception("wel_req_qty_over");}

				try
				{
					mysql_query('begin');
					
					if($this->wel_pattern!="")
					{
						$sql="SELECT * FROM #__wel_gentwow WHERE wel_pattern='$this->wel_pattern' LIMIT 1";
						$sql=revert_to_the_available_sql($sql);
						if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
						if(!($row=mysql_fetch_array($result))){throw new Exception("wel_pattern_not_found");}
						$wel_wo_nextno=intval(is_null($row["wel_wo_nextno"]) ? 0 : $row["wel_wo_nextno"]);
						$wel_wo_nextno=sprintf("%'08s",$wel_wo_nextno);
						if(strlen($wel_wo_nextno)>8){throw new Exception("wel_pattern_overflow");}
						$this->wel_wo_no=$this->wel_pattern.$wel_wo_nextno;
					
					//更新模式码表
						$sql="UPDATE #__wel_gentwow SET wel_wo_nextno=IFNULL(wel_wo_nextno,0)+1 WHERE ".
								"wel_pattern='".$this->wel_pattern."' LIMIT 1";
						$sql=revert_to_the_available_sql($sql);
						if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
					}
					
					//工作单档案是否存在
					$sql="SELECT * FROM #__wel_worhdrm WHERE wel_wo_no='".$this->wel_wo_no."' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
					if($row=mysql_fetch_array($result)){throw new Exception("wel_wo_no_exist");}
					
					//插入记录到工作单档案中
					$sql="INSERT INTO #__wel_worhdrm SET ".
						"wel_wo_no='".$this->wel_wo_no."',".
						"wel_pattern='".$this->wel_pattern."',".
						"wel_mo_no='".$this->wel_mo_no."',".
						"wel_mo_line=".$this->wel_mo_line.",".
						"wel_so_no='".$this->wel_so_no."',".
						"wel_so_line=".$this->wel_so_line.",".
						"wel_part_no='".$this->wel_part_no."',".
						"wel_req_qty=".$this->wel_req_qty.",".
						"wel_req_date=".$this->wel_req_date.",".
						"wel_start_date=".$this->wel_start_date.",".
						"wel_ctr_code='".$this->wel_ctr_code."',".
						"wel_wo_remark='".$this->wel_wo_remark."',".
						"wel_alrm_remark='".$this->wel_alrm_remark."',".
						"wel_crt_user='".$_SESSION["wel_user_id"]."',".
						"wel_crt_date=now() ";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
					
					//更新制造单明细的工单需求数
					$sql="UPDATE #__wel_mordetm SET ".
						"wel_wo_qty=".($this->wel_req_qty+$mo_wel_wo_qty)." WHERE ". 
						"wel_mo_no='".$this->wel_mo_no."' AND wel_mo_line=".$this->wel_mo_line." LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
					
					mysql_query('commit');
				}
				catch (Exception $e1)
				{
					mysql_query('rollback');
					throw new Exception($e1->getMessage());
				}
			}
			catch (Exception $e)
			{
				$msg_code=$e->getMessage();
			} 
			if($msg_code==""){$msg_code="addnew_succee";}
			$return_val["msg_code"]=$msg_code;
			$return_val["wel_wo_no"]=$this->wel_wo_no;
			return $return_val;
		}
		
		//=================================================================================
		//编辑工作单档案
		public function edit()
		{
			$msg_code="";
			$return_val=array();
			
			try
			{
				$conn=werp_db_connect();
				
				if (!check_permission($conn,$this->wel_prog_code,"wel_access_edit")){throw new Exception("unauthorized_access");}
				
				if($this->wel_wo_no=="") {throw new Exception("wel_wo_no_miss");}
				
				if($this->wel_req_date==""){throw new Exception("wel_req_date_miss");}
				$this->wel_req_date=(($this->wel_req_date=="") ? "null" : "'".$this->wel_req_date."'");
				$this->wel_start_date=(($this->wel_start_date=="") ? "null" : "'".$this->wel_start_date."'");
				
				//需求数量是否在有效范围内
				if(!is_numeric($this->wel_req_qty)){$this->wel_req_qty=0;}
				$this->wel_req_qty=doubleval($this->wel_req_qty);
				if($this->wel_req_qty<0){throw new Exception("wel_req_qty_error");}
				if($this->wel_req_qty==0){throw new Exception("wel_req_qty_miss");}
				
				//生产中心档案是否存在
				if($this->wel_ctr_code==""){throw new Exception("wel_ctr_code_miss");}
				$sql="SELECT * FROM #__wel_centrem WHERE wel_ctr_code='".$this->wel_ctr_code."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_ctr_code_not_found");}
				
				//制造单档案是否存在
				if($this->wel_mo_no==""){throw new Exception("wel_mo_no_miss");}
				$sql="SELECT * FROM #__wel_morhdrm WHERE wel_mo_no='".$this->wel_mo_no."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_mo_no_not_found");}
				
				//制造单档案明细是否存在
				if(!is_numeric($this->wel_mo_line)){$this->wo_mo_line=0;}
				$this->wel_mo_line=intval($this->wel_mo_line);
				$sql="SELECT * FROM #__wel_mordetm WHERE ".
					"wel_mo_no='".$this->wel_mo_no."' AND wel_mo_line=".$this->wel_mo_line." LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_mo_line_not_found");}
				
				$mo_wel_req_qty=doubleval(is_null($row["wel_req_qty"]) ? 0 : $row["wel_req_qty"]);
				$mo_wel_wo_qty=doubleval(is_null($row["wel_wo_qty"]) ? 0 : $row["wel_wo_qty"]);
				
				//物料档案是否存在
				$sql="SELECT * FROM #__wel_partflm WHERE wel_part_no='".$this->wel_part_no."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_part_no_not_found");}

				//工作单档案是否存在
				$sql="SELECT * FROM #__wel_worhdrm WHERE wel_wo_no='".$this->wel_wo_no."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_wo_no_not_found");}

				$wel_fg_qty=doubleval(is_null($row["wel_fg_qty"]) ? 0 : $row["wel_fg_qty"]);
				$wel_req_qty=doubleval(is_null($row["wel_req_qty"]) ? 0 : $row["wel_req_qty"]);
				$wel_settle_yn=doubleval(is_null($row["wel_settle_yn"]) ? 0 : $row["wel_settle_yn"]);
				if($wel_settle_yn!=0){throw new Exception("wel_wo_no_settle");}			//工作单已结算
				
				if($this->wel_req_qty!=$wel_req_qty){	//数量被修改，检测工作单明细是否存在
					$sql="SELECT * FROM #__wel_wordetm WHERE wel_wo_no='".$this->wel_wo_no."' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
					if($row=mysql_fetch_array($result)){throw new Exception("wel_wo_no_detail_exist");}
				}

				//这张工作单要增加的数量大于MO里WO的余数
				if(($this->wel_req_qty-$wel_req_qty)>($mo_wel_req_qty-$mo_wel_wo_qty)){
					throw new Exception("wel_req_qty_over");
				}
				//需求数小于入仓数
				if($this->wel_req_qty<$wel_fg_qty){throw new Exception("wel_req_qty_small_than_wel_fg_qty");}
				
				try
				{
					mysql_query('begin');

					//更新工作单
					$sql="UPDATE #__wel_worhdrm SET ".
						"wel_ctr_code='".$this->wel_ctr_code."',".
						"wel_req_date=".$this->wel_req_date.",".
						"wel_start_date=".$this->wel_start_date.",".
						"wel_req_qty=".$this->wel_req_qty.",".
						"wel_wo_remark='".$this->wel_wo_remark."',".
						"wel_alrm_remark='".$this->wel_alrm_remark."',".
						"wel_upd_user='".$_SESSION["wel_user_id"]."',".
						"wel_upd_date=now() ".
						"WHERE wel_wo_no='".$this->wel_wo_no."' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
					
					$sql="UPDATE #__wel_mordetm SET ".
								"wel_wo_qty=".($this->wel_req_qty-$wel_req_qty+$mo_wel_wo_qty)." ".
						"WHERE wel_mo_no='".$this->wel_mo_no."' AND wel_mo_line=".$this->wel_mo_line." LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
					
					mysql_query('commit');
				}
				catch (Exception $e1)
				{
					mysql_query('rollback');
					throw new Exception($e1->getMessage());
				}
			}
			catch (Exception $e)
			{
				$msg_code=$e->getMessage();
			}		
			if($msg_code==""){$msg_code="edit_succee";}
			$return_val["msg_code"]=$msg_code;
			return $return_val;
		}
		
		//=================================================================================
		//删除工作单档案
		public function delete()
		{
			$msg_code="";
			$return_val=array();
			
			try
			{
				$conn=werp_db_connect();
				
				if (!check_permission($conn,$this->wel_prog_code,"wel_access_delete")){throw new Exception("unauthorized_access");}
				
				if($this->wel_wo_no=="") {throw new Exception("wel_wo_no_miss");}
				
				//工作单档案是否存在
				$sql="SELECT * FROM #__wel_worhdrm WHERE wel_wo_no='".$this->wel_wo_no."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_wo_no_not_found");}
				
				$wel_fg_qty=doubleval(is_null($row["wel_fg_qty"]) ? 0 : $row["wel_fg_qty"]);
				$wel_req_qty=doubleval(is_null($row["wel_req_qty"]) ? 0 : $row["wel_req_qty"]);
				$this->wel_mo_no=is_null($row["wel_mo_no"]) ? "" : $row["wel_mo_no"];
				$this->wel_mo_line=intval(is_null($row["wel_mo_line"]) ? 0 : $row["wel_mo_line"]);

				if($wel_fg_qty>0){throw new Exception("wel_fg_qty_warehousing");}			//产品已入仓
				$wel_settle_yn=doubleval(is_null($row["wel_settle_yn"]) ? 0 : $row["wel_settle_yn"]);
				if($wel_settle_yn!=0){throw new Exception("wel_wo_no_settle");}			//工作单已结算
				
				//工作单明细是否已发料
				$sql="SELECT * FROM #__wel_wordetm ".
					"WHERE wel_wo_no='".$this->wel_wo_no."' AND IFNULL(wel_iss_qty,0)>0 LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if($row=mysql_fetch_array($result)){throw new Exception("wel_iss_qty_issuing");}
				
				//制造单档案明细是否存在
				if(!is_numeric($this->wel_mo_line)){$this->wo_mo_line=0;}
				$this->wel_mo_line=intval($this->wel_mo_line);
				$sql="SELECT * FROM #__wel_mordetm WHERE ".
					"wel_mo_no='".$this->wel_mo_no."' AND wel_mo_line=".$this->wel_mo_line." LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_mo_line_not_found");}
				
				$mo_wel_wo_qty=doubleval(is_null($row["wel_wo_qty"]) ? 0 : $row["wel_wo_qty"]);
				
				try
				{
					mysql_query('begin');
					
					//更新制造单明细
					$sql="UPDATE #__wel_mordetm SET ".
							"wel_wo_qty=".($mo_wel_wo_qty-$wel_req_qty)." ".
						"WHERE wel_mo_no='".$this->wel_mo_no."' AND wel_mo_line=".$this->wel_mo_line." LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
					
					//删除工作单档案
					$sql="DELETE FROM #__wel_worhdrm WHERE wel_wo_no='".$this->wel_wo_no."' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
					
					//删除工作单明细
					$sql="DELETE FROM #__wel_wordetm WHERE wel_wo_no='".$this->wel_wo_no."'";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
						
					mysql_query('commit');
				}
				catch (Exception $e1)
				{
					mysql_query('rollback');
					throw new Exception($e1->getMessage());
				}
			}
			catch (Exception $e)
			{
				$msg_code=$e->getMessage();
			}		
			if($msg_code==""){$msg_code="delete_succee";}
			$return_val["msg_code"]=$msg_code;
			return $return_val;
		}
		
		//=================================================================================
		//工作单结算
		public function wo_settle()
		{
			$msg_code="";
			$return_val=array();
			
			try
			{
				$conn=werp_db_connect();
				
				if (!check_permission($conn,$this->wel_prog_code,"wel_access_edit")){throw new Exception("unauthorized_access");}
				
				if($this->wel_wo_no=="") {throw new Exception("wel_wo_no_miss");}
				
				//工作单档案是否存在
				$sql="SELECT * FROM #__wel_worhdrm WHERE wel_wo_no='".$this->wel_wo_no."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_wo_no_not_found");}
				
				$wel_fg_qty=doubleval(is_null($row["wel_fg_qty"]) ? 0 : $row["wel_fg_qty"]);
				$wel_req_qty=doubleval(is_null($row["wel_req_qty"]) ? 0 : $row["wel_req_qty"]);
				$wel_settle_yn=doubleval(is_null($row["wel_settle_yn"]) ? 0 : $row["wel_settle_yn"]);
				if($wel_settle_yn!=0){throw new Exception("wel_wo_no_settle");}			//工作单已结算
				
				//制造单档案明细是否存在
				if(!is_numeric($this->wel_mo_line)){$this->wo_mo_line=0;}
				$this->wel_mo_line=intval($this->wel_mo_line);
				$sql="SELECT * FROM #__wel_mordetm WHERE ".
					"wel_mo_no='".$this->wel_mo_no."' AND wel_mo_line=".$this->wel_mo_line." LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_mo_line_not_found");}
				
				$mo_wel_req_qty=doubleval(is_null($row["wel_req_qty"]) ? 0 : $row["wel_req_qty"]);
				$mo_wel_wo_qty=doubleval(is_null($row["wel_wo_qty"]) ? 0 : $row["wel_wo_qty"]);
				
				try
				{
					mysql_query('begin');
					
					if(!is_numeric($this->wel_reduce_mo)){$this->wel_reduce_mo=0;}
					$this->wel_reduce_mo=doubleval($this->wel_reduce_mo);
					
					//更新制造单明细
					$sql="UPDATE #__wel_mordetm SET ".
						(($this->wel_reduce_mo==0)?"":
						"wel_req_qty=".($mo_wel_req_qty-doubleval($wel_req_qty-$wel_fg_qty)).",").
						"wel_wo_qty=". ($mo_wel_wo_qty-doubleval($wel_req_qty-$wel_fg_qty))." ".
						"WHERE wel_mo_no='".$this->wel_mo_no."' AND wel_mo_line=".$this->wel_mo_line." LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
					
					//更新工作单明细
					$sql="UPDATE #__wel_wordetm SET ".
						"wel_req_qty=IFNULL(wel_iss_qty,0) ".
						"WHERE wel_wo_no='".$this->wel_wo_no."' AND IFNULL(wel_req_qty,0)>0";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
					
					//更新工作单
					$sql="UPDATE #__wel_worhdrm SET ".
						"wel_reduce_mo=".$this->wel_reduce_mo.",".
						"wel_req_qty=IFNULL(wel_fg_qty,0),".
						"wel_upd_user='".$_SESSION["wel_user_id"]."',".
						"wel_upd_date=now(),".
						"wel_settle_yn=1,".
						"wel_settle_by='".$_SESSION["wel_user_id"]."',".
						"wel_settle_date=now() ".
						"WHERE wel_wo_no='".$this->wel_wo_no."' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
						
					mysql_query('commit');
				}
				catch (Exception $e1)
				{
					mysql_query('rollback');
					throw new Exception($e1->getMessage());
				}
			}
			catch (Exception $e)
			{
				$msg_code=$e->getMessage();
			}		
			if($msg_code==""){$msg_code="wo_settle_succee";}
			$return_val["msg_code"]=$msg_code;
			return $return_val;
		}
		
		//=================================================================================
		//工作单明细删除
		public function detail_tab0_del()
		{
			$msg_code="";
			$return_val=array();
			
			try
			{
				$conn=werp_db_connect();
				
				if (!check_permission($conn,$this->wel_prog_code,"wel_access_delete")){throw new Exception("unauthorized_access");}
				
				if($this->wel_wo_no=="") {throw new Exception("wel_wo_no_miss");}
				
				//工作单档案是否存在
				$sql="SELECT * FROM #__wel_worhdrm WHERE wel_wo_no='".$this->wel_wo_no."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_wo_no_not_found");}
				$wel_settle_yn=doubleval(is_null($row["wel_settle_yn"]) ? 0 : $row["wel_settle_yn"]);
				if($wel_settle_yn!=0){throw new Exception("wel_wo_no_settle");}			//工作单已结算
				
				//工作单明细是否存在
				$sql="SELECT * FROM #__wel_wordetm WHERE ".
					"wel_wo_no='".$this->wel_wo_no."' AND wel_wo_line=".$this->wel_wo_line." LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_wo_line_not_found");}
				$wel_iss_qty=doubleval(is_null($row["wel_iss_qty"]) ? 0 : $row["wel_iss_qty"]);
				if($wel_iss_qty>0){throw new Exception("wel_iss_qty_issuing");}		//工单细节已发料
				
				try
				{
					mysql_query('begin');
								
					//删除工作单明细
					$sql="DELETE FROM #__wel_wordetm WHERE ".
						"wel_wo_no='".$this->wel_wo_no."' and wel_wo_line=".$this->wel_wo_line." LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
					
					//更新工作单档案
					$sql="UPDATE #__wel_worhdrm SET ".
						 "wel_upd_user='".$_SESSION["wel_user_id"]."',".
						 "wel_upd_date=now() ".
						 "WHERE wel_wo_no='".$this->wel_wo_no."' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
						
					mysql_query('commit');
				}
				catch (Exception $e1)
				{
					mysql_query('rollback');
					throw new Exception($e1->getMessage());
				}
			}
			catch (Exception $e)
			{
				$msg_code=$e->getMessage();
			}		
			if($msg_code==""){$msg_code="delete_succee";}
			$return_val["msg_code"]=$msg_code;
			return $return_val;
		}
		
		//=================================================================================
		//工作单明细全部删除
		public function detail_tab0_del_all()
		{
			$msg_code="";
			$return_val=array();
			
			try
			{
				$conn=werp_db_connect();
				
				if (!check_permission($conn,$this->wel_prog_code,"wel_access_delete")){throw new Exception("unauthorized_access");}
				
				if($this->wel_wo_no=="") {throw new Exception("wel_wo_no_miss");}
				
				//工作单档案是否存在
				$sql="SELECT * FROM #__wel_worhdrm WHERE wel_wo_no='".$this->wel_wo_no."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_wo_no_not_found");}
				$wel_settle_yn=doubleval(is_null($row["wel_settle_yn"]) ? 0 : $row["wel_settle_yn"]);
				if($wel_settle_yn!=0){throw new Exception("wel_wo_no_settle");}			//工作单已结算
				
				//工作单明细是否存在
				$sql="SELECT * FROM #__wel_wordetm WHERE wel_wo_no='".$this->wel_wo_no."'";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_wo_line_not_found");}
				
				//检查物料是否有发料
				$sql="SELECT * FROM #__wel_wordetm ".
					"WHERE wel_wo_no='".$this->wel_wo_no."' AND IFNULL(wel_iss_qty,0)>0 LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if($row=mysql_fetch_array($result)){throw new Exception("wel_iss_qty_issuing");}
				
				try
				{
					mysql_query('begin');
					
					//删除工作单明细
					$sql="DELETE FROM #__wel_wordetm WHERE wel_wo_no='".$this->wel_wo_no."'";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
					
					//更新工作单档案
					$sql="UPDATE #__wel_worhdrm SET ".
						"wel_upd_user='".$_SESSION["wel_user_id"]."',".
						"wel_upd_date=now() ".
						"WHERE wel_wo_no='".$this->wel_wo_no."' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
					
					mysql_query('commit');
				}
				catch (Exception $e1)
				{
					mysql_query('rollback');
					throw new Exception($e1->getMessage());
				}
			}
			catch (Exception $e)
			{
				$msg_code=$e->getMessage();
			}		
			if($msg_code==""){$msg_code="delete_succee";}
			$return_val["msg_code"]=$msg_code;
			return $return_val;
		}
		
		//=================================================================================
		//工作单明细产生
		public function detail_tab0_generate()
		{
			$msg_code="";
			$return_val=array();
			
			try
			{
				$conn=werp_db_connect();
				
				if (!check_permission($conn,$this->wel_prog_code,"wel_access_addnew")){throw new Exception("unauthorized_access");}
				
				if($this->wel_wo_no=="") {throw new Exception("wel_wo_no_miss");}
				
				//工作单档案是否存在
				$sql="SELECT * FROM #__wel_worhdrm WHERE wel_wo_no='".$this->wel_wo_no."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_wo_no_not_found");}

				$wel_settle_yn=doubleval(is_null($row["wel_settle_yn"]) ? 0 : $row["wel_settle_yn"]);
				if($wel_settle_yn!=0){throw new Exception("wel_wo_no_settle");}			//工作单已结算
				
				$wel_part_no=is_null($row["wel_part_no"]) ? "" : $row["wel_part_no"];
				$wel_last_line=intval(is_null($row["wel_last_line"]) ? 0 : $row["wel_last_line"]);
				$wel_fg_no=is_null($row["wel_part_no"]) ? "" : $row["wel_part_no"];
				$top_wel_req_qty=doubleval(is_null($row["wel_req_qty"]) ? 0 : $row["wel_req_qty"]);
				
				//品号(生产)档案是否存在
				$sql="SELECT * FROM #__wel_partflm WHERE wel_part_no='".$wel_part_no."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_part_no_not_found");}
				$wel_bom_yn=intval(is_null($row["wel_bom_yn"]) ? 0 : $row["wel_bom_yn"]);
				if($wel_bom_yn==0){throw new Exception("wel_bom_not_approved");}		//产品BOM未批核
				
				//物料清单是否存在
				$sql="SELECT * FROM #__wel_engbomm WHERE wel_assm_no='".$wel_part_no."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_bom_not_found");}
				
				//工作单明细是否存在
				$sql="SELECT * FROM #__wel_wordetm WHERE wel_wo_no='".$this->wel_wo_no."' AND IFNULL(wel_req_qty,0)>0 LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if($row=mysql_fetch_array($result)){throw new Exception("wel_wo_no_detail_exist");}
				
				//删除工作单细节里多余记录行
				$sql="DELETE FROM #__wel_wordetm WHERE wel_wo_no='".$this->wel_wo_no."' AND IFNULL(wel_req_qty,0)<=0 LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				
				$int__stackspoint = 0;
				$arr__stacks=array();
				$arr__stacks[$int__stackspoint][0]=$wel_fg_no;      //ASSM NO
				$arr__stacks[$int__stackspoint][1]=$wel_fg_no;      //PART NO
				$arr__stacks[$int__stackspoint][2]=1;       //top_wel_qty_per
				
				while($int__stackspoint>=0){
					//刚进来是FINISH GOOD
					if (strtoupper($arr__stacks[$int__stackspoint][0])!=strtoupper($arr__stacks[$int__stackspoint][1])){
						$wel_last_line++;
						$wel_assm_no=$arr__stacks[$int__stackspoint][0];
						$wel_part_no=$arr__stacks[$int__stackspoint][1];
						$top_wel_qty_per=$arr__stacks[$int__stackspoint][2];
						$wel_req_qty=$arr__stacks[$int__stackspoint][3];
						$wel_org_qty=$arr__stacks[$int__stackspoint][3];
						/*
						$wel_part_no=$arr__stacks[$int__stackspoint][4];
						$wel_qp_eng=$arr__stacks[$int__stackspoint][5];
						$wel_qty_per=$arr__stacks[$int__stackspoint][6];
						$wel_scp_fact=$arr__stacks[$int__stackspoint][7];
						$wel_eng_unit=$arr__stacks[$int__stackspoint][8];
						$wel_bom_rmk=$arr__stacks[$int__stackspoint][9];
						$wel_bom_loc=$arr__stacks[$int__stackspoint][10];
						$wel_unit_exg=$arr__stacks[$int__stackspoint][11];
						*/
						$wel_cat_code="";
						$wel_avg_cost=0;
						$wel_std_cost=0;
						$sql="SELECT * FROM #__wel_partflm WHERE wel_part_no='".$wel_part_no."' LIMIT 1";
						$sql=revert_to_the_available_sql($sql);
						if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
						if($row=mysql_fetch_array($result)){
							$wel_cat_code=is_null($row["wel_cat_code"]) ? "" : $row["wel_cat_code"];
							$wel_avg_cost=doubleval(is_null($row["wel_avg_cost"]) ? 0 : $row["wel_avg_cost"]);
							$wel_std_cost=doubleval(is_null($row["wel_std_cost"]) ? 0 : $rwo["wel_std_cost"]);
						}

						$sql="SELECT * FROM #__wel_engbomm WHERE wel_assm_no='".$wel_part_no."' LIMIT 1";
						$sql=revert_to_the_available_sql($sql);
						if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
						if($row=mysql_fetch_array($result)){	//这个物料为中间半成品物料
							$wel_req_qty=0;
						}

						try
						{
							mysql_query('begin');
							
							//添加工作单明细
							$sql="INSERT INTO #__wel_wordetm SET ".
								"wel_wo_no='".$this->wel_wo_no."',".
								"wel_wo_line='".$wel_last_line."',".
								"wel_assm_no='".$wel_assm_no."',".
								"wel_part_no='".$wel_part_no."',".
								"wel_req_qty='".$wel_req_qty."',".
								"wel_qty_per=".$top_wel_qty_per.",".
								"wel_cat_code='".$wel_cat_code."',".
								"wel_remark='',".
								"wel_avg_cost='".$wel_avg_cost."',".
								"wel_std_cost='".$wel_std_cost."',".
								"wel_iss_qty='0',".
								"wel_org_qty='".$wel_org_qty."',".
								"wel_qp_ratio=".$top_wel_qty_per.",".
								//"wel_req_date='".$wel_req_date."',".
								"wel_crt_user='".$_SESSION["wel_user_id"]."',".
								"wel_crt_date=now() ";
							$sql=revert_to_the_available_sql($sql);
							if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}

							//更新工作单档案
							$sql="UPDATE #__wel_worhdrm SET ".
								"wel_last_line='$wel_last_line' ".
								"WHERE wel_wo_no='$this->wel_wo_no' LIMIT 1";
							$sql=revert_to_the_available_sql($sql);
							if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}

							mysql_query('commit');
						}
						catch (Exception $e1)
						{
							mysql_query('rollback');
							throw new Exception($e1->getMessage());
						}
					}
					
					$top_wel_qty_per=$arr__stacks[$int__stackspoint][2];
					$sql="SELECT #__wel_engbomm.* FROM #__wel_engbomm WHERE ".
						"#__wel_engbomm.wel_assm_no='".$arr__stacks[$int__stackspoint][1]."' ".
						"ORDER BY #__wel_engbomm.wel_part_no DESC";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}    //查询sql时出错了
					while($row=mysql_fetch_array($result)){
						$arr__stacks[$int__stackspoint][0]=$row["wel_assm_no"];
						$arr__stacks[$int__stackspoint][1]=$row["wel_part_no"];
						$arr__stacks[$int__stackspoint][2]=doubleval(is_null($row["wel_qty_per"])?0:$row["wel_qty_per"])*
							$top_wel_qty_per;
						$arr__stacks[$int__stackspoint][3]=doubleval(is_null($row["wel_qty_per"])?0:$row["wel_qty_per"])*
							$top_wel_qty_per*$top_wel_req_qty;
						/*
						$arr__stacks[$int__stackspoint][4]=$row["wel_part_no"];
						$arr__stacks[$int__stackspoint][5]=doubleval(is_null($row["wel_qp_eng"])?0:$row["wel_qp_eng"]);
						$arr__stacks[$int__stackspoint][6]=doubleval(is_null($row["wel_qty_per"])?0:$row["wel_qty_per"]);
						$arr__stacks[$int__stackspoint][7]=doubleval(is_null($row["wel_scp_fact"])?0:$row["wel_scp_fact"]);
						$arr__stacks[$int__stackspoint][8]=$row["wel_eng_unit"];
						$arr__stacks[$int__stackspoint][9]=(is_null($row["wel_bom_rmk"])?"":$row["wel_bom_rmk"]);
						$arr__stacks[$int__stackspoint][10]=(is_null($row["wel_bom_loc"])?"":$row["wel_bom_loc"]);
						$arr__stacks[$int__stackspoint][11]=doubleval(is_null($row["wel_unit_exg"])?0:$row["wel_unit_exg"]);
						*/

						$int__stackspoint++;
					}

					$int__stackspoint--;
				}

			}
			catch (Exception $e)
			{
				$msg_code=$e->getMessage();
			}		
			if($msg_code==""){$msg_code="generate_succee";}
			$return_val["msg_code"]=$msg_code;
			return $return_val;
		}
		
	}
?>
