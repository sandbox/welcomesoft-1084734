<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT

//require_once(WERP_SITE_PATH_INC."mktm_stdlib_cls.php");
?>
<?php 
class pmcm007a_cls
{
	public $wel_wo_no="";
	public $wel_wo_line=0;
	public $wel_assm_no="";
	public $wel_part_no="";
	public $wel_req_qty=0;
	public $wel_remark="";
	
	private $wel_prog_code="pmcm007";
	
	//读取工作单明细
	public function read()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_read")){throw new Exception("unauthorized_access");}
			
			$sql="SELECT w.*, a.wel_part_des as wel_assm_des, b.wel_part_des as wel_part_des, b.wel_unit ".
				"FROM #__wel_wordetm w ".
				"LEFT JOIN #__wel_partflm as a ON w.wel_assm_no=a.wel_part_no ".
				"LEFT JOIN #__wel_partflm as b ON w.wel_part_no=b.wel_part_no ".
				"WHERE w.wel_wo_no='".$this->wel_wo_no."' AND w.wel_wo_line=".$this->wel_wo_line." LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	//查询sql时出错了
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_wo_no_not_found");}	//没有符合条件的记录
			$int__count=0;
			while ($int__count < mysql_num_fields($result)) 
			{
				$field_name=mysql_fetch_field($result,$int__count)->name;
				$return_val[$field_name]=$row[$field_name];
				$int__count++;
			}
			mysql_free_result($result);
			throw new Exception("");
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	//新增工作单明细
	public function addnew()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_addnew")){throw new Exception("unauthorized_access");}
			
			if($this->wel_wo_no=="") {throw new Exception("wel_wo_no_miss");}

			//工作单档案是否存在
			$sql="SELECT * FROM #__wel_worhdrm WHERE wel_wo_no='".$this->wel_wo_no."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_wo_no_not_found");}
			$wel_settle_yn=doubleval(is_null($row["wel_settle_yn"]) ? 0 : $row["wel_settle_yn"]);
			if($wel_settle_yn!=0){throw new Exception("wel_wo_no_settle");}			//工作单已结算
			$wel_last_line=intval(is_null($row["wel_last_line"]) ? 0 : $row["wel_last_line"]);
			$wel_hdr_qty=intval(is_null($row["wel_req_qty"]) ? 0 : $row["wel_req_qty"]);
			
			//工作单明细行号
			$sql="SELECT wel_wo_line FROM #__wel_wordetm WHERE ".
				"wel_wo_no='".$this->wel_wo_no."' AND wel_wo_line>".$wel_last_line." ORDER BY wel_wo_line DESC LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if($row=mysql_fetch_array($result)){
				$wel_last_line=intval(is_null($row["wel_wo_line"]) ? 0 : $row["wel_wo_line"]);
			}
			$wel_last_line++;
			
			//产品档案是否存在
			if($this->wel_assm_no=="") {throw new Exception("wel_assm_no_miss");}
			$sql="SELECT * FROM #__wel_partflm WHERE wel_part_no='".$this->wel_assm_no."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_assm_no_not_found");}
			
			//物料档案是否存在
			if($this->wel_part_no=="") {throw new Exception("wel_part_no_miss");}
			$sql="SELECT * FROM #__wel_partflm WHERE wel_part_no='".$this->wel_part_no."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_part_no_not_found");}
			
			$wel_cat_code=is_null($row["wel_cat_code"]) ? "" : $row["wel_cat_code"];
			$wel_avg_cost=doubleval(is_null($row["wel_avg_cost"]) ? 0 : $row["wel_avg_cost"]);
			$wel_std_cost=doubleval(is_null($row["wel_std_cost"]) ? 0 : $rwo["wel_std_cost"]);
			
			//需求数是否在有效范围内
			if(!is_numeric($this->wel_req_qty)){$this->wel_req_qty=0;}
			$this->wel_req_qty=doubleval($this->wel_req_qty);
			if($this->wel_req_qty==0){throw new Exception("wel_req_qty_miss");}
			if($this->wel_req_qty<0){throw new Exception("wel_req_qty_error");}
			if($wel_hdr_qty>0)
			{
				$wel_qty_per=$this->wel_req_qty / $wel_hdr_qty;
			}else{
				$wel_qty_per=0;
			}
			
			try
			{
				mysql_query('begin');
				
				//添加工作单明细
				$sql="INSERT INTO #__wel_wordetm SET ".
					"wel_wo_no='".$this->wel_wo_no."',".
					"wel_wo_line='".$wel_last_line."',".
					"wel_assm_no='".$this->wel_assm_no."',".
					"wel_part_no='".$this->wel_part_no."',".
					"wel_req_qty='".$this->wel_req_qty."',".
					"wel_qty_per=".$wel_qty_per.",".
					"wel_cat_code='".$wel_cat_code."',".
					"wel_remark='".$this->wel_remark."',".
					"wel_avg_cost='".$wel_avg_cost."',".
					"wel_std_cost='".$wel_std_cost."',".
					"wel_iss_qty='0',".
					//"wel_org_qty='0',".
					"wel_qp_ratio=".$wel_qty_per.",".
					//"wel_req_date='".$wel_req_date."',".
					"wel_crt_user='".$_SESSION["wel_user_id"]."',".
					"wel_crt_date=now() ";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}

				//更新工作单档案
				$sql="UPDATE #__wel_worhdrm SET ".
					"wel_last_line='$wel_last_line'".
					" WHERE wel_wo_no='$this->wel_wo_no' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}

				mysql_query('commit');
			}
			catch (Exception $e1)
			{
				mysql_query('rollback');
				throw new Exception($e1->getMessage());
			}
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		if($msg_code==""){$msg_code="addnew_succee";}
		$return_val["msg_code"]=$msg_code;
		$return_val["wel_wo_no"]=$this->wel_wo_no;
		$return_val["wel_wo_line"]=$this->wel_wo_line;
		return $return_val;
	}
	
	
	//编辑工程通知档案细节
	public function edit()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_edit")){throw new Exception("unauthorized_access");}
			
			if($this->wel_wo_no=="") {throw new Exception("wel_wo_no_miss");}

			//工作单档案是否存在
			$sql="SELECT * FROM #__wel_worhdrm WHERE wel_wo_no='".$this->wel_wo_no."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_wo_no_not_found");}
			$wel_settle_yn=doubleval(is_null($row["wel_settle_yn"]) ? 0 : $row["wel_settle_yn"]);
			if($wel_settle_yn!=0){throw new Exception("wel_wo_no_settle");}			//工作单已结算
			$wel_hdr_qty=intval(is_null($row["wel_req_qty"]) ? 0 : $row["wel_req_qty"]);

			//产品档案是否存在
			if($this->wel_assm_no=="") {throw new Exception("wel_assm_no_miss");}
			$sql="SELECT * FROM #__wel_partflm WHERE wel_part_no='".$this->wel_assm_no."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_assm_no_not_found");}
			
			//物料档案是否存在
			if($this->wel_part_no=="") {throw new Exception("wel_part_no_miss");}
			$sql="SELECT * FROM #__wel_partflm WHERE wel_part_no='".$this->wel_part_no."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_part_no_not_found");}
			
			$wel_cat_code=is_null($row["wel_cat_code"]) ? "" : $row["wel_cat_code"];
			$wel_avg_cost=doubleval(is_null($row["wel_avg_cost"]) ? 0 : $row["wel_avg_cost"]);
			$wel_std_cost=doubleval(is_null($row["wel_std_cost"]) ? 0 : $rwo["wel_std_cost"]);
			
			//需求数是否在有效范围内
			if(!is_numeric($this->wel_req_qty)){$this->wel_req_qty=0;}
			$this->wel_req_qty=doubleval($this->wel_req_qty);
			if($this->wel_req_qty==0){throw new Exception("wel_req_qty_miss");}
			if($this->wel_req_qty<0){throw new Exception("wel_req_qty_error");}

			//工作单明细是否存在
			$sql="SELECT * FROM #__wel_wordetm WHERE ".
				"wel_wo_no='".$this->wel_wo_no."' AND wel_wo_line=".$this->wel_wo_line." LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_wo_line_not_found");}
			$wel_iss_qty=doubleval(is_null($row["wel_iss_qty"]) ? 0 : $row["wel_iss_qty"]);
			
			//需求数量应小于工作单数量
			$this->wel_req_qty=doubleval($this->wel_req_qty);
			if($this->wel_req_qty<$wel_iss_qty){throw new Exception("wel_req_qty_small_than_wel_iss_qty");}

			if($wel_hdr_qty>0)
			{
				$wel_qty_per=$this->wel_req_qty / $wel_hdr_qty;
			}else{
				$wel_qty_per=0;
			}
			
			try
			{
				mysql_query('begin');
					
				//更新工作单明细
				$sql="UPDATE #__wel_wordetm SET ".
					"wel_req_qty=".$this->wel_req_qty.",".
					"wel_qty_per=".$wel_qty_per.",".
					"wel_cat_code='".$wel_cat_code."',".
					"wel_avg_cost=".$wel_avg_cost.",".
					"wel_std_cost=".$wel_std_cost.",".
					"wel_remark='".$this->wel_remark."',".
					"wel_qp_ratio=".$wel_qty_per.",".
					//"wel_req_date='".$wel_req_date."',".
					"wel_upd_user='".$_SESSION['wel_user_id']."',".
					"wel_upd_date=now() ".
					"WHERE wel_wo_no='".$this->wel_wo_no."' AND wel_wo_line=".$this->wel_wo_line." ";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
					
				mysql_query('commit');
			}
			catch (Exception $e1)
			{
				mysql_query('rollback');
				throw new Exception($e1->getMessage());
			}
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}		
		if($msg_code==""){$msg_code="edit_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
}
?>