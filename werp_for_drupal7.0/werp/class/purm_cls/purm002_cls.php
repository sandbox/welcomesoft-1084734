<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>

<?php
class purm002_cls
{
	public $wel_ven_code="";
	public $wel_ven_des="";
	public $wel_ven_des1="";
	public $wel_abbre_des="";
		
	public $wel_ven_add1="";
	public $wel_ven_add2="";
	public $wel_ven_add3="";
	public $wel_ven_add4="";
	public $wel_ven_email="";
		
	public $wel_ven_cont="";
	public $wel_buyer_code="";
	public $wel_tax_type="";
	public $wel_tax_rate="";
	public $wel_tax_no="";
		
	public $wel_cur_code="";
	public $wel_ven_tele="";
	public $wel_ven_tele1="";
	public $wel_ven_fax="";
	public $wel_ven_fax1="";
		
	public $wel_bank_account="";
	public $wel_ven_term="";
	public $wel_ven_pbase="";
	public $wel_dely_code="";
	public $wel_ven_svia="";
	public $wel_active=0;
	public $wel_discount=0;
	
	private $wel_prog_code="purm002";

	public function read()
	{
		$msg_code="";
		$return_val=array();
 
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_read")){throw new Exception("unauthorized_access");}
			
			$sql="SELECT * FROM #__wel_venmasm WHERE wel_ven_code='".$this->wel_ven_code."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_ven_code_not_found");}
			$int__count=0;
			while ($int__count < mysql_num_fields($result)) 
			{
				$field_name=mysql_fetch_field($result,$int__count)->name;
				$return_val[$field_name]=$row[$field_name];
				$int__count++;
			}
			mysql_free_result($result);
			
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	public function addnew()
	{
		$msg_code="";
		$return_val=array();
 
		try
		{
			$conn	= werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_addnew")){throw new Exception("unauthorized_access");}
			
			if($this->wel_ven_code==""){throw new Exception("wel_ven_code_miss");}
			if($this->wel_ven_des==""){throw new Exception("wel_ven_des_miss");}
			$sql="SELECT * FROM #__wel_venmasm WHERE wel_ven_code='".$this->wel_ven_code."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if($row=mysql_fetch_array($result)){throw new Exception("wel_ven_code_exist");}
			
			if($this->wel_buyer_code!="")
			{
				$sql="SELECT * FROM #__wel_purhanm WHERE wel_buyer_code='".$this->wel_buyer_code."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_buyer_code_not_found");}
			}
			
			if($this->wel_cur_code!="")
			{
				$sql="SELECT * FROM #__wel_currenm WHERE wel_cur_code='".$this->wel_cur_code."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_cur_code_not_found");}
			}
			
			if($this->wel_ven_term!="")
			{
				$sql="SELECT * FROM #__wel_portyem WHERE wel_type_code='".$this->wel_ven_term."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_type_code_not_found");}
			}

			if($this->wel_ven_pbase!="")
			{
				$sql="SELECT * FROM #__wel_pbasefm WHERE wel_pbase_code='".$this->wel_ven_pbase."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_ven_pbase_not_found");}
			}

			if($this->wel_dely_code!="")
			{
				$sql="SELECT * FROM #__wel_delytom WHERE wel_dely_code='".$this->wel_dely_code."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_dely_code_not_found");}
			}
			
			if($this->wel_ven_svia!="")
			{
				$sql="SELECT * FROM #__wel_shpviam WHERE wel_svia_code='".$this->wel_ven_svia."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_svia_code_not_found");}
			}
			
			try
			{
				mysql_query("begin");
				
					$sql="INSERT INTO #__wel_venmasm(wel_ven_code,wel_ven_des,wel_ven_des1,wel_abbre_des,wel_ven_add1,
								wel_ven_add2,wel_ven_add3,wel_ven_add4,wel_ven_email,wel_ven_cont,wel_buyer_code,
								wel_tax_type,wel_tax_rate,wel_tax_no,wel_cur_code,wel_ven_tele,wel_ven_tele1,wel_ven_fax,
								wel_ven_fax1,wel_bank_account,wel_dely_code,wel_ven_term,wel_ven_pbase,wel_ven_svia,wel_active,
								wel_discount) ".
						"VALUES('".$this->wel_ven_code."','".$this->wel_ven_des."','".$this->wel_ven_des1."','".
								$this->wel_abbre_des."','".$this->wel_ven_add1."','".$this->wel_ven_add2."','".$this->wel_ven_add3."','".
								$this->wel_ven_add4."','".$this->wel_ven_email."','".$this->wel_ven_cont."','".$this->wel_buyer_code."',".
								$this->wel_tax_type.",".$this->wel_tax_rate.",'".$this->wel_tax_no."','".$this->wel_cur_code."','".
								$this->wel_ven_tele."','".$this->wel_ven_tele1."','".$this->wel_ven_fax."','".$this->wel_ven_fax1."','".
								$this->wel_bank_account."','".$this->wel_dely_code."','".$this->wel_ven_term."','".$this->wel_ven_pbase."','".$this->wel_ven_svia."',".
								$this->wel_active.",".$this->wel_discount.")";	 
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
					mysql_free_result($result);
				
				mysql_query("commit");
				
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}
			
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		if($msg_code==""){$msg_code="addnew_succee";}
		$return_val["msg_code"]=$msg_code;
		$return_val["wel_ven_code"]=$this->wel_ven_code;
		return $return_val;
	}
	
	public function edit()
	{
		$msg_code="";
		$return_val=array();
 
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_edit")){throw new Exception("unauthorized_access");}
			
			if($this->wel_ven_code==""){throw new Exception("wel_ven_code_miss");}
			if($this->wel_ven_des==""){throw new Exception("wel_ven_des_miss");}
			$sql="SELECT * FROM #__wel_venmasm WHERE wel_ven_code='".$this->wel_ven_code."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_ven_code_not_found");}
			
			if($this->wel_buyer_code!="")
			{
				$sql="SELECT * FROM #__wel_purhanm WHERE wel_buyer_code='".$this->wel_buyer_code."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_buyer_code_not_found");}
			}
			
			if($this->wel_cur_code!="")
			{
				$sql="SELECT * FROM #__wel_currenm WHERE wel_cur_code='".$this->wel_cur_code."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_cur_code_not_found");}
			}
			
			if($this->wel_cur_code!="")
			{
				$sql="SELECT * FROM #__wel_currenm WHERE wel_cur_code='".$this->wel_cur_code."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_cur_code_not_found");}
			}
			
			if($this->wel_ven_term!="")
			{
				$sql="SELECT * FROM #__wel_portyem WHERE wel_type_code='".$this->wel_ven_term."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_type_code_not_found");}
			}

			if($this->wel_ven_pbase!="")
			{
				$sql="SELECT * FROM #__wel_pbasefm WHERE wel_pbase_code='".$this->wel_ven_pbase."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_ven_pbase_not_found");}
			}

			if($this->wel_dely_code!="")
			{
				$sql="SELECT * FROM #__wel_delytom WHERE wel_dely_code='".$this->wel_dely_code."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_dely_code_not_found");}
			}
			
			if($this->wel_ven_svia!="")
			{
				$sql="SELECT * FROM #__wel_shpviam WHERE wel_svia_code='".$this->wel_ven_svia."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_svia_code_not_found");}
			}

			try
			{
				mysql_query("begin");
				
					$sql="UPDATE #__wel_venmasm SET ".
								"wel_ven_des='".$this->wel_ven_des."',".
								"wel_ven_des1='".$this->wel_ven_des1."',".
								"wel_abbre_des='".$this->wel_abbre_des."',".
								"wel_ven_add1='".$this->wel_ven_add1."',".
								"wel_ven_add2='".$this->wel_ven_add2."',".
								"wel_ven_add3='".$this->wel_ven_add3."',".
								"wel_ven_add4='".$this->wel_ven_add4."',".
								"wel_ven_email='".$this->wel_ven_email."',".
								"wel_ven_cont='".$this->wel_ven_cont."',".
								"wel_buyer_code='".$this->wel_buyer_code."',".
								"wel_tax_type=".$this->wel_tax_type.",".
								"wel_tax_rate=".$this->wel_tax_rate.",".
								"wel_tax_no='".$this->wel_tax_no."',".
								"wel_cur_code='".$this->wel_cur_code."',".
								"wel_ven_tele='".$this->wel_ven_tele."',".
								"wel_ven_tele1='".$this->wel_ven_tele1."',".
								"wel_ven_fax='".$this->wel_ven_fax."',".
								"wel_ven_fax1='".$this->wel_ven_fax1."',".
								"wel_bank_account='".$this->wel_bank_account."',".
								"wel_dely_code='".$this->wel_dely_code."',".
								"wel_ven_term='".$this->wel_ven_term."',".
								"wel_ven_pbase='".$this->wel_ven_pbase."',".
								"wel_ven_svia='".$this->wel_ven_svia."',".
								"wel_active=".$this->wel_active.",".
								"wel_discount=".$this->wel_discount." ".
						"WHERE wel_ven_code='".$this->wel_ven_code."' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
					mysql_free_result($result);
					
				mysql_query("commit");
				
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}	
			
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}		
		if($msg_code==""){$msg_code="edit_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	public function delete()
	{
		$msg_code="";
		$return_val=array();
 
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_delete")){throw new Exception("unauthorized_access");}
			
			if($this->wel_ven_code==""){throw new Exception("wel_ven_code_miss");}
			$sql="SELECT * FROM #__wel_venmasm WHERE wel_ven_code='".$this->wel_ven_code."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_ven_code_not_found");}
			
			try
			{
				mysql_query("begin");
				
					$sql="DELETE FROM #__wel_venmasm WHERE wel_ven_code='".$this->wel_ven_code."' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				
				mysql_query("commit");
				
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}
			
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		if($msg_code==""){$msg_code="delete_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
}
?>
