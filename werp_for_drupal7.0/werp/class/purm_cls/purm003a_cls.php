<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>

<?php
class purm003a_cls
{
	public $wel_ven_code="";
	public $wel_part_no="";
	public $wel_part_des;
	public $wel_unit_code;
	public $wel_pur_unit;
	public $wel_pur_unit_rate;
	public $wel_lead_tm;
	public $wel_pkg_ord;
	public $wel_min_ord;
	public $wel_ven_part_des;
	
	private $wel_prog_code="purm003";

	public function read()
	{
		$msg_code="";
		$return_val=array();
		 
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_read")){throw new Exception("unauthorized_access");}
			
			$sql="SELECT #__wel_venparm.wel_ven_code,".
						"#__wel_venparm.wel_part_no,".
						"#__wel_partflm.wel_part_des,".
						"#__wel_partflm.wel_unit as wel_unit_code,".
						"#__wel_venparm.wel_ven_part,".
						"#__wel_venparm.wel_ven_part_des,".
						"#__wel_venparm.wel_pur_unit,".
						"#__wel_venparm.wel_pur_unit_rate,".
						"#__wel_venparm.wel_min_ord,".
						"#__wel_venparm.wel_lead_tm,".
						"#__wel_venparm.wel_pkg_ord ".
				"FROM #__wel_venparm ".
				"LEFT JOIN #__wel_partflm ON #__wel_venparm.wel_part_no=#__wel_partflm.wel_part_no ".
				"WHERE #__wel_venparm.wel_ven_code='$this->wel_ven_code' ".
					"AND #__wel_venparm.wel_part_no='$this->wel_part_no' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	//查询sql时出错了
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_ven_part_not_found");}	//没有符合条件的记录
			$int__count=0;
			while ($int__count < mysql_num_fields($result)) 
			{
				$field_name=mysql_fetch_field($result,$int__count)->name;
				$return_val[$field_name]=$row[$field_name];
				$int__count++;
			}
			mysql_free_result($result);
			
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	public function addnew()
	{
		$msg_code="";
		$return_val=array();
	 
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_addnew")){throw new Exception("unauthorized_access");}

			//判断品号是否存在
			if ($this->wel_part_no==""){throw new Exception("wel_part_no_miss");}

			$sql="SELECT wel_part_no FROM #__wel_partflm WHERE wel_part_no='$this->wel_part_no' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_part_no_not_found");}
			
			if($this->wel_pur_unit==''){
				$sql="SELECT UPPER(wel_unit) as tmp_unit FROM #__wel_partflm WHERE wel_part_no='$this->wel_part_no' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_part_no_not_found");}

				$this->wel_pur_unit=$row["tmp_unit"];
				$this->wel_pur_unit_rate=1.000000;
			}else{
				$this->wel_pur_unit=strtoupper($this->wel_pur_unit);

				$sql="SELECT CASE WHEN wel_unit='$this->wel_pur_unit' THEN ROUND(1.000000,6)".
					" WHEN wel_pur_unit='$this->wel_pur_unit' THEN ROUND(IFNULL(wel_pur_unit_rate,1),6)".
					" WHEN wel_pur_unit1='$this->wel_pur_unit' THEN ROUND(IFNULL(wel_pur_unit_rate1,1),6)".
					" WHEN wel_pur_unit2='$this->wel_pur_unit' THEN ROUND(IFNULL(wel_pur_unit_rate2,1),6)". 
					" END AS pur_rate FROM #__wel_partflm WHERE wel_part_no='$this->wel_part_no'";
				$sql=revert_to_the_available_sql($sql);
				if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_pur_unit_error");}

				if (is_null($row["pur_rate"])){throw new Exception("wel_pur_unit_error");}
				$this->wel_pur_unit_rate=$row["pur_rate"];
			}
			
			//判断相同的供应商码＋品号的细节是否已经添加
			$sql="SELECT * FROM #__wel_venparm WHERE wel_part_no='$this->wel_part_no' AND wel_ven_code='$this->wel_ven_code' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
			if($row=mysql_fetch_array($result)){throw new Exception("wel_ven_part_exist");}
			
			if(!is_numeric($this->wel_lead_tm)){$this->wel_lead_tm=0;}
			$this->wel_lead_tm=intval($this->wel_lead_tm);
			if(!is_numeric($this->wel_min_ord)){$this->wel_min_ord=0;}
			$this->wel_min_ord=doubleval($this->wel_min_ord);
			if(!is_numeric($this->wel_pkg_ord)){$this->wel_pkg_ord=0;}
			$this->wel_pkg_ord=doubleval($this->wel_pkg_ord);
			
			if($this->wel_min_ord<0)
			{
				throw new Exception("wel_min_ord_error");
			}
			if($this->wel_pkg_ord<0)
			{
				throw new Exception("wel_pkg_ord_error");
			}
			
			try
			{
				mysql_query('begin');
				
					$sql="INSERT INTO #__wel_venparm SET ".
						"wel_ven_code='$this->wel_ven_code',".
						"wel_part_no='$this->wel_part_no',".
						"wel_pur_unit='$this->wel_pur_unit',".
						"wel_pur_unit_rate='$this->wel_pur_unit_rate',".
						"wel_lead_tm='$this->wel_lead_tm',".
						"wel_pkg_ord='$this->wel_pkg_ord',".
						"wel_min_ord='$this->wel_min_ord',".
						"wel_ven_part='$this->wel_ven_part'";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
					mysql_free_result($result);
					
				mysql_query("commit");
				
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}
			
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		if($msg_code==""){$msg_code="addnew_succee";}
		$return_val["msg_code"]=$msg_code;
		$return_val["wel_ven_code"]=$this->wel_ven_code;
		$return_val["wel_part_no"]=$this->wel_part_no;
		return $return_val;
	}
	
	public function edit()
	{
		$msg_code="";
		$return_val=array();
 
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_edit")){throw new Exception("unauthorized_access");}
			
			if ($this->wel_part_no==""){throw new Exception("wel_part_no_miss");}

			if($this->wel_pur_unit==''){
				$sql="SELECT UPPER(wel_unit) as tmp_unit FROM #__wel_partflm WHERE wel_part_no='$this->wel_part_no' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_part_no_not_found");}

				$this->wel_pur_unit=$row["tmp_unit"];
				$this->wel_pur_unit_rate=1.000000;
			}else{
				$this->wel_pur_unit=strtoupper($this->wel_pur_unit);

				$sql="SELECT CASE WHEN wel_unit='$this->wel_pur_unit' THEN ROUND(1.000000,6)".
					" WHEN wel_pur_unit='$this->wel_pur_unit' THEN ROUND(IFNULL(wel_pur_unit_rate,1),6)".
					" WHEN wel_pur_unit1='$this->wel_pur_unit' THEN ROUND(IFNULL(wel_pur_unit_rate1,1),6)".
					" WHEN wel_pur_unit2='$this->wel_pur_unit' THEN ROUND(IFNULL(wel_pur_unit_rate2,1),6)". 
					" END AS pur_rate FROM #__wel_partflm WHERE wel_part_no='$this->wel_part_no'";
				$sql=revert_to_the_available_sql($sql);
				if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_pur_unit_error");}

				if (is_null($row["pur_rate"])){throw new Exception("wel_pur_unit_error");}
				$this->wel_pur_unit_rate=$row["pur_rate"];
			}

			//
			//判断兑换率是否存在
			//$sql="SELECT * FROM #__wel_partflm WHERE wel_part_no='$this->wel_part_no' ".
			//		"AND (wel_pur_unit='$this->wel_pur_unit' OR wel_pur_unit1='$this->wel_pur_unit' OR wel_pur_unit2='$this->wel_pur_unit')";
			//$sql=revert_to_the_available_sql($sql);
			//if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
			//if(!($row=mysql_fetch_array($result))){throw new Exception("wel_pur_unit_error");}
			
			//判断相同的供应商码＋品号的细节是否已经添加
			$sql="SELECT * FROM #__wel_venparm WHERE wel_part_no='$this->wel_part_no' AND wel_ven_code='$this->wel_ven_code' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}	
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_ven_part_not_found");}
			
			if(!is_numeric($this->wel_lead_tm)){$this->wel_lead_tm=0;}
			$this->wel_lead_tm=intval($this->wel_lead_tm);
			if(!is_numeric($this->wel_min_ord)){$this->wel_min_ord=0;}
			$this->wel_min_ord=doubleval($this->wel_min_ord);
			if(!is_numeric($this->wel_pkg_ord)){$this->wel_pkg_ord=0;}
			$this->wel_pkg_ord=doubleval($this->wel_pkg_ord);
			
			if($this->wel_min_ord<0)
			{
				throw new Exception("wel_min_ord_error");
			}
			if($this->wel_pkg_ord<0)
			{
				throw new Exception("wel_pkg_ord_error");
			}
			
			try
			{
				mysql_query('begin');
				
					$sql="UPDATE #__wel_venparm SET ".
							"wel_ven_part='$this->wel_ven_part',".
							"wel_ven_part_des='$this->wel_ven_part_des', ".
							"wel_pur_unit='$this->wel_pur_unit',".
							"wel_pur_unit_rate='$this->wel_pur_unit_rate',". 
							"wel_min_ord='$this->wel_min_ord',".
							"wel_pkg_ord='$this->wel_pkg_ord',".
							"wel_lead_tm='$this->wel_lead_tm'".
						"WHERE wel_ven_code='$this->wel_ven_code' and wel_part_no='$this->wel_part_no'";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
					mysql_free_result($result);
					
				mysql_query("commit");
				
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}
			
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		if($msg_code==""){$msg_code="edit_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
}
?>
