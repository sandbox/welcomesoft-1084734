<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>

<?php
class purm004_cls
{
	public $wel_pattern="";
	public $wel_po_nextno='';
	public $wel_pat_des="";
	
	private $wel_prog_code="purm004";

	public function read()
	{
		$msg_code="";
		$return_val=array();
		 
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_read")){throw new Exception("unauthorized_access");}
			
			$sql="SELECT * FROM #__wel_gentpow WHERE wel_pattern='$this->wel_pattern' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_pattern_not_found");}
			$int__count=0;
			while ($int__count < mysql_num_fields($result)) 
			{
				$field_name=mysql_fetch_field($result,$int__count)->name;
				$return_val[$field_name]=$row[$field_name];
				$int__count++;
			}
			mysql_free_result($result);
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	public function addnew()
	{
		$msg_code="";
		$return_val=array(); 
		
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_addnew")){throw new Exception("unauthorized_access");}
			
			if ($this->wel_pattern==""){throw new Exception("wel_pattern_miss");}
			if (strlen($this->wel_pattern)<2 || strlen($this->wel_pattern)>4){throw new Exception("wel_pattern_length");}
			if($this->wel_pat_des==""){throw new Exception("wel_pat_des_miss");}
			if(!is_numeric($this->wel_po_nextno)){throw new Exception("wel_po_nextno_not_num");}
			$this->wel_po_nextno=intval($this->wel_po_nextno);
			
			//判断模式码是否存在
			$sql="SELECT * FROM #__wel_gentpow WHERE wel_pattern='$this->wel_pattern' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
			if($row=mysql_fetch_array($result)){throw new Exception("wel_pattern_exist");}
			
			try
			{
				mysql_query("begin");
				
					$sql="INSERT INTO #__wel_gentpow SET ".
							"wel_pattern='$this->wel_pattern',".
							"wel_pat_des='$this->wel_pat_des',".
							"wel_po_nextno='$this->wel_po_nextno',".
							"wel_ppo_no_yn='0'";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					
				mysql_query("commit");
				
			}
			catch(Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}		
			
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		if($msg_code==""){$msg_code="addnew_succee";}
		$return_val["msg_code"]=$msg_code;
		$return_val["wel_pattern"]=$this->wel_pattern;
		return $return_val;
	}
	
	public function edit()
	{
		$msg_code="";
		$return_val=array();
 
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_edit")){throw new Exception("unauthorized_access");}
			
			if($this->wel_pattern==""){throw new Exception("wel_pattern_miss");}
			if($this->wel_pat_des==""){throw new Exception("wel_pat_des_miss");}
			if(!is_numeric($this->wel_po_nextno)){throw new Exception("wel_po_nextno_not_num");}
			$this->wel_po_nextno=intval($this->wel_po_nextno);
			
			//判断模式码是否存在
			$sql="SELECT * FROM #__wel_gentpow WHERE wel_pattern='$this->wel_pattern' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_pattern_not_found");}
			$int__po_nextno=intval(is_null($row["wel_po_nextno"]) ? 0 : $row["wel_po_nextno"]);
			
			if($int__po_nextno>$this->wel_po_nextno){throw new Exception("wel_po_nextno_error");}
		
			try
			{
				mysql_query("begin");
				
					$sql="UPDATE #__wel_gentpow SET ".
							"wel_pat_des='$this->wel_pat_des',".
							"wel_po_nextno='$this->wel_po_nextno'".
						"WHERE wel_pattern='$this->wel_pattern' LIMIT 1";	
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					
				mysql_query("commit");
				
			}
			catch(Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}		
			
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}		
		if($msg_code==""){$msg_code="edit_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	public function delete()
	{
		$msg_code="";
		$return_val=array();
 
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_delete")){throw new Exception("unauthorized_access");}
			
			//判断模式码是否存在
			$sql="SELECT * FROM #__wel_gentpow WHERE wel_pattern='$this->wel_pattern' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_pattern_not_found");}
			
			try
			{
			
				mysql_query("begin");
			
					$sql="DELETE FROM #__wel_gentpow WHERE wel_pattern='$this->wel_pattern' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					
				mysql_query("commit");
				
			}
			catch(Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}		
				
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}		
		if($msg_code==""){$msg_code="delete_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
}
?>
