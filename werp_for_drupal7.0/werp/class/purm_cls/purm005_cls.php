<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>

<?php
class purm005_cls
{
	public $wel_po_no;
	public $wel_pattern;
	public $wel_comp_code;
	public $wel_ven_code;
	public $wel_ven_des;
	public $wel_buyer_code;
	public $wel_cur_code;
	public $wel_ex_rate;
	public $wel_req_date;
			
	public $wel_ven_term;
	public $wel_ven_pbase;
	public $wel_ven_svia;
	public $wel_dely_code;

	public $wel_dis_type;
	public $wel_dis_rate;
	public $wel_tax_type;
	public $wel_pr_no;

	public $wel_po_remark;

	public $wel_crt_user;
	public $wel_crt_date;
	public $wel_upd_user;
	public $wel_upd_date;

	public $wel_part_no;
	public $wel_po_line;	
	public $wel_line_no;    // 杂项的Line No
	
	private $wel_prog_code="purm005";

	public function read()
	{
		$msg_code="";
		$return_val=array();

		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_read")){throw new Exception("unauthorized_access");}
			
			$sql="SELECT r.*, ".
						"v.wel_ven_des, ".
						"v.wel_ven_des1, ".
						"v.wel_abbre_des, ".
						"v.wel_ven_add1, ".
						"v.wel_ven_add2, ".
						"v.wel_ven_add3, ".
						"v.wel_ven_add4, ".
						"v.wel_ven_email, ".
						"v.wel_ven_cont, ".
						"v.wel_ven_tele, ".
						"v.wel_ven_tele1, ".
						"v.wel_ven_fax, ".
						"v.wel_ven_fax1, ".
						"b.wel_type_des as wel_ven_term_des, ".
						"c.wel_pbase_name as wel_ven_pbase_des, ".
						"d.wel_svia_des as wel_ven_svia_des, ".
						"e.wel_dely_des as wel_dely_des ".
				"FROM #__wel_porhdrm r ".
					"LEFT JOIN #__wel_venmasm v ON r.wel_ven_code=v.wel_ven_code ".
					"LEFT JOIN #__wel_portyem b ON r.wel_ven_term=b.wel_type_code ".
					"LEFT JOIN #__wel_pbasefm c ON r.wel_ven_pbase=c.wel_pbase_code ".
					"LEFT JOIN #__wel_shpviam d ON r.wel_ven_svia=d.wel_svia_code ".
					"LEFT JOIN #__wel_delytom e ON r.wel_dely_code=e.wel_dely_code ".
				"WHERE r.wel_po_no='$this->wel_po_no' LIMIT 1"; 
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_po_no_not_found");}
			$int__count=0;
			while ($int__count < mysql_num_fields($result)) 
			{
				$field_name=mysql_fetch_field($result,$int__count)->name;
				$return_val[$field_name]=$row[$field_name];
				$int__count++;
			}
			mysql_free_result($result);
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	public function get_po_amt()
	{
		$msg_code="";
		$return_val=array();

		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_read")){throw new Exception("unauthorized_access");}
			
			$sql="SELECT wel_item_amt,(wel_dis_amt+wel_dis_amt2) as wel_dis_amt,wel_tax_amt,wel_order_amt ".
				"FROM #__wel_porhdrm ".
				"WHERE wel_po_no='".$this->wel_po_no."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_po_no_not_found");}
			
			$return_val["wel_item_amt"]=doubleval(is_null($row["wel_item_amt"]) ? 0 : $row["wel_item_amt"]);
			$return_val["wel_dis_amt"]=doubleval(is_null($row["wel_dis_amt"]) ? 0 : $row["wel_dis_amt"]);
			$return_val["wel_tax_amt"]=doubleval(is_null($row["wel_tax_amt"]) ? 0 : $row["wel_tax_amt"]);
			$return_val["wel_order_amt"]=doubleval(is_null($row["wel_order_amt"]) ? 0 : $row["wel_order_amt"]);
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	public function addnew()
	{
		$msg_code="";
		$return_val=array();
		
		try
		{
			$conn= werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_addnew")){throw new Exception("unauthorized_access");}
			
			if ($this->wel_po_no=="" && $this->wel_pattern==""){throw new Exception("wel_po_no_miss");}
			
			if($this->wel_comp_code==""){throw new Exception("wel_comp_code_miss");}
			if($this->wel_ven_code==""){throw new Exception("wel_ven_code_miss");}
			if($this->wel_cur_code==""){throw new Exception("wel_cur_code_miss");}
			if($this->wel_req_date==""){throw new Exception("wel_req_date_miss");}
//			if($this->wel_dely_code==""){throw new Exception("wel_dely_code_miss");}
//			if($this->wel_ven_term==""){throw new Exception("wel_ven_term_miss");}
//			if($this->wel_buyer_code==""){throw new Exception("wel_buyer_code_miss");}
			$this->wel_req_date=(is_null($this->wel_req_date) ? "null" : "'".$this->wel_req_date."'");
			if(!is_numeric($this->wel_ex_rate)){$this->wel_ex_rate=1;}
			$this->wel_ex_rate=doubleval($this->wel_ex_rate);
			if(!is_numeric($this->wel_tax_rate)){$this->wel_tax_rate=1;}
			$this->wel_tax_rate=doubleval($this->wel_tax_rate);
			
			if($this->wel_pattern!="")
			{
				$sql="SELECT wel_pattern FROM #__wel_gentpow WHERE wel_pattern='".$this->wel_pattern."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_pattern_not_found");}
			}
			if($this->wel_ven_code!="")
			{
				$sql="SELECT wel_ven_code FROM #__wel_venmasm WHERE wel_ven_code='".$this->wel_ven_code."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_ven_code_not_found");}
			}
			if($this->wel_comp_code!="")
			{
				$sql="SELECT wel_comp_code FROM #__wel_compflm WHERE wel_comp_code='".$this->wel_comp_code."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_comp_code_not_found");}
			}
			if($this->wel_cur_code!="")
			{
				$sql="SELECT wel_cur_code,wel_ex_rate FROM #__wel_currenm WHERE wel_cur_code='".$this->wel_cur_code."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_cur_code_not_found");}
				$this->wel_ex_rate=doubleval($row["wel_ex_rate"]);
			}
			if($this->wel_ven_term!="")
			{
				$sql="SELECT wel_type_code FROM #__wel_portyem WHERE wel_type_code='".$this->wel_ven_term."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_ven_term_not_found");}
			}
			if($this->wel_ven_pbase!="")
			{
				$sql="SELECT wel_pbase_code FROM #__wel_pbasefm WHERE wel_pbase_code='".$this->wel_ven_pbase."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_ven_pbase_not_found");}
			}
			if($this->wel_ven_svia!="")
			{
				$sql="SELECT wel_svia_code FROM #__wel_shpviam WHERE wel_svia_code='".$this->wel_ven_svia."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_ven_svia_not_found");}
			}
			if($this->wel_dely_code!="")
			{
				$sql="SELECT wel_dely_code FROM #__wel_delytom WHERE wel_dely_code='".$this->wel_dely_code."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_dely_code_not_found");}
			}
			if($this->wel_buyer_code!="")
			{
				$sql="SELECT wel_buyer_code FROM #__wel_purhanm WHERE wel_buyer_code='".$this->wel_buyer_code."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_buyer_code_not_found");}
			}

			try
			{

				mysql_query("begin");
				
					if ($this->wel_pattern!="")
					{
						$sql="SELECT * FROM #__wel_gentpow WHERE wel_pattern='$this->wel_pattern' LIMIT 1";
						$sql=revert_to_the_available_sql($sql);
						if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
						if(!($row=mysql_fetch_array($result))){throw new Exception("wel_pattern_not_found");}
						
						$wel_pattern=stripslashes(trim($row["wel_pattern"]));
						$wel_po_nextno=trim($row["wel_po_nextno"]);
						$wel_po_nextno=sprintf("%'08s",$wel_po_nextno);

						if(strlen($wel_po_nextno)>8){throw new Exception(wel_pattern_overflow);}
						$this->wel_po_no=$wel_pattern.$wel_po_nextno;

						$sql="UPDATE #__wel_gentpow set wel_po_nextno=IFNULL(wel_po_nextno,0)+1 WHERE wel_pattern='$this->wel_pattern'";
						$sql=revert_to_the_available_sql($sql);
						if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					}
				
					$sql="SELECT * FROM #__wel_porhdrm WHERE wel_po_no='$this->wel_po_no' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
					if($row=mysql_fetch_array($result)){throw new Exception("wel_po_no_exist");}
					
					$sql="INSERT INTO #__wel_porhdrm SET ".
								"wel_po_no='$this->wel_po_no',".
								"wel_pattern='$this->wel_pattern',".
								"wel_comp_code='$this->wel_comp_code',".
								"wel_ven_code='$this->wel_ven_code',".
								"wel_buyer_code='$this->wel_buyer_code',".
								"wel_cur_code='$this->wel_cur_code',".
								"wel_ex_rate='$this->wel_ex_rate',".
								"wel_req_date=".$this->wel_req_date.",".
								"wel_ven_term='$this->wel_ven_term',".
								"wel_ven_pbase='$this->wel_ven_pbase',".
								"wel_dely_code='$this->wel_dely_code',".
								"wel_ven_svia='$this->wel_ven_svia',".
								"wel_dis_type='$this->wel_dis_type',".
								"wel_dis_rate='$this->wel_dis_rate',".
								"wel_tax_type='$this->wel_tax_type',".
								"wel_pr_no='$this->wel_pr_no',".
								"wel_po_remark='$this->wel_po_remark',".
								"wel_crt_user='{$_SESSION['wel_user_id']}',".
								"wel_crt_date=now(),".
								"wel_cancel_yn=0,".
								"wel_appr_yn='0'";	 
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
			
				mysql_query("commit");
			
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}
			
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		if($msg_code==""){$msg_code="addnew_succee";}
		$return_val["msg_code"]=$msg_code;
		$return_val["wel_po_no"]=$this->wel_po_no;
		return $return_val;
	}
	
	public function edit()
	{
		$msg_code="";
		$return_val=array();
		 
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_edit")){throw new Exception("unauthorized_access");}
			
			if($this->wel_comp_code==""){throw new Exception("wel_comp_code_miss");}
			if($this->wel_ven_code==""){throw new Exception("wel_ven_code_miss");}
			if($this->wel_cur_code==""){throw new Exception("wel_cur_code_miss");}
			if($this->wel_req_date==""){throw new Exception("wel_req_date_miss");}
			$this->wel_req_date=(is_null($this->wel_req_date) ? "null" : "'".$this->wel_req_date."'");
//			if($this->wel_buyer_code==""){throw new Exception("wel_buyer_code_miss");}
//			if($this->wel_dely_code==""){throw new Exception("wel_dely_code_miss");}
//			if($this->wel_ven_term==""){throw new Exception("wel_ven_term_miss");}
			if(!is_numeric($this->wel_ex_rate)){$this->wel_ex_rate=0;}
			$this->wel_ex_rate=doubleval($this->wel_ex_rate);
			if(!is_numeric($this->wel_tax_rate)){$this->wel_tax_rate=0;}
			$this->wel_tax_rate=doubleval($this->wel_tax_rate);
			
			$sql="SELECT * FROM #__wel_porhdrm WHERE wel_po_no='$this->wel_po_no' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_po_no_not_found");}

			if($this->wel_ven_code!="")
			{
				$sql="SELECT wel_ven_code FROM #__wel_venmasm WHERE wel_ven_code='".$this->wel_ven_code."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_ven_code_not_found");}
			}
			if($this->wel_comp_code!="")
			{
				$sql="SELECT wel_comp_code FROM #__wel_compflm WHERE wel_comp_code='".$this->wel_comp_code."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_comp_code_not_found");}
			}
			if($this->wel_cur_code!="")
			{
				$sql="SELECT wel_cur_code,wel_ex_rate FROM #__wel_currenm WHERE wel_cur_code='".$this->wel_cur_code."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_cur_code_not_found");}
				$this->wel_ex_rate=doubleval($row["wel_ex_rate"]);
			}
			if($this->wel_ven_term!="")
			{
				$sql="SELECT wel_type_code FROM #__wel_portyem WHERE wel_type_code='".$this->wel_ven_term."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_ven_term_not_found");}
			}
			if($this->wel_ven_pbase!="")
			{
				$sql="SELECT wel_pbase_code FROM #__wel_pbasefm WHERE wel_pbase_code='".$this->wel_ven_pbase."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_ven_pbase_not_found");}
			}
			if($this->wel_ven_svia!="")
			{
				$sql="SELECT wel_svia_code FROM #__wel_shpviam WHERE wel_svia_code='".$this->wel_ven_svia."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_ven_svia_not_found");}
			}
			if($this->wel_dely_code!="")
			{
				$sql="SELECT wel_dely_code FROM #__wel_delytom WHERE wel_dely_code='".$this->wel_dely_code."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_dely_code_not_found");}
			}
			if($this->wel_buyer_code!="")
			{
				$sql="SELECT wel_buyer_code FROM #__wel_purhanm WHERE wel_buyer_code='".$this->wel_buyer_code."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_buyer_code_not_found");}
			}

			try
			{
				mysql_query("begin");
				
					$sql="UPDATE #__wel_porhdrm set ".
							 "wel_comp_code='$this->wel_comp_code',".
							 "wel_ven_code='$this->wel_ven_code',".
							 "wel_req_date=".$this->wel_req_date.",".
							 "wel_cur_code='$this->wel_cur_code',".
							 "wel_ex_rate='$this->wel_ex_rate',".
							 "wel_ven_term='$this->wel_ven_term',".
							 "wel_ven_pbase='$this->wel_ven_pbase',".
							 "wel_ven_svia='$this->wel_ven_svia',".
							 "wel_dely_code='$this->wel_dely_code',".
							 "wel_buyer_code='$this->wel_buyer_code',".
							 "wel_dis_type='$this->wel_dis_type',".
							 "wel_dis_rate='$this->wel_dis_rate',".
							 "wel_tax_type='$this->wel_tax_type',".
							 "wel_pr_no='$this->wel_pr_no',".
							 "wel_upd_user='{$_SESSION['wel_user_id']}',".
							 "wel_upd_date=now(),".
							 "wel_po_remark='$this->wel_po_remark' ".
						 "WHERE wel_po_no='$this->wel_po_no'";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				
					$sql="UPDATE #__wel_pordetm SET ".
						"wel_ven_code='$this->wel_ven_code' ".
						"WHERE wel_po_no='$this->wel_po_no'";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}

					if ($this->wel_dis_type==0 or $this->wel_dis_type==2 or $this->wel_dis_type==5)
					{
						$sql="UPDATE #__wel_pordetm SET ".
							"wel_dis_amt2 = 0 ".
							"WHERE wel_po_no='$this->wel_po_no'";
						$sql=revert_to_the_available_sql($sql);
						if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					}
					if ($this->wel_dis_type==1 or $this->wel_dis_type==3)
					{
						$sql="UPDATE #__wel_pordetm SET wel_dis_amt2 = CASE ".
							"WHEN wel_dis_rate > 0 THEN 0 ".
							"ELSE ROUND((wel_item_amt-wel_dis_amt)*'$this->wel_dis_rate'/100,2) END ".
							"WHERE wel_po_no='$this->wel_po_no'";
						$sql=revert_to_the_available_sql($sql);
						if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					}
					if ($this->wel_dis_type==4 or $this->wel_dis_type==6 )
					{
						$sql="UPDATE #__wel_pordetm SET ".
							"wel_dis_amt2 = ROUND((wel_item_amt-wel_dis_amt)*'$this->wel_dis_rate'/100,2) ".
							"WHERE wel_po_no='$this->wel_po_no'";
						$sql=revert_to_the_available_sql($sql);
						if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					}

					// 價外稅
					if ( $this->wel_tax_type==2 )
					{
						$sql="UPDATE #__wel_pordetm SET ".
							"wel_tax_amt = ROUND((wel_item_amt-wel_dis_amt-wel_dis_amt2)*wel_tax_rate/100,2), ".
							"wel_line_amt = ROUND((wel_item_amt-wel_dis_amt-wel_dis_amt2)*(1+wel_tax_rate/100),2) ".
							"WHERE wel_po_no='$this->wel_po_no'";
						$sql=revert_to_the_available_sql($sql);
						if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					}
					// 價內稅
					if ( $this->wel_tax_type==1 )
					{
						$sql="UPDATE #__wel_pordetm SET ".
							"wel_tax_amt = ROUND((wel_item_amt-wel_dis_amt-wel_dis_amt2)*wel_tax_rate/(100+wel_tax_rate),2), ".
							"wel_line_amt = ROUND((wel_item_amt-wel_dis_amt-wel_dis_amt2),2) ".
							"WHERE wel_po_no='$this->wel_po_no'";
						$sql=revert_to_the_available_sql($sql);
						if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					}
					if ( $this->wel_tax_type==0 )
					{
						$sql="UPDATE #__wel_pordetm SET ".
							"wel_tax_amt = 0, ".
							"wel_line_amt = ROUND((wel_item_amt-wel_dis_amt-wel_dis_amt2),2) ".
							"WHERE wel_po_no='$this->wel_po_no'";
						$sql=revert_to_the_available_sql($sql);
						if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					}

					// P/O MISC.
					if ($this->wel_dis_type==0 or $this->wel_dis_type==1 or $this->wel_dis_type==4)
					{
						$sql="UPDATE #__wel_pormism SET ".
							"wel_dis_amt2 = 0 ".
							"WHERE wel_po_no='$this->wel_po_no'";
						$sql=revert_to_the_available_sql($sql);
						if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					}
					if ($this->wel_dis_type==2 or $this->wel_dis_type==3)
					{
						$sql="UPDATE #__wel_pormism SET wel_dis_amt2 = CASE ".
							"WHEN wel_dis_rate > 0 THEN 0 ".
							"ELSE ROUND((wel_item_amt-wel_dis_amt)*'$this->wel_dis_rate'/100,2) END ".
							"WHERE wel_po_no='$this->wel_po_no'";
						$sql=revert_to_the_available_sql($sql);
						if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					}
					if ($this->wel_dis_type==5 or $this->wel_dis_type==6 )
					{
						$sql="UPDATE #__wel_pormism SET ".
							"wel_dis_amt2 = ROUND((wel_item_amt-wel_dis_amt)*'$this->wel_dis_rate'/100,2) ".
							"WHERE wel_po_no='$this->wel_po_no'";
						$sql=revert_to_the_available_sql($sql);
						if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					}
					// 價外稅
					if ( $this->wel_tax_type==2 )
					{
						$sql="UPDATE #__wel_pormism SET ".
							"wel_tax_amt = ROUND((wel_item_amt-wel_dis_amt-wel_dis_amt2)*wel_tax_rate/100,2), ".
							"wel_line_amt = ROUND((wel_item_amt-wel_dis_amt-wel_dis_amt2)*(1+wel_tax_rate/100),2) ".
							"WHERE wel_po_no='$this->wel_po_no'";
						$sql=revert_to_the_available_sql($sql);
						if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					}
					// 價內稅
					if ( $this->wel_tax_type==1 )
					{
						$sql="UPDATE #__wel_pormism SET ".
							"wel_tax_amt = ROUND((wel_item_amt-wel_dis_amt-wel_dis_amt2)*wel_tax_rate/(100+wel_tax_rate),2), ".
							"wel_line_amt = ROUND((wel_item_amt-wel_dis_amt-wel_dis_amt2),2) ".
							"WHERE wel_po_no='$this->wel_po_no'";
						$sql=revert_to_the_available_sql($sql);
						if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					}
					if ( $this->wel_tax_type==0 )
					{
						$sql="UPDATE #__wel_pormism SET ".
							"wel_tax_amt = 0, ".
							"wel_line_amt = ROUND((wel_item_amt-wel_dis_amt-wel_dis_amt2),2) ".
							"WHERE wel_po_no='$this->wel_po_no'";
						$sql=revert_to_the_available_sql($sql);
						if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					}

					// Update PORHDRM discount & Tax
					$tmp_item_amt=0;
					$tmp_dis_amt=0;
					$tmp_dis_amt2=0;
					$tmp_tax_amt=0;
					$tmp_line_amt=0;
					$sql="SELECT ".
						"SUM(wel_item_amt) as s1,".
						"SUM(wel_dis_amt) as s2,".
						"sum(wel_dis_amt2) as s3,".
						"SUM(wel_tax_amt) as s4,".
						"SUM(wel_line_amt) as s5 ".
						"FROM #__wel_pordetm ".
						"WHERE wel_po_no='$this->wel_po_no' ";
					$sql=revert_to_the_available_sql($sql);
					if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
					if( $row=mysql_fetch_array($result) )
					{
						$tmp_item_amt=doubleval(is_null($row["s1"]) ? 0 : $row["s1"]);
						$tmp_dis_amt =doubleval(is_null($row["s2"]) ? 0 : $row["s2"]);
						$tmp_dis_amt2=doubleval(is_null($row["s3"]) ? 0 : $row["s3"]);
						$tmp_tax_amt =doubleval(is_null($row["s4"]) ? 0 : $row["s4"]);
						$tmp_line_amt=doubleval(is_null($row["s5"]) ? 0 : $row["s5"]);
					}

					$sql="SELECT ".
						"SUM(wel_item_amt) as s1,".
						"SUM(wel_dis_amt) as s2,".
						"sum(wel_dis_amt2) as s3,".
						"SUM(wel_tax_amt) as s4,".
						"SUM(wel_line_amt) as s5 ".
						"FROM #__wel_pormism ".
						"WHERE wel_po_no='$this->wel_po_no' ";
					$sql=revert_to_the_available_sql($sql);
					if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
					if( $row=mysql_fetch_array($result) )
					{
						$tmp_item_amt=$tmp_item_amt + doubleval(is_null($row["s1"]) ? 0 : $row["s1"]);
						$tmp_dis_amt =$tmp_dis_amt  + doubleval(is_null($row["s2"]) ? 0 : $row["s2"]);
						$tmp_dis_amt2=$tmp_dis_amt2 + doubleval(is_null($row["s3"]) ? 0 : $row["s3"]);
						$tmp_tax_amt =$tmp_tax_amt  + doubleval(is_null($row["s4"]) ? 0 : $row["s4"]);
						$tmp_line_amt=$tmp_line_amt + doubleval(is_null($row["s5"]) ? 0 : $row["s5"]);
					}

					$sql="UPDATE #__wel_porhdrm SET ".
						"wel_item_amt='$tmp_item_amt',".
						"wel_dis_amt='$tmp_dis_amt',".
						"wel_dis_amt2='$tmp_dis_amt2',".
						"wel_tax_amt='$tmp_tax_amt',".
						"wel_order_amt='$tmp_line_amt', ".
						"wel_upd_user='{$_SESSION['wel_user_id']}',".
						"wel_upd_date=now() ".
						"WHERE wel_po_no='$this->wel_po_no'";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}

				mysql_query("commit");

			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}
			
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		if($msg_code==""){$msg_code="edit_succee";}		
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	public function delete()
	{
		$msg_code="";
		$return_val=array();
		 
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_delete")){throw new Exception("unauthorized_access");}
			
			$sql="SELECT * FROM #__wel_porhdrm WHERE wel_po_no='$this->wel_po_no' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}	
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_po_no_not_found");}
			
			try
			{
				mysql_query("begin");
				
					##删除采购单细节=============================================================
					$sql="SELECT * FROM #__wel_pordetm WHERE wel_po_no='$this->wel_po_no'";
					$sql=revert_to_the_available_sql($sql);
					if(!$pordetm_result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					while($pordetm=mysql_fetch_assoc($pordetm_result))
					{
						$dec__req_qty=doubleval(is_null($pordetm["wel_req_qty"]) ? 0 : $pordetm["wel_req_qty"]);
						$dec__os_qty=doubleval(is_null($pordetm["wel_os_qty"]) ? 0 : $pordetm["wel_os_qty"]);
						
						$dec_ship_qty=$dec__req_qty-$dec__os_qty;
						if($dec_ship_qty>0){throw new Exception("ship_qty_error");}
					}
					
					$sql="DELETE FROM #__wel_pordetm WHERE wel_po_no='$this->wel_po_no'";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					##=======================================================================
					
					##删除杂项=================================================================
					$sql="DELETE FROM #__wel_pormism WHERE wel_po_no='$this->wel_po_no'";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					##========================================================================
					
					##删除主表=================================================================
					$sql="DELETE FROM #__wel_porhdrm WHERE wel_po_no='$this->wel_po_no' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					##========================================================================
				
				mysql_query("commit"); 		
				
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage()); 
			}
			
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		if($msg_code==""){$msg_code="delete_succee";}		
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	public function approve()
	{
		$msg_code="";
		$return_val=array();

		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_approve")){throw new Exception("unauthorized_access");}
			
			$sql="SELECT * FROM #__wel_porhdrm WHERE wel_po_no='$this->wel_po_no' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_po_no_not_found");}
			
			try
			{
				mysql_query("begin");
				
					$sql="UPDATE #__wel_porhdrm SET ".
						 "wel_appr_yn=1,".
						 "wel_appr_by='{$_SESSION['wel_user_id']}',".
						 "wel_appr_date=now()".
						 " WHERE wel_po_no='$this->wel_po_no' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				
				mysql_query("commit");
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage()); 
			}
			
		} 
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		if($msg_code==""){$msg_code="approve_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;         
	}
	
	public function not_approve()
	{
		$msg_code="";
		$return_val=array();

		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_approve")){throw new Exception("unauthorized_access");}
			
			try
			{
				mysql_query("begin");
				
					$sql="UPDATE #__wel_porhdrm SET ".
							 "wel_appr_yn=0,".
							 "wel_appr_by='{$_SESSION['wel_user_id']}',".
							 "wel_appr_date=now() ".
						 "WHERE wel_po_no='$this->wel_po_no'";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				
				mysql_query("commit");
					
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage()); 
			}
				
		} 
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		if($msg_code==""){$msg_code="not_approve_succee";}	
		$return_val["msg_code"]=$msg_code;
		return $return_val;  
	}

	public function delete_detail_tab0()
	{
		$msg_code="";
		$return_val=array();
 
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_delete")){throw new Exception("unauthorized_access");}
			
			$sql="SELECT * FROM #__wel_pordetm WHERE wel_po_no='$this->wel_po_no' AND wel_po_line='$this->wel_po_line' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_po_line_not_found");}
			$dec__req_qty=doubleval(is_null($row["wel_req_qty"]) ? 0 : $row["wel_req_qty"]);
			$dec__os_qty=doubleval(is_null($row["wel_os_qty"]) ? 0 : $row["wel_os_qty"]);
			$dec_ship_qty=$dec__req_qty-$dec__os_qty;
		
			if($dec_ship_qty>0){throw new Exception("ship_qty_error");}
			
			try
			{
				mysql_query("begin");
				
					$sql="DELETE FROM #__wel_pordetm WHERE wel_po_no='$this->wel_po_no' AND wel_po_line='$this->wel_po_line' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				
					// Update PORHDRM discount & Tax
					$tmp_item_amt=0;
					$tmp_dis_amt=0;
					$tmp_dis_amt2=0;
					$tmp_tax_amt=0;
					$tmp_line_amt=0;
					$sql="SELECT ".
						"SUM(wel_item_amt) as s1,".
						"SUM(wel_dis_amt) as s2,".
						"sum(wel_dis_amt2) as s3,".
						"SUM(wel_tax_amt) as s4,".
						"SUM(wel_line_amt) as s5 ".
						"FROM #__wel_pordetm ".
						"WHERE wel_po_no='$this->wel_po_no' ";
					$sql=revert_to_the_available_sql($sql);
					if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
					if( $row=mysql_fetch_array($result) )
					{
						$tmp_item_amt=doubleval(is_null($row["s1"]) ? 0 : $row["s1"]);
						$tmp_dis_amt =doubleval(is_null($row["s2"]) ? 0 : $row["s2"]);
						$tmp_dis_amt2=doubleval(is_null($row["s3"]) ? 0 : $row["s3"]);
						$tmp_tax_amt =doubleval(is_null($row["s4"]) ? 0 : $row["s4"]);
						$tmp_line_amt=doubleval(is_null($row["s5"]) ? 0 : $row["s5"]);
					}

					$sql="SELECT ".
						"SUM(wel_item_amt) as s1,".
						"SUM(wel_dis_amt) as s2,".
						"sum(wel_dis_amt2) as s3,".
						"SUM(wel_tax_amt) as s4,".
						"SUM(wel_line_amt) as s5 ".
						"FROM #__wel_pormism ".
						"WHERE wel_po_no='$this->wel_po_no' ";
					$sql=revert_to_the_available_sql($sql);
					if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
					if( $row=mysql_fetch_array($result) )
					{
						$tmp_item_amt=$tmp_item_amt + doubleval(is_null($row["s1"]) ? 0 : $row["s1"]);
						$tmp_dis_amt =$tmp_dis_amt  + doubleval(is_null($row["s2"]) ? 0 : $row["s2"]);
						$tmp_dis_amt2=$tmp_dis_amt2 + doubleval(is_null($row["s3"]) ? 0 : $row["s3"]);
						$tmp_tax_amt =$tmp_tax_amt  + doubleval(is_null($row["s4"]) ? 0 : $row["s4"]);
						$tmp_line_amt=$tmp_line_amt + doubleval(is_null($row["s5"]) ? 0 : $row["s5"]);
					}

					$sql="UPDATE #__wel_porhdrm SET ".
						"wel_item_amt='$tmp_item_amt',".
						"wel_dis_amt='$tmp_dis_amt',".
						"wel_dis_amt2='$tmp_dis_amt2',".
						"wel_tax_amt='$tmp_tax_amt',".
						"wel_order_amt='$tmp_line_amt', ".
						"wel_upd_user='{$_SESSION['wel_user_id']}',".
						"wel_upd_date=now() ".
						"WHERE wel_po_no='$this->wel_po_no'";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}

				mysql_query("commit");
				
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage()); 
			}
			
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		if($msg_code==""){$msg_code="delete_succee";}			
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}

	public function delete_detail_tab0_all()
	{
		$msg_code="";
		$return_val=array();
		 
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_delete")){throw new Exception("unauthorized_access");}
				
			$sql="SELECT * FROM #__wel_pordetm WHERE wel_po_no='$this->wel_po_no'";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
			while($row=mysql_fetch_array($result))
			{
				$dec__req_qty=doubleval(is_null($row["wel_req_qty"]) ? 0 : $row["wel_req_qty"]);
				$dec__os_qty=doubleval(is_null($row["wel_os_qty"]) ? 0 : $row["wel_os_qty"]);
				$dec_ship_qty=$dec__req_qty-$dec__os_qty;
				if($dec_ship_qty>0){throw new Exception("ship_qty_error");}
			}
			
			try
			{
				mysql_query("begin");
				
					$sql="DELETE FROM #__wel_pordetm WHERE wel_po_no='$this->wel_po_no'";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					
					// Update PORHDRM discount & Tax
					$tmp_item_amt=0;
					$tmp_dis_amt=0;
					$tmp_dis_amt2=0;
					$tmp_tax_amt=0;
					$tmp_line_amt=0;
					$sql="SELECT ".
						"SUM(wel_item_amt) as s1,".
						"SUM(wel_dis_amt) as s2,".
						"sum(wel_dis_amt2) as s3,".
						"SUM(wel_tax_amt) as s4,".
						"SUM(wel_line_amt) as s5 ".
						"FROM #__wel_pormism ".
						"WHERE wel_po_no='$this->wel_po_no' ";
					$sql=revert_to_the_available_sql($sql);
					if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
					if( $row=mysql_fetch_array($result) )
					{
						$tmp_item_amt=$tmp_item_amt + doubleval(is_null($row["s1"]) ? 0 : $row["s1"]);
						$tmp_dis_amt =$tmp_dis_amt  + doubleval(is_null($row["s2"]) ? 0 : $row["s2"]);
						$tmp_dis_amt2=$tmp_dis_amt2 + doubleval(is_null($row["s3"]) ? 0 : $row["s3"]);
						$tmp_tax_amt =$tmp_tax_amt  + doubleval(is_null($row["s4"]) ? 0 : $row["s4"]);
						$tmp_line_amt=$tmp_line_amt + doubleval(is_null($row["s5"]) ? 0 : $row["s5"]);
					}

					$sql="UPDATE #__wel_porhdrm SET ".
						"wel_item_amt='$tmp_item_amt',".
						"wel_dis_amt='$tmp_dis_amt',".
						"wel_dis_amt2='$tmp_dis_amt2',".
						"wel_tax_amt='$tmp_tax_amt',".
						"wel_order_amt='$tmp_line_amt', ".
						"wel_upd_user='{$_SESSION['wel_user_id']}',".
						"wel_upd_date=now() ".
						"WHERE wel_po_no='$this->wel_po_no'";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					
				mysql_query("commit");
				
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage()); 
			}	
				
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}		
		if($msg_code==""){$msg_code="delete_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	public function delete_detail_tab1()
	{
		$msg_code="";
		$return_val=array();
		 
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_delete")){throw new Exception("unauthorized_access");}
			
			$sql="SELECT * FROM #__wel_pormism WHERE wel_po_no='$this->wel_po_no' and wel_line_no='$this->wel_line_no' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_po_line_not_found");}
			
			try
			{
				mysql_query("begin");
				
					$sql="DELETE FROM #__wel_pormism WHERE wel_po_no='$this->wel_po_no' AND wel_line_no='$this->wel_line_no'";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				
					// Update PORHDRM discount & Tax
					$tmp_item_amt=0;
					$tmp_dis_amt=0;
					$tmp_dis_amt2=0;
					$tmp_tax_amt=0;
					$tmp_line_amt=0;
					$sql="SELECT ".
						"SUM(wel_item_amt) as s1,".
						"SUM(wel_dis_amt) as s2,".
						"sum(wel_dis_amt2) as s3,".
						"SUM(wel_tax_amt) as s4,".
						"SUM(wel_line_amt) as s5 ".
						"FROM #__wel_pordetm ".
						"WHERE wel_po_no='$this->wel_po_no' ";
					$sql=revert_to_the_available_sql($sql);
					if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
					if( $row=mysql_fetch_array($result) )
					{
						$tmp_item_amt=doubleval(is_null($row["s1"]) ? 0 : $row["s1"]);
						$tmp_dis_amt =doubleval(is_null($row["s2"]) ? 0 : $row["s2"]);
						$tmp_dis_amt2=doubleval(is_null($row["s3"]) ? 0 : $row["s3"]);
						$tmp_tax_amt =doubleval(is_null($row["s4"]) ? 0 : $row["s4"]);
						$tmp_line_amt=doubleval(is_null($row["s5"]) ? 0 : $row["s5"]);
					}

					$sql="SELECT ".
						"SUM(wel_item_amt) as s1,".
						"SUM(wel_dis_amt) as s2,".
						"sum(wel_dis_amt2) as s3,".
						"SUM(wel_tax_amt) as s4,".
						"SUM(wel_line_amt) as s5 ".
						"FROM #__wel_pormism ".
						"WHERE wel_po_no='$this->wel_po_no' ";
					$sql=revert_to_the_available_sql($sql);
					if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
					if( $row=mysql_fetch_array($result) )
					{
						$tmp_item_amt=$tmp_item_amt + doubleval(is_null($row["s1"]) ? 0 : $row["s1"]);
						$tmp_dis_amt =$tmp_dis_amt  + doubleval(is_null($row["s2"]) ? 0 : $row["s2"]);
						$tmp_dis_amt2=$tmp_dis_amt2 + doubleval(is_null($row["s3"]) ? 0 : $row["s3"]);
						$tmp_tax_amt =$tmp_tax_amt  + doubleval(is_null($row["s4"]) ? 0 : $row["s4"]);
						$tmp_line_amt=$tmp_line_amt + doubleval(is_null($row["s5"]) ? 0 : $row["s5"]);
					}

					$sql="UPDATE #__wel_porhdrm SET ".
						"wel_item_amt='$tmp_item_amt',".
						"wel_dis_amt='$tmp_dis_amt',".
						"wel_dis_amt2='$tmp_dis_amt2',".
						"wel_tax_amt='$tmp_tax_amt',".
						"wel_order_amt='$tmp_line_amt', ".
						"wel_upd_user='{$_SESSION['wel_user_id']}',".
						"wel_upd_date=now() ".
						"WHERE wel_po_no='$this->wel_po_no'";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				
				mysql_query("commit");
				
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage()); 
			}
				
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}		
		if($msg_code==""){$msg_code="delete_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	public function delete_detail_tab1_all()
	{
		$msg_code="";
		$return_val=array();
	 
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_delete")){throw new Exception("unauthorized_access");}
			
			$sql="SELECT * FROM #__wel_pormism WHERE wel_po_no='$this->wel_po_no' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_po_line_not_found");}
			
			try
			{
				mysql_query("begin");
				
					$sql="DELETE FROM #__wel_pormism WHERE wel_po_no='$this->wel_po_no'";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
			
					// Update PORHDRM discount & Tax
					$tmp_item_amt=0;
					$tmp_dis_amt=0;
					$tmp_dis_amt2=0;
					$tmp_tax_amt=0;
					$tmp_line_amt=0;
					$sql="SELECT ".
						"SUM(wel_item_amt) as s1,".
						"SUM(wel_dis_amt) as s2,".
						"sum(wel_dis_amt2) as s3,".
						"SUM(wel_tax_amt) as s4,".
						"SUM(wel_line_amt) as s5 ".
						"FROM #__wel_pordetm ".
						"WHERE wel_po_no='$this->wel_po_no' ";
					$sql=revert_to_the_available_sql($sql);
					if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
					if( $row=mysql_fetch_array($result) )
					{
						$tmp_item_amt=doubleval(is_null($row["s1"]) ? 0 : $row["s1"]);
						$tmp_dis_amt =doubleval(is_null($row["s2"]) ? 0 : $row["s2"]);
						$tmp_dis_amt2=doubleval(is_null($row["s3"]) ? 0 : $row["s3"]);
						$tmp_tax_amt =doubleval(is_null($row["s4"]) ? 0 : $row["s4"]);
						$tmp_line_amt=doubleval(is_null($row["s5"]) ? 0 : $row["s5"]);
					}

					$sql="UPDATE #__wel_porhdrm SET ".
						"wel_item_amt='$tmp_item_amt',".
						"wel_dis_amt='$tmp_dis_amt',".
						"wel_dis_amt2='$tmp_dis_amt2',".
						"wel_tax_amt='$tmp_tax_amt',".
						"wel_order_amt='$tmp_line_amt', ".
						"wel_upd_user='{$_SESSION['wel_user_id']}',".
						"wel_upd_date=now() ".
						"WHERE wel_po_no='$this->wel_po_no'";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				
				mysql_query("commit");
				
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage()); 
			}
			
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}		
		if($msg_code==""){$msg_code="delete_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
}
?>
