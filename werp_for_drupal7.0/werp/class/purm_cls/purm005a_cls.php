<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>

<?php
class purm005a_cls
{
	public $wel_po_no;
	public $wel_ven_code;
	public $wel_po_line;
	public $wel_part_no;
	public $wel_quot_no;
	public $wel_part_des;
	public $wel_part_des1;
	public $wel_part_des2;
			
	public $wel_pur_unit;
	public $wel_pur_unit_rate=0;
	public $wel_unit;
	public $wel_pur_qty=0;
	public $wel_u_price=0;
	public $wel_dis_rate=0;
	public $wel_tax_rate=0;
	public $wel_req_date;
	public $wel_com_date;
	public $wel_po_rmk;
	
	##还没用到的属性
	public $wel_blk_pono;
	public $wel_blk_poln;
	##
	
	private $wel_prog_code="purm005";

	public function read()
	{
		$msg_code="";
		$return_val=array();
		 
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_read")){throw new Exception("unauthorized_access");}
			
			$sql="SELECT r.*,p.wel_part_des,p.wel_part_des1,p.wel_part_des2,p.wel_unit,p.wel_quot_no ".
				 "FROM #__wel_pordetm r ".
					"LEFT JOIN #__wel_partflm p ON r.wel_part_no=p.wel_part_no ".
				 "WHERE r.wel_po_no='$this->wel_po_no' AND r.wel_po_line='$this->wel_po_line' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	//查询sql时出错了
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_po_no_not_found");}	//没有符合条件的记录
			$int__count=0;
			while ($int__count < mysql_num_fields($result)) 
			{
				$field_name=mysql_fetch_field($result,$int__count)->name;
				$return_val[$field_name]=$row[$field_name];
				$int__count++;
			}
			mysql_free_result($result);
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	public function addnew()
	{
		$msg_code="";
		$return_val=array();
 
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_addnew")){throw new Exception("unauthorized_access");}
			
			if ($this->wel_po_no==""){throw new Exception("wel_po_no_miss");}
			if ($this->wel_part_no==""){throw new Exception("wel_part_no_miss");}
			$sql="SELECT wel_part_no FROM #__wel_partflm WHERE wel_part_no='$this->wel_part_no' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_part_no_not_found");}

			if(empty($this->wel_req_date)){throw new Exception("wel_req_date_miss");}
			$this->wel_req_date=(($this->wel_req_date=="") ? "null" : "'".$this->wel_req_date."'");
			$this->wel_com_date=(($this->wel_com_date=="") ? "null" : "'".$this->wel_com_date."'");

			if (!is_numeric($this->wel_pur_qty)){$this->wel_pur_qty=0;}
			$this->wel_pur_qty=doubleval($this->wel_pur_qty);
			if($this->wel_pur_qty<=0){throw new Exception("wel_pur_qty_error");}   // pur qty

			if (!is_numeric($this->wel_u_price)){$this->wel_u_price=0;}
			$this->wel_u_price=doubleval($this->wel_u_price);

			if($this->wel_pur_unit==''){
				$sql="SELECT UPPER(wel_unit) as tmp_unit,wel_cat_code FROM #__wel_partflm WHERE wel_part_no='$this->wel_part_no' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_part_no_not_found");}

				$this->wel_pur_unit=$row["tmp_unit"];
				$this->wel_pur_unit_rate=1.000000;
				$str_cat_code=is_null($row["wel_cat_code"]) ? "" : $row["wel_cat_code"];
			}else{
				$this->wel_pur_unit=strtoupper($this->wel_pur_unit);

				$sql="SELECT CASE WHEN wel_unit='$this->wel_pur_unit' THEN ROUND(1.000000,6)".
					" WHEN wel_pur_unit='$this->wel_pur_unit' THEN ROUND(IFNULL(wel_pur_unit_rate,1),6)".
					" WHEN wel_pur_unit1='$this->wel_pur_unit' THEN ROUND(IFNULL(wel_pur_unit_rate1,1),6)".
					" WHEN wel_pur_unit2='$this->wel_pur_unit' THEN ROUND(IFNULL(wel_pur_unit_rate2,1),6)". 
					" END AS pur_rate,wel_cat_code FROM #__wel_partflm WHERE wel_part_no='$this->wel_part_no'";
				$sql=revert_to_the_available_sql($sql);
				if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_pur_unit_error");}

				if (is_null($row["pur_rate"])){throw new Exception("wel_pur_unit_error");}
				$this->wel_pur_unit_rate=$row["pur_rate"];
				$str_cat_code=is_null($row["wel_cat_code"]) ? "" : $row["wel_cat_code"];
			}

			$nreq_qty=$this->wel_pur_qty*$this->wel_pur_unit_rate;					// pur unit rate

			// Get Info from PORHDRM
			$sql="SELECT * FROM #__wel_porhdrm WHERE wel_po_no = '$this->wel_po_no' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_po_no_not_found");}
			$str_ven_code=is_null($row["wel_ven_code"]) ? "" : $row["wel_ven_code"];
			$str_buyer_code=is_null($row["wel_buyer_code"]) ? "" : $row["wel_buyer_code"];
			$int_line=intval(is_null($row["wel_last_line"]) ? 0 : $row["wel_last_line"])+1;
			$int_dis_type_h=intval(is_null($row["wel_dis_type"]) ? 0 : $row["wel_dis_type"]);
			$int_dis_rate_h=doubleval(is_null($row["wel_dis_rate"]) ? 0 : $row["wel_dis_rate"]);
			$int_tax_type_h=intval(is_null($row["wel_tax_type"]) ? 0 : $row["wel_tax_type"]);

			// Get min_ord from Quot. 
			$dec_wel_min_ord = 0;
			$dec_wel_pkg_ord = 0;
			$sql="SELECT wel_min_ord,wel_pkg_ord ".
				"FROM #__wel_venqdtm ".
				"WHERE wel_ven_code='$this->wel_ven_code' ".
				"AND wel_part_no='$this->wel_part_no' ".
				"AND wel_pur_unit='$this->wel_pur_unit' ".
				"ORDER BY wel_quot_date desc limit 1";
			$sql=revert_to_the_available_sql($sql);
			if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result))
			{
				// no found Get from vendor-part 
				$sql="SELECT wel_min_ord,wel_pkg_ord ".
					"FROM #__wel_venparm ".
					"WHERE wel_ven_code='$this->wel_ven_code' ".
					"AND wel_part_no='$this->wel_part_no' ".
					"AND wel_pur_unit='$this->wel_pur_unit' ";
				$sql=revert_to_the_available_sql($sql);
				if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result))
				{
					$dec_wel_min_ord = 0;
					$dec_wel_pkg_ord = 0;
				}
				else{
					$dec_wel_min_ord=doubleval(is_null($row["wel_min_ord"]) ? 0 : $row["wel_min_ord"]);
					$dec_wel_pkg_ord=doubleval(is_null($row["wel_pkg_ord"]) ? 0 : $row["wel_pkg_ord"]);
				}
			}
			else{
				$dec_wel_min_ord=doubleval(is_null($row["wel_min_ord"]) ? 0 : $row["wel_min_ord"]);
				$dec_wel_pkg_ord=doubleval(is_null($row["wel_pkg_ord"]) ? 0 : $row["wel_pkg_ord"]);
			}

			if($dec_wel_min_ord>0)
			{
				if($this->wel_pur_qty<$dec_wel_min_ord){throw new Exception("wel_min_ord_less");}
			}
			if($dec_wel_pkg_ord>0)
			{
				if(($this->wel_pur_qty % $dec_wel_pkg_ord)!=0){throw new Exception("wel_pkg_ord_mod");}
			}

			$dec_wel_item_amt = Round($this->wel_pur_qty*$this->wel_u_price,2);
			$dec_wel_dis_amt = 0;
			$dec_wel_dis_amt2 = 0;
			$dec_wel_tax_amt = 0;
			$dec_wel_line_amt = 0;
			if($this->wel_dis_rate>0)
			{
				$dec_wel_dis_amt = Round($dec_wel_item_amt * $this->wel_dis_rate / 100,2);
			}
			if ( ($int_dis_type_h==1 or $int_dis_type_h==3) and $this->wel_dis_rate==0)
			{
				$dec_wel_dis_amt2 = Round(($dec_wel_item_amt-$dec_wel_dis_amt) * $int_dis_rate_h / 100,2);
			}
			if ( $int_dis_type_h==4 or $int_dis_type_h==6 )
			{
				$dec_wel_dis_amt2 = Round(($dec_wel_item_amt-$dec_wel_dis_amt) * $int_dis_rate_h / 100,2);
			}

			$dec_wel_line_amt = $dec_wel_item_amt-$dec_wel_dis_amt-$dec_wel_dis_amt2;
			// 價外稅
			if ( $int_tax_type_h==2 )
			{
				$dec_wel_tax_amt = Round($dec_wel_line_amt * $this->wel_tax_rate / 100,2);
				$dec_wel_line_amt = $dec_wel_line_amt + $dec_wel_tax_amt;
			}
			// 價內稅
			if ( $int_tax_type_h==1 )
			{
				$dec_wel_tax_amt = Round($dec_wel_line_amt * $this->wel_tax_rate /(100 + $this->wel_tax_rate),2);
			}
			
			// Check P/O Line exist
			$sql="SELECT wel_po_no,wel_po_line FROM #__wel_pordetm ".
				"WHERE wel_po_no='$this->wel_po_no' AND ".
				"(wel_po_line='$int_line') LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
			if($row=mysql_fetch_array($result))
			{
				$sql="SELECT max(wel_po_line) as max_line FROM #__wel_pordetm ".
					"WHERE wel_po_no='$this->wel_po_no' ";
				$sql=revert_to_the_available_sql($sql);
				if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_po_no_not_found");}
				$int_line=intval(is_null($row["max_line"]) ? 0 : $row["max_line"])+1;
			}
			
			try
			{
				 mysql_query("begin");
					$sql="INSERT INTO #__wel_pordetm SET ".
							 "wel_po_no='$this->wel_po_no',".
							 "wel_po_line='$int_line',".
							 "wel_part_no='$this->wel_part_no',".
							 "wel_pur_qty='$this->wel_pur_qty',".
							 "wel_u_price='$this->wel_u_price',".
							 "wel_req_qty='$nreq_qty',".
							 "wel_os_qty='$nreq_qty',".
							 "wel_req_date=".$this->wel_req_date.",".
							 "wel_com_date=".$this->wel_com_date.",".
							 "wel_pur_unit='$this->wel_pur_unit',".
							 "wel_pur_unit_rate='$this->wel_pur_unit_rate',".
							 "wel_tmp_price='$this->wel_u_price',".
							 "wel_po_rmk='$this->wel_po_rmk',".
							 "wel_cat_code='$str_cat_code',".
							 "wel_ven_code='$str_ven_code',".
							 "wel_buyer_code='$str_buyer_code',".
							 "wel_dis_rate='$this->wel_dis_rate',".
							 "wel_tax_rate='$this->wel_tax_rate',".
							 "wel_item_amt='$dec_wel_item_amt',".
							 "wel_dis_amt='$dec_wel_dis_amt',".
							 "wel_dis_amt2='$dec_wel_dis_amt2',".
							 "wel_tax_amt='$dec_wel_tax_amt',".
							 "wel_line_amt='$dec_wel_line_amt',".
							 "wel_crt_user='{$_SESSION['wel_user_id']}',".
							 "wel_crt_date=now()";
					//		 "wel_quot_no='$this->wel_quot_no',".
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					
					// Update PORHDRM discount & Tax
					// Update PORHDRM discount & Tax
					$tmp_item_amt=0;
					$tmp_dis_amt=0;
					$tmp_dis_amt2=0;
					$tmp_tax_amt=0;
					$tmp_line_amt=0;
					$sql="SELECT ".
						"SUM(wel_item_amt) as s1,".
						"SUM(wel_dis_amt) as s2,".
						"sum(wel_dis_amt2) as s3,".
						"SUM(wel_tax_amt) as s4,".
						"SUM(wel_line_amt) as s5 ".
						"FROM #__wel_pordetm ".
						"WHERE wel_po_no='$this->wel_po_no' ";
					$sql=revert_to_the_available_sql($sql);
					if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
					if( $row=mysql_fetch_array($result) )
					{
						$tmp_item_amt=doubleval(is_null($row["s1"]) ? 0 : $row["s1"]);
						$tmp_dis_amt =doubleval(is_null($row["s2"]) ? 0 : $row["s2"]);
						$tmp_dis_amt2=doubleval(is_null($row["s3"]) ? 0 : $row["s3"]);
						$tmp_tax_amt =doubleval(is_null($row["s4"]) ? 0 : $row["s4"]);
						$tmp_line_amt=doubleval(is_null($row["s5"]) ? 0 : $row["s5"]);
					}

					$sql="SELECT ".
						"SUM(wel_item_amt) as s1,".
						"SUM(wel_dis_amt) as s2,".
						"sum(wel_dis_amt2) as s3,".
						"SUM(wel_tax_amt) as s4,".
						"SUM(wel_line_amt) as s5 ".
						"FROM #__wel_pormism ".
						"WHERE wel_po_no='$this->wel_po_no' ";
					$sql=revert_to_the_available_sql($sql);
					if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
					if( $row=mysql_fetch_array($result) )
					{
						$tmp_item_amt=$tmp_item_amt + doubleval(is_null($row["s1"]) ? 0 : $row["s1"]);
						$tmp_dis_amt =$tmp_dis_amt  + doubleval(is_null($row["s2"]) ? 0 : $row["s2"]);
						$tmp_dis_amt2=$tmp_dis_amt2 + doubleval(is_null($row["s3"]) ? 0 : $row["s3"]);
						$tmp_tax_amt =$tmp_tax_amt  + doubleval(is_null($row["s4"]) ? 0 : $row["s4"]);
						$tmp_line_amt=$tmp_line_amt + doubleval(is_null($row["s5"]) ? 0 : $row["s5"]);
					}

					$sql="UPDATE #__wel_porhdrm SET ".
						"wel_item_amt='$tmp_item_amt',".
						"wel_dis_amt='$tmp_dis_amt',".
						"wel_dis_amt2='$tmp_dis_amt2',".
						"wel_tax_amt='$tmp_tax_amt',".
						"wel_order_amt='$tmp_line_amt', ".
						"wel_last_line='$int_line',".
						"wel_upd_user='{$_SESSION['wel_user_id']}',".
						"wel_upd_date=now() ".
						"WHERE wel_po_no='$this->wel_po_no'";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					
				mysql_query("commit");
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage()); 
			}
	
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		if($msg_code==""){$msg_code="addnew_succee";}
		$return_val["msg_code"]=$msg_code;
		$return_val["wel_po_no"]=$this->wel_po_no;
		$return_val["wel_part_no"]=$this->wel_part_no;
		$return_val["wel_po_line"]=$this->wel_po_line;
		return $return_val;
	}
	
	public function edit()
	{
		$msg_code="";
		$return_val=array();

		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_edit")){throw new Exception("unauthorized_access");}
			
			if (!is_numeric($this->wel_pur_qty)){$this->wel_pur_qty=0;}
			$this->wel_pur_qty=doubleval($this->wel_pur_qty);
			if (!is_numeric($this->wel_u_price)){$this->wel_u_price=0;}
			$this->wel_u_price=doubleval($this->wel_u_price);
			
			if ($this->wel_part_no==""){throw new Exception("wel_part_no_miss");}
			if($this->wel_req_date==""){throw new Exception("wel_req_date_miss");}
			$this->wel_req_date=(($this->wel_req_date=="") ? "null" : "'".$this->wel_req_date."'");
			$this->wel_com_date=(($this->wel_com_date=="") ? "null" : "'".$this->wel_com_date."'");
			
			if($this->wel_pur_qty==0){throw new Exception("wel_pur_qty_error");}
			
			$sql="SELECT * FROM #__wel_porhdrm WHERE wel_po_no = '$this->wel_po_no' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_po_no_not_found");}
			$str_ven_code=is_null($row["wel_ven_code"]) ? "" : $row["wel_ven_code"];
			$str_cur_code= is_null($row["wel_cur_code"]) ? "" : $row["wel_cur_code"];
			$int_dis_type_h=intval(is_null($row["wel_dis_type"]) ? 0 : $row["wel_dis_type"]);
			$int_dis_rate_h=doubleval(is_null($row["wel_dis_rate"]) ? 0 : $row["wel_dis_rate"]);
			$int_tax_type_h=intval(is_null($row["wel_tax_type"]) ? 0 : $row["wel_tax_type"]);

			//判断采购细节是否存在
			$sql="SELECT * FROM #__wel_pordetm WHERE wel_po_no='$this->wel_po_no' AND wel_po_line=$this->wel_po_line";
			$sql=revert_to_the_available_sql($sql);
			if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("detail_not_found");}
			$dec_req_qty=doubleval(is_null($row["wel_req_qty"]) ? 0 : $row["wel_req_qty"]);
			$dec_os_qty=doubleval(is_null($row["wel_os_qty"]) ? 0 : $row["wel_os_qty"]);
			$dec_ship_qty=$dec_req_qty-$dec_os_qty; //收货数
			
			if($this->wel_pur_unit==''){
				$sql="SELECT UPPER(wel_unit) as tmp_unit,wel_cat_code FROM #__wel_partflm WHERE wel_part_no='$this->wel_part_no' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_part_no_not_found");}

				$this->wel_pur_unit=$row["tmp_unit"];
				$this->wel_pur_unit_rate=1.000000;
				$str_cat_code=is_null($row["wel_cat_code"]) ? "" : $row["wel_cat_code"];
			}else{
				$this->wel_pur_unit=strtoupper($this->wel_pur_unit);

				$sql="SELECT CASE WHEN wel_unit='$this->wel_pur_unit' THEN ROUND(1.000000,6)".
					" WHEN wel_pur_unit='$this->wel_pur_unit' THEN ROUND(IFNULL(wel_pur_unit_rate,1),6)".
					" WHEN wel_pur_unit1='$this->wel_pur_unit' THEN ROUND(IFNULL(wel_pur_unit_rate1,1),6)".
					" WHEN wel_pur_unit2='$this->wel_pur_unit' THEN ROUND(IFNULL(wel_pur_unit_rate2,1),6)". 
					" END AS pur_rate,wel_cat_code FROM #__wel_partflm WHERE wel_part_no='$this->wel_part_no'";
				$sql=revert_to_the_available_sql($sql);
				if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_pur_unit_error");}

				if (is_null($row["pur_rate"])){throw new Exception("wel_pur_unit_error");}
				$this->wel_pur_unit_rate=$row["pur_rate"];
				$str_cat_code=is_null($row["wel_cat_code"]) ? "" : $row["wel_cat_code"];
			}

			$nreq_qty=$this->wel_pur_qty*$this->wel_pur_unit_rate;					// pur unit rate
			//判断采购数和收料数
			if($nreq_qty<$dec_ship_qty){throw new Exception("wel_po_ship_qty_err");}

			// Get min_ord from Quot. 
			$dec_wel_min_ord = 0;
			$dec_wel_pkg_ord = 0;
			$sql="SELECT wel_min_ord,wel_pkg_ord ".
				"FROM #__wel_venqdtm ".
				"WHERE wel_ven_code='$this->wel_ven_code' ".
				"AND wel_part_no='$this->wel_part_no' ".
				"AND wel_pur_unit='$this->wel_pur_unit' ".
				"ORDER BY wel_quot_date desc limit 1";
			$sql=revert_to_the_available_sql($sql);
			if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result))
			{
				// no found Get from vendor-part 
				$sql="SELECT wel_min_ord,wel_pkg_ord ".
					"FROM #__wel_venparm ".
					"WHERE wel_ven_code='$this->wel_ven_code' ".
					"AND wel_part_no='$this->wel_part_no' ".
					"AND wel_pur_unit='$this->wel_pur_unit' ";
				$sql=revert_to_the_available_sql($sql);
				if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result))
				{
					$dec_wel_min_ord = 0;
					$dec_wel_pkg_ord = 0;
				}
				else{
					$dec_wel_min_ord=doubleval(is_null($row["wel_min_ord"]) ? 0 : $row["wel_min_ord"]);
					$dec_wel_pkg_ord=doubleval(is_null($row["wel_pkg_ord"]) ? 0 : $row["wel_pkg_ord"]);
				}
			}
			else{
				$dec_wel_min_ord=doubleval(is_null($row["wel_min_ord"]) ? 0 : $row["wel_min_ord"]);
				$dec_wel_pkg_ord=doubleval(is_null($row["wel_pkg_ord"]) ? 0 : $row["wel_pkg_ord"]);
			}

			if($dec_wel_min_ord>0)
			{
				if($this->wel_pur_qty<$dec_wel_min_ord){throw new Exception("wel_min_ord_less");}
			}
			if($dec_wel_pkg_ord>0)
			{
				if(($this->wel_pur_qty % $dec_wel_pkg_ord)!=0){throw new Exception("wel_pkg_ord_mod");}
			}

			$dec_wel_item_amt = Round($this->wel_pur_qty*$this->wel_u_price,2);
			$dec_wel_dis_amt = 0;
			$dec_wel_dis_amt2 = 0;
			$dec_wel_tax_amt = 0;
			$dec_wel_line_amt = 0;
			if($this->wel_dis_rate>0)
			{
				$dec_wel_dis_amt = Round($dec_wel_item_amt * $this->wel_dis_rate / 100,2);
			}
			if ( ($int_dis_type_h==1 or $int_dis_type_h==3) and $this->wel_dis_rate==0)
			{
				$dec_wel_dis_amt2 = Round(($dec_wel_item_amt-$dec_wel_dis_amt) * $int_dis_rate_h / 100,2);
			}
			if ( $int_dis_type_h==4 or $int_dis_type_h==6 )
			{
				$dec_wel_dis_amt2 = Round(($dec_wel_item_amt-$dec_wel_dis_amt) * $int_dis_rate_h / 100,2);
			}

			$dec_wel_line_amt = $dec_wel_item_amt-$dec_wel_dis_amt-$dec_wel_dis_amt2;
			// 價外稅
			if ( $int_tax_type_h==2 )
			{
				$dec_wel_tax_amt = Round($dec_wel_line_amt * $this->wel_tax_rate / 100,2);
				$dec_wel_line_amt = $dec_wel_line_amt + $dec_wel_tax_amt;
			}
			// 價內稅
			if ( $int_tax_type_h==1 )
			{
				$dec_wel_tax_amt = Round($dec_wel_line_amt * $this->wel_tax_rate /(100 + $this->wel_tax_rate),2);
			}

			try
			{
				mysql_query("begin");
				
					$dec_req_qty=$nreq_qty;
					$dec_po_os_qty=$dec_req_qty-$dec_ship_qty;
					
					$sql="UPDATE #__wel_pordetm SET ".
							 "wel_pur_qty='$this->wel_pur_qty',".
							 "wel_req_qty='$dec_req_qty',".
							 "wel_os_qty='$dec_po_os_qty',".
							 "wel_u_price='$this->wel_u_price',".
							 "wel_req_date=".$this->wel_req_date.",".
							 "wel_com_date=".$this->wel_com_date.",".
							 "wel_tmp_price='$this->wel_u_price',".
							 "wel_po_rmk='$this->wel_po_rmk',".
							 "wel_dis_rate='$this->wel_dis_rate',".
							 "wel_tax_rate='$this->wel_tax_rate',".
							 "wel_item_amt='$dec_wel_item_amt',".
							 "wel_dis_amt='$dec_wel_dis_amt',".
							 "wel_dis_amt2='$dec_wel_dis_amt2',".
							 "wel_tax_amt='$dec_wel_tax_amt',".
							 "wel_line_amt='$dec_wel_line_amt',".
							 "wel_upd_user='{$_SESSION['wel_user_id']}',".
							 "wel_upd_date=now() ".
						"WHERE wel_po_no='$this->wel_po_no' AND wel_po_line='$this->wel_po_line'";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					
					// Update PORHDRM discount & Tax
					// Update PORHDRM discount & Tax
					$tmp_item_amt=0;
					$tmp_dis_amt=0;
					$tmp_dis_amt2=0;
					$tmp_tax_amt=0;
					$tmp_line_amt=0;
					$sql="SELECT ".
						"SUM(wel_item_amt) as s1,".
						"SUM(wel_dis_amt) as s2,".
						"sum(wel_dis_amt2) as s3,".
						"SUM(wel_tax_amt) as s4,".
						"SUM(wel_line_amt) as s5 ".
						"FROM #__wel_pordetm ".
						"WHERE wel_po_no='$this->wel_po_no' ";
					$sql=revert_to_the_available_sql($sql);
					if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
					if( $row=mysql_fetch_array($result) )
					{
						$tmp_item_amt=doubleval(is_null($row["s1"]) ? 0 : $row["s1"]);
						$tmp_dis_amt =doubleval(is_null($row["s2"]) ? 0 : $row["s2"]);
						$tmp_dis_amt2=doubleval(is_null($row["s3"]) ? 0 : $row["s3"]);
						$tmp_tax_amt =doubleval(is_null($row["s4"]) ? 0 : $row["s4"]);
						$tmp_line_amt=doubleval(is_null($row["s5"]) ? 0 : $row["s5"]);
					}

					$sql="SELECT ".
						"SUM(wel_item_amt) as s1,".
						"SUM(wel_dis_amt) as s2,".
						"sum(wel_dis_amt2) as s3,".
						"SUM(wel_tax_amt) as s4,".
						"SUM(wel_line_amt) as s5 ".
						"FROM #__wel_pormism ".
						"WHERE wel_po_no='$this->wel_po_no' ";
					$sql=revert_to_the_available_sql($sql);
					if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
					if( $row=mysql_fetch_array($result) )
					{
						$tmp_item_amt=$tmp_item_amt + doubleval(is_null($row["s1"]) ? 0 : $row["s1"]);
						$tmp_dis_amt =$tmp_dis_amt  + doubleval(is_null($row["s2"]) ? 0 : $row["s2"]);
						$tmp_dis_amt2=$tmp_dis_amt2 + doubleval(is_null($row["s3"]) ? 0 : $row["s3"]);
						$tmp_tax_amt =$tmp_tax_amt  + doubleval(is_null($row["s4"]) ? 0 : $row["s4"]);
						$tmp_line_amt=$tmp_line_amt + doubleval(is_null($row["s5"]) ? 0 : $row["s5"]);
					}

					$sql="UPDATE #__wel_porhdrm SET ".
						"wel_item_amt='$tmp_item_amt',".
						"wel_dis_amt='$tmp_dis_amt',".
						"wel_dis_amt2='$tmp_dis_amt2',".
						"wel_tax_amt='$tmp_tax_amt',".
						"wel_order_amt='$tmp_line_amt', ".
						"wel_upd_user='{$_SESSION['wel_user_id']}',".
						"wel_upd_date=now() ".
						"WHERE wel_po_no='$this->wel_po_no'";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					
				mysql_query("commit");
				
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage()); 
			}
			
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		if($msg_code==""){$msg_code="edit_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
}
?>
