<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>

<?php
class purm005b_cls
{
	public $wel_po_no;
	public $wel_line_no;
	public $wel_item;
	public $wel_item_amt;
	public $wel_dis_rate;
	public $wel_tax_rate;
	
	private $wel_prog_code="purm005";

	public function read()
	{
		$msg_code="";
		$return_val=array();

		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_read")){throw new Exception("unauthorized_access");}
			
			$sql="SELECT * FROM #__wel_pormism WHERE wel_po_no='$this->wel_po_no' AND wel_line_no='$this->wel_line_no' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_po_no_not_found");}
			$int__count=0;
			while ($int__count < mysql_num_fields($result)) 
			{
				$field_name=mysql_fetch_field($result,$int__count)->name;
				$return_val[$field_name]=$row[$field_name];
				$int__count++;
			}
			mysql_free_result($result);
			
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	public function addnew()
	{
		$msg_code="";
		$return_val=array();
 
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_addnew")){throw new Exception("unauthorized_access");}
		
			if($this->wel_item==""){throw new Exception("wel_item_miss");}
			if (!is_numeric($this->wel_item_amt)){$this->wel_item_amt=0;}
			$this->wel_item_amt=doubleval($this->wel_item_amt);
				
			// Get Info from PORHDRM
			$sql="SELECT * FROM #__wel_porhdrm WHERE wel_po_no = '$this->wel_po_no' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_po_no_not_found");}
			$int_dis_type_h=intval(is_null($row["wel_dis_type"]) ? 0 : $row["wel_dis_type"]);
			$int_dis_rate_h=doubleval(is_null($row["wel_dis_rate"]) ? 0 : $row["wel_dis_rate"]);
			$int_tax_type_h=intval(is_null($row["wel_tax_type"]) ? 0 : $row["wel_tax_type"]);

			$dec_wel_item_amt = $this->wel_item_amt;
			$dec_wel_dis_amt = 0;
			$dec_wel_dis_amt2 = 0;
			$dec_wel_tax_amt = 0;
			$dec_wel_line_amt = 0;
			if($this->wel_dis_rate>0)
			{
				$dec_wel_dis_amt = Round($dec_wel_item_amt * $this->wel_dis_rate / 100,2);
			}

			if ( ($int_dis_type_h==2 or $int_dis_type_h==3) and $this->wel_dis_rate==0)
			{
				$dec_wel_dis_amt2 = Round(($dec_wel_item_amt-$dec_wel_dis_amt) * $int_dis_rate_h / 100,2);
			}
			if ( $int_dis_type_h==5 or $int_dis_type_h==6 )
			{
				$dec_wel_dis_amt2 = Round(($dec_wel_item_amt-$dec_wel_dis_amt) * $int_dis_rate_h / 100,2);
			}

			$dec_wel_line_amt = $dec_wel_item_amt-$dec_wel_dis_amt-$dec_wel_dis_amt2;
			// 價外稅
			if ( $int_tax_type_h==2 )
			{
				$dec_wel_tax_amt = Round($dec_wel_line_amt * $this->wel_tax_rate / 100,2);
				$dec_wel_line_amt = $dec_wel_line_amt + $dec_wel_tax_amt;
			}
			// 價內稅
			if ( $int_tax_type_h==1 )
			{
				$dec_wel_tax_amt = Round($dec_wel_line_amt * $this->wel_tax_rate /(100 + $this->wel_tax_rate),2);
			}

			$sql="SELECT MAX(wel_line_no) AS wel_line_no FROM #__wel_pormism WHERE wel_po_no='$this->wel_po_no' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){$this->wel_line_no=1;}
			$this->wel_line_no=intval(is_null($row["wel_line_no"]) ? 0 : $row["wel_line_no"])+1;
				
			$sql="SELECT * FROM #__wel_pormism WHERE wel_po_no='".$this->wel_po_no."' AND wel_line_no=".$this->wel_line_no." LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
			if($row=mysql_fetch_array($result)){throw new Exception("wel_line_no_exist");}
			
			try
			{
				mysql_query("begin");
				
					$sql="INSERT INTO #__wel_pormism SET ".
							 "wel_po_no='$this->wel_po_no',".
							 "wel_line_no=".$this->wel_line_no.",".
							 "wel_item='$this->wel_item',".
							 "wel_item_amt=".$this->wel_item_amt.",".
							 "wel_dis_rate='$this->wel_dis_rate',".
							 "wel_tax_rate='$this->wel_tax_rate',".
							 "wel_dis_amt='$dec_wel_dis_amt',".
							 "wel_dis_amt2='$dec_wel_dis_amt2',".
							 "wel_tax_amt='$dec_wel_tax_amt',".
							 "wel_line_amt='$dec_wel_line_amt'";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					
					// Update PORHDRM discount & Tax
					$tmp_item_amt=0;
					$tmp_dis_amt=0;
					$tmp_dis_amt2=0;
					$tmp_tax_amt=0;
					$tmp_line_amt=0;
					$sql="SELECT ".
						"SUM(wel_item_amt) as s1,".
						"SUM(wel_dis_amt) as s2,".
						"sum(wel_dis_amt2) as s3,".
						"SUM(wel_tax_amt) as s4,".
						"SUM(wel_line_amt) as s5 ".
						"FROM #__wel_pordetm ".
						"WHERE wel_po_no='$this->wel_po_no' ";
					$sql=revert_to_the_available_sql($sql);
					if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
					if( $row=mysql_fetch_array($result) )
					{
						$tmp_item_amt=doubleval(is_null($row["s1"]) ? 0 : $row["s1"]);
						$tmp_dis_amt =doubleval(is_null($row["s2"]) ? 0 : $row["s2"]);
						$tmp_dis_amt2=doubleval(is_null($row["s3"]) ? 0 : $row["s3"]);
						$tmp_tax_amt =doubleval(is_null($row["s4"]) ? 0 : $row["s4"]);
						$tmp_line_amt=doubleval(is_null($row["s5"]) ? 0 : $row["s5"]);
					}

					$sql="SELECT ".
						"SUM(wel_item_amt) as s1,".
						"SUM(wel_dis_amt) as s2,".
						"sum(wel_dis_amt2) as s3,".
						"SUM(wel_tax_amt) as s4,".
						"SUM(wel_line_amt) as s5 ".
						"FROM #__wel_pormism ".
						"WHERE wel_po_no='$this->wel_po_no' ";
					$sql=revert_to_the_available_sql($sql);
					if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
					if( $row=mysql_fetch_array($result) )
					{
						$tmp_item_amt=$tmp_item_amt + doubleval(is_null($row["s1"]) ? 0 : $row["s1"]);
						$tmp_dis_amt =$tmp_dis_amt  + doubleval(is_null($row["s2"]) ? 0 : $row["s2"]);
						$tmp_dis_amt2=$tmp_dis_amt2 + doubleval(is_null($row["s3"]) ? 0 : $row["s3"]);
						$tmp_tax_amt =$tmp_tax_amt  + doubleval(is_null($row["s4"]) ? 0 : $row["s4"]);
						$tmp_line_amt=$tmp_line_amt + doubleval(is_null($row["s5"]) ? 0 : $row["s5"]);
					}

					$sql="UPDATE #__wel_porhdrm SET ".
						"wel_item_amt='$tmp_item_amt',".
						"wel_dis_amt='$tmp_dis_amt',".
						"wel_dis_amt2='$tmp_dis_amt2',".
						"wel_tax_amt='$tmp_tax_amt',".
						"wel_order_amt='$tmp_line_amt', ".
						"wel_upd_user='{$_SESSION['wel_user_id']}',".
						"wel_upd_date=now() ".
						"WHERE wel_po_no='$this->wel_po_no'";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}

				mysql_query("commit");
				
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage()); 
			}
			
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		if($msg_code==""){$msg_code="addnew_succee";}
		$return_val["msg_code"]=$msg_code;
		$return_val["wel_po_no"]=$this->wel_po_no;
		$return_val["wel_line_no"]=$this->wel_line_no;
		return $return_val;
	}
	
	public function edit()
	{
		$msg_code="";
		$return_val=array();
 
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_edit")){throw new Exception("unauthorized_access");}
			
			if($this->wel_item==""){throw new Exception("wel_item_miss");}
			if (!is_numeric($this->wel_item_amt)){$this->wel_item_amt=0;}
			$this->wel_item_amt=doubleval($this->wel_item_amt);
			
			// Get Info from PORHDRM
			$sql="SELECT * FROM #__wel_porhdrm WHERE wel_po_no = '$this->wel_po_no' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_po_no_not_found");}
			$int_dis_type_h=intval(is_null($row["wel_dis_type"]) ? 0 : $row["wel_dis_type"]);
			$int_dis_rate_h=doubleval(is_null($row["wel_dis_rate"]) ? 0 : $row["wel_dis_rate"]);
			$int_tax_type_h=intval(is_null($row["wel_tax_type"]) ? 0 : $row["wel_tax_type"]);

			$dec_wel_item_amt = $this->wel_item_amt;
			$dec_wel_dis_amt = 0;
			$dec_wel_dis_amt2 = 0;
			$dec_wel_tax_amt = 0;
			$dec_wel_line_amt = 0;
			if($this->wel_dis_rate>0)
			{
				$dec_wel_dis_amt = Round($dec_wel_item_amt * $this->wel_dis_rate / 100,2);
			}

			if ( ($int_dis_type_h==2 or $int_dis_type_h==3) and $this->wel_dis_rate==0)
			{
				$dec_wel_dis_amt2 = Round(($dec_wel_item_amt-$dec_wel_dis_amt) * $int_dis_rate_h / 100,2);
			}
			if ( $int_dis_type_h==5 or $int_dis_type_h==6 )
			{
				$dec_wel_dis_amt2 = Round(($dec_wel_item_amt-$dec_wel_dis_amt) * $int_dis_rate_h / 100,2);
			}

			$dec_wel_line_amt = $dec_wel_item_amt-$dec_wel_dis_amt-$dec_wel_dis_amt2;
			// 價外稅
			if ( $int_tax_type_h==2 )
			{
				$dec_wel_tax_amt = Round($dec_wel_line_amt * $this->wel_tax_rate / 100,2);
				$dec_wel_line_amt = $dec_wel_line_amt + $dec_wel_tax_amt;
			}
			// 價內稅
			if ( $int_tax_type_h==1 )
			{
				$dec_wel_tax_amt = Round($dec_wel_line_amt * $this->wel_tax_rate /(100 + $this->wel_tax_rate),2);
			}

			$sql="SELECT * FROM #__wel_pormism WHERE wel_po_no='$this->wel_po_no' AND wel_line_no='$this->wel_line_no' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_line_no_not_found");}

			try
			{
				mysql_query("begin");
				
					$sql="UPDATE #__wel_pormism SET ".
							 "wel_item='$this->wel_item',".
							 "wel_item_amt=".$this->wel_item_amt.",".
							 "wel_dis_rate='$this->wel_dis_rate',".
							 "wel_tax_rate='$this->wel_tax_rate',".
							 "wel_dis_amt='$dec_wel_dis_amt',".
							 "wel_dis_amt2='$dec_wel_dis_amt2',".
							 "wel_tax_amt='$dec_wel_tax_amt',".
							 "wel_line_amt='$dec_wel_line_amt' ".
						"WHERE wel_po_no='$this->wel_po_no' AND wel_line_no='$this->wel_line_no'";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					
					// Update PORHDRM discount & Tax
					$tmp_item_amt=0;
					$tmp_dis_amt=0;
					$tmp_dis_amt2=0;
					$tmp_tax_amt=0;
					$tmp_line_amt=0;
					$sql="SELECT ".
						"SUM(wel_item_amt) as s1,".
						"SUM(wel_dis_amt) as s2,".
						"sum(wel_dis_amt2) as s3,".
						"SUM(wel_tax_amt) as s4,".
						"SUM(wel_line_amt) as s5 ".
						"FROM #__wel_pordetm ".
						"WHERE wel_po_no='$this->wel_po_no' ";
					$sql=revert_to_the_available_sql($sql);
					if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
					if( $row=mysql_fetch_array($result) )
					{
						$tmp_item_amt=doubleval(is_null($row["s1"]) ? 0 : $row["s1"]);
						$tmp_dis_amt =doubleval(is_null($row["s2"]) ? 0 : $row["s2"]);
						$tmp_dis_amt2=doubleval(is_null($row["s3"]) ? 0 : $row["s3"]);
						$tmp_tax_amt =doubleval(is_null($row["s4"]) ? 0 : $row["s4"]);
						$tmp_line_amt=doubleval(is_null($row["s5"]) ? 0 : $row["s5"]);
					}

					$sql="SELECT ".
						"SUM(wel_item_amt) as s1,".
						"SUM(wel_dis_amt) as s2,".
						"sum(wel_dis_amt2) as s3,".
						"SUM(wel_tax_amt) as s4,".
						"SUM(wel_line_amt) as s5 ".
						"FROM #__wel_pormism ".
						"WHERE wel_po_no='$this->wel_po_no' ";
					$sql=revert_to_the_available_sql($sql);
					if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
					if( $row=mysql_fetch_array($result) )
					{
						$tmp_item_amt=$tmp_item_amt + doubleval(is_null($row["s1"]) ? 0 : $row["s1"]);
						$tmp_dis_amt =$tmp_dis_amt  + doubleval(is_null($row["s2"]) ? 0 : $row["s2"]);
						$tmp_dis_amt2=$tmp_dis_amt2 + doubleval(is_null($row["s3"]) ? 0 : $row["s3"]);
						$tmp_tax_amt =$tmp_tax_amt  + doubleval(is_null($row["s4"]) ? 0 : $row["s4"]);
						$tmp_line_amt=$tmp_line_amt + doubleval(is_null($row["s5"]) ? 0 : $row["s5"]);
					}

					$sql="UPDATE #__wel_porhdrm SET ".
						"wel_item_amt='$tmp_item_amt',".
						"wel_dis_amt='$tmp_dis_amt',".
						"wel_dis_amt2='$tmp_dis_amt2',".
						"wel_tax_amt='$tmp_tax_amt',".
						"wel_order_amt='$tmp_line_amt', ".
						"wel_upd_user='{$_SESSION['wel_user_id']}',".
						"wel_upd_date=now() ".
						"WHERE wel_po_no='$this->wel_po_no'";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				
				mysql_query("commit");
				
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage()); 
			}
			
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}		
		if($msg_code==""){$msg_code="edit_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
}
?>
