<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>

<?php
class purm008_cls
{
	public $wel_quot_no;	
	public $wel_ven_code;	
	public $wel_quot_date;
	public $wel_cur_code;
	public $wel_ex_rate;
	public $wel_type_code;
	public $wel_ven_pbase;
	public $wel_ven_svia;
	public $wel_buyer_code;
	public $wel_qt_remark;

	public $wel_crt_user;
	public $wel_crt_date;
	public $wel_upd_user;
	public $wel_upd_date;
	public $wel_appr_yn;
	public $wel_appr_by;
	public $wel_appr_date;
	
	public $wel_quot_line;	
	public $wel_quot_no_from;
	private $wel_prog_code="purm008";

	public function read()
	{
		$msg_code="";
		$return_val=array();
		 
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_read")){throw new Exception("unauthorized_access");}
			
			$sql="SELECT q.*, ".
						"v.wel_ven_des, ".
						"v.wel_ven_des1, ".
						"v.wel_abbre_des, ".
						"v.wel_ven_add1, ".
						"v.wel_ven_add2, ".
						"v.wel_ven_add3, ".
						"v.wel_ven_add4, ".
						"v.wel_ven_email, ".
						"v.wel_ven_cont, ".
						"v.wel_ven_tele, ".
						"v.wel_ven_tele1, ".
						"v.wel_ven_fax, ".
						"v.wel_ven_fax1, ".
						"b.wel_type_des as wel_ven_term_des, ".
						"c.wel_pbase_name as wel_ven_pbase_des, ".
						"d.wel_svia_des as wel_ven_svia_des ".
					"FROM #__wel_venqhdm q ".
					"LEFT JOIN #__wel_venmasm v ON q.wel_ven_code=v.wel_ven_code ".
					"LEFT JOIN #__wel_portyem b ON q.wel_ven_term=b.wel_type_code ".
					"LEFT JOIN #__wel_pbasefm c ON q.wel_ven_pbase=c.wel_pbase_code ".
					"LEFT JOIN #__wel_shpviam d ON q.wel_ven_svia=d.wel_svia_code ".
				"WHERE q.wel_quot_no='$this->wel_quot_no' LIMIT 1"; 
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_quot_no_not_found");}
			$int__count=0;
			while ($int__count < mysql_num_fields($result)) 
			{
				$field_name=mysql_fetch_field($result,$int__count)->name;
				$return_val[$field_name]=$row[$field_name];
				$int__count++;
			}
			mysql_free_result($result);
			
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	public function addnew()
	{
		$msg_code="";
		$return_val=array();
	 
		try
		{
			$conn= werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_addnew")){throw new Exception("unauthorized_access");}
			if($this->wel_quot_no==""){throw new Exception("wel_quot_no_miss");}
			if($this->wel_ven_code==""){throw new Exception("wel_ven_code_miss");}
			if($this->wel_quot_date==""){throw new Exception("wel_quot_date_miss");}
			if($this->wel_buyer_code==""){throw new Exception("wel_buyer_code_miss");}
			if($this->wel_cur_code==""){throw new Exception("wel_cur_code_miss");}
			if(!is_numeric($this->wel_ex_rate)){$this->wel_ex_rate=0;}
			$this->wel_ex_rate=doubleval($this->wel_ex_rate);
			
			if($this->wel_ven_code!="")
			{
				$sql="SELECT wel_ven_code FROM #__wel_venmasm WHERE wel_ven_code='".$this->wel_ven_code."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_ven_code_not_found");}
			}
			if($this->wel_buyer_code!="")
			{
				$sql="SELECT wel_buyer_code FROM #__wel_purhanm WHERE wel_buyer_code='".$this->wel_buyer_code."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_buyer_code_not_found");}
			}
			if($this->wel_cur_code!="")
			{
				$sql="SELECT wel_cur_code,wel_ex_rate FROM #__wel_currenm WHERE wel_cur_code='".$this->wel_cur_code."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_cur_code_not_found");}
				$this->wel_ex_rate=doubleval($row["wel_ex_rate"]);
			}
			if($this->wel_type_code!="")
			{
				$sql="SELECT wel_type_code FROM #__wel_portyem WHERE wel_type_code='".$this->wel_type_code."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_type_code_not_found");}
			}
			if($this->wel_ven_pbase!="")
			{
				$sql="SELECT wel_pbase_code FROM #__wel_pbasefm WHERE wel_pbase_code='".$this->wel_ven_pbase."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_ven_pbase_not_found");}
			}
			if($this->wel_ven_svia!="")
			{
				$sql="SELECT wel_svia_code FROM #__wel_shpviam WHERE wel_svia_code='".$this->wel_ven_svia."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_ven_svia_not_found");}
			}

			$sql="SELECT * FROM #__wel_venqhdm WHERE wel_quot_no='$this->wel_quot_no' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if($row=mysql_fetch_array($result)){throw new Exception("wel_quot_no_exist");}
			
			try
			{
				mysql_query('begin');
				
					$sql="INSERT INTO #__wel_venqhdm SET ".
							 "wel_quot_no='$this->wel_quot_no',".
							 "wel_ven_code='$this->wel_ven_code',".
							 "wel_quot_date='$this->wel_quot_date',".
							 "wel_cur_code='$this->wel_cur_code',".
							 "wel_ex_rate='$this->wel_ex_rate',".
							 "wel_ven_term='$this->wel_type_code',".
							 "wel_ven_pbase='$this->wel_ven_pbase',".
							 "wel_ven_svia='$this->wel_ven_svia',".
							 "wel_buyer_code='$this->wel_buyer_code',".
							 "wel_qt_remark='$this->wel_qt_remark',".
							 "wel_crt_user='{$_SESSION['wel_user_id']}',".
							 "wel_crt_date=now(),".
							 "wel_appr_yn=0"; 
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
					mysql_free_result($result);
					
				mysql_query("commit");
				
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}
			
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		if($msg_code==""){$msg_code="addnew_succee";}
		$return_val["msg_code"]=$msg_code;
		$return_val["wel_quot_no"]=$this->wel_quot_no;
		return $return_val;
	}
	
	public function edit()
	{
		$msg_code="";
		$return_val=array();
		 
		try
		{
			$conn= werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_edit")){throw new Exception("unauthorized_access");}
			
			if($this->wel_quot_no==""){throw new Exception("wel_quot_no_miss");}
			if($this->wel_ven_code==""){throw new Exception("wel_ven_code_miss");}
			if($this->wel_quot_date==""){throw new Exception("wel_quot_date_miss");}
			if($this->wel_buyer_code==""){throw new Exception("wel_buyer_code_miss");}
			if($this->wel_cur_code==""){throw new Exception("wel_cur_code_miss");}
			if(!is_numeric($this->wel_ex_rate)){$this->wel_ex_rate=0;}
			$this->wel_ex_rate=doubleval($this->wel_ex_rate);
			
			if($this->wel_ven_code!="")
			{
				$sql="SELECT wel_ven_code FROM #__wel_venmasm WHERE wel_ven_code='".$this->wel_ven_code."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_ven_code_not_found");}
			}
			if($this->wel_buyer_code!="")
			{
				$sql="SELECT wel_buyer_code FROM #__wel_purhanm WHERE wel_buyer_code='".$this->wel_buyer_code."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_buyer_code_not_found");}
			}
			if($this->wel_cur_code!="")
			{
				$sql="SELECT wel_cur_code,wel_ex_rate FROM #__wel_currenm WHERE wel_cur_code='".$this->wel_cur_code."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_cur_code_not_found");}
				$this->wel_ex_rate=doubleval($row["wel_ex_rate"]);
			}
			if($this->wel_type_code!="")
			{
				$sql="SELECT wel_type_code FROM #__wel_portyem WHERE wel_type_code='".$this->wel_type_code."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_type_code_not_found");}
			}
			if($this->wel_ven_pbase!="")
			{
				$sql="SELECT wel_pbase_code FROM #__wel_pbasefm WHERE wel_pbase_code='".$this->wel_ven_pbase."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_ven_pbase_not_found");}
			}
			if($this->wel_ven_svia!="")
			{
				$sql="SELECT wel_svia_code FROM #__wel_shpviam WHERE wel_svia_code='".$this->wel_ven_svia."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_ven_svia_not_found");}
			}

			$sql="SELECT * FROM #__wel_venqhdm WHERE wel_quot_no='$this->wel_quot_no' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_quot_no_not_found");}
			
			try
			{
				mysql_query('begin');
				
					$sql="UPDATE #__wel_venqhdm SET ".
							 "wel_quot_date='$this->wel_quot_date',".
							 "wel_cur_code='$this->wel_cur_code',".
							 "wel_ex_rate='$this->wel_ex_rate',".
							 "wel_ven_term='$this->wel_type_code',".
							 "wel_ven_pbase='$this->wel_ven_pbase',".
							 "wel_ven_svia='$this->wel_ven_svia',".
							 "wel_buyer_code='$this->wel_buyer_code',".
							 "wel_qt_remark='$this->wel_qt_remark',".
							 "wel_upd_user='{$_SESSION['wel_user_id']}',".
							 "wel_upd_date=now() ".
						 "WHERE wel_quot_no='$this->wel_quot_no' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
					mysql_free_result($result);
				mysql_query("commit");
				
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}
			
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}		
		if($msg_code==""){$msg_code="edit_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	public function copy()
	{
		$msg_code="";
		$return_val=array();
		 
		try
		{
			$conn= werp_db_connect();
			$str_wel_ven_code="";
			$str_wel_cur_code="";
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_edit")){throw new Exception("unauthorized_access");}

			if ($this->wel_quot_no_from==""){throw new Exception("wel_quot_no_from_miss");}
			if ($this->wel_quot_no==""){throw new Exception("wel_quot_no_miss");}
			if($this->wel_ven_code==""){throw new Exception("wel_ven_code_miss");}
			if($this->wel_quot_date==""){throw new Exception("wel_quot_date_miss");}
			if($this->wel_buyer_code==""){throw new Exception("wel_buyer_code_miss");}
			if($this->wel_cur_code==""){throw new Exception("wel_cur_code_miss");}
			if(!is_numeric($this->wel_ex_rate)){$this->wel_ex_rate=0;}
			$this->wel_ex_rate=doubleval($this->wel_ex_rate);

			//检查新输入的报价单号是否存在
			$sql="SELECT * FROM #__wel_venqhdm WHERE wel_quot_no='$this->wel_quot_no' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if($row=mysql_fetch_array($result)){throw new Exception("wel_quot_no_exist");}
			
			$sql="SELECT * FROM #__wel_venqhdm WHERE wel_quot_no='$this->wel_quot_no_from' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_quot_no_not_found");}
			$str_wel_ven_code=is_null($row["wel_ven_code"]) ? "" : $row["wel_ven_code"];
			$str_wel_cur_code=is_null($row["wel_cur_code"]) ? "" : $row["wel_cur_code"];
			
			try
			{
				mysql_query('begin');
					//复制head
					$sql="INSERT INTO #__wel_venqhdm SET ".
							 "wel_quot_no='$this->wel_quot_no',".
							 "wel_ven_code='".$row["wel_ven_code"]."',".
							 "wel_quot_date=now(),".
							 "wel_cur_code='".$row["wel_cur_code"]."',".
							 "wel_ex_rate='".$row["wel_ex_rate"]."',".
							 "wel_ven_term='".$row["wel_ven_term"]."',".
							 "wel_ven_pbase='".$row["wel_ven_pbase"]."',".
							 "wel_ven_svia='".$row["wel_ven_svia"]."',".
							 "wel_buyer_code='".$row["wel_buyer_code"]."',".
							 "wel_qt_remark='".$row["wel_qt_remark"]."',".
							 "wel_crt_user='{$_SESSION['wel_user_id']}',".
							 "wel_crt_date=now(),".
							 "wel_appr_yn=0"; 
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
					
					//复制detail
					$sql="SELECT * FROM #__wel_venqdtm WHERE wel_quot_no='$this->wel_quot_no_from'";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
					$int_count=1;
					while($row=mysql_fetch_array($result)){
						$sql="INSERT INTO #__wel_venqdtm SET ".
								 "wel_quot_no='$this->wel_quot_no',".
								 "wel_quot_line='".$int_count."',".
								 "wel_ven_code='".$str_wel_ven_code."',".
								 "wel_part_no='".$row["wel_part_no"]."',".
								 "wel_unit_code='".$row["wel_unit_code"]."',".
								 "wel_pur_unit='".$row["wel_pur_unit"]."',".
								 "wel_pur_unit_rate='".$row["wel_pur_unit_rate"]."',".
								 "wel_quot_qty='".$row["wel_quot_qty"]."',".
								 "wel_u_price='".$row["wel_u_price"]."',".
								 "wel_min_ord='".$row["wel_min_ord"]."',".
								 "wel_pkg_ord='".$row["wel_pkg_ord"]."',".
								 "wel_lead_tm='".$row["wel_lead_tm"]."',".
								 "wel_prmy_quot='".$row["wel_prmy_quot"]."',".
								 "wel_ven_part='".$row["wel_ven_part"]."',".
								 "wel_appr_yn=0,".
								 "wel_cur_code='".$str_wel_cur_code."',".
								 "wel_quot_date=now() ";
						$sql=revert_to_the_available_sql($sql);
						$int_count ++;	 
						if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					}
					mysql_free_result($result);
					
				mysql_query("commit");
				
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}
			
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}		
		if($msg_code==""){$msg_code="copy_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	public function delete()
	{
		$msg_code="";
		$return_val=array();
 
		try
		{
			$conn= werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_delete")){throw new Exception("unauthorized_access");}
			
			if($this->wel_quot_no==""){throw new Exception("wel_quot_no_miss");}
 
			$sql="SELECT * FROM #__wel_venqhdm WHERE wel_quot_no='$this->wel_quot_no' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}	//查询sql时出错了
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_quot_no_not_found");}	//没有符合条件的记录
			
			try 
			{
				mysql_query('begin');
				
					//删除细节
					$sql="DELETE FROM #__wel_venqdtm WHERE wel_quot_no='$this->wel_quot_no'";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					
					//删除主表
					$sql="DELETE FROM #__wel_venqhdm WHERE wel_quot_no='$this->wel_quot_no' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					
				mysql_query("commit");
				
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}
							
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		if($msg_code==""){$msg_code="delete_succee";}		
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	public function approve()
	{
		$msg_code="";
		$return_val=array();
 
		try
		{
			$conn= werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_approve")){throw new Exception("unauthorized_access");}
			
			$sql="SELECT * FROM #__wel_venqhdm WHERE wel_quot_no='$this->wel_quot_no' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_quot_no_not_found");}
			$str_wel_ven_code=is_null($row["wel_ven_code"]) ? "" : $row["wel_ven_code"];
			$str_wel_cur_code=is_null($row["wel_cur_code"]) ? "" : $row["wel_cur_code"];
			$dec_wel_ex_rate=doubleval(is_null($row["wel_ex_rate"]) ? 1 : $row["wel_ex_rate"]);
			
			try
			{
				mysql_query("begin");
			
					$sql="SELECT * FROM #__wel_venqdtm WHERE wel_quot_no='$this->wel_quot_no' AND wel_prmy_quot > 0";
					$sql=revert_to_the_available_sql($sql);
					if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
					while($row=mysql_fetch_array($result))
					{
						$str_wel_part_no=is_null($row["wel_part_no"]) ? "" : $row["wel_part_no"];
						$str_wel_quot_no=is_null($row["wel_quot_no"]) ? "" : $row["wel_quot_no"];
						//$str_wel_cur_code=is_null($row["wel_cur_code"]) ? "" : $row["wel_cur_code"];
						//$dec_wel_ex_rate=1;
						$dec_wel_u_price=doubleval(is_null($row["wel_u_price"]) ? 0 : $row["wel_u_price"]);
						$str_wel_pur_unit=is_null($row["wel_pur_unit"]) ? "" : $row["wel_pur_unit"];    
						$dec_wel_pur_unit_rate=doubleval(is_null($row["wel_pur_unit_rate"]) ? 1 : $row["wel_pur_unit_rate"]);
						$int_wel_lead_tm=intval(is_null($row["wel_lead_tm"]) ? 0 : $row["wel_lead_tm"]);
						$int_wel_min_ord=doubleval(is_null($row["wel_min_ord"]) ? 0 : $row["wel_min_ord"]);
						$int_wel_pkg_ord=doubleval(is_null($row["wel_pkg_ord"]) ? 0 : $row["wel_pkg_ord"]);
						$dec_wel_vqt_cost=0;
					
						//以倉存單位計算 min_ord, pkg_ord, vqt_cost
						$dec_wel_u_price=ROUND($dec_wel_u_price/$dec_wel_pur_unit_rate,6);
						$int_wel_min_ord=$int_wel_min_ord*$dec_wel_pur_unit_rate;
						$int_wel_pkg_ord=$int_wel_pkg_ord*$dec_wel_pur_unit_rate;
						$dec_wel_vqt_cost=ROUND(($dec_wel_u_price*$dec_wel_ex_rate)/$dec_wel_pur_unit_rate,4);

						//***** 为首选报价时，则更新物料档案表资料*************************************
						$sql="SELECT * FROM #__wel_partflm WHERE wel_part_no='$str_wel_part_no' LIMIT 1";
						$sql=revert_to_the_available_sql($sql);
						if(!(($the_result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
						if($rs_part=mysql_fetch_array($the_result))
						{
							//更新物料档案	  	
							$sql="UPDATE #__wel_partflm SET ".
										"wel_quot_no='$str_wel_quot_no',".
										"wel_ven_code='$str_wel_ven_code',".
										"wel_cur_code='$str_wel_cur_code',".
										"wel_u_price='$dec_wel_u_price',".
										"wel_min_ord='$int_wel_min_ord',".
										"wel_pkg_ord='$int_wel_pkg_ord',".
										"wel_lead_tm='$int_wel_lead_tm',".
										"wel_vqt_cost='$dec_wel_vqt_cost' ".
										"WHERE wel_part_no='$str_wel_part_no' LIMIT 1";
							$sql=revert_to_the_available_sql($sql);
							if(!($result1=mysql_query($sql,$conn))){throw new Exception(mysql_error());}  
						}
						mysql_free_result($the_result);
					}   // loop 
			  
					$sql="UPDATE #__wel_venqdtm SET ".
							 "wel_appr_yn=1,".
							 "wel_appr_by='{$_SESSION['wel_user_id']}',".
							 "wel_appr_date=now() ".
						 "WHERE wel_quot_no='$this->wel_quot_no'";     
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			   
					$sql="UPDATE #__wel_venqhdm SET ".
							"wel_appr_yn=1,".
							"wel_appr_by='{$_SESSION['wel_user_id']}',".
							"wel_appr_date=now() ".
						"WHERE wel_quot_no='$this->wel_quot_no' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
					mysql_free_result($result);
			
				mysql_query("commit");
			
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}
			
		} 
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}		
		if($msg_code==""){$msg_code="approve_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;         
	}
	
	public function cancel_approve()
	{
		$msg_code="";
		$return_val=array();
 
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_approve")){throw new Exception("unauthorized_access");}
			
			$sql="SELECT * FROM #__wel_venqhdm WHERE wel_quot_no='$this->wel_quot_no' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_quot_no_not_found");}
			
			try
			{
				mysql_query("begin");
				
					$sql="UPDATE #__wel_venqhdm SET ".
							"wel_appr_yn=0,".
							"wel_appr_by='{$_SESSION['wel_user_id']}',".
							"wel_appr_date=now() ".
						 "WHERE wel_quot_no='$this->wel_quot_no'";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
					
					$sql="UPDATE #__wel_venqdtm SET ".
							 "wel_appr_yn=0,".
							 "wel_appr_by='{$_SESSION['wel_user_id']}',".
							 "wel_appr_date=now() ".
						 "WHERE wel_quot_no='$this->wel_quot_no'";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
					mysql_free_result($result);
				
				mysql_query("commit");
				
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}
					 
		} 
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		if($msg_code==""){$msg_code="cancel_approve_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;  
	}
 
	public function delete_detail_tab0()
	{
		$msg_code="";
		$return_val=array();
 
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_delete")){throw new Exception("unauthorized_access");}
			
			$sql="SELECT * FROM #__wel_venqdtm WHERE wel_quot_no='$this->wel_quot_no' AND wel_quot_line='$this->wel_quot_line' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_quot_detail_not_found");}
			
			try
			{
			
				mysql_query("begin");
				
					$sql="DELETE FROM #__wel_venqdtm WHERE wel_quot_no='$this->wel_quot_no' AND wel_quot_line='$this->wel_quot_line' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
					mysql_free_result($result);
				
				mysql_query("commit");
				
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}
			
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}	
		if($msg_code==""){$msg_code="delete_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}

	public function delete_detail_tab0_all()
	{
		$msg_code="";
		$return_val=array();
		 
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_delete")){throw new Exception("unauthorized_access");}
			
			$sql="SELECT * FROM #__wel_venqdtm WHERE wel_quot_no='$this->wel_quot_no' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_quot_no_not_found");}
			
			try
			{
				mysql_query("begin");
				
					$sql="DELETE FROM #__wel_venqdtm WHERE wel_quot_no='$this->wel_quot_no'";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
					mysql_free_result($result);
				
				mysql_query("commit");
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}
			
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}		
		if($msg_code==""){$msg_code="delete_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
}
?>
