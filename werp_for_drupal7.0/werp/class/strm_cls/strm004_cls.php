<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>

<?php 
	class strm004_cls
	{
		public $wel_pattern="";
		public $wel_pat_des="";
		public $wel_to_nextno=0;
		
		private $wel_prog_code="strm004";
		//读取内部转仓单模式
		public function read()
		{
			$msg_code="";
			$return_val=array();
			
			try
			{
				$conn=werp_db_connect();
				
				if (!check_permission($conn,$this->wel_prog_code,"wel_access_read")){throw new Exception("unauthorized_access");}
				
				$sql="SELECT * FROM #__wel_genttow WHERE wel_pattern='".$this->wel_pattern."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
				if(!($row=mysql_fetch_array($result))){throw new Exception("pattern_not_found");}	
				$int__count=0;
				while ($int__count < mysql_num_fields($result)) 
				{
					$field_name=mysql_fetch_field($result,$int__count)->name;
					$return_val[$field_name]=$row[$field_name];
				$int__count++;
				}
				mysql_free_result($result);
				throw new Exception("");
			}
			catch (Exception $e)
			{
				$msg_code=$e->getMessage();
			}
			$return_val["msg_code"]=$msg_code;
			return $return_val;
		}
		//新增内部转仓单模式
		public function addnew()
		{
			$msg_code="";
			$return_val=array();
			
			try
			{
				$conn=werp_db_connect();
				
				if (!check_permission($conn,$this->wel_prog_code,"wel_access_addnew")){throw new Exception("unauthorized_access");}
				
				if($this->wel_pattern==""){throw new Exception("wel_pattern_miss");}
				if($this->wel_pat_des==""){throw new Exception("wel_pat_des_miss");}
				if((strlen($this->wel_pattern)<2) || (strlen($this->wel_pattern)>4)){throw new Exception("wel_pattern_length_error");}
				if($this->wel_to_nextno==""){$this->wel_to_nextno=0;}
				$this->wel_to_nextno=doubleval($this->wel_to_nextno);
				
				//内部转仓单模式是否存在
				$sql="SELECT * FROM #__wel_genttow WHERE wel_pattern='".$this->wel_pattern."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if($row=mysql_fetch_array($result)){throw new Exception("wel_pattern_exist");}
				
				try
				{
					mysql_query('begin');
					
						//插入记录到内部转仓单模式表中
						$sql="INSERT INTO #__wel_genttow(wel_pattern,wel_pat_des,wel_to_nextno)".
								" VALUES('".$this->wel_pattern."','".$this->wel_pat_des."',".$this->wel_to_nextno.")";
						$sql=revert_to_the_available_sql($sql);
						if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
						mysql_free_result($result);
						
					mysql_query('commit');
					
				}
				catch (Exception $e1)
				{
					mysql_query('rollback');
					throw new Exception($e1->getMessage());
				}
				
			}
			catch (Exception $e)
			{
				$msg_code=$e->getMessage();
			}
			if($msg_code==""){$msg_code="addnew_succee";}
			$return_val["msg_code"]=$msg_code;
			$return_val["wel_pattern"]=$this->wel_pattern;
			return $return_val;
		}
		//编辑内部转仓单模式
		public function edit()
		{
			$msg_code="";
			$return_val=array();
			
			try
			{
				$conn=werp_db_connect();
				
				if (!check_permission($conn,$this->wel_prog_code,"wel_access_edit")){throw new Exception("unauthorized_access");}
				
				if($this->wel_pattern==""){throw new Exception("wel_pattern_miss");}
				if($this->wel_pat_des==""){throw new Exception("wel_pat_des_miss");}
				if($this->wel_to_nextno==""){$this->wel_to_nextno=0;}
				$this->wel_to_nextno=doubleval($this->wel_to_nextno);

				//内部转仓单模式是否存在
				$sql="SELECT * FROM #__wel_genttow WHERE wel_pattern='".$this->wel_pattern."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_pattern_not_found");}
				$int__to_nextno=intval(is_null($row["wel_to_nextno"]) ? 0 : $row["wel_to_nextno"]);

				if($this->wel_to_nextno<$int__to_nextno){throw new Exception("wel_to_nextno_too_small");}
				
				try
				{
					mysql_query('begin');
					
						//更新内部转仓单模式表记录
						$sql="UPDATE #__wel_genttow SET ".
									"wel_pat_des='".$this->wel_pat_des."',".
									"wel_to_nextno=".$this->wel_to_nextno." ".
								"WHERE wel_pattern='".$this->wel_pattern."' LIMIT 1";
						$sql=revert_to_the_available_sql($sql);
						if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
						mysql_free_result($result);
						
					mysql_query('commit');
				}
				catch (Exception $e1)
				{
					mysql_query('rollback');
					throw new Exception($e1->getMessage());
				}
				
			}
			catch (Exception $e)
			{
				$msg_code=$e->getMessage();
			}		
			if($msg_code==""){$msg_code="edit_succee";}
			$return_val["msg_code"]=$msg_code;
			return $return_val;
		}
		//删除内部转仓单模式
		public function delete()
		{
			$msg_code="";
			$return_val=array();
			
			try
			{
				$conn=werp_db_connect();
				
				if (!check_permission($conn,$this->wel_prog_code,"wel_access_delete")){throw new Exception("unauthorized_access");}
				
				if($this->wel_pattern==""){throw new Exception("wel_pattern_miss");}
				
				//内部转仓单模式是否存在
				$sql="SELECT * FROM #__wel_genttow WHERE wel_pattern='".$this->wel_pattern."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_pattern_not_found");}
				
				try
				{
					mysql_query('begin');
					
						//删除内部转仓单模式
						$sql="DELETE FROM #__wel_genttow WHERE wel_pattern='".$this->wel_pattern."' LIMIT 1";
						$sql=revert_to_the_available_sql($sql);
						if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
						mysql_free_result($result);
						
					mysql_query('commit');
					 
				}
				catch (Exception $e1)
				{
					mysql_query('rollback');
					throw new Exception($e1->getMessage());
				}
				
			}
			catch (Exception $e)
			{
				$msg_code=$e->getMessage();
			}		
			if($msg_code==""){$msg_code="delete_succee";}
			$return_val["msg_code"]=$msg_code;
			return $return_val;
		}
	}
?>