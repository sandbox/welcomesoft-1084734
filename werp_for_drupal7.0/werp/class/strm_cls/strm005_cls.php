<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>

<?php 
	class strm005_cls
	{
		public $wel_to_no="";
		public $wel_ctrl_flow="";
		public $wel_pattern="";
		public $wel_req_date="";
		public $wel_to_remark="";
	
		public $wel_to_line=0;

		private $wel_prog_code="strm005";
		
		//读取内部转仓单
		public function read()
		{
			$msg_code="";
			$return_val=array();
			
			try
			{
				$conn=werp_db_connect();
				
				if (!check_permission($conn,$this->wel_prog_code,"wel_access_read")){throw new Exception("unauthorized_access");}
				
				$sql="SELECT h.*,".
					" a.wel_wh_des AS wel_wh_fm_des,".
					" b.wel_wh_des AS wel_wh_to_des".
					" FROM #__wel_torhdrm h".
					" LEFT JOIN #__wel_whlocfm a ON h.wel_wh_fm=a.wel_wh_code".
					" LEFT JOIN #__wel_whlocfm b ON h.wel_wh_to=b.wel_wh_code".
					" WHERE wel_to_no='".$this->wel_to_no."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_to_no_not_found");}	
				$int__count=0;
				while ($int__count < mysql_num_fields($result)) 
				{
					$field_name=mysql_fetch_field($result,$int__count)->name;
					$return_val[$field_name]=$row[$field_name];
					$int__count++;
				}
				mysql_free_result($result);
				
			}
			catch (Exception $e)
			{
				$msg_code=$e->getMessage();
			}
			$return_val["msg_code"]=$msg_code;
			return $return_val;
		}
		
		//新增内部转仓单
		public function addnew()
		{
			$msg_code="";
			$return_val=array();
			
			try
			{
				$conn=werp_db_connect();
				
				if (!check_permission($conn,$this->wel_prog_code,"wel_access_addnew")){throw new Exception("unauthorized_access");}
				
				if(($this->wel_to_no=="") && ($this->wel_pattern=="")) {throw new Exception("wel_to_no_miss");}
				if($this->wel_req_date==""){throw new Exception("wel_req_date_miss");}
				if($this->wel_ctrl_flow==""){throw new Exception("wel_ctrl_flow_miss");}
				
				$this->wel_req_date=(($this->wel_req_date=="") ? "null" : $this->wel_req_date);
				$sql="SELECT * FROM #__wel_closedm LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());} 
				if(!($row=mysql_fetch_array($result))){throw new Exception("system_init_error");}
				$sys_phy_date=is_null($row["wel_period1"]) ? $row["wel_phy_date"] : $row["wel_period1"];

				if( date("Y-m-d",strtotime($sys_phy_date)) >= date("Y-m-d",strtotime($this->wel_req_date)) )
				{
					throw new Exception("wel_req_date_error");
				}

				if($this->wel_pattern!="")
				{
					$sql="SELECT wel_pattern from #__wel_genttow WHERE wel_pattern='".$this->wel_pattern."' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
					if(!($row=mysql_fetch_array($result))){throw new Exception("wel_pattern_not_found");}
				}

				if($this->wel_ctrl_flow!="")
				{
					//流程是否存在
					$sql="SELECT * FROM #__wel_whctrlm  WHERE wel_ctrl_flow='".$this->wel_ctrl_flow."' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
					if(!($row=mysql_fetch_array($result))){throw new Exception("wel_whctrlm_not_found");}
					$int__int_flow=intval(is_null($row["wel_int_flow"]) ? 0 : $row["wel_int_flow"]);
					$int__dis_able=intval(is_null($row["wel_dis_able"]) ? 0 : $row["wel_dis_able"]);
					$str__wh_fm=is_null($row["wel_wh_fm"]) ? "" : $row["wel_wh_fm"];
					$str__wh_to=is_null($row["wel_wh_to"]) ? "" : $row["wel_wh_to"];
						
					if($int__int_flow==0){throw new Exception("wel_whctrlm_not_ito");}
					if($int__dis_able!=0){throw new Exception("wel_whctrlm_disable");}

					//从货仓是否存在
					$sql="SELECT * FROM #__wel_whlocfm WHERE wel_wh_code='".$str__wh_fm."' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
					if(!($row=mysql_fetch_array($result))){throw new Exception("wel_wh_fm_not_found");}	
						
					//至货仓是否存在
					$sql="SELECT * FROM #__wel_whlocfm WHERE wel_wh_code='".$str__wh_to."' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
					if(!($row=mysql_fetch_array($result))){throw new Exception("wel_wh_to_not_found");}	
				}

				try
				{
					mysql_query('begin');
					
					if ($this->wel_pattern!="")
					{
						$sql="SELECT * FROM #__wel_genttow WHERE wel_pattern='$this->wel_pattern' LIMIT 1";
						$sql=revert_to_the_available_sql($sql);
						if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
						if(!($row=mysql_fetch_array($result))){throw new Exception("wel_pattern_not_found");}
						$wel_pattern=stripslashes(trim($row["wel_pattern"]));
						$wel_to_nextno=trim($row["wel_to_nextno"])+1;
						$wel_to_nextno=sprintf("%'08s",$wel_to_nextno);

						if(strlen($wel_to_nextno)>8){throw new Exception(wel_pattern_overflow);}
						$this->wel_to_no=$wel_pattern.$wel_to_nextno;

						$sql="UPDATE #__wel_genttow set wel_to_nextno=IFNULL(wel_to_nextno,0)+1 WHERE wel_pattern='$this->wel_pattern'";
						$sql=revert_to_the_available_sql($sql);
						if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					}

					$sql="SELECT wel_to_no from #__wel_torhdrm WHERE wel_to_no='".$this->wel_to_no."' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
					if($row=mysql_fetch_array($result)){throw new Exception("wel_to_no_exist");}
						
					$sql="INSERT INTO #__wel_torhdrm SET ".
							"wel_to_no='$this->wel_to_no',".
							"wel_pattern='$this->wel_pattern',".
							"wel_ctrl_flow='$this->wel_ctrl_flow',".
							"wel_wh_fm='$str__wh_fm',".
							"wel_wh_to='$str__wh_to',".
							"wel_req_date='$this->wel_req_date',".
							"wel_to_remark='$this->wel_to_remark',".
							"wel_crt_user='{$_SESSION['wel_user_id']}',".
							"wel_crt_date=now()";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}

					mysql_query('commit');
					
				}
				catch (Exception $e1)
				{
					mysql_query('rollback');
					$msg_code=$e1->getMessage();
				}
				
			}
			catch (Exception $e)
			{
				$msg_code=$e->getMessage();
			}
			if($msg_code==""){$msg_code="addnew_succee";}
			$return_val["msg_code"]=$msg_code;
			$return_val["wel_to_no"]=$this->wel_to_no;
			return $return_val;
		}
		
		//编辑内部转仓单
		public function edit()
		{
			$msg_code="";
			$return_val=array();
			
			try
			{
				$conn=werp_db_connect();
				
				if (!check_permission($conn,$this->wel_prog_code,"wel_access_edit")){throw new Exception("unauthorized_access");}
				
				//if($this->wel_to_no=="") {throw new Exception("wel_to_no_miss");}
				if($this->wel_req_date==""){throw new Exception("wel_req_date_miss");}
				$this->wel_req_date=(($this->wel_req_date=="") ? "null" : $this->wel_req_date);

				$sql="SELECT * FROM #__wel_closedm LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());} 
				if(!($row=mysql_fetch_array($result))){throw new Exception("system_init_error");}
				$sys_phy_date=is_null($row["wel_period1"]) ? $row["wel_phy_date"] : $row["wel_period1"];

				if( date("Y-m-d",strtotime($sys_phy_date)) >= date("Y-m-d",strtotime($this->wel_req_date)) )
				{
					throw new Exception("wel_req_date_error");
				}

				//内部转仓单是否存在
				$sql="SELECT wel_to_no FROM #__wel_torhdrm WHERE wel_to_no='".$this->wel_to_no."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_to_no_not_found");}
				
				try
				{
					mysql_query('begin');
					
						//更新记录
						$sql="UPDATE #__wel_torhdrm SET ".
									"wel_req_date='$this->wel_req_date',".
									"wel_to_remark='$this->wel_to_remark',".
									"wel_upd_user='{$_SESSION['wel_user_id']}',".
									"wel_upd_date=now() ".
								"WHERE wel_to_no='".$this->wel_to_no."' LIMIT 1";
						$sql=revert_to_the_available_sql($sql);
						if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
						mysql_free_result($result);
						
					mysql_query('commit');
					
				}
				catch (Exception $e1)
				{
					mysql_query('rollback');
					$msg_code=$e1->getMessage();
				}
				
			}
			catch (Exception $e)
			{
				$msg_code=$e->getMessage();
			}		
			if($msg_code==""){$msg_code="edit_succee";}
			$return_val["msg_code"]=$msg_code;
			return $return_val;
		}
		
		//删除内部转仓单
		public function delete()
		{
			$msg_code="";
			$return_val=array();
			
			try
			{
				$conn=werp_db_connect();
				
				if (!check_permission($conn,$this->wel_prog_code,"wel_access_delete")){throw new Exception("unauthorized_access");}
				
				if($this->wel_to_no==""){throw new Exception("wel_to_no_miss");}
				
				//内部转仓单是否存在
				$sql="SELECT * FROM #__wel_torhdrm WHERE wel_to_no='".$this->wel_to_no."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_to_no_not_found");}
				
				$sql="SELECT * FROM #__wel_tordetm WHERE wel_to_no='".$this->wel_to_no."'";
				$sql=revert_to_the_available_sql($sql);
				if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
				while($row=mysql_fetch_array($result))
				{
					$ito_post_qty=doubleval(is_null($row["wel_post_qty"]) ? 0 : $row["wel_post_qty"]);
					if( $ito_post_qty>0 ){throw new Exception("wel_req_qty_posted");}
				}

				try
				{
					mysql_query('begin');
					
						//删除内部转仓单明细
						$sql="DELETE FROM #__wel_tordetm WHERE wel_to_no='".$this->wel_to_no."'";
						$sql=revert_to_the_available_sql($sql);
						if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
					
						//删除内部转仓单
						$sql="DELETE FROM #__wel_torhdrm WHERE wel_to_no='".$this->wel_to_no."' LIMIT 1";
						$sql=revert_to_the_available_sql($sql);
						if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
						mysql_free_result($result);
						
					mysql_query('commit');
					
				}
				catch (Exception $e1)
				{
					mysql_query('rollback');
					$msg_code=$e1->getMessage();
				}
				
			}
			catch (Exception $e)
			{
				$msg_code=$e->getMessage();
			}		
			if($msg_code==""){$msg_code="delete_succee";}
			$return_val["msg_code"]=$msg_code;
			return $return_val;
		}
		
		//删除内部转仓单明细
		public function delete_detail_tab0()
		{
			$msg_code="";
			$return_val=array();
			
			try
			{
				$conn=werp_db_connect();
				
				if (!check_permission($conn,$this->wel_prog_code,"wel_access_delete")){throw new Exception("unauthorized_access");}
				
				$sql="SELECT * FROM #__wel_tordetm".
					" WHERE wel_to_no='".$this->wel_to_no."' AND".
					" wel_to_line=".$this->wel_to_line." LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}	
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_to_detail_not_found");}	
				$ito_post_qty=doubleval(is_null($row["wel_post_qty"]) ? 0 : $row["wel_post_qty"]);

				if( $ito_post_qty>0 ){throw new Exception("wel_req_qty_posted");}

				try
				{
					mysql_query('begin');

						$sql="DELETE FROM #__wel_tordetm WHERE wel_to_no='".$this->wel_to_no."' AND wel_to_line=".$this->wel_to_line." LIMIT 1";
						$sql=revert_to_the_available_sql($sql);
						if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}

					mysql_query('commit');
					
				}
				catch (Exception $e1)
				{
					mysql_query('rollback');
					$msg_code=$e1->getMessage();
				}
				
				mysql_free_result($result);
				
			}
			catch (Exception $e)
			{
				$msg_code=$e->getMessage();
			}
			if($msg_code==""){$msg_code="delete_succee";}	
			$return_val["msg_code"]=$msg_code;
			return $return_val;
		}
		
		//内部转仓单明细全部删除
		public function delete_detail_tab0_all()
		{
			$msg_code="";
			$return_val=array();
			
			try
			{
				$conn=werp_db_connect();
				
				if (!check_permission($conn,$this->wel_prog_code,"wel_access_delete")){throw new Exception("unauthorized_access");}
				
				$sql="SELECT * FROM #__wel_tordetm WHERE wel_to_no='".$this->wel_to_no."'";
				$sql=revert_to_the_available_sql($sql);
				if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
				while($row=mysql_fetch_array($result))
				{
					$ito_post_qty=doubleval(is_null($row["wel_post_qty"]) ? 0 : $row["wel_post_qty"]);
					if( $ito_post_qty>0 ){throw new Exception("wel_req_qty_posted");}
				}

				try
				{
					mysql_query('begin');

						$sql="DELETE FROM #__wel_tordetm WHERE wel_to_no='".$this->wel_to_no."'";
						$sql=revert_to_the_available_sql($sql);
						if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}

					mysql_query('commit');
					
				}
				catch (Exception $e1)
				{
					mysql_query('rollback');
					$msg_code=$e1->getMessage();
				}
				
				mysql_free_result($result);
				
			}
			catch (Exception $e)
			{
				$msg_code=$e->getMessage();
			}
			if($msg_code==""){$msg_code="delete_succee";}	
			$return_val["msg_code"]=$msg_code;
			return $return_val;
		}
	}
?>