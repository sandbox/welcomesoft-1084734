<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>

<?php 
	class strm005a_cls
	{
		public $wel_to_no="";
		public $wel_to_line=0;
		public $wel_req_date="";
		public $wel_ctrl_flow="";
		public $wel_wh_fm="";
		public $wel_wh_to="";
		public $wel_part_no="";
		public $wel_lot_no_fm="";
		public $wel_lot_no_to="";
		public $wel_req_qty=0;
		public $wel_ref_no="";
		public $wel_to_rmk="";
		
		private $wel_prog_code="strm005";
		
		//读取内部转仓明细
		public function read()
		{
			$msg_code="";
			$return_val=array();
			
			try
			{
				$conn=werp_db_connect();
				
				if (!check_permission($conn,$this->wel_prog_code,"wel_access_read")){throw new Exception("unauthorized_access");}
				
				//内部转仓单明细是否存在
				$sql="SELECT d.*,p.wel_part_des as wel_part_des,p.wel_part_des1 as wel_part_des1,p.wel_part_des2 as wel_part_des2,p.wel_unit as wel_unit".
					" FROM #__wel_tordetm d".
					" LEFT JOIN #__wel_partflm p ON d.wel_part_no=p.wel_part_no ".
					" WHERE d.wel_to_no='".$this->wel_to_no."' AND d.wel_to_line=".$this->wel_to_line." LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_to_detail_not_found");}
				$ito_part_no=is_null($row["wel_part_no"]) ? "" : $row["wel_part_no"];
				$ito_lot_no=is_null($row["wel_lot_no"]) ? "" : $row["wel_lot_no"];
				
				$int__count=0;
				while ($int__count < mysql_num_fields($result)) 
				{
					$field_name=mysql_fetch_field($result,$int__count)->name;
					$return_val[$field_name]=$row[$field_name];
					$int__count++;
				}
				
				//物品是否存在
				$sql="SELECT *".
					" FROM #__wel_whlotfm".
					" WHERE wel_wh_code='".$this->wel_wh_fm."'".
					" AND wel_part_no='".$ito_part_no."'".
					" AND wel_lot_no='".$ito_lot_no."' AND wel_stk_loc='' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
				if(!($row=mysql_fetch_array($result)))
				{
					$return_val["wel_stk_qty"]=0;
				}else{
					$return_val["wel_stk_qty"]=doubleval(is_null($row["wel_stk_qty"]) ? 0 : $row["wel_stk_qty"]);
				}

				mysql_free_result($result);
			}
			catch (Exception $e)
			{
				$msg_code=$e->getMessage();
			}
			$return_val["msg_code"]=$msg_code;
			return $return_val;
		}
		
		//添加内部转仓明细
		public function addnew()
		{
			$msg_code="";
			$return_val=array();
			
			try
			{
				$conn=werp_db_connect();
				
				if (!check_permission($conn,$this->wel_prog_code,"wel_access_addnew")){throw new Exception("unauthorized_access");}
				
				if($this->wel_part_no==""){throw new Exception("wel_part_no_miss");}

				if(!is_numeric($this->wel_req_qty)){$this->wel_req_qty=0;}
				if($this->wel_req_qty==0){throw new Exception("wel_req_qty_miss");}
				if($this->wel_req_qty<0){throw new Exception("wel_req_qty_error");}
				$this->wel_req_qty=doubleval($this->wel_req_qty);

				if($this->wel_part_no!="")
				{
					$sql="SELECT wel_part_no from #__wel_partflm WHERE wel_part_no='".$this->wel_part_no."' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
					if(!($row=mysql_fetch_array($result))){throw new Exception("wel_part_no_not_found");}
				}

				//Get Info from ITO Header
				$sql="SELECT * FROM #__wel_torhdrm WHERE wel_to_no='".$this->wel_to_no."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_to_no_not_found");}
				$ito_last_line=intval(is_null($row["wel_last_line"]) ? 0 : $row["wel_last_line"])+1;

				//Check same part_no, same lot_no detail
				$sql="SELECT * FROM #__wel_tordetm".
					" WHERE wel_to_no='".$this->wel_to_no."' AND".
					" wel_part_no='".$this->wel_part_no."' AND".
					" wel_lot_no_fm='".$this->wel_lot_no_fm."' AND".
					" wel_lot_no_to='".$this->wel_lot_no_to."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
				if($row=mysql_fetch_array($result)){throw new Exception("wel_to_detail_duplicate");}

				// Check ITO Line exist
				$sql="SELECT wel_to_no,wel_to_line FROM #__wel_tordetm ".
					"WHERE wel_to_no='$this->wel_to_no' AND ".
					"wel_to_line=".$ito_last_line." LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				if($row=mysql_fetch_array($result))
				{
					$sql="SELECT max(wel_to_line) as max_line FROM #__wel_tordetm ".
						"WHERE wel_to_no='$this->wel_to_no' ";
					$sql=revert_to_the_available_sql($sql);
					if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					if(!$row=mysql_fetch_array($result)){throw new Exception("wel_to_detail_not_found");}
					$ito_last_line=intval(is_null($row["max_line"]) ? 0 : $row["max_line"])+1;
				}
				
				try
				{
					mysql_query('begin');
						
						$sql="INSERT INTO #__wel_tordetm SET ".
							"wel_to_no='".$this->wel_to_no."',".
							"wel_to_line=".$ito_last_line.",".
							"wel_part_no='".$this->wel_part_no."',".
							"wel_lot_no_fm='".$this->wel_lot_no_fm."',".
							"wel_lot_no_to='".$this->wel_lot_no_to."',".
							"wel_req_qty=".$this->wel_req_qty.",".
							"wel_ref_no='".$this->wel_ref_no."',".
							"wel_to_rmk='".$this->wel_to_rmk."',".
							"wel_crt_user='{$_SESSION['wel_user_id']}', ".
							"wel_crt_date=now()";
						$sql=revert_to_the_available_sql($sql);
						if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}

						$sql="UPDATE #__wel_torhdrm SET".
							" wel_last_line=".$ito_last_line.",".
							" wel_upd_user='{$_SESSION['wel_user_id']}',".
							" wel_upd_date=now()".
							" WHERE wel_to_no='".$this->wel_to_no."' LIMIT 1";
						$sql=revert_to_the_available_sql($sql);
						if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}

					mysql_query('commit');
				}
				catch(Exception $e1)
				{
					mysql_query('rollback');
					throw new Exception($e1->getMessage());
				}
			}
			catch(Exception $e)
			{
				$msg_code=$e->getMessage();
			}
			if($msg_code==""){$msg_code="addnew_succee";}	
			$return_val["msg_code"]=$msg_code;
			return $return_val;
		}
		
		//编辑内部转仓明细
		public function edit()
		{
			$msg_code="";
			$return_val=array();
			try
			{
				$conn=werp_db_connect();
				
				if (!check_permission($conn,$this->wel_prog_code,"wel_access_edit")){throw new Exception("unauthorized_access");}
				
				if($this->wel_part_no==""){throw new Exception("wel_part_no_miss");}

				if(!is_numeric($this->wel_req_qty)){$this->wel_req_qty=0;}
				if($this->wel_req_qty==0){throw new Exception("wel_req_qty_miss");}
				if($this->wel_req_qty<0){throw new Exception("wel_req_qty_error");}
				$this->wel_req_qty=doubleval($this->wel_req_qty);

				if($this->wel_part_no!="")
				{
					$sql="SELECT wel_part_no from #__wel_partflm WHERE wel_part_no='".$this->wel_part_no."' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
					if(!($row=mysql_fetch_array($result))){throw new Exception("wel_part_no_not_found");}
				}

				//get old info from ITO detail
				$sql="SELECT * FROM #__wel_tordetm".
					" WHERE wel_to_no='".$this->wel_to_no."' AND wel_to_line=".$this->wel_to_line." LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_to_detail_not_found");}
				$old_req_qty=doubleval(is_null($row["wel_req_qty"]) ? 0 : $row["wel_req_qty"]);
				$old_post_qty=doubleval(is_null($row["wel_post_qty"]) ? 0 : $row["wel_post_qty"]);
				
				if($this->wel_req_qty<$old_post_qty){throw new Exception("wel_req_qty_excess_posted");}

				//内部转仓单明细是否存在相同的品号、批号与仓位
				$sql="SELECT * FROM #__wel_tordetm".
					" WHERE wel_to_no='".$this->wel_to_no."' AND".
					" wel_to_line<>".$this->wel_to_line." AND".
					" wel_part_no='".$this->wel_part_no."' AND".
					" wel_lot_no_fm='".$this->wel_lot_no_fm."' AND".
					" wel_lot_no_to='".$this->wel_lot_no_to."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
				if($row=mysql_fetch_array($result)){throw new Exception("wel_to_detail_duplicate");}
			
				try
				{
					mysql_query('begin');
					
						$sql="UPDATE #__wel_tordetm SET".
							" wel_lot_no_fm='".$this->wel_lot_no_fm."',".
							" wel_lot_no_to='".$this->wel_lot_no_to."',".
							" wel_req_qty=".$this->wel_req_qty.",".
							" wel_ref_no='".$this->wel_ref_no."',".
							" wel_to_rmk='".$this->wel_to_rmk."' ".
							" WHERE wel_to_no='".$this->wel_to_no."' AND".
							" wel_to_line=".$this->wel_to_line." LIMIT 1";
						$sql=revert_to_the_available_sql($sql);
						if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}

						$sql="UPDATE #__wel_torhdrm SET".
							" wel_upd_user='{$_SESSION['wel_user_id']}',".
							" wel_upd_date=now()".
							" WHERE wel_to_no='".$this->wel_to_no."' LIMIT 1";
						$sql=revert_to_the_available_sql($sql);
						if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}

					mysql_query('commit');
				}
				catch (Exception $e1)
				{
					mysql_query('rollback');
					$msg_code=$e1->getMessage();
				}
				
			}
			catch (Exception $e)
			{
				$msg_code=$e->getMessage();
			}	
			if($msg_code==""){$msg_code="edit_succee";}	
			$return_val["msg_code"]=$msg_code;
			return $return_val;
		}
	}
?>