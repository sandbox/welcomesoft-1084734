<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>

<?php 
	class strm006b_cls
	{
		public $wel_ctrl_flow="";
		public $wel_tran_date="";
		public $wel_wh_fm="";
		public $wel_wh_to="";
		public $wel_wo_no="";
		public $wel_wo_line=0;
		public $wel_part_no="";
		public $wel_alt_part_no="";
		public $wel_lot_no="";
		public $wel_tran_qty=0;
		public $wel_ref_no="";
		public $wel_tran_rmk="";
		
		private $wel_prog_code="strm006";
		
		//读取
		public function read()
		{
			$msg_code="";
			$return_val=array();
			
			try
			{
				$conn=werp_db_connect();
				
				if (!check_permission($conn,$this->wel_prog_code,"wel_access_read")){throw new Exception("unauthorized_access");}
				
				//工作单明细是否存在
				$sql="SELECT * FROM #__wel_wordetm WHERE wel_wo_no='".$this->wel_wo_no."' AND wel_wo_line=".$this->wel_wo_line." LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_wo_detail_not_found");}
				$int__count=0;
				while ($int__count < mysql_num_fields($result)) 
				{
					$field_name=mysql_fetch_field($result,$int__count)->name;
					$return_val[$field_name]=$row[$field_name];
					$int__count++;
				}

				$tmp_part_no=is_null($row["wel_part_no"]) ? "" : $row["wel_part_no"];

				//物料档案
				$sql="SELECT * FROM #__wel_partflm WHERE wel_part_no='".$tmp_part_no."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
				if($row=mysql_fetch_array($result))
				{
					$return_val["wel_part_des"]=$row[wel_part_des];
					$return_val["wel_part_des1"]=$row[wel_part_des1];
					$return_val["wel_part_des2"]=$row[wel_part_des2];
					$return_val["wel_part_des3"]=$row[wel_part_des3];
					$return_val["wel_part_des4"]=$row[wel_part_des4];
					$return_val["wel_unit"]=$row[wel_unit];
				}else{
					$return_val["wel_part_des"]="";
					$return_val["wel_part_des1"]="";
					$return_val["wel_part_des2"]="";
					$return_val["wel_part_des3"]="";
					$return_val["wel_part_des4"]="";
					$return_val["wel_unit"]="";
				}
			}
			catch (Exception $e)
			{
				$msg_code=$e->getMessage();
			}
			$return_val["msg_code"]=$msg_code;
			return $return_val;
		}
		
		//发料
		public function edit()
		{
			$msg_code="";
			$return_val=array();
			
			try
			{
				$conn=werp_db_connect();
				
				if (!check_permission($conn,$this->wel_prog_code,"wel_access_edit")){throw new Exception("unauthorized_access");}
				
				if($this->wel_ctrl_flow==""){throw new Exception("wel_ctrl_flow_miss");}
				if($this->wel_tran_date==""){throw new Exception("wel_tran_date_miss");}
				if($this->wel_wo_no==""){throw new Exception("wel_wo_no_miss");}
				if($this->wel_part_no==""){throw new Exception("wel_part_no_miss");}
				if($this->wel_alt_part_no==""){throw new Exception("wel_alt_part_no_miss");}

				if(!is_numeric($this->wel_wo_line)) {throw new Exception("wel_wo_line_miss");}
				$this->wel_wo_line=doubleval($this->wel_wo_line);
				if($this->wel_wo_line==0){throw new Exception("wel_wo_line_miss");}

				if(!is_numeric($this->wel_tran_qty)) {throw new Exception("wel_tran_qty_miss");}
				$this->wel_tran_qty=doubleval($this->wel_tran_qty);
				if($this->wel_tran_qty==0){throw new Exception("wel_tran_qty_miss");}

				if($this->wel_ctrl_flow!="")
				{
					$sql="SELECT * FROM #__wel_whctrlm WHERE wel_ctrl_flow='".$this->wel_ctrl_flow."' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
					if(!($row=mysql_fetch_array($result))){throw new Exception("wel_whctrlm_not_found");}	
					$str__wh_fm=is_null($row["wel_wh_fm"]) ? "" : $row["wel_wh_fm"];
					$str__wh_to=is_null($row["wel_wh_to"]) ? "" : $row["wel_wh_to"];
					$int__dis_able=intval(is_null($row["wel_dis_able"]) ? 0 : $row["wel_dis_able"]);
					$int__iss_flow=intval(is_null($row["wel_iss_flow"]) ? 0 : $row["wel_iss_flow"]);

					if($int__iss_flow!=1){throw new Exception("wel_whctrlm_not_iss_flow");}
					if($int__dis_able==1){throw new Exception("wel_whctrlm_disable");}

					$sql="SELECT * FROM #__wel_whlocfm WHERE wel_wh_code='".$str__wh_fm."' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
					if(!($row=mysql_fetch_array($result))){throw new Exception("wel_wh_fm_not_found");}	
				
					$sql="SELECT * FROM #__wel_whlocfm WHERE wel_wh_code='".$str__wh_to."' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
					if(!($row=mysql_fetch_array($result))){throw new Exception("wel_wh_to_not_found");}	
				}

				if($this->wel_tran_date!="")
				{
					$this->wel_tran_date=(($this->wel_tran_date=="") ? "null" : $this->wel_tran_date);
					$sql="SELECT * FROM #__wel_closedm LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());} 
					if(!($row=mysql_fetch_array($result))){throw new Exception("system_init_error");}
					$sys_phy_date=is_null($row["wel_period1"]) ? $row["wel_phy_date"] : $row["wel_period1"];

					if( date("Y-m-d",strtotime($sys_phy_date)) >= date("Y-m-d",strtotime($this->wel_tran_date)) )
					{
						throw new Exception("wel_req_date_error");
					}
				}

				//工作单明细是否存在
				$sql="SELECT * FROM #__wel_worhdrm ".
						"WHERE wel_wo_no='".$this->wel_wo_no."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_wo_detail_not_found");}
				$wo_model_no=is_null($row["wel_part_no"]) ? "" : $row["wel_part_no"];
				$wo_model_qty=doubleval(is_null($row["wel_req_qty"]) ? 0 : $row["wel_req_qty"]);

				$sql="SELECT * FROM #__wel_wordetm ".
						"WHERE wel_wo_no='".$this->wel_wo_no."' AND wel_wo_line='".$this->wel_wo_line."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_wo_detail_not_found");}
				$wo_part_no=is_null($row["wel_part_no"]) ? "" : $row["wel_part_no"];
				$wo_assm_no=is_null($row["wel_assm_no"]) ? "" : $row["wel_assm_no"];
				$wo_req_qty=doubleval(is_null($row["wel_req_qty"]) ? 0 : $row["wel_req_qty"]);
				$wo_iss_qty=doubleval(is_null($row["wel_iss_qty"]) ? 0 : $row["wel_iss_qty"]);
				$wo_qty_per=doubleval(is_null($row["wel_qty_per"]) ? 0 : $row["wel_qty_per"]);
				$wo_qp_ratio=doubleval(is_null($row["wo_qp_ratio"]) ? 0 : $row["wo_qp_ratio"]);

				$sql="SELECT * FROM #__wel_partflm WHERE wel_part_no='".$this->wel_alt_part_no."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_part_no_error");}
				$part_cat_code=is_null($row["wel_cat_code"]) ? "" : $row["wel_cat_code"];
				$part_std_cost=doubleval(is_null($row["wel_std_cost"]) ? 0 : $row["wel_std_cost"]);
				$part_avg_cost=doubleval(is_null($row["wel_avg_cost"]) ? 0 : $row["wel_avg_cost"]);

				//代用物料是否存在
				$sql="SELECT * FROM #__wel_altparm ".
					" WHERE wel_prod_no='".$wo_model_no."'".
					" AND wel_assm_no='".$wo_assm_no."'".
					" AND wel_part_no='".$wo_part_no."' ".
					" AND wel_alt_part='".$this->wel_alt_part_no."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_alt_part_not_found");}
				$alt_ex_qty=doubleval(is_null($row["wel_ex_qty"]) ? 1 : $row["wel_ex_qty"]);
				if($alt_ex_qty==0){$alt_ex_qty=1;}

				if($this->wel_tran_qty<0){throw new Exception("wel_tran_qty_excess_wo");}
				if($wo_req_qty-$wo_iss_qty-$this->wel_tran_qty/$alt_ex_qty < 0){throw new Exception("wel_tran_qty_excess_wo");}

				//物品仓存数
				$stk_stk_qty = 0;
				$sql="SELECT * FROM #__wel_whlotfm ".
					" WHERE wel_wh_code='".$str__wh_fm."' AND wel_part_no='".$this->wel_alt_part_no."' ".
					" AND wel_lot_no='".$this->wel_lot_no."' AND wel_stk_loc='' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
				if($row=mysql_fetch_array($result))
				{
					$stk_stk_qty=doubleval(is_null($row["wel_stk_qty"]) ? 0 : $row["wel_stk_qty"]);
				}
				if($stk_stk_qty-$this->wel_tran_qty<0){throw new Exception("wel_tran_qty_excess_stock");}

				try
				{
					mysql_query('begin');
					
						//获得最新交易记录号
						$sql="SELECT * FROM #__wel_tranflm ORDER BY wel_trn_recno DESC LIMIT 1";
						$sql=revert_to_the_available_sql($sql);
						if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
						if(!($row=mysql_fetch_array($result)))
						{
							$dec__next_trn_recno=1;
						}
						else
						{
							$dec__next_trn_recno=doubleval(is_null($row["wel_trn_recno"]) ? 0 : $row["wel_trn_recno"])+1;
						}
						
						//reduce W/O detail qty
						$sql="UPDATE #__wel_wordetm SET ".
							" wel_req_qty=IFNULL(wel_req_qty,0)-$this->wel_tran_qty/$alt_ex_qty ".
							" WHERE wel_wo_no='".$this->wel_wo_no."' ".
							" AND wel_wo_line='$this->wel_wo_line' LIMIT 1";
						$sql=revert_to_the_available_sql($sql);
						if(!($result1=mysql_query($sql,$conn))){throw new Exception(mysql_error());}

						//Add new alternate W/O detail qty
						$sql="SELECT * FROM #__wel_wordetm ".
							" WHERE wel_wo_no='".$this->wel_wo_no."'".
							" AND wel_assm_no='".$wo_assm_no."'".
							" AND wel_part_no='".$this->wel_alt_part_no."' LIMIT 1";
						$sql=revert_to_the_available_sql($sql);
						if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
						if($row=mysql_fetch_array($result))
						{
							// Old WO detail exist
							$wo_new_line=doubleval(is_null($row["wel_wo_line"]) ? 0 : $row["wel_wo_line"]);

							$sql="UPDATE #__wel_wordetm SET ".
								" wel_req_qty=IFNULL(wel_req_qty,0)+$this->wel_tran_qty,".
								" wel_iss_qty=IFNULL(wel_iss_qty,0)+$this->wel_tran_qty".
								" WHERE wel_wo_no='".$this->wel_wo_no."' ".
								" AND wel_wo_line='$wo_new_line' LIMIT 1";
							$sql=revert_to_the_available_sql($sql);
							if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
						}else{
							// Get last WO line
							$sql="SELECT * FROM #__wel_worhdrm".
								" WHERE wel_wo_no='".$this->wel_wo_no."' LIMIT 1";
							$sql=revert_to_the_available_sql($sql);
							if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
							if(!$row=mysql_fetch_array($result)){throw new Exception("wel_wo_detail_not_found");}
							$wo_new_line=doubleval(is_null($row["wel_last_line"]) ? 0 : $row["wel_last_line"]);

							$sql="INSERT INTO #__wel_wordetm SET ".
									"wel_wo_no='".$this->wel_wo_no."',".
									"wel_wo_line=".$wo_new_line.",".
									"wel_assm_no='".$wo_assm_no."',".
									"wel_part_no='".$this->wel_alt_part_no."',".
									"wel_cat_code='".$part_cat_code."',".
									"wel_req_qty=".$this->wel_tran_qty.", ".
									"wel_iss_qty=".$this->wel_tran_qty.", ".
									"wel_qty_per=".$wo_qty_per/$alt_ex_qty.", ".
									"wel_qp_ratio=".$wo_qp_ratio/$alt_ex_qty.", ".
									"wel_std_cost=".$part_std_cost.", ".
									"wel_avg_cost=".$part_avg_cost.", ".
									"wel_crt_user='{$_SESSION['wel_user_id']}', ".
									"wel_crt_date=now()";
							$sql=revert_to_the_available_sql($sql);
							if(!($result1=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
						}

						//添加记录到tranflm表
						$sql="INSERT INTO #__wel_tranflm SET ".
								"wel_trn_recno=".$dec__next_trn_recno.",".
								"wel_tran_date='".$this->wel_tran_date."',".
								"wel_wh_fm='".$str__wh_fm."',".
								"wel_wh_to='".$str__wh_to."',".
								"wel_tran_code='".$this->wel_ctrl_flow."',".
								"wel_flow_code='CIN',".
								"wel_wo_no='".$this->wel_wo_no."',".
								"wel_part_no='".$this->wel_alt_part_no."',".
								"wel_cat_code='".$part_cat_code."',".
								"wel_lot_no_fm='".$this->wel_lot_no."',".
								"wel_lot_no_to='',".
								"wel_stk_loc_fm='',".
								"wel_stk_loc_to='',".
								"wel_tran_qty=".$this->wel_tran_qty.",".
								"wel_ref_no='".$this->wel_ref_no."',".
								"wel_tran_rmk='".$this->wel_tran_rmk."',".
								"wel_std_cost=".$part_std_cost.",".
								"wel_avg_cost=".$part_avg_cost.",".
								"wel_assm_no='".$wo_assm_no."',".
								"wel_crt_user='{$_SESSION['wel_user_id']}', ".
								"wel_crt_date=now()";
						$sql=revert_to_the_available_sql($sql);
						if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}

						//更新物品仓存数
						$sql="UPDATE #__wel_whlotfm SET ".
							" wel_stk_qty=IFNULL(wel_stk_qty,0)-$this->wel_tran_qty ".
							" WHERE wel_wh_code='".$str__wh_fm."' AND wel_part_no='".$this->wel_alt_part_no."' ".
							" AND wel_lot_no='".$this->wel_lot_no."' AND wel_stk_loc='' LIMIT 1";
						$sql=revert_to_the_available_sql($sql);
						if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}

					mysql_query('commit');
					
				}
				catch (Exception $e1)
				{
					mysql_query('rollback');
					throw new Exception($e1->getMessage());
				}
				
			}
			catch (Exception $e)
			{
				$msg_code=$e->getMessage();
			}	
			if($msg_code==""){$msg_code="edit_succee";}	
			$return_val["msg_code"]=$msg_code;
			return $return_val;
		}

	}
?>