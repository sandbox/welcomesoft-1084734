<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>

<?php 
	class strm009_cls
	{
		public $wel_ctrl_flow="";
		public $wel_tran_date="";
		public $wel_pt_no="";
		
		private $wel_prog_code="strm009";
		
		//读取收货单入仓
		public function read()
		{
			$msg_code="";
			$return_val=array();
			
			try
			{
				$conn=werp_db_connect();
				
				if (!check_permission($conn,$this->wel_prog_code,"wel_access_read")){throw new Exception("unauthorized_access");}
				
				//流程是否存在
				if($this->wel_ctrl_flow==""){throw new Exception("wel_ctrl_flow_miss");}
				if($this->wel_pt_no==""){throw new Exception("wel_pt_no_miss");}
				if($this->wel_tran_date==""){throw new Exception("wel_tran_date_miss");}

				if($this->wel_ctrl_flow!="")
				{
					$sql="SELECT * FROM #__wel_whctrlm WHERE wel_ctrl_flow='".$this->wel_ctrl_flow."' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
					if(!($row=mysql_fetch_array($result))){throw new Exception("wel_whctrlm_not_found");}	
					$str__wh_fm=is_null($row["wel_wh_fm"]) ? "" : $row["wel_wh_fm"];
					$str__wh_to=is_null($row["wel_wh_to"]) ? "" : $row["wel_wh_to"];
					$int__dis_able=intval(is_null($row["wel_dis_able"]) ? 0 : $row["wel_dis_able"]);
					$int__rec_flow=intval(is_null($row["wel_rec_flow"]) ? 0 : $row["wel_rec_flow"]);

					$int__count=0;
					while ($int__count < mysql_num_fields($result)) 
					{
						$field_name=mysql_fetch_field($result,$int__count)->name;
						$return_val[$field_name]=$row[$field_name];
						$int__count++;
					}

					if($int__rec_flow!=1){throw new Exception("wel_whctrlm_not_rec_flow");}
					if($int__dis_able==1){throw new Exception("wel_whctrlm_disable");}

					$sql="SELECT * FROM #__wel_whlocfm WHERE wel_wh_code='".$str__wh_fm."' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
					if(!($row=mysql_fetch_array($result))){throw new Exception("wel_wh_fm_not_found");}	
					$return_val[wel_wh_fm_des]=$row[wel_wh_des];
				
					$sql="SELECT * FROM #__wel_whlocfm WHERE wel_wh_code='".$str__wh_to."' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
					if(!($row=mysql_fetch_array($result))){throw new Exception("wel_wh_to_not_found");}	
					$return_val[wel_wh_to_des]=$row[wel_wh_des];
				}

				if($this->wel_pt_no!="")
				{
					$sql="SELECT h.*,v.wel_ven_des".
						" FROM #__wel_ptrhdrm h".
						" LEFT JOIN #__wel_venmasm v ON h.wel_ven_code=v.wel_ven_code".
						" WHERE h.wel_order_type='PT' AND".
							" h.wel_pt_no='".$this->wel_pt_no."' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
					if(!($row=mysql_fetch_array($result))){throw new Exception("wel_pt_no_not_found");}

					$int__count=0;
					while ($int__count < mysql_num_fields($result)) 
					{
						$field_name=mysql_fetch_field($result,$int__count)->name;
						$return_val[$field_name]=$row[$field_name];
						$int__count++;
					}
				}

				if($this->wel_tran_date!="")
				{
					$this->wel_tran_date=(($this->wel_tran_date=="") ? "null" : $this->wel_tran_date);
					$sql="SELECT * FROM #__wel_closedm LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());} 
					if(!($row=mysql_fetch_array($result))){throw new Exception("system_init_error");}
					$sys_phy_date=is_null($row["wel_period1"]) ? $row["wel_phy_date"] : $row["wel_period1"];

					if( date("Y-m-d",strtotime($sys_phy_date)) >= date("Y-m-d",strtotime($this->wel_tran_date)) )
					{
						throw new Exception("wel_tran_date_error");
					}
				}

				mysql_free_result($result);
				
			}
			catch (Exception $e)
			{
				$msg_code=$e->getMessage();
			}
			$return_val["msg_code"]=$msg_code;
			return $return_val;
		}
	}
?>