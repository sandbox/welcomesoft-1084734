<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>

<?php 
	class strm009a_cls
	{
		public $wel_ctrl_flow="";
		public $wel_tran_date="";
		public $wel_wh_fm="";
		public $wel_wh_to="";
		public $wel_pt_no="";
		public $wel_pt_line=0;

		public $wel_part_no="";
		public $wel_lot_no="";
		public $wel_tran_qty=0;
		public $wel_spar_qty=0;
		public $wel_ref_no="";
		public $wel_tran_rmk="";
		
		private $wel_prog_code="strm009";
		
		//读取收货单入仓
		public function read()
		{
			$msg_code="";
			$return_val=array();
			
			try
			{
				$conn=werp_db_connect();
				if (!check_permission($conn,$this->wel_prog_code,"wel_access_read")){throw new Exception("unauthorized_access");}
				
				//工作单明细是否存在
				$sql="SELECT * FROM #__wel_ptrdetm WHERE wel_pt_no='".$this->wel_pt_no."' AND wel_pt_line=".$this->wel_pt_line." LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_pt_detail_not_found");}
				$int__count=0;
				while ($int__count < mysql_num_fields($result)) 
				{
					$field_name=mysql_fetch_field($result,$int__count)->name;
					$return_val[$field_name]=$row[$field_name];
					$int__count++;
				}

				$tmp_part_no=is_null($row["wel_part_no"]) ? "" : $row["wel_part_no"];
				//物料档案
				$sql="SELECT * FROM #__wel_partflm WHERE wel_part_no='".$tmp_part_no."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
				if($row=mysql_fetch_array($result))
				{
					$return_val["wel_part_des"]=$row[wel_part_des];
					$return_val["wel_part_des1"]=$row[wel_part_des1];
					$return_val["wel_part_des2"]=$row[wel_part_des2];
					$return_val["wel_part_des3"]=$row[wel_part_des3];
					$return_val["wel_part_des4"]=$row[wel_part_des4];
					$return_val["wel_unit"]=$row[wel_unit];
				}else{
					$return_val["wel_part_des"]="";
					$return_val["wel_part_des1"]="";
					$return_val["wel_part_des2"]="";
					$return_val["wel_part_des3"]="";
					$return_val["wel_part_des4"]="";
					$return_val["wel_unit"]="";
				}

				mysql_free_result($result);
				
			}
			catch (Exception $e)
			{
				$msg_code=$e->getMessage();
			}
			$return_val["msg_code"]=$msg_code;
			return $return_val;
		}
		
		//收货单入仓
		public function edit()
		{
			$msg_code="";
			$return_val=array();
			
			try
			{
				$conn=werp_db_connect();
				if (!check_permission($conn,$this->wel_prog_code,"wel_access_edit")){throw new Exception("unauthorized_access");}

				if($this->wel_ctrl_flow==""){throw new Exception("wel_ctrl_flow_miss");}
				if($this->wel_tran_date==""){throw new Exception("wel_tran_date_miss");}
				if($this->wel_pt_no==""){throw new Exception("wel_pt_no_miss");}

				if(!is_numeric($this->wel_pt_line)) {throw new Exception("wel_pt_line_miss");}
				$this->wel_pt_line=doubleval($this->wel_pt_line);
				if($this->wel_pt_line==0){throw new Exception("wel_pt_line_miss");}

				if(!is_numeric($this->wel_tran_qty)) {throw new Exception("wel_tran_qty_miss");}
				$this->wel_tran_qty=doubleval($this->wel_tran_qty);
				if($this->wel_tran_qty==0){throw new Exception("wel_tran_qty_miss");}

				if($this->wel_ctrl_flow!="")
				{
					$sql="SELECT * FROM #__wel_whctrlm WHERE wel_ctrl_flow='".$this->wel_ctrl_flow."' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
					if(!($row=mysql_fetch_array($result))){throw new Exception("wel_whctrlm_not_found");}	
					$str__wh_fm=is_null($row["wel_wh_fm"]) ? "" : $row["wel_wh_fm"];
					$str__wh_to=is_null($row["wel_wh_to"]) ? "" : $row["wel_wh_to"];
					$int__dis_able=intval(is_null($row["wel_dis_able"]) ? 0 : $row["wel_dis_able"]);
					$int__rec_flow=intval(is_null($row["wel_rec_flow"]) ? 0 : $row["wel_rec_flow"]);
					$int__avg_cflow=intval(is_null($row["wel_avg_cflow"]) ? 0 : $row["wel_avg_cflow"]);

					if($int__rec_flow!=1){throw new Exception("wel_whctrlm_not_rec_flow");}
					if($int__dis_able==1){throw new Exception("wel_whctrlm_disable");}

					$sql="SELECT * FROM #__wel_whlocfm WHERE wel_wh_code='".$str__wh_fm."' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
					if(!($row=mysql_fetch_array($result))){throw new Exception("wel_wh_fm_not_found");}	
				
					$sql="SELECT * FROM #__wel_whlocfm WHERE wel_wh_code='".$str__wh_to."' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
					if(!($row=mysql_fetch_array($result))){throw new Exception("wel_wh_to_not_found");}	
				}

				if($this->wel_tran_date!="")
				{
					$this->wel_tran_date=(($this->wel_tran_date=="") ? "null" : $this->wel_tran_date);
					$sql="SELECT * FROM #__wel_closedm LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());} 
					if(!($row=mysql_fetch_array($result))){throw new Exception("system_init_error");}
					$sys_phy_date=is_null($row["wel_period1"]) ? $row["wel_phy_date"] : $row["wel_period1"];

					if( date("Y-m-d",strtotime($sys_phy_date)) >= date("Y-m-d",strtotime($this->wel_tran_date)) )
					{
						throw new Exception("wel_req_date_error");
					}
				}

				//收货单是否存在
				$sql="SELECT * FROM #__wel_ptrhdrm WHERE wel_pt_no='".$this->wel_pt_no."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_pt_no_not_found");}	
				$str__order_type=is_null($row["wel_order_type"]) ? "" : $row["wel_order_type"];
				if(strtolower($str__order_type)!="pt"){throw new Exception("wel_pt_no_not_found");}

				//收货单明细是否存在
				$sql="SELECT * FROM #__wel_ptrdetm ".
						"WHERE wel_pt_no='".$this->wel_pt_no."' AND wel_pt_line=".$this->wel_pt_line." LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_pt_detail_not_found");}
				$pt_part_no=is_null($row["wel_part_no"]) ? "" : $row["wel_part_no"];
				$pt_po_no=is_null($row["wel_po_no"]) ? "" : $row["wel_po_no"];
				$pt_po_line=intval(is_null($row["wel_po_line"]) ? 0 : $row["wel_po_line"]);
				$pt_rcv_qty=ROUND(doubleval(is_null($row["wel_rcv_qty"]) ? 0 : $row["wel_rcv_qty"]),4);
				$pt_spar_qty=ROUND(doubleval(is_null($row["wel_spar_qty"]) ? 0 : $row["wel_spar_qty"]),4);
				$pt_iqc_qty=ROUND(doubleval(is_null($row["wel_iqc_qty"]) ? 0 : $row["wel_iqc_qty"]),4);
				$pt_rn_qty=ROUND(doubleval(is_null($row["wel_rn_qty"]) ? 0 : $row["wel_rn_qty"]),4);
				$pt_spar_os_qty=doubleval(is_null($row["wel_spar_os_qty"]) ? 0 : $row["wel_spar_os_qty"]);

				//收货单明细是否存在
				$sql="SELECT d.*,h.wel_cur_code,h.wel_ex_rate".
					" FROM #__wel_pordetm d".
					" LEFT JOIN #__wel_porhdrm h ON d.wel_po_no=h.wel_po_no".
					" WHERE d.wel_po_no='".$pt_po_no."' AND d.wel_po_line=".$pt_po_line." LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_po_detail_not_found");}
				$po_req_qty=doubleval(is_null($row["wel_req_qty"]) ? 0 : $row["wel_req_qty"]);
				$po_os_qty=doubleval(is_null($row["wel_os_qty"]) ? 0 : $row["wel_os_qty"]);
				$po_u_price=doubleval(is_null($row["wel_u_price"]) ? 0 : $row["wel_u_price"]);
				$po_tmp_price=doubleval(is_null($row["wel_tmp_price"]) ? 0 : $row["wel_tmp_price"]);
				$po_pur_unit_rate=doubleval(is_null($row["wel_pur_unit_rate"]) ? 1 : $row["wel_pur_unit_rate"]);
				$po_ven_code=is_null($row["wel_ven_code"]) ? "" : $row["wel_ven_code"];
				$po_cur_code=is_null($row["wel_cur_code"]) ? "" : $row["wel_cur_code"];
				$po_ex_rate=doubleval(is_null($row["wel_ex_rate"]) ? 1 : $row["wel_ex_rate"]);

				if($po_u_price==0){$po_u_price=$po_tmp_price;}
				$po_u_price=$po_u_price/$po_pur_unit_rate;
				
				//品号是否存在
				$sql="SELECT * FROM #__wel_partflm WHERE wel_part_no='".$pt_part_no."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_part_no_not_found");}
				$part_std_cost=doubleval(is_null($row["wel_std_cost"]) ? 0 : $row["wel_std_cost"]);
				$part_avg_cost=doubleval(is_null($row["wel_avg_cost"]) ? 0 : $row["wel_avg_cost"]);
				$part_unit=is_null($row["wel_unit"]) ? "" : $row["wel_unit"];
				$part_cat_code=is_null($row["wel_cat_code"]) ? "" : $row["wel_cat_code"];

				//货币兑换率
				$int_year = date('Y',strtotime($this->wel_tran_date));
				$int_month = date('m',strtotime($this->wel_tran_date));
				if(strlen($int_month) == 1)
				{
					$int_month = '0'.$int_month;
				}
				$int_yyyymm = strval($int_year.$int_month);
	  
				$sql = "SELECT * FROM #__wel_curtabh WHERE wel_cur_code='".$po_cur_code."' AND wel_yyyy_mm='".$int_yyyymm."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				if($row=mysql_fetch_array($result))
				{
					$cur_ex_rate = doubleval(is_null($row['wel_ex_rate']) ? 1 : $row['wel_ex_rate']);
				}	
				else
				{
					$cur_ex_rate = $po_ex_rate;
				}

				if($this->wel_tran_qty<0)
				{
					if($pt_rn_qty+$this->wel_tran_qty < 0){throw new Exception("wel_tran_qty_excess_GRN");}
					if($pt_spar_os_qty+$this->wel_spar_qty < 0){throw new Exception("wel_spar_qty_excess_GRN");}
					if($pt_spar_qty-$pt_spar_os_qty-$this->wel_spar_qty < 0){throw new Exception("wel_spar_qty_excess_GRN");}

					if($po_req_qty-$po_os_qty+$this->wel_tran_qty < 0){throw new Exception("wel_tran_qty_excess_po");}

					$stk_stk_qty = 0;
					$sql="SELECT * FROM #__wel_whlotfm ".
						" WHERE wel_wh_code='".$str__wh_to."' AND wel_part_no='".$pt_part_no."' ".
						" AND wel_lot_no='".$this->wel_lot_no."' AND wel_stk_loc='' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
					if($row=mysql_fetch_array($result))
					{
						$stk_stk_qty=doubleval(is_null($row["wel_stk_qty"]) ? 0 : $row["wel_stk_qty"]);
					}

					if($stk_stk_qty+$this->wel_tran_qty+$this->wel_spar_qty<0){throw new Exception("wel_tran_qty_excess_stock");}
				}else{
					if($pt_rcv_qty-$this->wel_tran_qty < 0){throw new Exception("wel_tran_qty_excess_GRN");}
					if($pt_iqc_qty-$this->wel_tran_qty < 0){throw new Exception("wel_tran_qty_excess_IQC");}
					if($pt_spar_qty-$pt_spar_os_qty-$this->wel_spar_qty < 0){throw new Exception("wel_spar_qty_excess_GRN");}

					if($po_os_qty-$this->wel_tran_qty < 0){throw new Exception("wel_tran_qty_excess_po");}
				}

				if($int__avg_cflow==1)
				{
					$stk_sum_qty = 0;
					$sql="SELECT SUM(wel_stk_qty) AS wel_sum_qty".
						" FROM #__wel_whlotfm a".
						" LEFT JOIN #__wel_whlocfm w ON a.wel_wh_code=w.wel_wh_code".
						" WHERE a.wel_part_no='".$pt_part_no."' AND w.wel_wh_cost>0";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
					if($row=mysql_fetch_array($result))
					{
						$stk_sum_qty=doubleval(is_null($row["wel_sum_qty"]) ? 0 : $row["wel_sum_qty"]);
					}

					if($stk_sum_qty+$this->wel_tran_qty>0)
					{
						$part_avg_cost = ROUND( ($stk_sum_qty*$part_avg_cost + $po_u_price*$this->wel_tran_qty)/($stk_sum_qty+$this->wel_tran_qty),4);
						if($part_avg_cost<=0){$part_avg_cost=0;}
					}
				}

				try
				{
					mysql_query('begin');
						
						//获得最新交易记录号
						$sql="SELECT wel_trn_recno FROM #__wel_tranflm ORDER BY wel_trn_recno DESC LIMIT 1";
						$sql=revert_to_the_available_sql($sql);
						if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
						if(!($row=mysql_fetch_array($result)))
						{
							$dec__next_trn_recno=1;
						}
						else
						{
							$dec__next_trn_recno=doubleval(is_null($row["wel_trn_recno"]) ? 1 : $row["wel_trn_recno"]+1);
						}
				
						//添加记录到tranflm表
						$sql="INSERT INTO #__wel_tranflm SET ".
								"wel_trn_recno=".$dec__next_trn_recno.",".
								"wel_tran_date='".$this->wel_tran_date."',".
								"wel_wh_fm='".$str__wh_fm."',".
								"wel_wh_to='".$str__wh_to."',".
								"wel_tran_code='".$this->wel_ctrl_flow."',".
								"wel_flow_code='REC',".
								"wel_rt_no='".$this->wel_pt_no."',".
								"wel_rt_line=".$this->wel_pt_line.",".
								"wel_ven_code='".$po_ven_code."',".
								"wel_po_no='".$pt_po_no."',".
								"wel_po_line=".$pt_po_line.",".
								"wel_part_no='".$pt_part_no."',".
								"wel_cat_code='".$part_cat_code."',".
								"wel_lot_no_to='".$this->wel_lot_no."',".
								"wel_tran_qty=".$this->wel_tran_qty.",".
								"wel_spar_qty=".$this->wel_spar_qty.",".
								"wel_rec_unit='".$part_unit."',".
								"wel_pur_unit_rate=1,".
								"wel_ref_no='".$this->wel_ref_no."',".
								"wel_tran_rmk='".$this->wel_tran_rmk."',".
								"wel_u_price=".$po_u_price.",".
								"wel_std_cost=".$part_std_cost.",".
								"wel_avg_cost=".$part_avg_cost.",".
								"wel_crt_user='{$_SESSION['wel_user_id']}', ".
								"wel_crt_date=now()";
						$sql=revert_to_the_available_sql($sql);
						if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}

						//更新物品仓存数
						$sql="SELECT * FROM #__wel_whlotfm ".
							" WHERE wel_wh_code='".$str__wh_to."' AND wel_part_no='".$pt_part_no."' ".
							" AND wel_lot_no='".$this->wel_lot_no."' AND wel_stk_loc='' LIMIT 1";
						$sql=revert_to_the_available_sql($sql);
						if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
						if($row=mysql_fetch_array($result))
						{
							$sql="UPDATE #__wel_whlotfm SET ".
								" wel_stk_qty=IFNULL(wel_stk_qty,0)+$this->wel_tran_qty+$this->wel_spar_qty ".
								" WHERE wel_wh_code='".$str__wh_to."' AND wel_part_no='".$pt_part_no."' ".
								" AND wel_lot_no='".$this->wel_lot_no."' AND wel_stk_loc='' LIMIT 1";
							$sql=revert_to_the_available_sql($sql);
							if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
						}else{
							$sql="INSERT INTO #__wel_whlotfm SET ".
									"wel_wh_code='".$str__wh_to."',".
									"wel_part_no='".$pt_part_no."',".
									"wel_lot_no='".$this->wel_lot_no."',".
									"wel_stk_loc='',".
									"wel_stk_qty=".($this->wel_tran_qty+$this->wel_spar_qty).",".
									"wel_cur_code='".$po_cur_code."',".
									"wel_ex_rate=".$cur_ex_rate.",".
									"wel_u_price=".$po_u_price.",".
									"wel_crt_user='{$_SESSION['wel_user_id']}', ".
									"wel_crt_date=now()";
							$sql=revert_to_the_available_sql($sql);
							if(!($result1=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
						}

						//更新收货单明细
						$sql="UPDATE #__wel_ptrdetm SET ".
							" wel_rn_qty=IFNULL(wel_rn_qty,0)+$this->wel_tran_qty,".
							" wel_spar_os_qty=IFNULL(wel_spar_os_qty,0)+$this->wel_spar_qty".
							" WHERE wel_pt_no='".$this->wel_pt_no."' ".
							" AND wel_pt_line='$this->wel_pt_line' LIMIT 1";
						$sql=revert_to_the_available_sql($sql);
						if(!($result1=mysql_query($sql,$conn))){throw new Exception(mysql_error());}

						//更新采购单明细
						$sql="UPDATE #__wel_pordetm SET ".
							" wel_os_qty=IFNULL(wel_os_qty,0)-$this->wel_tran_qty ".
							" WHERE wel_po_no='".$pt_po_no."' ".
							" AND wel_po_line='$pt_po_line' LIMIT 1";
						$sql=revert_to_the_available_sql($sql);
						if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
						
						if($int__avg_cflow==1)
						{
							$sql="UPDATE #__wel_partflm SET ".
								" wel_avg_cost=".$part_avg_cost." ".
								" WHERE wel_part_no='".$pt_part_no."' LIMIT 1";
							$sql=revert_to_the_available_sql($sql);
							if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
						}

						mysql_free_result($result);
						
					mysql_query('commit');
					
				}
				catch (Exception $e1)
				{
					mysql_query('rollback');
					$msg_code=$e1->getMessage();
				}
				
			}
			catch (Exception $e)
			{
				$msg_code=$e->getMessage();
			}
			if($msg_code==""){$msg_code="addnew_succee";}
			$return_val["msg_code"]=$msg_code;
			$return_val["wel_pt_no"]=$str__wel_pt_no;
			return $return_val;
		}
		
	}
?>