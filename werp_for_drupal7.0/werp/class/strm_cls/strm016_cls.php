<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>

<?php 
	class strm016_cls
	{
		public $wel_wh_code="";
		public $wel_wh_des="";
		public $wel_wh_loc1="";
		public $wel_wh_loc2="";
		public $wel_wh_loc3="";
		public $wel_wh_loc4="";
		public $wel_wh_cont="";
		public $wel_wh_tele="";
		public $wel_wh_email="";
		public $wel_net_able="";
		public $wel_wh_cost="";
		public $wel_wh_dum="";
		
		private $wel_prog_code="strm016";
		//读取货仓档案
		public function read()
		{
			$msg_code="";
			$return_val=array();
			
			try
			{
				$conn=werp_db_connect();
				
				if (!check_permission($conn,$this->wel_prog_code,"wel_access_read")){throw new Exception("unauthorized_access");}
				
				$sql="SELECT * FROM #__wel_whlocfm WHERE wel_wh_code='".$this->wel_wh_code."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_wh_code_not_found");}	
				$int__count=0;
				while ($int__count < mysql_num_fields($result)) 
				{
					$field_name=mysql_fetch_field($result,$int__count)->name;
					$return_val[$field_name]=$row[$field_name];
				$int__count++;
				}
				mysql_free_result($result);
			}
			catch (Exception $e)
			{
				$msg_code=$e->getMessage();
			}
			$return_val["msg_code"]=$msg_code;
			return $return_val;
		}
		//新增货仓档案
		public function addnew()
		{
			$msg_code="";
			$return_val=array();
			
			try
			{
				$conn=werp_db_connect();
				
				if (!check_permission($conn,$this->wel_prog_code,"wel_access_addnew")){throw new Exception("unauthorized_access");}
				
				if($this->wel_wh_code==""){throw new Exception("wel_wh_code_miss");}
				if($this->wel_wh_des==""){throw new Exception("wel_wh_des_miss");}
				
				//货仓档案是否存在
				$sql="SELECT * FROM #__wel_whlocfm WHERE wel_wh_code='".$this->wel_wh_code."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if($row=mysql_fetch_array($result)){throw new Exception("wel_wh_code_exist");}
				
				try
				{
					mysql_query('begin');
					
						//插入记录到货仓档案表中
						$sql="INSERT INTO #__wel_whlocfm SET".
							" wel_wh_code='".$this->wel_wh_code."',".
							" wel_wh_des='".$this->wel_wh_des."',".
							" wel_wh_loc1='".$this->wel_wh_loc1."',".
							" wel_wh_loc2='".$this->wel_wh_loc2."',".
							" wel_wh_loc3='".$this->wel_wh_loc3."',".
							" wel_wh_loc4='".$this->wel_wh_loc4."',".
							" wel_wh_cont='".$this->wel_wh_cont."',".
							" wel_wh_tele='".$this->wel_wh_tele."',".
							" wel_wh_email='".$this->wel_wh_email."',".
							" wel_net_able=".intval($this->wel_net_able).",".
							" wel_wh_cost=".intval($this->wel_wh_cost).",".
							" wel_wh_dum=".intval($this->wel_wh_dum)."";
						$sql=revert_to_the_available_sql($sql);
						if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
						mysql_free_result($result);
						
					mysql_query('commit');
					
				}
				catch (Exception $e1)
				{
					mysql_query('rollback');
					throw new Exception($e1->getMessage());
				}
				
			}
			catch (Exception $e)
			{
				$msg_code=$e->getMessage();
			}
			if($msg_code==""){$msg_code="addnew_succee";}
			$return_val["msg_code"]=$msg_code;
			$return_val["wel_wh_code"]=$this->wel_wh_code;
			return $return_val;
		}
		//编辑货仓档案
		public function edit()
		{
			$msg_code="";
			$return_val=array();
			
			try
			{
				$conn=werp_db_connect();
				
				if (!check_permission($conn,$this->wel_prog_code,"wel_access_edit")){throw new Exception("unauthorized_access");}
				
				if($this->wel_wh_code==""){throw new Exception("wel_wh_code_miss");}
				if($this->wel_wh_des==""){throw new Exception("wel_wh_des_miss");}

				//货仓档案是否存在
				$sql="SELECT * FROM #__wel_whlocfm WHERE wel_wh_code='".$this->wel_wh_code."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_wh_code_not_found");}
				
				try
				{
					mysql_query('begin');
					
						//更新货仓档案表记录
						$sql="UPDATE #__wel_whlocfm SET ".
									"wel_wh_des='".$this->wel_wh_des."',".
									"wel_wh_loc1='".$this->wel_wh_loc1."',".
									"wel_wh_loc2='".$this->wel_wh_loc2."',".
									"wel_wh_loc3='".$this->wel_wh_loc3."',".
									"wel_wh_loc4='".$this->wel_wh_loc4."',".
									"wel_wh_cont='".$this->wel_wh_cont."',".
									"wel_wh_tele='".$this->wel_wh_tele."',".
									"wel_wh_email='".$this->wel_wh_email."',".
									"wel_net_able=".intval($this->wel_net_able).",".
									"wel_wh_cost=".intval($this->wel_wh_cost).",".
									"wel_wh_dum=".intval($this->wel_wh_dum)." ".
								"WHERE wel_wh_code='".$this->wel_wh_code."' LIMIT 1";
						$sql=revert_to_the_available_sql($sql);
						if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
						mysql_free_result($result);
						
					mysql_query('commit');
				}
				catch (Exception $e1)
				{
					mysql_query('rollback');
					throw new Exception($e1->getMessage());
				}
				
			}
			catch (Exception $e)
			{
				$msg_code=$e->getMessage();
			}		
			if($msg_code==""){$msg_code="edit_succee";}
			$return_val["msg_code"]=$msg_code;
			return $return_val;
		}
		//删除货仓档案
		public function delete()
		{
			$msg_code="";
			$return_val=array();
			
			try
			{
				$conn=werp_db_connect();
				
				if (!check_permission($conn,$this->wel_prog_code,"wel_access_delete")){throw new Exception("unauthorized_access");}
				
				if($this->wel_wh_code==""){throw new Exception("wel_wh_code_miss");}
				
				//货仓档案是否存在
				$sql="SELECT * FROM #__wel_whlocfm WHERE wel_wh_code='".$this->wel_wh_code."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_wh_code_not_found");}
				
				try
				{
					mysql_query('begin');
					
						//删除货仓档案
						$sql="DELETE FROM #__wel_whlocfm WHERE wel_wh_code='".$this->wel_wh_code."' LIMIT 1";
						$sql=revert_to_the_available_sql($sql);
						if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
						mysql_free_result($result);
						
					mysql_query('commit');
					 
				}
				catch (Exception $e1)
				{
					mysql_query('rollback');
					throw new Exception($e1->getMessage());
				}
				
			}
			catch (Exception $e)
			{
				$msg_code=$e->getMessage();
			}		
			if($msg_code==""){$msg_code="delete_succee";}
			$return_val["msg_code"]=$msg_code;
			return $return_val;
		}
	}
?>