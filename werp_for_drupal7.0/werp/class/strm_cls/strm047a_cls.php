<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>

<?php 
	class strm047a_cls
	{
		public $wel_pt_no="";
		public $wel_pt_line=0;
		public $wel_po_no="";
		public $wel_po_line=0;
		public $wel_rcv_qty=0;
		public $wel_spar_qty=0;
		public $wel_pt_rmk="";
		
		private $wel_prog_code="strm047";
		
		//读取单位档案
		public function read()
		{
			$msg_code="";
			$return_val=array();
			
			try
			{
				$conn=werp_db_connect();
				
				if (!check_permission($conn,$this->wel_prog_code,"wel_access_read")){throw new Exception("unauthorized_access");}
				
				$sql="SELECT d.*,".
					" p.wel_part_des,p.wel_part_des1,p.wel_part_des2,p.wel_unit,".
					" ROUND(IFNULL(a.wel_req_qty,0),4) AS wel_po_qty,".
					" ROUND(IFNULL(a.wel_os_qty,0),4) AS wel_po_os_qty,".
					" ROUND(IFNULL(a.wel_req_qty,0)-IFNULL(a.wel_rcv_qty,0)+IFNULL(a.wel_rtv_qty,0)+IFNULL(d.wel_rcv_qty,0),4) AS wel_rcv_bal_qty".
					" FROM #__wel_ptrdetm d".
						" LEFT JOIN #__wel_pordetm a ON d.wel_po_no=a.wel_po_no AND d.wel_po_line=a.wel_po_line ".
						" LEFT JOIN #__wel_partflm p ON d.wel_part_no=p.wel_part_no ".
					" WHERE d.wel_pt_no='".$this->wel_pt_no."' AND d.wel_pt_line=".$this->wel_pt_line." LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_pt_detail_not_found");}	
				$int__count=0;
				while ($int__count < mysql_num_fields($result)) 
				{
					$field_name=mysql_fetch_field($result,$int__count)->name;
					$return_val[$field_name]=$row[$field_name];
					$int__count++;
				}
				
				mysql_free_result($result);
				
			}
			catch (Exception $e)
			{
				$msg_code=$e->getMessage();
			}
			$return_val["msg_code"]=$msg_code;
			return $return_val;
		}
		
		//添加收货单明细信息
		public function addnew()
		{
			$msg_code="";
			$return_val=array();
			
			try
			{
				$conn=werp_db_connect();
				
				if (!check_permission($conn,$this->wel_prog_code,"wel_access_addnew")){throw new Exception("unauthorized_access");}
				
				if($this->wel_pt_no=="") {throw new Exception("wel_pt_no_miss");}
				if($this->wel_po_no=="") {throw new Exception("wel_po_no_miss");}

				if($this->wel_po_line=="") {throw new Exception("wel_po_detail_miss");}
				$this->wel_po_line=intval($this->wel_po_line);
				if($this->wel_po_line==0){throw new Exception("wel_po_detail_miss");}

				if($this->wel_rcv_qty=="") {throw new Exception("wel_rcv_qty_miss");}
				$this->wel_rcv_qty=doubleval($this->wel_rcv_qty);
				if($this->wel_rcv_qty==0){throw new Exception("wel_rcv_qty_miss");}
				if($this->wel_rcv_qty<0){throw new Exception("wel_rcv_qty_error");}

				$this->wel_spar_qty=doubleval($this->wel_spar_qty);
				if($this->wel_spar_qty<0){throw new Exception("wel_spar_qty_error");}

				//收货单是否存在
				$sql="SELECT * FROM #__wel_ptrhdrm WHERE wel_pt_no='".$this->wel_pt_no."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_pt_no_not_found");}	
				$pt_last_line=intval(is_null($row["wel_last_line"]) ? 0 : $row["wel_last_line"])+1;
				$pt_ven_code=is_null($row["wel_ven_code"]) ? "" : $row["wel_ven_code"];
				
				//采购单是否存在
				$sql="SELECT * FROM #__wel_porhdrm WHERE wel_po_no='".$this->wel_po_no."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_po_no_not_found");}
				$po_appr_yn=intval(is_null($row["wel_appr_yn"]) ? 0 : $row["wel_appr_yn"]);
				$po_closed=intval(is_null($row["wel_closed"]) ? 0 : $row["wel_closed"]);
				$po_ven_code=is_null($row["wel_ven_code"]) ? "" : $row["wel_ven_code"];
				
				if($po_appr_yn==0){throw new Exception("wel_po_no_not_approved");}
				if($po_closed==1){throw new Exception("wel_po_no_closed");}
				if(strtolower($po_ven_code)!=strtolower($pt_ven_code)){throw new Exception("wel_ven_code_not_match");}
				
				//采购单明细是否存在
				$sql="SELECT * FROM #__wel_pordetm WHERE wel_po_no='".$this->wel_po_no."' AND wel_po_line=".$this->wel_po_line." LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_po_detail_not_found");}
				 //是否已结案
				$po_closed=intval(is_null($row["wel_closed"]) ? 0 : $row["wel_closed"]);
				if($po_closed==1){throw new Exception("wel_po_no_closed");}
	
				$po_part_no=is_null($row["wel_part_no"]) ? "" : $row["wel_part_no"];
				$po_req_qty=doubleval(is_null($row["wel_req_qty"]) ? 0 : $row["wel_req_qty"]);
				$po_os_qty=doubleval(is_null($row["wel_os_qty"]) ? 0 : $row["wel_os_qty"]);
				$po_rcv_qty=doubleval(is_null($row["wel_rcv_qty"]) ? 0 : $row["wel_rcv_qty"]);
				$po_rtv_qty=doubleval(is_null($row["wel_rtv_qty"]) ? 0 : $row["wel_rtv_qty"]);
				$po_u_price=doubleval(is_null($row["wel_u_price"]) ? 0 : $row["wel_u_price"]);
				
				//物料是否存在
				$sql="SELECT * FROM #__wel_partflm WHERE wel_part_no='".$po_part_no."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_part_no_not_found");}	
				$part_unit=is_null($row["wel_unit"]) ? "" : $row["wel_unit"];
				$part_checkyn=intval(is_null($row["wel_checkyn"]) ? 0 : $row["wel_checkyn"]);
				
				if($part_checkyn==0)		//免检
				{
					$pt_iqc_qty=$this->wel_rcv_qty;
					$pt_unpass_qty=0;
					$pt_closed=1;
				}else{
					$pt_iqc_qty=0;
					$pt_unpass_qty=0;
					$pt_closed=0;
				}

				//收货数是否在有效范围内
				if(ROUND($po_req_qty-$po_rcv_qty+$po_rtv_qty-$this->wel_rcv_qty,4)<0){throw new Exception("wel_rcv_qty_over");}
				
				//收货单明细是否存在
				$sql="SELECT * FROM #__wel_ptrdetm ".
						"WHERE wel_pt_no='".$this->wel_pt_no."' AND wel_pt_line=".$pt_last_line." LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
				if($row=mysql_fetch_array($result))
				{
					$sql="SELECT max(wel_pt_line) as max_line FROM #__wel_ptrdetm ".
						"WHERE wel_pt_no='$this->wel_pt_no' ";
					$sql=revert_to_the_available_sql($sql);
					if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					if(!$row=mysql_fetch_array($result)){throw new Exception("wel_pt_detail_not_found");}
					$pt_last_line=intval(is_null($row["max_line"]) ? 0 : $row["max_line"])+1;
				}

				try
				{
					mysql_query("begin");
						
						//=================================================================================================
						//更新采购订单明细
						$sql="UPDATE #__wel_pordetm SET".
							" wel_rcv_qty=IFNULL(wel_rcv_qty,0)+".$this->wel_rcv_qty.",".
							" wel_iqc_qty=IFNULL(wel_iqc_qty,0)+".$pt_iqc_qty." ".
							" WHERE wel_po_no='".$this->wel_po_no."' AND wel_po_line=".$this->wel_po_line." LIMIT 1";
						$sql=revert_to_the_available_sql($sql);
						if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}

						//=================================================================================================
						//添加收货单明细
						$str__sql_field="";
						$str__sql_value="";
						//是否免检
						if($int_checkyn==0)			//免检	
						{
							$str__sql_field.=",wel_iqc_qty,wel_unpass_qty,wel_closed";
							$str__sql_value.=",".$this->wel_rcv_qty.",0,1";
						}
						$sql="INSERT INTO #__wel_ptrdetm SET".
							" wel_pt_no='".$this->wel_pt_no."',".
							" wel_pt_line=".$pt_last_line.",".
							" wel_po_no='".$this->wel_po_no."',".
							" wel_po_line=".$this->wel_po_line.",".
							" wel_part_no='".$po_part_no."',".
							" wel_unit='".$part_unit."',".
							" wel_rcv_qty=".$this->wel_rcv_qty.",".
							" wel_spar_qty=".$this->wel_spar_qty.",".
							" wel_iqc_qty=".$pt_iqc_qty.",".
							" wel_unpass_qty=".$pt_unpass_qty.",".
							" wel_closed=".$pt_closed.",".
							" wel_u_price=".$po_u_price.",".
							" wel_pt_rmk='".$this->wel_pt_rmk."'";
						$sql=revert_to_the_available_sql($sql);
						if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}

						//=================================================================================================
						//更新收货单
						$sql="UPDATE #__wel_ptrhdrm SET".
							" wel_last_line=".$pt_last_line.",".
							" wel_upd_user='{$_SESSION['wel_user_id']}',".
							" wel_upd_date=now()".
							" WHERE wel_pt_no='".$this->wel_pt_no."' LIMIT 1";
						$sql=revert_to_the_available_sql($sql);
						if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}

					mysql_query("commit");
					
				}
				catch(Exception $e1)
				{
					mysql_query("rollback");
					throw new Exception($e1->getMessage());
				}
				
			}
			catch(Exception $e)
			{
				$msg_code=$e->getMessage();
			}
			if($msg_code==""){$msg_code="addnew_succee";}	
			$return_val["msg_code"]=$msg_code;
			$return_val["wel_pt_no"]=$this->wel_pt_no;
			return $return_val;
		}
		
		//编辑收货单明细
		public function edit()
		{
			$msg_code="";
			$return_val=array();
			try
			{
				$conn=werp_db_connect();
				
				if (!check_permission($conn,$this->wel_prog_code,"wel_access_edit")){throw new Exception("unauthorized_access");}
				
				if($this->wel_pt_no=="") {throw new Exception("wel_pt_no_miss");}
				if($this->wel_pt_line=="") {throw new Exception("wel_pt_detail_miss");}
				$this->wel_pt_line=intval($this->wel_pt_line);
				if($this->wel_po_line==0){throw new Exception("wel_pt_detail_miss");}

				if($this->wel_rcv_qty=="") {throw new Exception("wel_rcv_qty_miss");}
				$this->wel_rcv_qty=doubleval($this->wel_rcv_qty);
				if($this->wel_rcv_qty==0){throw new Exception("wel_rcv_qty_miss");}
				if($this->wel_rcv_qty<0){throw new Exception("wel_rcv_qty_error");}

				$this->wel_spar_qty=doubleval($this->wel_spar_qty);
				if($this->wel_spar_qty<0){throw new Exception("wel_spar_qty_error");}
				
				//收货单明细是否存在
				$sql="SELECT * FROM #__wel_ptrdetm ".
						"WHERE wel_pt_no='".$this->wel_pt_no."' AND wel_pt_line=".$this->wel_pt_line." LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_pt_detail_not_found");}
				$pt_rn_qty=intval(is_null($row["wel_rn_qty"]) ? 0 : $row["wel_rn_qty"]);
				if($pt_rn_qty>0){throw new Exception("wel_pt_detail_to_store");}

				$pt_po_no=is_null($row["wel_po_no"]) ? "" : $row["wel_po_no"];
				$pt_po_line=intval(is_null($row["wel_po_line"]) ? 0 : $row["wel_po_line"]);
				$pt_part_no=is_null($row["wel_part_no"]) ? "" : $row["wel_part_no"];
				$wel_closed=intval(is_null($row["wel_closed"]) ? 0 : $row["wel_closed"]);
				$old_rcv_qty=doubleval(is_null($row["wel_rcv_qty"]) ? 0 : $row["wel_rcv_qty"]);
				$old_iqc_qty=doubleval(is_null($row["wel_iqc_qty"]) ? 0 : $row["wel_iqc_qty"]);
				$old_unpass_qty=doubleval(is_null($row["wel_unpass_qty"]) ? 0 : $row["wel_unpass_qty"]);

				$sql="SELECT * FROM #__wel_porhdrm WHERE wel_po_no='".$pt_po_no."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_po_no_not_found");}
				$po_appr_yn=intval(is_null($row["wel_appr_yn"]) ? 0 : $row["wel_appr_yn"]);
				$po_closed=intval(is_null($row["wel_closed"]) ? 0 : $row["wel_closed"]);
				
				if($po_appr_yn==0){throw new Exception("wel_po_no_not_approved");}
				if($po_closed==1){throw new Exception("wel_po_no_closed");}

				//采购单明细是否存在
				$sql="SELECT * FROM #__wel_pordetm WHERE wel_po_no='".$pt_po_no."' AND wel_po_line=".$pt_po_line." LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_po_detail_not_found");}

				$po_closed=intval(is_null($row["wel_closed"]) ? 0 : $row["wel_closed"]);
				if($po_closed==1){throw new Exception("wel_po_no_closed");}
	
				$po_req_qty=doubleval(is_null($row["wel_req_qty"]) ? 0 : $row["wel_req_qty"]);
				$po_os_qty=doubleval(is_null($row["wel_os_qty"]) ? 0 : $row["wel_os_qty"]);
				$po_rcv_qty=doubleval(is_null($row["wel_rcv_qty"]) ? 0 : $row["wel_rcv_qty"]);
				$po_rtv_qty=doubleval(is_null($row["wel_rtv_qty"]) ? 0 : $row["wel_rtv_qty"]);
				$po_u_price=doubleval(is_null($row["wel_u_price"]) ? 0 : $row["wel_u_price"]);
				
				//物料是否存在
				$sql="SELECT * FROM #__wel_partflm WHERE wel_part_no='".$pt_part_no."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_part_no_not_found");}	
				$part_unit=is_null($row["wel_unit"]) ? "" : $row["wel_unit"];
				$part_checkyn=intval(is_null($row["wel_checkyn"]) ? 0 : $row["wel_checkyn"]);

				if($part_checkyn==0)		//免检
				{
					$pt_iqc_qty=$this->wel_rcv_qty;
					$pt_unpass_qty=0;
					$pt_closed=1;
				}else{
					if($wel_closed==1){throw new Exception("wel_pt_detail_had_iqc");}
					$pt_iqc_qty=0;
					$pt_unpass_qty=0;
					$pt_closed=0;
				}
				
				if(ROUND($po_req_qty-$po_rcv_qty+$po_rtv_qty+$old_rcv_qty-$this->wel_rcv_qty,4)<0){throw new Exception("wel_rcv_qty_over");}

				try
				{
					mysql_query("begin");
					
						//=================================================================================================
						//更新采购单明细
						$sql="UPDATE #__wel_pordetm SET".
							" wel_rcv_qty=IFNULL(wel_rcv_qty,0)+".$this->wel_rcv_qty."-".$old_rcv_qty.",".
							" wel_iqc_qty=IFNULL(wel_iqc_qty,0)+".$pt_iqc_qty."-".$old_iqc_qty." ".
							" WHERE wel_po_no='".$this->wel_po_no."' AND wel_po_line=".$this->wel_po_line." LIMIT 1";
						$sql=revert_to_the_available_sql($sql);
						if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
									
						//=================================================================================================
						//更新收货单明细
						$sql="UPDATE #__wel_ptrdetm SET ".
							" wel_rcv_qty=".doubleval($this->wel_rcv_qty).",".
							" wel_spar_qty=".doubleval($this->wel_spar_qty).",".
							" wel_iqc_qty=".$pt_iqc_qty.",".
							" wel_unpass_qty=".$pt_unpass_qty.",".
							" wel_closed=".$pt_closed.",".
							" wel_u_price=".$po_u_price.",".
							" wel_pt_rmk='".$this->wel_pt_rmk."'".
							" WHERE wel_pt_no='".$this->wel_pt_no."' AND wel_pt_line=".$this->wel_pt_line." LIMIT 1";
						$sql=revert_to_the_available_sql($sql);
						if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}

						//=================================================================================================
						//更新收货单
						$sql="UPDATE #__wel_ptrhdrm SET".
							" wel_upd_user='{$_SESSION['wel_user_id']}',".
							" wel_upd_date=now()".
							" WHERE wel_pt_no='".$this->wel_pt_no."' LIMIT 1";
						$sql=revert_to_the_available_sql($sql);
						if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}

					mysql_query("commit");
				}
				catch (Exception $e1)
				{
					mysql_query("rollback");
					$msg_code=$e1->getMessage();
				}
				
			}
			catch (Exception $e)
			{
				$msg_code=$e->getMessage();
			}	
			if($msg_code==""){$msg_code="edit_succee";}	
			$return_val["msg_code"]=$msg_code;
			$return_val["wel_pt_no"]=$this->wel_pt_no;
			return $return_val;
		}
	}
?>