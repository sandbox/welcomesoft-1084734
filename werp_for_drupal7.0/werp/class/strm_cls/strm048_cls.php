<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>

<?php 
	class strm048_cls
	{
		public $wel_pt_no="";
		public $wel_pattern="";
		public $wel_req_date="";
		public $wel_ven_code="";
		public $wel_ven_dn="";
		public $wel_pt_remark="";
		
		private $wel_prog_code="strm048";
		
		//读取收货单
		public function read()
		{
			$msg_code="";
			$return_val=array();
			
			try
			{
				$conn=werp_db_connect();
				
				if (!check_permission($conn,$this->wel_prog_code,"wel_access_read")){throw new Exception("unauthorized_access");}
				
				$sql="SELECT h.*, ".
						"v.wel_ven_des, ".
						"v.wel_ven_des1, ".
						"v.wel_abbre_des, ".
						"v.wel_ven_add1, ".
						"v.wel_ven_add2, ".
						"v.wel_ven_add3, ".
						"v.wel_ven_add4, ".
						"v.wel_ven_email, ".
						"v.wel_ven_cont, ".
						"v.wel_ven_tele, ".
						"v.wel_ven_tele1, ".
						"v.wel_ven_fax, ".
						"v.wel_ven_fax1 ".
						"FROM #__wel_ptrhdrm h ".
							"LEFT JOIN #__wel_venmasm v ON h.wel_ven_code=v.wel_ven_code ".
						"WHERE h.wel_pt_no='".$this->wel_pt_no."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_pt_no_not_found");}	
				$int__count=0;
				while ($int__count < mysql_num_fields($result)) 
				{
					$field_name=mysql_fetch_field($result,$int__count)->name;
					$return_val[$field_name]=$row[$field_name];
					$int__count++;
				}
				mysql_free_result($result);
				
			}
			catch (Exception $e)
			{
				$msg_code=$e->getMessage();
			}
			$return_val["msg_code"]=$msg_code;
			return $return_val;
		}
		
		//检查是否已进行IQC
		function check_iqc()
		{
			$msg_code="";
			$return_val=array();
			
			try
			{
				$conn=werp_db_connect();
				
				if (!check_permission($conn,$this->wel_prog_code,"wel_access_edit")){throw new Exception("unauthorized_access");}
				
				$sql="SELECT wel_closed FROM #__wel_ptrdetm WHERE wel_pt_no='".$this->wel_pt_no."' AND wel_pt_line=".intval($this->wel_pt_line)." LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_pt_no_detail_not_found");}
				$int__closed=intval(is_null($row["wel_closed"]) ? 0 : $row["wel_closed"]);
				if($int__closed==1){throw new Exception("wel_pt_detail_had_iqc");}
				
				mysql_free_result($result);
			}
			catch (Exception $e)
			{
				$msg_code=$e->getMessage();
			}
			$return_val["msg_code"]=$msg_code;
			return $return_val;
		}
	}
?>