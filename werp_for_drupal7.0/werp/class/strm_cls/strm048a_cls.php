<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>

<?php 
	class strm048a_cls
	{
		public $wel_pt_no="";
		public $wel_pt_line=0;
		public $wel_po_no="";
		public $wel_po_line=0;
		public $wel_part_no="";
		public $wel_spar_qty=0;
		public $wel_rcv_qty=0;
		public $wel_iqc_qty=0;
		public $wel_unpass_qty=0;
		public $wel_pt_rmk="";
		
		private $wel_prog_code="strm048";
		
		//读取收货单档案
		public function read()
		{
			$msg_code="";
			$return_val=array();
			
			try
			{
				$conn=werp_db_connect();
				
				if (!check_permission($conn,$this->wel_prog_code,"wel_access_read")){throw new Exception("unauthorized_access");}
				
				$sql="SELECT d.*,".
					" p.wel_part_des,p.wel_part_des1,p.wel_part_des2,p.wel_unit".
					" FROM #__wel_ptrdetm d".
						" LEFT JOIN #__wel_partflm p ON d.wel_part_no=p.wel_part_no".
					" WHERE d.wel_pt_no='".$this->wel_pt_no."' AND d.wel_pt_line=".$this->wel_pt_line." LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_pt_detail_not_found");}	
				$int__count=0;
				while ($int__count < mysql_num_fields($result)) 
				{
					$field_name=mysql_fetch_field($result,$int__count)->name;
					$return_val[$field_name]=$row[$field_name];
					$int__count++;
				}
				mysql_free_result($result);
				
			}
			catch (Exception $e)
			{
				$msg_code=$e->getMessage();
			}
			$return_val["msg_code"]=$msg_code;
			return $return_val;
		}
		
		//编辑来料检查明细
		public function edit()
		{
			$msg_code="";
			$return_val=array();
			
			try
			{
				$conn=werp_db_connect();
				
				if (!check_permission($conn,$this->wel_prog_code,"wel_access_edit")){throw new Exception("unauthorized_access");}
				
				if($this->wel_iqc_qty=="") {throw new Exception("wel_iqc_qty_miss");}
				$this->wel_iqc_qty=doubleval($this->wel_iqc_qty);
				if($this->wel_iqc_qty==0){throw new Exception("wel_iqc_qty_miss");}
				if($this->wel_iqc_qty<0){throw new Exception("wel_rcv_qty_error");}

				$this->wel_unpass_qty=doubleval($this->wel_unpass_qty);
				
				//收货单明细是否存在
				$sql="SELECT * FROM #__wel_ptrdetm".
					" WHERE wel_pt_no='".$this->wel_pt_no."' AND wel_pt_line=".$this->wel_pt_line." LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_pt_detail_not_found");}
				$pt_closed=intval(is_null($row["wel_closed"]) ? 0 : $row["wel_closed"]);
				$pt_rcv_qty=doubleval(is_null($row["wel_rcv_qty"]) ? 0 : $row["wel_rcv_qty"]);
				$pt_po_no=is_null($row["wel_po_no"]) ? "" : $row["wel_po_no"];
				$pt_po_line=intval(is_null($row["wel_po_line"]) ? 0 : $row["wel_po_line"]);

				if($pt_closed==1){throw new Exception("wel_pt_detail_had_iqc");}

				if($this->wel_iqc_qty+$this->wel_unpass_qty!=$pt_rcv_qty){throw new Exception("wel_iqc_qty_match");}

				try
				{
					mysql_query("begin");
						
						//更新采购单明细
						$sql="UPDATE #__wel_pordetm SET".
							" wel_iqc_qty=IFNULL(wel_iqc_qty,0)+".$this->wel_iqc_qty." ".
							" WHERE wel_po_no='".$pt_po_no."' AND wel_po_line=".$pt_po_line." LIMIT 1";
						$sql=revert_to_the_available_sql($sql);
						if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
								
						//更新收货单明细
						$sql="UPDATE #__wel_ptrdetm SET".
							" wel_iqc_qty=".$this->wel_iqc_qty.",".
							" wel_unpass_qty=".$this->wel_unpass_qty.",".
							" wel_closed=1 ".
							" WHERE wel_pt_no='".$this->wel_pt_no."' AND wel_pt_line=".$this->wel_pt_line." LIMIT 1";
						$sql=revert_to_the_available_sql($sql);
						if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
						mysql_free_result($result);
						
					mysql_query("commit");
					
				}
				catch (Exception $e1)
				{
					mysql_query("rollback");
					$msg_code=$e1->getMessage();
				}
				
			}
			catch (Exception $e)
			{
				$msg_code=$e->getMessage();
			}	
			if($msg_code==""){$msg_code="edit_succee";}	
			$return_val["msg_code"]=$msg_code;
			return $return_val;
		}
	}
?>