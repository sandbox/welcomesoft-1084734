<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>

<?php
class strm189a_cls{
	public $wel_wh_code="";
	public $wel_tag_no;
	public $wel_part_no="";
	public $wel_part_des;
	public $wel_lot_no;
	public $wel_tag_qty;
	
	private $wel_prog_code="strm189a";

	public function read(){
		$msg_code="";
		$return_val=array();
		 
		try
		{
			$conn=werp_db_connect();
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_read")){throw new Exception("unauthorized_access");}
			
			$sql="SELECT d.*,".
				" p.wel_part_des,p.wel_part_des1,p.wel_part_des2,p.wel_part_des3,p.wel_part_des4,p.wel_unit".
				" FROM #__wel_countfm d ".
				" LEFT JOIN #__wel_partflm p ON d.wel_part_no=p.wel_part_no ".
				" WHERE d.wel_wh_code='".$this->wel_wh_code."' AND d.wel_tag_no='".$this->wel_tag_no."' limit 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_tag_no_not_found");}

			$int__count=0;
			while ($int__count < mysql_num_fields($result)) {
				$field_name=mysql_fetch_field($result,$int__count)->name;
				$return_val[$field_name]=$row[$field_name];
				//$msg_code=$msg_code."|".$field_name;
   				$int__count++;
			}
			mysql_free_result($result);
			throw new Exception("");
		}
		catch (Exception $e){
			$msg_code=$e->getMessage();
		}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	public function addnew(){
		$msg_code="";
		$return_val=array();
	 
		try
		{
			$conn=werp_db_connect();
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_addnew")){throw new Exception("unauthorized_access");}
			
			$this->wel_part_no=strtoupper($this->wel_part_no);
			if($this->wel_wh_code==''){throw new Exception('wel_wh_code_miss');}
			if($this->wel_tag_no==''){throw new Exception('wel_tag_no_miss');}
			if($this->wel_part_no==''){throw new Exception('wel_part_no_miss');}
			
			if(!is_numeric($this->wel_tag_qty)) {throw new Exception('wel_tag_qty_miss');}
			$this->wel_tag_qty=doubleval($this->wel_tag_qty);
			if($this->wel_tag_qty < 0){throw new Exception("wel_tag_qty_miss");}

			//检测货仓
			if($this->wel_wh_code!="")
			{
				$sql="SELECT * FROM #__wel_whlocfm WHERE wel_wh_code='".$this->wel_wh_code."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_wh_code_not_found");}	
				$wh_wh_dum=intval(is_null($row["wel_wh_dum"]) ? 0 : $row["wel_wh_dum"]);

				if($wh_wh_dum==1){throw new Exception("wel_wh_code_not_found");}
			}

			if($this->wel_part_no!="")
			{
				$sql="SELECT * FROM #__wel_partflm WHERE wel_part_no='".$this->wel_part_no."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_part_no_not_found");}	
			}

			//检测标签号码
			if($this->wel_tag_no!="")
			{
				$sql="SELECT * FROM #__wel_countft WHERE wel_wh_code='".$this->wel_wh_code."' AND wel_tag_no='".$this->wel_tag_no."' limit 1";
				$sql=revert_to_the_available_sql($sql);
				if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_tag_no_not_found");}
			
				$sql="SELECT * FROM #__wel_countfm WHERE wel_wh_code='".$this->wel_wh_code."' AND wel_tag_no='".$this->wel_tag_no."' limit 1";
				$sql=revert_to_the_available_sql($sql);
				if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				if($row=mysql_fetch_array($result)){throw new Exception("wel_tag_no_exist");}

				$sql="SELECT * FROM #__wel_countfm WHERE wel_wh_code='".$this->wel_wh_code."' AND wel_part_no='".$this->wel_part_no."' AND wel_lot_no='".$this->wel_lot_no."' limit 1";
				$sql=revert_to_the_available_sql($sql);
				if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				if($row=mysql_fetch_array($result)){throw new Exception("wel_tag_same_detail");}
			}

			try
			{
				mysql_query('begin');

					$sql="INSERT INTO #__wel_countfm SET ".
							"wel_wh_code='".strtoupper($this->wel_wh_code)."',".
							"wel_tag_no='".$this->wel_tag_no."',".
							"wel_part_no='".$this->wel_part_no."',".
							"wel_lot_no='".strtoupper($this->wel_lot_no)."',".
							"wel_tag_qty=".$this->wel_tag_qty;
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}

					mysql_free_result($result);
					
				mysql_query('commit');
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}	
		}
		catch (Exception $e){
			$msg_code=$e->getMessage();
		}
		if($msg_code==""){$msg_code="addnew_succee";}
		$return_val["msg_code"]=$msg_code;
		$return_val["wel_wh_code"]=$this->wel_wh_code;
		$str_next_no=substr(strval(intval($this->wel_tag_no)+1000001),1,6);
		$return_val["wel_tag_no"]=$str_next_no;
		return $return_val;
	}

	public function edit(){
		$msg_code="";
		$return_val=array();
 
		try
		{
			$conn=werp_db_connect();
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_edit")){throw new Exception("unauthorized_access");}
			
			$this->wel_part_no=strtoupper($this->wel_part_no);
			if($this->wel_wh_code==''){throw new Exception('wel_wh_code_miss');}
			if($this->wel_tag_no==''){throw new Exception('wel_tag_no_miss');}
			if($this->wel_part_no==''){throw new Exception('wel_part_no_miss');}

			if(!is_numeric($this->wel_tag_qty)) {throw new Exception('wel_tag_qty_miss');}
			$this->wel_tag_qty=doubleval($this->wel_tag_qty);
			if($this->wel_tag_qty < 0){throw new Exception("wel_tag_qty_miss");}

			$sql="SELECT * FROM #__wel_countfm WHERE wel_wh_code='".$this->wel_wh_code."' AND wel_tag_no='".$this->wel_tag_no."' limit 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_tag_no_not_found");}

			$sql="SELECT * FROM #__wel_countfm WHERE wel_wh_code='".$this->wel_wh_code."' AND wel_part_no='".$this->wel_part_no."' AND wel_lot_no='".$this->wel_lot_no."' AND wel_tag_no<>'".$this->wel_tag_no."' limit 1";
			$sql=revert_to_the_available_sql($sql);
			if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
			if($row=mysql_fetch_array($result)){throw new Exception("wel_tag_same_detail");}

			try
			{
				mysql_query('begin');

					$sql="UPDATE #__wel_countfm SET ".
						" wel_lot_no='".strtoupper($this->wel_lot_no)."',".
						" wel_tag_qty=".$this->wel_tag_qty.
						" WHERE wel_wh_code='".$this->wel_wh_code."' AND wel_tag_no='".$this->wel_tag_no."'";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}

					mysql_free_result($result);
					
				mysql_query('commit');
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}	
		}
		catch (Exception $e){
			$msg_code=$e->getMessage();
		}		
		if($msg_code==""){$msg_code="edit_succee";}	
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
}
?>
