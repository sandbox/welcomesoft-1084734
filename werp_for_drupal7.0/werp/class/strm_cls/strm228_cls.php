<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>

<?php
class strm228_cls{
	public $wel_dn_no="";
	public $wel_pattern="";
	public $wel_tran_date="";
	public $wel_comp_code="";
	public $wel_cus_code="";
	public $wel_dely_code="";
	public $wel_remark="";

	public $wel_dn_line=0;

	public $wel_crt_user="";
	public $wel_crt_date="";
	public $wel_upd_user="";
	public $wel_upd_date="";
	
	private $wel_prog_code="strm228";

	public function read()
	{
		$msg_code="";
		$return_val=array();

		try
		{
			$conn=werp_db_connect();
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_read")){throw new Exception("unauthorized_access");}

			if ($this->wel_dn_no==""){throw new Exception("wel_dn_no_miss");}
			$sql="SELECT h.*, ".
						"c.wel_cus_des, ".
						"c.wel_cus_des1, ".
						"c.wel_abbre_des, ".
						"c.wel_cus_add1, ".
						"c.wel_cus_add2, ".
						"c.wel_cus_add3, ".
						"c.wel_cus_add4, ".
						"c.wel_cus_email, ".
						"c.wel_cus_tele, ".
						"c.wel_cus_fax, ".
						"f.wel_dely_des as wel_dely_des, ".
						"f.wel_dely_add1 as wel_dely_add1, ".
						"f.wel_dely_add2 as wel_dely_add2, ".
						"f.wel_dely_add3 as wel_dely_add3, ".
						"f.wel_dely_add4 as wel_dely_add4, ".
						"f.wel_dely_cont as wel_dely_cont, ".
						"f.wel_dely_tel as wel_dely_tel, ".
						"f.wel_dely_fax as wel_dely_fax, ".
						"f.wel_dely_email as wel_dely_email, ".
						"f.wel_dely_mob as wel_dely_mob ".
				 "FROM #__wel_dnhdrfm h ".
					"LEFT JOIN #__wel_cusmasm c ON h.wel_cus_code=c.wel_cus_code ".
					"LEFT JOIN #__wel_delyflm f ON (h.wel_cus_code=f.wel_cus_code and h.wel_dely_code=f.wel_dely_line) ".
				 "WHERE wel_dn_no='$this->wel_dn_no' limit 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_dn_no_not_found");}
			$int__count=0;
			while ($int__count<mysql_num_fields($result)) 
			{
				$field_name=mysql_fetch_field($result,$int__count)->name;
				$return_val[$field_name]=$row[$field_name];
				$int__count++;
			}
			mysql_free_result($result);
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}

	public function addnew()
	{
		$msg_code="";
		$return_val=array();

		try
		{
			$conn=werp_db_connect();
			if (!check_permission($conn,$this->private_wel_prog_code,"wel_access_addnew")){throw new Exception("unauthorized_access");}

			if ($this->wel_dn_no=="" && $this->wel_pattern==""){throw new Exception("wel_dn_no_miss");}
			if ($this->wel_tran_date==""){throw new Exception("wel_dn_date_miss");}
			if ($this->wel_comp_code==""){throw new Exception("wel_comp_code_miss");}
			if ($this->wel_cus_code==""){throw new Exception("wel_cus_code_miss");}

			$this->wel_tran_date=(($this->wel_tran_date=="")?"null":"'".$this->wel_tran_date."'");

			if($this->wel_pattern!="")
			{
				$sql="SELECT wel_pattern from #__wel_gentdnw WHERE wel_pattern='".$this->wel_pattern."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_pattern_not_found");}
			}

			if($this->wel_cus_code!="")
			{
				$sql="SELECT wel_cus_code from #__wel_cusmasm WHERE wel_cus_code='".$this->wel_cus_code."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_cus_code_not_found");}
			}

			if($this->wel_comp_code!="")
			{
				$sql="SELECT wel_comp_code from #__wel_compflm WHERE wel_comp_code='".$this->wel_comp_code."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_comp_code_not_found");}
			}
			
			if($this->wel_dely_code!="")
			{
				$sql="SELECT wel_dely_line from #__wel_delyflm WHERE wel_cus_code='".$this->wel_cus_code."' and wel_dely_line='".$this->wel_dely_code."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_dely_code_not_found");}
			}

			try
			{
				mysql_query("begin");	
								
				if ($this->wel_pattern!="")
				{
					$sql="SELECT * FROM #__wel_gentdnw WHERE wel_pattern='$this->wel_pattern' limit 1";
					$sql=revert_to_the_available_sql($sql);
					if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
					if(!($row=mysql_fetch_array($result))){throw new Exception("wel_pattern_not_found");}
					
					$wel_pattern=stripslashes(trim($row["wel_pattern"]));
					$wel_dn_nextno=trim($row["wel_dn_nextno"]);
					$wel_dn_nextno=sprintf("%'08s",$wel_dn_nextno);

					if(strlen($wel_dn_nextno)>8){throw new Exception(wel_pattern_overflow);}
					$this->wel_dn_no=$wel_pattern.$wel_dn_nextno;

					$sql="UPDATE #__wel_gentdnw set wel_dn_nextno=IFNULL(wel_dn_nextno,0)+1 WHERE wel_pattern='$this->wel_pattern'";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
				}

				$sql="SELECT wel_dn_no FROM #__wel_dnhdrfm WHERE wel_dn_no='$this->wel_dn_no' limit 1";
				$sql=revert_to_the_available_sql($sql);
				if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
				if($row=mysql_fetch_array($result)){throw new Exception("wel_dn_no_exist");}

				$sql="INSERT INTO #__wel_dnhdrfm SET ".
						"wel_dn_no='$this->wel_dn_no',".
						"wel_pattern='$this->wel_pattern',".
						"wel_dn_date=$this->wel_tran_date,".
						"wel_cus_code='$this->wel_cus_code',".
						"wel_comp_code='$this->wel_comp_code',".
						"wel_dely_code='$this->wel_dely_code',".
						"wel_remark='$this->wel_remark',".
						"wel_crt_user='{$_SESSION['wel_user_id']}',".
						"wel_crt_date=now() ";
				$sql=revert_to_the_available_sql($sql);
				if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					
				mysql_query("commit");
					
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		if($msg_code==""){$msg_code="addnew_succee";}
		$return_val["msg_code"]=$msg_code;
		$return_val["wel_dn_no"]=$this->wel_dn_no;
		return $return_val;
	}

	public function edit()
	{
		$msg_code="";
		$return_val=array();

		try
		{
			$conn=werp_db_connect();
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_edit")){throw new Exception("unauthorized_access");}

			if ($this->wel_dn_no==""){throw new Exception("wel_dn_no_miss");}
			if ($this->wel_comp_code==""){throw new Exception("wel_comp_code_miss");}
			if ($this->wel_tran_date==""){throw new Exception("wel_dn_date_miss");}
			if ($this->wel_cus_code==""){throw new Exception("wel_cus_code_miss");}

			$this->wel_tran_date=(($this->wel_tran_date=="")?"null":"'".$this->wel_tran_date."'");

			if($this->wel_cus_code!="")
			{
				$sql="SELECT wel_cus_code from #__wel_cusmasm WHERE wel_cus_code='".$this->wel_cus_code."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_cus_code_not_found");}
			}

			if($this->wel_comp_code!="")
			{
				$sql="SELECT wel_comp_code from #__wel_compflm WHERE wel_comp_code='".$this->wel_comp_code."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_comp_code_not_found");}
			}
			
			if($this->wel_dely_code!="")
			{
				$sql="SELECT wel_dely_line from #__wel_delyflm WHERE wel_cus_code='".$this->wel_cus_code."' and wel_dely_line='".$this->wel_dely_code."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_dely_code_not_found");}
			}
			
			try
			{
				mysql_query("begin");
				
				$sql="UPDATE #__wel_dnhdrfm SET ".
						"wel_dn_date=$this->wel_tran_date,".
						"wel_dely_code='$this->wel_dely_code',".
						"wel_remark='$this->wel_remark',".
						"wel_upd_user='{$_SESSION['wel_user_id']}',".
						"wel_upd_date=now() ".
						"WHERE wel_dn_no='$this->wel_dn_no' LIMIT 1";	
				$sql=revert_to_the_available_sql($sql);
				if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}

				mysql_query("commit");
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}		
		if($msg_code==""){$msg_code="edit_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	public function delete()
	{
		$msg_code="";
		$return_val=array();

		try
		{
			$conn=werp_db_connect();
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_delete")){throw new Exception("unauthorized_access");}

			if ($this->wel_dn_no=="" ){throw new Exception("wel_dn_no_miss");}
			$sql="SELECT * FROM #__wel_dnhdrfm WHERE wel_dn_no='$this->wel_dn_no' limit 1";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_dn_no_not_found");}

			$sql="SELECT * FROM #__wel_dndetfm WHERE wel_dn_no='$this->wel_dn_no'";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
			while($row=mysql_fetch_array($result))
			{
				$dn_req_qty=doubleval(is_null($row["wel_req_qty"]) ? 0 : $row["wel_req_qty"]);
				$dn_inv_qty=doubleval(is_null($row["wel_inv_qty"]) ? 0 : $row["wel_inv_qty"]);
				$dn_tran_qty=doubleval(is_null($row["wel_tran_qty"]) ? 0 : $row["wel_tran_qty"]);

				if( $dn_tran_qty>0 ){throw new Exception("wel_dn_line_delivered");}
				if( $dn_inv_qty>0 ){throw new Exception("wel_dn_line_invoiced");}
			}

			try
			{
				mysql_query('begin');

					$sql="SELECT * FROM #__wel_dndetfm WHERE wel_dn_no='$this->wel_dn_no'";
					$sql=revert_to_the_available_sql($sql);
					if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					while($row=mysql_fetch_array($result))
					{
						$dn_req_qty=doubleval(is_null($row["wel_req_qty"]) ? 0 : $row["wel_req_qty"]);
						$dn_inv_qty=doubleval(is_null($row["wel_inv_qty"]) ? 0 : $row["wel_inv_qty"]);
						$dn_tran_qty=doubleval(is_null($row["wel_tran_qty"]) ? 0 : $row["wel_tran_qty"]);
						$dn_so_no=is_null($row["wel_so_no"]) ? "" : $row["wel_so_no"];
						$dn_so_line=intval(is_null($row["wel_so_line"]) ? 0 : $row["wel_so_line"]);
						
						if($dn_so_no!="")
						{
							$sql="UPDATE #__wel_sordetm SET ".
									"wel_dn_qty=IFNULL(wel_dn_qty,0)-$dn_req_qty ".
								"WHERE wel_so_no='$dn_so_no' AND wel_so_line=$dn_so_line LIMIT 1";
							$sql=revert_to_the_available_sql($sql);
							if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
						}
					}
	
					$sql="DELETE FROM #__wel_dndetfm WHERE wel_dn_no='$this->wel_dn_no'";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}

					$sql="DELETE FROM #__wel_dnhdrfm WHERE wel_dn_no='$this->wel_dn_no' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}

				mysql_query("commit");
					
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}		
		if($msg_code==""){$msg_code="delete_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}


	/////////////////detail start///////////////////////
	public function delete_detail_tab0()
	{
		$msg_code="";
		$return_val=array();

		try
		{
			$conn=werp_db_connect();
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_delete")){throw new Exception("unauthorized_access");}

			if ($this->wel_dn_no=="" ){throw new Exception("wel_dn_no_miss");}
			if(!is_numeric($this->wel_dn_line)){$this->wel_dn_line=0;}
			$this->wel_dn_line=intval($this->wel_dn_line);

			$sql="SELECT * FROM #__wel_dndetfm WHERE wel_dn_no='$this->wel_dn_no' AND wel_dn_line='$this->wel_dn_line' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}	
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_dn_line_not_found");}	
			$dn_so_no=is_null($row["wel_so_no"]) ? "" : $row["wel_so_no"];
			$dn_so_line=intval(is_null($row["wel_so_line"]) ? 0 : $row["wel_so_line"]);
			$dn_req_qty=doubleval(is_null($row["wel_req_qty"]) ? 0 : $row["wel_req_qty"]);
			$dn_inv_qty=doubleval(is_null($row["wel_inv_qty"]) ? 0 : $row["wel_inv_qty"]);
			$dn_tran_qty=doubleval(is_null($row["wel_tran_qty"]) ? 0 : $row["wel_tran_qty"]);

			if( $dn_tran_qty>0 ){throw new Exception("wel_dn_line_delivered");}
			if( $dn_inv_qty>0 ){throw new Exception("wel_dn_line_invoiced");}

			try
			{
				mysql_query('begin');

					if($dn_so_no!="")
					{
						$sql="UPDATE #__wel_sordetm SET ".
								"wel_dn_qty=IFNULL(wel_dn_qty,0)-$dn_req_qty ".
							"WHERE wel_so_no='$dn_so_no' AND wel_so_line=$dn_so_line LIMIT 1";
						$sql=revert_to_the_available_sql($sql);
						if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					}

					$sql="DELETE FROM #__wel_dndetfm WHERE wel_dn_no='$this->wel_dn_no' AND wel_dn_line='$this->wel_dn_line' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}

					$sql="UPDATE #__wel_dnhdrfm SET ".
						"wel_upd_user='{$_SESSION['wel_user_id']}',".
						"wel_upd_date=now() ".
						"WHERE wel_dn_no='$this->wel_dn_no'";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					
				mysql_query("commit");
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}
			
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		if($msg_code==""){$msg_code="delete_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}

	
	public function delete_detail_tab0_all()
	{
		$msg_code="";
		$return_val=array();

		try
		{
			$conn=werp_db_connect();
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_delete")){throw new Exception("unauthorized_access");}

			if ($this->wel_dn_no=="" ){throw new Exception("wel_dn_no_miss");}

			$sql="SELECT * FROM #__wel_dndetfm WHERE wel_dn_no='$this->wel_dn_no'";
			$sql=revert_to_the_available_sql($sql);
			if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}
			while($row=mysql_fetch_array($result))
			{
				$dn_req_qty=doubleval(is_null($row["wel_req_qty"]) ? 0 : $row["wel_req_qty"]);
				$dn_inv_qty=doubleval(is_null($row["wel_inv_qty"]) ? 0 : $row["wel_inv_qty"]);
				$dn_tran_qty=doubleval(is_null($row["wel_tran_qty"]) ? 0 : $row["wel_tran_qty"]);

				if( $dn_tran_qty>0 ){throw new Exception("wel_dn_line_delivered");}
				if( $dn_inv_qty>0 ){throw new Exception("wel_dn_line_invoiced");}
			}

			try
			{
				mysql_query('begin');
	
					$sql="SELECT * FROM #__wel_dndetfm WHERE wel_dn_no='$this->wel_dn_no'";
					$sql=revert_to_the_available_sql($sql);
					if(!$result=mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					while($row=mysql_fetch_array($result))
					{
						$dn_req_qty=doubleval(is_null($row["wel_req_qty"]) ? 0 : $row["wel_req_qty"]);
						$dn_inv_qty=doubleval(is_null($row["wel_inv_qty"]) ? 0 : $row["wel_inv_qty"]);
						$dn_tran_qty=doubleval(is_null($row["wel_tran_qty"]) ? 0 : $row["wel_tran_qty"]);
						$dn_so_no=is_null($row["wel_so_no"]) ? "" : $row["wel_so_no"];
						$dn_so_line=intval(is_null($row["wel_so_line"]) ? 0 : $row["wel_so_line"]);

						if($dn_so_no!="")
						{
							$sql="UPDATE #__wel_sordetm SET ".
									"wel_dn_qty=IFNULL(wel_dn_qty,0)-$dn_req_qty ".
								"WHERE wel_so_no='$dn_so_no' AND wel_so_line=$dn_so_line LIMIT 1";
							$sql=revert_to_the_available_sql($sql);
							if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
						}
					}
	
					$sql="DELETE FROM #__wel_dndetfm WHERE wel_dn_no='$this->wel_dn_no'";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}

					$sql="UPDATE #__wel_dnhdrfm SET ".
						"wel_upd_user='{$_SESSION['wel_user_id']}',".
						"wel_upd_date=now() ".
						"WHERE wel_dn_no='$this->wel_dn_no'";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					
				mysql_query("commit");
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}
				
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		if($msg_code==""){$msg_code="delete_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}

	///////////////detail end///////////////////////
}
?>
