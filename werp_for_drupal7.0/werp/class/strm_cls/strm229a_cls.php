<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>

<?php 
	class strm229a_cls
	{
		public $wel_ctrl_flow="";
		public $wel_dn_date="";
		public $wel_wh_fm="";
		public $wel_wh_to="";
		public $wel_dn_no="";
		public $wel_dn_line=0;

		public $wel_part_no="";
		public $wel_lot_no="";
		public $wel_tran_qty=0;
		public $wel_ref_no="";
		public $wel_tran_rmk="";
		
		private $wel_prog_code="strm229";
		
		//读取出货单入仓
		public function read()
		{
			$msg_code="";
			$return_val=array();
			
			try
			{
				$conn=werp_db_connect();
				
				if (!check_permission($conn,$this->wel_prog_code,"wel_access_read")){throw new Exception("unauthorized_access");}
				
				//工作单明细是否存在
				$sql="SELECT * FROM #__wel_dndetfm WHERE wel_dn_no='".$this->wel_dn_no."' AND wel_dn_line=".$this->wel_dn_line." LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_dn_no_not_found");}
				$int__count=0;
				while ($int__count < mysql_num_fields($result)) 
				{
					$field_name=mysql_fetch_field($result,$int__count)->name;
					$return_val[$field_name]=$row[$field_name];
					$int__count++;
				}

				$tmp_part_no=is_null($row["wel_part_no"]) ? "" : $row["wel_part_no"];
				//物料档案
				$sql="SELECT * FROM #__wel_partflm WHERE wel_part_no='".$tmp_part_no."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
				if($row=mysql_fetch_array($result))
				{
					$return_val["wel_part_des"]=$row[wel_part_des];
					$return_val["wel_part_des1"]=$row[wel_part_des1];
					$return_val["wel_part_des2"]=$row[wel_part_des2];
					$return_val["wel_part_des3"]=$row[wel_part_des3];
					$return_val["wel_part_des4"]=$row[wel_part_des4];
					$return_val["wel_unit"]=$row[wel_unit];
				}else{
					$return_val["wel_part_des"]="";
					$return_val["wel_part_des1"]="";
					$return_val["wel_part_des2"]="";
					$return_val["wel_part_des3"]="";
					$return_val["wel_part_des4"]="";
					$return_val["wel_unit"]="";
				}

				//物品仓存数
				$sql="SELECT wel_stk_qty FROM #__wel_whlotfm WHERE wel_wh_code='".$this->wel_wh_fm."' AND wel_part_no='".$tmp_part_no."' AND wel_lot_no='' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
				if($row=mysql_fetch_array($result))
				{
					$return_val["wel_stk_qty"]=$row["wel_stk_qty"];
				}else{
					$return_val["wel_stk_qty"]=0;
				}
				mysql_free_result($result);
				
			}
			catch (Exception $e)
			{
				$msg_code=$e->getMessage();
			}
			$return_val["msg_code"]=$msg_code;
			return $return_val;
		}

		//出货单出仓
		public function addnew()
		{
			$msg_code="";
			$return_val=array();
			
			try
			{
				$conn=werp_db_connect();
				
				if (!check_permission($conn,$this->wel_prog_code,"wel_access_edit")){throw new Exception("unauthorized_access");}

				if($this->wel_ctrl_flow==""){throw new Exception("wel_ctrl_flow_miss");}
				if($this->wel_dn_date==""){throw new Exception("wel_dn_date_miss");}
				if($this->wel_dn_no==""){throw new Exception("wel_dn_no_miss");}

				if(!is_numeric($this->wel_dn_line)) {throw new Exception("wel_dn_line_miss");}
				$this->wel_dn_line=doubleval($this->wel_dn_line);
				if($this->wel_dn_line==0){throw new Exception("wel_dn_line_miss");}

				if(!is_numeric($this->wel_tran_qty)) {throw new Exception("wel_tran_qty_miss");}
				$this->wel_tran_qty=doubleval($this->wel_tran_qty);
				if($this->wel_tran_qty==0){throw new Exception("wel_tran_qty_miss");}

				if($this->wel_ctrl_flow!="")
				{
					$sql="SELECT * FROM #__wel_whctrlm WHERE wel_ctrl_flow='".$this->wel_ctrl_flow."' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
					if(!($row=mysql_fetch_array($result))){throw new Exception("wel_whctrlm_not_found");}	
					$str__wh_fm=is_null($row["wel_wh_fm"]) ? "" : $row["wel_wh_fm"];
					$str__wh_to=is_null($row["wel_wh_to"]) ? "" : $row["wel_wh_to"];
					$int__dis_able=intval(is_null($row["wel_dis_able"]) ? 0 : $row["wel_dis_able"]);
					$int__shp_flow=intval(is_null($row["wel_shp_flow"]) ? 0 : $row["wel_shp_flow"]);

					if($int__shp_flow!=1){throw new Exception("wel_whctrlm_not_shp_flow");}
					if($int__dis_able==1){throw new Exception("wel_whctrlm_disable");}

					$sql="SELECT * FROM #__wel_whlocfm WHERE wel_wh_code='".$str__wh_fm."' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
					if(!($row=mysql_fetch_array($result))){throw new Exception("wel_wh_fm_not_found");}	
				
					$sql="SELECT * FROM #__wel_whlocfm WHERE wel_wh_code='".$str__wh_to."' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
					if(!($row=mysql_fetch_array($result))){throw new Exception("wel_wh_to_not_found");}	
				}

				if($this->wel_dn_date!="")
				{
					$this->wel_dn_date=(($this->wel_dn_date=="") ? "null" : $this->wel_dn_date);
					$sql="SELECT * FROM #__wel_closedm LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());} 
					if(!($row=mysql_fetch_array($result))){throw new Exception("system_init_error");}
					$sys_phy_date=is_null($row["wel_period1"]) ? $row["wel_phy_date"] : $row["wel_period1"];

					if( date("Y-m-d",strtotime($sys_phy_date)) >= date("Y-m-d",strtotime($this->wel_dn_date)) )
					{
						throw new Exception("wel_dn_date_error");
					}
				}

				//出货单明细是否存在
				$sql="SELECT * FROM #__wel_dndetfm ".
						"WHERE wel_dn_no='".$this->wel_dn_no."' AND wel_dn_line=".$this->wel_dn_line." LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
				if(!$row=mysql_fetch_array($result)){throw new Exception("wel_dn_detail_not_found");}
				$dn_part_no=is_null($row["wel_part_no"]) ? "" : $row["wel_part_no"];
				$dn_cus_code=is_null($row["wel_cus_code"]) ? "" : $row["wel_cus_code"];
				$dn_so_no=is_null($row["wel_so_no"]) ? "" : $row["wel_so_no"];
				$dn_so_line=intval(is_null($row["wel_so_line"]) ? 0 : $row["wel_so_line"]);
				$dn_req_qty=ROUND(doubleval(is_null($row["wel_req_qty"]) ? 0 : $row["wel_req_qty"]),4);
				$dn_tran_qty=ROUND(doubleval(is_null($row["wel_tran_qty"]) ? 0 : $row["wel_tran_qty"]),4);

				//品号是否存在
				$sql="SELECT * FROM #__wel_partflm WHERE wel_part_no='".$dn_part_no."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
				if(!($row=mysql_fetch_array($result))){throw new Exception("wel_part_no_not_found");}
				$part_cat_code=is_null($row["wel_cat_code"]) ? "" : $row["wel_cat_code"];

				if($this->wel_tran_qty<0)
				{
					if($dn_tran_qty+$this->wel_tran_qty < 0){throw new Exception("wel_tran_qty_excess_dn");}
				}else{
					if($dn_req_qty-$dn_tran_qty-$this->wel_tran_qty < 0){throw new Exception("wel_tran_qty_excess_dn");}

					$stk_stk_qty = 0;
					$sql="SELECT * FROM #__wel_whlotfm ".
						" WHERE wel_wh_code='".$str__wh_fm."' AND wel_part_no='".$dn_part_no."' ".
						" AND wel_lot_no='".$this->wel_lot_no."' AND wel_stk_loc='' LIMIT 1";
					$sql=revert_to_the_available_sql($sql);
					if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
					if($row=mysql_fetch_array($result))
					{
						$stk_stk_qty=doubleval(is_null($row["wel_stk_qty"]) ? 0 : $row["wel_stk_qty"]);
					}

					if($stk_stk_qty-$this->wel_tran_qty<0){throw new Exception("wel_tran_qty_excess_stock");}
				}
				
				try
				{
					mysql_query('begin');
						
						//获得最新交易记录号
						$sql="SELECT wel_trn_recno FROM #__wel_tranflm ORDER BY wel_trn_recno DESC LIMIT 1";
						$sql=revert_to_the_available_sql($sql);
						if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
						if(!($row=mysql_fetch_array($result)))
						{
							$dec__next_trn_recno=1;
						}
						else
						{
							$dec__next_trn_recno=doubleval(is_null($row["wel_trn_recno"]) ? 1 : $row["wel_trn_recno"]+1);
						}

						//添加记录到tranflm表
						$sql="INSERT INTO #__wel_tranflm SET ".
								"wel_trn_recno=".$dec__next_trn_recno.",".
								"wel_tran_date='".$this->wel_dn_date."',".
								"wel_wh_fm='".$str__wh_fm."',".
								"wel_wh_to='".$str__wh_to."',".
								"wel_tran_code='".$this->wel_ctrl_flow."',".
								"wel_flow_code='SHP',".
								"wel_dn_no='".$this->wel_dn_no."',".
								"wel_cus_code='".$dn_cus_code."',".
								"wel_so_no='".$dn_so_no."',".
								"wel_so_line=".$dn_so_line.",".
								"wel_part_no='".$dn_part_no."',".
								"wel_cat_code='".$part_cat_code."',".
								"wel_lot_no_fm='".$this->wel_lot_no."',".
								"wel_tran_qty=".$this->wel_tran_qty.",".
								"wel_ref_no='".$this->wel_ref_no."',".
								"wel_tran_rmk='".$this->wel_tran_rmk."',".
								"wel_crt_user='{$_SESSION['wel_user_id']}', ".
								"wel_crt_date=now()";
						$sql=revert_to_the_available_sql($sql);
						if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}

						//更新物品仓存数
						$sql="SELECT * FROM #__wel_whlotfm ".
							" WHERE wel_wh_code='".$str__wh_fm."' AND wel_part_no='".$dn_part_no."' ".
							" AND wel_lot_no='".$this->wel_lot_no."' AND wel_stk_loc='' LIMIT 1";
						$sql=revert_to_the_available_sql($sql);
						if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	
						if($row=mysql_fetch_array($result))
						{
							$sql="UPDATE #__wel_whlotfm SET ".
								" wel_stk_qty=IFNULL(wel_stk_qty,0)-$this->wel_tran_qty ".
								" WHERE wel_wh_code='".$str__wh_fm."' AND wel_part_no='".$dn_part_no."' ".
								" AND wel_lot_no='".$this->wel_lot_no."' AND wel_stk_loc='' LIMIT 1";
							$sql=revert_to_the_available_sql($sql);
							if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
						}else{
							$sql="INSERT INTO #__wel_whlotfm SET ".
									"wel_wh_code='".$str__wh_fm."',".
									"wel_part_no='".$dn_part_no."',".
									"wel_lot_no='".$this->wel_lot_no."',".
									"wel_stk_loc='',".
									"wel_stk_qty=".-1*$this->wel_tran_qty.",".
									"wel_crt_user='{$_SESSION['wel_user_id']}', ".
									"wel_crt_date=now()";
							$sql=revert_to_the_available_sql($sql);
							if(!($result1=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
						}

						//修改出货单明细
						$sql="UPDATE #__wel_dndetfm SET ".
							" wel_tran_qty=IFNULL(wel_tran_qty,0)+$this->wel_tran_qty".
							" WHERE wel_dn_no='".$this->wel_dn_no."' ".
							" AND wel_dn_line=".$this->wel_dn_line." LIMIT 1";
						$sql=revert_to_the_available_sql($sql);
						if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
						
						mysql_free_result($result);

					mysql_query('commit');
					
				}
				catch (Exception $e1)
				{
					mysql_query('rollback');
					$msg_code=$e1->getMessage();
				}
				
			}
			catch (Exception $e)
			{
				$msg_code=$e->getMessage();
			}
			if($msg_code==""){$msg_code="addnew_succee";}
			$return_val["msg_code"]=$msg_code;
			return $return_val;
		}
		
	}
?>