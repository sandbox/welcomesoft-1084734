<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT

?>
<?php
class sysm007a_cls{
	public $wel_group_code="";
	public $wel_user_id="";
	public $from_wel_group_code="";
	
	private $private_wel_prog_code="sysm007";

	public function read(){
		$msg_code="";
		$return_val=array();
		
		try{
			$conn=werp_db_connect();
			if (!check_permission($conn,$this->private_wel_prog_code,"wel_access_read")){throw new Exception("unauthorized_access");}

			$sql="SELECT usergroup.*,users.name AS wel_user_name ".
				"FROM #__wel_usergroup AS usergroup LEFT JOIN #__users AS users ".
				"ON usergroup.wel_user_id=users.name WHERE ".
				"usergroup.wel_user_id='$this->wel_user_id' and ".
				"usergroup.wel_group_code='$this->wel_group_code' limit 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	//查询sql时出错了
			if(!($row=mysql_fetch_array($result))){throw new Exception("wel_user_id_not_found");}	//没有符合条件的记录
			$int__count=0;
			while ($int__count < mysql_num_fields($result)) {
				$field_name=mysql_fetch_field($result,$int__count)->name;
				$return_val[$field_name]=$row[$field_name];
				//$msg_code=$msg_code."|".$field_name;
				$int__count++;
			}
			mysql_free_result($result);
			throw new Exception("");
		}
		catch (Exception $e){
			$msg_code=$e->getMessage();
		}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	public function addnew(){
		$msg_code="";
		$return_val=array();
		
		try{
			$conn=werp_db_connect();
			if (!check_permission($conn,$this->private_wel_prog_code,"wel_access_addnew")){throw new Exception("unauthorized_access");}

			if ($this->wel_group_code==""){throw new Exception("wel_group_code_miss");}
			if (!$this->check_wel_groupflm($conn,$this->wel_group_code)){throw new Exception("wel_group_code_not_found");}

			if ($this->from_wel_group_code!=""){
				if (!$this->check_wel_groupflm($conn,$this->from_wel_group_code)){throw new Exception("from_wel_group_code_not_found");}
			}elseif ($this->wel_user_id!=""){
				if (!$this->check_users($conn,$this->wel_user_id)){throw new Exception("wel_user_id_not_found");}
			}else{
				throw new Exception("wel_user_id_miss");
			}
			
			if ($this->from_wel_group_code!=""){
				$sql="SELECT wel_user_id FROM #__wel_usergroup WHERE wel_group_code='$this->from_wel_group_code' limit 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}	//查询sql时出错了
				//没有符合条件的记录
				if(!($row=mysql_fetch_array($result))){throw new Exception("from_wel_group_code_detail_not_found");}
				mysql_free_result($result);
				
				try{
					mysql_query('begin');
					$sql="INSERT INTO #__wel_usergroup(wel_user_id,wel_group_code) SELECT wel_user_id,'$this->wel_group_code' ".
						"FROM #__wel_usergroup WHERE wel_group_code='$this->from_wel_group_code' and not wel_user_id in ".
						"(SELECT wel_user_id FROM #__wel_usergroup WHERE wel_group_code='$this->wel_group_code')";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					mysql_query('commit');
					$msg_code="addnew_succee";
				}catch (Exception $e){
					$msg_code=$e->getMessage();
					mysql_query('rollback');
				}
			}else{
				$sql="SELECT wel_user_id FROM #__wel_usergroup WHERE wel_user_id='$this->wel_user_id' and ".
					"wel_group_code='$this->wel_group_code' limit 1";
				$sql=revert_to_the_available_sql($sql);
				if(!(($result=mysql_query($sql,$conn)))){throw new Exception(mysql_error());}	//查询sql时出错了
				if(($row=mysql_fetch_array($result))){throw new Exception("wel_user_id_exist");}	//单据已存在
				mysql_free_result($result);

				try{
					mysql_query('begin');
					$sql="INSERT INTO #__wel_usergroup SET ".
						"wel_user_id='$this->wel_user_id',".
						"wel_group_code='$this->wel_group_code' ";
					$sql=revert_to_the_available_sql($sql);
					if(!mysql_query($sql,$conn)){throw new Exception(mysql_error());}
					mysql_query('commit');
					$msg_code="addnew_succee";
				}catch (Exception $e){
					$msg_code=$e->getMessage();
					mysql_query('rollback');
				}
			}
		}
		catch (Exception $e){
			$msg_code=$e->getMessage();
		}
		$return_val["msg_code"]=$msg_code;
		$return_val["wel_user_id"]=$this->wel_user_id;
		$return_val["wel_group_code"]=$this->wel_group_code;
		return $return_val;
	}
	
	public function edit(){	//没有编辑的功能
		$msg_code="";
		$return_val=array();
		$return_val["msg_code"]="edit_succee";
		return $return_val;
	}
	
	private function check_wel_groupflm($conn,$wel_group_code){
		$check_found=false;
		if($wel_group_code=="" ) return true;//如果系空的,唔理它
		try{
			$sql="SELECT * from #__wel_groupflm WHERE wel_group_code='$wel_group_code' limit 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());} //查询sql时出错了
			if(!($row=mysql_fetch_array($result))){throw new Exception("");}	//没有符合条件的记录
			mysql_free_result($result);
			$check_found=true;
		}catch(Exception $e){
		}
		return $check_found;
	}
	
	private function check_users($conn,$wel_user_id){
		$check_found=false;
		if($wel_user_id=="" ) return true;//如果系空的,唔理它
		try{
			$sql="SELECT * from #__users WHERE name='$wel_user_id' limit 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());} //查询sql时出错了
			if(!($row=mysql_fetch_array($result))){throw new Exception("");}	//没有符合条件的记录
			mysql_free_result($result);
			$check_found=true;
		}catch(Exception $e){
		}
		return $check_found;
	}
}
?>