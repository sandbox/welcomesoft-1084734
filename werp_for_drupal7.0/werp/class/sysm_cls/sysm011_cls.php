<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>

<?php
class sysm011_cls
{
	public $wel_comp_code="";
	public $wel_comp_des="";
		
	public $wel_comp_add1="";
	public $wel_comp_add2="";
	public $wel_comp_add3="";
	public $wel_comp_add4="";
		
	public $wel_comp_cont="";
	public $wel_comp_tele="";
	public $wel_comp_fax="";

	private $wel_prog_code="sysm011";

	public function read()
	{
		$msg_code="";
		$return_val=array();
 
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_read")){throw new Exception("unauthorized_access");}
			
			$sql="SELECT * FROM #__wel_compflm WHERE wel_comp_code='".$this->wel_comp_code."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_comp_code_not_found");}
			$int__count=0;
			while ($int__count < mysql_num_fields($result)) 
			{
				$field_name=mysql_fetch_field($result,$int__count)->name;
				$return_val[$field_name]=$row[$field_name];
				$int__count++;
			}
			mysql_free_result($result);
			
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	public function addnew()
	{
		$msg_code="";
		$return_val=array();
 
		try
		{
			$conn	= werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_addnew")){throw new Exception("unauthorized_access");}
			
			if($this->wel_comp_code==""){throw new Exception("wel_comp_code_miss");}
			if($this->wel_comp_des==""){throw new Exception("wel_comp_des_miss");}
			$sql="SELECT * FROM #__wel_compflm WHERE wel_comp_code='".$this->wel_comp_code."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if($row=mysql_fetch_array($result)){throw new Exception("wel_comp_code_exist");}
			
			try
			{
				mysql_query("begin");
				
				$sql="INSERT INTO #__wel_compflm(
					wel_comp_code,wel_comp_des,wel_comp_add1,
					wel_comp_add2,wel_comp_add3,wel_comp_add4,
					wel_comp_cont,wel_comp_tele,wel_comp_fax) ".
					"VALUES('".$this->wel_comp_code."','".$this->wel_comp_des."','".
					$this->wel_comp_add1."','".$this->wel_comp_add2."','".$this->wel_comp_add3."','".$this->wel_comp_add4."','".
					$this->wel_comp_cont."','".$this->wel_comp_tele."','".$this->wel_comp_fax."' )";	 
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				mysql_free_result($result);
				
				mysql_query("commit");
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		if($msg_code==""){$msg_code="addnew_succee";}
		$return_val["msg_code"]=$msg_code;
		$return_val["wel_comp_code"]=$this->wel_comp_code;
		return $return_val;
	}
	
	public function edit()
	{
		$msg_code="";
		$return_val=array();
 
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_edit")){throw new Exception("unauthorized_access");}
			
			if($this->wel_comp_code==""){throw new Exception("wel_comp_code_miss");}
			if($this->wel_comp_des==""){throw new Exception("wel_comp_des_miss");}
			$sql="SELECT * FROM #__wel_compflm WHERE wel_comp_code='".$this->wel_comp_code."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_comp_code_not_found");}
			
			try
			{
				mysql_query("begin");
				
				$sql="UPDATE #__wel_compflm SET ".
					"wel_comp_des='".$this->wel_comp_des."',".
					"wel_comp_add1='".$this->wel_comp_add1."',".
					"wel_comp_add2='".$this->wel_comp_add2."',".
					"wel_comp_add3='".$this->wel_comp_add3."',".
					"wel_comp_add4='".$this->wel_comp_add4."',".
					"wel_comp_cont='".$this->wel_comp_cont."',".
					"wel_comp_tele='".$this->wel_comp_tele."',".
					"wel_comp_fax='".$this->wel_comp_fax."' ".
					"WHERE wel_comp_code='".$this->wel_comp_code."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				mysql_free_result($result);
					
				mysql_query("commit");
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}	
			
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}		
		if($msg_code==""){$msg_code="edit_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	public function delete()
	{
		$msg_code="";
		$return_val=array();
 
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_delete")){throw new Exception("unauthorized_access");}
			
			if($this->wel_comp_code==""){throw new Exception("wel_comp_code_miss");}
			$sql="SELECT * FROM #__wel_compflm WHERE wel_comp_code='".$this->wel_comp_code."' LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("wel_comp_code_not_found");}
			
			try
			{
				mysql_query("begin");
				
				$sql="DELETE FROM #__wel_compflm WHERE wel_comp_code='".$this->wel_comp_code."' LIMIT 1";
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				mysql_free_result($result);
				
				mysql_query("commit");
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		if($msg_code==""){$msg_code="delete_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
}
?>
