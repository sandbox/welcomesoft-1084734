<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>

<?php
class sysm012_cls
{
	public $wel_comp_des="";
	public $wel_comp_add1="";
	public $wel_comp_add2="";
	public $wel_comp_add3="";
	public $wel_comp_add4="";
		
	public $wel_comp_cont="";
	public $wel_comp_tele="";
	public $wel_comp_fax="";

	private $wel_prog_code="sysm012";

	public function read()
	{
		$msg_code="";
		$return_val=array();
 
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_read")){throw new Exception("unauthorized_access");}
			
			$sql="SELECT * FROM #__wel_syssets LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if(!$row=mysql_fetch_array($result)){throw new Exception("");}		//无需错误信息
			$int__count=0;
			while ($int__count < mysql_num_fields($result)) 
			{
				$field_name=mysql_fetch_field($result,$int__count)->name;
				$return_val[$field_name]=$row[$field_name];
				$int__count++;
			}
			mysql_free_result($result);
			
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
	
	public function edit()
	{
		$msg_code="";
		$return_val=array();
 
		try
		{
			$conn=werp_db_connect();
			
			if (!check_permission($conn,$this->wel_prog_code,"wel_access_edit")){throw new Exception("unauthorized_access");}
			
			$wel_system_settings_exists=false;
			$sql="SELECT * FROM #__wel_syssets LIMIT 1";
			$sql=revert_to_the_available_sql($sql);
			if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
			if($row=mysql_fetch_array($result)){$wel_system_settings_exists=true;}
			$wel_sets_code=$row["wel_sets_code"];
			
			try
			{
				mysql_query("begin");
				
				if ($wel_system_settings_exists){
					$sql="UPDATE #__wel_syssets SET ".
						"wel_comp_des='".$this->wel_comp_des."',".
						"wel_comp_add1='".$this->wel_comp_add1."',".
						"wel_comp_add2='".$this->wel_comp_add2."',".
						"wel_comp_add3='".$this->wel_comp_add3."',".
						"wel_comp_add4='".$this->wel_comp_add4."',".
						"wel_comp_cont='".$this->wel_comp_cont."',".
						"wel_comp_tele='".$this->wel_comp_tele."',".
						"wel_comp_fax='".$this->wel_comp_fax."' ".
						"WHERE wel_sets_code='".$wel_sets_code."' LIMIT 1";
				}else{
					$sql="INSERT INTO #__wel_syssets(wel_sets_code,wel_comp_des,
						wel_comp_add1,wel_comp_add2,
						wel_comp_add3,wel_comp_add4,
						wel_comp_cont,wel_comp_tele,wel_comp_fax) ".
						"VALUES('".md5(uniqid(rand()))."','".$this->wel_comp_des."','".
						$this->wel_comp_add1."','".$this->wel_comp_add2."','".
						$this->wel_comp_add3."','".$this->wel_comp_add4."','".
						$this->wel_comp_cont."','".$this->wel_comp_tele."','".$this->wel_comp_fax."' )";	 
				}
				$sql=revert_to_the_available_sql($sql);
				if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
				
				mysql_query("commit");
			}
			catch (Exception $e1)
			{
				mysql_query("rollback");
				throw new Exception($e1->getMessage());
			}	
			
		}
		catch (Exception $e)
		{
			$msg_code=$e->getMessage();
		}		
		if($msg_code==""){$msg_code="edit_succee";}
		$return_val["msg_code"]=$msg_code;
		return $return_val;
	}
}
?>
