<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
// no direct access
define( 'WERP_EXEC', 1 );

define( 'WERP_PACKAGE_TYPE', 'drupal' );
if (!defined("DS")){define( 'DS', DIRECTORY_SEPARATOR );}

function extract_wel_db_config(){
	global $databases;
	$wel_db_config=array();
	$wel_db_config["wel_db_host"] = $databases['default']['default']['host'];
	$wel_db_config["wel_db_user"] = $databases['default']['default']['username'];
	$wel_db_config["wel_db_password"] = $databases['default']['default']['password'];
	$wel_db_config["wel_db_name"] = $databases['default']['default']['database'];
	$wel_db_config["wel_table_prefix"] = $databases['default']['default']['prefix'];
	$wel_db_config["wel_db_type"] = "mysql";
	return $wel_db_config;		
	//以下为drupal6.x的做法
	global $db_url,$db_prefix;

	$url = parse_url($db_url);
	// Decode url-encoded information in the db connection string
	$url['user'] = urldecode($url['user']);
	// Test if database url has a password.
	$url['pass'] = isset($url['pass']) ? urldecode($url['pass']) : '';
	$url['host'] = urldecode($url['host']);
	$url['path'] = urldecode($url['path']);
	
	$wel_db_config=array();
	$wel_db_config["wel_db_host"] = $url['host'];
	$wel_db_config["wel_db_user"] = $url['user'];
	$wel_db_config["wel_db_password"] = $url['pass'];
	$wel_db_config["wel_db_name"] = substr($url['path'], 1);
	$wel_db_config["wel_table_prefix"] = $db_prefix;
	$wel_db_config["wel_db_type"] = "mysql";
	return $wel_db_config;		
}

include_once( dirname(__FILE__).DS.'function.sql.php' );
include_once( dirname(__FILE__).DS.'uninstall.sql.php' );
?>