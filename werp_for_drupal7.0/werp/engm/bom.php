<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>

<?php
tree_html_heading();
?>

<div style="display:none;">
	<input type="button" id="btn_wel_test_info" onclick="javascript:test_info();" value="view node info" />
</div>

<script language="javascript">
function test_info(){
	if (tree_node.node_id==""){return false;}
	window.alert(
		"node_id:" + tree_node.node_id +"\n" + 
		"wel_fg_no:" + tree_node.wel_fg_no +"\n" + 
		"wel_assm_no:" + tree_node.wel_assm_no +"\n" + 
		"wel_part_no:" + tree_node.wel_part_no +"\n" + 
		"wel_alt_part:" + tree_node.wel_alt_part +"\n" + 
		"wel_had_sub:" + tree_node.wel_had_sub +"\n" + 
		"wel_had_alt:" + tree_node.wel_had_alt +"\n" + 
		"wel_part_des:" + tree_node.wel_part_des +"\n" + 
		"wel_qp_eng:" + tree_node.wel_qp_eng +"\n" + 
		"wel_eng_unit:" + tree_node.wel_eng_unit +"\n" 
		);
}

function test_operation_node(){
	if (tree_node.node_id==""){return null;}else{return tree_node;}
}
</script>

<script type="text/javascript">
var tree_node = new Object();
$(document).ready(function(){
	tree_node.node_id="";
	tree_node.wel_fg_no="";
	tree_node.wel_assm_no="";
	tree_node.wel_part_no="";
	tree_node.wel_alt_part="";
	tree_node.wel_had_sub="";
	tree_node.wel_had_alt="";
	tree_node.wel_part_des="";
	tree_node.wel_qp_eng="";
	tree_node.wel_eng_unit="";
});

var simpleTreeCollection;
$(document).ready(function(){
	simpleTreeCollection = $('.simpleTree').simpleTree({
		drag : false,
		autoclose: false,
		afterClick:function(node){
			
			tree_node.node_id=node.attr("id");
			tree_node.wel_fg_no=node.attr("wel_fg_no");
			tree_node.wel_assm_no=node.attr("wel_assm_no");
			tree_node.wel_part_no=node.attr("wel_part_no");
			tree_node.wel_alt_part=node.attr("wel_alt_part");
			tree_node.wel_had_sub=node.attr("wel_had_sub");
			tree_node.wel_had_alt=node.attr("wel_had_alt");
			tree_node.wel_part_des=node.attr("wel_part_des");
			tree_node.wel_qp_eng=node.attr("wel_qp_eng");
			tree_node.wel_eng_unit=node.attr("wel_eng_unit");

			//alert("text-"+$('span:first',node).text());
		},
		afterDblClick:function(node){
			//alert("text-"+$('span:first',node).text());
		},
		afterMove:function(destination, source, pos){
			//alert("destination-"+destination.attr('id')+" source-"+source.attr('id')+" pos-"+pos);
		},
		afterAjax:function()
		{
			//alert('Loaded');
		},
		animate:true
		//,docToFolderConvert:true
	});
});
</script>


<?php
$txt_wel_fg_no=werp_get_request_var('txt_wel_fg_no');
$sub_node_html=generate_wel_fg_no_node($txt_wel_fg_no);
if($sub_node_html!=""){echo '<div style="height: 100%;width: 100%;overflow: auto;">'.$sub_node_html.'</div>';}

function get_wel_part_des($wel_part_no){
	$wel_part_des="";

	try 
	{
		$conn=werp_db_connect();

		$sql="SELECT wel_part_des FROM #__wel_partflm WHERE wel_part_no='".$wel_part_no."' LIMIT 1";
		$sql=revert_to_the_available_sql($sql);
		if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
		if(!($row=mysql_fetch_array($result))){}
		$wel_part_des=$row["wel_part_des"];
	}
	catch (Exception $e){
		//return $e->getMessage();
		//return "";
	}
	return $wel_part_des;
}

function generate_wel_fg_no_node($wel_fg_no){
	$return_html="";
	
	try 
	{
		$conn=werp_db_connect();
		
		$sql="SELECT *,".
			"IF(IFNULL((".
			"SELECT COUNT(wel_assm_no) FROM #__wel_engbomm as sub_wel_engbomm ".
			"WHERE sub_wel_engbomm.wel_assm_no=#__wel_partflm.wel_part_no".
			"),0)>0,1,0) AS wel_had_sub ".
		"FROM #__wel_partflm WHERE wel_part_no='".$wel_fg_no."' LIMIT 1";
		$sql=revert_to_the_available_sql($sql);
		if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
		if(!($row=mysql_fetch_array($result))){throw new Exception("");}
		
		//$wel_fg_no=is_null($row['wel_part_no']) ? "" : $row['wel_part_no'];
		$wel_assm_no=is_null($row['wel_part_no']) ? "" : $row['wel_part_no'];
		$wel_part_no=is_null($row['wel_part_no']) ? "" : $row['wel_part_no'];

		$wel_had_sub=doubleval(is_null($row["wel_had_sub"]) ? 0 : $row["wel_had_sub"]);
		$wel_had_alt="0";
		$wel_alt_part="";
		$wel_qp_eng="1";
		$wel_eng_unit=is_null($row['wel_eng_unit']) ? "" : $row['wel_eng_unit'];
		$wel_part_des=is_null($row['wel_part_des']) ? "" : $row['wel_part_des'];
		
		$node_id=md5(uniqid(rand()));
		$return_html .='<ul class="simpleTree"><li class="root" id="'.$node_id.'"><span></span>';
		$node_id=md5(uniqid(rand()));
		$node_attribute='
			id="'.str_ireplace('"','&quot;',$node_id).'" 
			wel_fg_no="'.str_ireplace('"','&quot;',$wel_fg_no).'" 
			wel_assm_no="'.str_ireplace('"','&quot;',$wel_assm_no).'" 
			wel_part_no="'.str_ireplace('"','&quot;',$wel_part_no).'" 
			wel_part_des="'.str_ireplace('"','&quot;',$wel_part_des).'" 
			wel_had_sub="'.str_ireplace('"','&quot;',$wel_had_sub).'" 
			wel_had_alt="'.str_ireplace('"','&quot;',$wel_had_alt).'" 
			wel_alt_part="'.str_ireplace('"','&quot;',$wel_alt_part).'" 
			wel_qp_eng="'.str_ireplace('"','&quot;',$wel_qp_eng).'" 
			wel_eng_unit="'.str_ireplace('"','&quot;',$wel_eng_unit).'" 
			';
		$return_html .='<ul><li class="open" '.$node_attribute.'><span>'.
			$wel_part_no.' '.$wel_part_des.' ('.$wel_qp_eng.' '.$wel_eng_unit.')</span>';
		$sub_node_html=generate_wel_assm_no_node($wel_fg_no,$wel_assm_no);
		if($sub_node_html!=""){$return_html .='<ul>'.$sub_node_html.'</ul>';}
		$return_html .='</li></ul></li></ul>';
	}
	catch (Exception $e){
		//return $e->getMessage();
		return "";
	}
	return $return_html;
}


function generate_wel_assm_no_node($wel_fg_no,$wel_assm_no){
	$return_html="";
	
	try 
	{
		$conn=werp_db_connect();
		
		$sql="SELECT wel_part_no FROM #__wel_partflm WHERE wel_part_no='".$wel_assm_no."' LIMIT 1";
		$sql=revert_to_the_available_sql($sql);
		if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
		if(!($row=mysql_fetch_array($result))){throw new Exception("");}

		$sql = "SELECT *,".
			"IF(IFNULL((".
			"SELECT COUNT(wel_assm_no) FROM #__wel_engbomm as sub_wel_engbomm ".
			"WHERE sub_wel_engbomm.wel_assm_no=#__wel_engbomm.wel_part_no".
			"),0)>0,1,0) AS wel_had_sub,".
			"IF(IFNULL((".
			"SELECT COUNT(wel_prod_no) FROM #__wel_altparm as alt_wel_altparm ".
			"WHERE alt_wel_altparm.wel_prod_no='".$wel_fg_no."' ".
			"and alt_wel_altparm.wel_assm_no='".$wel_assm_no."' ".
			"and alt_wel_altparm.wel_part_no=#__wel_engbomm.wel_part_no".
			"),0)>0,1,0) AS wel_had_alt FROM #__wel_engbomm ".
			"WHERE wel_assm_no='".$wel_assm_no."' ORDER BY wel_had_sub DESC,wel_part_no ASC";
		$sql=revert_to_the_available_sql($sql);
		if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
		while ($row=mysql_fetch_array($result))
		{
			//$wel_fg_no=is_null($row['wel_part_no']) ? "" : $row['wel_part_no'];			//成品
			//$wel_assm_no=is_null($row['wel_assm_no']) ? "" : $row['wel_assm_no'];			//父级品号
			$wel_part_no=is_null($row['wel_part_no']) ? "" : $row['wel_part_no'];			//品号

			$wel_had_sub=doubleval(is_null($row["wel_had_sub"]) ? 0 : $row["wel_had_sub"]);
			$wel_had_alt=doubleval(is_null($row["wel_had_alt"]) ? 0 : $row["wel_had_alt"]);
			$wel_alt_part="";
			$wel_qp_eng=doubleval(is_null($row["wel_qp_eng"]) ? 0 : $row["wel_qp_eng"]);	//工程用量
			$wel_eng_unit=is_null($row["wel_eng_unit"]) ? "" : $row["wel_eng_unit"];		//工程单位
			$wel_part_des=get_wel_part_des($wel_part_no);
			
			$node_id=md5(uniqid(rand()));
			$node_attribute='
				id="'.str_ireplace('"','&quot;',$node_id).'" 
				wel_fg_no="'.str_ireplace('"','&quot;',$wel_fg_no).'" 
				wel_assm_no="'.str_ireplace('"','&quot;',$wel_assm_no).'" 
				wel_part_no="'.str_ireplace('"','&quot;',$wel_part_no).'" 
				wel_part_des="'.str_ireplace('"','&quot;',$wel_part_des).'" 
				wel_had_sub="'.str_ireplace('"','&quot;',$wel_had_sub).'" 
				wel_had_alt="'.str_ireplace('"','&quot;',$wel_had_alt).'" 
				wel_alt_part="'.str_ireplace('"','&quot;',$wel_alt_part).'" 
				wel_qp_eng="'.str_ireplace('"','&quot;',$wel_qp_eng).'" 
				wel_eng_unit="'.str_ireplace('"','&quot;',$wel_eng_unit).'" 
				';
			$return_html .='<li class="open" '.$node_attribute.'><span>'.
				$wel_part_no.' '.$wel_part_des.' ('.$wel_qp_eng.' '.$wel_eng_unit.')</span>';
			if($wel_had_sub==1){	//有子物料，继续爆子BOM
				$sub_node_html=generate_wel_assm_no_node($wel_fg_no,$wel_part_no);
				if($sub_node_html!=""){$return_html .='<ul>'.$sub_node_html.'</ul>';}
			}elseif($wel_had_alt==1){	//有代用物料，提取代用物料
				$alt_node_html=generate_wel_alt_part_node($wel_fg_no,$wel_assm_no,$wel_part_no);
				if($alt_node_html!=""){$return_html .='<ul>'.$alt_node_html.'</ul>';}
			}
			$return_html .='</li>';
		}
	}
	catch (Exception $e){
		//return $e->getMessage();
		return "";
	}
	return $return_html;
}

function generate_wel_alt_part_node($wel_fg_no,$wel_assm_no,$wel_part_no){
	$return_html="";
	
	try 
	{
		$conn=werp_db_connect();
		
		$sql="SELECT * FROM #__wel_altparm WHERE wel_prod_no='".$wel_fg_no."' and ".
			"wel_assm_no='".$wel_assm_no."' and wel_part_no='".$wel_part_no."' ".
			"ORDER BY wel_priority ASC,wel_alt_part ASC";
		$sql=revert_to_the_available_sql($sql);
		if(!($result=mysql_query($sql,$conn))){throw new Exception(mysql_error());}
		while ($row=mysql_fetch_array($result))
		{
			//$wel_fg_no=is_null($row['wel_part_no']) ? "" : $row['wel_part_no'];			//成品
			//$wel_assm_no=is_null($row['wel_assm_no']) ? "" : $row['wel_assm_no'];			//父级品号
			//$wel_part_no=is_null($row['wel_part_no']) ? "" : $row['wel_part_no'];			//品号

			$wel_had_sub="0";
			$wel_had_alt="0";
			$wel_alt_part=is_null($row['wel_alt_part']) ? "" : $row['wel_alt_part'];		//代用物料
			$wel_qp_eng=doubleval(is_null($row["wel_ex_qty"]) ? 0 : $row["wel_ex_qty"]);	//代用用量
			$wel_eng_unit=doubleval(is_null($row["wel_priority"]) ? 0 : $row["wel_priority"]);	//代用优先级
			$wel_part_des=get_wel_part_des($wel_alt_part);

			$node_id=md5(uniqid(rand()));
			$node_attribute='
				id="'.str_ireplace('"','&quot;',$node_id).'" 
				wel_fg_no="'.str_ireplace('"','&quot;',$wel_fg_no).'" 
				wel_assm_no="'.str_ireplace('"','&quot;',$wel_assm_no).'" 
				wel_part_no="'.str_ireplace('"','&quot;',$wel_part_no).'" 
				wel_part_des="'.str_ireplace('"','&quot;',$wel_part_des).'" 
				wel_had_sub="'.str_ireplace('"','&quot;',$wel_had_sub).'" 
				wel_had_alt="'.str_ireplace('"','&quot;',$wel_had_alt).'" 
				wel_alt_part="'.str_ireplace('"','&quot;',$wel_alt_part).'" 
				wel_qp_eng="'.str_ireplace('"','&quot;',$wel_qp_eng).'" 
				wel_eng_unit="'.str_ireplace('"','&quot;',$wel_eng_unit).'" 
				';
			$return_html .='<li class="open" '.$node_attribute.'><span>'.
				'*ALT*  '.$wel_alt_part.' '.$wel_part_des.' ('.$wel_qp_eng.')</span></li>';
		}
	}
	catch (Exception $e){
		//return $e->getMessage();
		return "";
	}
	return $return_html;
}

tree_html_footer();
?>
