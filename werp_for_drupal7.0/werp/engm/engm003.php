<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>
<?php
html_heading();
eval(rebuild_layout(__FILE__));
//Draw Body Layout
?>

<script language="javascript"><!--
//var hidden_object_id_list;	//隐藏对象的列表
//var object_id_list;	//所有对象的列表
//权限基本只有 read addnew edit delete approve print 六种
//var access_read;		//读取权限
//var access_addnew;	//新增权限
//var access_edit;		//编辑权限
//var access_delete;	//删除权限
//var access_approve;	//批核权限
//var access_print;		//打印权限
//以上js变量无需设定，已经由类库自动产生，直接使用即可

var external_opt_action="";
var action_page="";
var txt_wel_fg_no="";
var dim_object_id_list="";
var security_button="";

$(document).ready(function(){
	//opt_action操作状态
	//外部要求的操作
	external_opt_action="<?php echo werp_get_request_var('opt_action'); ?>";
	action_page="<?php echo werp_pathinfo_filename(__FILE__); ?>_opt.php";
	txt_wel_fg_no="<?php echo werp_get_request_var('txt_wel_fg_no'); ?>";
	//一直处于暗淡的对象列表(无法编辑的对象)
	dim_object_id_list="txt_wel_fg_des|txt_wel_proj_no|txt_wel_fg_unit|dtxt_wel_bom_date|"+
		"chk_wel_bom_yn|txt_wel_bom_by|"+
		"txt_wel_assm_no|txt_wel_assm_des|txt_wel_part_no|bbtn_wel_part_no|txt_wel_part_des|"+
		"ntxt_wel_scp_fact|txt_wel_eng_unit|bbtn_wel_eng_unit|ntxt_wel_qp_eng|txt_wel_unit|"+
		"chk_wel_featu_yn|rmk_wel_bom_rmk|rmk_wel_bom_loc|btn_detail_save|btn_detail_return|"+
		"txt_wel_fg_no_alt|txt_wel_assm_no_alt|txt_wel_part_no_alt|txt_wel_alt_part_alt|"+
		"bbtn_wel_alt_part_alt|ntxt_wel_ex_qty_alt|ntxt_wel_priority_alt|btn_detail_alt_part_save|btn_detail_alt_part_return|"+
		"txt_wel_assm_no_to|txt_wel_assm_no_from|bbtn_wel_assm_no_from|"+
		"chk_wel_alt_copy|btn_detail_bom_copy_save|btn_detail_bom_copy_return";
	//要用权限控制的按钮列表
	security_button="btn_head_approve|btn_head_not_approve|btn_detail_addnew|btn_detail_edit|"+
		"btn_detail_del|btn_detail_del_all|btn_detail_alt_part_addnew|btn_detail_alt_part_edit|"+
		"btn_detail_alt_part_del|btn_detail_bom_copy|btn_head_print";
});

function return_handler_info(return_message)
{
	//window.alert(return_message);	//显示所有信息供测试时查看
	var ret_msg_arr=return_message.split("|");
	if (ret_msg_arr.length<5){window.alert(return_message);return false;}
	for (var i=0;i<ret_msg_arr.length;i++){ret_msg_arr[i]=un_coding_str(ret_msg_arr[i]);}
	var opt_action=ret_msg_arr[1];
	var msg_code=ret_msg_arr[2];
	var msg_detail=ret_msg_arr[3];
	var msg_script=ret_msg_arr[4];
	switch(opt_action)
	{
		case "bbtn_wel_fg_no_load_click":
			if (msg_detail!==""){window.alert(msg_detail);}
			if (msg_code==""){eval(msg_script);}//执行返回后产生的脚本
			/*
			document.getElementById("bom_sql").src="./engm/bom_tree/bom.php"+
				"?name=bom_grid&txt_wel_fg_no=" + document.getElementById("txt_wel_fg_no").value;
			*/
			document.getElementById("bom_sql").src=WERP_BASE_URI+WERP_EXPA_PARA+
				"&main_page=bom&action_page=bom&main_page_dir=engm&action_page_dir=engm"+
				"&name=bom_grid&txt_wel_fg_no=" + document.getElementById("txt_wel_fg_no").value;
			break;
			
		case "detail_read":
			if (msg_detail!==""){window.alert(msg_detail);return;}
			if(msg_code==""){eval(msg_script);}//执行返回后产生的脚本
			break;
			
		case "detail_alt_part_read":
			if (msg_detail!==""){window.alert(msg_detail);}
			if(msg_code==""){eval(msg_script);}//执行返回后产生的脚本
			break;
			
		case "btn_head_approve_click":
			if (msg_detail!==""){window.alert(msg_detail);}
			eval(msg_script);//执行返回后产生的脚本
			break;
			
		case "btn_head_not_approve_click":
			if (msg_detail!==""){window.alert(msg_detail);}
			eval(msg_script);//执行返回后产生的脚本
			break;
			
		case "detail_addnew":					//新增细节
			if (msg_detail!==""){window.alert(msg_detail);}
			eval(msg_script);//执行返回后产生的脚本
			if(msg_code=="addnew_succee"){}
			break;
			
		case "detail_edit":						//编辑细节
			if (msg_detail!==""){window.alert(msg_detail);}
			eval(msg_script);//执行返回后产生的脚本
			if(msg_code=="edit_succee"){}
			break;
			
		case "detail_del":
			if (msg_detail!==""){window.alert(msg_detail);}
			eval(msg_script);//执行返回后产生的脚本
			if(msg_code=="delete_succee"){}
			break;
			
		case "detail_del_all":
			if (msg_detail!==""){window.alert(msg_detail);}
			eval(msg_script);//执行返回后产生的脚本
			if(msg_code=="delete_succee"){}
			break;
			
		case "detail_alt_part_addnew":					//新增代用物料
			if (msg_detail!==""){window.alert(msg_detail);}
			eval(msg_script);//执行返回后产生的脚本
			if(msg_code=="addnew_succee"){}
			break;
			
		case "detail_alt_part_edit":						//编辑代用物料
			if (msg_detail!==""){window.alert(msg_detail);}
			eval(msg_script);//执行返回后产生的脚本
			if(msg_code=="edit_succee"){}
			break;
			
		case "detail_alt_part_del":
			if (msg_detail!==""){window.alert(msg_detail);}
			eval(msg_script);//执行返回后产生的脚本
			if(msg_code=="delete_succee"){}
			break;

		case "detail_bom_copy":					//新增细节
			if (msg_detail!==""){window.alert(msg_detail);}
			eval(msg_script);//执行返回后产生的脚本
			if(msg_code=="copy_succee"){}
			break;
			
		default:
			if (msg_detail!==""){window.alert(msg_detail);}eval(msg_script);
			break;
	}
}

function clear_and_disable_other_tab(){
	document.getElementById("txt_wel_assm_no").value="";
	document.getElementById("txt_wel_assm_des").value="";
	document.getElementById("txt_wel_part_no").value="";
	document.getElementById("txt_wel_part_des").value="";
	document.getElementById("txt_wel_eng_unit").value="";
	document.getElementById("txt_wel_unit").value="";
	document.getElementById("ntxt_wel_qp_eng").value="";
	document.getElementById("ntxt_wel_scp_fact").value="";
	document.getElementById("rmk_wel_bom_rmk").value="";
	document.getElementById("rmk_wel_bom_loc").value="";
	document.getElementById("chk_wel_featu_yn").checked=false;
	enable_object("txt_wel_assm_no|txt_wel_assm_des|txt_wel_part_no|txt_wel_part_des|"+
		"txt_wel_eng_unit|"+
		"txt_wel_unit|ntxt_wel_qp_eng|ntxt_wel_scp_fact|rmk_wel_bom_rmk|rmk_wel_bom_loc|"+
		"bbtn_wel_part_no|bbtn_wel_eng_unit|chk_wel_featu_yn|btn_detail_save|btn_detail_return",false,"");
	
	document.getElementById("txt_wel_fg_no_alt").value="";
	document.getElementById("txt_wel_assm_no_alt").value="";
	document.getElementById("txt_wel_part_no_alt").value="";
	document.getElementById("txt_wel_alt_part_alt").value="";
	document.getElementById("ntxt_wel_ex_qty_alt").value="";
	document.getElementById("ntxt_wel_priority_alt").value="";
	enable_object("txt_wel_fg_no_alt|txt_wel_assm_no_alt|txt_wel_part_no_alt|txt_wel_alt_part_alt|"+
		"ntxt_wel_ex_qty_alt|ntxt_wel_priority_alt|"+
		"bbtn_wel_alt_part_alt|btn_detail_alt_part_save|btn_detail_alt_part_return",false,"");
	
	document.getElementById("txt_wel_assm_no_to").value="";
	document.getElementById("txt_wel_assm_no_from").value="";
	document.getElementById("chk_wel_alt_copy").checked=false;
	enable_object("txt_wel_assm_no_to|txt_wel_assm_no_from|bbtn_wel_assm_no_from|"+
		"chk_wel_alt_copy|btn_detail_bom_copy_save|btn_detail_bom_copy_return",false,"");

	format_number_el("ntxt_wel_scp_fact");
	format_number_el("ntxt_wel_qp_eng");
	format_number_el("ntxt_wel_ex_qty_alt");
	format_number_el("ntxt_wel_priority_alt");
	
	enable_object(
		"btn_detail_addnew|btn_detail_edit|btn_detail_del|btn_detail_del_all|"+
		"btn_detail_alt_part_addnew|btn_detail_alt_part_edit|btn_detail_alt_part_del|"+
		"btn_detail_bom_copy",false,"");
}

function return_to_bom_tree_tab(){
	clear_and_disable_other_tab();
	enable_object(
		"btn_detail_addnew|btn_detail_edit|btn_detail_del|btn_detail_del_all|"+
		"btn_detail_alt_part_addnew|btn_detail_alt_part_edit|btn_detail_alt_part_del|"+
		"btn_detail_bom_copy",
		true,
		access_addnew+"|"+access_edit+"|"+access_delete+"|"+access_delete+"|"+
		access_addnew+"|"+access_edit+"|"+access_delete+"|"+
		access_addnew);
	$('#detail_tabList>ul').tabs('select',0);
}

function btn_head_print_click(){
				var wel_report_title="Indented Level BOM Listing";
				var wel_report_name="engr003a";
				var wel_report_calc="indented_level_bom";	//计算报表数据的功能函数
				//wel_report_id，wel_assm_no_list是indented_level_bom计算报表数据所使用的条件
				var wel_report_id=""+Math.random()+"";
				var wel_assm_no_list=trim(document.getElementById("txt_wel_fg_no").value);
				var wel_report_conditions="rpttemp01.wel_report_id='"+wel_report_id+"'";
				
				show_report(
					new Array("wel_report_name",
						"wel_report_title",
						"wel_report_id",
						"wel_report_calc",
						"wel_assm_no_list",
						"wel_report_conditions"),
					new Array(wel_report_name,
						wel_report_title,
						wel_report_id,
						wel_report_calc,
						wel_assm_no_list,
						wel_report_conditions));
}$(document).ready(function(){bind_event("btn_head_print","click",btn_head_print_click);});

function bbtn_wel_fg_no_load_click(){
	if (document.getElementById("txt_wel_fg_no").value.trim()==""){return;}
	var url=get_url_parameter(action_page,"bbtn_wel_fg_no_load_click",object_id_list);
	var handler=new net.content_loader(url,return_handler_info);
}$(document).ready(function(){bind_event("bbtn_wel_fg_no_load","click",bbtn_wel_fg_no_load_click);});

function btn_head_approve_click(){
	if (document.getElementById("txt_wel_fg_no").value.trim()==""){return;}
	var url=get_url_parameter(action_page,"btn_head_approve_click",object_id_list);
	var handler=new net.content_loader(url,return_handler_info);
}$(document).ready(function(){bind_event("btn_head_approve","click",btn_head_approve_click);});

function btn_head_not_approve_click(){
	if (document.getElementById("txt_wel_fg_no").value.trim()==""){return;}
	var url=get_url_parameter(action_page,"btn_head_not_approve_click",object_id_list);
	var handler=new net.content_loader(url,return_handler_info);
}$(document).ready(function(){bind_event("btn_head_not_approve","click",btn_head_not_approve_click);});

function btn_head_next_click(){
	txt_wel_fg_no="";
	clear_screen_layout(object_id_list);
	enable_object(object_id_list,true,"");
	enable_object(dim_object_id_list,false,"");
	enable_object(security_button,false,"");
	clear_and_disable_other_tab();
	$('#detail_tabList>ul').tabs('select',0);
	
	/*
	document.getElementById("bom_sql").src="./engm/bom_tree/bom.php"+
		"?name=bom_grid&txt_wel_fg_no=" + txt_wel_fg_no;
	*/
	document.getElementById("bom_sql").src=WERP_BASE_URI+WERP_EXPA_PARA+
		"&main_page=bom&action_page=bom&main_page_dir=engm&action_page_dir=engm"+
		"&name=bom_grid&txt_wel_fg_no=" + document.getElementById("txt_wel_fg_no").value;
}$(document).ready(function(){bind_event("btn_head_next","click",btn_head_next_click);});

function btn_detail_addnew_click(){
	var operation_node=document.getElementById("bom_sql").contentWindow.test_operation_node();
	if (!operation_node){
		window.alert(extract_message("select_part_no_please"));
		return false;
	}
	if (operation_node.wel_had_alt=="1"){	//有代用物料
		window.alert(extract_message("alternate_part_exist").replace("s1",operation_node.wel_part_des));
		return false;
	}
	if (operation_node.wel_alt_part!=""){	//该节点是代用物料
		window.alert(extract_message("part_is_alternate_part").replace("s1",operation_node.wel_part_des));
		return false;
	}
	
	clear_and_disable_other_tab();
	enable_object("txt_wel_part_no|bbtn_wel_part_no|txt_wel_eng_unit|bbtn_wel_eng_unit|"+
		"ntxt_wel_qp_eng|ntxt_wel_scp_fact|chk_wel_featu_yn|rmk_wel_bom_rmk|rmk_wel_bom_loc|"+
		"btn_detail_save|btn_detail_return",true,"");
	
	document.getElementById("txt_wel_assm_no").value=operation_node.wel_part_no;
	document.getElementById("txt_wel_assm_des").value=operation_node.wel_part_des;
	$('#detail_tabList>ul').tabs('select',1);
	external_opt_action="btn_detail_addnew_click";
}$(document).ready(function(){bind_event("btn_detail_addnew","click",btn_detail_addnew_click);});

function btn_detail_edit_click(){
	var operation_node=document.getElementById("bom_sql").contentWindow.test_operation_node();
	if (!operation_node){
		window.alert(extract_message("select_part_no_please"));
		return false;
	}
	if (operation_node.wel_fg_no==operation_node.wel_assm_no && 
		operation_node.wel_fg_no==operation_node.wel_part_no){	//该节点是成品货品
		window.alert(extract_message("part_is_finish_goods").replace("s1",operation_node.wel_part_des));
		return false;
	}
	if (operation_node.wel_alt_part!=""){	//该节点是代用物料
		window.alert(extract_message("part_is_alternate_part").replace("s1",operation_node.wel_part_des));
		return false;
	}
	
	clear_and_disable_other_tab();
	document.getElementById("txt_wel_assm_no").value=operation_node.wel_assm_no;
	document.getElementById("txt_wel_part_no").value=operation_node.wel_part_no;
	var url=get_url_parameter(action_page,"detail_read",object_id_list);
	var handler=new net.content_loader(url,return_handler_info);
	external_opt_action="btn_detail_edit_click";
}$(document).ready(function(){bind_event("btn_detail_edit","click",btn_detail_edit_click);});

function btn_detail_del_click(){
	var operation_node=document.getElementById("bom_sql").contentWindow.test_operation_node();
	if (!operation_node){
		window.alert(extract_message("select_part_no_please"));
		return false;
	}
	if (operation_node.wel_fg_no==operation_node.wel_assm_no && 
		operation_node.wel_fg_no==operation_node.wel_part_no){	//该节点是成品货品
		window.alert(extract_message("part_is_finish_goods").replace("s1",operation_node.wel_part_des));
		return false;
	}
	if (operation_node.wel_alt_part!=""){	//该节点是代用物料
		window.alert(extract_message("part_is_alternate_part").replace("s1",operation_node.wel_part_des));
		return false;
	}
	
	var confirm_message=extract_message("delete_part_node");
	confirm_message=confirm_message.replace("s1",operation_node.wel_assm_no);
	confirm_message=confirm_message.replace("s2",operation_node.wel_part_no);
	if (!window.confirm(confirm_message)){return false;}

	clear_and_disable_other_tab();
	document.getElementById("txt_wel_assm_no").value=operation_node.wel_assm_no;
	document.getElementById("txt_wel_part_no").value=operation_node.wel_part_no;
	var url=get_url_parameter(action_page,"detail_del",object_id_list);
	var handler=new net.content_loader(url,return_handler_info);
}$(document).ready(function(){bind_event("btn_detail_del","click",btn_detail_del_click);});

function btn_detail_del_all_click(){
	if (!window.confirm("Delete BOM ?")){return false;}
	var url=get_url_parameter(action_page,"detail_del_all",object_id_list);
	var handler=new net.content_loader(url,return_handler_info);
}$(document).ready(function(){bind_event("btn_detail_del_all","click",btn_detail_del_all_click);});

function btn_detail_save_click(){
	if (external_opt_action=="btn_detail_addnew_click"){opt_action="detail_addnew";}
	else if (external_opt_action=="btn_detail_edit_click"){opt_action="detail_edit";}
	var url=get_url_parameter(action_page,opt_action,object_id_list);
	var handler=new net.content_loader(url,return_handler_info);
}$(document).ready(function(){bind_event("btn_detail_save","click",btn_detail_save_click);});

function btn_detail_return_click(){
	return_to_bom_tree_tab();
}$(document).ready(function(){bind_event("btn_detail_return","click",btn_detail_return_click);});

//新增代用物料
function btn_detail_alt_part_addnew_click(){
	var operation_node=document.getElementById("bom_sql").contentWindow.test_operation_node();
	if (!operation_node){
		window.alert(extract_message("select_part_no_please"));
		return false;
	}
	if (operation_node.wel_fg_no==operation_node.wel_assm_no && 
		operation_node.wel_fg_no==operation_node.wel_part_no){	//该节点是成品货品
		window.alert(extract_message("part_is_finish_goods").replace("s1",operation_node.wel_part_des));
		return false;
	}
	if (operation_node.wel_had_sub=="1"){	//有子物料，不是最终物料
		window.alert(extract_message("part_not_final_part").replace("s1",operation_node.wel_part_des));
		return false;
	}
	if (operation_node.wel_alt_part!=""){	//该节点是代用物料
		window.alert(extract_message("part_is_alternate_part").replace("s1",operation_node.wel_part_des));
		return false;
	}
	
	clear_and_disable_other_tab();
	enable_object("txt_wel_alt_part_alt|ntxt_wel_ex_qty_alt|ntxt_wel_priority_alt|"+
		"bbtn_wel_alt_part_alt|btn_detail_alt_part_save|btn_detail_alt_part_return",true,"");
	
	document.getElementById("txt_wel_fg_no_alt").value=operation_node.wel_fg_no;
	document.getElementById("txt_wel_assm_no_alt").value=operation_node.wel_assm_no;
	document.getElementById("txt_wel_part_no_alt").value=operation_node.wel_part_no;
	document.getElementById("ntxt_wel_ex_qty_alt").value=1;
	document.getElementById("ntxt_wel_priority_alt").value=0;
	format_number_el("ntxt_wel_ex_qty_alt");
	format_number_el("ntxt_wel_priority_alt");
	$('#detail_tabList>ul').tabs('select',2);
	external_opt_action="btn_detail_alt_part_addnew_click";
}$(document).ready(function(){bind_event("btn_detail_alt_part_addnew","click",btn_detail_alt_part_addnew_click);});

function btn_detail_alt_part_edit_click(){
	var operation_node=document.getElementById("bom_sql").contentWindow.test_operation_node();
	if (!operation_node){
		window.alert(extract_message("select_part_no_please"));
		return false;
	}
	if (operation_node.wel_fg_no==operation_node.wel_assm_no && 
		operation_node.wel_fg_no==operation_node.wel_part_no){	//该节点是成品货品
		window.alert(extract_message("part_is_finish_goods").replace("s1",operation_node.wel_part_des));
		return false;
	}
	if (operation_node.wel_alt_part==""){	//该节点不是代用物料
		window.alert(extract_message("part_not_alternate_part").replace("s1",operation_node.wel_part_des));
		return false;
	}
	
	clear_and_disable_other_tab();
	document.getElementById("txt_wel_fg_no_alt").value=operation_node.wel_fg_no;
	document.getElementById("txt_wel_assm_no_alt").value=operation_node.wel_assm_no;
	document.getElementById("txt_wel_part_no_alt").value=operation_node.wel_part_no;
	document.getElementById("txt_wel_alt_part_alt").value=operation_node.wel_alt_part;
	var url=get_url_parameter(action_page,"detail_alt_part_read",object_id_list);
	var handler=new net.content_loader(url,return_handler_info);
	external_opt_action="btn_detail_alt_part_edit_click";
}$(document).ready(function(){bind_event("btn_detail_alt_part_edit","click",btn_detail_alt_part_edit_click);});

function btn_detail_alt_part_del_click(){
	var operation_node=document.getElementById("bom_sql").contentWindow.test_operation_node();
	if (!operation_node){
		window.alert(extract_message("select_part_no_please"));
		return false;
	}
	if (operation_node.wel_fg_no==operation_node.wel_assm_no && 
		operation_node.wel_fg_no==operation_node.wel_part_no){	//该节点是成品货品
		window.alert(extract_message("part_is_finish_goods").replace("s1",operation_node.wel_part_des));
		return false;
	}
	if (operation_node.wel_alt_part==""){	//该节点不是代用物料
		window.alert(extract_message("part_not_alternate_part").replace("s1",operation_node.wel_part_des));
		return false;
	}
	
	var confirm_message=extract_message("delete_alternate_part_node");
	confirm_message=confirm_message.replace("s1",operation_node.wel_alt_part);
	confirm_message=confirm_message.replace("s2",operation_node.wel_fg_no);
	confirm_message=confirm_message.replace("s3",operation_node.wel_assm_no);
	confirm_message=confirm_message.replace("s4",operation_node.wel_part_no);
	if (!window.confirm(confirm_message)){return false;}

	clear_and_disable_other_tab();
	document.getElementById("txt_wel_fg_no_alt").value=operation_node.wel_fg_no;
	document.getElementById("txt_wel_assm_no_alt").value=operation_node.wel_assm_no;
	document.getElementById("txt_wel_part_no_alt").value=operation_node.wel_part_no;
	document.getElementById("txt_wel_alt_part_alt").value=operation_node.wel_alt_part;
	var url=get_url_parameter(action_page,"detail_alt_part_del",object_id_list);
	var handler=new net.content_loader(url,return_handler_info);
}$(document).ready(function(){bind_event("btn_detail_alt_part_del","click",btn_detail_alt_part_del_click);});

function btn_detail_alt_part_save_click(){
	if (external_opt_action=="btn_detail_alt_part_addnew_click"){opt_action="detail_alt_part_addnew";
	}else if (external_opt_action=="btn_detail_alt_part_edit_click"){opt_action="detail_alt_part_edit";}
	var url=get_url_parameter(action_page,opt_action,object_id_list);
	var handler=new net.content_loader(url,return_handler_info);
}$(document).ready(function(){bind_event("btn_detail_alt_part_save","click",btn_detail_alt_part_save_click);});

function btn_detail_alt_part_return_click(){
	return_to_bom_tree_tab();
}$(document).ready(function(){bind_event("btn_detail_alt_part_return","click",btn_detail_alt_part_return_click);});

function btn_detail_bom_copy_click(){
	var operation_node=document.getElementById("bom_sql").contentWindow.test_operation_node();
	if (!operation_node){
		window.alert(extract_message("select_part_no_please"));
		return false;
	}
	if (operation_node.wel_had_sub=="1"){	//有子物料，不是最终物料
		window.alert(extract_message("part_not_final_part").replace("s1",operation_node.wel_part_des));
		return false;
	}
	if (operation_node.wel_had_alt=="1"){	//有代用物料
		window.alert(extract_message("alternate_part_exist").replace("s1",operation_node.wel_part_des));
		return false;
	}
	if (operation_node.wel_alt_part!=""){	//该节点是代用物料
		window.alert(extract_message("part_is_alternate_part").replace("s1",operation_node.wel_part_des));
		return false;
	}	
	
	clear_and_disable_other_tab();
	enable_object("txt_wel_assm_no_from|bbtn_wel_assm_no_from|"+
		"chk_wel_alt_copy|btn_detail_bom_copy_save|btn_detail_bom_copy_return",true,"");
	
	document.getElementById("txt_wel_assm_no_to").value=operation_node.wel_part_no;
	$('#detail_tabList>ul').tabs('select',3);
}$(document).ready(function(){bind_event("btn_detail_bom_copy","click",btn_detail_bom_copy_click);});

function btn_detail_bom_copy_save_click(){
	var url=get_url_parameter(action_page,"detail_bom_copy",object_id_list);
	var handler=new net.content_loader(url,return_handler_info);
}$(document).ready(function(){bind_event("btn_detail_bom_copy_save","click",btn_detail_bom_copy_save_click);});

function btn_detail_bom_copy_return_click(){
	return_to_bom_tree_tab();
}$(document).ready(function(){bind_event("btn_detail_bom_copy_return","click",btn_detail_bom_copy_return_click);});

$(document).ready(function(){
	if (external_opt_action==""){
		btn_head_next_click();
	}
});
//
--></script>
<?php
html_footer();
?>
