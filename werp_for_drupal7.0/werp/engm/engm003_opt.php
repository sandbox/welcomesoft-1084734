<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>
<?php
eval(receipt_url_parameter($_GET,$_POST));	//将参数的值放入相应以参数为名的变量中
//echo receipt_url_parameter($_GET,$_POST);
eval(read_cache_from_wel_wrcache($main_page."msg.inc"));
require_once(WERP_SITE_PATH_CLASS.$main_page."_cls.php");

//$opt_action,$msg_code,$msg_detail,$msg_script 为标准定义，用coding_str编码后以
//"|".$opt_action."|".$msg_code."|".$msg_detail."|".$msg_script  格式作为信息返回给回调函数
$opt_action=strtolower($opt_action);	//调用者的动作
$msg_code="";	//调用后产生的信息代码
$msg_detail="";	//信息代码对应的细节内容
$msg_script="";	//要执行的脚本

$cls_engm003=new engm003_cls();
$cls_engm003->wel_fg_no=$txt_wel_fg_no;
$cls_engm003->wel_assm_no=$txt_wel_assm_no;
$cls_engm003->wel_fg_no_alt=$txt_wel_fg_no_alt;
$cls_engm003->wel_assm_no_alt=$txt_wel_assm_no_alt;
$cls_engm003->wel_part_no=$txt_wel_part_no;
$cls_engm003->wel_part_no_alt=$txt_wel_part_no_alt;
$cls_engm003->wel_alt_part_alt=$txt_wel_alt_part_alt;
$cls_engm003->wel_eng_unit=$txt_wel_eng_unit;
$cls_engm003->wel_scp_fact=doubleval($ntxt_wel_scp_fact);
$cls_engm003->wel_qp_eng=doubleval($ntxt_wel_qp_eng);
$cls_engm003->wel_bom_rmk=$rmk_wel_bom_rmk;
$cls_engm003->wel_bom_loc=$rmk_wel_bom_loc;
$cls_engm003->wel_featu_yn=$chk_wel_featu_yn;
$cls_engm003->wel_ex_qty_alt=doubleval($ntxt_wel_ex_qty_alt);
$cls_engm003->wel_priority_alt=intval($ntxt_wel_priority_alt);

$cls_engm003->wel_assm_no_to=$txt_wel_assm_no_to;
$cls_engm003->wel_assm_no_from=$txt_wel_assm_no_from;
$cls_engm003->wel_alt_copy=intval($chk_wel_alt_copy);

switch ($opt_action)
{
	//读取
	//======================================================================
	case "bbtn_wel_fg_no_load_click":
		$return_val=$cls_engm003->read();
		$msg_code=$return_val["msg_code"];
		$msg_detail=extract_message($msg_code);
		if ($msg_code=="")
		{
			$msg_script=
			"clear_screen_layout(object_id_list);\n".
			"enable_object(object_id_list,false,'');\n".
			"enable_object('btn_head_next',true,'');\n".
			"enable_object('btn_head_print',true,access_print);\n".
			"document.getElementById('txt_wel_fg_no').value='".format_slashes($return_val["wel_part_no"])."';\n".
			"document.getElementById('txt_wel_fg_des').value='".format_slashes($return_val["wel_part_des"])."';\n".
			"document.getElementById('txt_wel_proj_no').value='".format_slashes($return_val["wel_proj_no"])."';\n".
			"document.getElementById('txt_wel_fg_unit').value='".format_slashes($return_val["wel_unit"])."';\n".
			"document.getElementById('dtxt_wel_bom_date').value='".format_slashes($return_val["wel_bom_date"])."';\n".
			"document.getElementById('txt_wel_bom_by').value='".format_slashes($return_val["wel_bom_by"])."';\n".
			"format_date_el('dtxt_wel_bom_date');\n".
			"format_number_el('ntxt_wel_scp_fact');\n".
			"format_number_el('ntxt_wel_qp_eng');\n".
			"format_number_el('ntxt_wel_ex_qty_alt');\n".
			"format_number_el('ntxt_wel_priority_alt');\n";
			if($return_val["wel_bom_yn"]==1){
				$msg_script .="document.getElementById('chk_wel_bom_yn').checked=true;\n".
					"enable_object('btn_detail_addnew|btn_detail_edit|btn_detail_del|btn_detail_del_all|".
					"btn_detail_alt_part_addnew|btn_detail_alt_part_edit|btn_detail_alt_part_del|".
					"btn_detail_bom_copy',false,'');".
					"enable_object('btn_head_approve',false,'');\n".
					"enable_object('btn_head_not_approve',true,access_approve);\n";
			}else{
				$msg_script .="document.getElementById('chk_wel_bom_yn').checked=false;\n".
					"enable_object('btn_detail_addnew|btn_detail_edit|btn_detail_del|btn_detail_del_all|".
					"btn_detail_alt_part_addnew|btn_detail_alt_part_edit|btn_detail_alt_part_del|".
					"btn_detail_bom_copy',true,".
					"access_addnew+'|'+access_edit+'|'+access_delete+'|'+access_delete+'|'+".
					"access_addnew+'|'+access_edit+'|'+access_delete+'|'+access_delete+'|'+access_addnew);".
					"enable_object('btn_head_approve',true,access_approve);\n".
					"enable_object('btn_head_not_approve',false,'');\n";
			}
		}
		break;
		
	//读取bom细节代用物料
	//======================================================================
	case "detail_alt_part_read":
		$return_val=$cls_engm003->detail_alt_part_read();
		$msg_code=$return_val["msg_code"];
		$msg_detail=extract_message($msg_code);
		if ($msg_code==""){
			$msg_script=
				"clear_and_disable_other_tab();\n".
				"document.getElementById('txt_wel_fg_no_alt').value='".format_slashes($return_val["wel_prod_no"])."';\n".
				"document.getElementById('txt_wel_assm_no_alt').value='".format_slashes($return_val["wel_assm_no"])."';\n".
				"document.getElementById('txt_wel_part_no_alt').value='".format_slashes($return_val["wel_part_no"])."';\n".
				"document.getElementById('txt_wel_alt_part_alt').value='".format_slashes($return_val["wel_alt_part"])."';\n".
				"document.getElementById('ntxt_wel_ex_qty_alt').value='".format_slashes($return_val["wel_ex_qty"])."';\n".
				"document.getElementById('ntxt_wel_priority_alt').value='".format_slashes($return_val["wel_priority"])."';\n".
				"format_number_el('ntxt_wel_ex_qty_alt');\n".
				"format_number_el('ntxt_wel_priority_alt');\n".
				"enable_object('ntxt_wel_ex_qty_alt|ntxt_wel_priority_alt|btn_detail_alt_part_save|btn_detail_alt_part_return',true,'');\n".
				"\$('#detail_tabList>ul').tabs('select',2);\n";
		}
		break;
		
	 //======================================================
	//detail读取数据
	case "detail_read":
		$return_val=$cls_engm003->detail_read();
		$msg_code=$return_val["msg_code"];
		$msg_detail=extract_message($msg_code);
		if ($msg_code==""){
			$msg_script=
				"clear_and_disable_other_tab();\n".
				"document.getElementById('txt_wel_assm_no').value='".format_slashes($return_val["wel_assm_no"])."';\n".
				"document.getElementById('txt_wel_assm_des').value='".format_slashes($return_val["wel_assm_des"])."';\n".
				"document.getElementById('txt_wel_part_no').value='".format_slashes($return_val["wel_part_no"])."';\n".
				"document.getElementById('txt_wel_part_des').value='".format_slashes($return_val["wel_part_des"])."';\n".
				"document.getElementById('ntxt_wel_scp_fact').value='".format_slashes($return_val["wel_scp_fact"])."';\n".
				"document.getElementById('txt_wel_eng_unit').value='".format_slashes($return_val["wel_eng_unit"])."';\n".
				"document.getElementById('txt_wel_unit').value='".format_slashes($return_val["wel_unit"])."';\n".
				"document.getElementById('ntxt_wel_qp_eng').value='".format_slashes($return_val["wel_qp_eng"])."';\n".
				"document.getElementById('rmk_wel_bom_rmk').value='".format_slashes($return_val["wel_bom_rmk"])."';\n".
				"document.getElementById('rmk_wel_bom_loc').value='".format_slashes($return_val["wel_bom_loc"])."';\n".
				"document.getElementById('chk_wel_featu_yn').checked=to_boolean('".$return_val["wel_featu_yn"]."',false);\n".
				"format_number_el('ntxt_wel_scp_fact');\n".
				"format_number_el('ntxt_wel_qp_eng');\n".
				"enable_object('txt_wel_eng_unit|bbtn_wel_eng_unit|ntxt_wel_qp_eng|chk_wel_featu_yn|'+\n".
				"'ntxt_wel_scp_fact|rmk_wel_bom_rmk|rmk_wel_bom_loc|btn_detail_save|btn_detail_return',true,'');\n".
				"\$('#detail_tabList>ul').tabs('select',1);\n";
		}
		break;
		
	//批核 
	//=========================================================================
	case "btn_head_approve_click":
		$return_val=$cls_engm003->approve();
		$msg_code=$return_val["msg_code"];
		$msg_detail=extract_message($msg_code);
		if ($msg_code=="approve_succee"){
			$msg_script.="bbtn_wel_fg_no_load_click();\n";
		}
		break;
		
	//取消批核
	//=========================================================================
	case "btn_head_not_approve_click":
		$return_val=$cls_engm003->not_approve();
		$msg_code=$return_val["msg_code"];
		$msg_detail=extract_message($msg_code);
		if ($msg_code=="not_approve_succee"){
			$msg_script.="bbtn_wel_fg_no_load_click();\n";
		}
		break;
		
	//========================================================================
	//detail 添加数据
	case "detail_addnew":
		$return_val=$cls_engm003->detail_addnew();
		$msg_code=$return_val["msg_code"];
		$msg_detail=extract_message($msg_code);
		if ($msg_code=="addnew_succee"){
			$msg_script=
				"document.getElementById('txt_wel_part_no').value='';\n".
				"document.getElementById('txt_wel_part_des').value='';\n".
				"document.getElementById('txt_wel_eng_unit').value='';\n".
				"document.getElementById('txt_wel_unit').value='';\n".
				"document.getElementById('ntxt_wel_qp_eng').value='';\n".
				"document.getElementById('ntxt_wel_scp_fact').value='';\n".
				"document.getElementById('rmk_wel_bom_rmk').value='';\n".
				"document.getElementById('rmk_wel_bom_loc').value='';\n".
				"document.getElementById('chk_wel_featu_yn').checked=false;\n".
				"format_number_el('ntxt_wel_scp_fact');\n".
				"format_number_el('ntxt_wel_qp_eng');\n".
				"document.getElementById('bom_sql').contentWindow.document.location.reload();\n";
		}
		break;
		
	//=========================================================================
	//detail编辑数据
	case "detail_edit":
		$return_val=$cls_engm003->detail_edit();
		$msg_code=$return_val["msg_code"];
		$msg_detail=extract_message($msg_code);
		if ($msg_code=="edit_succee"){
			$msg_script=
				"document.getElementById('bom_sql').contentWindow.document.location.reload();\n".
				"return_to_bom_tree_tab();\n";
		}
		break;
		
	//=======================================================================
	//bom细节删除
	case "detail_del":
		$return_val=$cls_engm003->detail_del();
		$msg_code=$return_val["msg_code"];
		$msg_detail=extract_message($msg_code);
		if($msg_code=="delete_succee"){
			$msg_script=
				"document.getElementById('bom_sql').contentWindow.document.location.reload();\n".
				"return_to_bom_tree_tab();\n";
		}
		break;
		
	//=======================================================================
	//bom细节全部删除
	case "detail_del_all":
		$return_val=$cls_engm003->detail_del_all();
		$msg_code=$return_val["msg_code"];
		$msg_detail=extract_message($msg_code);
		if ($msg_code=="delete_succee"){
			$msg_script=
				"document.getElementById('bom_sql').contentWindow.document.location.reload();\n".
				"return_to_bom_tree_tab();\n";
		}
		break;
		
	//========================================================================
	//新增代用物料
	case "detail_alt_part_addnew":
		$return_val=$cls_engm003->detail_alt_part_addnew();
		$msg_code=$return_val["msg_code"];
		$msg_detail=extract_message($msg_code);
		if ($msg_code=="addnew_succee"){
			$msg_script=
			"document.getElementById('txt_wel_alt_part_alt').value='';\n".
			"document.getElementById('ntxt_wel_ex_qty_alt').value='1';\n".
			"document.getElementById('ntxt_wel_priority_alt').value='0';\n".
			"format_number_el('ntxt_wel_ex_qty_alt');\n".
			"format_number_el('ntxt_wel_priority_alt');\n".
			"document.getElementById('bom_sql').contentWindow.document.location.reload();\n";
		}
		break;
		
	//=========================================================================
	//编辑代用物料
	case "detail_alt_part_edit":
		$return_val=$cls_engm003->detail_alt_part_edit();
		$msg_code=$return_val["msg_code"];
		$msg_detail=extract_message($msg_code);
		if ($msg_code=="edit_succee"){
			$msg_script=
			"document.getElementById('bom_sql').contentWindow.document.location.reload();\n".
			"return_to_bom_tree_tab();\n";
		}
		break;
		
	//=========================================================================
	//删除代用物料
	case "detail_alt_part_del":
		$return_val=$cls_engm003->detail_alt_part_del();
		$msg_code=$return_val["msg_code"];
		$msg_detail=extract_message($msg_code);
		if ($msg_code=="delete_succee"){
			$msg_script=
			"document.getElementById('bom_sql').contentWindow.document.location.reload();\n".
			"return_to_bom_tree_tab();\n";
		}
		break;
		
	//========================================================================
	//复制BOM
	case "detail_bom_copy":
		$return_val=$cls_engm003->detail_bom_copy();
		$msg_code=$return_val["msg_code"];
		$msg_detail=extract_message($msg_code);
		if ($msg_code=="copy_succee"){
			$msg_script=
			"document.getElementById('bom_sql').contentWindow.document.location.reload();\n".
			"return_to_bom_tree_tab();\n";
		}
		break;

}

echo "|".coding_str($opt_action)."|".coding_str($msg_code)."|".coding_str($msg_detail)."|".coding_str($msg_script);
?>
