<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>
<?php
eval(receipt_url_parameter($_GET,$_POST));	//将参数的值放入相应以参数为名的变量中
//echo receipt_url_parameter($_GET,$_POST);
eval(read_cache_from_wel_wrcache($main_page."msg.inc"));
require_once(WERP_SITE_PATH_CLASS.$main_page."_cls.php");

//$opt_action,$msg_code,$msg_detail,$msg_script 为标准定义，用coding_str编码后以
//"|".$opt_action."|".$msg_code."|".$msg_detail."|".$msg_script  格式作为信息返回给回调函数
$opt_action=strtolower($opt_action);	//调用者的动作
$msg_code="";	//调用后产生的信息代码
$msg_detail="";	//信息代码对应的细节内容
$msg_script="";	//要执行的脚本

switch ($opt_action)
{
	case "load_wel_engbomh":
		$cls_engm013a=new engm013a_cls();
		$cls_engm013a->wel_ecn_no=$txt_wel_ecn_no;
		$cls_engm013a->wel_assm_no=$txt_wel_assm_no;
		$cls_engm013a->wel_part_no=$txt_wel_part_no;
		
		$return_val=$cls_engm013a->read();
		$msg_code=$return_val["msg_code"];
		$msg_detail=extract_message($msg_code);
		if ($msg_code=="")
		{
			$msg_script="clear_screen_layout(object_id_list);\n".
				"document.getElementById('txt_wel_ecn_no').value='".format_slashes($return_val["wel_ecn_no"])."';\n".
				"document.getElementById('txt_wel_assm_no').value='".format_slashes($return_val["wel_assm_no"])."';\n".
				"document.getElementById('txt_wel_assm_des').value='".format_slashes($return_val["wel_assm_des"])."';\n".
				"document.getElementById('txt_wel_part_no').value='".format_slashes($return_val["wel_part_no"])."';\n".
				"document.getElementById('txt_wel_part_des').value='".format_slashes($return_val["wel_part_des"])."';\n".
				"document.getElementById('txt_wel_unit').value='".format_slashes($return_val["wel_unit"])."';\n".
				"document.getElementById('sel_wel_ecn_act').value='".format_slashes($return_val["wel_ecn_act"])."';\n".
				"document.getElementById('txt_wel_eng_unit').value='".format_slashes($return_val["wel_eng_unit"])."';\n".
				"document.getElementById('ntxt_wel_qp_eng').value='".format_slashes($return_val["wel_qp_eng"])."';\n".
				"document.getElementById('ntxt_wel_scp_fact').value='".format_slashes($return_val["wel_scp_fact"])."';\n".
				"document.getElementById('rmk_wel_ecn_rmk').value='".format_slashes($return_val["wel_ecn_rmk"])."';\n".
				"document.getElementById('rmk_wel_ecn_loc').value='".format_slashes($return_val["wel_ecn_loc"])."';\n".
				"document.getElementById('sel_wel_mod_sign').value='".format_slashes($return_val["wel_mod_sign"])."';\n".
				"format_number_el('ntxt_wel_qp_eng');\n".
				"format_number_el('ntxt_wel_scp_fact');\n";
		}
		break;
		
	case "addnew":
		$cls_engm013a=new engm013a_cls();
		$cls_engm013a->wel_ecn_no=$txt_wel_ecn_no;
		$cls_engm013a->wel_assm_no=$txt_wel_assm_no;
		$cls_engm013a->wel_part_no=$txt_wel_part_no;
		$cls_engm013a->wel_ecn_act=$sel_wel_ecn_act;
		$cls_engm013a->wel_eng_unit=$txt_wel_eng_unit;
		$cls_engm013a->wel_qp_eng=$ntxt_wel_qp_eng;
		$cls_engm013a->wel_scp_fact=$ntxt_wel_scp_fact;
		$cls_engm013a->wel_ecn_rmk=$rmk_wel_ecn_rmk;
		$cls_engm013a->wel_ecn_loc=$rmk_wel_ecn_loc;
		$cls_engm013a->wel_mod_sign=$sel_wel_mod_sign;
		
		$return_val=$cls_engm013a->addnew();
		$msg_code=$return_val["msg_code"];
		$msg_detail=extract_message($msg_code);
		if ($msg_code=="addnew_succee")
		{
			$msg_script=
				"document.getElementById('txt_wel_ecn_no').value='".format_slashes($txt_wel_ecn_no)."';\n".
				"wel_ecn_no='".format_slashes($txt_wel_ecn_no)."';".
				"addnew_init();\n";
		}
		break;
		
	case "edit";
		$cls_engm013a=new engm013a_cls();
		$cls_engm013a->wel_ecn_no=$txt_wel_ecn_no;
		$cls_engm013a->wel_assm_no=$txt_wel_assm_no;
		$cls_engm013a->wel_part_no=$txt_wel_part_no;
		$cls_engm013a->wel_ecn_act=$sel_wel_ecn_act;
		$cls_engm013a->wel_eng_unit=$txt_wel_eng_unit;
		$cls_engm013a->wel_qp_eng=$ntxt_wel_qp_eng;
		$cls_engm013a->wel_scp_fact=$ntxt_wel_scp_fact;
		$cls_engm013a->wel_ecn_rmk=$rmk_wel_ecn_rmk;
		$cls_engm013a->wel_ecn_loc=$rmk_wel_ecn_loc;
		$cls_engm013a->wel_mod_sign=$sel_wel_mod_sign;
		
		$return_val=$cls_engm013a->edit();
		$msg_code=$return_val["msg_code"];
		$msg_detail=extract_message($msg_code);
		if ($msg_code=="edit_succee")
		{
			$msg_script=
				"btn_return_click();\n";
		}
		break;
	
	default:
		break;
}
echo "|".coding_str($opt_action)."|".coding_str($msg_code)."|".coding_str($msg_detail)."|".coding_str($msg_script);
?>