<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>
<?php
html_heading();
eval(rebuild_layout(__FILE__));
//Draw Body Layout
?>

<script language="javascript">
//var hidden_object_id_list;	//隐藏对象的列表
//var object_id_list;	//所有对象的列表
//权限基本只有 read addnew edit delete approve print 六种
//var access_read;		//读取权限
//var access_addnew;	//新增权限
//var access_edit;		//编辑权限
//var access_delete;	//删除权限
//var access_approve;	//批核权限
//var access_print;		//打印权限
//以上js变量无需设定，已经由类库自动产生，直接使用即可

var external_opt_action="";
var action_page="";
var wel_ecn_no="";
var dim_object_id_list="";
var security_button="";

$(document).ready(function()
{
	//opt_action操作状态
	//外部要求的操作
	external_opt_action="<?php echo werp_get_request_var("opt_action"); ?>";
	action_page="<?php echo werp_pathinfo_filename(__FILE__); ?>_opt.php";
	wel_ecn_no="<?php echo werp_get_request_var("txt_wel_ecn_no"); ?>";
	//一直处于暗淡的对象列表(无法编辑的对象)
	dim_object_id_list=
		"txt_wel_appr_by|dtxt_wel_appr_date|chk_wel_appr_yn|"+
		"txt_wel_post_by|dtxt_wel_post_date|chk_wel_post_yn";
	//要用权限控制的按钮列表
	security_button="btn_head_post";
});

function return_handler_info(return_message)
{
	//window.alert(return_message);	//显示所有信息供测试时查看
	var ret_msg_arr=return_message.split("|");
	if (ret_msg_arr.length<5){window.alert(return_message);return false;}
	for (var i=0;i<ret_msg_arr.length;i++){ret_msg_arr[i]=un_coding_str(ret_msg_arr[i]);}
	var opt_action=ret_msg_arr[1];
	var msg_code=ret_msg_arr[2];
	var msg_detail=ret_msg_arr[3];
	var msg_script=ret_msg_arr[4];
	switch(opt_action)
	{
		case "btn_head_post_click":
		case "bbtn_wel_ecn_no_load_click":
			if (msg_detail!==""){window.alert(msg_detail);}
			eval(msg_script);//执行返回后产生的脚本
			break;
			
		default:
			if (msg_detail!==""){window.alert(msg_detail);}eval(msg_script);
			break;
	}
}

function bbtn_wel_ecn_no_load_click()
{
	if (document.getElementById("txt_wel_ecn_no").value.trim()==""){return;}
	var url=get_url_parameter(action_page,"bbtn_wel_ecn_no_load_click",object_id_list);
	var handler=new net.content_loader(url,return_handler_info);
}
$(document).ready(function(){bind_event("bbtn_wel_ecn_no_load","click",bbtn_wel_ecn_no_load_click);});

function btn_head_post_click()
{
	var confirm_message=extract_message("post_confirm");
	confirm_message=confirm_message.replace("s1",$("#txt_wel_ecn_no").attr("value"));
	if (!window.confirm(confirm_message)){return;}
	var url=get_url_parameter(action_page,"btn_head_post_click",object_id_list);
	var handler=new net.content_loader(url,return_handler_info);
}
$(document).ready(function(){bind_event("btn_head_post","click",btn_head_post_click);});

function btn_head_next_click()
{
	wel_ecn_no="";
	clear_screen_layout(object_id_list);
	enable_object(object_id_list,false,"");
	enable_object(dim_object_id_list,false,"");
	enable_object(security_button,false,"");
	enable_object("txt_wel_ecn_no|bbtn_wel_ecn_no_load|bbtn_wel_ecn_no",true,"");
}
$(document).ready(function(){bind_event("btn_head_next","click",btn_head_next_click);});

$(document).ready(function()
{
	if (external_opt_action=="")
	{
		btn_head_next_click();	
	}
});
</script>
<?php
	html_footer();
?>
