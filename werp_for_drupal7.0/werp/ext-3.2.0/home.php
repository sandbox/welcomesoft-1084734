<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
defined('WERP_EXEC') or die('Unauthorized access'); 
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT

$redirect_url=WERP_BASE_URI.WERP_EXPA_PARA.
	"&action_page=homeframe".
	"&lang=".WERP_EXTE_LANG;
?>
<iframe name="ifra_home" id="ifra_home" src ="<?php echo $redirect_url; ?>" 
	width="100%" height="680" frameborder="0" frameSpacing="0" marginHeight="0" marginWidth="0">
	<p>Your browser does not support iframes.</p>
</iframe>
<?php

?>
