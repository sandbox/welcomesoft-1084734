Docs.classData ={"id":"apidocs","iconCls":"icon-docs","text":"Welcome ERP","singleClickExpand":true,"children":[
		{"id":"pkg-WERP","text":"Welcome ERP","iconCls":"icon-pkg","cls":"package","singleClickExpand":true, children:[
		{"id":"pkg-WERP.engm","text":"engineering","iconCls":"icon-pkg","cls":"package","singleClickExpand":true, children:[
			{"href":"http://localhost/werp/index2.php?option=com_werp&main_page=enge003&main_page_dir=engm&action_page=enge003&action_page_dir=engm",
				"text":"BOM Enquiry",
				"id":"WERP.engm.enge003",
				"isClass":true,
				"iconCls":"icon-form",
				"cls":"cls",
				"leaf":true},
			{"href":"http://localhost/werp/index2.php?option=com_werp&main_page=engm003&main_page_dir=engm&action_page=engm003&action_page_dir=engm",
				"text":"BOM - Bill of Material",
				"id":"WERP.engm.engm003",
				"isClass":true,
				"iconCls":"icon-form",
				"cls":"cls",
				"leaf":true},
			{"href":"http://localhost/werp/index2.php?option=com_werp&main_page=engm013&main_page_dir=engm&action_page=engm013&action_page_dir=engm",
				"text":"ECN Material",
				"id":"WERP.engm.engm013",
				"isClass":true,
				"iconCls":"icon-form",
				"cls":"cls",
				"leaf":true},
			{"href":"output/Ext.chart.BarSeries.html",
				"text":"BarSeries",
				"id":"Ext.chart.BarSeries",
				"isClass":true,
				"iconCls":"icon-cls",
				"cls":"cls","leaf":true},
			{"href":"output/Ext.chart.CartesianChart.html",
				"text":"CartesianChart",
				"id":"Ext.chart.CartesianChart",
				"isClass":true,
				"iconCls":"icon-cmp",
				"cls":"cls",
				"leaf":true}
		]}
		]}
		]};
Docs.icons = {   
	"WERP.engm.engm003":"icon-form",
	"WERP.engm.enge003":"icon-form"
};
