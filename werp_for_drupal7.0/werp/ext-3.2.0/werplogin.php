<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT

html_heading();
?>
<style type="text/css">
.LabForm
{
	font-size: 9pt;
	font-family: "Arial" , "Helvetica" , "sans-serif";
}
.txtForm
{
	border-right: #333333 1px solid;
	border-top: #333333 1px solid;
	font-size: 9pt;
	border-left: #333333 1px solid;
	border-bottom: #333333 1px solid;
	font-style: normal;
	font-family: "Arial" , "Helvetica" , "sans-serif";
	background-color: #ffffff;
}
.btnSubmit
{
	background: #5599dd;
	width: 120px;
	color: #000000;
	font-family: Arial;
}
</style>

<div style="height:40px"></div>

<form name="frm_index" id="frm_index" method="POST" action="index.php" target="_top">
	<div style="display:none;">
		<input type="text" name="main_page" id="main_page" value="" />
		<input type="text" name="main_page_dir" id="main_page_dir" value="" />
		<input type="text" name="action_page" id="action_page" value="homeframe" />
		<input type="text" name="action_page_dir" id="action_page_dir" value="" />

		<input type="text" name="guest_token" id="guest_token" value="guestlogin" />
		<input type="text" name="lang" id="lang" value="<?php echo WERP_EXTE_LANG; ?>" />
	</div>

	<table height="423" cellSpacing="0" cellPadding="0" width="738" align="center" border="0" 
		style="background-image:url(<?php echo WERP_FOLL_URI; ?>images/login.gif); background-repeat:no-repeat">
	<tr>
	  <td width="450" height="250"></td>
	  <td width="308" height="250"></td>
	</tr>
	<tr>
	  <td width="450" height="173"></td>
	  <td width="308" height="173" valign="top">

		<table cellSpacing="0" cellPadding="0" width="308" align="center">
		<tr>
		  <td align="right">
			<label class="LabForm" id="labUsername" name="labUsername">User ID:&nbsp;</label>
		  </td>
		  <td>
			<input type="text" name="txt_wel_user_id" id="txt_wel_user_id" maxLength="20" size="22" class="txtForm" />
		  </td>
		</tr>
		<tr>
		  <td height="5"></td>
		  <td height="5"></td>
		</tr>
		<tr>
		  <td align="right">
			<label class="LabForm" id="labEnglish" name="labEnglish">Password:&nbsp;</label>
		  </td>
		  <td>
			<input type="password" name="txt_wel_password" id="txt_wel_password" maxLength="20" size="22" class="txtForm" />
		  </td>
		</tr>
		<tr>
		  <td height="5"></td>
		  <td height="5"></td>
		</tr>
		<tr>
		  <td colspan="2" align="center">
			<input type="submit" name="btn_login_in" id="btn_login_in" value="Login" class="btnSubmit" />
		  </td>
		</tr>
		</table>

	  </td>
	</tr>
	</table>
</form>

<table cellSpacing="0" cellPadding="0" width="600" align="center" border="0">
<tr>
  <td>
	<div name="div_werp_footer_info" name="div_werp_footer_info" style="text-align: center;height:32px;">
	<?php echo WERP_COPYRIGHT; ?>
	</div>
  </td>
</tr>
</table>

<?php
$txt_wel_user_id=werp_get_request_var("txt_wel_user_id");
$txt_wel_password=werp_get_request_var("txt_wel_password");
$btn_login_in=werp_get_request_var("btn_login_in");
$login_in_error_message="";
if ($btn_login_in!=""){
	try{
		$conn=werp_db_connect();
		
		$sql="SELECT * FROM #__wel_userflm WHERE ".
			"wel_user_id='$txt_wel_user_id' and wel_password='$txt_wel_password' limit 1";
		$sql=revert_to_the_available_sql($sql);
		if(!($result=mysql_query($sql,$conn))){	//查询sql时出错了
			throw new Exception("Invalid User ID or Password !");
		}
		if(!($row=mysql_fetch_array($result))){	//没有符合条件的记录
			throw new Exception("Invalid User ID or Password !");
		}

		if (!is_null($row["wel_disable"])){
			$wel_disable=$row["wel_disable"];
			if ($wel_disable>0){
				//帐户已经停用,请联系系统管理员
				throw new Exception("Account has been disabled, please contact the system administrator !");
			}
		}
		if (!is_null($row["wel_expire_date"])){
			$wel_expire_date=$row["wel_expire_date"];
			//strtotime('2038-01-19 11:14:07')结果正常
			//strtotime('2038-01-19 11:14:08')结果出错
			if (strtotime("$wel_expire_date")<strtotime(date("d F Y"))){
				//帐户已经过期,请联系系统管理员
				throw new Exception("Account has expired, please contact the system administrator !");
			}
		}
		$_SESSION["wel_user_id"]=$txt_wel_user_id;
		
		$lang=trim(WERP_EXTE_LANG);
		$txt_wel_language_id=strtolower($row["wel_language_id"]);
		?>
		<script language="javascript">
		$(document).ready(function(){
			//document.getElementById("frm_index").target="_top";
			//document.getElementById("action_page").value="homeframe";
			document.getElementById("lang").value="<?php echo $lang==""?$txt_wel_language_id:$lang; ?>";
			document.getElementById("frm_index").submit();
		});
		</script>
		<?php
	}catch (Exception $e){
		$login_in_error_message=$e->getMessage();
		?>
		<script language="javascript">
		$(document).ready(function(){
			document.getElementById("txt_wel_user_id").value="<?php echo $txt_wel_user_id; ?>";
			var login_in_error_message="<?php echo $login_in_error_message; ?>";
			if (login_in_error_message!=""){window.alert(login_in_error_message);}
		});
		</script>
		<?php
	}
}

?>
<?php
html_footer();
?>