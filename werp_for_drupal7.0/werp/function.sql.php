<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or LGPL Version 2.1 (LGPLv2.1-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');

function revert_to_the_available_sql($sql){
	static $wel_table_prefix=null;
	if (!$wel_table_prefix) {
		$wel_db_config=extract_wel_db_config();
		$wel_table_prefix=$wel_db_config["wel_table_prefix"];
	}
	
	return str_ireplace("#__",$wel_table_prefix,$sql);
}

function werp_db_connect(){
	static $conn=null;
	if($conn){return $conn;}
	
	$wel_db_config=extract_wel_db_config();
	$wel_db_host=$wel_db_config["wel_db_host"];
	$wel_db_user=$wel_db_config["wel_db_user"];
	$wel_db_password=$wel_db_config["wel_db_password"];
	$wel_db_name=$wel_db_config["wel_db_name"];
	
	$conn=mysql_connect($wel_db_host,$wel_db_user,$wel_db_password,true);
	if(!$conn){
		//trigger_error(mysql_error(),E_USER_ERROR);
	}
	if(!(mysql_select_db($wel_db_name,$conn))){
		//trigger_error(mysql_error(),E_USER_ERROR);
	}
	mysql_query("SET NAMES 'utf8'");
	return $conn;
}

function col_name_not_exists($tbl_name,$col_name){
	$sql = "DESCRIBE ".$tbl_name." ".$col_name;
	$sql = revert_to_the_available_sql($sql);
	
	$conn = werp_db_connect();
	if(!($result=mysql_query($sql,$conn))){	//查询sql时出错了
		return true;
	}
	if(!($row=mysql_fetch_array($result))){	//没有符合条件的记录
		return true;
	}
	return false;
}

function record_not_exists($sql){
	$sql = revert_to_the_available_sql($sql);
	
	$conn = werp_db_connect();
	if(!($result=mysql_query($sql,$conn))){	//查询sql时出错了
		return true;
	}
	if(!($row=mysql_fetch_array($result))){	//没有符合条件的记录
		return true;
	}
	return false;
}

function execute_sql($sql,$rv_all_msg=false){
	static $execute_message=array();
	$message=array();
	$original_sql=$sql;
	$sql=trim($sql);
	if ($sql==""){
		if ($rv_all_msg){
			return $execute_message;
		}else{
			return $message;
		}
	}
	
	$sql = revert_to_the_available_sql($sql);
	
	$conn = werp_db_connect();
	if(!($result=mysql_query($sql,$conn))){	//执行sql时出错了
		$message["error_sql"]=$original_sql;
		$message["error_description"]=mysql_error();
		$execute_message[]=$message;
	}

	if ($rv_all_msg){
		return $execute_message;
	}else{
		return $message;
	}
}

?>
