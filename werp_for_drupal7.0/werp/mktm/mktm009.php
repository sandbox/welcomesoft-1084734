<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>
<?php
html_heading();
eval(rebuild_layout(__FILE__));
//Draw Body Layout
?>

<script language="javascript">
//var hidden_object_id_list;	//隐藏对象的列表
//var object_id_list;	//所有对象的列表
//权限基本只有 read addnew edit delete approve print 六种
//var access_read;		//读取权限
//var access_addnew;	//新增权限
//var access_edit;		//编辑权限
//var access_delete;	//删除权限
//var access_approve;	//批核权限
//var access_print;		//打印权限
//以上js变量无需设定，已经由类库自动产生，直接使用即可

//opt_action操作状态
//外部要求的操作
var external_opt_action="";
var action_page="";
var wel_so_no="";
//一直处于暗淡的对象列表(无法编辑的对象)
var dim_object_id_list="";
//要用权限控制的按钮列表
var security_button="";

$(document).ready(function()
{
	external_opt_action="<?php echo werp_get_request_var('opt_action'); ?>";
	action_page="<?php echo werp_pathinfo_filename(__FILE__); ?>_opt.php";
	wel_so_no="<?php echo werp_get_request_var('txt_wel_so_no'); ?>";
	//一直处于暗淡的对象列表(无法编辑的对象)
	dim_object_id_list=	"txt_wel_abbre_des|txt_wel_cus_des|txt_wel_cus_des1|"+
						"txt_wel_cus_add1|txt_wel_cus_add2|txt_wel_cus_add3|txt_wel_cus_add4|"+
						"txt_wel_cus_tele|txt_wel_cus_fax|"+
						"ntxt_wel_ex_rate|txt_wel_cus_term_des|txt_wel_cus_pbase_des|txt_wel_cus_svia_des|"+
						"ntxt_wel_item_amt|ntxt_wel_dis_amt|ntxt_wel_tax_amt|ntxt_wel_order_amt|"+
						"txt_wel_dely_des|txt_wel_dely_add1|txt_wel_dely_add2|txt_wel_dely_add3|txt_wel_dely_add4|"+
						"txt_wel_dely_cont|txt_wel_dely_tel|txt_wel_dely_fax|txt_wel_dely_email|txt_wel_dely_mob|"+
						"txt_wel_crt_user|dtxt_wel_crt_date|txt_wel_upd_user|dtxt_wel_upd_date|txt_wel_appr_by|"+
						"dtxt_wel_appr_date|chk_wel_appr_yn|ntxt_wel_rev_no";//readonly对象
	//要要用权限控制的按钮列表
	security_button="btn_head_addnew|btn_head_edit|btn_head_del|btn_head_next|btn_head_save|"+
		"btn_head_cancel|btn_head_approve|btn_head_not_approve|btn_head_print|"+
		"btn_detail_tab0_addnew|btn_detail_tab0_edit|btn_detail_tab0_del|btn_detail_tab0_del_all|"+
		"btn_detail_tab1_addnew|btn_detail_tab1_edit|btn_detail_tab1_del|btn_detail_tab1_del_all";
});

$(document).ready(function()
{
	wel_sordetm_sql_grid(document.getElementById("txt_wel_so_no").value);
	wel_sormism_sql_grid(document.getElementById("txt_wel_so_no").value);
});

function return_handler_info(return_message)
{
	//window.alert(return_message);	//显示所有信息供测试时查看
	if (script_timeout(return_message)){return false};
	
	var ret_msg_arr=return_message.split("|");
	if (ret_msg_arr.length<5){window.alert(return_message);return false;}
	for (var i=0;i<ret_msg_arr.length;i++){ret_msg_arr[i]=un_coding_str(ret_msg_arr[i]);}
	var opt_action=ret_msg_arr[1];
	var msg_code=ret_msg_arr[2];
	var msg_detail=ret_msg_arr[3];
	var msg_script=ret_msg_arr[4];
	switch(opt_action)
	{
		case "lbtn_wel_so_no_load_click":
			if (msg_detail!==""){window.alert(msg_detail);}
			if (msg_code=="")
			{
				enable_object(object_id_list,false,"");
				eval(msg_script);//执行返回后产生的脚本
				if(appr)
				{
					enable_object("btn_head_next|btn_head_not_approve|btn_head_print",true,
						access_read+"|"+access_approve+"|"+access_print);
				}
				else
				{
					enable_object("btn_head_edit|btn_head_del|btn_head_next|btn_head_approve|btn_head_print|"+
						"btn_detail_tab0_addnew|btn_detail_tab0_edit|btn_detail_tab0_del|btn_detail_tab0_del_all|"+
						"btn_detail_tab1_addnew|btn_detail_tab1_edit|btn_detail_tab1_del|btn_detail_tab1_del_all",true,
						access_edit+"|"+access_delete+"|"+access_read+"|"+access_approve+"|"+access_print+"|"+
						access_addnew+"|"+access_edit+"|"+access_delete+"|"+access_delete+"|"+
						access_addnew+"|"+access_edit+"|"+access_delete+"|"+access_delete);
				}
			}
			format_number_el("ntxt_wel_ex_rate");
			format_number_el("ntxt_wel_dis_rate");
			format_number_el("ntxt_wel_rev_no");
			
			break;
			
		case "read_wel_total_amt":
		case "addnew":
		case "edit":
		case "approve":
		case "not_approve":
		case "btn_head_del_click":
		case "btn_detail_tab0_del_click":
		case "btn_detail_tab0_del_all_click":
		case "btn_detail_tab1_del_click":
		case "btn_detail_tab1_del_all_click":
			if (msg_detail!==""){window.alert(msg_detail);}
			eval(msg_script);//执行返回后产生的脚本
			break;

		default:
			if (msg_detail!==""){window.alert(msg_detail);}eval(msg_script);
			break;
	}
}

function lbtn_wel_so_no_load_click()
{
	if (document.getElementById("txt_wel_so_no").value.trim()==""){return;}
	var url=get_url_parameter(action_page,"lbtn_wel_so_no_load_click",object_id_list);
	var handler=new net.content_loader(url,return_handler_info);
}
$(document).ready(function(){bind_event("lbtn_wel_so_no_load","click",lbtn_wel_so_no_load_click);});

function read_wel_total_amt(){ //重新加载总额与税额所定义的函数
	var url=get_url_parameter(action_page,"read_wel_total_amt",object_id_list);
	var handler=new net.content_loader(url,return_handler_info);
}

function btn_head_addnew_click()
{
	wel_so_no="";
	wel_sordetm_sql_grid("");
	wel_sormism_sql_grid("");
	clear_screen_layout(object_id_list);
	enable_object(object_id_list,true,"");
	enable_object(dim_object_id_list,false,"");
	enable_object(security_button,false,"");
	enable_object("btn_head_save|btn_head_cancel",true,
				access_addnew+"|"+access_read);
	format_number_el("ntxt_wel_ex_rate");
	format_number_el("ntxt_wel_dis_rate");
	format_number_el("ntxt_wel_rev_no");
	
	set_select_selected("sel_wel_tax_type",0);
}
$(document).ready(function(){bind_event("btn_head_addnew","click",btn_head_addnew_click);});

function btn_head_edit_click()
{
	enable_object(object_id_list,true,"");
	enable_object(dim_object_id_list,false,"");
	enable_object(security_button,false,"");
	enable_object("txt_wel_so_no|lbtn_wel_so_no_load|bbtn_wel_so_no|txt_wel_pattern|bbtn_wel_pattern|"+
	"txt_wel_cus_code|bbtn_wel_cus_code|txt_wel_comp_code|bbtn_wel_comp_code",false,"");
	enable_object("btn_head_save|btn_head_cancel",true,
				access_edit+"|"+access_read);
}
$(document).ready(function(){bind_event("btn_head_edit","click",btn_head_edit_click);});

function btn_head_del_click()
{
	var confirm_message=extract_message("delete_sales_order");
	confirm_message=confirm_message.replace("s1",$("#txt_wel_so_no").attr("value").trim());
	if (!window.confirm(confirm_message)){return;}
	var url=get_url_parameter(action_page,"btn_head_del_click",object_id_list);
	var handler=new net.content_loader(url,return_handler_info);
}
$(document).ready(function(){bind_event("btn_head_del","click",btn_head_del_click);});

function btn_head_next_click()
{
	wel_so_no="";
	wel_sordetm_sql_grid("");
	wel_sormism_sql_grid("");

	clear_screen_layout(object_id_list);
	enable_object(object_id_list,false,"");
	enable_object(dim_object_id_list,false,"");
	enable_object(security_button,false,"");
	enable_object("txt_wel_so_no|lbtn_wel_so_no_load|bbtn_wel_so_no",true,"");
	enable_object("btn_head_addnew",true,access_addnew);

	format_number_el("ntxt_wel_ex_rate");
	format_number_el("ntxt_wel_dis_rate");
	format_number_el("ntxt_wel_rev_no");

	set_select_selected("sel_wel_tax_type",0);
}
$(document).ready(function(){bind_event("btn_head_next","click",btn_head_next_click);});

function btn_head_save_click()
{
	if(wel_so_no==""){opt_action="addnew";}else{opt_action="edit";}
	var url=get_url_parameter(action_page,opt_action,object_id_list);
	var handler=new net.content_loader(url,return_handler_info);
}
$(document).ready(function(){bind_event("btn_head_save","click",btn_head_save_click);});

function btn_head_cancel_click()
{
	if (wel_so_no!="")
	{
		lbtn_wel_so_no_load_click();
	}
	else
	{
		btn_head_next_click();
//		enable_object(object_id_list,true,"");
//		enable_object(dim_object_id_list,false,"");
//		enable_object(security_button,false,"");
//		enable_object("btn_head_addnew|btn_head_save|btn_head_cancel",true,
//			access_addnew+"|"+access_addnew+"|"+access_addnew);
	}
}
$(document).ready(function(){bind_event("btn_head_cancel","click",btn_head_cancel_click);});

function btn_head_approve_click()
{
	var url=get_url_parameter(action_page,"approve",object_id_list);
	var handler=new net.content_loader(url,return_handler_info);
}
$(document).ready(function(){bind_event("btn_head_approve","click",btn_head_approve_click);});

function btn_head_not_approve_click()
{
	var url=get_url_parameter(action_page,"not_approve",object_id_list);
	var handler=new net.content_loader(url,return_handler_info);
}
$(document).ready(function(){bind_event("btn_head_not_approve","click",btn_head_not_approve_click);});

function btn_head_print_click()
{
	var wel_report_title="Sales Order";
	var wel_report_name="mktr009a";
	var wel_report_conditions="sorhdrm.wel_so_no='"+document.getElementById("txt_wel_so_no").value+"'";

	show_report(
		new Array("wel_report_name",
			"wel_report_title",
			"wel_report_conditions"),
		new Array(wel_report_name,
			wel_report_title,
			wel_report_conditions));
}
$(document).ready(function(){bind_event("btn_head_print","click",btn_head_print_click);});

//start///////////////////////////////////////////////////////////////////
function btn_detail_tab0_addnew_click()
{
	var new_object_id_list=object_id_list;
	new_object_id_list=remove_object_id(new_object_id_list,"rmk_wel_alrm_remark");
	new_object_id_list=remove_object_id(new_object_id_list,"rmk_wel_remark");
	var url=get_url_parameter("mktm009a.php","btn_detail_tab0_addnew_click",new_object_id_list);
	document.location.replace(url);
}
$(document).ready(function(){bind_event("btn_detail_tab0_addnew","click",btn_detail_tab0_addnew_click);});

function btn_detail_tab0_edit_click()
{
	var rt=wel_sordetm_sql_selected_col(0);if (!rt[0]){return false;}
	var new_object_id_list=object_id_list;
	new_object_id_list=remove_object_id(new_object_id_list,"rmk_wel_alrm_remark");
	new_object_id_list=remove_object_id(new_object_id_list,"rmk_wel_remark");
	var url=get_url_parameter("mktm009a.php","btn_detail_tab0_edit_click",new_object_id_list);
	url=url+"&ntxt_wel_so_line="+url_escape(rt[1]);
	document.location.replace(url);
}
$(document).ready(function(){bind_event("btn_detail_tab0_edit","click",btn_detail_tab0_edit_click);});

function btn_detail_tab0_del_click()
{
	var rt=wel_sordetm_sql_selected_col(0);if (!rt[0]){return false;}
	var confirm_message=extract_message("delete_detail_line");
	confirm_message=confirm_message.replace("s1",rt[1]);
	if (!window.confirm(confirm_message)){return;}
	var url=get_url_parameter(action_page,"btn_detail_tab0_del_click",object_id_list);
	url=url+"&ntxt_wel_so_line="+url_escape(rt[1]);
	var handler=new net.content_loader(url,return_handler_info);
//	read_wel_total_amt();//重新加载总额与税额
}
$(document).ready(function(){bind_event("btn_detail_tab0_del","click",btn_detail_tab0_del_click);});

function btn_detail_tab0_del_all_click()
{
	var confirm_message=extract_message("delete_detail_all");
	if (!window.confirm(confirm_message)){return;}
	var url=get_url_parameter(action_page,"btn_detail_tab0_del_all_click",object_id_list);
	var handler=new net.content_loader(url,return_handler_info);
//	read_wel_total_amt();//重新加载总额与税额
}
$(document).ready(function(){bind_event("btn_detail_tab0_del_all","click",btn_detail_tab0_del_all_click);});

function btn_detail_tab1_addnew_click()
{
	var new_object_id_list=object_id_list;
	new_object_id_list=remove_object_id(new_object_id_list,"rmk_wel_alrm_remark");
	new_object_id_list=remove_object_id(new_object_id_list,"rmk_wel_remark");
	var url=get_url_parameter("mktm009b.php","btn_detail_tab1_addnew_click",new_object_id_list);
	document.location.replace(url);
}
$(document).ready(function(){bind_event("btn_detail_tab1_addnew","click",btn_detail_tab1_addnew_click);});

function btn_detail_tab1_edit_click()
{
	var rt=wel_sormism_sql_selected_col(0);if (!rt[0]){return false;}
	var new_object_id_list=object_id_list;
	new_object_id_list=remove_object_id(new_object_id_list,"rmk_wel_alrm_remark");
	new_object_id_list=remove_object_id(new_object_id_list,"rmk_wel_remark");
	var url=get_url_parameter("mktm009b.php","btn_detail_tab1_edit_click",new_object_id_list);
	url=url+"&ntxt_wel_line_no="+url_escape(rt[1]);
	document.location.replace(url);
}
$(document).ready(function(){bind_event("btn_detail_tab1_edit","click",btn_detail_tab1_edit_click);});

function btn_detail_tab1_del_click()
{
	var rt=wel_sormism_sql_selected_col(0);if (!rt[0]){return false;}
	var confirm_message=extract_message("delete_miscellaneous_line");
	confirm_message=confirm_message.replace("s1",rt[1]);
	if (!window.confirm(confirm_message)){return;}
	var url=get_url_parameter(action_page,"btn_detail_tab1_del_click",object_id_list);
	url=url+"&ntxt_wel_line_no="+url_escape(rt[1]);
	var handler=new net.content_loader(url,return_handler_info);
//	read_wel_total_amt();//重新加载总额与税额
}
$(document).ready(function(){bind_event("btn_detail_tab1_del","click",btn_detail_tab1_del_click);});

function btn_detail_tab1_del_all_click()
{
	var confirm_message=extract_message("delete_miscellaneous_all");
	if (!window.confirm(confirm_message)){return;}
	var url=get_url_parameter(action_page,"btn_detail_tab1_del_all_click",object_id_list);
	var handler=new net.content_loader(url,return_handler_info);
//	read_wel_total_amt();//重新加载总额与税额
}
$(document).ready(function(){bind_event("btn_detail_tab1_del_all","click",btn_detail_tab1_del_all_click);});

$(document).ready(function()
{
	if (external_opt_action=="")
	{
		btn_head_next_click();
	}

	if ((external_opt_action=="btn_detail_tab0_addnew_click") || 
		(external_opt_action=="btn_detail_tab0_edit_click"))
	{
		document.getElementById("txt_wel_so_no").value=wel_so_no;
		lbtn_wel_so_no_load_click();
		$("#detail_tab_list > ul").tabs({selected: 0});
	}
	if ((external_opt_action=="btn_detail_tab1_addnew_click") || 
		(external_opt_action=="btn_detail_tab1_edit_click"))
	{
		document.getElementById("txt_wel_so_no").value=wel_so_no;
		lbtn_wel_so_no_load_click();
		$("#detail_tab_list > ul").tabs({selected: 1});
	}
});

//end////////////////////////////////////////////////////////////////////
</script>
<?php
html_footer();
?>