<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>
<?php
eval(receipt_url_parameter($_GET,$_POST));	//将参数的值放入相应以参数为名的变量中
//echo receipt_url_parameter($_GET,$_POST);
eval(read_cache_from_wel_wrcache($main_page."msg.inc"));
require_once(WERP_SITE_PATH_CLASS.$main_page."_cls.php");

//$opt_action,$msg_code,$msg_detail,$msg_script 为标准定义，用coding_str编码后以
//"|".$opt_action."|".$msg_code."|".$msg_detail."|".$msg_script  格式作为信息返回给回调函数
$opt_action=strtolower($opt_action);	//调用者的动作
$msg_code="";	//调用后产生的信息代码
$msg_detail="";	//信息代码对应的细节内容
$msg_script="";	//要执行的脚本

$cls_pmcm008=new pmcm008_cls();
$cls_pmcm008->wel_mr_no=$txt_wel_mr_no;
$cls_pmcm008->wel_pattern=$txt_wel_pattern;
$cls_pmcm008->wel_ctr_code=$txt_wel_ctr_code;
$cls_pmcm008->wel_mr_line=intval($ntxt_wel_mr_line);
$cls_pmcm008->wel_req_date=$dtxt_wel_req_date;
$cls_pmcm008->wel_remark=$rmk_wel_remark;

switch ($opt_action)
{
	//=================================================================================
	//读取制造单
	case "lbtn_wel_mr_no_load_click":
		$return_val=$cls_pmcm008->read();
		$msg_code=$return_val["msg_code"];
		$msg_detail=extract_message($msg_code);
		if ($msg_code=="")
		{
			$msg_script="clear_screen_layout(object_id_list);\n".
			"document.getElementById('txt_wel_mr_no').value='".format_slashes($return_val["wel_mr_no"])."';\n".
			"document.getElementById('txt_wel_pattern').value='".format_slashes($return_val["wel_pattern"])."';\n".
			"document.getElementById('txt_wel_ctr_code').value='".format_slashes($return_val["wel_ctr_code"])."';\n".
			"document.getElementById('dtxt_wel_req_date').value='".format_slashes($return_val["wel_req_date"])."';\n".
			"document.getElementById('rmk_wel_remark').value='".format_slashes($return_val["wel_remark"])."';\n".
			
			"wel_mrdetfm_sql_grid(document.getElementById(\"txt_wel_mr_no\").value);\n".
			"wel_mr_no=document.getElementById('txt_wel_mr_no').value;\n".
			
			"document.getElementById('txt_wel_crt_user').value='".format_slashes($return_val["wel_crt_user"])."';\n".
			"document.getElementById('dtxt_wel_crt_date').value='".format_slashes($return_val["wel_crt_date"])."';\n".
			"document.getElementById('txt_wel_upd_user').value='".format_slashes($return_val["wel_upd_user"])."';\n".
			"document.getElementById('dtxt_wel_upd_date').value='".format_slashes($return_val["wel_upd_date"])."';\n";
		}
		break;
		
	//=================================================================================
	//新增制造单
	case "addnew":
		$return_val=$cls_pmcm008->addnew();
		$msg_code=$return_val["msg_code"];
		$msg_detail=extract_message($msg_code);
		if ($msg_code=="addnew_succee")
		{
			$msg_script="wel_mr_no='".format_slashes($return_val["wel_mr_no"])."';\n".
				"document.getElementById('txt_wel_mr_no').value='".format_slashes($return_val["wel_mr_no"])."';\n".
				"lbtn_wel_mr_no_load_click();\n";
		}
		break;
		
	//=================================================================================
	//更新制造单
	case "edit":
		$return_val=$cls_pmcm008->edit();
		$msg_code=$return_val["msg_code"];
		$msg_detail=extract_message($msg_code);
		if ($msg_code=="edit_succee")
		{
			$msg_script="lbtn_wel_mr_no_load_click();\n";
		}
		break;
		
	//=================================================================================
	//删除制造单
	case "btn_head_del_click":
		$return_val=$cls_pmcm008->delete();
		$msg_code=$return_val["msg_code"];
		$msg_detail=extract_message($msg_code);
		if ($msg_code=="delete_succee")
		{
			$msg_script="btn_head_next_click();\n";
		}
		break;
		
	//=================================================================================
	//制造单明细删除
	case "btn_detail_tab0_del_click":
		$return_val=$cls_pmcm008->detail_tab0_del();
		$msg_code=$return_val["msg_code"];
		$msg_detail=extract_message($msg_code);
		if ($msg_code=="delete_succee")
		{
			$msg_script="wel_mrdetfm_sql_selected_del();\n";
		}
		break;
		
	//=================================================================================
	//制造单明细全部删除
	case "btn_detail_tab0_del_all_click":
		$return_val=$cls_pmcm008->detail_tab0_del_all();
		$msg_code=$return_val["msg_code"];
		$msg_detail=extract_message($msg_code);
		if ($msg_code=="delete_succee")
		{
			$msg_script="wel_mrdetfm_sql_grid(document.getElementById(\"txt_wel_mr_no\").value);\n";
		}
		break;
		
	default:
		break;
}
echo "|".coding_str($opt_action)."|".coding_str($msg_code)."|".coding_str($msg_detail)."|".coding_str($msg_script);
?>
