<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>
<?php
html_heading();
eval(rebuild_layout(__FILE__));
//Draw Body Layout
?>

<script language="javascript">
//var hidden_object_id_list;	//隐藏对象的列表
//var object_id_list;	//所有对象的列表
//权限基本只有 read addnew edit delete approve print 六种
//var access_read;		//读取权限
//var access_addnew;	//新增权限
//var access_edit;		//编辑权限
//var access_delete;	//删除权限
//var access_approve;	//批核权限
//var access_print;		//打印权限
//以上js变量无需设定，已经由类库自动产生，直接使用即可

//opt_action操作状态
//外部要求的操作
var external_opt_action;
var action_page;
var wel_ven_code;
//一直处于暗淡的对象列表(无法编辑的对象)
var dim_object_id_list;
//要用权限控制的按钮列表
var security_button;

$(document).ready(function()
{
	external_opt_action="<?php echo werp_get_request_var("opt_action"); ?>";
	action_page="<?php echo werp_pathinfo_filename(__FILE__); ?>_opt.php";
	wel_ven_code="<?php echo werp_get_request_var("txt_wel_ven_code"); ?>";
	dim_object_id_list="txt_wel_ven_des";
	security_button="btn_detail_tab0_addnew|btn_detail_tab0_edit|btn_detail_tab0_del|btn_detail_tab0_del_all";
});	

$(document).ready(function()
{
	wel_venparm_sql_grid(document.getElementById("txt_wel_ven_code").value);
});

function return_handler_info(return_message)
{
	//window.alert(return_message);	//显示所有信息供测试时查看
	if (script_timeout(return_message)){return false};
	var ret_msg_arr=return_message.split("|");
	if (ret_msg_arr.length<5){window.alert(return_message);return false;}
	for (var i=0;i<ret_msg_arr.length;i++){ret_msg_arr[i]=un_coding_str(ret_msg_arr[i]);}
	var opt_action=ret_msg_arr[1];
	var msg_code=ret_msg_arr[2];
	var msg_detail=ret_msg_arr[3];
	var msg_script=ret_msg_arr[4];
	switch(opt_action)
	{
		case "btn_head_del_click":
		case "btn_detail_tab0_del_click":
		case "btn_detail_tab0_del_all_click":
		case "lbtn_wel_ven_code_load_click":
			if (msg_code=="")
			{
				enable_object(object_id_list,false,"");
				enable_object("btn_head_next",true,"");
				enable_object("btn_detail_tab0_addnew|btn_detail_tab0_edit|btn_detail_tab0_del|btn_detail_tab0_del_all",true,
					access_addnew+"|"+access_edit+"|"+access_delete+"|"+access_delete);
			}
			if (msg_detail!==""){window.alert(msg_detail);}
			eval(msg_script);//执行返回后产生的脚本
			break;
			
		default:
			if (msg_detail!==""){window.alert(msg_detail);}eval(msg_script);
			break;
	}
}

function lbtn_wel_ven_code_load_click()
{
	var url=get_url_parameter(action_page,"lbtn_wel_ven_code_load_click",object_id_list);
	var handler=new net.content_loader(url,return_handler_info);
}
$(document).ready(function(){bind_event("lbtn_wel_ven_code_load","click",lbtn_wel_ven_code_load_click);});

function btn_head_next_click()
{
	wel_ven_code="";
	wel_venparm_sql_grid("");

	clear_screen_layout(object_id_list);
	enable_object(object_id_list,true,"");
	enable_object(dim_object_id_list,false,"");
	enable_object(security_button,false,"");
	enable_object("btn_head_next",false,"");
}
$(document).ready(function(){bind_event("btn_head_next","click",btn_head_next_click);});

function btn_detail_tab0_addnew_click()
{
	var new_object_id_list=object_id_list;
	var url=get_url_parameter("purm003a.php","btn_detail_tab0_addnew_click",new_object_id_list);
	document.location.replace(url);
}
$(document).ready(function(){bind_event("btn_detail_tab0_addnew","click",btn_detail_tab0_addnew_click);});

function btn_detail_tab0_edit_click()
{
	var rt=wel_venparm_sql_selected_col(0);

	if (!rt[0]){return false;}
	var new_object_id_list=remove_object_id(object_id_list,"");
	var url=get_url_parameter("purm003a.php","btn_detail_tab0_edit_click",new_object_id_list);
	url=url+"&txt_wel_part_no="+url_escape(rt[1]);
	document.location.replace(url);
}
$(document).ready(function(){bind_event("btn_detail_tab0_edit","click",btn_detail_tab0_edit_click);});

function btn_detail_tab0_del_click()
{
	var rt=wel_venparm_sql_selected_col(0);
	if (!rt[0]){return false;}
	if (!window.confirm("Delete data line ["+rt[1]+"] ?")){return;}
	var url=get_url_parameter(action_page,"btn_detail_tab0_del_click",object_id_list);
	url=url+"&txt_wel_part_no="+url_escape(rt[1]);
	var handler=new net.content_loader(url,return_handler_info);
}
$(document).ready(function(){bind_event("btn_detail_tab0_del","click",btn_detail_tab0_del_click);});

function btn_detail_tab0_del_all_click()
{
	if (!window.confirm("Delete all data line ?")){return;}
	var url=get_url_parameter(action_page,"btn_detail_tab0_del_all_click",object_id_list);
	var handler=new net.content_loader(url,return_handler_info);
}
$(document).ready(function(){bind_event("btn_detail_tab0_del_all","click",btn_detail_tab0_del_all_click);});

$(document).ready(function()
{
	if (external_opt_action=="")
	{
		btn_head_next_click();
	}
	
	if ((external_opt_action=="btn_detail_tab0_addnew_click") || (external_opt_action=="btn_detail_tab0_edit_click"))
	{
		document.getElementById("txt_wel_ven_code").value=wel_ven_code;
		lbtn_wel_ven_code_load_click();
	}
});
</script>
<?php
html_footer();
?>