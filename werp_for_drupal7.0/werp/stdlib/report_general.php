<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or LGPL Version 2.1 (LGPLv2.1-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>
<?php
eval(receipt_url_parameter($_GET,$_POST));	//将参数的值放入相应以参数为名的变量中
//echo receipt_url_parameter($_GET,$_POST);

//=================================================================
class report_datasrc_info{
	private $lay_xml_contents;
	private $parser;
	private $part_tag;
	
	private $datasrc="datasrc";
	
	private $datafld="datafld";
	private $table="table";
	private $where="where";
	private $sort="sort";
	private $group="group";
	private $limit="limit";

	private $datafld_id_arr=array();
	private $datafld_title_arr=array();
	private $datafld_title_style_arr=array();
	private $datafld_data_style_arr=array();
	private $datafld_from_arr=array();
	private $datafld_number_format_arr=array();
	
	private $table_from_arr=array();

	private $where_from_arr=array();
	
	private $sort_direction_arr=array();
	private $sort_from_arr=array();
	
	private $group_arr=array();
	
	private $limit_arr=array();
	
	function __construct($_lay_xml_contents){
		$this->lay_xml_contents=$_lay_xml_contents;
	}
	function __destruct(){
	}
	
	private function startElement($parser_instance, $element_name, $attrs){     //起始标签事件的函数
		switch ($this->part_tag){
			case $this->datasrc:
				switch (strtolower($element_name)){
					case strtolower($this->datafld):
						$id="";
						$title="";
						$from="";
						$title_style="";
						$data_style="";
						$number_format="";
						foreach ($attrs as $key=>$value){
							if (strtolower($key)=="id"){$id=$value;}
							if (strtolower($key)=="title"){$title=$value;}
							if (strtolower($key)=="from"){$from=$value;}
							if (strtolower($key)=="title_style"){$title_style=$value;}
							if (strtolower($key)=="data_style"){$data_style=$value;}
							if (strtolower($key)=="number_format"){$number_format=$value;}
						}
						if ($id=="" || $from==""){break;}
						$this->datafld_id_arr[]=$id;
						$this->datafld_title_arr[]=$title;
						$this->datafld_from_arr[]=$from;
						$this->datafld_title_style_arr[]=$title_style;
						$this->datafld_data_style_arr[]=$data_style;
						$this->datafld_number_format_arr[]=$number_format;
						break;
					case strtolower($this->table):
						$from="";
						foreach ($attrs as $key=>$value){
							if (strtolower($key)=="from"){$from=$value;}
						}
						if ($from==""){break;}
						$this->table_from_arr[]=$from;
						break;
					case strtolower($this->where):
						$from="";
						foreach ($attrs as $key=>$value){
							if (strtolower($key)=="from"){$from=$value;}
						}
						if ($from==""){break;}
						$this->where_from_arr[]=$from;
						break;
					case strtolower($this->sort):
						$direction="";
						$from="";
						foreach ($attrs as $key=>$value){
							if (strtolower($key)=="direction"){$direction=$value;}
							if (strtolower($key)=="from"){$from=$value;}
						}
						if ($direction!="asc" && $direction!="desc"){$direction="asc";}			//错误的排序标识
						if ($from==""){break;}
						$this->sort_direction_arr[]=$direction;
						$this->sort_from_arr[]=$from;
						break;
					case strtolower($this->group):
						$id="";
						$title="";
						$from="";
						$direction="";
						$sum_list="";
						$data_style="";
						foreach ($attrs as $key=>$value){
							if (strtolower($key)=="id"){$id=$value;}
							if (strtolower($key)=="title"){$title=$value;}
							if (strtolower($key)=="from"){$from=$value;}
							if (strtolower($key)=="direction"){$direction=$value;}
							if (strtolower($key)=="sum_list"){$sum_list=$value;}
							if (strtolower($key)=="data_style"){$data_style=$value;}
						}
						if ($direction!="asc" && $direction!="desc"){$direction="asc";}			//错误的排序标识
						$this->group_arr[$id]["from"]=$from;
						$this->group_arr[$id]["direction"]=$direction;
						$this->group_arr[$id]["sum_list"]=explode("|",$sum_list);
						$this->group_arr[$id]["title"]=$title;
						$this->group_arr[$id]["data_style"]=$data_style;
						break;
					case strtolower($this->limit):
						$limit="";
						foreach ($attrs as $key=>$value){
							if (strtolower($key)=="limit"){$limit=$value;}
						}
						if ($limit==""){break;}
						$limit=floatval(is_numeric($limit)?$limit:0);
						if ($limit<=0){break;}
						$this->limit_arr[]=$limit;
						break;
					default:
						break;
				}
				break;
			default:
				break;
		}

		switch (strtolower($element_name)){
			case strtolower($this->datasrc):
				$this->part_tag=$this->datasrc;
				break;
			default:
				break;
		}
	}

	private function characterData($parser_instance, $XML_data){                //读取数据时的函数 
		switch ($this->part_tag){
			case $this->datasrc:
				break;
				
			case $this->datafld:
				break;
			case $this->table:
				break;
			case $this->where:
				break;
			case $this->sort:
				break;
			case $this->group:
				break;
			case $this->limit:
				break;
			default:
				break;
		}
	}
	
	private function endElement($parser_instance, $element_name){               //结束标签事件的函数
		switch ($this->part_tag){
			case $this->datasrc:
				switch (strtolower($element_name)){
					case strtolower($this->datasrc):
						$this->part_tag='';
						break;
					default:
						break;
				}
				break;
			case $this->datafld:
				break;
			case $this->table:
				break;
			case $this->where:
				break;
			case $this->sort:
				break;
			case $this->group:
				break;
			case $this->limit:
				break;
			default:
				break;
		}
	}
	
	private function analysis_extract_xml(){
		$this->parser = xml_parser_create("UTF-8"); //创建一个parser编辑器
		//pool xml_set_object ( resource $parser , object &$object )
		//该函数使得 parser 指定的解析器可以被用在 object 对象中。
		//所有的回叫函数（callback function）
		//都可以由 xml_set_element_handler() 等函数来设置，它们被假定为 object 对象的方法。
		xml_set_object($this->parser, $this);
		//设立标签触发时的相应函数 这里分别为startElement和endElenment
		xml_set_element_handler($this->parser, "startElement", "endElement");
		//设立数据读取时的相应函数
		xml_set_character_data_handler($this->parser, "characterData");
		
		if (!xml_parse($this->parser, $this->lay_xml_contents, true)){
			//报错
			die(sprintf("XML error: %s at line %d",
			xml_error_string(xml_get_error_code($this->parser)),
			xml_get_current_line_number($this->parser)));
		}
		xml_parser_free($this->parser);//关闭和释放parser解析器	
	}
	
	public function get_report_datasrc_info(){
		$this->analysis_extract_xml();
		//整理组信息，删除无效的分组信息
		$temp_arr=$this->group_arr;
		foreach ($temp_arr as $group_id=>$group_info_arr){
			foreach ($temp_arr[$group_id]["sum_list"] as $datafld_index=>$datafld_id){
				if (!in_array($datafld_id,$this->datafld_id_arr)){
					unset($this->group_arr[$group_id]["sum_list"][$datafld_index]);	//该Sum无效
				}
			}
			if (!in_array($this->group_arr[$group_id]["from"],$this->datafld_id_arr)){
				unset($this->group_arr[$group_id]);		//该组无效
			}
		}
		
		
		$str_dataflds="";
		foreach ($this->datafld_from_arr as $key=>$value){
			$str_dataflds .=($key==0?"":",").$value." as ".$this->datafld_id_arr[$key];
		}
		
		$str_tables="";
		foreach ($this->table_from_arr as $key=>$value){
			$str_tables .=$value;
		}
		
		$str_wheres="";
		foreach ($this->where_from_arr as $key=>$value){
			if ($key==0){$str_wheres=$value;}else{$str_wheres .=" and ".$value;}
		}
		if (trim($str_wheres)==""){$str_wheres="1=1";}
		
		$temp_count=0;
		$str_groups="";
		$str_group_sorts="";
		foreach ($this->group_arr as $group_id=>$group_info_arr){
			//经过组检查，$this->group_arr 元素下标不一定从0开始
			$str_groups .=($temp_count==0?"":",").
				$this->datafld_from_arr[array_search($this->group_arr[$group_id]["from"],$this->datafld_id_arr)];
			$str_group_sorts .=($temp_count==0?"":",").
				$this->datafld_from_arr[array_search($this->group_arr[$group_id]["from"],$this->datafld_id_arr)].
				" ".$this->group_arr[$group_id]["direction"];
			$temp_count++;
		}
		
		$str_sorts="";
		foreach ($this->sort_from_arr as $key=>$value){
			$str_sorts .=($key==0?"":",").$value." ".$this->sort_direction_arr[$key];
		}
		
		$str_order_by="";
		$str_group_sorts=trim($str_group_sorts);
		$str_sorts=trim($str_sorts);
		if ($str_group_sorts!="" && $str_sorts!=""){
			$str_order_by=$str_group_sorts.",".$str_sorts;
		}else{
			$str_order_by=$str_group_sorts.$str_sorts;
		}
		
		$str_limit="";
		foreach ($this->limit_arr as $key=>$value){
			$str_limit =trim($value);
		}
		
		$sql_sentence="select ".$str_dataflds." from ".$str_tables." where ".$str_wheres;
		//if ($str_groups!=""){$sql_sentence=$sql_sentence." group by ".$str_groups;}
		if ($str_order_by!=""){$sql_sentence=$sql_sentence." order by ".$str_order_by;}
		if ($str_limit!=""){$sql_sentence=$sql_sentence." limit ".$str_limit;}
		
		$return_info=array();
		$return_info["sql_sentence"]=$sql_sentence;
		$return_info["datafld_title_style_arr"]=$this->datafld_title_style_arr;
		$return_info["datafld_data_style_arr"]=$this->datafld_data_style_arr;
		$return_info["datafld_number_format_arr"]=$this->datafld_number_format_arr;
		$return_info["datafld_title_arr"]=$this->datafld_title_arr;
		$return_info["datafld_id_arr"]=$this->datafld_id_arr;
		$return_info["group_arr"]=$this->group_arr;
		return $return_info;
	}
}
//=================================================================

//=================================================================
class report_body_info{
	private $lay_xml_contents;
	private $parser;
	private $part_tag;
	
	private $report_style="report_style";
	private $report="report";
	private $report_header="report_header";
	private $page_header="page_header";
	private $group_header="group_header";
	private $details="details";
	private $group_footer="group_footer";
	private $page_footer="page_footer";
	private $report_footer="report_footer";

	private $report_style_arr=array();
	private $report_arr=array();
	private $report_header_arr=array();
	private $page_header_arr=array();
	private $group_header_arr=array();
	private $details_arr=array();
	private $group_footer_arr=array();
	private $page_footer_arr=array();
	private $report_footer_arr=array();
	
	private $body_html="";
	private $section_attrs=array();
	
	function __construct($_lay_xml_contents){
		$this->lay_xml_contents=$_lay_xml_contents;
	}
	function __destruct(){
	}
	
	private function startElement($parser_instance, $element_name, $attrs){     //起始标签事件的函数
		switch ($this->part_tag){
			case strtolower($this->report_style):
			case strtolower($this->report):
			case strtolower($this->report_header):
			case strtolower($this->page_header):
			case strtolower($this->group_header):
			case strtolower($this->details):
			case strtolower($this->group_footer):
			case strtolower($this->page_footer):
			case strtolower($this->report_footer):
				$this->body_html .="<".strtolower($element_name);
				foreach ($attrs as $key=>$value){
					$this->body_html .=" ".strtolower($key)."=\"$value\"";
				}
				switch (strtolower($element_name)){
					case "input":
					case "br":
						$this->body_html .=" />";
						break;
					default:
						$this->body_html .=">";
						break;
				}
				break;
			default:
				break;
		}
		
		switch (strtolower($element_name)){
			case strtolower($this->report_style):
				$this->part_tag=$this->report_style;
				break;
			case strtolower($this->report):
				$this->part_tag=$this->report;
				break;
			case strtolower($this->report_header):
				$this->part_tag=$this->report_header;
				break;
			case strtolower($this->page_header):
				$this->part_tag=$this->page_header;
				break;
			case strtolower($this->group_header):
				$this->part_tag=$this->group_header;
				break;
			case strtolower($this->details):
				$this->part_tag=$this->details;
				break;
			case strtolower($this->group_footer):
				$this->part_tag=$this->group_footer;
				break;
			case strtolower($this->page_footer):
				$this->part_tag=$this->page_footer;
				break;
			case strtolower($this->report_footer):
				$this->part_tag=$this->report_footer;
				break;
			default:
				break;
		}
		
		switch (strtolower($element_name)){
			case strtolower($this->report_style):
			case strtolower($this->report):
			case strtolower($this->report_header):
			case strtolower($this->page_header):
			case strtolower($this->group_header):
			case strtolower($this->details):
			case strtolower($this->group_footer):
			case strtolower($this->page_footer):
			case strtolower($this->report_footer):
				$this->body_html="";
				$this->section_attrs=array();
				$this->section_attrs["part_tag"]=strtolower($element_name);
				foreach ($attrs as $key=>$value){
					$this->section_attrs[strtolower($key)]=$value;
				}
				$height=$this->section_attrs["height"];
				$height=floatval(is_numeric($height)?$height:0);
				if ($height<=0){
					switch (strtolower($element_name)){
						case strtolower($this->report_header):
						case strtolower($this->page_header):
						case strtolower($this->group_header):
						case strtolower($this->details):
						case strtolower($this->group_footer):
						case strtolower($this->page_footer):
						case strtolower($this->report_footer):
							$this->section_attrs=array();			//移除没有高度的部份
							break;
						default:
							break;
					}
				}
				break;
			default:
				break;
		}
	}

	private function characterData($parser_instance, $XML_data){                //读取数据时的函数 
		switch ($this->part_tag){
			case $this->report_style:
			case $this->report:
			case $this->report_header:
			case $this->page_header:
			case $this->group_header:
			case $this->details:
			case $this->group_footer:
			case $this->page_footer:
			case $this->report_footer:
				$this->body_html .=$XML_data;
				break;
			default:
				break;
		}
	}
	
	private function endElement($parser_instance, $element_name){               //结束标签事件的函数
		switch ($this->part_tag){
			case $this->report_style:
			case $this->report:
			case $this->report_header:
			case $this->page_header:
			case $this->group_header:
			case $this->details:
			case $this->group_footer:
			case $this->page_footer:
			case $this->report_footer:
				switch (strtolower($element_name)){
					case $this->report_style:
						$this->part_tag='';
						$this->section_attrs["body_html"]=$this->body_html;
						$this->report_style_arr[]=$this->section_attrs;
						break;
					case $this->report:
						$this->part_tag='';
						$this->section_attrs["body_html"]=$this->body_html;
						$this->report_arr[]=$this->section_attrs;
						break;
					case $this->report_header:
						$this->part_tag='';
						$this->section_attrs["body_html"]=$this->body_html;
						$this->report_header_arr[]=$this->section_attrs;
						break;
					case $this->page_header:
						$this->part_tag='';
						$this->section_attrs["body_html"]=$this->body_html;
						$this->page_header_arr[]=$this->section_attrs;
						break;
					case $this->group_header:
						$this->part_tag='';
						$this->section_attrs["body_html"]=$this->body_html;
						$this->group_header_arr[]=$this->section_attrs;
						break;
					case $this->details:
						$this->part_tag='';
						$this->section_attrs["body_html"]=$this->body_html;
						$this->details_arr[]=$this->section_attrs;
						break;
					case $this->group_footer:
						$this->part_tag='';
						$this->section_attrs["body_html"]=$this->body_html;
						$this->group_footer_arr[]=$this->section_attrs;
						break;
					case $this->page_footer:
						$this->part_tag='';
						$this->section_attrs["body_html"]=$this->body_html;
						$this->page_footer_arr[]=$this->section_attrs;
						break;
					case $this->report_footer:
						$this->part_tag='';
						$this->section_attrs["body_html"]=$this->body_html;
						$this->report_footer_arr[]=$this->section_attrs;
						break;
					case "input":
					case "br":
						break;
					default:
						$this->body_html .="</".strtolower($element_name).">";
						break;
				}
				break;
			default:
				break;
		}
	}
	
	private function analysis_extract_xml(){
		$this->parser = xml_parser_create("UTF-8"); //创建一个parser编辑器
		//pool xml_set_object ( resource $parser , object &$object )
		//该函数使得 parser 指定的解析器可以被用在 object 对象中。
		//所有的回叫函数（callback function）
		//都可以由 xml_set_element_handler() 等函数来设置，它们被假定为 object 对象的方法。
		xml_set_object($this->parser, $this);
		//设立标签触发时的相应函数 这里分别为startElement和endElenment
		xml_set_element_handler($this->parser, "startElement", "endElement");
		//设立数据读取时的相应函数
		xml_set_character_data_handler($this->parser, "characterData");
		
		if (!xml_parse($this->parser, $this->lay_xml_contents, true)){
			//报错
			die(sprintf("XML error: %s at line %d",
			xml_error_string(xml_get_error_code($this->parser)),
			xml_get_current_line_number($this->parser)));
		}
		xml_parser_free($this->parser);//关闭和释放parser解析器	
	}
	
	public function get_report_body_info(){
		$this->analysis_extract_xml();
		
		$return_info=array();
		$return_info["report_style_arr"]=$this->report_style_arr;
		$return_info["report_arr"]=$this->report_arr;
		$return_info["report_header_arr"]=$this->report_header_arr;
		$return_info["page_header_arr"]=$this->page_header_arr;
		$return_info["group_header_arr"]=$this->group_header_arr;
		$return_info["details_arr"]=$this->details_arr;
		$return_info["group_footer_arr"]=$this->group_footer_arr;
		$return_info["page_footer_arr"]=$this->page_footer_arr;
		$return_info["report_footer_arr"]=$this->report_footer_arr;
		return $return_info;
	}
}
//=================================================================

$cls_report_body_info=new report_body_info($lay_xml_contents);
$return_info=$cls_report_body_info->get_report_body_info();
unset($cls_report_body_info);		//销毁变量对象？不知道行不行
$report_style_arr=$return_info["report_style_arr"];
$report_arr=$return_info["report_arr"];
$report_header_arr=$return_info["report_header_arr"];
$page_header_arr=$return_info["page_header_arr"];
$group_header_arr=$return_info["group_header_arr"];
$details_arr=$return_info["details_arr"];
$group_footer_arr=$return_info["group_footer_arr"];
$page_footer_arr=$return_info["page_footer_arr"];
$report_footer_arr=$return_info["report_footer_arr"];
unset($return_info);		//销毁变量对象？不知道行不行

$cls_report_datasrc_info=new report_datasrc_info($lay_xml_contents);
$return_info=$cls_report_datasrc_info->get_report_datasrc_info();
unset($cls_report_datasrc_info);		//销毁变量对象？不知道行不行
$sql_sentence=$return_info["sql_sentence"];
$datafld_title_style_arr=$return_info["datafld_title_style_arr"];
$datafld_data_style_arr=$return_info["datafld_data_style_arr"];
$datafld_number_format_arr=$return_info["datafld_number_format_arr"];
$datafld_title_arr=$return_info["datafld_title_arr"];
$datafld_id_arr=$return_info["datafld_id_arr"];
$group_arr=$return_info["group_arr"];
$sql=revert_to_the_available_sql($sql_sentence);//echo $sql ;
unset($return_info);		//销毁变量对象？不知道行不行
//echo $sql ;
//exit;

unset($lay_xml_contents);		//销毁变量对象？不知道行不行
?>

<?php
//整理分组
$group_id_list=array();
$group_sum_list_value=array();
$group_current_value=array();
$group_next_value=array();
foreach ($group_arr as $group_id=>$group_info_arr){		//始初化
	$group_sum_list_value[$group_id]=array();
	foreach ($group_arr[$group_id]["sum_list"] as $datafld_index=>$datafld_id){
		$group_sum_list_value[$group_id][$datafld_id]=0;
	}
	$group_current_value[$group_id]="";
	$group_next_value[$group_id]="";
	$group_id_list[]=$group_id;
}


$conn=werp_db_connect();
if(!($result=mysql_query($sql,$conn))){
	trigger_error(mysql_error(),E_USER_ERROR);
}
for ($count=0;$count<mysql_num_fields($result);$count++){
	$fields[]=mysql_fetch_field($result,$count);
	$dataflds =mysql_fetch_field($result,$count);
	$fields_type[]=$dataflds->type;
}
while($row=mysql_fetch_array($result,MYSQL_ASSOC)){$instances[]=$row;}

function datafld_number_format($num,$fmt){
	//格式化数字字符串
	$number_format_arr=explode("|",$fmt);
	$decimals=trim($number_format_arr[0]."");
	$dec_point=trim($number_format_arr[1]."");if ($dec_point==""){$dec_point=".";}
	$thousands_sep=trim($number_format_arr[2]."");
	if (is_numeric($decimals)){
		$decimals=number_format($decimals);
		if ($decimals>=0){
			$num=number_format($num,$decimals,$dec_point,$thousands_sep);
		}
	}
	return $num;
}

$html_display_buffer='
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title></title>
';
foreach ($report_style_arr as $key=>$value){$html_display_buffer .=$value["body_html"];}	//报表的 CSS
$html_display_buffer .='
</head>
<body bottommargin="0" topmargin="0" leftmargin="0" rightmargin="0">
';

foreach ($report_header_arr as $key=>$value){$html_display_buffer .=$value["body_html"];}

$html_display_buffer .='<table id="tab_detail_grid" cellpadding="0" cellspacing="0" border="0">';

$html_display_buffer .='<tr>';
for ($j=0;$j<count($datafld_title_arr);$j++){
	$html_display_buffer .='<td noWrap><div style="'.$datafld_title_style_arr[$j].'">'.$datafld_title_arr[$j].'</div></td>';
}
$html_display_buffer .='</tr>';

for($data_row=0;$data_row<count($instances);$data_row++){
	$html_display_buffer .='<tr>';
	for($j=0;$j<count($datafld_id_arr);$j++){
		$filed_content=$instances[$data_row][$datafld_id_arr[$j]];
		$filed_content=trim($filed_content);
		
		$filed_content=datafld_number_format($filed_content,$datafld_number_format_arr[$j]);
		
		$filed_content=nl2br(htmlspecialchars($filed_content));
		$filed_content=$filed_content."&nbsp;";
		
		$html_display_buffer .='<td noWrap><div style="'.$datafld_data_style_arr[$j].'">'.$filed_content.'</div></td>';
	}
	$html_display_buffer .='</tr>';
	foreach ($group_arr as $group_id=>$group_info_arr){		//取得当前行键值内容
		$group_current_value[$group_id]=$instances[$data_row][$group_arr[$group_id]["from"]];
	}

	//累计分组的所有求和栏
	foreach ($group_arr as $group_id=>$group_info_arr){
		foreach ($group_arr[$group_id]["sum_list"] as $datafld_index=>$datafld_id){
			$sum_value=floatval($instances[$data_row][$datafld_id]);

			//格式化数字字符串
			$number_format_arr=explode("|",
				$datafld_number_format_arr[array_search($datafld_id,$datafld_id_arr)]);
			$decimals=trim($number_format_arr[0]."");
			if (is_numeric($decimals)){
				$decimals=number_format($decimals);
				if ($decimals>=0){$sum_value=number_format($sum_value,$decimals,".","");}
			}
			$group_sum_list_value[$group_id][$datafld_id]=
				floatval($group_sum_list_value[$group_id][$datafld_id])+floatval($sum_value);
		}
	}

	$display_group="";
	if ($data_row==count($instances)-1){	//到达了最后一行数据，将显示所有分组
		foreach ($group_arr as $group_id=>$group_info_arr){
			$display_group=$group_id;
			break;
		}
	}else{
		foreach ($group_arr as $group_id=>$group_info_arr){		//取得下一行键值内容
			$group_next_value[$group_id]=$instances[$data_row+1][$group_arr[$group_id]["from"]];
		}
		foreach ($group_arr as $group_id=>$group_info_arr){		//进行分组测试
			$current_value=strtolower($group_current_value[$group_id]);
			$next_value=strtolower($group_next_value[$group_id]);
			if ($current_value!=$next_value){
				$display_group=$group_id;
				break;
			}
		}
	}
	
	if ($display_group!=""){		//分组出现了，从最里的分组开始加入
		for($count=count($group_id_list)-1;$count>=array_search($display_group,$group_id_list);$count--){
			if (count($group_sum_list_value[$group_id_list[$count]])>0){
				$html_display_buffer .='<tr>';
				for($j=0;$j<count($datafld_id_arr);$j++){
					$sum_data_style=$group_arr[$group_id_list[$count]]["data_style"];
					$sum_list_title="";
					if ($j==0){$sum_list_title=$group_arr[$group_id_list[$count]]["title"];}
					$sum_list_value="";
					foreach ($group_sum_list_value[$group_id_list[$count]] as $datafld_id=>$sum_value){
						if (strtolower($datafld_id_arr[$j])==strtolower($datafld_id)){
							$sum_list_value=datafld_number_format($sum_value,$datafld_number_format_arr[$j]);
						}
					}
					$html_display_buffer .=
						'<td noWrap><div style="'.$sum_data_style.'">'.$sum_list_title.$sum_list_value.'&nbsp;</div></td>';
				}
				$html_display_buffer .='</tr>';
			}

			//重置该分组的所有求和栏
			foreach ($group_sum_list_value[$group_id_list[$count]] as $datafld_id=>$sum_value){
				$group_sum_list_value[$group_id_list[$count]][$datafld_id]=0;
			}
		}
	}
	
}
$html_display_buffer .='</table>';

foreach ($report_footer_arr as $key=>$value){$html_display_buffer .=$value["body_html"];}

$html_display_buffer .='</body></html>';
foreach ($report_para_d230c01e08285d10e0c26984588f7e0 as $key=>$value){
	$html_display_buffer=str_replace("{@".$key."}",stripslashes($value),$html_display_buffer);
}
echo $html_display_buffer;

//require_once(write_to_temp_file_for_analysis_test($html_display_buffer));
?>