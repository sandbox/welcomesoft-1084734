<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or LGPL Version 2.1 (LGPLv2.1-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>
<?php
eval(receipt_url_parameter($_GET,$_POST));	//将参数的值放入相应以参数为名的变量中
//echo receipt_url_parameter($_GET,$_POST);

class report_general_adjustment{
	private $lay_xml_contents;
	private $parser;
	private $part_tag;
	
	private $label_normal="label_normal";
	
	private $label_message=array();
	private $filled_label_xml="<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
	
	function __construct($_lay_xml_contents){
		$this->lay_xml_contents=$_lay_xml_contents;
	}
	function __destruct(){
	}
	
	private function startElement($parser_instance, $element_name, $attrs){     //起始标签事件的函数
		switch ($this->part_tag){
			case $this->label_normal:
				if ($attrs!=null){
					foreach ($attrs as $key=>$value){
						$this->label_message[strtolower($element_name)][strtolower($key)]=$value;
					}
				}
				break;
			default:
				break;
		}

		switch (strtolower($element_name)){
			case strtolower($this->label_normal):
				$this->part_tag=$this->label_normal;
				break;
			default:
				break;
		}
	}

	private function characterData($parser_instance, $XML_data){                //读取数据时的函数 
		switch ($this->part_tag){
			case $this->label_normal:
				break;
			default:
				break;
		}
	}

	private function endElement($parser_instance, $element_name){               //结束标签事件的函数
		switch ($this->part_tag){
			case $this->label_normal:
				switch (strtolower($element_name)){
					case strtolower($this->label_normal):
						$this->part_tag='';
						break;
					default:
						break;
				}
				break;
			default:
				break;
		}
	}

	//=======================================================================
	private function fill_label_startElement($parser_instance, $element_name, $attrs){     //起始标签事件的函数
		switch ($this->part_tag){
			case $this->label_normal:
				break;
			default:
				if (strtolower($element_name)==$this->label_normal){break;}
				
				$this->filled_label_xml .="<".strtolower($element_name);
				$label_id="";
				if ($attrs!=null){
					foreach ($attrs as $key=>$value){
						if (strtolower($key)=="id"){
							$label_id=strtolower($value);
						}
						if ((strtolower($element_name)=="datafld" || strtolower($element_name)=="group") 
							&& strtolower($key)=="title"){}else{
							$this->filled_label_xml .=" ".strtolower($key)."=\"".htmlspecialchars($value)."\"";
						}
					}
				}
				$wel_language_id=$_SESSION["wel_language_id"];
				$title=$this->label_message[$label_id][$wel_language_id];
				if ($title==""){$title=$this->label_message[$label_id]["english"];}
				
				if (strtolower($element_name)=="datafld" || strtolower($element_name)=="group"){
					$this->filled_label_xml .=" "."title=\"".htmlspecialchars($title)."\"";
				}

				if (strtolower($element_name)=="datafld" || strtolower($element_name)=="group" ||
					strtolower($element_name)=="input" || strtolower($element_name)=="br"){
					$this->filled_label_xml .=" />";
				}else{
					$this->filled_label_xml .=">".htmlspecialchars($title);
				}
				break;
		}

		switch (strtolower($element_name)){
			case strtolower($this->label_normal):
				$this->part_tag=$this->label_normal;
				break;
			default:
				break;
		}
	}

	private function fill_label_characterData($parser_instance, $XML_data){                //读取数据时的函数 
		switch ($this->part_tag){
			case $this->label_normal:
				break;
			default:
				//仍需二次解析，所以将被解析后的$XML_data用htmlspecialchars再打包回去
				$this->filled_label_xml .=htmlspecialchars($XML_data);
				break;
		}
	}

	private function fill_label_endElement($parser_instance, $element_name){               //结束标签事件的函数
		switch ($this->part_tag){
			case $this->label_normal:
				switch (strtolower($element_name)){
					case strtolower($this->label_normal):
						$this->part_tag='';
						break;
					default:
						break;
				}
				break;
			default:
				if (strtolower($element_name)=="datafld" || strtolower($element_name)=="group" ||
					strtolower($element_name)=="input" || strtolower($element_name)=="br"){
				}else{
					$this->filled_label_xml .="</".strtolower($element_name).">";
				}
				break;
		}
	}
	
	public function adjustment(){
		$this->part_tag = "";
		$this->parser = xml_parser_create("UTF-8"); //创建一个parser编辑器
		//pool xml_set_object ( resource $parser , object &$object )
		//该函数使得 parser 指定的解析器可以被用在 object 对象中。
		//所有的回叫函数（callback function）
		//都可以由 xml_set_element_handler() 等函数来设置，它们被假定为 object 对象的方法。
		xml_set_object($this->parser, $this);
		//设立标签触发时的相应函数 这里分别为startElement和endElenment
		xml_set_element_handler($this->parser, "startElement", "endElement");
		//设立数据读取时的相应函数
		xml_set_character_data_handler($this->parser, "characterData");
		
		if (!xml_parse($this->parser, $this->lay_xml_contents, true)){
			//报错
			die(sprintf("XML error: %s at line %d",
			xml_error_string(xml_get_error_code($this->parser)),
			xml_get_current_line_number($this->parser)));
		}
		xml_parser_free($this->parser);//关闭和释放parser解析器	

		//=================================================================
		$this->part_tag = "";
		$this->parser = xml_parser_create("UTF-8"); //创建一个parser编辑器
		//pool xml_set_object ( resource $parser , object &$object )
		//该函数使得 parser 指定的解析器可以被用在 object 对象中。
		//所有的回叫函数（callback function）
		//都可以由 xml_set_element_handler() 等函数来设置，它们被假定为 object 对象的方法。
		xml_set_object($this->parser, $this);
		//设立标签触发时的相应函数 这里分别为startElement和endElenment
		xml_set_element_handler($this->parser, "fill_label_startElement", "fill_label_endElement");
		//设立数据读取时的相应函数
		xml_set_character_data_handler($this->parser, "fill_label_characterData");
		
		if (!xml_parse($this->parser, $this->lay_xml_contents, true)){
			//报错
			die(sprintf("XML error: %s at line %d",
			xml_error_string(xml_get_error_code($this->parser)),
			xml_get_current_line_number($this->parser)));
		}
		xml_parser_free($this->parser);//关闭和释放parser解析器	
		
		$return_info=array();
		$return_info["filled_label_xml"]=$this->filled_label_xml;
		return $return_info;
	}
}

//=================================================================
class report_advanced_adjustment{
	private $lay_xml_contents;
	private $parser;
	private $part_tag;
	
	private $label_normal="label_normal";
	private $report_class="report_class";
	
	public $label_message=array();
	private $filled_label_xml="<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
	private $report_class_arr=array();
	
	private $XML_data="";
	
	function __construct($_lay_xml_contents){
		$this->lay_xml_contents=$_lay_xml_contents;
	}
	function __destruct(){
	}
	
	private function startElement($parser_instance, $element_name, $attrs){     //起始标签事件的函数
		switch ($this->part_tag){
			case $this->label_normal:
				if ($attrs!=null){
					foreach ($attrs as $key=>$value){
						$this->label_message[strtolower($element_name)][strtolower($key)]=$value;
					}
				}
				break;
			case $this->report_class:
				break;
			default:
				break;
		}

		switch (strtolower($element_name)){
			case strtolower($this->label_normal):
				$this->part_tag=$this->label_normal;
				break;
			case strtolower($this->report_class):
				$this->part_tag=$this->report_class;
				$this->XML_data="";
				break;
			default:
				break;
		}
	}

	private function characterData($parser_instance, $XML_data){                //读取数据时的函数 
		switch ($this->part_tag){
			case $this->label_normal:
				break;
			case $this->report_class:
				$this->XML_data .=$XML_data;
				break;
			default:
				break;
		}
	}

	private function endElement($parser_instance, $element_name){               //结束标签事件的函数
		switch ($this->part_tag){
			case $this->label_normal:
				switch (strtolower($element_name)){
					case strtolower($this->label_normal):
						$this->part_tag='';
						break;
					default:
						break;
				}
				break;
			case $this->report_class:
				switch (strtolower($element_name)){
					case strtolower($this->report_class):
						$this->part_tag='';
						$this->report_class_arr[]=$this->XML_data;
						break;
					default:
						break;
				}
				break;
			default:
				break;
		}
	}

	//=======================================================================
	private function fill_label_startElement($parser_instance, $element_name, $attrs){     //起始标签事件的函数
		switch ($this->part_tag){
			case $this->label_normal:
				break;
			default:
				if (strtolower($element_name)==$this->label_normal){break;}
				
				$this->filled_label_xml .="<".strtolower($element_name);
				$label_id="";
				if ($attrs!=null){
					foreach ($attrs as $key=>$value){
						if (strtolower($key)=="id"){$label_id=strtolower($value);}
						$this->filled_label_xml .=" ".strtolower($key)."=\"".htmlspecialchars($value)."\"";
					}
				}
				$title="";
				if ($label_id==""){
				}else{
					$wel_language_id=$_SESSION["wel_language_id"];
					$title=$this->label_message[$label_id][$wel_language_id];
					if ($title==""){$title=$this->label_message[$label_id]["english"];}
					if ($title==""){}else{$title="{label:".htmlspecialchars($title)."}";}
				}
				
				if (strtolower($element_name)=="input" || strtolower($element_name)=="br"){
					$this->filled_label_xml .=" />";
				}else{
					$this->filled_label_xml .=">".$title;
				}
				break;
		}

		switch (strtolower($element_name)){
			case strtolower($this->label_normal):
				$this->part_tag=$this->label_normal;
				break;
			default:
				break;
		}
	}

	private function fill_label_characterData($parser_instance, $XML_data){                //读取数据时的函数 
		switch ($this->part_tag){
			case $this->label_normal:
				break;
			default:
				//仍需二次解析，所以将被解析后的$XML_data用htmlspecialchars再打包回去
				$this->filled_label_xml .=htmlspecialchars($XML_data);
				break;
		}
	}

	private function fill_label_endElement($parser_instance, $element_name){               //结束标签事件的函数
		switch ($this->part_tag){
			case $this->label_normal:
				switch (strtolower($element_name)){
					case strtolower($this->label_normal):
						$this->part_tag='';
						break;
					default:
						break;
				}
				break;
			default:
				if (strtolower($element_name)=="input" || strtolower($element_name)=="br"){
				}else{
					$this->filled_label_xml .="</".strtolower($element_name).">";
				}
				break;
		}
	}
	
	public function adjustment(){
		$this->part_tag = "";
		$this->parser = xml_parser_create("UTF-8"); //创建一个parser编辑器
		//pool xml_set_object ( resource $parser , object &$object )
		//该函数使得 parser 指定的解析器可以被用在 object 对象中。
		//所有的回叫函数（callback function）
		//都可以由 xml_set_element_handler() 等函数来设置，它们被假定为 object 对象的方法。
		xml_set_object($this->parser, $this);
		//设立标签触发时的相应函数 这里分别为startElement和endElenment
		xml_set_element_handler($this->parser, "startElement", "endElement");
		//设立数据读取时的相应函数
		xml_set_character_data_handler($this->parser, "characterData");
		
		if (!xml_parse($this->parser, $this->lay_xml_contents, true)){
			//报错
			die(sprintf("XML error: %s at line %d",
			xml_error_string(xml_get_error_code($this->parser)),
			xml_get_current_line_number($this->parser)));
		}
		xml_parser_free($this->parser);//关闭和释放parser解析器	

		
		//=================================================================
		$this->part_tag = "";
		$this->parser = xml_parser_create("UTF-8"); //创建一个parser编辑器
		//pool xml_set_object ( resource $parser , object &$object )
		//该函数使得 parser 指定的解析器可以被用在 object 对象中。
		//所有的回叫函数（callback function）
		//都可以由 xml_set_element_handler() 等函数来设置，它们被假定为 object 对象的方法。
		xml_set_object($this->parser, $this);
		//设立标签触发时的相应函数 这里分别为startElement和endElenment
		xml_set_element_handler($this->parser, "fill_label_startElement", "fill_label_endElement");
		//设立数据读取时的相应函数
		xml_set_character_data_handler($this->parser, "fill_label_characterData");
		
		if (!xml_parse($this->parser, $this->lay_xml_contents, true)){
			//报错
			die(sprintf("XML error: %s at line %d",
			xml_error_string(xml_get_error_code($this->parser)),
			xml_get_current_line_number($this->parser)));
		}
		xml_parser_free($this->parser);//关闭和释放parser解析器	
		
		
		$report_temp_class="";
		foreach ($this->report_class_arr as $key=>$value){$report_temp_class .=$value;}
		$report_temp_class="
			class report_special{
				public static \$wel_report_id=null;			//报表名字
				public static \$wel_user_id=null;			//当前登入系统的用户ID
				public static \$system_settings=null;		//系统设置表#__wel_syssets记录内容
				public static \$wel_language_id=null;		//当前系统所使用的语言
				public static \$label_message=null;		//报表模板 label_normal 块的标签设置
				public static \$record_number=null;		//reserve
				public static \$dataset=null;				//reserve
				public static \$datarow=null;				//当前报表记录
				public static \$sub_record_number=null;		//reserve
				public static \$sub_dataset=null;				//reserve
				public static \$sub_datarow=null;		//当前子报表记录
			}
			class report_class{
			".$report_temp_class."
			}
			";
		
		$return_info=array();
		$return_info["filled_label_xml"]=$this->filled_label_xml;
		$return_info["label_message"]=$this->label_message;
		$return_info["report_temp_class"]=$report_temp_class;
		return $return_info;
	}
}

//=================================================================
class report_information{
	private $lay_xml_contents;
	private $parser;
	private $part_tag;
	
	private $report="report";

	private $report_type="";
	
	function __construct($_lay_xml_contents){
		$this->lay_xml_contents=$_lay_xml_contents;
	}
	function __destruct(){
	}
	
	private function startElement($parser_instance, $element_name, $attrs){     //起始标签事件的函数
		switch (strtolower($element_name)){
			case strtolower($this->report):
				foreach ($attrs as $key=>$value){
					if (strtolower($key)=="type"){
							$this->report_type=strtolower($value);
							break 2;	//直接跳出 switch 之外
					}
				}
				break;
			default:
				break;
		}
	}

	private function characterData($parser_instance, $XML_data){                //读取数据时的函数 
	}
	
	private function endElement($parser_instance, $element_name){               //结束标签事件的函数
	}
	
	private function analysis_extract_xml(){
		$this->parser = xml_parser_create("UTF-8"); //创建一个parser编辑器
		//pool xml_set_object ( resource $parser , object &$object )
		//该函数使得 parser 指定的解析器可以被用在 object 对象中。
		//所有的回叫函数（callback function）
		//都可以由 xml_set_element_handler() 等函数来设置，它们被假定为 object 对象的方法。
		xml_set_object($this->parser, $this);
		//设立标签触发时的相应函数 这里分别为startElement和endElenment
		xml_set_element_handler($this->parser, "startElement", "endElement");
		//设立数据读取时的相应函数
		xml_set_character_data_handler($this->parser, "characterData");
		
		if (!xml_parse($this->parser, $this->lay_xml_contents, true)){
			//报错
			die(sprintf("XML error: %s at line %d",
			xml_error_string(xml_get_error_code($this->parser)),
			xml_get_current_line_number($this->parser)));
		}
		xml_parser_free($this->parser);//关闭和释放parser解析器	
	}
	
	public function get_report_type(){
		$this->analysis_extract_xml();
		return $this->report_type;
	}
}
//=================================================================

/*
参数wel_report_name是必传的，并且必须以wel_report_name命名，描述使用那一个报表
*/
eval(read_cache_from_wel_wrcache($report_para_filename));

//将所有参数替换到报表模板中，并将替换后的模板保存到临时文件夹中
$wel_report_name=$report_para_d230c01e08285d10e0c26984588f7e0["wel_report_name"];
$lay_xml_file=get_lay_xml_file(WERP_SITE_PATH_REPORT,$wel_report_name);
$lay_xml_contents=file_get_contents($lay_xml_file);
foreach ($report_para_d230c01e08285d10e0c26984588f7e0 as $key=>$value){
	$value=htmlspecialchars(stripslashes($value));
	$lay_xml_contents=str_replace("{?".$key."}",$value,$lay_xml_contents);
}
//=================================================================

$cls_report_information=new report_information($lay_xml_contents);
$wel_report_type=strtolower($cls_report_information->get_report_type());
unset($cls_report_information);		//销毁变量对象？不知道行不行

switch($wel_report_type){		//测试报表类型
	case "":
	case "general":
		$cls_adjustment=new report_general_adjustment($lay_xml_contents);
		$return_info=$cls_adjustment->adjustment();
		unset($cls_adjustment);		//销毁变量对象？不知道行不行
		$lay_xml_contents=$return_info["filled_label_xml"];
		unset($return_info);		//销毁变量对象？不知道行不行

		require_once(WERP_SITE_PATH_STDLIB."report_general.php");
		break;
	case "advanced":
		$cls_adjustment=new report_advanced_adjustment($lay_xml_contents);
		$return_info=$cls_adjustment->adjustment();
		unset($cls_adjustment);		//销毁变量对象？不知道行不行
		//声明报表内部自建的 class
		eval($return_info["report_temp_class"]);
		//初始化报表内部自建的特殊 class 的变量
		report_special::$wel_report_id=$wel_report_name;
		report_special::$wel_user_id=$_SESSION["wel_user_id"];
		report_special::$system_settings=extract_system_settings_d230c01e08285d10e0c26984588f7e0();
		report_special::$wel_language_id=$_SESSION["wel_language_id"];
		report_special::$label_message=$return_info["label_message"];
		$lay_xml_contents=$return_info["filled_label_xml"];
		unset($return_info);		//销毁变量对象？不知道行不行
		
		require_once(WERP_SITE_PATH_STDLIB."report_advanced.php");
		break;
	case "chart":
		require_once(WERP_SITE_PATH_STDLIB."report_chart.php");
		break;
	default:
		break;
}

?>