<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>
<?php
eval(receipt_url_parameter($_GET,$_POST));	//将参数的值放入相应以参数为名的变量中
//echo receipt_url_parameter($_GET,$_POST);
eval(read_cache_from_wel_wrcache($main_page."msg.inc"));
require_once(WERP_SITE_PATH_CLASS.$main_page."_cls.php");

//$opt_action,$msg_code,$msg_detail,$msg_script 为标准定义，用coding_str编码后以
//"|".$opt_action."|".$msg_code."|".$msg_detail."|".$msg_script  格式作为信息返回给回调函数
$opt_action=strtolower($opt_action);	//调用者的动作
$msg_code="";	//调用后产生的信息代码
$msg_detail="";	//信息代码对应的细节内容
$msg_script="";	//要执行的脚本

$cls_strm017=new strm017_cls();
$cls_strm017->wel_ctrl_flow=$txt_wel_ctrl_flow;
$cls_strm017->wel_wh_fm=$txt_wel_wh_fm;
$cls_strm017->wel_wh_to=$txt_wel_wh_to;
$cls_strm017->wel_ctrl_rmk1=$txt_wel_ctrl_rmk1;
$cls_strm017->wel_ctrl_rmk2=$txt_wel_ctrl_rmk2;
$cls_strm017->wel_ctrl_rmk3=$txt_wel_ctrl_rmk3;
$cls_strm017->wel_rec_flow=$chk_wel_rec_flow;
$cls_strm017->wel_vrt_flow=$chk_wel_vrt_flow;
$cls_strm017->wel_int_flow=$chk_wel_int_flow;
$cls_strm017->wel_iss_flow=$chk_wel_iss_flow;
$cls_strm017->wel_rtn_flow=$chk_wel_rtn_flow;
$cls_strm017->wel_fgs_flow=$chk_wel_fgs_flow;
$cls_strm017->wel_shp_flow=$chk_wel_shp_flow;
$cls_strm017->wel_crt_flow=$chk_wel_crt_flow;
$cls_strm017->wel_avg_cflow=$chk_wel_avg_cflow;
$cls_strm017->wel_dis_able=$chk_wel_dis_able;

switch ($opt_action)
{
	//读取货仓控制
	//===================================================================================================
	case "bbtn_wel_ctrl_flow_load_click":
		$return_val=$cls_strm017->read();
		$msg_code=$return_val["msg_code"];
		$msg_detail=extract_message($msg_code);
		if ($msg_code=="")
		{
			$msg_script="clear_screen_layout(object_id_list);\n".
			"document.getElementById('txt_wel_ctrl_flow').value='".format_slashes($return_val["wel_ctrl_flow"])."';\n".
			"document.getElementById('txt_wel_wh_fm').value='".format_slashes($return_val["wel_wh_fm"])."';\n".
			"document.getElementById('txt_wel_wh_fm_des').value='".format_slashes($return_val["wel_wh_fm_des"])."';\n".
			"document.getElementById('txt_wel_wh_to').value='".format_slashes($return_val["wel_wh_to"])."';\n".
			"document.getElementById('txt_wel_wh_to_des').value='".format_slashes($return_val["wel_wh_to_des"])."';\n".
			"document.getElementById('txt_wel_ctrl_rmk1').value='".format_slashes($return_val["wel_ctrl_rmk1"])."';\n".
			"document.getElementById('txt_wel_ctrl_rmk2').value='".format_slashes($return_val["wel_ctrl_rmk2"])."';\n".
			"document.getElementById('txt_wel_ctrl_rmk3').value='".format_slashes($return_val["wel_ctrl_rmk3"])."';\n".
			"document.getElementById('chk_wel_rec_flow').checked=to_boolean('".$return_val["wel_rec_flow"]."',false);\n".
			"document.getElementById('chk_wel_vrt_flow').checked=to_boolean('".$return_val["wel_vrt_flow"]."',false);\n".
			"document.getElementById('chk_wel_int_flow').checked=to_boolean('".$return_val["wel_int_flow"]."',false);\n".
			"document.getElementById('chk_wel_iss_flow').checked=to_boolean('".$return_val["wel_iss_flow"]."',false);\n".
			"document.getElementById('chk_wel_rtn_flow').checked=to_boolean('".$return_val["wel_rtn_flow"]."',false);\n".
			"document.getElementById('chk_wel_fgs_flow').checked=to_boolean('".$return_val["wel_fgs_flow"]."',false);\n".
			"document.getElementById('chk_wel_shp_flow').checked=to_boolean('".$return_val["wel_shp_flow"]."',false);\n".
			"document.getElementById('chk_wel_crt_flow').checked=to_boolean('".$return_val["wel_crt_flow"]."',false);\n".
			"document.getElementById('chk_wel_avg_cflow').checked=to_boolean('".$return_val["wel_avg_cflow"]."',false);\n".
			"document.getElementById('chk_wel_dis_able').checked=to_boolean('".$return_val["wel_dis_able"]."',false);\n";
			"wel_ctrl_flow=document.getElementById('txt_wel_ctrl_flow').value;\n";
		}
		break;
		//=====================================================================================================
		//新增货仓控制
		case "addnew":
			$return_val=$cls_strm017->addnew();
			$msg_code=$return_val["msg_code"];
			$msg_detail=extract_message($msg_code);
			if ($msg_code=="addnew_succee")
			{
				$msg_script="wel_pattern='".format_slashes($return_val["wel_pattern"])."';".
					"document.getElementById('txt_wel_ctrl_flow').value='".format_slashes($txt_wel_ctrl_flow)."';\n".
					"document.getElementById('txt_wel_wh_fm').value='".format_slashes($txt_wel_wh_fm)."';\n".
					"document.getElementById('txt_wel_wh_to').value='".format_slashes($txt_wel_wh_to)."';\n".
					"bbtn_wel_ctrl_flow_load_click();\n";
			}
			break;
		//======================================================================================================
		//编辑货仓控制
		case "edit":
			$return_val=$cls_strm017->edit();
			$msg_code=$return_val["msg_code"];
			$msg_detail=extract_message($msg_code);
			if ($msg_code=="edit_succee")
			{
				$msg_script="bbtn_wel_ctrl_flow_load_click();\n";
			}
			break;
		//======================================================================================================
		//删除货仓控制
		case "btn_head_del_click":
			$return_val=$cls_strm017->delete();
			$msg_code=$return_val["msg_code"];
			$msg_detail=extract_message($msg_code);
			if ($msg_code=="delete_succee")
			{
				$msg_script="btn_head_next_click();\n";
			}
			break;
}

echo "|".coding_str($opt_action)."|".coding_str($msg_code)."|".coding_str($msg_detail)."|".coding_str($msg_script);
?>
