<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>
<?php
eval(receipt_url_parameter($_GET,$_POST));	//将参数的值放入相应以参数为名的变量中
//echo receipt_url_parameter($_GET,$_POST);
eval(read_cache_from_wel_wrcache($main_page."msg.inc"));
require_once(WERP_SITE_PATH_CLASS.$main_page."_cls.php");

//$opt_action,$msg_code,$msg_detail,$msg_script 为标准定义，用coding_str编码后以
//"|".$opt_action."|".$msg_code."|".$msg_detail."|".$msg_script  格式作为信息返回给回调函数
$opt_action=strtolower($opt_action);	//调用者的动作
$msg_code="";	//调用后产生的信息代码
$msg_detail="";	//信息代码对应的明细内容
$msg_script="";	//要执行的脚本

$cls_strm050=new strm050_cls();
$cls_strm050->wel_pt_no=$txt_wel_pt_no;
$cls_strm050->wel_pt_line=$ntxt_wel_pt_line;
$cls_strm050->wel_pattern=$txt_wel_pattern;
$cls_strm050->wel_req_date=$dtxt_wel_req_date;
$cls_strm050->wel_ven_code=$txt_wel_ven_code;
$cls_strm050->wel_pt_remark=$rmk_wel_pt_remark;

switch ($opt_action)
{
	//读取收货单
	//===================================================================================================
	case "bbtn_wel_pt_no_load_click":
		$return_val=$cls_strm050->read();
		$msg_code=$return_val["msg_code"];
		$msg_detail=extract_message($msg_code);
		if ($msg_code=="")
		{
			$msg_script="clear_screen_layout(object_id_list);\n".
				"wel_ptrdetm_sql_grid('".$return_val["wel_pt_no"]."');\n".
				"document.getElementById('txt_wel_pt_no').value='".format_slashes($return_val["wel_pt_no"])."';\n".
				"document.getElementById('txt_wel_pattern').value='".format_slashes($return_val["wel_pattern"])."';\n".
				"document.getElementById('dtxt_wel_req_date').value='".format_slashes($return_val["wel_req_date"])."';\n".
				"document.getElementById('rmk_wel_pt_remark').value='".format_slashes($return_val["wel_pt_remark"])."';\n".

				"document.getElementById('txt_wel_ven_code').value='".format_slashes($return_val["wel_ven_code"])."';\n".
				"document.getElementById('txt_wel_abbre_des').value='".format_slashes($return_val["wel_abbre_des"])."';\n".
				"document.getElementById('txt_wel_ven_des').value='".format_slashes($return_val["wel_ven_des"])."';\n".
				"document.getElementById('txt_wel_ven_des1').value='".format_slashes($return_val["wel_ven_des1"])."';\n".
				"document.getElementById('txt_wel_ven_add1').value='".format_slashes($return_val["wel_ven_add1"])."';\n".
				"document.getElementById('txt_wel_ven_add2').value='".format_slashes($return_val["wel_ven_add2"])."';\n".
				"document.getElementById('txt_wel_ven_add3').value='".format_slashes($return_val["wel_ven_add3"])."';\n".
				"document.getElementById('txt_wel_ven_add4').value='".format_slashes($return_val["wel_ven_add4"])."';\n".
				"document.getElementById('txt_wel_ven_cont').value='".format_slashes($return_val["wel_ven_cont"])."';\n".
				"document.getElementById('txt_wel_ven_tele').value='".format_slashes($return_val["wel_ven_tele"])."';\n".
				"document.getElementById('txt_wel_ven_tele1').value='".format_slashes($return_val["wel_ven_tele1"])."';\n".
				"document.getElementById('txt_wel_ven_fax').value='".format_slashes($return_val["wel_ven_fax"])."';\n".
				"document.getElementById('txt_wel_ven_fax1').value='".format_slashes($return_val["wel_ven_fax1"])."';\n".

				"document.getElementById('txt_wel_crt_user').value='".format_slashes($return_val["wel_crt_user"])."';\n".
				"document.getElementById('dtxt_wel_crt_date').value='".format_slashes($return_val["wel_crt_date"])."';\n".
				"document.getElementById('txt_wel_upd_user').value='".format_slashes($return_val["wel_upd_user"])."';\n".
				"document.getElementById('dtxt_wel_upd_date').value='".format_slashes($return_val["wel_upd_date"])."';\n";
		}
		break;
		
		//=====================================================================================================
		//新增收货单
		case "addnew":
			$return_val=$cls_strm050->addnew();
			$msg_code=$return_val["msg_code"];
			$msg_detail=extract_message($msg_code);
			if ($msg_code=="addnew_succee")
			{
				$msg_script="document.getElementById('txt_wel_pt_no').value='".format_slashes($return_val["wel_pt_no"])."';\n".
						"bbtn_wel_pt_no_load_click();\n";
			}
			break;
			
		//======================================================================================================
		//编辑收货单
		case "edit":
			$return_val=$cls_strm050->edit();
			$msg_code=$return_val["msg_code"];
			$msg_detail=extract_message($msg_code);
			if ($msg_code=="edit_succee")
			{
				$msg_script="bbtn_wel_pt_no_load_click();\n";
			}
			break;
			
		//======================================================================================================
		//删除收货单
		case "btn_head_del_click":
			$return_val=$cls_strm050->delete();
			$msg_code=$return_val["msg_code"];
			$msg_detail=extract_message($msg_code);
			if ($msg_code=="delete_succee")
			{
				$msg_script="btn_head_next_click();\n";
			}
			break;
			
		//======================================================================================================
		//删除收货单明细
		case "btn_detail_tab0_del_click":
			$return_val=$cls_strm050->delete_detail_tab0();
			$msg_code=$return_val["msg_code"];
			$msg_detail=extract_message($msg_code);
			if ($msg_code=="delete_succee")
			{
				$msg_script="wel_ptrdetm_sql_selected_del();\n";
			}
			break;
			
		//======================================================================================================
		//全部删除收货单明细
		case "btn_detail_tab0_del_all_click":				
			$return_val=$cls_strm050->delete_detail_tab0_all();
			$msg_code=$return_val["msg_code"];
			$msg_detail=extract_message($msg_code);
			if ($msg_code=="delete_succee")
				{
					$msg_script="wel_ptrdetm_sql_grid(document.getElementById(\"txt_wel_pt_no\").value);\n";
				}
				
			break;	
		
		default:
			break;
}

echo "|".coding_str($opt_action)."|".coding_str($msg_code)."|".coding_str($msg_detail)."|".coding_str($msg_script);
?>
