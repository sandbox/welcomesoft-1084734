<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>
<?php
html_heading();
eval(rebuild_layout(__FILE__));
//Draw Body Layout
?>

	<script language="javascript"><!--
		//var hidden_object_id_list;	//隐藏对象的列表
		//var object_id_list;	//所有对象的列表
		//权限基本只有 read addnew edit delete approve print 六种
		//var access_read;		//读取权限
		//var access_addnew;	//新增权限
		//var access_edit;		//编辑权限
		//var access_delete;	//删除权限
		//var access_approve;	//批核权限
		//var access_print;		//打印权限
		//以上js变量无需设定，已经由类库自动产生，直接使用即可

		var external_opt_action="";
		var action_page="";
		var wel_ctrl_flow="";
		var wel_dn_date="";
		var wel_wh_fm="";
		var wel_wh_to="";
		var wel_dn_no="";
		var dim_object_id_list="";
		var security_button="";
	
		$(document).ready(function()
		{
			//opt_action操作状态
			//外部要求的操作
			external_opt_action="<?php echo werp_get_request_var("opt_action"); ?>";
			action_page="<?php echo werp_pathinfo_filename(__FILE__); ?>_opt.php";
			wel_ctrl_flow="<?php echo werp_get_request_var("txt_wel_ctrl_flow"); ?>";
			wel_dn_date="<?php echo werp_get_request_var("dtxt_wel_dn_date"); ?>";
			wel_dn_no="<?php echo werp_get_request_var("txt_wel_dn_no"); ?>";
			//一直处于暗淡的对象列表(无法编辑的对象)
			dim_object_id_list="txt_wel_wh_fm|txt_wel_wh_fm_des|txt_wel_wh_to|txt_wel_wh_to_des|"+
							"txt_wel_cus_code|txt_wel_cus_des|txt_wel_comp_code";
			//要用权限控制的按钮列表
			security_button="btn_detail_tab0_edit";;
		});

		$(document).ready(function()
		{
			wel_dndetfm_sql_grid(document.getElementById("txt_wel_dn_no").value);
		});
		
		function return_handler_info(return_message)
		{
			//window.alert(return_message);	//显示所有信息供测试时查看
			var ret_msg_arr=return_message.split("|");
			if (ret_msg_arr.length<5){window.alert(return_message);return false;}
			for (var i=0;i<ret_msg_arr.length;i++){ret_msg_arr[i]=un_coding_str(ret_msg_arr[i]);}
			var opt_action=ret_msg_arr[1];
			var msg_code=ret_msg_arr[2];
			var msg_detail=ret_msg_arr[3];
			var msg_script=ret_msg_arr[4];
			switch(opt_action)
			{
				case "btn_head_read_click":
					if (msg_detail!==""){window.alert(msg_detail);}
					if (msg_code=="")
					{
						enable_object(object_id_list,false,"");
						enable_object("btn_head_next",true,"");
						enable_object("btn_detail_tab0_edit",true,access_edit);
					}
					eval(msg_script);//执行返回后产生的脚本
					break;
					
				default:
					if (msg_detail!==""){window.alert(msg_detail);}eval(msg_script);
					break;
			}
		}

		function btn_head_read_click()
		{
			var url=get_url_parameter(action_page,"btn_head_read_click",object_id_list);
			var handler=new net.content_loader(url,return_handler_info);
		}
		$(document).ready(function(){bind_event("btn_head_read","click",btn_head_read_click);});
		
		function btn_head_next_click()
		{
			clear_screen_layout(object_id_list);
			enable_object(object_id_list,true,"");
			enable_object(dim_object_id_list,false,"");
			enable_object(security_button,false,"");
			enable_object("btn_head_read",true,access_addnew);
			enable_object("btn_head_next",false,access_read);
			wel_dndetfm_sql_grid("");
		}
		$(document).ready(function(){bind_event("btn_head_next","click",btn_head_next_click);});

		function btn_detail_tab0_edit_click()
		{
			var rt=wel_dndetfm_sql_selected_col(0);if (!rt[0]){return false;}
			var url=get_url_parameter("strm229a.php","btn_detail_tab0_edit_click",object_id_list);
			url=url+"&ntxt_wel_dn_line="+url_escape(rt[1]);
			document.location.replace(url);
		}
		$(document).ready(function(){bind_event("btn_detail_tab0_edit","click",btn_detail_tab0_edit_click);});
		
		$(document).ready(function()
		{
			if (external_opt_action=="")
			{
				btn_head_next_click();	
			}
			if (external_opt_action=="btn_detail_tab0_edit_click")
			{
				document.getElementById("txt_wel_ctrl_flow").value=wel_ctrl_flow;
				document.getElementById("dtxt_wel_dn_date").value=wel_dn_date;
				document.getElementById("txt_wel_dn_no").value=wel_dn_no;
				btn_head_read_click();
				$("#detail_tab_list > ul").tabs({selected: 0});
			}
		});
	//
	--></script>
<?php
html_footer();
?>
