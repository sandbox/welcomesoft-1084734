<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>
<?php
eval(receipt_url_parameter($_GET,$_POST));	//将参数的值放入相应以参数为名的变量中
//echo receipt_url_parameter($_GET,$_POST);
eval(read_cache_from_wel_wrcache($main_page."msg.inc"));
require_once(WERP_SITE_PATH_CLASS.$main_page."_cls.php");

//$opt_action,$msg_code,$msg_detail,$msg_script 为标准定义，用coding_str编码后以
//"|".$opt_action."|".$msg_code."|".$msg_detail."|".$msg_script  格式作为信息返回给回调函数
$opt_action=strtolower($opt_action);	//调用者的动作
$msg_code="";	//调用后产生的信息代码
$msg_detail="";	//信息代码对应的细节内容
$msg_script="";	//要执行的脚本

$cls_sysm001=new sysm001_cls();
$cls_sysm001->wel_user_id=$txt_wel_user_id;
$cls_sysm001->wel_password=$txt_wel_password;
$cls_sysm001->confirm_wel_password=$txt_confirm_wel_password;
$cls_sysm001->wel_user_name=$txt_wel_user_name;
$cls_sysm001->wel_department=$txt_wel_department;
$cls_sysm001->wel_language_id=$txt_wel_language_id;
$cls_sysm001->wel_e_mail=$txt_wel_e_mail;
$cls_sysm001->wel_group_code=$txt_wel_group_code;
$cls_sysm001->wel_prog_code=$txt_wel_prog_code;
$cls_sysm001->wel_disable=$chk_wel_disable;
$cls_sysm001->wel_expire_date=$dtxt_wel_expire_date;

switch ($opt_action){
	case "lbtn_wel_user_id_load_click":
		$return_val=$cls_sysm001->read();
		$msg_code=$return_val["msg_code"];
		$msg_detail=extract_message($msg_code);
		if ($msg_code==""){
			$msg_script="clear_screen_layout(object_id_list);\n".
			"document.getElementById('txt_wel_user_id').value='".format_slashes($return_val["wel_user_id"])."';\n".
			"document.getElementById('txt_wel_user_name').value='".format_slashes($return_val["wel_user_name"])."';\n".
			"document.getElementById('txt_wel_e_mail').value='".format_slashes($return_val["wel_e_mail"])."';\n".
			"document.getElementById('txt_wel_department').value='".format_slashes($return_val["wel_department"])."';\n".
			"document.getElementById('txt_wel_language_id').value='".format_slashes($return_val["wel_language_id"])."';\n".
			"document.getElementById('dtxt_wel_expire_date').value='".$return_val["wel_expire_date"]."';\n".
			"document.getElementById('chk_wel_disable').checked=to_boolean('".$return_val["wel_disable"]."',false);\n".
			"wel_usergroup_sql_grid(document.getElementById(\"txt_wel_user_id\").value);\n".
			"wel_userprog_sql_grid(document.getElementById(\"txt_wel_user_id\").value);\n".
			"wel_user_id=document.getElementById('txt_wel_user_id').value;\n";
		}
		break;
		
	case "addnew":
		$return_val=$cls_sysm001->addnew();
		$msg_code=$return_val["msg_code"];
		$msg_detail=extract_message($msg_code);
		if ($msg_code=="addnew_succee"){
			$msg_script="wel_user_id='".format_slashes($return_val["wel_user_id"])."';\n".
				"document.getElementById('txt_wel_user_id').value='".format_slashes($return_val["wel_user_id"])."';\n".
				"lbtn_wel_user_id_load_click();\n";
		}
		break;
		
	case "edit":
		$return_val=$cls_sysm001->edit();
		$msg_code=$return_val["msg_code"];
		$msg_detail=extract_message($msg_code);
		if ($msg_code=="edit_succee"){
			$msg_script="lbtn_wel_user_id_load_click();\n";
		}
		break;

	case "btn_head_del_click":
		$return_val=$cls_sysm001->delete();
		$msg_code=$return_val["msg_code"];
		$msg_detail=extract_message($msg_code);
		if ($msg_code=="delete_succee"){
			$msg_script="btn_head_next_click();\n";
		}
		break;

	case "btn_group_del_click":
		$return_val=$cls_sysm001->delete_group();
		$msg_code=$return_val["msg_code"];
		$msg_detail=extract_message($msg_code);
		if ($msg_code=="delete_succee"){
			$msg_script="wel_usergroup_sql_selected_del();\n";
		}
		break;

	case "btn_group_del_all_click":
		$return_val=$cls_sysm001->delete_group_all();
		$msg_code=$return_val["msg_code"];
		$msg_detail=extract_message($msg_code);
		if ($msg_code=="delete_succee"){
			$msg_script="wel_usergroup_sql_grid(document.getElementById(\"txt_wel_user_id\").value);\n";
		}
		break;

	case "btn_program_del_click":
		$return_val=$cls_sysm001->delete_program();
		$msg_code=$return_val["msg_code"];
		$msg_detail=extract_message($msg_code);
		if ($msg_code=="delete_succee"){
			$msg_script="wel_userprog_sql_selected_del();\n";
		}
		break;

	case "btn_program_del_all_click":
		$return_val=$cls_sysm001->delete_program_all();
		$msg_code=$return_val["msg_code"];
		$msg_detail=extract_message($msg_code);
		if ($msg_code=="delete_succee"){
			$msg_script="wel_userprog_sql_grid(document.getElementById(\"txt_wel_user_id\").value);\n";
		}
		break;

	default:
		break;
}
echo "|".coding_str($opt_action)."|".coding_str($msg_code)."|".coding_str($msg_detail)."|".coding_str($msg_script);
?>