<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or GPL Version 2 (GPLv2-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
//CONFIG_BEGIN_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');
//CONFIG_END_DO_NOT_EDIT_OR_DELETE_THIS_LINE_CONTENT
?>
<?php
eval(receipt_url_parameter($_GET,$_POST));	//将参数的值放入相应以参数为名的变量中
//echo receipt_url_parameter($_GET,$_POST);
eval(read_cache_from_wel_wrcache($main_page."msg.inc"));
require_once(WERP_SITE_PATH_CLASS.$main_page."_cls.php");

//$opt_action,$msg_code,$msg_detail,$msg_script 为标准定义，用coding_str编码后以
//"|".$opt_action."|".$msg_code."|".$msg_detail."|".$msg_script  格式作为信息返回给回调函数
$opt_action=strtolower($opt_action);	//调用者的动作
$msg_code="";	//调用后产生的信息代码
$msg_detail="";	//信息代码对应的细节内容
$msg_script="";	//要执行的脚本

$cls_sysm009=new sysm009_cls();
$cls_sysm009->wel_root_code=$txt_wel_root_code;
$cls_sysm009->wel_parent_code=$txt_wel_parent_code;
$cls_sysm009->wel_prog_code=$txt_wel_prog_code;
$cls_sysm009->wel_working_dir=$txt_wel_working_dir;
$cls_sysm009->wel_ordering=$sel_wel_ordering;

function refresh_item_to_sel_wel_ordering_d230c01e08285d10e0c26984588f7e0($value_text){
	if (!is_array($value_text)){return "";}

	$value_script="";
	$text_script="";
	foreach ($value_text as $value=>$text){
		$value_script .="'".format_slashes($value)."',";
		$text_script .="'".format_slashes($text)."',";
	}
	$value_script=substr($value_script,0,strlen($value_script)-1);
	$text_script=substr($text_script,0,strlen($text_script)-1);
	
	return 
		"remove_select_list('sel_wel_ordering','');\n".
		"add_select_item('sel_wel_ordering',Array(".$value_script."),Array(".$text_script."));\n".
		"set_select_selected('sel_wel_ordering','".format_slashes($value)."');\n";
}

switch ($opt_action)
{
	 //========================================================================
	//detail读取数据
	case "ordering_read":
		$return_val=$cls_sysm009->ordering_read();
		$msg_code=$return_val["msg_code"];
		$msg_detail=extract_message($msg_code);
		if ($msg_code==""){
			$msg_script=
				"remove_select_list('sel_wel_ordering','');\n".
				refresh_item_to_sel_wel_ordering_d230c01e08285d10e0c26984588f7e0($return_val["value_text"])."\n";
				
		}
		break;
		
	 //========================================================================
	//detail读取数据
	case "detail_read":
		$return_val=$cls_sysm009->detail_read();
		$msg_code=$return_val["msg_code"];
		$msg_detail=extract_message($msg_code);
		if ($msg_code==""){
			$msg_script=
				"document.getElementById('txt_wel_parent_code').value='".format_slashes($return_val["wel_parent_code"])."';\n".
				"document.getElementById('txt_wel_parent_des').value='".format_slashes($return_val["wel_parent_des"])."';\n".
				"document.getElementById('txt_wel_prog_code').value='".format_slashes($return_val["wel_prog_code"])."';\n".
				"document.getElementById('txt_wel_prog_des').value='".format_slashes($return_val["wel_prog_des"])."';\n".
				"document.getElementById('txt_wel_working_dir').value='".format_slashes($return_val["wel_working_dir"])."';\n".
				"remove_select_list('sel_wel_ordering','');\n".
				refresh_item_to_sel_wel_ordering_d230c01e08285d10e0c26984588f7e0($return_val["value_text"])."\n".
				"set_select_selected('sel_wel_ordering','".$return_val["wel_ordering"]."');\n";
				
		}
		break;
		
	//========================================================================
	//detail 添加数据
	case "detail_addnew":
		$return_val=$cls_sysm009->detail_addnew();
		$msg_code=$return_val["msg_code"];
		$msg_detail=extract_message($msg_code);
		if ($msg_code=="addnew_succee"){
			$msg_script=
				"document.getElementById('txt_wel_prog_code').value='';\n".
				"document.getElementById('txt_wel_prog_des').value='';\n".
				"remove_select_list('sel_wel_ordering','');\n".
				refresh_item_to_sel_wel_ordering_d230c01e08285d10e0c26984588f7e0($return_val["value_text"])."\n".
				"document.getElementById('lof_sql').contentWindow.document.location.reload();\n";
		}
		break;
		
	//========================================================================
	//detail编辑数据
	case "detail_edit":
		$return_val=$cls_sysm009->detail_edit();
		$msg_code=$return_val["msg_code"];
		$msg_detail=extract_message($msg_code);
		if ($msg_code=="edit_succee"){
			$msg_script=
				"document.getElementById('lof_sql').contentWindow.document.location.reload();\n".
				"return_to_lof_tree_tab();\n";
		}
		break;
		
	//=======================================================================
	//lof细节删除
	case "detail_del":
		$return_val=$cls_sysm009->detail_del();
		$msg_code=$return_val["msg_code"];
		$msg_detail=extract_message($msg_code);
		if($msg_code=="delete_succee"){
			$msg_script=
				"document.getElementById('lof_sql').contentWindow.document.location.reload();\n".
				"return_to_lof_tree_tab();\n";
		}
		break;
		
}

echo "|".coding_str($opt_action)."|".coding_str($msg_code)."|".coding_str($msg_detail)."|".coding_str($msg_script);
?>
