<?php
/**
* @version		$Id$
* @package		Welcome ERP
* @author			WelcomeSoft admin@welcomesoft.org
* @copyright 	Copyright (C) 2010 Welcome Soft Limited. All rights reserved. http://welcomeerp.com
* @license		Dual licensed under the MIT (MIT-LICENSE.txt) or LGPL Version 2.1 (LGPLv2.1-LICENSE.txt) licenses.
* @url				http://welcomesoft.org
*/
?>
<?php
// no direct access
defined('WERP_EXEC') or die('Unauthorized access');


@ini_set( "max_execution_time", "120" );


//每次在 install.sql.php 中新加一张表格，
//就在以下数组中加一个元素，
//以便在卸除时删除
$wel_table_name_arr=array(
"#__wel_compflm",
"#__wel_currenm",
"#__wel_curtabh",
"#__wel_groupflm",
"#__wel_groupprog",
"#__wel_language",
"#__wel_pbasefm",
"#__wel_proghdrm",
"#__wel_rpttemp01",
"#__wel_shpviam",
"#__wel_userflm",
"#__wel_usergroup",
"#__wel_userprog",
"#__wel_altparm",
"#__wel_catcdem",
"#__wel_ecnhdrm",
"#__wel_engbomh",
"#__wel_engbomm",
"#__wel_famcdem",
"#__wel_gentenw",
"#__wel_partflm",
"#__wel_projflm",
"#__wel_unitflm",
"#__wel_delytom",
"#__wel_gentpow",
"#__wel_planpod",
"#__wel_planpoh",
"#__wel_pordetm",
"#__wel_porhdrm",
"#__wel_pormism",
"#__wel_portyem",
"#__wel_purhanm",
"#__wel_venmasm",
"#__wel_venparm",
"#__wel_venqdtm",
"#__wel_venqhdm",
"#__wel_centrem",
"#__wel_gentmow",
"#__wel_gentmrw",
"#__wel_gentwow",
"#__wel_mordetm",
"#__wel_morhdrm",
"#__wel_mrdetfm",
"#__wel_mrhdrfm",
"#__wel_wordetm",
"#__wel_worhdrm",
"#__wel_areaflm",
"#__wel_cuscont",
"#__wel_cusmasm",
"#__wel_cusmodm",
"#__wel_delyflm",
"#__wel_gentivw",
"#__wel_gentsow",
"#__wel_invdetm",
"#__wel_invhdrm",
"#__wel_invmism",
"#__wel_mkthanm",
"#__wel_paytflm",
"#__wel_sordetm",
"#__wel_sorhdrm",
"#__wel_sormism",
"#__wel_closedm",
"#__wel_countfm",
"#__wel_countft",
"#__wel_dndetfm",
"#__wel_dnhdrfm",
"#__wel_gentdnw",
"#__wel_genttow",
"#__wel_partwcm",
"#__wel_partwhm",
"#__wel_ptrdetm",
"#__wel_ptrhdrm",
"#__wel_rcvnote",
"#__wel_rtvnote",
"#__wel_tordetm",
"#__wel_torhdrm",
"#__wel_tranflm",
"#__wel_whctrlm",
"#__wel_whlocfm",
"#__wel_whlotfm",
"#__wel_whlotft",
"#__wel_proglofm",
"#__wel_syssets",
"#__wel_wrcache"
);

foreach ($wel_table_name_arr as $key=>$value){
	execute_sql("DROP TABLE IF EXISTS `".$value."`;");
}

//show execute error message
$execute_message=execute_sql("",true);
if (is_array($execute_message)){
	foreach ($execute_message as $key=>$message){
		echo '<div style="color:blue;">'.$message["error_sql"].'</div>';
		echo '<div style="color:red;"><B>'.$message["error_description"].'</B></div>';
	}
}

?>